<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class LiveToTest_model extends CI_Model
{ 

    //In use
    public function getFactoryData($factoryId)
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $this->db->where('factoryId', $factoryId);
        $query= $this->db->get();        
        return $query->row(); 
    }

     //In use
    public function getallReasons()
    {
        $this->db->select('*');
        $this->db->from('breakdownReason');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    } 
   
    //In use
    public function getallMachines($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        return $query;
    }

    public function getWhere($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

       //In use
    public function getWhereSingle($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }
    
    //In use
    public function getMachineStatusData($machineId,$choose1,$choose2,$machineStatus,$state,$viewStartTime = "",$viewEndTime = "",$breakViewHour = array())
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <',$viewEndTime);
        }

        if ($choose1 == "day") 
        {
            
            $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $this->factory->where('week',$week);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = $choose2[0];


            if ($month == "Jan") 
            {
                $month = "1";
            }
            elseif ($month == "Feb") 
            {
                $month = "2";
            }
            elseif ($month == "Mar") 
            {
                $month = "3";
            }
            elseif ($month == "Apr") 
            {
                $month = "4";
            }
            elseif ($month == "May") 
            {
                $month = "5";
            }
            elseif ($month == "Jun") 
            {
                $month = "6";
            }
            elseif ($month == "Jul") 
            {
                $month = "7";
            }
            elseif ($month == "Aug") 
            {
                $month = "8";
            }
            elseif ($month == "Sep") 
            {
                $month = "9";
            }
            elseif ($month == "Oct") 
            {
                $month = "10";
            }
            elseif ($month == "Nov") 
            {
                $month = "11";
            }
            elseif ($month == "Dec") 
            {
                $month = "12";
            }

            $year = $choose2[1];
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('year',$choose2);
        }
            if ($state == "noStacklight") 
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"1");
                $this->factory->where('state','running');
            }else
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
                $this->factory->where('state',$state);
            }
            $this->factory->where('analyticsGraphData.machineStatus',$machineStatus);

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }

            $query = $this->factory->get('analyticsGraphData');
           // echo $this->factory->last_query();exit;
            return $query->row();
    }

    public function getAnalyticsMachineCount()
    {
        $this->factory->group_by('machineId');
        $query = $this->factory->get('analyticsGraphData');
        return $query->num_rows();
    }

    //In use
    public function getHourData($machineId,$hour,$choose2,$state,$viewStartTime = "",$viewEndTime = "")
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <=',$viewEndTime);
        }

        $this->factory->where('hour',$hour);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getHourDataActualProducation($machineId,$hour,$choose2,$state,$viewStartTime = "",$viewEndTime = "")
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <=',$viewEndTime);
        }

        $this->factory->where('hour',$hour);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekData($machineId,$weekDate,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }


        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekDataActualProducation($machineId,$weekDate,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }

        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getMonthData($machineId,$day,$month,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            if ($state == "setup") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"1");
            }
            elseif ($state == "no_producation") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"2");
            }
            else
            {
                $this->factory->where('analyticsGraphData.machineStatus',"0");
                $this->factory->where('state',$state);
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
            }

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
                $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
            }

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }
            //echo $this->factory->last_query();exit;
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    //In use
    public function getMonthDataActualProducation($machineId,$day,$month,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"1");

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
                $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
            }

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    //In use
    public function getYearData($machineId,$month,$choose2,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }

        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getYearDataActualProducation($machineId,$month,$choose2,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }

        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getStopAnalyticsData($factory, $machineId, $choose1, $choose2, $choose4, $analytics) 
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
        }
        $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if($factory->table_exists($tableName) )
        { 
            $date = $choose1."-".$choose2Pad."-".$choose4;
            $sql = "SELECT $tableName.timeDiff as colorDuration, TIME_TO_SEC($tableName.originalTime) as timeS, $tableName.userId, notification.status, notification.comment, notification.commentFlag FROM $tableName left join notification on notification.logId = $tableName.logId WHERE $tableName.originalTime like '%".$date."%' order by $tableName.originalTime asc";
            $query = $factory->query($sql);
            return $query;  
        } 
        else 
        {
            return '';
        }
    } 
    
    
    //In use
    public function getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration, $maxDuration, $machineId=0, $analytics)
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
            $choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
            $date = $choose1."-".$choose2Pad."-".$choose4Pad;
            $this->factory->select('notification.notificationId, notification.comment, stopT.timeDiff' );  
            $this->factory->from('notification');
            $this->factory->where('notification.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('notification.machineId', $machineId); 
            }
            $this->factory->join($tableName.' as stopT','stopT.logId = notification.logId');   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('notification.type', $type); 
            $this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); 
            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else 
        {
            return '';
        }
    }

}   
?>
