<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Cron_model extends CI_Model
{ 

    function sendEmail($message,$subject,$url,$fileExt)
    {
        include APPPATH . 'third_party/sendgrid-php/sendgrid-php.php';

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("root@nyttdev.com", "NYTT ADMIN");
        $email->setSubject($subject.' '.emailSubject);
        $email->addTo('info@nytt-tech.com');//info@nytt-tech.com
        $email->addBcc( "support@insightech.ai");//support@insightech.ai
        //$email->addBcc( "support@insightech.ai");
        $email->addContent("text/plain", "subject");
        $email->addContent(
            "text/html",$message);

        if (!empty($url)) 
        {   
            $filename = basename( $url  );
            $file_encoded = base64_encode(file_get_contents($url));
            $attachment = new SendGrid\Mail\Attachment();
            $attachment->setType('application/'.$fileExt);
            $attachment->setContent($file_encoded);
            $attachment->setDisposition("attachment");
            $attachment->setFilename($filename);
            $email->addAttachment($attachment);
        }
        $sendgrid = new \SendGrid(('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'));
        try {
            $response = $sendgrid->send($email);
            $headers = $response->headers();

            if (!empty($url)) 
            {
                $headers = explode(":", $headers[7]);
            }else
            {
                $headers = explode(":", $headers[5]);
            }
            $MessageId = trim($headers[1]);

            $data = array("MessageId" => $MessageId,"insertTime" => date('Y-m-d H:i:s'),"message" => $message,"factoryId" => $this->factoryId);
            $this->CM->insertDBData($data,"emailStatus");
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }
 
    function addMNotificationAll($factoryId, $activeId, $categoryId, $addedDate) 
    { 
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
        $addField = array("activeId"=>$activeId,
                        "categoryId"=>$categoryId,
                        "addedDate"=>date('Y-m-d H:i:s', $addedDate),
                        "respondTime"=>date('Y-m-d H:i:s', $addedDate),
                        ); 
        $this->factory->insert('mNotificationv2',$addField); 
        $responseInsert['notificationId'] = $this->factory->insert_id(); 
        $categoryName = "";
        if($categoryId == '1') { $categoryName = "daily";}
        if($categoryId == '2') { $categoryName = "weekly";}
        if($categoryId == '3') { $categoryName = "monthly";}
        if($categoryId == '4') { $categoryName = "querterly";}
        if($categoryId == '5') { $categoryName = "yearly";}
        $responseInsert['categoryId'] = $categoryId; 
        if ($categoryId == '1') 
        {
            $responseInsert['notificationText'] = "Have you done your";
        }
        else
        {
            $responseInsert['notificationText'] = "Done with ".$categoryName." maintanance for assigned machines?";
        }
        return $responseInsert;
    }

    function updateCheckOut($factoryId,$activeId,$data) 
    { 
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where("activeId",$activeId);
        $this->factory->update("machineUserv2",$data);
    }

    function userUnlockMachine($factoryId, $machineId) 
    {           
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where('machineId',$machineId);
        $this->factory->update('machine',array("machineSelected"=>"0"));
    }

    function userLeaveMachine($factoryId, $activeId, $endTime) 
    {   
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $update = array(
                        "endTime"=>date('Y-m-d H:i:s', $endTime),  
                        "isActive"=>'0'
                        );
        $this->factory->where('activeId',$activeId);
        $this->factory->update('activeMachine',$update);
        return $activeId; 
    }

    public function updateData($where,$data, $table)
    {   
        $this->factory->where($where);
        $this->factory->update($table,$data);
    }

    function updateActivePhonePosition($activePhoneId)
    {   
        $this->factory->where("activePhoneId",$activePhoneId);
        $this->factory->set("isActive","0");
        $this->factory->set("endTime",date('Y-m-d H:i:s'));
        $this->factory->update('activePhonePosition');
    }

    public function getWhereDBJoin($select, $where, $join, $table)
    {   
        $this->db->select($select);
        $this->db->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->db->join($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function updateDBData($where,$data, $table)
    {   
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    public function addStopAndWaitNotification($type)
    { 
        //echo date('Y-m-d h:i:s');exit;
        $this->getLanguage();
        $date = date('Y-m-d H:i:s');
        $notifyField = "notify".$type; 
        $prolongedField = "prolonged".$type; 
        $compareStatus =  ($type == "Stop") ? "Stopped" : "Waiting"; 
        $notificationTitle =  ($type == "Stop") ? "Stop notification" : "Wait notification"; 
        $factories = $this->getAllFactoriesList();
        for($fact=0;$fact<count($factories);$fact++) 
        {
            $currentTime = date('Y-m-d H:i:s');
            $factory = $this->load->database('factory'.$factories[$fact]->factoryId, TRUE);   
            $factoryData = $this->getFactoryData($factories[$fact]->factoryId); 
            $sql2 = "SELECT betaLogImageColorOverview.color, machine.machineName, betaLogImageColorOverview.logId, betaLogImageColorOverview.machineId, machine.$notifyField as notifyField, machine.$prolongedField as prolongedField, TIMESTAMPDIFF(SECOND, originalTime, '$currentTime') as colorTime FROM betaLogImageColorOverview left join machine on betaLogImageColorOverview.machineId = machine.machineId order by  originalTime desc ";
            $query2 = $factory->query($sql2); 
            $redStatus = $query2->result();
        
            if(is_array($redStatus)) 
            { 
                for($x=0;$x<count($redStatus);$x++) 
                {
                    if ($redStatus[$x]->color != 'NoData' && $redStatus[$x]->color != 'NoDataStacklight' && $redStatus[$x]->color != 'NoDataError' && $redStatus[$x]->color != 'NoDataForceFully' && $redStatus[$x]->color != 'NoInternet' && $redStatus[$x]->color != 'NoDataHome' && $redStatus[$x]->color != 'NoDataRestart') 
                    {
                        $notifyFieldDur = $redStatus[$x]->notifyField*60;
                        $prolongedFieldDur = $redStatus[$x]->prolongedField*60;
                        $colorArrQ = '';
                        if($redStatus[$x]->color != 'NoData' && $redStatus[$x]->color != 'off' && $redStatus[$x]->color != 'Off' && $redStatus[$x]->color != 'NoDataStacklight' && $redStatus[$x]->color != 'NoDataError' && $redStatus[$x]->color != 'NoDataForceFully' && $redStatus[$x]->color != 'NoInternet' && $redStatus[$x]->color != 'NoDataHome' && $redStatus[$x]->color != 'NoDataRestart') 
                        {
                            $colorArr = explode(" ",$redStatus[$x]->color);
                            for($c=0;$c<count($colorArr);$c++) 
                            {
                                if($colorArr[$c] != 'and' && $colorArr[$c] != 'undefined' && $colorArr[$c] != '' ) 
                                {
                                    $colorArrQ .= " and ".strtolower($colorArr[$c])."Status = '1' "; 
                                }
                            }
                            if(!strpos($colorArrQ, 'redStatus')) 
                            {
                                $colorArrQ .= " and redStatus = '0' "; 
                            }
                            if(!strpos($colorArrQ, 'yellowStatus')) 
                            {
                                $colorArrQ .= " and yellowStatus = '0' "; 
                            }
                            if(!strpos($colorArrQ, 'greenStatus')) 
                            {
                                $colorArrQ .= " and greenStatus = '0' "; 
                            }
                            if(!strpos($colorArrQ, 'blueStatus')) 
                            {
                                $colorArrQ .= " and blueStatus = '0' "; 
                            }
                            if(!strpos($colorArrQ, 'whiteStatus')) 
                            {
                                $colorArrQ .= " and whiteStatus = '0' "; 
                            } 
                            
                        } 
                        else if($redStatus[$x]->color == 'off' || $redStatus[$x]->color == 'Off') 
                        {
                            $colorArrQ .= " and greenStatus = '0' and yellowStatus = '0' and redStatus = '0' and blueStatus = '0' and whiteStatus = '0'";
                        } 

                        $sqlStop = "SELECT machineStateVal from machineStateColorLookup where machineId = ".$redStatus[$x]->machineId." ".$colorArrQ; 
                        
                        $sqlStopQ = $factory->query($sqlStop); 
                        $sqlStopRes = $sqlStopQ->row();

                        if(isset($sqlStopRes) && trim($sqlStopRes->machineStateVal) == $compareStatus) 
                        { 
                            $stopDuration = $redStatus[$x]->colorTime;  
                            $commentFlag = '0'; 
                            $insertFlag = '0';
                            if($stopDuration >= $notifyFieldDur && $stopDuration < $prolongedFieldDur) 
                            {
                                if ($notifyFieldDur > 0) 
                                {   
                                    $insertFlag = '1';
                                }
                            } 
                            else if( $stopDuration >= $prolongedFieldDur && !empty($prolongedFieldDur)) 
                            {
                                if ($prolongedFieldDur > 0) 
                                {   
                                    $insertFlag = '1';
                                    $commentFlag = '1';
                                }
                            } 

                            if($insertFlag == '1') 
                            {  
                                $sqlMU = "SELECT `user`.*, 
                                    (
                                    SELECT machineUserv2.isActive 
                                    from `machineUserv2` 
                                    where `machineUserv2`.userId = `user`.userId 
                                    and (
                                        (FIND_IN_SET('". $redStatus[$x]->machineId ."', machineUserv2.machines) and workingMachine =  '') 
                                        or 
                                        (FIND_IN_SET('". $redStatus[$x]->machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
                                    ) 
                                    order by machineUserv2.activeId desc limit 1) as isActive 
                                    FROM r_nytt_main.`user` 
                                    WHERE `user`.isDeleted = '0' 
                                    and factoryId = ".$factories[$fact]->factoryId;
                                $queryMU = $factory->query($sqlMU); 
                                $machineUser[$x] = $queryMU->result();
                            
                                $userId = ''; $z=0; 
                                if(is_array($machineUser[$x]) && count($machineUser[$x]) > 0 ) 
                                { 
                                    $userDevice = array(); $p=0; 
                                    for($y=0;$y<count($machineUser[$x]);$y++) 
                                    {
                                        if($machineUser[$x][$y]->isActive != null) 
                                        {
                                            if($machineUser[$x][$y]->getNotified == '1' &&  $machineUser[$x][$y]->isActive == '1') 
                                            { 
                                                if(isset($this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken)) {
                                                    $userDevice[$p] = $this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken;

                                               $userDeviceId[$p] = $machineUser[$x][$y]->userId;

                                                } 
                                                else 
                                                {
                                                    $userDevice[$p] = '';
                                                }
                                                $p++; 
                                                if($z==0) 
                                                {
                                                    $userId = $machineUser[$x][$y]->userId;
                                                } 
                                                else 
                                                {
                                                    $userId .= ','.$machineUser[$x][$y]->userId; 
                                                }
                                                $z++;  
                                            }
                                        }
                                    } 
                                    
                                    $factory->select('notificationId, commentFlag');
                                    $factory->from('notification');
                                    $factory->where('isDeleted', '0');
                                    $factory->where('logId', $redStatus[$x]->logId);
                                    $queryN= $factory->get();
                                    $notification[$x] = $queryN->row();  
                                    
                                    $notificationText = $redStatus[$x]->machineName." " . strtolower($compareStatus).".";
                                    if($commentFlag == '1') 
                                    {
                                        $notificationText .= " Please explain reason for breakdown.";
                                    }
                                    
                                    if(!isset($notification[$x]) || (isset($notification[$x]) && $notification[$x]->commentFlag == '0' && $commentFlag == '1' )) 
                                    {
                                        if(!isset($notification[$x])) 
                                        {
                                            $insert=array(
                                                'userId'=>$userId, 
                                                'machineId'=>$redStatus[$x]->machineId,  
                                                'logId'=>$redStatus[$x]->logId,  
                                                'notificationText'=>$notificationText, 
                                                'addedDate'=>$date,  
                                                'respondTime'=>$date,  
                                                'type'=>$type, 
                                                'commentFlag'=>$commentFlag
                                            ); 
                                                
                                            $factory->insert('notification',$insert); 
                                            $notId = $factory->insert_id();

                                             $newData = array("notificationText" => $notificationText,"machineId" => $redStatus[$x]->machineId,"factoryId" => $factories[$fact]->factoryId,"flag" => "0");

                                            $this->db->insert("notificationLog",$newData);
                                        } 
                                        else 
                                        {
                                            $updateTime = $factory->where('notificationId', $notification[$x]->notificationId)->update('notification', array('commentFlag'=>$commentFlag,'notificationText'=>$redStatus[$x]->machineName." " . strtolower($compareStatus) ." . Please explain reason for breakdown."));  
                                            $notId = $notification[$x]->notificationId; 

                                            $newData = array("notificationText" => $redStatus[$x]->machineName." " . strtolower($compareStatus) ." . Please explain reason for breakdown.","machineId" => $redStatus[$x]->machineId,"factoryId" => $factories[$fact]->factoryId,"flag" => "0");

                                            $this->db->insert("notificationLog",$newData);
                                        }
                                        if($commentFlag == '1' ) 
                                        {
                                            $statusResponse  = $this->updateNotificationLiveStatus($factories[$fact]->factoryId, $notId, $redStatus[$x]->machineId, '0'); 
                                        }
                                        foreach ($userDeviceId as $key => $value) 
                                        {
                                            $userDeviceSignle = $this->getUserDevice($value)->deviceToken;
                                            $languageType = $this->getUserDevice($value)->languageType;
                                            if ($languageType == "2") 
                                            {
                                                if ($notificationTitle == "Stop notification") 
                                                {
                                                    $notificationTitleNew = Stopnotification2;
                                                    $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Stopped2).' ';
                                                }else
                                                {
                                                    $notificationTitleNew = Waitnotification2;
                                                    $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Waiting2).' ';
                                                }   

                                                if($commentFlag == '1') 
                                                {
                                                    $notificationTextNew .= strtolower(Pleaseexplainreasonforbreakdown2).'.';
                                                }
                                            }else
                                            {
                                                if ($notificationTitle == "Stop notification") 
                                                {
                                                    $notificationTitleNew = Stopnotification1;
                                                    $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Stopped1).' ';
                                                }else
                                                {
                                                    $notificationTitleNew = Waitnotification1;
                                                    $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Waiting1).' ';
                                                }  

                                                if($commentFlag == '1') 
                                                {
                                                    $notificationTextNew .= strtolower(Pleaseexplainreasonforbreakdown1).'.';
                                                }
                                            }
                                            $userSoundConfig = $this->getWhereSingle($factories[$fact]->factoryId, array("machineId" => $redStatus[$x]->machineId,"userId" => $value), "userSoundConfig");
                        
                                            if (!empty($userSoundConfig)) 
                                            {
                                                $machineSound = $userSoundConfig->sound;
                                            }
                                            else
                                            {
                                                $machineSound = "1";
                                            }

                                            $msg = array
                                            (
                                                'message'   => $notificationTextNew,
                                                'title'     => $notificationTitleNew,
                                                'subtitle'  => $notificationTitleNew,
                                                'vibrate'   => ($machineSound == "1" || $machineSound == "2") ? 1 : 0,
                                                'sound'     => "default",
                                                'machineSound' => $machineSound,
                                                'machineId' => $redStatus[$x]->machineId,
                                                'factoryId' => $factories[$fact]->factoryId,
                                                'commentFlag' => $commentFlag,
                                                'notificationUserLogId' => 0
                                            ); 

                                            $fields = array
                                            (
                                                'registration_ids'  => array($userDeviceSignle), 
                                                'data' => $msg
                                            );

                                            $headers = array
                                            (
                                                'Authorization: key=' . API_ACCESS_KEY,
                                                'Content-Type: application/json'
                                            );
                                            $ch = curl_init();
                                            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
                                            curl_setopt( $ch,CURLOPT_POST, true );
                                            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
                                            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                            curl_exec($ch);
                                            curl_close($ch);

                                            unset($machineSound);
                                            unset($userSoundConfig);
                                        }
                                    } 
                                }
                            }
                        } 
                    }
                }
            }
        }
    }



    public function addMtConnectStopAndWaitNotification()
    { 
        $this->getLanguage();
        $date = date('Y-m-d H:i:s');
        
        
        $factories = $this->getAllFactoriesList();
        for($fact=0;$fact<count($factories);$fact++) 
        {
            $currentTime = date('Y-m-d H:i:s');
            $factory = $this->load->database('factory'.$factories[$fact]->factoryId, TRUE);   
            $factoryData = $this->getFactoryData($factories[$fact]->factoryId); 
            $sql2 = "SELECT mtConnectOverview.State, machine.machineName, mtConnectOverview.mtConnectId, mtConnectOverview.machineId, machine.notifyStop as notifyFieldStop, machine.prolongedStop as prolongedFieldStop, machine.notifyWait as notifyFieldWait, machine.prolongedWait as prolongedFieldWait, TIMESTAMPDIFF(SECOND, originalTime, '$currentTime') as colorTime FROM mtConnectOverview left join machine on mtConnectOverview.machineId = machine.machineId where machine.machineStatus = '0' order by  originalTime desc ";

           // echo $sql2;exit;
            $query2 = $factory->query($sql2); 
            $redStatus = $query2->result();

        
            if(!empty($redStatus)) 
            { 
                for($x=0;$x<count($redStatus);$x++) 
                {
                    
                    if ($redStatus[$x]->State != 'ACTIVE' && $redStatus[$x]->State != 'UNAVAILABLE' && $redStatus[$x]->State != 'undefined' && $redStatus[$x]->State != '') 
                    {

                        if ($redStatus[$x]->State == 'STOPPED') {
                            $compareStatus =  "Stopped"; 
                            $notificationTitle =  "Stop notification"; 
                            $type =  "Stop";   
                            $notifyField = $redStatus[$x]->notifyFieldStop; 
                            $prolongedField = $redStatus[$x]->prolongedFieldStop;
                            $notifyFieldDur = $notifyField*60;
                            $prolongedFieldDur = $prolongedField*60;     
                        }else if($redStatus[$x]->State == 'INTERRUPTED' || $redStatus[$x]->State == 'READY')
                        {
                            $compareStatus =  "Waiting"; 
                            $notificationTitle = "Wait notification"; 
                            $type =  "Wait";
                            $notifyField = $redStatus[$x]->colorTime;
                            $notifyField = $redStatus[$x]->notifyFieldWait; 
                            $prolongedField = $redStatus[$x]->prolongedFieldWait;
                            $notifyFieldDur = $notifyField*60;
                            $redStatus[$x]->State;
                            $prolongedFieldDur = $prolongedField*60;
                        }

                         
                       
                        $stopDuration = $redStatus[$x]->colorTime; 
                        $commentFlag = '0'; 
                        $insertFlag = '0';
                        if($stopDuration >= $notifyFieldDur && $stopDuration < $prolongedFieldDur) 
                        {
                           
                            if ($notifyFieldDur > 0) 
                            {   
                               $insertFlag = '1';
                            }
                        } 
                        else if( $stopDuration >= $prolongedFieldDur && !empty($prolongedFieldDur)) 
                        {
                            if ($prolongedFieldDur > 0) 
                            {   
                                $insertFlag = '1';
                                $commentFlag = '1';
                            }
                        } 
                        if($redStatus[$x]->machineId == "473" && $factories[$fact]->factoryId == "5")
                        {   
                            //echo $commentFlag;echo "<br>";
                            //echo $insertFlag;echo "<br>";
                            //echo $stopDuration;echo "<br>";
                            //echo $notifyFieldDur;echo "<br>";
                            //echo $prolongedFieldDur;echo "<br>";
                        }
                        if ($insertFlag == '1') 
                        {
                            $sqlMU = "SELECT `user`.*, 
                                    (
                                    SELECT machineUserv2.isActive 
                                    from `machineUserv2` 
                                    where `machineUserv2`.userId = `user`.userId 
                                    and (
                                        (FIND_IN_SET('". $redStatus[$x]->machineId ."', machineUserv2.machines) and workingMachine =  '') 
                                        or 
                                        (FIND_IN_SET('". $redStatus[$x]->machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
                                    ) 
                                    order by machineUserv2.activeId desc limit 1) as isActive 
                                    FROM r_nytt_main.`user` 
                                    WHERE `user`.isDeleted = '0' 
                                    and factoryId = ".$factories[$fact]->factoryId;
                            $queryMU = $factory->query($sqlMU); 
                            $machineUser[$x] = $queryMU->result();
                           // echo $sqlMU;
                        }

                        $userId = ''; $z=0; 
                        if(is_array($machineUser[$x]) && count($machineUser[$x]) > 0 ) 
                        { 
                             if($redStatus[$x]->machineId == "473")
                            {   
                               echo "<pre>"; print_r($machineUser[$x]);
                            }
                            $userDevice = array(); $p=0; 
                            for($y=0;$y<count($machineUser[$x]);$y++) 
                            {
                                if($machineUser[$x][$y]->isActive != null) 
                                {
                                    if($machineUser[$x][$y]->getNotified == '1' &&  $machineUser[$x][$y]->isActive == '1') 
                                    { 
                                        if(isset($this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken)) {
                                            $userDevice[$p] = $this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken;

                                       $userDeviceId[$p] = $machineUser[$x][$y]->userId;

                                        } 
                                        else 
                                        {
                                            $userDevice[$p] = '';
                                        }
                                        $p++; 
                                        if($z==0) 
                                        {
                                            $userId = $machineUser[$x][$y]->userId;
                                        } 
                                        else 
                                        {
                                            $userId .= ','.$machineUser[$x][$y]->userId; 
                                        }
                                        $z++;  
                                    }
                                }
                            } 
                            
                            $factory->select('notificationId, commentFlag');
                            $factory->from('notification');
                            $factory->where('isDeleted', '0');
                            $factory->where('logId', $redStatus[$x]->mtConnectId);
                            $queryN= $factory->get();
                            $notification[$x] = $queryN->row();  
                            
                            $notificationText = $redStatus[$x]->machineName." " . strtolower($compareStatus).".";
                            if($commentFlag == '1') 
                            {
                                $notificationText .= " Please explain reason for breakdown.";
                            }
                            
                            if(!isset($notification[$x]) || (isset($notification[$x]) && $notification[$x]->commentFlag == '0' && $commentFlag == '1' )) 
                            {
                                if(!isset($notification[$x])) 
                                {
                                    $insert=array(
                                        'userId'=>$userId, 
                                        'machineId'=>$redStatus[$x]->machineId,  
                                        'logId'=>$redStatus[$x]->mtConnectId,  
                                        'notificationText'=>$notificationText, 
                                        'addedDate'=>$date,  
                                        'respondTime'=>$date,  
                                        'type'=>$type, 
                                        'commentFlag'=>$commentFlag
                                    ); 
                                        
                                    $factory->insert('notification',$insert); 
                                    $notId = $factory->insert_id();

                                     $newData = array("notificationText" => $notificationText,"machineId" => $redStatus[$x]->machineId,"factoryId" => $factories[$fact]->factoryId,"flag" => "0");

                                    $this->db->insert("notificationLog",$newData);
                                } 
                                else 
                                {
                                    $updateTime = $factory->where('notificationId', $notification[$x]->notificationId)->update('notification', array('commentFlag'=>$commentFlag,'notificationText'=>$redStatus[$x]->machineName." " . strtolower($compareStatus) ." . Please explain reason for breakdown."));  
                                    $notId = $notification[$x]->notificationId; 

                                    $newData = array("notificationText" => $redStatus[$x]->machineName." " . strtolower($compareStatus) ." . Please explain reason for breakdown.","machineId" => $redStatus[$x]->machineId,"factoryId" => $factories[$fact]->factoryId,"flag" => "0");

                                    $this->db->insert("notificationLog",$newData);
                                }
                                if($commentFlag == '1' ) 
                                {
                                    $statusResponse  = $this->updateNotificationLiveStatus($factories[$fact]->factoryId, $notId, $redStatus[$x]->machineId, '0'); 
                                }
                                foreach ($userDeviceId as $key => $value) 
                                {
                                    $userDeviceSignle = $this->getUserDevice($value)->deviceToken;
                                    $languageType = $this->getUserDevice($value)->languageType;
                                    if ($languageType == "2") 
                                    {
                                        if ($notificationTitle == "Stop notification") 
                                        {
                                            $notificationTitleNew = Stopnotification2;
                                            $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Stopped2).' ';
                                        }else
                                        {
                                            $notificationTitleNew = Waitnotification2;
                                            $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Waiting2).' ';
                                        }   

                                        if($commentFlag == '1') 
                                        {
                                            $notificationTextNew .= strtolower(Pleaseexplainreasonforbreakdown2).'.';
                                        }
                                    }else
                                    {
                                        if ($notificationTitle == "Stop notification") 
                                        {
                                            $notificationTitleNew = Stopnotification1;
                                            $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Stopped1).' ';
                                        }else
                                        {
                                            $notificationTitleNew = Waitnotification1;
                                            $notificationTextNew = $redStatus[$x]->machineName." " . strtolower(Waiting1).' ';
                                        }  

                                        if($commentFlag == '1') 
                                        {
                                            $notificationTextNew .= strtolower(Pleaseexplainreasonforbreakdown1).'.';
                                        }
                                    }
                                    $userSoundConfig = $this->getWhereSingle($factories[$fact]->factoryId, array("machineId" => $redStatus[$x]->machineId,"userId" => $value), "userSoundConfig");
                
                                    if (!empty($userSoundConfig)) 
                                    {
                                        $machineSound = $userSoundConfig->sound;
                                    }
                                    else
                                    {
                                        $machineSound = "1";
                                    }

                                    $msg = array
                                    (
                                        'message'   => $notificationTextNew,
                                        'title'     => $notificationTitleNew,
                                        'subtitle'  => $notificationTitleNew,
                                        'vibrate'   => ($machineSound == "1" || $machineSound == "2") ? 1 : 0,
                                        'sound'     => "default",
                                        'machineSound' => $machineSound,
                                        'machineId' => $redStatus[$x]->machineId,
                                        'factoryId' => $factories[$fact]->factoryId,
                                        'commentFlag' => $commentFlag,
                                        'notificationUserLogId' => 0
                                    ); 

                                    $fields = array
                                    (
                                        'registration_ids'  => array($userDeviceSignle), 
                                        'data' => $msg
                                    );

                                    $headers = array
                                    (
                                        'Authorization: key=' . API_ACCESS_KEY,
                                        'Content-Type: application/json'
                                    );
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    curl_exec($ch);
                                    curl_close($ch);

                                    unset($machineSound);
                                    unset($userSoundConfig);
                                }
                            } 
                        }

                    }
                }
            }
        }
    }

    function getLanguage()
    {       
        //$result = $this->getWhereDB(array("type" => 'cron'),"translations");
        $result = $this->db->where('find_in_set("cron", type) <> 0')->get('translations')->result_array();

        foreach ($result as $key => $value) 
        {
            define($value['textIdentify'].'1', $value['englishText']);
            define($value['textIdentify'].'2', $value['swedishText']);
        }
    }

    public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getWhereSingle($factoryId, $where, $table)
    {   
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    public function getAllFactoriesList()
    {
        $this->db->select('factoryId');
        $this->db->from('factory');
        $this->db->where('type', 'machine');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        return $query->result(); 
    }

    public function getFactoryData($factoryId)
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $this->db->where('factoryId', $factoryId);
        $query= $this->db->get();        
        return $query->row(); 
    }

    public function getUserDevice($userId)
    { 
        $this->db->select('deviceType, deviceToken, languageType');
        $this->db->from('user');
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    function updateNotificationLiveStatus($factoryId, $notificationId, $machineId, $flag) 
    {  
        $addField = array(
                        "notificationId"=>$notificationId,
                        "flag"=>$flag, 
                        "machineId"=>$machineId,
                        "factoryId"=>$factoryId,
                        ); 
        $this->db->insert('notificationLiveStatus',$addField); 
        return $notificationId;
    }

    public function insertDBData($data, $table)
    {   
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    public function getUserData($deviceToken)
    { 
        $this->db->from('user');
        $this->db->where('deviceToken', $deviceToken); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    public function getIncreaseReduceHour()
    { 
        $this->db->where('id', 1);
        $query= $this->db->get("increaseReduceHour");
        return $query->row();
    }

     public function getWhereJoin($factoryId, $select, $where, $join, $table)
    { 
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);  
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId,
                'setAppStartType' => "cron",
                'userId' => "0"
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,
                'data' => $data
            );
        $headers = array
            (
                'Authorization: key='.SETAPP_SERVER_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    function getLastActiveMachine($factoryId,$machineId)
    {
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);  
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;
    }

    public function getWhereJoinWithWhereIn($factoryId, $select, $where, $join, $whereIn, $table)
    { 
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);  
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($whereIn as $key => $value) 
        { 
            $this->factory->where_in($key,$value);
        }
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }
}
?>
