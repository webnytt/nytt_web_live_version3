<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Task_testing_model extends CI_Model
{ 
    //In use
    public function checkIsvalidated()
    {
        if($this->session->userdata('validated') && $this->session->userdata('url3') == 'live' && $this->session->userdata('factoryId') != '') {   
            return true;
        } else {
            return false;
        }
    }

    //In use
    public function getallFactories()
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    function getAllReportedUsers($factory)
    {
        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId  group by reportProblem.userId";  
        $query = $factory->query($sql)->result_array(); 
        return $query; 
    } 
    
    //In use
    function getUserPassword($userName) 
    {
        if($userName != '' ) 
        {
            $sql = "SELECT userPassword from user where userName = '".$userName."' and isDeleted  = '0'";    
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) 
            {
                $users = $query->row_array();   
                return base64_decode($users['userPassword']);
            } 
            else 
            {
                return false;
            }
            
        } 
        else 
        {
            return false;
        }
    }

    //In use
    public function getWhereDBSingle($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

    //In use
  	public function validate()
  	{
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean(base64_encode($this->input->post('password')));
        $this->db->where("userName like binary '$username'",NULL, FALSE);
        $this->db->where("userPassword like binary '$password'",NULL, FALSE);
        $this->db->where('isDeleted', '0'); 
        $query = $this->db->get('user');
        if($query->num_rows() == 1) 
        {
            $row = $query->row();
            $data = array(
                    'userId' => $row->userId,
                    'userName' => $row->userName,
                    'userImage' => $row->userImage,
                    'role' => $row->userRole,
                    'validated' => true,
                    'url3'=>'version2',
                    'userTheme'=> $row->theme, 
                    );
            if($row->userRole < 2 ) 
            {
                $data['factoryId'] = $row->factoryId; 
            }
            $this->session->set_userdata($data);
            if($row->userRole >= 2 ) 
            {
                redirect('admin/selectFactory');
            } 
            return $row;
        }
        return false;
    }
    
    //In use
    public function doLogout()
    {
        $this->session->sess_destroy();
        redirect('admin/login'); 
    }

    //In use
    public function checkUserRole()
    {
    	return (int)$this->session->userdata('role') ; 
    }

    //In use
    public function getMachinesOverview($factory, $isOperator, $userId, $factoryId)
    {
        $machineQ = '';
        if($isOperator == 1) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $userId);
            $this->db->where('isDeleted', '0');
            $query= $this->db->get(); 
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
            } else {
                $machineQ = " and 1 != 1 "; 
            }
        }
        
        $sql = "SELECT machine.*, betaLogImageColorOverview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag, machineCode.machineCode, machine.machineId
            FROM machine 
            join r_nytt_main.machineCode on machineCode.machineId = machine.machineId
            left join betaLogImageColorOverview on betaLogImageColorOverview.machineId = machine.machineId 
            left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
            WHERE machine.isDeleted = '0' AND  r_nytt_main.machineCode.factoryId = $factoryId $machineQ  order by machine.machineId";  
        $query = $factory->query($sql);   
        return $query; 
    }

    //In use
    public function getallColors()
    {
        $this->db->select('*');
        $this->db->from('color');
        $this->db->order_by('orderId','asc'); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    //In use
    public function getPhoneDetail($factory, $machineId)
    {
        $factory->select('phoneId');
        $factory->from('phoneDetailLogv2');
        $factory->where('machineId', $machineId);
        $factory->order_by('phoneId', 'desc');
        $query= $factory->get();
        $result = $query->row();
        if(!isset($result)) 
        {
            $result = new stdClass;
            $result->phoneId = 0;
        }
        return $result; 
    }

    //In use
    public function getVisulizationData($factory, $machineId, $type, $state, $choose1='', $choose2='', $choose3='', $choose4='') 
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and analyticsGraphData.machineId = $machineId";  
        }
        $yearQ = '';
        if($type == 'year' ) 
        {
            $yearQ = "and analyticsGraphData.year = $choose1";  
            $period_value = 'month';
        }
        $monthQ = '';
        if($type == 'month' ) 
        {
            $monthQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2";  
            $period_value = 'day';  
        }
        $weekQ = '';
        if($type == 'week' ) 
        {
            $weekQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.week = $choose3 and analyticsGraphData.week != 0  "; //and analyticsGraphData.weekday != 0 
            $period_value = 'weekday'; 
        }
        $dayQ = '';
        if($type == 'day' ) 
        {
            $dayQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2 and analyticsGraphData.day = $choose4"; 
            $period_value = 'hour'; 
        }
        $sql = "SELECT sum(duration) as countVal, $period_value FROM analyticsGraphData left join machine on analyticsGraphData.machineId = machine.machineId WHERE machine.isDeleted = '0' and  analyticsGraphData.machineStatus = '0' $machineQ and state='".$state."' $yearQ $monthQ $weekQ $dayQ group by $period_value order by $period_value asc"; 
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    function checkSetApp($factoryId, $machineId) 
    {
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $sql =  $this->factory
                    ->select('*')
                    ->where('machineId', intval($machineId))
                    ->order_by('appId', 'desc')
                    ->get('setAppLive'); 
        return $sql->row_array(); 
    }

    //In use
    public function getMachineStatusText($factory, $machineId, $redStatus, $greenStatus, $yellowStatus, $whiteStatus, $blueStatus)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and machineId = $machineId"; 
        }
        $sql = "SELECT machineStateVal FROM `machineStateColorLookup` where redStatus = '$redStatus' and greenStatus = '$greenStatus' and yellowStatus = '$yellowStatus' and whiteStatus = '$whiteStatus' and blueStatus = '$blueStatus' $machineQ"; 
        $query = $factory->query($sql); 
        if($query->num_rows() >= 1) 
        {
            $result = $query->row()->machineStateVal; 
        } 
        else 
        {
            $result = 'Off';
        }
        return $result; 
    }

    //In use
    public function checkUserRoleName()
    {
        if($this->session->userdata('role') == '0') 
        {
            $roleName = 'Operator';
        } 
        else if($this->session->userdata('role') == '1') 
        {
            $roleName = 'Factory Manager';
        } 
        else if($this->session->userdata('role') == '2') 
        {
            $roleName = 'Administrator';
        } 
        else if($this->session->userdata('role') == '3') 
        {
            $roleName = 'Developer';
        }
        return $roleName;
    }
	
    //In use
	public function getFactoryData($factoryId)
	{
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $this->db->where('factoryId', $factoryId);
        $query= $this->db->get();        
        return $query->row(); 
    }

    //In use
   	public function newMachine($factory, $insert)
  	{ 
        $factory->insert('machine',$insert);
        return $factory->insert_id();
    }

    //In use
    public function addMachineMaintananceSteps($factoryId, $machineId, $mTemplateId) 
    { 
    	$sql = "INSERT mSteps (factoryId, machineId, stepText, categoryId, stepOrder ) SELECT ".$factoryId.",".$machineId.", mTCText, mTCCategory, mTCOrder FROM maintenanceTemplateContent WHERE maintenanceTemplateContent.mTemplateId = 1 and maintenanceTemplateContent.isDeleted = '0' ";    
        $queryLog = $this->db->query($sql);  
        return $queryLog; 
    }

    //In use
    public function insertDBData($data, $table)
    {   
        $this->db->insert($table,$data);
    }

    //In use
    public function newMachineColorState($factory, $insert)
    { 
        $factory->insert('machineStateColorLookup',$insert);
        return $factory->insert_id();
    }

    //In use
    public function getWhere($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    //In use
    public function getWhereCount($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->num_rows();
    }

    public function displayTask()
    {
        $query=$this->db->query("select * from task_testing");
        return $query->result();
    }

    //In use
    public function insertData($data, $table)
    {   
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    function displayMachines()
    {
        $query=$this->db->query("select * from machineStatusLog");
        return $query->result();
    }

    //In use
    public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    //In use
    public function updateMachineColorState($factory, $update, $aclrId)
    { 
       	$factory->where('aclrId',$aclrId);
        $factory->update('machineStateColorLookup',$update);
    } 

    //In use
    public function updateMachine($factory, $update, $machineId)
    { 
        $factory->where('machineId',$machineId);
        $factory->update('machine',$update);
    } 


    //In use
    public function updateData($where,$data, $table)
    {   
        $this->factory->where($where);
        $this->factory->update($table,$data);
    }

    //In use
    public function deleteMachine($factory, $machineId)
    {
        $factory->where('machineId',$machineId);
        $update = array('isDeleted'=>'1');
        $factory->update('machine',$update);
    } 

    //In use
    public function unlockMachine($factory, $machineId)
    {
      	$factory->where('machineId',$machineId);
        $update = array('machineSelected'=>'0');
        $factory->update('machine',$update); 
    }

    //In use
    function getLastActiveMachine($machineId)
    {
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;
    }

    //In use
    function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId,
                'userId' => $this->session->userdata('userId')
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,
                'data' => $data
            );
		$headers = array
            (
                'Authorization: key='.SETAPP_SERVER_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //In use
    public function machineExists($factory, $name, $machineId=0)
    { 
        $factory->select('*');
        $factory->from('machine');
        $factory->where('machineName', $name);
        if($machineId != 0) {
            $factory->where('machineId != ', $machineId); 
        }
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        $row = $query->row();
        return $row;  
    }

    //In use
    public function userExists($userName, $userId=0)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userName', $userName);
        if($userId != 0) 
        {
            $this->db->where('userId != ', $userId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    //In use
    public function stackLightTypeExists($stackLightTypeName, $stackLightTypeId=0)
    { 
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('stackLightTypeName', $stackLightTypeName);
        if($stackLightTypeId != 0) 
        { 
            $this->db->where('stackLightTypeId != ', $stackLightTypeId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    //In use
    public function getallMachines($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        return $query;
    }

    //In use
    public function getallMachinesNoStacklight($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $factory->where('noStacklight', '0');
        $query= $factory->get();
        return $query;
    }

    //In use
    public function getAssignedMachines($factory, $userId)
    {
		$this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        $rowUser = $query->row();
        if($rowUser->machines != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId in (".$rowUser->machines.") AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine WHERE 1 != 1"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    public function getAssignedMachinesNoStacklight($factory, $userId)
    {
        $this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        $rowUser = $query->row();
        if($rowUser->machines != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId in (".$rowUser->machines.") AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine WHERE 1 != 1"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    public function getMachineStatusData($machineId,$choose1,$choose2,$machineStatus,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        if ($choose1 == "day") 
        {
            $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $this->factory->where('week',$week);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = $choose2[0];


            if ($month == "Jan") 
            {
                $month = "1";
            }
            elseif ($month == "Feb") 
            {
                $month = "2";
            }
            elseif ($month == "Mar") 
            {
                $month = "3";
            }
            elseif ($month == "Apr") 
            {
                $month = "4";
            }
            elseif ($month == "May") 
            {
                $month = "5";
            }
            elseif ($month == "Jun") 
            {
                $month = "6";
            }
            elseif ($month == "Jul") 
            {
                $month = "7";
            }
            elseif ($month == "Aug") 
            {
                $month = "8";
            }
            elseif ($month == "Sep") 
            {
                $month = "9";
            }
            elseif ($month == "Oct") 
            {
                $month = "10";
            }
            elseif ($month == "Nov") 
            {
                $month = "11";
            }
            elseif ($month == "Dec") 
            {
                $month = "12";
            }

            $year = $choose2[1];
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('year',$choose2);
        }
            if ($state == "noStacklight") 
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"1");
                $this->factory->where('state','running');
            }else
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
                $this->factory->where('state',$state);
            }
            $this->factory->where('analyticsGraphData.machineStatus',$machineStatus);
            $query = $this->factory->get('analyticsGraphData');
            //echo $this->factory->last_query();exit;
            return $query->row();
    }

    //In use
    public function getHourData($machineId,$hour,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        $this->factory->where('hour',$hour);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getHourDataActualProducation($machineId,$hour,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        $this->factory->where('hour',$hour);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekData($machineId,$weekDate,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekDataActualProducation($machineId,$weekDate,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getMonthData($machineId,$day,$month,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            if ($state == "setup") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"1");
            }
            elseif ($state == "no_producation") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"2");
            }
            else
            {
                $this->factory->where('analyticsGraphData.machineStatus',"0");
                $this->factory->where('state',$state);
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
            }
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    //In use
    public function getMonthDataActualProducation($machineId,$day,$month,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"1");
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    //In use
    public function getYearData($machineId,$month,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getYearDataActualProducation($machineId,$month,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getallReasons()
    {
        $this->db->select('*');
        $this->db->from('breakdownReason');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    } 

    //In use
    public function getStopAnalyticsData($factory, $machineId, $choose1, $choose2, $choose4, $analytics) 
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
        }
        $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if($factory->table_exists($tableName) )
        { 
            $date = $choose1."-".$choose2Pad."-".$choose4;
            $sql = "SELECT $tableName.timeDiff as colorDuration, TIME_TO_SEC($tableName.originalTime) as timeS, $tableName.userId, notification.status, notification.comment, notification.commentFlag FROM $tableName left join notification on notification.logId = $tableName.logId WHERE $tableName.originalTime like '%".$date."%' order by $tableName.originalTime asc";
            $query = $factory->query($sql);
            return $query;  
        } 
        else 
        {
            return '';
        }
    } 
    
    //In use
    public function getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration, $maxDuration, $machineId=0, $analytics)
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
            $choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
            $date = $choose1."-".$choose2Pad."-".$choose4Pad;
            $this->factory->select('notification.notificationId, notification.comment, stopT.timeDiff' );  
            $this->factory->from('notification');
            $this->factory->where('notification.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('notification.machineId', $machineId); 
            }
            $this->factory->join($tableName.' as stopT','stopT.logId = notification.logId');   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('notification.type', $type); 
            $this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); 
            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else 
        {
            return '';
        }
    }

    //In use
    public function isValidCurrentPassword($pass, $userId)
    { 
        $this->db->select('userId');
        $this->db->from('user');
        $this->db->where('userPassword', base64_encode($pass));
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    
    //In use
    public function getAllStackLightTypes($factoryId)
    {
        $this->db->select('stackLightType.*, GROUP_CONCAT(machine.machineId SEPARATOR ",") as machines');
        $this->db->from('stackLightType');
        $this->db->join('r_nytt_factory'.$factoryId.'.machine','stackLightType.stackLightTypeId = machine.stackLightTypeId','left'); 
        $this->db->where('stackLightType.isDeleted', '0');
        $this->db->group_by('stackLightType.stackLightTypeId'); 
        $query= $this->db->get();
        return $query; 
    }

    //In use
    function lastStackLightTypeId() 
    {
       $this->db->select('max(stackLightTypeId) as maxId'); 
       $this->db->from('stackLightType'); 
       $va=$this->db->get()->row();
       return $va; 
    } 

    //In use
    public function newStackLightType($insert)
    { 
        $this->db->insert('stackLightType',$insert);
        return $this->db->insert_id();
    } 

    //In use
    public function updateStackLightType($update, $stackLightTypeId)
    {
       $this->db->where('stackLightTypeId',$stackLightTypeId);
        $this->db->update('stackLightType',$update); 
    }

    //In use
    public function deleteStackLightType($stackLightTypeId)
    { 
        $this->db->where('stackLightTypeId',$stackLightTypeId);
        $update = array('isDeleted'=>'1');
        $this->db->update('stackLightType',$update); 
    }

    //In use
    public function getAllAppVersionCount($db)
    {
        $sql = "SELECT count(*) as totalCount FROM appVersions";
        // echo $sql; exit;
        $query = $this->db->query($sql); 
        return $query;  
  	}

    //In use
  	public function getAppVersionCount($app_version_id, $app_version_file,$app_version_name,$app_version_code,$update_file,$dateValS,$dateValE)
  	{ 
        $app_version_nameQ = '';
        if(!empty($app_version_name)) 
        {
            $app_version_nameQ = "and appVersions.app_version_id IN (".implode(",", $app_version_name).")";
        } 

        $dateQ = "and date(appVersions.created_date) >= '$dateValS' and date(appVersions.created_date) <= '$dateValE'";
          
    $sql = "SELECT count(*) as totalCount FROM appVersions WHERE 1=1  $searchQuery $dateQ $app_version_nameQ  order by app_version_id desc"; 
        // echo $sql; exit;

        $query = $this->db->query($sql); 
        return $query; 
    } 

    //In use
    public function getallAppVersion($start, $limit, $app_version_id, $app_version_file, $app_version_name, $app_version_code, $update_file, $dateValS, $dateValE)
    {
        $app_version_nameQ = '';
        if(!empty($app_version_name)) 
        {
            $app_version_nameQ = "and appVersions.app_version_id IN (".implode(",", $app_version_name).")";
        } 

        $dateQ = "and date(appVersions.created_date) >= '$dateValS' and date(appVersions.created_date) <= '$dateValE'";

        $sql = "SELECT * FROM appVersions WHERE 1=1  $searchQuery $dateQ $app_version_nameQ order by app_version_id desc limit 
        $start, $limit";  
        // echo $sql;exit;
	    $query = $this->db->query($sql); 
        return $query; 
    }



    public function getAllmachineStatusLogCount($factory)
    {
        $machineQ = '';
        if(!empty($machineId) && $machineId != 'all') 
        {
            $machineQ = "and taskMaintenace.machineId IN ($machineIds)";  
        }
        $sql = "SELECT count(*) as totalCount FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'";
        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function machineStatusLog_pagination($start, $limit, $logId, $machineId, $machineStatus, $insertTime, $dateValS, $dateValE,$machineName)
    { 
        $machineStatusQ = '';
        if(!empty($machineStatus)) 
        {
            $machineStatusQ = "and machineStatusLog.machineStatus IN (".implode(",", $machineStatus).")";
        } 

        $machineNameQ = '';
        if(!empty($machineName)) 
        {
            $machineNameQ = "and machineStatusLog.machineId IN (".implode(",", $machineName).")";
        } 

        $dateQ = "and date(machineStatusLog.insertTime) >= '$dateValS' and date(machineStatusLog.insertTime) <= '$dateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'  $searchQuery $machineStatusQ $dateQ $machineNameQ"; 
        // echo $sql; exit;
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallmachineStatusLog($start, $limit, $logId, $machineId, $machineStatus, $insertTime, $dateValS, $dateValE,$machineName)
    {
        $machineStatusQ = '';
        if(!empty($machineStatus)) 
        {
            $machineStatusQ = "and machineStatusLog.machineStatus IN (".implode(",", $machineStatus).")";
        }

        $machineNameQ = '';
        if(!empty($machineName)) 
        {
            $machineNameQ = "and machineStatusLog.machineId IN (".implode(",", $machineName).")";
        } 

        $dateQ = "and date(machineStatusLog.insertTime) >= '$dateValS' and date(machineStatusLog.insertTime) <= '$dateValE'";
        
        $sql = "SELECT machineStatusLog.*,machine.machineName FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'  $searchQuery $machineStatusQ $dateQ $machineNameQ order by machineStatusLog.logId desc limit $start, $limit";  
        $query = $this->factory->query($sql); 
        return $query; 
    }
    //In use
    public function getWhereDBWithGroupBY($where,$group_by,$table)
    {   
        $this->db->where($where);
        $this->db->group_by($group_by);
        $query = $this->db->get($table);
        return $query->result_array();
    }


    //In use
    public function getalloperatordetailslistCount($factory, $problemId, $is_admin=1)
    {
        $machineQ = '';
        if($problemId > 0 ) 
        {
            $machineQ = "and reportProblem.problemId = $problemId";  
        }
        $sql = "SELECT count(*) as totalCount FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1 $machineQ";
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchoperatordetailsProblemCount($factory, $problemId, $is_admin,$status,$type,$userName,$dateValS,$dateValE,$isActive)
    { 
        $statusQ = '';
        if(!empty($status)) 
        {
           $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }

        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and reportProblem.isActive IN (".implode(",", $isActive).")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {  
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        } 

        $dateQ = "and reportProblem.date >= '$dateValS' and reportProblem.date <= '$dateValE'";

        $sql = "SELECT count(*) as totalCount FROM reportProblem  left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1  $userNameQ $typeQ $isActiveQ $statusQ $dateQ";  
        $query = $factory->query($sql); 
        return $query; 
    }

    //In use
    public function getallReportProblemLogs($factory, $problemId , $is_admin=1, $start, $limit, $status,$type,$userName,$dateValS,$dateValE,$isActive)
    {
        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }

        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and reportProblem.isActive IN (".implode(",", $isActive).")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        }

        $dateQ = "and reportProblem.date >= '$dateValS' and reportProblem.date <= '$dateValE'";

        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1 $userNameQ $isActiveQ $typeQ $statusQ $dateQ order by problemId desc limit $start, $limit ";  
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    function updateReportProblem($factory,$problemId,$data)
    {
        $factory->where("problemId",$problemId);
        $factory->update("reportProblem",$data);
    }
	
    //In use
	public function addHelpReport($insert) 
    {   
        $this->db->insert('helpReport',$insert); 
    } 
    
    //In use
    public function getAllMaintenanceCheckinCount($factory, $userId, $is_admin=1)
    {
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join r_nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0'  $userQ group by machineUserv2.activeId";  
        $query = $this->factory->query($sql); 
        return $query; 
    }

    //In use
    public function getMaintenanceCheckin($factory, $dateValS, $dateValE, $userId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("machineUserv2.activeId", "user.userName", "machineUserv2.machineNames", "machineUserv2.isActive", "machineUserv2.startTime", "machineUserv2.endTime" ); 
        $orderCol = $columnArr[$columnIndex];
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }       
        $dateQ = " and machineUserv2.startTime BETWEEN '".$dateValS."' and '".$dateValE."' ";
        $sql = "SELECT machineUserv2.*, user.userName, mNotificationv2.addedDate, mNotificationv2.status, GROUP_CONCAT( maintenanceTemplateCategory.mTCategoryName separator ', ') as type FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join r_nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0' $userQ $dateQ $searchQuery group by machineUserv2.activeId order by $orderCol $columnSortOrder limit $start, $limit";   
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallOperators($factoryId)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('isDeleted', '0');
        $this->db->where('userRole', '0');
        $this->db->where('factoryId', $factoryId); 
        $query= $this->db->get();
        return $query;
    }

    //In use
    function lastUserId()
    {
       $this->db->select('max(userId) as maxId'); 
       $this->db->from('user'); 
       $va=$this->db->get()->row();
       return $va; 
    } 

    //In use
    public function newUser($insert)
    { 
        $this->db->insert('user',$insert);
        return $this->db->insert_id();
    } 

    //In use
    public function updateUser($update, $userId)
    { 
        $this->db->where('userId',$userId);
        $this->db->update('user',$update);
    } 

    //In use
    public function deleteUser($userId)
    { 
        $this->db->where('userId',$userId);
        $update = array('isDeleted'=>'1');
        $this->db->update('user',$update); 
    } 

    //In use
    function userLogOutOpApp($userId) 
    { 
        $checkIn = $this->AM->isUserCheckedIn($this->factoryId, $userId);
        if(isset($checkIn) && is_array($checkIn)) 
        {
            $data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s')); 
            $this->AM->updateCheckOut($this->factoryId,$checkIn['activeId'],$data); 
        }
        $logKeyOpApp = '0';
        $update = array("logKeyOpApp"=>$logKeyOpApp);
        $this->db->where('userId',$userId);
        $this->db->update('user',$update); 
        return $logKeyOpApp; 
    }

    //In use
    function isUserCheckedIn($factoryId, $userId) 
    {  
        $sql = "SELECT activeId FROM machineUserv2 WHERE userId = $userId and isActive = '1' order by activeId desc";  
        $query = $this->factory->query($sql);
        if ($query->num_rows() > 0) 
        {
            $result = $query->row_array();   
            return $result;
        } 
        else 
        {
            return false;
        }   
    }

    //In use
    function updateCheckOut($factoryId,$activeId,$data) 
    { 
        $this->factory->where("activeId",$activeId);
        $this->factory->update("machineUserv2",$data);
    }


    //In use
    public function getReportProblemById($factory,$problemId)
    {
        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE problemId = ".$problemId;  
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    public function getconfigureVersionsById($factory,$stackLightTypeId)
    {
        $sql = "SELECT stackLightType.*,stackLightType.stackLightTypeName,stackLightTypeAgv.stackLightTypeImage FROM stackLightType left join r_nytt_main.user on stackLightType.stackLightTypeId = stackLightType.stackLightTypeId WHERE stackLightTypeId = ".$stackLightTypeId;  
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    public function getallOperatorsById($factory,$activeId)
    {
        $sql = "SELECT machineUserv2.*,user.userName,user.userImage from machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId where activeId=".$activeId;
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    function sendNotification($message,$title,$fcmToken,$type)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId' => $this->session->userdata('factoryId')
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //In use
   
    //In use
    public function getallStackLightType()
    {
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    
     public function getallappVersions()
    {
        $this->db->select('*');
        $this->db->from('appVersions');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    public function getallmachine()
    {
        $this->db->select('*');
        $this->db->from('machineStatusLog');
        $query= $this->db->get();
        return $query;
    }

    public function getappVersionById($db, $app_version_id)
    {
        $sql = "SELECT * FROM appVersions WHERE app_version_id = ".$app_version_id; 
        // echo $sql; exit; 
        // $this->db->last_query();exit;
        $query = $db->query($sql)->row(); 
        return $query; 
    }

    
    //In use
    public function getAllStackLightVersion($stackLightTypeId)
    {
        $this->db->where(array("stackLightTypeId" => $stackLightTypeId));
        $query = $this->db->get('stackLightTypeFile');
        return $query->result_array();
    }

     public function getallStacklighttypeVersionsCount($factory, $stackLightTypeId, $is_admin=1)
    {
        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile WHERE 1=1 $machineQ";
        // echo $sql;exit; 
        $query = $this->db->query($sql); 
        return $query; 
    }
    
    //In use
    public function getSearchStacklighttypeVersionsCount($versionId, $stackLightTypeName, $stackLightTypeId, $versionName, $fileName, $dateValS, $dateValE)
    { 
        $stackLightTypeNameQ = '';
        if(!empty($stackLightTypeName)) 
        {
            $stackLightTypeNameQ = "and stackLightType.stackLightTypeId IN (".implode(",", $stackLightTypeName).")";
        } 

        $dateQ = "and date(stackLightTypeFile.versionDate) >= '$dateValS' and date(stackLightTypeFile.versionDate) <= '$dateValE'";

        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile left join r_nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE r_nytt_main.stackLightType.isDeleted = '0' $versionIdQ $stackLightTypeIdQ 
            $stackLightTypeNameQ $dateQ";

        $query = $this->db->query($sql); 
        return $query; 
    } 

    public function getallStacklighttypeVersionsLogs($start, $limit, $versionId, $stackLightTypeId, $stackLightTypeName, $versionName, 
        $fileName, $dateValS, $dateValE)
    {
        $stackLightTypeNameQ = '';
        if(!empty($stackLightTypeName)) 
        {
            $stackLightTypeNameQ = "and stackLightType.stackLightTypeId IN (".implode(",", $stackLightTypeName).")";
        } 

        $dateQ = "and date(stackLightTypeFile.versionDate) >= '$dateValS' and date(stackLightTypeFile.versionDate) <= '$dateValE'";
        
        $sql = "SELECT * FROM stackLightType left join r_nytt_main.stackLightTypeFile on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE r_nytt_main.stackLightType.isDeleted = '0' $versionIdQ $stackLightTypeNameQ $versionNameQ $dateQ order by versionId desc limit $start, $limit";

        $query = $this->db->query($sql); 
        return $query; 
    }

    public function getconfigureVersionById($db, $versionId)
    {
        $sql = "SELECT * FROM stackLightTypeFile left join r_nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stacklightTypeId WHERE  versionId = ". $versionId; 
        $query = $db->query($sql)->row(); 
        return $query; 
    }


    //In use
    function addReportImage($factoryId,$captureTime, $logTime, $fileExt, $machineId, $userId) 
    {  
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $insert = array("imageName"=>$machineId."_".$userId ."_".$logTime.".".$fileExt, 
                    "captureTime"=>date('Y-m-d H:i:s', $captureTime),
                    "logTime"=>date('Y-m-d H:i:s', $logTime),
                    "machineId"=>$machineId,
                    "userId"=>$userId,
                    );
        $this->factory->insert('reportedImages',$insert);  
        return $this->factory->insert_id(); 
    }

    //In use
    function machineExistsNew($machineId) 
    {       
        $sql = "SELECT * from machine where machineId = $machineId and isDeleted = '0' ";   
        $query = $this->factory->query($sql);
        if ($query->num_rows() > 0) 
        {
            $result = $query->row_array();   
            return $result;
        } 
        else 
        {
            return false;
        }
    } 

    //In use
    function checkFactoryDetail($factoryId) 
    { 
        $sql = "SELECT * from factory where factoryId = $factoryId and isDeleted  = '0'";    
        $query = $this->db->query($sql);
        $factory = $query->result_array();   
        return $factory;
    }

    //In use
    public function updateDBData($where,$data, $table)
    {   
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    //In use
    public function getNotificationLog($factoryId)
    {
        $this->db->where('factoryId',$factoryId);
        $this->db->where('status','pending');
        $this->db->order_by('notificationId','desc');
        $this->db->limit(2);
        $query = $this->db->get('notificationLog')->result_array();
        return $query;
    }

    //In use
    public function getallNotificationLogCount($factoryId, $notificationId, $is_admin=1)
    {
        $sql = "SELECT count(*) as totalCount FROM notificationLog WHERE factoryId = $factoryId";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchNotificationLogCount($factoryId, $notificationId, $is_admin=1)
    { 
        $sql = "SELECT count(*) as totalCount FROM notificationLog WHERE factoryId = $factoryId";  
        $query = $this->db->query($sql);
        return $query; 
    }

    //In use
    public function getallNotificationLogLogs($factoryId, $notificationId,$is_admin=1 , $start, $limit)
    {
        $sql = "SELECT machine.machineName,notificationLog.* FROM r_nytt_main.notificationLog left join machine on machine.machineId = notificationLog.machineId WHERE notificationLog.factoryId = $factoryId order by notificationLog.notificationId desc limit $start, $limit";
        $query = $this->factory->query($sql);
        return $query; 
    }

    //In use
    function updateNotificationLiveStatus($factoryId, $notificationId, $machineId, $flag) 
    {  
        $addField = array(
                        "notificationId"=>$notificationId,
                        "flag"=>$flag, 
                        "machineId"=>$machineId,
                        "factoryId"=>$factoryId,
                        ); 
        $this->db->insert('notificationLiveStatus',$addField); 
        return $notificationId;
    }
    
    //In use
    public function getallTaskMaintenaceCount()
    {
        $sql = "SELECT count(*) as totalCount FROM task_testing"; 
        $query = $this->db->query($sql); 
        return $query;
    }

    //In use
    public function getSearchTaskMaintenaceCount($isDelete, $type, $status, $dateValS, $dateValE)
    { 

        
        $isDeleteQ = '';
        if(!empty($isDelete)) 
        {
            $isDeleteQ = "and task_testing.id_deleted IN (".implode(",", $isDelete).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and task_testing.type IN (".implode(",", $type).")";
        }

        $statusQ = '';
        if(!empty($statusQ)) 
        {
            $statusQ = "and task_testing.type IN (".implode(",", $statusQ).")";
        }

        $dateQ = "and date(task_testing.created_date) >= '$dateValS' and date(task_testing.created_date) <= '$dateValE'";

        
        $sql = "SELECT count(*) as totalCount FROM task_testing WHERE 1=1 $isDeleteQ $typeQ $statusQ $dateQ"; 
        // echo $sql; exit;
        $query = $this->db->query($sql); 
        return $query; 
    }

    function gettype()
    {
        $this->db->select('task_id,type');
        $this->db->order_by("type","ASC");
        $query = $this->db->get('task_testing');
        return $query->result();
    }  
    function getstatus()
    {
        $this->db->select('task_id,status');
        $this->db->order_by("status","ASC");
        $query = $this->db->get('task_testing');
        return $query->result();
    }
    
    //In use
    public function getallTaskMaintenaceLogs($start, $limit, $isDelete, $type, $status, $dateValS, $dateValE)
    {
        $isDeleteQ = '';
        if(!empty($isDelete)) 
        {
            $isDeleteQ = "and task_testing.id_deleted IN (".implode(",", $isDelete).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and task_testing.type IN (".implode(",", $type).")";
        }

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and task_testing.status IN (".implode(",", $status).")";
        } 

        $dateQ = "and date(task_testing.created_date) >= '$dateValS' and date(task_testing.created_date) <= '$dateValE'";


        $sql = "SELECT * FROM task_testing WHERE 1=1 $isDeleteQ $typeQ $statusQ $dateQ order by task_id desc limit $start, $limit";
        // echo $sql; exit;
        $query = $this->db->query($sql);
        return $query; 
    }

    //In use
    public function getTaskById($db, $task_id)
    {
        $sql = "SELECT * FROM task_testing WHERE task_id = ". $task_id; 
        $query = $db->query($sql)->row(); 
        return $query; 
    } 

    //In use
    public function getWhereDBJoin($select, $where, $join, $table)
    {   
        $this->db->select($select);
        $this->db->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->db->join($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    //In use
    public function getWhereIn($select,$whereIn, $table)
    {   
        $this->factory->select($select);
        foreach ($whereIn as $key => $value) 
        { 
            $this->factory->where_in($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    //In use
    public function getWhereInWithWhere($select, $where, $whereIn, $table)
    {  
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($whereIn as $key => $value) 
        { 
            $this->factory->where_in($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    //In use
    function checkOperatorOnline($userId)
    {
        $this->factory->where('userId',$userId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('machineUserv2')->row();
        return $query;
    }

    //In use
    public function getWhereSingle($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    //In use
    public function get_operator_checkin_checkout_count()
    {
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE user.isDeleted = '0' group by machineUserv2.activeId";  
        $query = $this->factory->query($sql); 
        return $query; 
    }

    //In use
    public function get_search_operator_checkin_checkout_count($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE)
    { 
        $machineIdsQ = '';
        if(!empty($machineIds)) 
        {   
            $machineIdsQ .= "and (";
            foreach ($machineIds as $key => $value) 
            {
                $or = ($key == 0) ? "" : "or";
                $machineIdsQ .= " $or FIND_IN_SET('$value', machineUserv2.workingMachine)";
            }
            $machineIdsQ .= ")";
        }
       
        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and machineUserv2.userId IN (".implode(",", $userName).")";
        }

        $startTimeQ = "";
        if (!empty($startDateValS) && !empty($startDateValE)) 
        {
            $startTimeQ = "and date(machineUserv2.startTime) >= '$startDateValS' and date(machineUserv2.startTime) <= '$startDateValE'";
        }

        $endTimeQ = "";
        if (!empty($endDateValS) && !empty($endDateValE)) 
        {
            $endTimeQ = "and date(machineUserv2.endTime) >= '$endDateValS' and date(machineUserv2.endTime) <= '$endDateValE'";
        }
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId  WHERE user.isDeleted = '0' $userNameQ $startTimeQ  $endTimeQ $machineIdsQ  group by machineUserv2.activeId";   
        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function get_search_operator_checkin_checkout_logs($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE,$start, $limit)
    {   
        $machineIdsQ = '';
        if(!empty($machineIds)) 
        {   
            $machineIdsQ .= "and (";
            foreach ($machineIds as $key => $value) 
            {
                $or = ($key == 0) ? "" : "or";
                $machineIdsQ .= " $or FIND_IN_SET('$value', machineUserv2.workingMachine)";
            }
            $machineIdsQ .= ")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and machineUserv2.userId IN (".implode(",", $userName).")";
        }

        $startTimeQ = "";
        if (!empty($startDateValS) && !empty($startDateValE)) 
        {
            $startTimeQ = "and date(machineUserv2.startTime) >= '$startDateValS' and date(machineUserv2.startTime) <= '$startDateValE'";
        }

        $endTimeQ = "";
        if (!empty($endDateValS) && !empty($endDateValE)) 
        {
            $endTimeQ = "and date(machineUserv2.endTime) >= '$endDateValS' and date(machineUserv2.endTime) <= '$endDateValE'";
        }
        
        $sql = "SELECT machineUserv2.*, user.userName,user.userImage, mNotificationv2.addedDate, mNotificationv2.status FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE user.isDeleted = '0'  $userNameQ  $startTimeQ  $endTimeQ $machineIdsQ group by machineUserv2.activeId order by activeId  desc  limit $start, $limit";   
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function display_records($machineId)
    {
        $query = $this->factory->where('machineId',$machineId)->get('phoneDetailLogv2');
        return $query->row();
    }

    
    //In use
    public function getallBetaColorLogsCount2($factory, $machineId, $dateValS, $dateValE, $is_admin)
    {
        $machineQ = '';
        if(!empty($machineId)) 
        {
            $machineQ = "and betaDetectionLog.machineId IN (".implode(",", $machineId).")";
        }

        $colorQ = '';
        if(!empty($color)) 
        {
            $colorQ = "and betaDetectionLog.color IN (".implode(",", $color).")";
        }

        $dateQ = "and date(betaDetectionLog.originalTime) >= '$dateValS' and date(betaDetectionLog.originalTime) <= '$dateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM betaDetectionLog WHERE 1=1 $machineQ $colorQ $dateQ";
        $query = $this->factory->query($sql); 
        // echo $sql;exit;
        return $query;  
    }
    
    //In use
    public function getSearchBetaColorLogsCount2($factory, $dateValS, $dateValE, $machineId, $color, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if(!empty($machineId)) 
        {
            $machineQ = "and betaDetectionLog.machineId IN (".implode(",", $machineId).")";
        }

        $colorQ = '';
        if(!empty($color)) 
        {
            $colorQ = "and betaDetectionLog.color IN (".implode(",", $color).")";
        }

        $dateQ = "and date(betaDetectionLog.originalTime) >= '$dateValS' and date(betaDetectionLog.originalTime) <= '$dateValE'";

        $sql = "SELECT count(*) as totalCount FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $colorQ $dateQ $searchQuery ";  
        $query = $this->factory->query($sql); 
        // echo $sql;exit;
        return $query;  
    }

    public function getallBetaColorLogs2($factory, $dateValS, $dateValE, $machineId, $color, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("betaDetectionLog.logId", "user.userName", "machine.machineName", "betaDetectionLog.color", "betaDetectionLog.accuracy", "betaDetectionLog.originalTime"); 
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        if(!empty($machineId)) 
        {
            $machineQ = "and betaDetectionLog.machineId IN (".implode(",", $machineId).")";
        } 

        $colorQ = '';
        if(!empty($color)) 
        {
            $colorQ = "and betaDetectionLog.color IN (".implode(",", $color).")";
        }

        $dateQ = "and date(betaDetectionLog.originalTime) >= '$dateValS' and date(betaDetectionLog.originalTime) <= '$dateValE'";

        $sql = "SELECT betaDetectionLog.*, user.userName, machine.machineName FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $colorQ $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $this->factory->query($sql); 
        // echo $sql; exit;
        return $query; 
    }

   
    public function phoneMovementDetailLogv2display_records()
    {
        $query = $this->factory->query("select * from phoneMovementDetailLogv2 limit 50000");
        return $query->result();
    } 


    public function getAllnotificationListLogCount($factory)
    {
        $sql = "SELECT count(*) as totalCount FROM notification";
        // echo $sql; exit;
        $query = $this->factory->query($sql); 
        return $query;    
    }

    //In use
    public function getAllnotificationListLogpagination($start, $limit, $userId, $userName, $machineId, $machineName, $addedDate, $status, $respondTime, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)
    { 
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and notification.type IN (".implode(",", $type).")";
        } 

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and notification.status IN (".implode(",", $status).")";
        } 


        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and notification.userId IN (".implode(",", $userId).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and notification.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(notification.addedDate) >= '$dateValS' and date(notification.addedDate) <= '$dateValE'";

        $respondeddateQ = "and date(notification.respondTime) >= '$dueDateValS' and date(notification.respondTime) <= '$dueDateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM notification join machine on notification.machineId = machine.machineId join r_nytt_main.user on notification.userId = user.userId WHERE 1=1  $searchQuery $typeQ $statusQ $userNameQ $machineNameQ 
        $dateQ $respondeddateQ order by notificationId desc"; 

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallnotificationLog($row, $rowperpage, $userId, $userName, $machineId ,$machineName ,$addedDate , $status, $respondTime, $responderId, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)
    {
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and notification.type IN (".implode(",", $type).")";
        } 

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and notification.status IN (".implode(",", $status).")";
        } 


        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and notification.userId IN (".implode(",", $userId).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and notification.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(notification.addedDate) >= '$dateValS' and date(notification.addedDate) <= '$dateValE'";

        $respondeddateQ = "and date(notification.respondTime) >= '$dueDateValS' and date(notification.respondTime) <= '$dueDateValE'";
        
        $sql = "SELECT * FROM notification join machine on notification.machineId = machine.machineId join r_nytt_main.user on notification.userId = user.userId WHERE 1=1 $searchQuery $typeQ $statusQ $userNameQ $machineNameQ $dateQ $respondeddateQ order by notificationId desc"; 

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query;
    }
    public function getallBetaColorLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        //$columnArr = array("betaDetectionLog.logId", "user.userName", "machine.machineName", "betaDetectionLog.color", "betaDetectionLog.accuracy", "betaDetectionLog.originalTime"); 
        $columnArr = array("betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime");   
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        $userQ = ''; 
        $userStatusQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        //$sql = "SELECT betaDetectionLog.*, user.userName, machine.machineName FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 

        /* echo $sql = "SELECT * FROM
            (SELECT betaDetectionLog.machineId, betaDetectionLog.originalTime, betaDetectionLog.color, machine.machineName FROM betaDetectionLog left join machine on betaDetectionLog.machineId = machine.machineId where 1=1 $machineQ $dateQ $userQ UNION ALL 
            SELECT machineStatusLog.machineId, machineStatusLog.insertTime, machineStatusLog.machineStatus, machine.machineName FROM machineStatusLog left join machine on betaDetectionLog.machineId = machine.machineId where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable order by $orderCol $columnSortOrder limit $start, $limit"; */

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1 and betaDetectionLog.durationLogId != 0  $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by $orderCol $columnSortOrder limit $start, $limit";   
           // echo $sql;exit;
            
        $query = $this->factory->query($sql); 
        return $query; 
    }

     public function getallBetaColorLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1  and betaDetectionLog.durationLogId != 0  $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc";   
            
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    //In use
    public function getallBetaColorLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog WHERE 1=1 $machineQ $userQ";


        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime , color FROM betaDetectionLog where 1=1 and betaDetectionLog.durationLogId != 0 $machineQ $userQ UNION ALL 
            SELECT machineId, insertTime as originalTime, machineStatus FROM machineStatusLog where 1=1  $machineStatusQ $userStatusQ)betaTable";

        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    //In use
    public function getSearchBetaColorLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }

        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery "; 

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime, color FROM betaDetectionLog where 1=1 and betaDetectionLog.durationLogId != 0  $machineQ $dateQ $userQ UNION ALL 
            SELECT machineId, insertTime, machineStatus FROM machineStatusLog where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable left join machine on betaTable.machineId = machine.machineId where 1=1 $searchQuery ";
        //echo $sql;exit;

        $query = $this->factory->query($sql); 
        return $query;  
    } 
}   
?>
