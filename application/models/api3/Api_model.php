<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Api_model extends CI_Model
{ 
	

	function sendEmail($message,$subject,$url,$fileExt,$factoryId)
    {
        include APPPATH . 'third_party/sendgrid-php/sendgrid-php.php';

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("root@nyttdev.com", "NYTT ADMIN");
        $email->setSubject($subject.' '.emailSubject);
        $email->addTo('info@nytt-tech.com');//info@nytt-tech.com
        $email->addBcc( "support@insightech.ai");//support@insightech.ai
        //$email->addBcc( "support@insightech.ai");
        $email->addContent("text/plain", "subject");
        $email->addContent(
            "text/html",$message);

        if (!empty($url)) 
        {   
            $filename = basename( $url  );
            $file_encoded = base64_encode(file_get_contents($url));
            $attachment = new SendGrid\Mail\Attachment();
            $attachment->setType('application/'.$fileExt);
            $attachment->setContent($file_encoded);
            $attachment->setDisposition("attachment");
            $attachment->setFilename($filename);
            $email->addAttachment($attachment);
        }
        $sendgrid = new \SendGrid(('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'));
        try {
            $response = $sendgrid->send($email);
            $headers = $response->headers();
            $headers = explode(":", $headers[5]);
            $MessageId = trim($headers[1]);

            $data = array("MessageId" => $MessageId,"insertTime" => date('Y-m-d H:i:s'),"message" => $message,"factoryId" => $factoryId);
            $this->APIM->insertDBData($data,"emailStatus");
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }

    function getLanguage()
    {    
        $factoryId = !empty($_POST['factoryId']) ? $_POST['factoryId'] : "0";   
        $machineId = !empty($_POST['machineId']) ? $_POST['machineId'] : "0";   
    	$machine = $this->APIM->getWhereDBSingle(array("factoryId" => $factoryId,"machineId" => $machineId),"machineCode");
        //$result = $this->APIM->getWhereDB(array("type" => 'setappapi'),"translations");
        $result = $this->db->where('find_in_set("setappapi", type) <> 0')->get('translations')->result_array();

       // echo $this->db->last_query();exit;
        foreach ($result as $key => $value) 
        {
        	if (!empty($machine)) 
        	{
        		define($value['textIdentify'], ($machine->languageType == "2") ? $value['swedishText'] : $value['englishText']);
        	}else
        	{
            	define($value['textIdentify'], $value['swedishText']);
        	}
            define($value['textIdentify'].'1', $value['englishText']);
            define($value['textIdentify'].'2', $value['swedishText']);
        }
    }

    public function getWhereDBSingle($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

     public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }


	function checkMachineCode($machineCode)
	{
		//$query = $this->db->query("SELECT * FROM `machineCode` WHERE machineCode like binary '".base64_encode($machineCode)."'");
		$query = $this->db->query("SELECT * FROM `machineCode`  join r_nytt_main.factory on factory.factoryId = machineCode.factoryId WHERE factory.isDeleted = '0' and machineCode like binary '".base64_encode($machineCode)."'");
		if ($query->num_rows() > 0) 
        {
			$result = $query->row();
			$factoryId = $result->factoryId;
			$machineId = $result->machineId;

        	$this->factory = $this->load->database('factory'.$factoryId, TRUE);

        	$sql = "SELECT machine.*,machineCode.machineCode,machineCode.languageType
            FROM machine 
            join r_nytt_main.machineCode on machineCode.machineId = machine.machineId 
            WHERE machine.isDeleted = '0' AND machine.machineId = '$machineId'";  
        	$query1 = $this->factory->query($sql)->row(); 
        	if (!empty($query1)) 
        	{
        		$query1->factoryId = $factoryId;
        	}
        	return $query1;
		} 
		else 
		{
			return false;
		} 
	}	

	function insertDBData($data,$table)
	{
		$this->db->insert($table,$data);
		return $this->db->insert_id();
	}

	function insertFactoryData($factoryId,$data,$table)
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->insert($table,$data);
		return $this->factory->insert_id();
	}

	function updateFactoryData($factoryId,$where,$data,$table)
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->where($where);
		$this->factory->update($table,$data);
	}

	function updateDBData($where,$data,$table) 
	{ 
		$this->db->where($where);
		$this->db->update($table,$data); 
		return 1;
	}

	function phoneDetailExists($factoryId, $machineId) 
	{ //final_version_1  
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->select('phoneId');
        $this->factory->from('phoneDetailLogv2');
        $this->factory->where('machineId', intval($machineId));
        $phoneDetail = $this->factory->get()->row(); 
        return $phoneDetail; 
    }

    function updatePhoneDetailLog($phoneId, $factoryId, $currentTime, $model, $manufacture, $version, $batteryCapacity, $batteryLevel, $totalRam, $ramUsage, $cameraResolution, $deviceId, $appVersion, $appVersionCode, $wifiStatus, $networkType, $imsi, $totalStorage, $storageAvailable, $deviceResolution, $sdkVersion, $batteryHealth, $batteryType, $batteryTemperature, $chargingSource, $chargeFlag) 
    {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$update = array("model"=>$model,
					"manufacture"=>$manufacture,
					"version"=>$version,
					"batteryCapacity"=>$batteryCapacity, 
					"batteryLevel"=>$batteryLevel, 
					"totalRam"=>$totalRam, 
					"ramUsage"=>$ramUsage, 
					"cameraResolution"=>$cameraResolution, 
					"insertTime"=>date('Y-m-d H:i:s', $currentTime),
					"deviceId"=>$deviceId,
					"appVersion"=>$appVersion,
					"appVersionCode"=>$appVersionCode, 
					"wifiStatus"=>$wifiStatus, 
					"networkType"=>$networkType, 
					"imsi"=>$imsi, 
					"totalStorage"=>$totalStorage, 
					"storageAvailable"=>$storageAvailable, 
					"deviceResolution"=>$deviceResolution, 
					"sdkVersion"=>$sdkVersion, 
					"batteryHealth"=>$batteryHealth, 
					"batteryType"=>$batteryType, 
					"batteryTemperature"=>$batteryTemperature, 
					"chargingSource"=>$chargingSource, 
					"chargeFlag"=>$chargeFlag
					); 
		
		$this->factory->where('phoneId',$phoneId ); 
		$this->factory->update('phoneDetailLogv2',$update);
        return $phoneId;
    }

    function addPhoneDetailLog($factoryId, $currentTime, $machineId, $model, $manufacture, $version, $batteryCapacity, $batteryLevel, $totalRam, $ramUsage, $cameraResolution, $deviceId, $appVersion, $appVersionCode, $wifiStatus, $networkType, $imsi, $totalStorage, $storageAvailable, $deviceResolution, $sdkVersion, $batteryHealth, $batteryType, $batteryTemperature, $chargingSource, $chargeFlag) 
    { //final_version_1  
     	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$insert = array("model"=>$model,
					"manufacture"=>$manufacture,
					"version"=>$version,
					"batteryCapacity"=>$batteryCapacity, 
					"batteryLevel"=>$batteryLevel, 
					"totalRam"=>$totalRam, 
					"ramUsage"=>$ramUsage, 
					"cameraResolution"=>$cameraResolution, 
					"insertTime"=>date('Y-m-d H:i:s', $currentTime),
					"machineId"=>$machineId,
					"deviceId"=>$deviceId,
					"appVersion"=>$appVersion,
					"appVersionCode"=>$appVersionCode, 
					"wifiStatus"=>$wifiStatus, 
					"networkType"=>$networkType, 
					"imsi"=>$imsi, 
					"totalStorage"=>$totalStorage, 
					"storageAvailable"=>$storageAvailable, 
					"deviceResolution"=>$deviceResolution, 
					"sdkVersion"=>$sdkVersion, 
					"batteryHealth"=>$batteryHealth, 
					"batteryType"=>$batteryType, 
					"batteryTemperature"=>$batteryTemperature, 
					"chargingSource"=>$chargingSource, 
					"chargeFlag"=>$chargeFlag
					);  
		$this->factory->insert('phoneDetailLogv2',$insert); 
        return $this->factory->insert_id(); 
    }

    function checkFactoryDetail($factoryId) 
    { 
		$sql = "SELECT * from factory where factoryId = $factoryId and isDeleted  = '0'";    
		$query = $this->db->query($sql);
		$factory = $query->result_array();   
		return $factory;
	}

	function factoryExists($factoryId) 
	{ 	
		$sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'r_nytt_factory".$factoryId."'"; 
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		}
	}

	function getMachineStackLightType($factoryId, $machineId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT machine.stackLightTypeId, machine.machineLight, stackLightType.stackLightTypeVersion from machine left join r_nytt_main.stackLightType on stackLightType.stackLightTypeId = machine.stackLightTypeId where machine.machineId = ".$machineId." and machine.isDeleted = '0'";   
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row();
			return $result;
		} 
		else 
		{
			return -1;
		} 
	}

	function addLogImageFrame($factoryId, $originalTime, $file_name, $file_ext, $machineId) 
	{   
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$insert = array("imageName"=>$machineId."_".$file_name.".".$file_ext, 
					"originalTime"=>date('Y-m-d H:i:s', $originalTime),
					"logTime"=>date('Y-m-d H:i:s', $file_name),
					"uploadedTime"=>date('Y-m-d H:i:s'),
					"machineId"=>$machineId
					);
		
		$this->factory->insert('tempLogImageFrame',$insert);
		return $this->factory->insert_id(); 
    } 

    function getLastLogIdBeta($factoryId, $machineId) 
    { 	
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);   
		
		$sql = "SELECT logId FROM betaLogImageColorOverview WHERE machineId = $machineId order by logId desc"; 
		$query = $this->factory->query($sql);
		$content = $query->row(); 
		if($query->num_rows() >= 1) 
		{ 
			return $content->logId; 
		} 
		else 
		{
			return 0;
		}
	}

	function getVideoStatus($factoryId, $machineId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT * FROM machine WHERE machineId = '".$machineId."' and isDeleted  = '0'"; 
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		} 
	}

	function addNoInternetLog($factoryId, $startTime, $endTime, $machineId, $notificationType='0') 
	{	
		$this->getLanguage();
		if($notificationType == '0') 
		{
			$notificationTitle = InternetwasdowninSETAPP;
			$notificationText = Internetwasdownfrom." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
		} 
		else 
		{
			$notificationTitle = SETAPPwascrashedanddown;
			$notificationText = SETAPPwascrashedanddownfrom." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
		} 

		//not remove this code use in reminder notification
		$newData = array("notificationText" => $notificationText,"machineId" => $machineId,"factoryId" => $factoryId,"flag" => "0");

		$this->APIM->insertDBData($newData,"notificationLog");
		//echo $this->db->last_query();exit;
		$addField = array(
						"startTime"=>date('Y-m-d H:i:s', $startTime),
						"endTime"=>date('Y-m-d H:i:s', $endTime),
						"notificationType"=>$notificationType,
						);
		if($machineId != '') 
		{
			$addField['machineId'] = $machineId; 
			$machine = $this->machineExists($factoryId, $machineId); 
			$notificationTitle .= " ". forText ." ".$machine['machineName'];
			$notificationText .= " ". forText ." ".$machine['machineName']; 
		}
		if($userId != '') 
		{
			$addField['userId'] = $userId; 
		}  
		
		$addField['notificationText'] = $notificationText; 
		$this->factory->insert('noInternetLog',$addField);  
        $inserted =  $this->factory->insert_id();
			$sqlMU = "SELECT `user`.*, 
                (
                SELECT machineUserv2.isActive 
                from `machineUserv2` 
                where `machineUserv2`.userId = `user`.userId 
                and (
                    (FIND_IN_SET('". $machineId ."', machineUserv2.machines) and workingMachine =  '') 
                    or 
                    (FIND_IN_SET('". $machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
                ) 
                order by machineUserv2.activeId desc limit 1) as isActive 
                FROM r_nytt_main.`user` 
                WHERE `user`.isDeleted = '0' 
                and factoryId = ".$factoryId;
			
			$queryMU = $this->factory->query($sqlMU); 
			$machineUser = $queryMU->result();

			$userId = ''; $z=0; 
			
			if(is_array($machineUser) && count($machineUser) > 0 ) 
			{ 
				$userDevice = array(); $p=0; 
				for($y=0;$y<count($machineUser);$y++) 
				{
					if($machineUser[$y]->isActive != null) 
					{
						if($machineUser[$y]->getNotified == '1' &&  $machineUser[$y]->isActive == '1') 
						{ 
						if(!empty($machineUser[$y]->deviceToken)) 
							{
								$userSoundConfig = $this->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $machineUser[$y]->userId), "userSoundConfig");

								

								if($notificationType == '0') 
								{
									if ($machineUser[$y]->languageType == 2) 
									{
										$notificationTitleNew = InternetwasdowninSETAPP2;
										$notificationTextNew = Internetwasdownfrom2." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
									}else
									{
										$notificationTitleNew = InternetwasdowninSETAPP1;
										$notificationTextNew = Internetwasdownfrom1." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
									}
								} 
								else 
								{
									if ($machineUser[$y]->languageType == 2) 
									{
										$notificationTitleNew = SETAPPwascrashedanddown2;
										$notificationTextNew = SETAPPwascrashedanddownfrom2." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
									}else
									{
										$notificationTitleNew = SETAPPwascrashedanddown1;
										$notificationTextNew = SETAPPwascrashedanddownfrom1." ".date('d-m-Y H:i:s', $startTime)." ".to." ".date('d-m-Y H:i:s', $endTime);
									}
								} 

								if(!empty($machineId)) 
								{
									$machine = $this->machineExists($factoryId, $machineId); 
									
									if ($machineUser[$y]->languageType == 2) 
									{
										$notificationTitleNew .= " ".forText2." ".$machine['machineName'];
										$notificationTextNew .= " ".forText2." ".$machine['machineName']; 
									}else
									{
										$notificationTitleNew .= " ".forText1." ".$machine['machineName'];
										$notificationTextNew .= " ".forText1." ".$machine['machineName']; 
									}
								}
					
								if (!empty($userSoundConfig)) 
								{
									$machineSound = $userSoundConfig->sound;
								}
								else
								{
									$machineSound = "1";
								}

								$msg = array
								(
									'message' 	=> $notificationTextNew,
									'title'		=> $notificationTitleNew,
									'subtitle'	=> $notificationTextNew,
									'vibrate'   => ($machineSound == "1" || $machineSound == "2") ? 1 : 0,
					                'sound'     => "default",
					                'machineSound'  => $machineSound,
									'machineId' => $machineId,
									'factoryId' => $factoryId,
									'notificationUserLogId' => 0
								);

								$fields = array
								(
									'registration_ids' 	=> array($machineUser[$y]->deviceToken), 
									'data' => $msg
								);

								$headers = array
								(
									'Authorization: key=' . API_ACCESS_KEY,
									'Content-Type: application/json'
								);

								$ch = curl_init();
								curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
								curl_setopt( $ch,CURLOPT_POST, true );
								curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
								curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
								curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
								curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
								curl_exec($ch);
								curl_close($ch);
							}
						}
					}
				}
			}
	}

	function machineExists($factoryId, $machineId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT * from machine where machineId = $machineId and isDeleted = '0' ";   
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		}
	}

	function checkMachineDetail($factoryId, $machineId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT * from machine where machineId = $machineId and isDeleted = '0' "; 
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		}
	}

	public function getMachineOperator($factoryId, $machineId) 
	{	
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		/*$sql = "SELECT `user`.*, 
					(
					SELECT machineUserv2.isActive
					from `machineUserv2` 
					where `machineUserv2`.userId = `user`.userId 
					and (
						(FIND_IN_SET('". $machineId ."', machineUserv2.machines) and workingMachine =  '') 
						or 
						(FIND_IN_SET('". $machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
					) 
					order by machineUserv2.activeId desc limit 1) as isActive 
					FROM r_nytt_main.`user` 
					WHERE `user`.isDeleted = '0' 
					and factoryId = ".$factoryId;*/

		$sql = "SELECT `user`.*, 
					(
					SELECT machineUserv2.isActive
					from `machineUserv2` 
					where `machineUserv2`.userId = `user`.userId 
					and 
						(FIND_IN_SET('". $machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
					
					order by machineUserv2.activeId desc limit 1) as isActive 
					FROM r_nytt_main.`user` 
					WHERE `user`.isDeleted = '0' 
					and factoryId = ".$factoryId;
		return $this->factory->query($sql)->result_array();
	}

	public function getNewMachineOperator($factoryId, $machineId) 
	{	
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT `user`.*, 
					(
					SELECT machineUserv2.isActive 
					from `machineUserv2` 
					where `machineUserv2`.userId = `user`.userId 
					and (
						(FIND_IN_SET('". $machineId ."', machineUserv2.machines) and workingMachine =  '') 
						or 
						(FIND_IN_SET('". $machineId ."', machineUserv2.workingMachine) and workingMachine !=  '')
					) 
					order by machineUserv2.activeId desc limit 1) as isActive 
					FROM r_nytt_main.`user` 
					WHERE `user`.isDeleted = '0' 
					and factoryId = ".$factoryId;
		return $this->factory->query($sql)->result_array();
	}

	function sendbatteryLowNotification($message,$title,$fcmToken,$factoryId,$machineId,$batteryPercentage,$notificationUserLogId,$machineSound)
	{
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => ($machineSound == "1" || $machineSound == "2") ? 1 : 0,
                'sound'     => "default",
                'machineSound'     => $machineSound,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'batteryPercentage' => $batteryPercentage,
                'notificationUserLogId' => $notificationUserLogId
            );

        $fields = array
            (
                'registration_ids'  =>  $fcmToken,  
                'data' => $data,
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        //print_r($result);exit;
        curl_close($ch);
    }

    function sendBatteryTemperatureNotification($message,$title,$fcmToken,$factoryId,$machineId,$batteryTemperature,$notificationUserLogId,$machineSound)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => ($machineSound == "1" || $machineSound == "2") ? 1 : 0,
                'sound'     => "default",
                'machineSound'     => $machineSound,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'batteryPercentage' => $batteryPercentage,
                'notificationUserLogId' => $notificationUserLogId
            );

        $fields = array
            (
                'registration_ids'  =>  $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }
	

	public function addHelpReport($factoryId, $currentTime, $problemText, $workDuration, $file_ext) 
	{   
		$insert = array(
					"problemText"=>$problemText,
					"workDuration"=>$workDuration,
					"factoryId"=>$factoryId,
					"currentTime"=>date('Y-m-d H:i:s', $currentTime),
					);
		if($file_ext != '') 
		{
			$insert["image"] = $currentTime.".".$file_ext;
		} 
		$this->db->insert('helpReport',$insert); 
		return true; 
    } 

    function getFactoryDetail($factoryId) 
    { 	
		$sql = "SELECT factoryName from factory where factoryId = $factoryId and isDeleted  = '0'";    
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) 
		{
			$factory = $query->row_array();   
			return base64_decode($factory['factoryName']);
		} 
		else
		{
			return false;
		}
	}

	function checkLatestAppVersion($app_type)
	{
		$this->db->where(array("app_type" => $app_type));
		$this->db->order_by('app_version_id','desc');
		return $this->db->get('appVersions')->row();
	}

	function getLastMachineSelectedStatus($factoryId,$machineId,$deviceId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->where(array("machineId" => $machineId,"deviceId" => $deviceId));
		$query = $this->factory->get('phoneDetailLogv2');
		return $query->result_array();
	}

	function checkScheduleImageCapturingStatus($factoryId,$machineId,$currentTime)
	{
		$this->db->where(array("factoryId" => $factoryId,"machineId" => $machineId,"startTime <=" =>  $currentTime,"endTime >=" =>  $currentTime));
		$query = $this->db->get('scheduleImageCapturing');
		return $query->row();
	}

	public function getWhereSingle($factoryId, $where, $table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    function virtualMachineLogLastRecord($factoryId,$IOName)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where(array("IOName" => $IOName));
        $this->factory->order_by("logId","desc");
        $query = $this->factory->get("virtualMachineLog");
        return $query->row();
    }

    public function unlockMachine($factoryId, $machineId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where('machineId',$machineId);
        $update = array('machineSelected'=>'0');
        $this->factory->update('machine',$update); 
    }

    function getLastActiveMachine($factoryId, $machineId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;
    }

    function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId,
                'setAppStartType' => "dashboard",
                'userId' => $this->session->userdata('userId')
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,
                'data' => $data
            );
        $headers = array
            (
                'Authorization: key='.SETAPP_SERVER_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }


    public function getallColors()
    {
        $this->db->select('*');
        $this->db->from('color');
        $this->db->order_by('colorId','asc'); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
}