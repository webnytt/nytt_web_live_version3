<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Oppapi_model extends CI_Model
{ 
	function getUser($userName, $password) 
	{	
		if($userName != '' && $password != '') 
		{	
			$sql = "SELECT user.*, factory.factoryName from user left join factory on factory.factoryId = user.factoryId where user.userName = '".$userName."' and user.userPassword = '".base64_encode($password)."' and user.isDeleted  = '0'";    
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) 
			{
				$users = $query->row_array();  
				return $users;
			} 
			else 
			{
				return false;
			}
			
		} 
		else 
		{
			return false;
		}
	}

	function getRespondedNotifications($factoryId, $machineId, $userId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$today = date("Y-m-d");
		$sql = "SELECT * from notification where machineId = $machineId and find_in_set($userId,userId) <> 0  and isDeleted = '0' and status = '1' and respondTime like '%".$today."%' order by notificationId desc limit 100 ";    
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->result();
			return $result;
		} 
		else 
		{
			return 0;
		} 
	}

	function getNotifications($factoryId, $machineId, $userId) 
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT * from notification where machineId = $machineId and find_in_set($userId,userId) <> 0  and isDeleted = '0' and status = '0' and commentFlag = '1' order by notificationId desc limit 100 ";   
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->result();
			return $result;
		} 
		else 
		{
			return 0;
		} 
	}

	function getWarningNotifications($factoryId, $machineId, $userId, $completion = "") 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$completionQ = "";
		if ($completion == "0") 
		{
			$completionQ = "and status = '0'";
		}
		elseif ($completion == "1") 
		{
			$completionQ = "and status = '1'";
		}
		$date = date('Y-m-d H:i:s',strtotime('-1 hour'));
		$dayDate = date('Y-m-d H:i:s',strtotime('-24 hour'));
		$sql = "SELECT * from notification where machineId = $machineId and isDeleted = '0' and commentFlag = '1' $completionQ and ((respondTime >  '$date' and status = '1') or (addedDate >  '$dayDate' and status = '0') ) order by notificationId desc limit 100 ";   
		$query = $this->factory->query($sql);
		$result = $query->result();
		return $result;
	}
	
	function getNotificationTime($factoryId, $machineId, $logId, $month, $year) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$month = str_pad($month, 2, '0', STR_PAD_LEFT);
        $tableName = $machineId.'stoppedStateBetalogWithState'.$month.$year;
        $monthPad = str_pad($month, 2, '0', STR_PAD_LEFT);
        if($this->factory->table_exists($tableName) )
        { 
            $sql = "SELECT $tableName.timeDiff as colorDuration  FROM $tableName WHERE logId = ".$logId;
            $query = $this->factory->query($sql)->row();
            return $query;  
        } 
        else 
        {
            return '';
        }
	}

	function updateData($where,$data,$table) 
	{ 
		$this->db->where($where);
		$this->db->update($table,$data); 
		return 1;
	}

	function insertFactoryData($factoryId,$data,$table)
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->insert($table,$data);
		return $this->factory->insert_id();
	}

	function updateFactoryData($factoryId,$where,$data,$table)
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$this->factory->where($where);
		$this->factory->update($table,$data);
		//echo $this->factory->last_query();exit;
	}

	function userLogOutOpApp($userId, $endTime) 
	{  
		$userDetail = $this->APIM->getUserDetail($userId);
		$checkIn = $this->APIM->isUserCheckedIn($userDetail->factoryId, $userId);
		if(isset($checkIn) && is_array($checkIn)) {
			$data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s',$endTime)); 
			$this->APIM->updateCheckOut($userDetail->factoryId,$checkIn['activeId'],$data); 
		}
		$logKeyOpApp = '0';
		$update = array("logKeyOpApp"=>$logKeyOpApp);
		$this->db->where('userId',$userId);
		$this->db->update('user',$update); 
		return $logKeyOpApp; 
		
	}

	function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId,$userId)
	{
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId,
                'setAppStartType' => "opapp",
                'userId'      => $userId
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key='.SETAPP_SERVER_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }
    function updateLogDate($userId, $logDate) 
    { 
		$logKeyOpApp = '1';
		$update = array("logDate"=>date('Y-m-d H:i:s', $logDate),"logKeyOpApp"=>$logKeyOpApp);
		$this->db->where('userId',$userId);
		$this->db->update('user',$update); 
		return $logKeyOpApp;
		
	}

	function sendNotification($message,$title,$fcmToken,$type,$factoryId = 0)
	{
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId' => $factoryId
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }
    
    function getUserPassword($userName) 
    {  
		if($userName != '' ) 
		{
			$sql = "SELECT userPassword from user where userName = '".$userName."' and isDeleted  = '0'";    
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) 
			{
				$users = $query->row_array();   
				return base64_decode($users['userPassword']);
			} 
			else 
			{
				return false;
			}
			
		} 
		else 
		{
			return false;
		}
	}

	function getUserFactory($userName) 
	{  
		if($userName != '' ) 
		{
			$sql = "SELECT factory.factoryName from user left join factory on factory.factoryId = user.factoryId where user.userName = '".$userName."' and user.isDeleted  = '0'";    
			$query = $this->db->query($sql);
			if ($query->num_rows() > 0) 
			{
				$users = $query->row_array();   
				return base64_decode($users['factoryName']);
			} 
			else 
			{
				return false;
			}
			
		} 
		else 
		{
			return false;
		}
	}

	function userExists($userId) 
	{ 
		$sql = "SELECT * from user where userId = $userId and isDeleted = '0' ";   
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		}
		
	}

	public function userExistsName($userName, $userId=0)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userName', $userName);
        if($userId != 0) 
        {
            $this->db->where('userId != ', $userId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

	function checkFactoryDetail($factoryId) 
	{ 
		$sql = "SELECT * from factory where factoryId = $factoryId and isDeleted  = '0'";    
		$query = $this->db->query($sql);
		$factory = $query->result_array();   
		return $factory;
	}

	function updateUserDevice($userId, $deviceToken) 
	{ 
		global $con; 
		$updateField = array(
						"deviceType"=>"Android",  
						"deviceToken"=>$deviceToken
						);	 
		$this->db->where('userId',$userId);
		$this->db->update('user',$updateField);
		return $userId;
	}

	function updateUserNotified($userId, $getNotified) 
	{ 
		$updateField = array();  
		if($getNotified != '') 
		{
			$updateField['getNotified'] = $getNotified;
		}
		$this->db->where('userId',$userId); 
		$this->db->update('user',$updateField);
		return 1; 
	}

	public function getUserDetail($userId)
	{   
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row;
    }
    
    public function userNameExists($userName, $userId=0) 
    { 
	    $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userName', $userName);
        $this->db->where('userId != ', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    function updateUser($userId, $userName='', $userGender='', $userAge='', $userExperience='', $userImage='') 
    {
		$updateField = array();  
		if($userName != '') 
		{
			$updateField['userName'] = $userName;
		}

		if($userAge != '') 
		{
			$updateField['userAge'] = $userAge;
		}

		if($userExperience != '') 
		{
			$updateField['userExperience'] = $userExperience;
		}

		if($userGender != '') 
		{
			$updateField['userGender'] = $userGender;
		}

		if($userImage != '') 
		{
			$updateField['userImage'] = $userImage; 
		}

		$this->db->where('userId',$userId); 
		$this->db->update('user',$updateField);
		return 1; 
	}

	function factoryExists($factoryId) 
	{ 
		$sql = "SELECT SCHEMA_NAME FROM INFORMATION_SCHEMA.SCHEMATA WHERE SCHEMA_NAME = 'r_nytt_factory".$factoryId."'"; 
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->row_array();   
			return $result;
		} 
		else 
		{
			return false;
		}
	}

	function getAssignedMachines($factoryId, $userId) 
	{ 
		if($userId != '') 
		{
			$this->factory = $this->load->database('factory'.$factoryId, TRUE);   
			$sqlU = "SELECT machines, userRole from user where userId = '".$userId."' and isDeleted  = '0'";    
			$queryU = $this->db->query($sqlU);
			$user = $queryU->row_array(); 
			
			if($user['userRole'] == '0') 
			{
				if($user['machines'] != '') 
				{
					$queryM = " and machineId in (".$user['machines'].") "; 
				} 
				else 
				{
					$queryM = ' and 1 != 1 ';
				}
			} 
			else 
			{
				$queryM = '';
			}
			
			$sql = "SELECT * from machine where isDeleted  = '0' $queryM ";    
			$query = $this->factory->query($sql);
			$result = $query->result();
			return $result;
		} 
		else 
		{
			return false;
		}
	}

	function userCheckIn($factoryId, $machines, $machineNames, $userId, $startTime,$workingMachine) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$addField = array("machines"=>$machines,
						"machineNames"=>$machineNames,
						"workingMachine"=>$workingMachine,
						"userId"=>$userId,
						"startTime"=>date('Y-m-d H:i:s', $startTime), 
						"endTime"=>date('Y-m-d H:i:s', $startTime),  
						"isActive"=>'1'
						);	 
		$this->factory->insert('machineUserv2',$addField);  
        return $this->factory->insert_id();
	}

	

	function getPendingMNotifications($factoryId, $userId, $startTime) 
	{   
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$compareDate = date("Y-m-d 00:00:00", strtotime('-1 day', $startTime)); 
		$sql = "SELECT mNotificationv2.notificationId, mNotificationv2.categoryId FROM mNotificationv2 left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId where machineUserv2.userId = $userId and mNotificationv2.status = '0' and machineUserv2.startTime >= '".$compareDate."'";    
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) 
		{
			$result = $query->result();  
			for($i=0;$i<count($result);$i++) 
			{
				$categoryName = '';
				if($result[$i]->categoryId == '1') { $categoryName = "yesterday's"; }
				if($result[$i]->categoryId == '2') { $categoryName = "last week's"; }
				if($result[$i]->categoryId == '3') { $categoryName = "last month's"; }
				if($result[$i]->categoryId == '4') { $categoryName = "last quarter's"; }
				if($result[$i]->categoryId == '5') { $categoryName = "last year's"; }
				if ($result[$i]->categoryId == '1') {
					$result[$i]->notificationText = "Have you done your" ;
				}else
				{
					$result[$i]->notificationText = "Done with ".$categoryName." maintanance for assigned machines?" ;
				}
			}
			return $result;
		} 
		else 
		{
			$result = array();
			return $result; 
		}
	}

	function checkInData($factoryId, $activeId) {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		return $this->factory
					->where(array('activeId' => $activeId,"isActive" => "1"))
					->get('machineUserv2')
					->result_array();
	}

	function updateCheckOut($factoryId,$activeId,$data) {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);

		$this->factory->where("activeId",$activeId);
		$this->factory->update("machineUserv2",$data);
	}

	function addMNotificationAll($factoryId, $activeId, $categoryId, $addedDate) {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$addField = array("activeId"=>$activeId,
						"categoryId"=>$categoryId,
						"addedDate"=>date('Y-m-d H:i:s', $addedDate),
						"respondTime"=>date('Y-m-d H:i:s', $addedDate),
						); 
		$this->factory->insert('mNotificationv2',$addField); 
		$responseInsert['notificationId'] = $this->factory->insert_id(); 
		$categoryName = "";
		if($categoryId == '1') { $categoryName = "daily";}
		if($categoryId == '2') { $categoryName = "weekly";}
		if($categoryId == '3') { $categoryName = "monthly";}
		if($categoryId == '4') { $categoryName = "querterly";}
		if($categoryId == '5') { $categoryName = "yearly";}
		$responseInsert['categoryId'] = $categoryId; 
		if ($categoryId == '1') {
			$responseInsert['notificationText'] = "Have you done your";
		}else
		{
			$responseInsert['notificationText'] = "Done with ".$categoryName." maintanance for assigned machines?";
		}
        return $responseInsert;
	}

	function insertProblem($factoryId,$data){
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	$this->factory->insert("reportProblem",$data);
    }
    function getHelpText(){
		
		$sql = "SELECT infoText from generalInfo where infoName  = 'Help' ";   
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
	}

	function getProblemReportText() { 
		
		$sql = "SELECT infoText from generalInfo where infoName  = 'Report A Problem' ";   
		$query = $this->db->query($sql);
		$result = $query->row_array();
		return $result;
		
	}

	function getFactoryDetail($factoryId) { 
		
		$sql = "SELECT factoryName from factory where factoryId = $factoryId and isDeleted  = '0'";    
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$factory = $query->row_array();   
			return base64_decode($factory['factoryName']);
		} else {
			return false;
		}
	}
	public function addHelpReport($factoryId, $userId, $currentTime, $problemText, $workDuration, $file_ext) { 
        
		$insert = array(
					"problemText"=>$problemText,
					"workDuration"=>$workDuration, 
					"userId"=>$userId,
					"factoryId"=>$factoryId,
					"currentTime"=>date('Y-m-d H:i:s', $currentTime),
					);
		if($file_ext != '') {
			$insert["image"] = $userId ."_".$currentTime.".".$file_ext;
		} 
		$this->db->insert('helpReport',$insert);  
        return true; 
        
    } 
    
	function mNotificationExists($factoryId, $notificationId) {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT 1 FROM mNotificationv2 WHERE notificationId = $notificationId and status = '0' "; 
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->result();   
			return $result;
		} else {
			return false;
		}
	}

	function respondMNotification($factoryId, $notificationId, $respondTime, $status, $star) {    
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$updateField = array(
						"respondTime"=>date('Y-m-d H:i:s', $respondTime),  
						"status"=>$status,
						"star"=>$star,
						);	 
		$this->factory->where('notificationId',$notificationId);
		$this->factory->update('mNotificationv2',$updateField); 
		return $notificationId;
		
	}

	function isUserCheckedIn($factoryId, $userId) 
	{  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT activeId,startTime FROM machineUserv2 WHERE userId = $userId and isActive = '1' order by activeId desc";  
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();   
			return $result;
		} else {
			return false;
		}	
	}


	public function getMachinesOverview($factoryId, $isOperator, $userId)
	{  
       $this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$machineQ = '';
		if($isOperator == 1) {
			$this->db->select('machines');
			$this->db->from('user');
			$this->db->where('userId', $userId);
			$this->db->where('isDeleted', '0');
			$query= $this->db->get(); 
			$rowUser = $query->row();
			
			if($rowUser->machines != '') {
				$machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
			} else {
				$machineQ = " and 1 != 1 "; 
			}
		}
		
		$sql = "SELECT machine.*, betaLogImageColorOverview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag
			FROM machine left join betaLogImageColorOverview on betaLogImageColorOverview.machineId = machine.machineId 
			left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
			WHERE machine.isDeleted = '0' $machineQ  order by machine.machineId";  
		$query = $this->factory->query($sql); 
       return $query; 
    }

    public function getWorkingMachine($factoryId, $isOperator, $userId, $viewOnly)
    {  
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT activeId,startTime,workingMachine,isActive FROM machineUserv2 WHERE userId = $userId order by activeId desc";  
		$query = $this->factory->query($sql);
		$result = $query->row_array(); 
		if ($result['isActive'] == "1") 
		{
			$machineQ = " and machine.machineId in (".$result['workingMachine'].") "; 
		}elseif ($viewOnly == "1") 
		{
			$machineQ = ""; 
		}
		else
		{
			$this->db->select('machines');
			$this->db->from('user');
			$this->db->where('userId', $userId);
			$this->db->where('isDeleted', '0');
			$query= $this->db->get(); 
			$rowUser = $query->row();
			
			if($rowUser->machines != '') {
				$machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
			} else 
			{
				$machineQ = " and 1 != 1 "; 
			}
		}

		if($isOperator == 0) {
			$this->db->select('machines');
			$this->db->from('user');
			$this->db->where('userId', $userId);
			$this->db->where('isDeleted', '0');
			$query= $this->db->get(); 
			$rowUser = $query->row();
			
			if($rowUser->machines != '') {
				$machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
			} else {
				$machineQ = ""; 
			}
		}
		
		$sql = "SELECT machine.*, betaLogImageColorOverview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag
			FROM machine left join betaLogImageColorOverview on betaLogImageColorOverview.machineId = machine.machineId 
			left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
			WHERE machine.isDeleted = '0' $machineQ  order by machine.machineId";  
			//echo $sql;exit;
		$query = $this->factory->query($sql); 
        return $query; 
    }

   

	function checkSetApp($factoryId, $machineId) {  
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		return $this->factory
					->select('*')
					->where('machineId', $machineId)
					->order_by('appId', 'desc')
					->get('setAppLive')
					->row_array();  
	}

	function getAllColors() { 
		
		$sql = "SELECT * FROM color WHERE isDeleted = '0' order by orderId asc "; 
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->result();   
			return $result;
		} else {
			return false;
		}
	}
	public function getMachineStatusText($factoryId, $machineId, $redStatus, $greenStatus, $yellowStatus, $whiteStatus,$blueStatus){
       $this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$machineQ = '';
		if($machineId > 0 ) {
			$machineQ = "and machineId = $machineId"; 
		}
		$sql = "SELECT machineStateVal FROM `machineStateColorLookup` where redStatus = '$redStatus' and greenStatus = '$greenStatus' and yellowStatus = '$yellowStatus' and whiteStatus = '$whiteStatus' and blueStatus = '$blueStatus' $machineQ"; 
		
		$query = $this->factory->query($sql); 
		if($query->num_rows() >= 1) {
			$result = $query->row()->machineStateVal; 
		} else {
			$result = 'Off'; 
		}
        return $result; 
    }
    
	function getLastActiveMachine($factoryId,$machineId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;

    }
    public function getMachineStatusData($factoryId,$machineId,$choose1,$choose2,$machineStatus,$state)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
            $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('machineId',$machineId);
        }
        if ($choose1 == "Daily") 
        {
            $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        }else if ($choose1 == "Weekly") 
        {
        	$choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];
            $this->factory->where('week',$week);
            $this->factory->where('year',$year);
        }else if ($choose1 == "Monthly") {
            $choose2 = explode("/", $choose2);
            $month = $choose2[0];
            $year = $choose2[1];
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
        }else if ($choose1 == "Yearly") {
            $this->factory->where('year',$choose2);
        }
            $this->factory->where('machineStatus',$machineStatus);
            $this->factory->where('state',$state);
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

	function getBreakdownReasonList() { 
		
		$sql = "SELECT * from breakdownReason where isDeleted = '0' ";    
		
		$query = $this->db->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->result();
			return $result;
		} else {
			return 0;
		} 
	}

	function getNotificationsSingle($factoryId, $machineId, $userId, $notificationId) { 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);

		$notificationIdQ = '';
		if(!empty($notificationId)) {
			$notificationIdQ = "and notificationId = $notificationId"; 
		}
		
		$sql = "SELECT notification.*,machine.machineName from notification left join machine ON notification.machineId = machine.machineId  where notification.machineId = $machineId and notification.isDeleted = '0' and notification.status = '0' and notification.commentFlag = '1' and find_in_set($userId,userId) <> 0 $notificationIdQ order by notificationId desc limit 1 ";   
		
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->result();
			return $result;
		} else {
			return 0;
		} 
	}

	function getNotificationAddedDate($factoryId, $logId) { 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT originalTime from betaDetectionLog where logId = $logId";    
		
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result;
		} else {
			return false; 
		} 
	}

	function getNotificationChangeDate($factoryId, $machineId, $logId) { 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT originalTime from betaDetectionLog where machineId = $machineId and logId > $logId order by logId asc limit 1 ";    
		
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row();
			return $result;
		} else {
			return false; 
		} 
	}

	function notificationExists($factoryId, $notificationId) { 
		
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT machineId from notification where notificationId = $notificationId and status = '0'  ";     
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array(); 
			return $result;
		} else {
			return 0;
		} 
		
	}

	function respondNotification($factoryId, $notificationId, $respondTime, $reason, $responderId, $otherReason) {  
		
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		if ($reason == "Other") 
		{
			$result = $this->factory->where(array("notificationId" => $notificationId,"comment" => $reason,"otherReason" => $otherReason))->get('notification')->row();
		}else{
			$result = $this->factory->where(array("notificationId" => $notificationId,"comment" => $reason))->get('notification')->row();
		}
		if (empty($result)) 
		{
			$updateField = array(
				"respondTime"=>date('Y-m-d H:i:s'),  
				"comment"=>$reason,
				"responderId"=>$responderId, 
				"otherReason"=>$otherReason, 
				"status"=>'1'
				);  
		}else
		{
			$updateField = array(
				"comment"=>$reason,
				"responderId"=>$responderId, 
				"otherReason"=>$otherReason, 
				"status"=>'1'
				);  
		}


		$this->factory->where('notificationId',$notificationId);
		$this->factory->update('notification',$updateField); 

		$notificationLog = $this->getWhereDBSingle(array("notId" => $notificationId),"notificationLog");
		if (!empty($notificationLog)) 
		{
			$notificationLogId = $notificationLog->notificationId;

			$this->updateData(array("notificationId" => $notificationLogId),array("flag" => "1"),"notificationLog");
			$this->updateData(array("notificationId" => $notificationLogId),array("flag" => "1"),"notificationUserLog");
		}
		return $notificationId;
		
	}

	function updateNotificationLiveStatus($factoryId, $notificationId, $machineId, $flag) { 
		
		$addField = array(
						"notificationId"=>$notificationId,
						"flag"=>$flag, 
						"machineId"=>$machineId,
						"factoryId"=>$factoryId,
						); 
		$this->db->where('notificationId',$notificationId); 
		$this->db->update('notificationLiveStatus',$addField); 
		return $notificationId;
		
	}

	function getNotificationsCount($factoryId, $machineId, $userId) { 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$sql = "SELECT notificationId from notification where machineId = $machineId and find_in_set($userId,userId) <> 0  and isDeleted = '0' and status = '0' and commentFlag = '1' order by notificationId desc ";  
		$query = $this->factory->query($sql);
		return $query->num_rows() ;
	}

	function addReportImage($factoryId, $captureTime, $logTime, $fileExt, $machineId, $userId) {  
        
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$insert = array("imageName"=>$machineId."_".$userId ."_".$logTime.".".$fileExt, 
					"captureTime"=>date('Y-m-d H:i:s', $captureTime),
					"logTime"=>date('Y-m-d H:i:s', $logTime),
					"machineId"=>$machineId,
					"userId"=>$userId,
					);
		
		$this->factory->insert('reportedImages',$insert);  
		return $this->factory->insert_id(); 
    } 
    function machineExists($factoryId, $machineId) {  
		
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
			
		$sql = "SELECT * from machine where machineId = $machineId and isDeleted = '0' ";   
		$query = $this->factory->query($sql);
		if ($query->num_rows() > 0) {
			$result = $query->row_array();   
			return $result;
		} else {
			return false;
		}
		
	}


	public function getTaskMaintenace($factoryId,$userId,$userIds,$date,$status,$machineId)
    {

       $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
        $userIdsQ = '';
        if(!empty($userIds)) 
        {	
        	if ($userIds == "all") 
        	{
        		$userIdsQ .= "and FIND_IN_SET('$userId', taskMaintenace.userIds)";
        	}else
        	{
        		$NewUserIds = explode(",", $userIds);
        		foreach ($NewUserIds as $key => $value) {
        			$userIdsQ .= "and FIND_IN_SET('$value', taskMaintenace.userIds)";
        		}
        	}
        }

        $dateQ = '';
        if(!empty($date) && $date != "all") 
        {
        	if ($date == "today") 
        	{
        		$dateQ = "and date(taskMaintenace.dueDate) = '". date('Y-m-d') ."'";
        	}else if ($date == "yesterday") 
        	{
        		$dateQ = "and date(taskMaintenace.dueDate) = '". date('Y-m-d',strtotime("-1 day")) ."'";
        	}else if ($date == "week") 
        	{
        		$dateQ = "and WEEKOFYEAR(dueDate)=WEEKOFYEAR(NOW())";
        	}else if ($date == "month") 
        	{
        		$dateQ = "and MONTH(dueDate)= MONTH(NOW()) and YEAR(dueDate)= YEAR(NOW())";
        	}else if ($date == "year") 
        	{
        		$dateQ = "and YEAR(dueDate)= YEAR(NOW())";
        	}else
        	{
        		$date1 = date('Y-m-d',strtotime($date));
        		$dateQ = "and date(taskMaintenace.dueDate) = '". $date1 ."'";
        	}
        }

        $statusQ = '';
        if(!empty($status) && $status != "all") 
        {
            $statusQ = "and taskMaintenace.status = '$status'";
        }

        $machineIdQ = '';
        if(!empty($machineId) && $machineId != "all") 
        {
            $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
        }

        $select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId";
        $currentDate = date('Y-m-d');
        $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE date(dueDate) >= '$currentDate' and isDelete = '0' and machine.isDeleted = '0' $userIdsQ  $dateQ $statusQ  $machineIdQ  order by dueDate asc,repeatOrder asc";  
        $query = $this->factory->query($sql);
        return $query; 
    }
	
	public function getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,$type)
    {
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
        $compareDateCheck = $date;
        $compareWeekCheck = date('W',strtotime($date));
        $compareMonthCheck = date('m',strtotime($date));
        $compareYearCheck = date('Y',strtotime($date));

        $currentDate = date('Y-m-d');
        $dayCheck = date('l',strtotime($currentDate));

    	if ($dayCheck == "Saturday") 
		{
			$currentDate = date('Y-m-d',strtotime("+2 day"));
		}
		else if ($dayCheck == "Sunday") 
		{
			$currentDate = date('Y-m-d',strtotime("+1 day"));
		}

        $currentWeek = date('W',strtotime($currentDate));
        $currentMonth = date('m',strtotime($currentDate));
        $currentYear = date('Y',strtotime($currentDate));

        $userIdsQ = '';
        if(!empty($userIds)) 
        {	
        	if ($userIds == "all") 
        	{
        		$userIdsQ .= " and FIND_IN_SET('$userId', taskMaintenace.userIds)";
        	}else
        	{
        		$this->factory->group_start();
        		$NewUserIds = explode(",", $userIds);
        		$userIdsQ .= "and (";
        		foreach ($NewUserIds as $key => $value) {
        			$or = ($key == 0) ? "" : "or";
        			$userIdsQ .= " $or FIND_IN_SET('$value', taskMaintenace.userIds)";
        		}
        		$userIdsQ .= ")";
        		$this->factory->group_end();
        	}
        }

        $dateQ = '';
        $dateQuery = "";
        if($type == "never") 
        {
        	if ($currentDate >=  $compareDateCheck) 
        	{
        		$dateQ = "and date(taskMaintenace.dueDate) >= '". $date ."' and `repeat` = 'never'";
        		$dateQuery = "and date(dueDate) >= '$date'";
        	}else
        	{
        		$dateQ = "and date(taskMaintenace.dueDate) >= '". $date ."' and mainTaskId = 0 and `repeat` = 'never'";
        	}
        }else if($type == "everyDay") 
        {
        	if ($currentDate >=  $compareDateCheck) 
        	{
        		$dateQ = "and date(taskMaintenace.dueDate) = '". $date ."' and `repeat` = 'everyDay'";
        		$dateQuery = "and date(dueDate) >= '$date'";
        	}else
        	{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyDay'";
        		//$date = date('Y-m-d');
        		//$dateQuery = "and date(dueDate) >= '$date'";
        	}
        }
        else if($type == "everyWeek") 
        {
        	$day = date('l',strtotime($date));

        	if ($day == "Monday") 
			{
				$startDate = date('Y-m-d',strtotime($date));
			}else
			{
				$startDate = date('Y-m-d',strtotime($date." last monday"));
			}

			if ($day == "Friday") 
			{
				$endDate = date('Y-m-d',strtotime($date));
			}else
			{
				$endDate = date('Y-m-d',strtotime("next friday"));
			}

			if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
			{
        		$dateQ = "and date(taskMaintenace.dueDate) >= '$startDate' and date(taskMaintenace.dueDate) <= '$endDate' and `repeat` = 'everyWeek'";
        		$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyWeek'";
        		//$date = date('Y-m-d');
			}
        }
        else if($type == "everyMonth") 
        {
        	if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
			{
        		$dateQ = "and MONTH(taskMaintenace.dueDate) = '". date('m',strtotime($date)) ."' and `repeat` = 'everyMonth'";
        		$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyMonth'";
        		//$date = date('Y-m-d');
			}
        }
        else if ($type == "everyYear") 
    	{
    		if ($currentYear >=  $compareYearCheck) 
			{
    			$dateQ = "and YEAR(taskMaintenace.dueDate) = '". date('Y',strtotime($date)) ."' and `repeat` = 'everyYear'";
    			$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyYear'";
        		//$date = date('Y-m-d');
			}
    	}

        $statusQ = '';
        if(!empty($status) && $status != "all") 
        {
            $statusQ = "and taskMaintenace.status = '$status'";
        }

        $machineIdQ = '';
        if(!empty($machineId) && $machineId != "all") 
        {
            $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
        }

        $select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId";

        
         $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' and machine.isDeleted = '0' $dateQuery $userIdsQ  $dateQ $statusQ  $machineIdQ  order by dueDate asc,repeatOrder asc";  

         if($type == "everyWeek") 
         {
         	//echo $sql;exit;
         }
         $query = $this->factory->query($sql);
        return $query; 
    }

 public function getWhereDBSingle($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

    public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getWhereDBSelect($select, $where, $table)
    {   
        $this->db->select($select);
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getWhere($factoryId, $where, $table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereSelect($factoryId, $select, $where, $table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	$this->factory->select($select);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereSingle($factoryId, $where, $table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    public function getWhereJoinSingle($factoryId,$select ,$where, $join,$table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	$this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->row();
    }

   

    public function getWhereIn($factoryId,$select, $where, $whereIn, $table)
    {   
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	$this->factory->select($select);
    	$this->factory->where($where);
    	foreach ($whereIn as $key => $value) {
    		$this->factory->where_in($key,$value);
    	}
        $query = $this->factory->get($table);
        return $query->result_array();
    }


    function checkWorkingMachineStatus($factoryId,$userId) 
    { 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
		$today = date("Y-m-d");
		$sql = "SELECT * from machineUserv2 where userId = '$userId' order by activeId desc limit 1";    
		
		$query = $this->factory->query($sql);
		return $query->row_array();
	}

	public function getTaskData($factoryId,$userId,$date,$machineId,$type)
    {
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
      	$select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId,
        		   taskMaintenace.dueWeekDay,
        		   taskMaintenace.monthDueOn,
        		   taskMaintenace.dueMonthMonth,
        		   taskMaintenace.dueMonthWeek,
        		   taskMaintenace.dueMonthDay,
        		   taskMaintenace.yearDueOn,
        		   taskMaintenace.dueYearMonth,
        		   taskMaintenace.dueYearMonthDay,
        		   taskMaintenace.dueYearWeek,
        		   taskMaintenace.dueYearDay,
        		   taskMaintenace.dueYearMonthOnThe,
        		   taskMaintenace.endTaskDate,
        		   taskMaintenace.EndAfteroccurances,
        		   taskMaintenace.newTask
        		   ";

        $userIdsQ = " and FIND_IN_SET('$userId', taskMaintenace.userIds)";
	    $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
       

	    if ($type == "today") 
	    {

	    	if ($date > date('Y-m-d')) 
	        {
	        	$dateQ = "and mainTaskId = 0";
	        	$dateQuery = "";
	        }else
	        {
	    		$dateQ = "and date(taskMaintenace.dueDate) = '$date'";
	    		$dateQuery = "and date(dueDate) >= '$date'";
	        }
	    }else if ($type == "month") 
	    {
	    	$startDate = date('Y-m-d',strtotime($date));
			$endDate = date('Y-m-d',strtotime($date." +7 days"));
	    	$dateQ = "and date(taskMaintenace.dueDate) > '$startDate' and date(taskMaintenace.dueDate) < '$endDate' and `repeat` = 'everyMonth'";
	    	$dateQuery = "and date(dueDate) >= '$date'";
	    }else if ($type == "year") 
	    {
	    	$startDate = date('Y-m-d',strtotime($date));
			$endDate = date('Y-m-d',strtotime($date." +1 months"));
	    	$dateQ = "and date(taskMaintenace.dueDate) > '$startDate' and date(taskMaintenace.dueDate) < '$endDate' and `repeat` = 'everyYear'";
	    	$dateQuery = "and date(dueDate) >= '$date'";
	    }

        
        $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' and machine.isDeleted = '0' $dateQuery $userIdsQ  $dateQ $machineIdQ and ((endTaskDate >=  '$date') or (endTaskDate  IS NULL))  order by dueDate asc,repeatOrder asc";  
         
       // echo $sql;exit;
        $query = $this->factory->query($sql);
        return $query; 
    }

    public function getWorkingMachineWithPagination($factoryId, $isOperator, $userId, $viewOnly,$limit,$start)
    {  
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
		$sql = "SELECT activeId,startTime,workingMachine,isActive FROM machineUserv2 WHERE userId = $userId order by activeId desc";  
		$query = $this->factory->query($sql);
		$result = $query->row_array(); 
		if ($result['isActive'] == "1") 
		{
			$machineQ = " and machine.machineId in (".$result['workingMachine'].") "; 
		}elseif ($viewOnly == "1") 
		{
			$machineQ = ""; 
		}
		else
		{
			$this->db->select('machines');
			$this->db->from('user');
			$this->db->where('userId', $userId);
			$this->db->where('isDeleted', '0');
			$query= $this->db->get(); 
			$rowUser = $query->row();
			
			if($rowUser->machines != '') {
				$machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
			} else 
			{
				$machineQ = " and 1 != 1 "; 
			}
		}

		if($isOperator == 0) {
			$this->db->select('machines');
			$this->db->from('user');
			$this->db->where('userId', $userId);
			$this->db->where('isDeleted', '0');
			$query= $this->db->get(); 
			$rowUser = $query->row();
			
			if($rowUser->machines != '') {
				$machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
			} else {
				$machineQ = ""; 
			}
		}
		
		$sql = "SELECT machine.*, betaLogImageColorOverview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag
			FROM machine left join betaLogImageColorOverview on betaLogImageColorOverview.machineId = machine.machineId 
			left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
			WHERE machine.isDeleted = '0' $machineQ  order by machine.machineId limit $limit OFFSET $start";  
			//echo $sql;exit;
		$query = $this->factory->query($sql); 
        return $query; 
    }

    function getWarningFlagNotificationsStatus($factoryId, $machineId) 
	{ 
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);

		$dayDate = date('Y-m-d H:i:s',strtotime('-24 hour'));
		$sql = "SELECT * from notification where machineId = $machineId and isDeleted = '0' and commentFlag = '1'  and addedDate >  '$dayDate' and status = '0'";   
		$query = $this->factory->query($sql);
		$result = $query->num_rows();
		return $result;
	}

	public function getTaskMaintenaceListNew($factoryId,$userId,$userIds,$date,$status,$machineId,$type)
    {
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
        $compareDateCheck = $date;
        $compareWeekCheck = date('W',strtotime($date));
        $compareMonthCheck = date('m',strtotime($date));
        $compareYearCheck = date('Y',strtotime($date));

        $currentDate = date('Y-m-d');
        $dayCheck = date('l',strtotime($currentDate));

    	if ($dayCheck == "Saturday") 
		{
			$currentDate = date('Y-m-d',strtotime("+2 day"));
		}
		else if ($dayCheck == "Sunday") 
		{
			$currentDate = date('Y-m-d',strtotime("+1 day"));
		}

        $currentWeek = date('W',strtotime($currentDate));
        $currentMonth = date('m',strtotime($currentDate));
        $currentYear = date('Y',strtotime($currentDate));

        $userIdsQ = '';
        if(!empty($userIds)) 
        {	
        	if ($userIds == "all") 
        	{
        		$userIdsQ .= " and FIND_IN_SET('$userId', taskMaintenace.userIds)";
        	}else
        	{
        		$this->factory->group_start();
        		$NewUserIds = explode(",", $userIds);
        		$userIdsQ .= "and (";
        		foreach ($NewUserIds as $key => $value) {
        			$or = ($key == 0) ? "" : "or";
        			$userIdsQ .= " $or FIND_IN_SET('$value', taskMaintenace.userIds)";
        		}
        		$userIdsQ .= ")";
        		$this->factory->group_end();
        	}
        }

        $dateQ = '';
        $dateQuery = "";
        
        if($type == "everyWeek") 
        {
        	$day = date('l',strtotime($date));

        	if ($day == "Monday") 
			{
				$startDate = date('Y-m-d',strtotime($date));
			}else
			{
				$startDate = date('Y-m-d',strtotime($date." last monday"));
			}

			if ($day == "Sunday") 
			{
				$endDate = date('Y-m-d',strtotime($date));
			}else
			{
				$endDate = date('Y-m-d',strtotime("next sunday"));
			}

			if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
			{
        		$dateQ = "and date(taskMaintenace.dueDate) >= '$startDate' and date(taskMaintenace.dueDate) <= '$endDate' and `repeat` = 'everyWeek'";
        		$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyWeek'";
			}
        }
        else if($type == "everyMonth") 
        {
        	if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
			{
        		$dateQ = "and MONTH(taskMaintenace.dueDate) = '". date('m',strtotime($date)) ."' and date(taskMaintenace.dueDate) >= '$date' and `repeat` = 'everyMonth'";
        		$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyMonth'";
			}
        }
        else if ($type == "everyYear") 
    	{
    		if ($currentYear >=  $compareYearCheck) 
			{
    			$dateQ = "and YEAR(taskMaintenace.dueDate) = '". date('Y',strtotime($date)) ."' and date(taskMaintenace.dueDate) >= '$date' and `repeat` = 'everyYear'";
    			$dateQuery = "and date(dueDate) >= '$date'";
			}else
			{
        		$dateQ = "and mainTaskId = 0 and `repeat` = 'everyYear'";
			}
    	}

        $statusQ = '';
        if(!empty($status) && $status != "all") 
        {
            $statusQ = "and taskMaintenace.status = '$status'";
        }

        $machineIdQ = '';
        if(!empty($machineId) && $machineId != "all") 
        {
            $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
        }

       	$select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId,
        		   taskMaintenace.dueWeekDay,
        		   taskMaintenace.monthDueOn,
        		   taskMaintenace.dueMonthMonth,
        		   taskMaintenace.dueMonthWeek,
        		   taskMaintenace.dueMonthDay,
        		   taskMaintenace.yearDueOn,
        		   taskMaintenace.dueYearMonth,
        		   taskMaintenace.dueYearMonthDay,
        		   taskMaintenace.dueYearWeek,
        		   taskMaintenace.dueYearDay,
        		   taskMaintenace.dueYearMonthOnThe,
        		   taskMaintenace.endTaskDate,
        		   taskMaintenace.EndAfteroccurances,
        		   taskMaintenace.newTask
        		   ";

        
         $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' and machine.isDeleted = '0' $dateQuery $userIdsQ  $dateQ $statusQ  $machineIdQ  and ((endTaskDate >=  '$date') or (endTaskDate  IS NULL)) order by dueDate asc,repeatOrder asc";  

         $query = $this->factory->query($sql);
        return $query; 
    }


    function getLastSubTask($taskId)
    {
        $this->factory->where('mainTaskId',$taskId);
        $this->factory->order_by('taskId','desc');
        $query = $this->factory->get('taskMaintenace')->row();
        return $query;
    }

}