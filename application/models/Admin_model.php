<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Admin_model extends CI_Model
{ 
    //In use
    public function checkIsvalidated()
    {
        if($this->session->userdata('validated') && $this->session->userdata('url3') == 'version4' && $this->session->userdata('factoryId') != '') {   
            return true;
        } else {
            return false;
        }
    }

    function getLanguage($languageType)
    {       
        //$result = $this->AM->getWhereDB(array("type" => 'dashboard'),"translations");
        $result = $this->db->where('find_in_set("dashboard", type) <> 0')->get('translations')->result_array();
        
        foreach ($result as $key => $value) 
        {
            define($value['textIdentify'], ($languageType == "1") ? $value['englishText'] : $value['swedishText']);
        }
    }

    function sendEmail($message,$subject,$url,$fileExt)
    {
        include APPPATH . 'third_party/sendgrid-php/sendgrid-php.php';

        $email = new \SendGrid\Mail\Mail();
        $email->setFrom("root@nyttdev.com", "NYTT ADMIN");
        $email->setSubject($subject.' '.emailSubject);
        $email->addTo('info@nytt-tech.com');//info@nytt-tech.com
        $email->addBcc( "support@insightech.ai");//support@insightech.ai
        //$email->addBcc( "support@insightech.ai");
        $email->addContent("text/plain", "subject");
        $email->addContent(
            "text/html",$message);

        if (!empty($url)) 
        {   
            $filename = basename( $url  );
            $file_encoded = base64_encode(file_get_contents($url));
            $attachment = new SendGrid\Mail\Attachment();
            $attachment->setType('application/'.$fileExt);
            $attachment->setContent($file_encoded);
            $attachment->setDisposition("attachment");
            $attachment->setFilename($filename);
            $email->addAttachment($attachment);
        }
        $sendgrid = new \SendGrid(('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'));
        try {
            $response = $sendgrid->send($email);
            $headers = $response->headers();

            if (!empty($url)) 
            {
                $headers = explode(":", $headers[7]);
            }else
            {
                $headers = explode(":", $headers[5]);
            }
            $MessageId = trim($headers[1]);

            $data = array("MessageId" => $MessageId,"insertTime" => date('Y-m-d H:i:s'),"message" => $message,"factoryId" => $this->factoryId);
            $this->AM->insertDBData($data,"emailStatus");
        } catch (Exception $e) {
            echo 'Caught exception: '. $e->getMessage() ."\n";
        }
    }


     function accessPoint($responsibilitiesId)
    {
        /*$rolesType = $this->session->userdata('role');
        $rolesData = $this->AM->getWhereDBSingle(array("rolesType" => $rolesType), "roles");
        $rolesId = $rolesData->rolesId;
        $rolesResponsibilities = $this->AM->getWhereDB(array("rolesId" => $rolesId,"isView" => "1"), "rolesResponsibilities");
        $rolesIsViews = array_column($rolesResponsibilities, "responsibilitiesId");
        if (in_array($responsibilitiesId, $rolesIsViews))
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return true;
    }

    function accessReportPoint($responsibilitiesId,$rolesType)
    {
        /*$rolesData = $this->AM->getWhereDBSingle(array("rolesType" => $rolesType), "roles");
        $rolesId = $rolesData->rolesId;
        $rolesResponsibilities = $this->AM->getWhereDB(array("rolesId" => $rolesId,"isView" => "1"), "rolesResponsibilities");
        //echo $this->db->last_query();exit;
        $rolesIsViews = array_column($rolesResponsibilities, "responsibilitiesId");
        if (in_array($responsibilitiesId, $rolesIsViews))
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return true;
    }

    function accessReportPointEdit($responsibilitiesId,$rolesType)
    {
        /*$rolesData = $this->AM->getWhereDBSingle(array("rolesType" => $rolesType), "roles");
        $rolesId = $rolesData->rolesId;
        $rolesResponsibilities = $this->AM->getWhereDB(array("rolesId" => $rolesId,"isEdit" => "1"), "rolesResponsibilities");
        //echo $this->db->last_query();exit;
        $rolesIsViews = array_column($rolesResponsibilities, "responsibilitiesId");
        if (in_array($responsibilitiesId, $rolesIsViews))
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return true;
    }

    function accessPointExecute($responsibilitiesId)
    {
       /* $rolesType = $this->session->userdata('role');
        $rolesData = $this->AM->getWhereDBSingle(array("rolesType" => $rolesType), "roles");
        $rolesId = $rolesData->rolesId;
        $rolesResponsibilities = $this->AM->getWhereDB(array("rolesId" => $rolesId,"isView" => "1"), "rolesResponsibilities");
        $rolesIsViews = array_column($rolesResponsibilities, "responsibilitiesId");
        if (in_array($responsibilitiesId, $rolesIsViews))
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return true;
    }


    function accessPointEdit($responsibilitiesId)
    {
        /*$rolesType = $this->session->userdata('role');
        $rolesData = $this->AM->getWhereDBSingle(array("rolesType" => $rolesType), "roles");
        $rolesId = $rolesData->rolesId;
        $rolesResponsibilities = $this->AM->getWhereDB(array("rolesId" => $rolesId,"isEdit" => "1"), "rolesResponsibilities");
        $rolesIsViews = array_column($rolesResponsibilities, "responsibilitiesId");
        if (in_array($responsibilitiesId, $rolesIsViews))
        {
            return true;
        }
        else
        {
            return false;
        }*/

        return true;
    }

     function getMachineRunnignWaitingTime($machineId,$insertTime)
    { 
        $tableName = $machineId."machineRunnignWaitingTime";
        if($this->factory->table_exists($tableName) )
        {
            $this->factory->select("sum(timeDiff) as timeDiff");
            $this->factory->where('originalTime >=',$insertTime);
            $query = $this->factory->get($tableName);
            //echo $this->factory->last_query();exit;
            return $query->row();
        }else
        {
            return "";  
        }
    } 

    function getMachineRunnignWaitingTimeWithEndTime($machineId,$insertTime,$endTime)
    { 
        $tableName = $machineId."machineRunnignWaitingTime";
        if($this->factory->table_exists($tableName) )
        {
            $this->factory->select("sum(timeDiff) as timeDiff");
            $this->factory->where('originalTime >=',$insertTime);
            $this->factory->where('originalTime <=',$endTime);
            $query = $this->factory->get($tableName);
            //echo $this->factory->last_query();exit;
            return $query->row();
        }else
        {
            return "";  
        }
    }

    function getLastProductionTimeWithFinishOrder($machinePartsId,$insertTime)
    {
        $this->factory->where('machinePartsId',$machinePartsId);
        $this->factory->where('insertTime >=',$insertTime);
        $this->factory->where('machinePartsStatus',"0");
        $this->factory->order_by('machinePartsLogId','asc');
        $query = $this->factory->get('machinePartsLog')->row();

        return $query;
    }

    function getMachineSetupTime($machineId,$startTime,$endTime)
    { 
        $tableName = $machineId."setupTable";
        if($this->factory->table_exists($tableName) )
        {
            $this->factory->select("sum(timeDiff) as timeDiff");
            $this->factory->where('originalTime >=',$startTime);
            if (!empty($endTime)) 
            {
                $this->factory->where('originalTime <=',$endTime);
            }
            $query = $this->factory->get($tableName);
            //echo $this->factory->last_query();exit;
            return $query->row();
        }else
        {
            return "";  
        }
    }

    function getLastProductionTime($machinePartsId,$startDate,$endDate)
    {
        $this->factory->where('machinePartsId',$machinePartsId);
        $this->factory->where('insertTime >',$startDate);
        $this->factory->where('insertTime <=',$endDate);
//this->factory->where('machinePartsStatus','0');
        $this->factory->order_by('insertTime','asc');
        $query = $this->factory->get('machinePartsLog')->row();

        //echo $this->factory->last_query();exit;

        return $query;
    }

    function getLastProductionTimeFinalOrder($machinePartsId,$startDate)
    {
        $this->factory->where('machinePartsId',$machinePartsId);
        $this->factory->where('insertTime <',$startDate);
        $this->factory->where('machinePartsStatus','0');
        $this->factory->order_by('insertTime','asc');
        $query = $this->factory->get('machinePartsLog')->row();

        //echo $this->factory->last_query();exit;

        return $query;
    }

    //In use
    public function getallFactories()
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    function getAllReportedUsers($factory)
    {
        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId  group by reportProblem.userId";  
        $query = $factory->query($sql)->result_array(); 
        return $query; 
    } 
    
    //In use
    function getUserPassword($userName) 
    {
        if($userName != '' ) 
        {
            $sql = "SELECT userPassword from user where userName = '".$userName."' and isDeleted  = '0'";    
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) 
            {
                $users = $query->row_array();   
                return base64_decode($users['userPassword']);
            } 
            else 
            {
                return false;
            }
            
        } 
        else 
        {
            return false;
        }
    }

    //In use
    public function getWhereDBSingle($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->row();
    }

    //In use
    public function validate()
    {
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean(base64_encode($this->input->post('password')));
        $this->db->join("factory","user.factoryId = factory.factoryId");
        $this->db->where("userName like binary '$username'",NULL, FALSE);
        $this->db->where("userPassword like binary '$password'",NULL, FALSE);
        $this->db->where('user.isDeleted', '0'); 
        $this->db->where('factory.isDeleted', '0'); 
        $query = $this->db->get('user');
        if($query->num_rows() == 1) 
        {
            $row = $query->row();
            $data = array(
                    'userId' => $row->userId,
                    'userName' => $row->userName,
                    'userImage' => $row->userImage,
                    'role' => $row->userRole,
                    'validated' => true,
                    'url3'=>'version4',
                    'userTheme'=> $row->theme, 
                    'site_lang'=> $row->languageType, 
                    );
            $factory = $this->getWhereDBSingle(array("factoryId" => $row->factoryId),"factory");
            $data['isMonitor'] = $factory->isMonitor; 
            if($row->userRole != 2 ) 
            {
                $data['factoryId'] = $row->factoryId; 
            }
            $this->session->set_userdata($data);
            if($row->userRole == 2 ) 
            {
                redirect('admin/selectFactory');
            } 
            return $row;
        }
        return false;
    }
    
    //In use
    public function doLogout()
    {
        $this->session->sess_destroy();
        redirect('admin/login'); 
    }

    //In use
    public function checkUserRole()
    {
        return (int)$this->session->userdata('role'); 
    }

    //In use
    public function getMachinesOverview($factory, $isOperator, $userId, $factoryId)
    {
        $machineQ = '';
        if($isOperator == 1) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $userId);
            $this->db->where('isDeleted', '0');
            $query= $this->db->get(); 
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
            } else {
                $machineQ = " and 1 != 1 "; 
            }
        }
        
        $sql = "SELECT machine.*, betaLogImageColorOverview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag, machineCode.machineCode, machine.machineId
            FROM machine 
            join r_nytt_main.machineCode on machineCode.machineId = machine.machineId
            left join betaLogImageColorOverview on betaLogImageColorOverview.machineId = machine.machineId 
            left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
            WHERE machine.isDeleted = '0' AND  r_nytt_main.machineCode.factoryId = $factoryId $machineQ  order by machine.machineId";  
        $query = $factory->query($sql);   
        return $query; 
    }

    //In use
    public function getallColors()
    {
        $this->db->select('*');
        $this->db->from('color');
        $this->db->order_by('orderId','asc'); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    //In use
    public function getPhoneDetail($factory, $machineId)
    {
        $factory->select('phoneId');
        $factory->from('phoneDetailLogv2');
        $factory->where('machineId', $machineId);
        $factory->order_by('phoneId', 'desc');
        $query= $factory->get();
        $result = $query->row();
        if(!isset($result)) 
        {
            $result = new stdClass;
            $result->phoneId = 0;
        }
        return $result; 
    }

    //In use
    public function getVisulizationData($factory, $machineId, $type, $state, $choose1='', $choose2='', $choose3='', $choose4='') 
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and analyticsGraphData.machineId = $machineId";  
        }
        $yearQ = '';
        if($type == 'year' ) 
        {
            $yearQ = "and analyticsGraphData.year = $choose1";  
            $period_value = 'month';
        }
        $monthQ = '';
        if($type == 'month' ) 
        {
            $monthQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2";  
            $period_value = 'day';  
        }
        $weekQ = '';
        if($type == 'week' ) 
        {
            $weekQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.week = $choose3 and analyticsGraphData.week != 0  "; 
            //and analyticsGraphData.weekday != 0 
            $period_value = 'weekday'; 
        }
        $dayQ = '';
        if($type == 'day' ) 
        {
            $dayQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2 and analyticsGraphData.day = $choose4"; 
            $period_value = 'hour'; 
        }
        $sql = "SELECT sum(duration) as countVal, $period_value FROM analyticsGraphData left join machine on analyticsGraphData.machineId = machine.machineId WHERE machine.isDeleted = '0' and  analyticsGraphData.machineStatus = '0' $machineQ and state='".$state."' $yearQ $monthQ $weekQ $dayQ group by $period_value order by $period_value asc"; 
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    function checkSetApp($factoryId, $machineId) 
    {
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $sql =  $this->factory
                    ->select('*')
                    ->where('machineId', intval($machineId))
                    ->order_by('appId', 'desc')
                    ->get('setAppLive'); 
        return $sql->row_array(); 
    }

    //In use
    public function getMachineStatusText($factory, $machineId, $redStatus, $greenStatus, $yellowStatus, $whiteStatus, $blueStatus)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and machineId = $machineId"; 
        }
        $sql = "SELECT machineStateVal FROM `machineStateColorLookup` where redStatus = '$redStatus' and greenStatus = '$greenStatus' and yellowStatus = '$yellowStatus' and whiteStatus = '$whiteStatus' and blueStatus = '$blueStatus' $machineQ"; 
        $query = $factory->query($sql); 
        if($query->num_rows() >= 1) 
        {
            $result = $query->row()->machineStateVal; 
        } 
        else 
        {
            $result = 'Off';
        }
        return $result; 
    }

    //In use
    public function checkUserRoleName()
    {
        if($this->session->userdata('role') == '0') 
        {
            $roleName = 'Operator';
        } 
        else if($this->session->userdata('role') == '1') 
        {
            $roleName = 'Factory Manager';
        } 
        else if($this->session->userdata('role') == '2') 
        {
            $roleName = 'Administrator';
        } 
        else if($this->session->userdata('role') == '3') 
        {
            $roleName = 'Developer';
        }
        return $roleName;
    }
    
    //In use
    public function getFactoryData($factoryId)
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $this->db->where('factoryId', $factoryId);
        $query= $this->db->get();        
        return $query->row(); 
    }

    //In use
    public function newMachine($factory, $insert)
    { 
        $factory->insert('machine',$insert);
        return $factory->insert_id();
    }

    //In use
    public function addMachineMaintananceSteps($factoryId, $machineId, $mTemplateId) 
    { 
        $sql = "INSERT mSteps (factoryId, machineId, stepText, categoryId, stepOrder ) SELECT ".$factoryId.",".$machineId.", mTCText, mTCCategory, mTCOrder FROM maintenanceTemplateContent WHERE maintenanceTemplateContent.mTemplateId = 1 and maintenanceTemplateContent.isDeleted = '0' ";    
        $queryLog = $this->db->query($sql);  
        return $queryLog; 
    }

    //In use
    public function insertDBData($data, $table)
    {   
        $this->db->insert($table,$data);
        return $this->db->insert_id();
    }

    //In use
    public function newMachineColorState($factory, $insert)
    { 
        $factory->insert('machineStateColorLookup',$insert);
        return $factory->insert_id();
    }

    //In use
    public function getWhere($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereWithOrderBy($where,$orderKey,$orderBy, $table)
    {   
        $this->factory->where($where);
        $this->factory->order_by($orderKey,$orderBy);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereWithLike($where,$likeColumn,$searchText, $table)
    {   
        $this->factory->where($where);
        if (!empty($searchText)) 
        {
            $this->factory->like($likeColumn, $searchText);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereWithOrWithLike($where,$whereOr,$like, $table)
    {   
        $this->factory->where($where);
        if (!empty($whereOr)) 
        {
            $this->factory->group_start();
            foreach ($whereOr as $key => $value) 
            { 
                $this->factory->or_where($key,$value);
            }
            $this->factory->group_end();
        }
        if (!empty($like)) 
        {
            foreach ($like as $keyL => $valueL) 
            { 
                $this->factory->like($keyL, $valueL);
            }
        }
        $query = $this->factory->get($table);
        //echo $this->factory->last_query();exit;
        return $query->result_array();
    }

    public function getWhereSingleWithOrderBy($where,$orderKey,$orderBy, $table)
    {   
        $this->factory->where($where);
        $this->factory->order_by($orderKey,$orderBy);
        $query = $this->factory->get($table);
        return $query->row();
    }

    //In use
    public function getWhereCount($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->num_rows();
    }

    //In use
    public function insertData($data, $table)
    {   
        $this->factory->insert($table,$data);
        return $this->factory->insert_id();
    }

    //In use
    public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    //In use
    public function updateMachineColorState($factory, $update, $aclrId)
    { 
        $factory->where('aclrId',$aclrId);
        $factory->update('machineStateColorLookup',$update);
    } 

    //In use
    public function updateMachine($factory, $update, $machineId)
    { 
        $factory->where('machineId',$machineId);
        $factory->update('machine',$update);
    } 


    //In use
    public function updateData($where,$data, $table)
    {   
        $this->factory->where($where);
        $this->factory->update($table,$data);
    }

    //In use
    public function deleteMachine($factory, $machineId)
    {
        $factory->where('machineId',$machineId);
        $update = array('isDeleted'=>'1');
        $factory->update('machine',$update);
    } 

    //In use
    public function unlockMachine($factory, $machineId)
    {
        $factory->where('machineId',$machineId);
        $update = array('machineSelected'=>'0');
        $factory->update('machine',$update); 
    }

    //In use
    function getLastActiveMachine($machineId)
    {
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;
    }

    //In use
    function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId,
                'setAppStartType' => "dashboard",
                'userId' => $this->session->userdata('userId')
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,
                'data' => $data
            );
        $headers = array
            (
                'Authorization: key='.SETAPP_SERVER_KEY,
                'Content-Type: application/json'
            );
            $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //In use
    public function machineExists($factory, $name, $machineId=0)
    { 
        $factory->select('*');
        $factory->from('machine');
        $factory->where('machineName', $name);
        if($machineId != 0) 
        {
            $factory->where('machineId != ', $machineId); 
        }
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        $row = $query->row();
        return $row;  
    }

    //In use
    public function userExists($userName, $userId=0)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userName', $userName);
        if($userId != 0) 
        {
            $this->db->where('userId != ', $userId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    //In use
    public function stackLightTypeExists($stackLightTypeName, $stackLightTypeId=0)
    { 
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('stackLightTypeName', $stackLightTypeName);
        if($stackLightTypeId != 0) 
        { 
            $this->db->where('stackLightTypeId != ', $stackLightTypeId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }

    //In use
    public function getallMachines($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        return $query;
    }  

   public function getalldeviceName($factory)
    {
        $factory->select('*');
        $factory->from('phoneDetailLogv2');
        $factory->group_by('model');
        // $factory->where('isDeleted', '0');
        $query= $factory->get();
        return $query;
    }
  
    //In use
    public function getallMachinesNoStacklight($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $factory->where('noStacklight', '0');
        $query= $factory->get();
        return $query;
    }

    //In use
    public function getAssignedMachines($factory, $userId)
    {
        $this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        $rowUser = $query->row();
        if($rowUser->machines != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId in (".$rowUser->machines.") AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine WHERE 1 != 1"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    public function getAssignedMachinesNoStacklight($factory, $userId)
    {
        $this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $rowUser = $query->row();
        if($rowUser->machines != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId in (".$rowUser->machines.") AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    public function getMachineStatusData($machineId,$choose1,$choose2,$machineStatus,$state,$viewStartTime = "",$viewEndTime = "",$breakViewHour = array())
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <',$viewEndTime);
        }

        if ($choose1 == "day") 
        {
            
            $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];
            $this->factory->where('week',$week);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = $choose2[0];
            if ($month == "Jan") 
            {
                $month = "1";
            }
            elseif ($month == "Feb") 
            {
                $month = "2";
            }
            elseif ($month == "Mar") 
            {
                $month = "3";
            }
            elseif ($month == "Apr") 
            {
                $month = "4";
            }
            elseif ($month == "May") 
            {
                $month = "5";
            }
            elseif ($month == "Jun") 
            {
                $month = "6";
            }
            elseif ($month == "Jul") 
            {
                $month = "7";
            }
            elseif ($month == "Aug") 
            {
                $month = "8";
            }
            elseif ($month == "Sep") 
            {
                $month = "9";
            }
            elseif ($month == "Oct") 
            {
                $month = "10";
            }
            elseif ($month == "Nov") 
            {
                $month = "11";
            }
            elseif ($month == "Dec") 
            {
                $month = "12";
            }

            $year = $choose2[1];
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('year',$choose2);
        }
            if ($state == "noStacklight") 
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"1");
                $this->factory->where('state','running');
            }else
            {
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
                $this->factory->where('state',$state);
            }
            $this->factory->where('analyticsGraphData.machineStatus',$machineStatus);

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }

            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    public function getMachinePartsData($machineId,$choose1,$choose2,$viewStartTime,$viewEndTime,$breakViewHour,$isDataGet)
    {
        if ($isDataGet == "1") 
        {
            $tableName = $machineId.'ioParts';
        }else
        {
            $tableName = $machineId.'partsLogAnalytics';
        }
        
        if($this->factory->table_exists($tableName) )
        { 
            $this->factory->select("count(dayThrouputMax) as dayThrouputMaxCount,sum(dayThrouputMax) as dayThrouputMax,sum(dayProductionTime) as dayProductionTime,sum(dayCycleMax) as dayCycleMax,sum(dayThrouputActual) as dayThrouputActual,sum(dayProductivity) as dayProductivity,avg(dayProductivity) as dayProductivityIo,sum(dayRunningTime) as dayRunningTime,sum(dayCycleActual) as dayCycleActual,sum(dayPartsProduce) as dayPartsProduce,avg(dayThrouputActual) as dayThrouputActualText,avg(dayThrouputMax) as dayThrouputMaxText");
            if ($choose1 == "day") 
            {   
                $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
            }
            else if ($choose1 == "weekly") 
            {
               /* $choose2 = explode("/", $choose2);
                $week = $choose2[0];
                $year = $choose2[1];

                $this->factory->where('WEEK(date)',$week);
                $this->factory->where('YEAR(date)',$year);*/

                $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $dto = new DateTime();
            $dto->setISODate($year, $week);
            $startDate = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $endDate = $dto->format('Y-m-d');

            $this->factory->where('date(date) >=',$startDate);
            $this->factory->where('date(date) <=',$endDate);
            }
            else if ($choose1 == "monthly") 
            {
                $choose2 = explode(" ", $choose2);
                $month = $choose2[0];
                $month = date('m',strtotime($choose2[0]));
                $year = $choose2[1];

                $this->factory->where('MONTH(date)',$month);
                $this->factory->where('YEAR(date)',$year);
            }
            else if ($choose1 == "yearly") 
            {
                $this->factory->where('YEAR(date)',$choose2);
            }

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('hour >=',$viewStartTime);
                $this->factory->where('hour <',$viewEndTime);
            }

            if(!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('hour', $breakViewHour);
            }
               

            $query = $this->factory->get($tableName);
           // echo $this->factory->last_query();exit;
            return $query->row();
        }else
        {
            return array();
        }
    }

    public function getOpportunityAndParts($machineId,$dateData,$viewStartTime,$viewEndTime,$breakViewHour,$isDataGet)
    {
        if ($isDataGet == "1") 
        {
            $tableName = $machineId.'ioParts';
        }else
        {
            $tableName = $machineId.'partsLogAnalytics';
        }
        
        if($this->factory->table_exists($tableName) )
        { 
            $this->factory->select("sum(dayOpprtunityGap) as dayOpprtunityGap,sum(dayPartsProduce) as dayPartsProduce");
            $this->factory->where('date(date)',date('Y-m-d',strtotime($dateData)));

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('hour >=',$viewStartTime);
                $this->factory->where('hour <',$viewEndTime);
            }

            if(!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('hour', $breakViewHour);
            }
               

            $query = $this->factory->get($tableName);
           // echo $this->factory->last_query();exit;
            return $query->row();
        }else
        {
            return array();
        }
    }

    public function getMachinePartsFinishData($machineId,$choose1,$choose2)
    {
        $this->factory->select("sum(scrapParts) as scrapParts");
        if ($choose1 == "day") 
        {   
            $this->factory->where('date(insertTime)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $this->factory->where('WEEK(insertTime)',$week);
            $this->factory->where('YEAR(insertTime)',$year);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = $choose2[0];
            $month = date('m',strtotime($choose2[0]));
            $year = $choose2[1];

            $this->factory->where('MONTH(insertTime)',$month);
            $this->factory->where('YEAR(insertTime)',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('YEAR(insertTime)',$choose2);
        }
           
        $this->factory->where('machineId',$machineId);
        $query = $this->factory->get('machinePartsFinish');
        return $query->row();
       
    }

    public function getAnalyticsMachineCount()
    {
        $this->factory->group_by('machineId');
        $query = $this->factory->get('analyticsGraphData');
        return $query->num_rows();
    }

    //In use
    public function getHourData($machineId,$hour,$choose2,$state,$viewStartTime = "",$viewEndTime = "")
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <=',$viewEndTime);
        }

        $this->factory->where('hour',$hour);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getHourDataActualProducation($machineId,$hour,$choose2,$state,$viewStartTime = "",$viewEndTime = "")
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('hour >=',$viewStartTime);
            $this->factory->where('hour <=',$viewEndTime);
        }

        $this->factory->where('hour',$hour);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekData($machineId,$weekDate,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }


        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getWeekDataActualProducation($machineId,$weekDate,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }

        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getMonthData($machineId,$day,$month,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            if ($state == "setup") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"1");
            }
            elseif ($state == "no_producation") 
            {
                $this->factory->where('analyticsGraphData.machineStatus',"2");
            }
            else
            {
                $this->factory->where('analyticsGraphData.machineStatus',"0");
                $this->factory->where('state',$state);
                $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
            }

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
                $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
            }

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }
            $query = $this->factory->get('analyticsGraphData');
            //echo $this->factory->last_query();exit;
            return $query->row();
    }

    //In use
    public function getMonthDataActualProducation($machineId,$day,$month,$year,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"1");

            if (!empty($viewStartTime) || !empty($viewEndTime)) 
            {
                $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
                $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
            }

            if (!empty($breakViewHour)) 
            {
                $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
            }
            $query = $this->factory->get('analyticsGraphData');
            return $query->row();
    }

    //In use
    public function getYearData($machineId,$month,$choose2,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        if ($state == "setup") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analyticsGraphData.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analyticsGraphData.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }
        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getYearDataActualProducation($machineId,$month,$choose2,$state,$breakViewHour = array(),$viewStartTime,$viewEndTime)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analyticsGraphData.machineId',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        $this->factory->where('analyticsGraphData.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analyticsGraphData.machineId = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");

        if (!empty($viewStartTime) || !empty($viewEndTime)) 
        {
            $this->factory->where('analyticsGraphData.hour >=',$viewStartTime);
            $this->factory->where('analyticsGraphData.hour <',$viewEndTime);
        }

        if (!empty($breakViewHour)) 
        {
            $this->factory->where_not_in('analyticsGraphData.hour', $breakViewHour);
        }
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getallReasons()
    {
        $this->db->select('*');
        $this->db->from('breakdownReason');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    } 

    //In use
    public function getStopAnalyticsData($factory, $machineId, $choose1, $choose2, $choose4, $analytics) 
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
        }
        $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if($factory->table_exists($tableName) )
        { 
            $date = $choose1."-".$choose2Pad."-".$choose4;
            $sql = "SELECT $tableName.timeDiff as colorDuration, TIME_TO_SEC($tableName.originalTime) as timeS, $tableName.userId, notification.status, notification.comment, notification.commentFlag FROM $tableName left join notification on notification.logId = $tableName.logId WHERE $tableName.originalTime like '%".$date."%' order by $tableName.originalTime asc";
            $query = $factory->query($sql);
            return $query;  
        } 
        else 
        {
            return '';
        }
    } 
    
    //In use
    public function getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration, $maxDuration, $machineId=0, $analytics)
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
            $choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
            $date = $choose1."-".$choose2Pad."-".$choose4Pad;
            $this->factory->select('notification.notificationId, notification.comment, stopT.timeDiff' );  
            $this->factory->from('notification');
            $this->factory->where('notification.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('notification.machineId', $machineId); 
            }
            $this->factory->join($tableName.' as stopT','stopT.logId = notification.logId');   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('notification.type', $type); 
            $this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); 
            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else 
        {
            return '';
        }
    }


    public function getIssueReasonAnalyticsDataOther($choose1, $choose2, $choose4, $minDuration, $maxDuration, $machineId=0, $analytics,$breakdownReasonArr)
    {
        $choose2 = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState'.$choose2.$choose1;
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState'.$choose2.$choose1;
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
            $choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
            $date = $choose1."-".$choose2Pad."-".$choose4Pad;
            $this->factory->select('notification.notificationId, notification.comment, stopT.timeDiff' );  
            $this->factory->from('notification');
            $this->factory->where('notification.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('notification.machineId', $machineId); 

            }
            $this->factory->where_not_in('notification.comment', $breakdownReasonArr);
            $this->factory->where('notification.comment !=', ""); 
            $this->factory->join($tableName.' as stopT','stopT.logId = notification.logId');   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('notification.type', $type); 
            $this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); 
            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else 
        {
            return '';
        }
    }

    //In use
    public function isValidCurrentPassword($pass, $userId)
    { 
        $this->db->select('userId');
        $this->db->from('user');
        $this->db->where('userPassword', base64_encode($pass));
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    
    //In use
    public function getAllStackLightTypes($factoryId)
    {
        $this->db->select('stackLightType.*, GROUP_CONCAT(machine.machineId SEPARATOR ",") as machines');
        $this->db->from('stackLightType');
        $this->db->join('r_nytt_factory'.$factoryId.'.machine','stackLightType.stackLightTypeId = machine.stackLightTypeId','left'); 
        $this->db->where('stackLightType.isDeleted', '0');
        $this->db->group_by('stackLightType.stackLightTypeId'); 
        $query= $this->db->get();
        return $query; 
    }

    //In use
    function lastStackLightTypeId() 
    {
       $this->db->select('max(stackLightTypeId) as maxId'); 
       $this->db->from('stackLightType'); 
       $va=$this->db->get()->row();
       return $va; 
    } 

    //In use
    public function newStackLightType($insert)
    { 
        $this->db->insert('stackLightType',$insert);
        return $this->db->insert_id();
    } 

    //In use
    public function updateStackLightType($update, $stackLightTypeId)
    {
       $this->db->where('stackLightTypeId',$stackLightTypeId);
        $this->db->update('stackLightType',$update); 
    }

    //In use
    public function deleteStackLightType($stackLightTypeId)
    { 
        $this->db->where('stackLightTypeId',$stackLightTypeId);
        $update = array('isDeleted'=>'1');
        $this->db->update('stackLightType',$update); 
    }

    //In use
    public function getAllAppVersionCount($factory)
    {
        $sql = "SELECT count(*) as totalCount FROM appVersions";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getAppVersionCount($factory, $searchQuery)
    { 
        $sql = "SELECT count(*) as totalCount FROM appVersions WHERE 1=1  $searchQuery ";  
        $query = $this->db->query($sql); 
        return $query; 
    } 

    //In use
    public function getallAppVersion($factory, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $sql = "SELECT * FROM appVersions WHERE 1=1  $searchQuery order by app_version_code desc,$columnIndex $columnSortOrder limit $start, $limit ";  
        $query = $this->db->query($sql); 
        return $query; 
    }

    //In use
    public function getWhereDBWithGroupBY($where,$group_by,$table)
    {   
        $this->db->where($where);
        $this->db->group_by($group_by);
        $query = $this->db->get($table);
        return $query->result_array();
    }


    //In use
    public function getalloperatordetailslistCount($factory, $problemId, $is_admin=1)
    {
        $machineQ = '';
        if($problemId > 0 ) 
        {
            $machineQ = "and reportProblem.problemId = $problemId";  
        }
        $sql = "SELECT count(*) as totalCount FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1 $machineQ";
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchoperatordetailsProblemCount($factory, $problemId, $is_admin,$status,$type,$userName,$dateValS,$dateValE,$isActive)
    { 
        $statusQ = '';
        if(!empty($status)) 
        {
           $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }

        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and reportProblem.isActive IN (".implode(",", $isActive).")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {  
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        } 

        $dateQ = "and reportProblem.date >= '$dateValS' and reportProblem.date <= '$dateValE'";

        $sql = "SELECT count(*) as totalCount FROM reportProblem  left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1  $userNameQ $typeQ $isActiveQ $statusQ $dateQ";  
        $query = $factory->query($sql); 
        return $query; 
    }

    //In use
    public function getallReportProblemLogs($factory, $problemId , $is_admin=1, $start, $limit, $status,$type,$userName,$dateValS,$dateValE,$isActive)
    {
        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }

        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and reportProblem.isActive IN (".implode(",", $isActive).")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        }

        $dateQ = "and reportProblem.date >= '$dateValS' and reportProblem.date <= '$dateValE'";

        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE 1=1 $userNameQ $isActiveQ $typeQ $statusQ $dateQ order by problemId desc limit $start, $limit ";  
        $query = $factory->query($sql);
        return $query; 
    }

    //In use
    function updateReportProblem($factory,$problemId,$data)
    {
        $factory->where("problemId",$problemId);
        $factory->update("reportProblem",$data);
    }
    
    //In use
    public function addHelpReport($insert) 
    {   
        $this->db->insert('helpReport',$insert); 
    } 


    //In use
    public function getAllMaintenanceCheckinCount($factory, $userId, $is_admin=1)
    {
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join r_nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0'  $userQ group by machineUserv2.activeId";  
        $query = $this->factory->query($sql); 
        return $query; 
    }

    //In use
    public function getMaintenanceCheckin($factory, $dateValS, $dateValE, $userId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("machineUserv2.activeId", "user.userName", "machineUserv2.machineNames", "machineUserv2.isActive", "machineUserv2.startTime", "machineUserv2.endTime" ); 
        $orderCol = $columnArr[$columnIndex];
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }       
        $dateQ = " and machineUserv2.startTime BETWEEN '".$dateValS."' and '".$dateValE."' ";
        $sql = "SELECT machineUserv2.*, user.userName, mNotificationv2.addedDate, mNotificationv2.status, GROUP_CONCAT( maintenanceTemplateCategory.mTCategoryName separator ', ') as type FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join r_nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0' $userQ $dateQ $searchQuery group by machineUserv2.activeId order by $orderCol $columnSortOrder limit $start, $limit";   
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallOperators($factoryId)
    {
       $this->db->select('*');
        $this->db->from('user');
        $this->db->where('isDeleted', '0');
        $this->db->where('userRole', '0');
        $this->db->where('factoryId', $factoryId); 
        $query= $this->db->get();
        return $query;
    }

    //In use
    function lastUserId()
    {
       $this->db->select('max(userId) as maxId'); 
       $this->db->from('user'); 
       $va=$this->db->get()->row();
       return $va; 
    } 

    //In use
    public function newUser($insert)
    { 
        $this->db->insert('user',$insert);
        return $this->db->insert_id();
    } 

    //In use
    public function updateUser($update, $userId)
    { 
        $this->db->where('userId',$userId);
        $this->db->update('user',$update);
    } 

    //In use
    public function deleteUser($userId)
    { 
        $this->db->where('userId',$userId);
        $update = array('isDeleted'=>'1');
        $this->db->update('user',$update); 
    } 

    //In use
    function userLogOutOpApp($userId) 
    { 
        $checkIn = $this->AM->isUserCheckedIn($this->factoryId, $userId);
        if(isset($checkIn) && is_array($checkIn)) 
        {
            $data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s')); 
            $this->AM->updateCheckOut($this->factoryId,$checkIn['activeId'],$data); 
        }
        $logKeyOpApp = '0';
        $update = array("logKeyOpApp"=>$logKeyOpApp);
        $this->db->where('userId',$userId);
        $this->db->update('user',$update); 
        return $logKeyOpApp; 
    }

    //In use
    function isUserCheckedIn($factoryId, $userId) 
    {  
        $sql = "SELECT activeId FROM machineUserv2 WHERE userId = $userId and isActive = '1' order by activeId desc";  
        $query = $this->factory->query($sql);
        if ($query->num_rows() > 0) 
        {
            $result = $query->row_array();   
            return $result;
        } 
        else 
        {
            return false;
        }   
    }

    //In use
    function updateCheckOut($factoryId,$activeId,$data) 
    { 
        $this->factory->where("activeId",$activeId);
        $this->factory->update("machineUserv2",$data);
    }


    //In use
    public function getReportProblemById($factory,$problemId)
    {
        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE problemId = ".$problemId;  
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    public function getconfigureVersionsById($factory,$stackLightTypeId)
    {
        
        $sql = "SELECT stackLightType.*,stackLightType.stackLightTypeName,stackLightType.stackLightTypeImage FROM stackLightType left join r_nytt_main.user on stackLightType.stackLightTypeId = stackLightType.stackLightTypeId WHERE stackLightTypeId = ".$stackLightTypeId;  
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    public function getallOperatorsById($factory,$activeId)
    {
        $sql = "SELECT machineUserv2.*,user.userName,user.userImage from machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId where activeId=".$activeId;
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    //In use
    function sendNotification($message,$title,$fcmToken,$type)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId' => $this->session->userdata('factoryId')
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    //In use
    public function getallStacklighttypeVersionsCount($factory, $stackLightTypeId, $is_admin=1)
    {
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }
        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile WHERE 1=1 $machineQ";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getallStackLightType()
    {
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    public function getallMeetingNotes()
    {
        $this->db->select('*');
        $this->db->from('meetingNotes');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    //In use
    public function getallStacklighttypeVersionsLogs($factory, $dateValS, $dateValE, $stackLightTypeId , $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }

        $dateQ = " and stackLightTypeFile.versionDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT * FROM stackLightTypeFile left join r_nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE r_nytt_main.stackLightType.isDeleted = '0' $machineQ  $dateQ $searchQuery order by $columnIndex $columnSortOrder limit $start, $limit ";  
        $query = $this->db->query($sql); 
        return $query; 
    }

    //In use
    public function getMaxVersion($stackLightTypeId)
    {
        $this->db->where(array("stackLightTypeId" => $stackLightTypeId));
        $query = $this->db->get('stackLightTypeFile');
        return $query->num_rows();
    }

    //In use
    public function getAllVisulizationData($factory, $machineId, $type, $choose1='', $choose2='', $choose3='', $choose4='')
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and analyticsGraphData.machineId = $machineId";  
        }
        $yearQ = '';
        if($type == 'year' ) 
        {
            $yearQ = "and analyticsGraphData.year = $choose1";  
            $period_value = 'month';
        }
        
        $monthQ = '';
        if($type == 'month' ) 
        {
            $monthQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2";  
            $period_value = 'week';  
        }
        
        $weekQ = '';
        if($type == 'week' ) 
        {
            $weekQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.week = $choose3 and analyticsGraphData.week != 0  "; 
            $period_value = 'weekday'; 
        }
        
        $dayQ = '';
        if($type == 'day' ) {
            $dayQ = "and analyticsGraphData.year = $choose1 and analyticsGraphData.month = $choose2 and analyticsGraphData.day = $choose4"; 
            $period_value = 'hour'; 
        }
        
        $state = " and state in('running', 'waiting', 'stopped', 'off', 'nodet') ";
        $sql = "SELECT sum(duration) as countVal, $period_value FROM analyticsGraphData left join machine on analyticsGraphData.machineId = machine.machineId WHERE machine.isDeleted = '0' $machineQ $state $yearQ $monthQ $weekQ $dayQ group by $period_value order by $period_value asc"; 
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getallBetaColorLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        //$columnArr = array("betaDetectionLog.logId", "user.userName", "machine.machineName", "betaDetectionLog.color", "betaDetectionLog.accuracy", "betaDetectionLog.originalTime"); 
        $columnArr = array("betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime");   
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        $userQ = ''; 
        $userStatusQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        //$sql = "SELECT betaDetectionLog.*, user.userName, machine.machineName FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 

        /* echo $sql = "SELECT * FROM
            (SELECT betaDetectionLog.machineId, betaDetectionLog.originalTime, betaDetectionLog.color, machine.machineName FROM betaDetectionLog left join machine on betaDetectionLog.machineId = machine.machineId where 1=1 $machineQ $dateQ $userQ UNION ALL 
            SELECT machineStatusLog.machineId, machineStatusLog.insertTime, machineStatusLog.machineStatus, machine.machineName FROM machineStatusLog left join machine on betaDetectionLog.machineId = machine.machineId where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable order by $orderCol $columnSortOrder limit $start, $limit"; */

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1   $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.logId desc  limit $start, $limit";   
           // echo $sql;exit;
            
        $query = $this->factory->query($sql); 
        return $query; 
    }

     public function getallBetaColorLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1    $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc";   
            
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    //In use
    public function getallBetaColorLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog WHERE 1=1 $machineQ $userQ";


        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime , color FROM betaDetectionLog where 1=1  $machineQ $userQ UNION ALL 
            SELECT machineId, insertTime as originalTime, machineStatus FROM machineStatusLog where 1=1  $machineStatusQ $userStatusQ)betaTable";

        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    //In use
    public function getSearchBetaColorLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }

        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery "; 

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime, color FROM betaDetectionLog where 1=1   $machineQ $dateQ $userQ UNION ALL 
            SELECT machineId, insertTime, machineStatus FROM machineStatusLog where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable left join machine on betaTable.machineId = machine.machineId where 1=1 $searchQuery ";
        //echo $sql;exit;

        $query = $this->factory->query($sql); 
        return $query;  
    } 
    
    //In use
    public function getSearchMaintenanceCheckinCount($factory, $dateValS, $dateValE, $userId, $is_admin=1, $searchQuery)
    { 
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }
        $dateQ = " and machineUserv2.startTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId  left join r_nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0' $userQ $dateQ $searchQuery  group by machineUserv2.activeId";   
        $query = $this->factory->query($sql); 
        return $query;  
    }


    //In use
    public function getAllStackLightVersion($stackLightTypeId)
    {
        $this->db->where(array("stackLightTypeId" => $stackLightTypeId));
        $query = $this->db->get('stackLightTypeFile');
        return $query->result_array();
    }
    
    //In use
    public function getSearchStacklighttypeVersionsCount($factory, $dateValS, $dateValE, $stackLightTypeId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }
        $dateQ = " and stackLightTypeFile.versionDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile left join r_nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE r_nytt_main.stackLightType.isDeleted = '0' $machineQ  $dateQ $searchQuery ";  
        $query = $this->db->query($sql); 
        return $query; 
    } 

    //In use
    function addReportImage($factoryId,$captureTime, $logTime, $fileExt, $machineId, $userId) 
    {  
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $insert = array("imageName"=>$machineId."_".$userId ."_".$logTime.".".$fileExt, 
                    "captureTime"=>date('Y-m-d H:i:s', $captureTime),
                    "logTime"=>date('Y-m-d H:i:s', $logTime),
                    "machineId"=>$machineId,
                    "userId"=>$userId,
                    );
        $this->factory->insert('reportedImages',$insert);  
        return $this->factory->insert_id(); 
    }

    //In use
    function machineExistsNew($machineId) 
    {       
        $sql = "SELECT * from machine where machineId = $machineId and isDeleted = '0' ";   
        $query = $this->factory->query($sql);
        if ($query->num_rows() > 0) 
        {
            $result = $query->row_array();   
            return $result;
        } 
        else 
        {
            return false;
        }
    } 

    //In use
    function checkFactoryDetail($factoryId) 
    { 
        $sql = "SELECT * from factory where factoryId = $factoryId and isDeleted  = '0'";    
        $query = $this->db->query($sql);
        $factory = $query->result_array();   
        return $factory;
    }

    //In use
    public function updateDBData($where,$data, $table)
    {   
        $this->db->where($where);
        $this->db->update($table,$data);
    }

    //In use
    public function getNotificationLog($factoryId)
    {
        $this->db->where('factoryId',$factoryId);
        $this->db->where('status','pending');
        $this->db->order_by('notificationId','desc');
        $this->db->limit(2);
        $query = $this->db->get('notificationLog')->result_array();
        return $query;
    }

    //In use
    public function getallNotificationLogCount($factoryId, $notificationId, $is_admin=1)
    {
        $sql = "SELECT count(*) as totalCount FROM notificationLog WHERE factoryId = $factoryId";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchNotificationLogCount($factoryId, $notificationId, $is_admin=1)
    { 
        $sql = "SELECT count(*) as totalCount FROM notificationLog WHERE factoryId = $factoryId";  
        $query = $this->db->query($sql);
        return $query; 
    }

    //In use
    public function getallNotificationLogLogs($factoryId, $notificationId,$is_admin=1 , $start, $limit)
    {
        $sql = "SELECT machine.machineName,notificationLog.* FROM r_nytt_main.notificationLog left join machine on machine.machineId = notificationLog.machineId WHERE notificationLog.factoryId = $factoryId order by notificationLog.notificationId desc limit $start, $limit";
        $query = $this->factory->query($sql);
        return $query; 
    }

    //In use
    function updateNotificationLiveStatus($factoryId, $notificationId, $machineId, $flag) 
    {  
        $addField = array(
                        "notificationId"=>$notificationId,
                        "flag"=>$flag, 
                        "machineId"=>$machineId,
                        "factoryId"=>$factoryId,
                        ); 
        $this->db->insert('notificationLiveStatus',$addField); 
        return $notificationId;
    }
    
    //In use
    public function getallTaskMaintenaceCount($factory, $machineIds, $is_admin=1)
    {
        $machineQ = '';
        if(!empty($machineId) && $machineId != 'all') 
        {
            $machineQ = "and taskMaintenace.machineId IN ($machineIds)";  
        }
        $sql = "SELECT count(*) as totalCount FROM taskMaintenace WHERE isDelete = '0' and mainTaskId = 0 $machineQ";
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchTaskMaintenaceCount($factory, $userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)
    { 
       $userIdsQ = '';
        if(!empty($userIds)) 
        {   
            $userIdsQ .= "and (";
            foreach ($userIds as $key => $value) 
            {
                $or = ($key == 0) ? "" : "or";
                $userIdsQ .= " $or FIND_IN_SET('$value', taskMaintenace.userIds)";
            }
            $userIdsQ .= ")";
        }
        $repeatQ = '';
        $mainTaskQ = "and mainTaskId = 0";
        if(!empty($repeat)) 
        {
            $repeatQ = "and taskMaintenace.repeat IN (".implode(",", $repeat).")";
        }
        
        $dateQ = "";
        if (!empty($dateValS) && !empty($dateValE)) 
        {
            $dateQ = "and date(taskMaintenace.createdDate) >= '$dateValS' and date(taskMaintenace.createdDate) <= '$dateValE'";
        }
        
        $dueDateQ = "";
        if (!empty($dueDateValS) && !empty($dueDateValE) && $dueDateValS != "2013-01-12") 
        {
            $dueDateQ = "and date(taskMaintenace.dueDate) >= '$dueDateValS' and date(taskMaintenace.dueDate) <= '$dueDateValE'";
            $mainTaskQ = "";
        }

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and taskMaintenace.status IN (".implode(",", $status).")";
        }

        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and taskMaintenace.machineId IN (".implode(",", $machineId).")";
        }

        $machineIdsQ = '';
        if(!empty($machineIds) && $machineIds != 'all') {
            $machineIdsQ = "and taskMaintenace.machineId IN ($machineIds)";  
        }

        $sql = "SELECT count(*) as totalCount FROM taskMaintenace WHERE isDelete = '0' $mainTaskQ $userIdsQ $repeatQ $dateQ $dueDateQ $statusQ $machineIdsQ $machineIdQ "; 
        $query = $factory->query($sql); 
        // echo $sql;exit;
        return $query; 
    }

    //In use
    public function getallTaskMaintenaceLogs($factory, $start, $limit,$userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)
    {
        $userIdsQ = '';
        if(!empty($userIds)) 
        {   
            $userIdsQ .= "and (";
            foreach ($userIds as $key => $value) {
                $or = ($key == 0) ? "" : "or";
                $userIdsQ .= " $or FIND_IN_SET('$value', taskMaintenace.userIds)";
            }
            $userIdsQ .= ")";
        }

       $repeatQ = '';
       $mainTaskQ = "and mainTaskId = 0";
        if(!empty($repeat)) 
        {
            $repeatQ = "and taskMaintenace.repeat IN (".implode(",", $repeat).")";
        }

        $dateQ = "";
        if (!empty($dateValS) && !empty($dateValE)) 
        {
            $dateQ = "and date(taskMaintenace.createdDate) >= '$dateValS' and date(taskMaintenace.createdDate) <= '$dateValE'";
        }

        $dueDateQ = "";
        if (!empty($dueDateValS) && !empty($dueDateValE) && $dueDateValS != "2013-01-12") 
        {
            $dueDateQ = "and date(taskMaintenace.dueDate) >= '$dueDateValS' and date(taskMaintenace.dueDate) <= '$dueDateValE'";
            $mainTaskQ = "";
        }

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and taskMaintenace.status IN (".implode(",", $status).")";
        }
        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and taskMaintenace.machineId IN (".implode(",", $machineId).")";
        }

        $machineIdsQ = '';
        if(!empty($machineIds) && $machineIds != 'all') 
        {
            $machineIdsQ = "and taskMaintenace.machineId IN ($machineIds)";  
        }

        $sql = "SELECT taskMaintenace.*,machine.machineName FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' $mainTaskQ $userIdsQ $repeatQ $dateQ $dueDateQ $statusQ $machineIdsQ $machineIdQ  order by taskId desc limit $start, $limit ";  
        $query = $factory->query($sql);
        // echo $sql; exit;
        return $query; 
    }

    //In use
    public function getTaskById($factory,$taskId)
    {
        $sql = "SELECT taskMaintenace.*,machine.machineName,user.userName,user.userImage FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId left join r_nytt_main.user on user.userId = taskMaintenace.createdByUserId WHERE taskId = ".$taskId;  
        $query = $factory->query($sql)->row(); 
        return $query; 
    }

    function getLastSubTask($taskId)
    {
        $this->factory->where('mainTaskId',$taskId);
        $this->factory->order_by('taskId','desc');
        $query = $this->factory->get('taskMaintenace')->row();
        return $query;
    }

    //In use
    public function getWhereDBJoin($select, $where, $join, $table)
    {   
        $this->db->select($select);
        $this->db->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->db->join($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getWhereJoin($select, $where, $join, $table)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $query = $this->factory->get($table);
        //echo $this->factory->last_query();exit;
        return $query->result_array();
    } 

    public function getWhereJoinSingle($select, $where, $join, $table)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $query = $this->factory->get($table);
        //echo $this->factory->last_query();exit;
        return $query->row();
    }


    public function getWhereJoinWithOr($select, $where,$whereOr, $join, $table)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        if (!empty($whereOr)) 
        {
            $this->factory->group_start();
            foreach ($whereOr as $key => $value) 
            { 
                $this->factory->or_where($key,$value);
            }
            $this->factory->group_end();
        }
        $query = $this->factory->get($table);

        return $query->result_array();
    }

    public function getWhereWithOr($where,$whereOr, $table)
    {   
        $this->factory->where($where);
        if (!empty($whereOr)) 
        {
            $this->factory->group_start();
            foreach ($whereOr as $key => $value) 
            { 
                $this->factory->or_where($key,$value);
            }
            $this->factory->group_end();
        }
        $query = $this->factory->get($table);
        //echo $this->factory->last_query();exit;
        return $query->result_array();
    }

    public function getWhereJoinWithOrderBy($select, $where, $join, $table, $orderKey, $orderS)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->factory->join($key,$value);
        }
        $this->factory->order_by($orderKey,$orderS);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    //In use
    public function getWhereIn($select,$whereIn, $table)
    {   
        $this->factory->select($select);
        foreach ($whereIn as $key => $value) 
        { 
            $this->factory->where_in($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereWithOrWithLikeWithWhereIn($where,$whereOr,$like,$whereIn, $table)
    {   
        $this->factory->where($where);
        if (!empty($whereOr)) 
        {
            $this->factory->group_start();
            foreach ($whereOr as $key => $value) 
            { 
                $this->factory->or_where($key,$value);
            }
            $this->factory->group_end();
        }
        if (!empty($like)) 
        {
            foreach ($like as $keyL => $valueL) 
            { 
                $this->factory->like($keyL, $valueL);
            }
        }
        foreach ($whereIn as $keyIn => $valueIn) 
        { 
            $this->factory->where_in($keyIn, $valueIn);
        }
        $query = $this->factory->get($table);
        //echo $this->factory->last_query();exit;
        return $query->result_array();
    }

     public function getWhereInDB($select,$whereIn, $table)
    {   
        $this->db->select($select);
        foreach ($whereIn as $key => $value) 
        { 
            $this->db->where_in($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    //In use
    public function getWhereInWithWhere($select, $where, $whereIn, $table)
    {  
        $this->factory->select($select);
        $this->factory->where($where);
        foreach ($whereIn as $key => $value) 
        { 
            $this->factory->where_in($key,$value);
        }
        $query = $this->factory->get($table);
        return $query->result_array();
    } 

    public function getWhereInWithWhereDB($select, $where, $whereIn, $table)
    {  
        $this->db->select($select);
        $this->db->where($where);
        foreach ($whereIn as $key => $value) 
        { 
            $this->db->where_in($key,$value);
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    //In use
    function checkOperatorOnline($userId)
    {
        $this->factory->where('userId',$userId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('machineUserv2')->row();
        return $query;
    }

    //In use
    public function getWhereSingle($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    public function getWhereSingleSelect($select, $where, $table)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->row();
    }

    //In use
    public function get_operator_checkin_checkout_count($userRole)
    {
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE user.isDeleted = '0' and user.userRole = '$userRole' group by machineUserv2.activeId";  
        $query = $this->factory->query($sql); 

        // echo $sql; exit;

        return $query; 
    }

    //In use
    public function get_search_operator_checkin_checkout_count($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE,$userRole)
    { 
        $machineIdsQ = '';
        if(!empty($machineIds)) 
        {   
            $machineIdsQ .= "and (";
            foreach ($machineIds as $key => $value) 
            {
                $or = ($key == 0) ? "" : "or";
                $machineIdsQ .= " $or FIND_IN_SET('$value', machineUserv2.workingMachine)";
            }
            $machineIdsQ .= ")";
        }
       
        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and machineUserv2.userId IN (".implode(",", $userName).")";
        }

        $startTimeQ = "";
        if (!empty($startDateValS) && !empty($startDateValE)) 
        {
            $startTimeQ = "and date(machineUserv2.startTime) >= '$startDateValS' and date(machineUserv2.startTime) <= '$startDateValE'";
        }

        $endTimeQ = "";
        if (!empty($endDateValS) && !empty($endDateValE)) 
        {
            $endTimeQ = "and date(machineUserv2.endTime) >= '$endDateValS' and date(machineUserv2.endTime) <= '$endDateValE'";
        }
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId  WHERE user.isDeleted = '0' and user.userRole = '$userRole' $userNameQ $startTimeQ  $endTimeQ $machineIdsQ  group by machineUserv2.activeId";   
        $query = $this->factory->query($sql); 
         // echo $sql; exit;
        return $query;  
    }

    //In use
    public function get_search_operator_checkin_checkout_logs($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE,$start, $limit, $userRole)
    {   
        $machineIdsQ = '';
        if(!empty($machineIds)) 
        {   
            $machineIdsQ .= "and (";
            foreach ($machineIds as $key => $value) 
            {
                $or = ($key == 0) ? "" : "or";
                $machineIdsQ .= " $or FIND_IN_SET('$value', machineUserv2.workingMachine)";
            }
            $machineIdsQ .= ")";
        }

        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and machineUserv2.userId IN (".implode(",", $userName).")";
        }

        $startTimeQ = "";
        if (!empty($startDateValS) && !empty($startDateValE)) 
        {
            $startTimeQ = "and date(machineUserv2.startTime) >= '$startDateValS' and date(machineUserv2.startTime) <= '$startDateValE'";
        }

        $endTimeQ = "";
        if (!empty($endDateValS) && !empty($endDateValE)) 
        {
            $endTimeQ = "and date(machineUserv2.endTime) >= '$endDateValS' and date(machineUserv2.endTime) <= '$endDateValE'";
        }
        
        $sql = "SELECT machineUserv2.*, user.userName,user.userImage, mNotificationv2.addedDate, mNotificationv2.status FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE user.isDeleted = '0' and user.userRole = '$userRole' $userNameQ  $startTimeQ  $endTimeQ $machineIdsQ group by machineUserv2.activeId order by activeId  desc  limit $start, $limit";   

        $query = $this->factory->query($sql);
         // echo $sql; exit;
        return $query; 
    } 

    //In use
    public function display_records($machineId)
    {
        $query = $this->factory->where('machineId',$machineId)->get('phoneDetailLogv2');
        return $query->row();
    } 

    public function displaymachineStatusLog()
    {
        $query = $this->factory->get('machineStatusLog');
        return $query->result();

    }

    //In use
    public function getAllMeetingNotesCount()
    {
        $sql = "SELECT count(*) as totalCount FROM meetingNotes WHERE isDelete = '0'";
        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchMeetingNotesCount($noteId, $noteName, $notes, $createdDate, $filterType, $dateValS, $dateValE)
    { 
        $filterTypeQ = '';
        if(!empty($filterType)) 
        {
            $filterTypeQ = "and meetingNotes.filterType IN (".implode(",", $filterType).")";
        } 

        $dateQ = "and date(meetingNotes.createdDate) >= '$dateValS' and date(meetingNotes.createdDate) <= '$dateValE'";

        $sql = "SELECT count(*) as totalCount FROM meetingNotes WHERE isDelete='0' $filterTypeQ $dateQ"; 
        $query = $this->factory->query($sql); 
        return $query; 
    }

    //In use
    public function getAllMeetingNotesLogs($start, $limit, $noteId, $noteName, $notes, $createdDate, $filterType, $dateValS, $dateValE)
    {
        $filterTypeQ = '';
        if(!empty($filterType)) 
        {
            $filterTypeQ = "and meetingNotes.filterType IN (".implode(",", $filterType).")";
        } 

        $dateQ = "and date(meetingNotes.createdDate) >= '$dateValS' and date(meetingNotes.createdDate) <= '$dateValE'";

        $sql = "SELECT * FROM meetingNotes WHERE isDelete='0' $filterTypeQ $dateQ order by noteId desc limit $start, $limit ";  
        $query = $this->factory->query($sql);
        return $query; 
    }

    //In use
    public function getMeetingNoteDetails($noteId)
    {
        $this->factory->where('noteId',$noteId);
        return $this->factory->get('meetingNotes')->row();
    }

    //In use
    public function getMeetingMachineRunningStatusData($machineIds,$date,$filterType)
    {
        $this->factory->select('sum(duration) as countVal');
        $this->factory->where_in('analyticsGraphData.machineId',explode(",", $machineIds));
        if ($filterType == "1") {
            $this->factory->where('date(date)',date('Y-m-d',strtotime($date)));
        }
        else if ($filterType == "2") 
        {
            $endDate = date('Y-m-d',strtotime("+6 days". $date));
            $this->factory->where('date(date) >=',date('Y-m-d',strtotime($date)));
            $this->factory->where('date(date) <=',date('Y-m-d',strtotime($endDate)));
        }
        else if ($filterType == "3") 
        {
            $this->factory->where('month(date)',date('m',strtotime($date)));
        }
        else if ($filterType == "4") 
        {
            $this->factory->where('year(date)',date('Y',strtotime($date)));
        }
        $this->factory->where('state','running');
        $this->factory->where('analyticsGraphData.machineStatus','0');
        $query = $this->factory->get('analyticsGraphData');
        //echo $this->factory->last_query();echo "<br>";
        return $query->row();
    }

    //In use
    public function getMeetingMachineNoProductionStatusData($machineIds,$date,$filterType)
    {
        $this->factory->select('sum(duration) as countVal');
        $this->factory->where_in('analyticsGraphData.machineId',explode(",", $machineIds));
        if ($filterType == "1") 
        {
            $this->factory->where('date(date)',date('Y-m-d',strtotime($date)));
        }
        else if ($filterType == "2") 
        {
            $endDate = date('Y-m-d',strtotime("+6 days". $date));
            $this->factory->where('date(date) >=',date('Y-m-d',strtotime($date)));
            $this->factory->where('date(date) <=',date('Y-m-d',strtotime($endDate)));
        }
        else if ($filterType == "3") 
        {
            $this->factory->where('month(date)',date('m',strtotime($date)));
        }
        else if ($filterType == "4") 
        {
            $this->factory->where('year(date)',date('Y',strtotime($date)));
        }
        $this->factory->where('analyticsGraphData.machineStatus','2');
        $query = $this->factory->get('analyticsGraphData');
        return $query->row();
    }

    //In use
    public function getMeetingNoOfReportsCount($date,$filterType)
    {
        if ($filterType == "1") 
        {
            $this->factory->where('date',date('Y-m-d',strtotime($date)));
        }
        else if ($filterType == "2") 
        {
            $endDate = date('Y-m-d',strtotime("+6 days". $date));
            $this->factory->where('date >=',date('Y-m-d',strtotime($date)));
            $this->factory->where('date <=',date('Y-m-d',strtotime($endDate)));
        }
        else if ($filterType == "3") 
        {
            $this->factory->where('date',date('m',strtotime($date)));
        }
        else if ($filterType == "4") 
        {
            $this->factory->where('date',date('Y',strtotime($date)));
        }
        $query = $this->factory->get('reportProblem');
        return $query->num_rows();
    }

    //In use
    public function getMeetingNoOfStoppagesCount($machineIds,$date,$filterType)
    {   
        $machineIds = explode(",", $machineIds);
        $result = array();
        foreach ($machineIds as $key => $value) 
        {
            $month = str_pad(date('m',strtotime($date)), 2, '0', STR_PAD_LEFT);
            $year = date('Y',strtotime($date));
            $machineId = $value;
            $tableName = $machineId.'stoppedStateBetalogWithState'.$month.$year;
            if($this->factory->table_exists($tableName))
            {  
                if ($filterType == "1") 
                {
                    $this->factory->where('date(originalTime)',date('Y-m-d',strtotime($date)));
                }
                else if ($filterType == "2") 
                {
                    $endDate = date('Y-m-d',strtotime("+6 days". $date));
                    $this->factory->where('date(originalTime) >=',date('Y-m-d',strtotime($date)));
                    $this->factory->where('date(originalTime) <=',date('Y-m-d',strtotime($endDate)));
                }
                else if ($filterType == "3") 
                {
                    $this->factory->where('month(originalTime)',date('m',strtotime($date)));
                }
                else if ($filterType == "4") 
                {
                    $this->factory->where('year(originalTime)',date('Y',strtotime($date)));
                }
                $query= $this->factory->get($tableName); 
                $result[$key] = $query->num_rows();
            } 
            else 
            {
                $result[$key] = 0;
            }
        }
        return array_sum($result);
    }


    //In use
    public function getAllReportsAProblemCountMeetingNotes($factory, $problemId, $is_admin=1,$filterType,$dateValS,$dateValE)
    {
        $machineQ = '';
        if($problemId > 0 ) 
        {
            $machineQ = "and reportProblem.problemId = $problemId";  
        }

        if ($filterType == "1") 
        {
            $dateQ = "and (reportProblem.date = '$dateValS' or reportProblem.date = '$dateValE')";
        }
        else if ($filterType == "2") 
        {
            $endDateDateValS = date('Y-m-d',strtotime("+6 days" . $dateValS));
            $endDateDateValE = date('Y-m-d',strtotime("+6 days" . $dateValE));
            $dateQ = "and ((reportProblem.date >= '$dateValS' and reportProblem.date <= '$endDateDateValS') or (reportProblem.date >= '$dateValE' and reportProblem.date <= '$endDateDateValE'))";
        }
        else if ($filterType == "3") 
        {
            $dateValSSM = date('m',strtotime($dateValS));
            $dateValSEM = date('m',strtotime($dateValE));
            $dateQ = "and (month(reportProblem.date) = '$dateValSSM' or month(reportProblem.date) = '$dateValSEM')";
        }
        else if ($filterType == "4") 
        {
            $dateValSSY = date('Y',strtotime($dateValS));
            $dateValSEY = date('Y',strtotime($dateValE));
            $dateQ = "and (year(reportProblem.date) = '$dateValSSY' or year(reportProblem.date) = '$dateValSEY')";
        }

        $sql = "SELECT count(*) as totalCount FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE reportProblem.isActive = '0' $machineQ $dateQ";
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchReportsAProblemCountMeetingNotes($factory, $problemId, $is_admin,$status,$type,$userName,$dateValS,$dateValE,$isActive,$filterType)
    { 
        $statusQ = '';
        if(!empty($status)) 
        {
           $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }
        
        $userNameQ = '';
        if(!empty($userName)) 
        {  
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        } 

        if ($filterType == "1") 
        {
            $dateQ = "and (reportProblem.date = '$dateValS' or reportProblem.date = '$dateValE')";
        }
        else if ($filterType == "2") 
        {
            $endDateDateValS = date('Y-m-d',strtotime("+6 days" . $dateValS));
            $endDateDateValE = date('Y-m-d',strtotime("+6 days" . $dateValE));
            $dateQ = "and ((reportProblem.date >= '$dateValS' and reportProblem.date <= '$endDateDateValS') or (reportProblem.date >= '$dateValE' and reportProblem.date <= '$endDateDateValE'))";
        }
        else if ($filterType == "3") 
        {
            $dateValSSM = date('m',strtotime($dateValS));
            $dateValSEM = date('m',strtotime($dateValE));
            $dateQ = "and (month(reportProblem.date) = '$dateValSSM' or month(reportProblem.date) = '$dateValSEM')";
        }
        else if ($filterType == "4") 
        {
            $dateValSSY = date('Y',strtotime($dateValS));
            $dateValSEY = date('Y',strtotime($dateValE));
            $dateQ = "and (year(reportProblem.date) = '$dateValSSY' or year(reportProblem.date) = '$dateValSEY')";
        }

        $sql = "SELECT count(*) as totalCount FROM reportProblem  left join r_nytt_main.user on user.userId = reportProblem.userId WHERE reportProblem.isActive = '0'  $userNameQ $typeQ $statusQ $dateQ";  
        $query = $factory->query($sql); 
        return $query; 
    }

    //In use
    public function getReportsAProblemLogsMeetingNotes($factory, $problemId , $is_admin=1, $start, $limit, $status,$type,$userName,$dateValS,$dateValE,$isActive,$filterType)
    {
        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and reportProblem.status IN (".implode(",", $status).")";
        }

        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and reportProblem.type IN (".implode(",", $type).")";
        }
        
        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and reportProblem.userId IN (".implode(",", $userName).")";
        }

        if ($filterType == "1") 
        {
            $dateQ = "and (reportProblem.date = '$dateValS' or reportProblem.date = '$dateValE')";
        }
        else if ($filterType == "2") 
        {
            $endDateDateValS = date('Y-m-d',strtotime("+6 days" . $dateValS));
            $endDateDateValE = date('Y-m-d',strtotime("+6 days" . $dateValE));
            $dateQ = "and ((reportProblem.date >= '$dateValS' and reportProblem.date <= '$endDateDateValS') or (reportProblem.date >= '$dateValE' and reportProblem.date <= '$endDateDateValE'))";
        }
        else if ($filterType == "3") 
        {
            $dateValSSM = date('m',strtotime($dateValS));
            $dateValSEM = date('m',strtotime($dateValE));
            $dateQ = "and (month(reportProblem.date) = '$dateValSSM' or month(reportProblem.date) = '$dateValSEM')";
        }
        else if ($filterType == "4") 
        {
            $dateValSSY = date('Y',strtotime($dateValS));
            $dateValSEY = date('Y',strtotime($dateValE));
            $dateQ = "and (year(reportProblem.date) = '$dateValSSY' or year(reportProblem.date) = '$dateValSEY')";
        }

        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE reportProblem.isActive = '0' $userNameQ $typeQ $statusQ order by problemId desc limit $start, $limit ";$query = $factory->query($sql);
        return $query; 
    }

    //In use
    public function getMachineStatusDataDashboard4($machineId,$choose1,$choose2,$machineStatus,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        if ($choose1 == "day") 
        {
            $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $this->factory->where('week',$week);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = $choose2[0];


            if ($month == "Jan") 
            {
                $month = "1";
            }
            elseif ($month == "Feb") 
            {
                $month = "2";
            }
            elseif ($month == "Mar") 
            {
                $month = "3";
            }
            elseif ($month == "Apr") 
            {
                $month = "4";
            }
            elseif ($month == "May") 
            {
                $month = "5";
            }
            elseif ($month == "Jun") 
            {
                $month = "6";
            }
            elseif ($month == "Jul") 
            {
                $month = "7";
            }
            elseif ($month == "Aug") 
            {
                $month = "8";
            }
            elseif ($month == "Sep") 
            {
                $month = "9";
            }
            elseif ($month == "Oct") 
            {
                $month = "10";
            }
            elseif ($month == "Nov") 
            {
                $month = "11";
            }
            elseif ($month == "Dec") 
            {
                $month = "12";
            }

            $year = $choose2[1];
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('year',$choose2);
        }
            if ($state == "noStacklight") 
            {
                $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
                $this->factory->where('machine.noStacklight',"1");
                $this->factory->where('state','running');
            }else
            {
                $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
                $this->factory->where('state',$state);
            }
            $this->factory->where('analytics_graph_data.machineStatus',$machineStatus);
            $query = $this->factory->get('analytics_graph_data');
            return $query->row();
    }

    //In use
    public function getHourDataDashboard4($machineId,$hour,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        $this->factory->where('hour',$hour);
        if ($state == "setup") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analytics_graph_data.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }

    //In use
    public function getHourDataActualProducationDashboard4($machineId,$hour,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($choose2)));
        $this->factory->where('hour',$hour);
        $this->factory->where('analytics_graph_data.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }

    //In use
    public function getWeekDataDashboard4($machineId,$weekDate,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        if ($state == "setup") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"1");
        }

        elseif ($state == "no_producation") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"2");
        }

        else
        {
            $this->factory->where('analytics_graph_data.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }

    //In use
    public function getWeekDataActualProducationDashboard4($machineId,$weekDate,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        
        $this->factory->where('date(date)',date('Y-m-d',strtotime($weekDate)));
        $this->factory->where('year',$year);
        $this->factory->where('analytics_graph_data.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }

    //In use
    public function getMonthDataDashboard4($machineId,$day,$month,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            if ($state == "setup") 
            {
                $this->factory->where('analytics_graph_data.machineStatus',"1");
            }
            elseif ($state == "no_producation") 
            {
                $this->factory->where('analytics_graph_data.machineStatus',"2");
            }
            else
            {
                $this->factory->where('analytics_graph_data.machineStatus',"0");
                $this->factory->where('state',$state);
                $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
                $this->factory->where('machine.noStacklight',"0");
            }
            $query = $this->factory->get('analytics_graph_data');
            return $query->row();
    }

    //In use
    public function getMonthDataActualProducationDashboard4($machineId,$day,$month,$year,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
            $this->factory->where('day',$day);
            $this->factory->where('month',$month);
            $this->factory->where('year',$year);
            $this->factory->where('analytics_graph_data.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
            $this->factory->where('machine.noStacklight',"1");
            $query = $this->factory->get('analytics_graph_data');
            return $query->row();
    }

    //In use
    public function getYearDataDashboard4($machineId,$month,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        if ($state == "setup") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"1");
        }
        elseif ($state == "no_producation") 
        {
            $this->factory->where('analytics_graph_data.machineStatus',"2");
        }
        else
        {
            $this->factory->where('analytics_graph_data.machineStatus',"0");
            $this->factory->where('state',$state);
            $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
            $this->factory->where('machine.noStacklight',"0");
        }
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }

    //In use
    public function getYearDataActualProducationDashboard4($machineId,$month,$choose2,$state)
    {
        $this->factory->select('sum(duration) as countVal');
        if (!empty($machineId)) 
        {
            $this->factory->where('analytics_graph_data.machine_id',$machineId);
        }
        $this->factory->where('month',$month);
        $this->factory->where('year',$choose2);
        $this->factory->where('analytics_graph_data.machineStatus',"0");
        $this->factory->where('state',$state);
        $this->factory->join("machine","analytics_graph_data.machine_id = machine.machineId");
        $this->factory->where('machine.noStacklight',"1");
        $query = $this->factory->get('analytics_graph_data');
        return $query->row();
    }


    //In use
    public function getReportsAProblemLogsMeetingNotesPdf($dateValS,$dateValE,$filterType)
    {
        $sql = "SELECT reportProblem.*,user.userName,user.userImage FROM reportProblem left join r_nytt_main.user on user.userId = reportProblem.userId WHERE reportProblem.isActive = '0'  order by problemId desc ";  
        $query = $this->factory->query($sql);
        return $query; 
    }

    //In use
    public function getAllvirtualMachineLogCount($factory)
    {
        $sql = "SELECT count(*) as totalCount FROM virtualMachineLog";
        $query = $factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchvirtualMachineLogCount($factory,$IOName,$dateValS,$dateValE, $endDateValS, $endDateValE, $signal)
    { 
        $IONameQ = "";
        if (!empty($IOName)) {
            $IONameQ = "and virtualMachineLog.IOName IN ($IOName)";  
        }

        $signalQ = "";
        if (!empty($signal)) {
            $signalQ = "and virtualMachineLog.signalType IN ($signal)";  
        }

        //$dateQ = "and date(virtualMachineLog.addedTime) >= '$dateValS' and date(virtualMachineLog.addedTime) <= '$dateValE'";

        $dateOrignialQ = "";
        if(!empty($endDateValS) && !empty($endDateValE))
        {
            $dateOrignialQ = "and date(virtualMachineLog.originalTime) >= '$endDateValS' and date(virtualMachineLog.originalTime) <= '$endDateValE'";
        }

        $sql = "SELECT count(*) as totalCount FROM virtualMachineLog WHERE 1 =1 $IONameQ $dateQ $dateOrignialQ $signalQ";  
        $query = $factory->query($sql); 
        return $query; 
    }

    //In use
    public function getvirtualMachineLogLogs($factory,$start, $limit,$IOName,$dateValS, $dateValE, $endDateValS, $endDateValE, $signal)
    {
        $IONameQ = "";
        if (!empty($IOName)) {
            $IONameQ = "and virtualMachineLog.IOName IN ($IOName)";  
        }

        $signalQ = "";
        if (!empty($signal)) {
            $signalQ = "and virtualMachineLog.signalType IN ($signal)";  
        }

        //$dateQ = "and date(virtualMachineLog.addedTime) >= '$dateValS' and date(virtualMachineLog.addedTime) <= '$dateValE'"; 

        $dateOrignialQ = "";
        if(!empty($endDateValS) && !empty($endDateValE))
        {
            $dateOrignialQ = "and date(virtualMachineLog.originalTime) >= '$endDateValS' and date(virtualMachineLog.originalTime) <= '$endDateValE'";
        }

        $sql = "SELECT * FROM virtualMachineLog WHERE 1 =1 $IONameQ $dateQ $dateOrignialQ $signalQ  order by logId desc limit $start, $limit";  
        $query = $factory->query($sql);
        return $query; 
    }






    public function getvirtualMachineLogLogsExport($IOName,$dateValS,$dateValE,$signal,$endDateValS,$endDateValE)
    {
        $IONameQ = "";
        if (!empty($IOName)) {
            $IONameQ = "and virtualMachineLog.IOName IN ($IOName)";  
        }

        $signalQ = "";
        if (!empty($signal)) {
            $signalQ = "and virtualMachineLog.signalType IN ($signal)";  
        }

        //$dateQ = "and date(virtualMachineLog.addedTime) >= '$dateValS' and date(virtualMachineLog.addedTime) <= '$dateValE'";

        $dateOrignialQ = "";
        if(!empty($endDateValS) && !empty($endDateValE))
        {
            $dateOrignialQ = "and date(virtualMachineLog.originalTime) >= '$endDateValS' and date(virtualMachineLog.originalTime) <= '$endDateValE'";
        }
        $sql = "SELECT * FROM virtualMachineLog WHERE 1 =1 $IONameQ $dateQ $signalQ $dateOrignialQ order by logId desc";  
        $query = $this->factory->query($sql);
        return $query; 
    }

    //In use
    public function getVirtualMachineLogData($IOName,$choose1,$choose2,$machineStatus,$choose7,$choose8)
    {
        if (!empty($IOName)) {
            $this->factory->where('virtualMachineLog.IOName',$IOName);
        }

        if ($choose1 == "day") 
        {
            $this->factory->where('date(originalTime)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $dto = new DateTime();
            $dto->setISODate($year, $week);
            $startDate = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $endDate = $dto->format('Y-m-d');

            $this->factory->where('date(originalTime) >=',$startDate);
            $this->factory->where('date(originalTime) <=',$endDate);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = date('m',strtotime($choose2[0]));
            $year = $choose2[1];
            $this->factory->where('MONTH(originalTime)',$month);
            $this->factory->where('YEAR(originalTime)',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('YEAR(originalTime)',$choose2);
        }
        else if ($choose1 == "custom") 
        {
            $choose2 = explode(" - ", $choose2);
            $startDate = $choose2[0];
            $endDate = $choose2[1];
            $this->factory->where('date(originalTime) >=', date('Y-m-d',strtotime($startDate)));
            $this->factory->where('date(originalTime) <=', date('Y-m-d',strtotime($endDate)));
        }

        $choose8 = !empty($choose8) ? $choose8 : "23:00";
        $this->factory->where('time(originalTime) >=',date('H:i:s',strtotime($choose7)));
        $this->factory->where('time(originalTime) <=',date('H:i:s',strtotime($choose8)));

        $this->factory->select('sum(duration) as countVal');
        $this->factory->where('virtualMachineLog.signalType',$machineStatus);
        $query = $this->factory->get('virtualMachineLog');
        return $query->row();
    }

    //In use
    public function getPartCountData($IOName,$choose1,$choose2,$choose7,$choose8)
    {
        if (!empty($IOName)) {
            $this->factory->where('virtualMachineLog.IOName',$IOName);
        }

        if ($choose1 == "day") 
        {
            $this->factory->where('date(originalTime)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $dto = new DateTime();
            $dto->setISODate($year, $week);
            $startDate = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $endDate = $dto->format('Y-m-d');

            $this->factory->where('date(originalTime) >=',$startDate);
            $this->factory->where('date(originalTime) <=',$endDate);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = date('m',strtotime($choose2[0]));
            $year = $choose2[1];
            $this->factory->where('MONTH(originalTime)',$month);
            $this->factory->where('YEAR(originalTime)',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('YEAR(originalTime)',$choose2);
        }
        else if ($choose1 == "custom") 
        {
            $choose2 = explode(" - ", $choose2);
            $startDate = $choose2[0];
            $endDate = $choose2[1];
            $this->factory->where('date(originalTime) >=', date('Y-m-d',strtotime($startDate)));
            $this->factory->where('date(originalTime) <=', date('Y-m-d',strtotime($endDate)));
        }


        $choose8 = !empty($choose8) ? $choose8 : "23:59";
        $this->factory->where('time(originalTime) >=',date('H:i:00',strtotime($choose7)));
        $this->factory->where('time(originalTime) <=',date('H:i:59',strtotime($choose8)));

        $this->factory->where('virtualMachineLog.signalType',"1");
        $query = $this->factory->get('virtualMachineLog');
       // echo $this->factory->last_query();exit;
        return $query->num_rows();
    }

    public function getPartCountDataInAnalytics($IOName,$choose1,$choose2,$choose7,$choose8)
    {
        if (!empty($IOName)) {
            $this->factory->where('virtualMachineLog.IOName',$IOName);
        }

        if ($choose1 == "day") 
        {
            $this->factory->where('date(originalTime)',date('Y-m-d',strtotime($choose2)));
        }
        else if ($choose1 == "weekly") 
        {
            $choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];

            $dto = new DateTime();
            $dto->setISODate($year, $week);
            $startDate = $dto->format('Y-m-d');
            $dto->modify('+6 days');
            $endDate = $dto->format('Y-m-d');

            $this->factory->where('date(originalTime) >=',$startDate);
            $this->factory->where('date(originalTime) <=',$endDate);
        }
        else if ($choose1 == "monthly") 
        {
            $choose2 = explode(" ", $choose2);
            $month = date('m',strtotime($choose2[0]));
            $year = $choose2[1];
            $this->factory->where('MONTH(originalTime)',$month);
            $this->factory->where('YEAR(originalTime)',$year);
        }
        else if ($choose1 == "yearly") 
        {
            $this->factory->where('YEAR(originalTime)',$choose2);
        }
        else if ($choose1 == "custom") 
        {
            $choose2 = explode(" - ", $choose2);
            $startDate = $choose2[0];
            $endDate = $choose2[1];
            $this->factory->where('date(originalTime) >=', date('Y-m-d',strtotime($startDate)));
            $this->factory->where('date(originalTime) <=', date('Y-m-d',strtotime($endDate)));
        }

        if (!empty($choose7) && !empty($choose8)) 
        {
            $this->factory->where('time(originalTime) >=',date('H:i:s',strtotime($choose7)));
            $this->factory->where('time(originalTime) <=',date('H:i:s',strtotime($choose8)));
        }else
        {
            $this->factory->where('time(originalTime) >=',date('H:i:s',strtotime('0:00')));
            $this->factory->where('time(originalTime) <=',date('H:i:s',strtotime('23:59')));
        }

        $this->factory->where('virtualMachineLog.signalType',"1");
        $query = $this->factory->get('virtualMachineLog');
        return $query->num_rows();
    }
    public function display_task()
    {
        $query=$this->db->query("select * from task_testing");
        return $query->result();
    }
    
    function insertTask($data)
    {
        $result=$this->db->insert('task_testing',$data);
        return $result;
    }

    function deleteHard($where,$table)
    {
        $this->factory->where($where);
        $this->factory->delete($table);
    }
    
    public function getallsocketConnectDisConnectDataCount()
    {
        $sql = "SELECT count(*) as totalCount FROM setAppLive join machine on machine.machineId = setAppLive.machineId WHERE machine.isDeleted = '0'";
        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchsocketConnectDisConnectDataCount($dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)
    { 
        $dateQ = "";
        if (!empty($dateValS)) {
            $dateQ = "and date(setAppLive.startTime) >= '$dateValS' and date(setAppLive.endTime) <= '$dateValE'";
        }

        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and setAppLive.machineId IN (".implode(",", $machineId).")";
        }

        $dueDateQ = "";
        if (!empty($dueDateValS)) 
        {
            $dueDateQ = "and date(setAppLive.startTime) >= '$dueDateValS' and date(setAppLive.endTime) <= '$dueDateValE'";
        }
        $sql = "SELECT count(*) as totalCount FROM setAppLive join machine on machine.machineId = setAppLive.machineId WHERE machine.isDeleted = '0' $dateQ $dueDateQ $machineIdQ";  
        $query = $this->factory->query($sql); 
        return $query; 
    }

    //In use
    public function getsocketConnectDisConnectDataLogs($start, $limit,$dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)
    {
        $dateQ = "";
        if (!empty($dateValS)) {
            $dateQ = "and date(setAppLive.startTime) >= '$dateValS' and date(setAppLive.endTime) <= '$dateValE'";
        }

        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and setAppLive.machineId IN (".implode(",", $machineId).")";
        }

        $dueDateQ = "";
        if (!empty($dueDateValS)) 
        {
            $dueDateQ = "and date(setAppLive.startTime) >= '$dueDateValS' and date(setAppLive.endTime) <= '$dueDateValE'";
        }

        $sql = "SELECT setAppLive.*,machine.machineName FROM setAppLive join machine on machine.machineId = setAppLive.machineId WHERE machine.isDeleted = '0' $dateQ $dueDateQ $machineIdQ order by setAppLive.appId desc limit $start, $limit";  
        $query = $this->factory->query($sql);
        return $query; 
    }

    public function getErrorType()
    {
        $this->db->select('*');
        $this->db->from('errorType');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    } 

    public function getAllSettAppStartLogCount($factory)
    {
        $sql = "SELECT count(*) as totalCount FROM startSetuppLog";
        // echo $sql; exit;
        $query = $this->factory->query($sql); 
        return $query;    
    }

    //In use
    public function SettAppStartLog_pagination($start, $limit, $type, $userId, $startDate, $dateValS, $dateValE, $machineId)
    { 
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and startSetuppLog.type IN (".implode(",", $type).")";
        } 

       $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and startSetuppLog.userId IN (".implode(",", $userName).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and startSetuppLog.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(startSetuppLog.startDate) >= '$dateValS' and date(startSetuppLog.startDate) <= '$dateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM startSetuppLog join machine on startSetuppLog.machineId = machine.machineId left join  r_nytt_main.user on startSetuppLog.userId = user.userId WHERE 1=1  $searchQuery $typeQ $userNameQ $dateQ $machineNameQ order by id desc"; 

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallSettAppStartLog($start, $limit, $type, $userName, $startDate, $dateValS, $dateValE, $machineId)
    {
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and startSetuppLog.type IN (".implode(",", $type).")";
        } 

        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and startSetuppLog.userId IN (".implode(",", $userName).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and startSetuppLog.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(startSetuppLog.startDate) >= '$dateValS' and date(startSetuppLog.startDate) <= '$dateValE'";

        $sql = "SELECT startSetuppLog.*,machine.machineName,user.userName FROM startSetuppLog join machine on startSetuppLog.machineId = machine.machineId left join  r_nytt_main.user on startSetuppLog.userId = user.userId where machine.isDeleted = '0' $searchQuery $typeQ $userNameQ $dateQ $machineNameQ order by id desc limit $start, $limit";

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query;
    }

      public function getallappVersions()
    {
        $this->db->select('*');
        $this->db->from('appVersions');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }

    function AddInstruction($data)
    {
        $result=$this->db->insert('instruction',$data);
        return $result;
    }

    public function getWhereLike($where, $like, $table = '') {
        $this->db->where($where);
        if (!empty($like['search_keyword'])) 
        {   
            $this->db->group_start();
            foreach ($like as $key => $value) { 
                $this->db->or_like("$key","$value",false);
            }
            $this->db->group_end();
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function getWhereLikeWithLeftJoin($select, $where, $like, $join, $table = '') {
        $this->db->select($select);
        $this->db->where($where);
        foreach ($join as $key => $value) 
        { 
            $this->db->join("$key","$value",'left');
        }
        if (!empty($like['search_keyword'])) 
        {   
            $this->db->group_start();
            foreach ($like as $key => $value) { 
                $this->db->or_like("$key","$value",false);
            }
            $this->db->group_end();
        }
        $query = $this->db->get($table);
        return $query->result_array();
    }
    public function getAllRolesCount($db)
    {
        $sql = "SELECT count(*) as totalCount FROM user where isDeleted = '0' and  factoryId = ".$this->factoryId;
        $query = $this->db->query($sql); 
        return $query;    
    }

    public function getROlesandMaincount($start, $limit, $userName, $userRole, $dateValS, $dateValE)
    { 
        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and user.userId IN (".implode(",", $userName).")";
        }

        $userRoleQ = '';
        if(!empty($userRole)) 
        {
            $userRoleQ = "and user.userRole IN (".implode(",", $userRole).")";
        }  

        $dateQ = "and date(user.createdDate) >= '$dateValS' and date(user.createdDate) <= '$dateValE' and  factoryId = ".$this->factoryId;
          
        $sql = "SELECT count(*) as totalCount FROM user WHERE 1=1 and isDeleted = '0' $dateQ $userNameQ $userRoleQ order by userId desc"; 
        $query = $this->db->query($sql); 
        return $query; 
    } 

    public function getallRolesAdnMain($row, $rowperpage, $userName, $userRole, $dateValS, $dateValE)
    {
        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and user.userId IN (".implode(",", $userName).")";
        }
            
        $userRoleQ = '';
        if(!empty($userRole)) 
        {
            $userRoleQ = "and user.userRole IN (".implode(",", $userRole).")";
        }  

        $dateQ = "and date(user.createdDate) >= '$dateValS' and date(user.createdDate) <= '$dateValE' and  factoryId = ".$this->factoryId;

        $sql = "SELECT * FROM user Where 1=1 AND isDeleted='0' $dateQ $userNameQ $userRoleQ and userId != 540 order by userId desc limit $row, $rowperpage";
        $query = $this->db->query($sql); 
        return $query; 
    }




    public function getallEmailStatusDataCount($db)
    {
        $sql = "SELECT count(*) as totalCount FROM emailStatus";
        // echo $sql; exit;
        $query = $this->db->query($sql); 
        return $query;    
    }


    //In use
    public function getSearchEmailStatus($start, $limit, $dateValS  ,$dateValE ,$isActive ,$insertTime)
    { 
        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and emailStatus.isActive IN (".implode(",", $isActive).")";
        }

        $dateQ = "and date(emailStatus.insertTime) >= '$dateValS' and date(emailStatus.insertTime) <= '$dateValE' and  factoryId = ".$this->factoryId;

        $sql = "SELECT count(*) as totalCount FROM emailStatus WHERE 1=1 $dateQ $isActiveQ";  
        $query = $this->db->query($sql);
        // echo $sql;exit; 
        return $query; 
    }

    //In use
    public function getemailStatusDataLogs($row, $rowperpage, $dateValS, $dateValE, $isActive, $insertTime)
    {
        $isActiveQ = '';
        if(!empty($isActive)) 
        {
            $isActiveQ = "and emailStatus.isActive IN (".implode(",", $isActive).")";
        }

        $dateQ = "and date(emailStatus.insertTime) >= '$dateValS' and date(emailStatus.insertTime) <= '$dateValE' and  factoryId = ".$this->factoryId;

         $sql = "SELECT * FROM emailStatus  WHERE 1=1  $searchQuery $dateQ $isActiveQ order by emailStatusId  desc limit $row, $rowperpage";
        $query = $this->db->query($sql); 
        // echo $sql;exit;
        return $query;

    }

     public function getAllmachineStatusLogCount($factory)
    {
        $machineQ = '';
        if(!empty($machineId) && $machineId != 'all') 
        {
            $machineQ = "and taskMaintenace.machineId IN ($machineIds)";  
        }
        $sql = "SELECT count(*) as totalCount FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'";
        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function machineStatusLog_pagination($start, $limit, $logId, $machineId, $machineStatus, $insertTime, $dateValS, $dateValE,$machineName)
    { 
        $machineStatusQ = '';
        if(!empty($machineStatus)) 
        {
            $machineStatusQ = "and machineStatusLog.machineStatus IN (".implode(",", $machineStatus).")";
        } 

        $machineNameQ = '';
        if(!empty($machineName)) 
        {
            $machineNameQ = "and machineStatusLog.machineId IN (".implode(",", $machineName).")";
        } 

        $dateQ = "and date(machineStatusLog.insertTime) >= '$dateValS' and date(machineStatusLog.insertTime) <= '$dateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'  $searchQuery $machineStatusQ $dateQ $machineNameQ"; 
        // echo $sql; exit;
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallmachineStatusLog($start, $limit, $logId, $machineId, $machineStatus, $insertTime, $dateValS, $dateValE,$machineName)
    {
        $machineStatusQ = '';
        if(!empty($machineStatus)) 
        {
            $machineStatusQ = "and machineStatusLog.machineStatus IN (".implode(",", $machineStatus).")";
        }

        $machineNameQ = '';
        if(!empty($machineName)) 
        {
            $machineNameQ = "and machineStatusLog.machineId IN (".implode(",", $machineName).")";
        } 

        $dateQ = "and date(machineStatusLog.insertTime) >= '$dateValS' and date(machineStatusLog.insertTime) <= '$dateValE'";
        
        $sql = "SELECT machineStatusLog.*,machine.machineName FROM machineStatusLog join machine on machineStatusLog.machineId = machine.machineId WHERE machine.isDeleted = '0'  $searchQuery $machineStatusQ $dateQ $machineNameQ order by machineStatusLog.logId desc limit $start, $limit";  
        $query = $this->factory->query($sql); 
        return $query; 
    }

    public function phoneMovementDetailLogv2display_records()
    {
        $query = $this->factory->query("select * from phoneMovementDetailLogv2 limit 50000");
        return $query->result();
    }

    public function getAllnotificationListLogCount($factory)
    {
        $sql = "SELECT count(*) as totalCount FROM notification";
        // echo $sql; exit;
        $query = $this->factory->query($sql); 
        return $query;    
    }

    //In use
    public function getAllnotificationListLogpagination($start, $limit, $userId, $userName, $machineId, $machineName, $addedDate, $status, $respondTime, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)
    { 
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and notification.type IN (".implode(",", $type).")";
        } 

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and notification.status IN (".implode(",", $status).")";
        } 


        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and notification.userId IN (".implode(",", $userId).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and notification.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(notification.addedDate) >= '$dateValS' and date(notification.addedDate) <= '$dateValE'";

        $respondeddateQ = "and date(notification.respondTime) >= '$dueDateValS' and date(notification.respondTime) <= '$dueDateValE'";
        
        $sql = "SELECT count(*) as totalCount FROM notification join machine on notification.machineId = machine.machineId join r_nytt_main.user on notification.userId = user.userId WHERE 1=1  $searchQuery $typeQ $statusQ $userNameQ $machineNameQ 
        $dateQ $respondeddateQ order by notificationId desc"; 

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query; 
    } 

    //In use
    public function getallnotificationLog($row, $rowperpage, $userId, $userName, $machineId ,$machineName ,$addedDate , $status, $respondTime, $responderId, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)
    {
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and notification.type IN (".implode(",", $type).")";
        } 

        $statusQ = '';
        if(!empty($status)) 
        {
            $statusQ = "and notification.status IN (".implode(",", $status).")";
        } 


        $userNameQ = '';
        if(!empty($userName)) 
        {
            $userNameQ = "and notification.userId IN (".implode(",", $userId).")";
        } 

        $machineNameQ = '';
        if(!empty($machineId)) 
        {
            $machineNameQ = "and notification.machineId IN (".implode(",", $machineId).")";
        } 

        $dateQ = "and date(notification.addedDate) >= '$dateValS' and date(notification.addedDate) <= '$dateValE'";

        $respondeddateQ = "and date(notification.respondTime) >= '$dueDateValS' and date(notification.respondTime) <= '$dueDateValE'";
        
        $sql = "SELECT * FROM notification join machine on notification.machineId = machine.machineId join r_nytt_main.user on notification.userId = user.userId WHERE 1=1 $searchQuery $typeQ $statusQ $userNameQ $machineNameQ $dateQ $respondeddateQ order by notificationId desc"; 

        // echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query;
    } 


    public function getStopAndWaitpAnalyticsData($factory, $machineId, $choose1, $choose2, $analytics) 
    {
        if ($analytics == "waitAnalytics") {
            $tableName = $machineId.'waitingStateBetalogWithState';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState';
        }

        
        if($factory->table_exists($tableName) )
        { 
            $date = $choose1."-".$choose2Pad."-".$choose4;

            if ($choose1 == "day") 
            {
                $dateQ = "and date($tableName.originalTime) = '".date('Y-m-d',strtotime($choose2))."'";
            }
            else if ($choose1 == "weekly") 
            {
                $choose2 = explode("/", $choose2);
                $week = $choose2[0];
                $year = $choose2[1];

                $dateQ = "and WEEK($tableName.originalTime) = ".$week." and YEAR($tableName.originalTime) = ".$year;
            }
            else if ($choose1 == "monthly") 
            {
                $choose2 = explode(" ", $choose2);
                $month = $choose2[0];
                $month = date('m',strtotime($choose2[0]));
                $year = $choose2[1];

                $dateQ = "and MONTH($tableName.originalTime) = ".$month." and YEAR($tableName.originalTime) = ".$year;
            }
            else if ($choose1 == "yearly") 
            {
                $dateQ = "and YEAR($tableName.originalTime) = ".$choose2;
            }
            $sql = "SELECT $tableName.timeDiff as colorDuration, TIME_TO_SEC($tableName.originalTime) as timeS, $tableName.userId, $tableName.originalTime, $tableName.status, $tableName.comment, $tableName.commentFlag, $tableName.addedDate, $tableName.reasonId, $tableName.upperLimit FROM $tableName WHERE 1 = 1 $dateQ and isToIgnore = '0' order by $tableName.originalTime asc";
            //echo $sql;exit;
            $query = $factory->query($sql);
            return $query;  
        } 
        else 
        {
            return '';
        }
    } 
    
    //In use
    public function getStopAndWaitIssueReasonAnalyticsData($choose1, $choose2, $minDuration, $maxDuration, $machineId=0, $analytics)
    {
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState';
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState';
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $this->factory->select('stopT.notificationId, stopT.comment, stopT.timeDiff, stopT.reasonId' );  
            $this->factory->from($tableName.' as stopT');
            $this->factory->where('stopT.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('stopT.machineId', $machineId); 
            } 
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('stopT.type', $type); 
            $this->factory->where('stopT.isToIgnore', '0'); 
            /*$this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); */


            if ($choose1 == "day") 
            {
                $this->factory->where('date(stopT.addedDate)', date('Y-m-d',strtotime($choose2))); 
            }
            else if ($choose1 == "weekly") 
            {
                $choose2 = explode("/", $choose2);
                $week = $choose2[0];
                $year = $choose2[1];

                $this->factory->where('WEEK(stopT.addedDate)', $week); 
                $this->factory->where('YEAR(stopT.addedDate)', $year); 
            }
            else if ($choose1 == "monthly") 
            {
                $choose2 = explode(" ", $choose2);
                $month = $choose2[0];
                $month = date('m',strtotime($choose2[0]));
                $year = $choose2[1];

                $this->factory->where('MONTH(stopT.addedDate)', $month); 
                $this->factory->where('YEAR(stopT.addedDate)', $year); 
            }
            else if ($choose1 == "yearly") 
            {
                $this->factory->where('YEAR(stopT.addedDate)', $choose2); 
            }



            $query= $this->factory->get(); 
            $result = $query->result();
            //echo $this->factory->last_query();exit;
            return $result;
        } 
        else 
        {
            return '';
        }
    }


    public function getStopAndWaitIssueReasonAnalyticsDataOther($choose1, $choose2, $minDuration, $maxDuration, $machineId=0, $analytics,$breakdownReasonArr)
    {
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState';
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState';
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $this->factory->select('stopT.notificationId, stopT.comment, stopT.timeDiff, stopT.reasonId' );  
            $this->factory->from($tableName.' as stopT');
            $this->factory->where('stopT.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('stopT.machineId', $machineId); 

            }
            $this->factory->where_not_in('stopT.comment', $breakdownReasonArr);
            $this->factory->where('stopT.comment !=', "");   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('stopT.type', $type); 
            $this->factory->where('stopT.isToIgnore', '0'); 
            
            if ($choose1 == "day") 
            {
                $this->factory->where('date(addedDate)', date('Y-m-d',strtotime($choose2))); 
            }
            else if ($choose1 == "weekly") 
            {
                $choose2 = explode("/", $choose2);
                $week = $choose2[0];
                $year = $choose2[1];

                $this->factory->where('WEEK(addedDate)', $week); 
                $this->factory->where('YEAR(addedDate)', $year); 
            }
            else if ($choose1 == "monthly") 
            {
                $choose2 = explode(" ", $choose2);
                $month = $choose2[0];
                $month = date('m',strtotime($choose2[0]));
                $year = $choose2[1];

                $this->factory->where('MONTH(addedDate)', $month); 
                $this->factory->where('YEAR(addedDate)', $year); 
            }
            else if ($choose1 == "yearly") 
            {
                $this->factory->where('YEAR(addedDate)', $choose2); 
            }

            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else 
        {
            return '';
        }
    }

    //In use
    public function getallBeta_GraphsLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        //$columnArr = array("betaDetectionLog.logId", "user.userName", "machine.machineName", "betaDetectionLog.color", "betaDetectionLog.accuracy", "betaDetectionLog.originalTime"); 
        $columnArr = array("betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime", "betaTable.originalTime");   
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        $userQ = ''; 
        $userStatusQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1   $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.logId desc  limit $start, $limit";   
           // echo $sql;exit;
            
        $query = $this->factory->query($sql); 
        return $query; 
    }

    public function getallBetaGraphLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }
        
        
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1    $machineQ $dateQ $userQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc";   
            
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    //In use
    public function getallBeta_GraphLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog WHERE 1=1 $machineQ $userQ";


        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime , color FROM betaDetectionLog where 1=1  $machineQ $userQ UNION ALL 
            SELECT machineId, insertTime as originalTime, machineStatus FROM machineStatusLog where 1=1  $machineStatusQ $userStatusQ)betaTable";

        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    //In use
    public function getSearchBeta_GraphLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and betaDetectionLog.machineId in (".$rowUser->machines.")";
                $userStatusQ = " and machineStatusLog.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
                $userStatusQ = " and 1 != 1 "; 
            }
        }

        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $dateStatusQ = " and insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        //$sql = "SELECT count(*) as totalCount FROM betaDetectionLog left join r_nytt_main.user on betaDetectionLog.userId = user.userId left join machine on betaDetectionLog.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery "; 

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime, color FROM betaDetectionLog where 1=1   $machineQ $dateQ $userQ UNION ALL 
            SELECT machineId, insertTime, machineStatus FROM machineStatusLog where 1=1 $machineStatusQ $dateStatusQ $userStatusQ)betaTable left join machine on betaTable.machineId = machine.machineId where 1=1 $searchQuery ";
        //echo $sql;exit;

        $query = $this->factory->query($sql); 
        return $query;  
    }

    function InsertRecordsLanguage($data)
    {
        $this->db->insert('translations',$data);
        return true;
    }

     //In use
    public function getAllChangeLanguageCount()
    {
        $sql = "SELECT count(*) as totalCount FROM translations";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getChangeLanguageCount($start, $limit, $type, $searchQuery)
    { 
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and language.type IN (".implode(",", $type).")";
        } 

        $sql = "SELECT count(*) as totalCount FROM translations WHERE 1=1 $typeQ $searchQuery order by languageId desc"; 
        //echo $sql;exit;
        $query = $this->db->query($sql); 
        return $query; 
    } 

    //In use
    public function getChangeLanguageText($start, $limit, $type, $searchQuery)
    {
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and language.type IN (".implode(",", $type).")";
        } 

        $sql = "SELECT * FROM translations WHERE 1=1 $typeQ $searchQuery order by englishText asc limit $start, $limit"; 
        $query = $this->db->query($sql); 
        
        return $query; 
    }

    public function getAllChangeLanguageCount_backup()
    {
        $sql = "SELECT count(*) as totalCount FROM translations";
        $query = $this->db->query($sql); 
        return $query;  
    }

    //In use
    public function getChangeLanguageCount_backup($start, $limit, $type, $searchQuery)
    { 
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and translations.type IN (".implode(",", $type).")";
        } 

        $sql = "SELECT count(*) as totalCount FROM translations WHERE 1=1 $typeQ $searchQuery order by languageId desc"; 
        // echo $sql;exit;
        $query = $this->db->query($sql); 
        return $query; 
    } 

    //In use
    public function getChangeLanguageText_backup($start, $limit, $type, $searchQuery, $columnName, $columnSortOrder)
    {
        $typeQ = '';
        if(!empty($type)) 
        {
            $typeQ = "and translations.type IN (".implode(",", $type).")";
        } 

       /* if ($columnName != "languageId") 
        {
            $columnName = "englishText";
            $columnSortOrder = "asc";
        }*/

        $sql = "SELECT * FROM translations WHERE 1=1 $typeQ $searchQuery order by $columnName $columnSortOrder limit $start, $limit"; 
        $query = $this->db->query($sql); 
        
        return $query; 
    }

    function getLastFinishOrderTime($machineId)
    {
        $this->factory->where('machineId',$machineId);
        $this->factory->order_by('machinePartsFinishLogId','desc');
        $query = $this->factory->get('machinePartsFinish')->row();

        return $query;
    }

    function getLastProductionTimeEntry($machinePartsId)
    {
        $this->factory->where('machinePartsId',$machinePartsId);
        $this->factory->where('machinePartsStatus',"0");
        $this->factory->order_by('machinePartsLogId','asc');
        $query = $this->factory->get('machinePartsLog')->row();

        return $query;
    }


    public function getStopAndWaitReasonOldData($machineId=0, $analytics, $comment)
    {
        if ($analytics == "waitAnalytics") 
        {
            $tableName = $machineId.'waitingStateBetalogWithState';
            $type = 'Wait';
        }
        else
        {
            $tableName = $machineId.'stoppedStateBetalogWithState';
            $type = 'Stop';
        }
        if($this->factory->table_exists($tableName) )
        {  
            $this->factory->select('stopT.notificationId, stopT.comment, stopT.timeDiff, stopT.reasonId' );  
            $this->factory->from($tableName.' as stopT');
            $this->factory->where('stopT.commentFlag', '1');
            if($machineId > 0) 
            {
                $this->factory->where('stopT.machineId', $machineId); 
            } 
            $this->factory->where('stopT.type', $type); 
            $this->factory->where('stopT.comment', $comment); 
            $this->factory->order_by('notificationId','desc');
            $query= $this->factory->get(); 
            $result = $query->row();
            return $result;
        } 
        else 
        {
            return '';
        }
    }

    function checkStateTransform($betweenStartTime,$betweenEndTime,$timeRangeStartTime,$timeRangeEndTime,$machineId,$choose1)
    {
        $startDate = date('Y-m-d',strtotime($timeRangeStartTime));
        $endDate = date('Y-m-d',strtotime($timeRangeEndTime));
        $startDateMonth = date('m',strtotime($timeRangeStartTime));
        $endDateMonth = date('m',strtotime($timeRangeEndTime));
        $startDateYear = date('Y',strtotime($timeRangeStartTime));
        $endDateYear = date('Y',strtotime($timeRangeEndTime));

         $weekFirstDay = date('l',strtotime($timeRangeStartTime));
        if ($weekFirstDay == "Monday") 
        {
            $startDateWeek = date('Y-m-d',strtotime($timeRangeStartTime));
        }else
        {
            $startDateWeek = date('Y-m-d',strtotime($timeRangeStartTime." last monday"));
        }
        
        

        $weekEndDay = date('l',strtotime($timeRangeEndTime));
        if ($weekEndDay == "Sunday") 
        {
            $endDateWeek = date('Y-m-d',strtotime($timeRangeEndTime));
        }else
        {
            $endDateWeek = date('Y-m-d',strtotime($timeRangeEndTime." next sunday"));
        }

        if ($choose1 == "yearly") 
        {
             $sql = "SELECT * FROM customMachineState
                WHERE ((date(timeRangeStartTime) = '$startDate' AND  date(timeRangeEndTime) = '$endDate' AND choose1 = 'day' ) or (year(timeRangeStartTime) = '$startDateYear' AND  year(timeRangeEndTime) = '$endDateYear') or (month(timeRangeStartTime) = '$startDateMonth' AND  month(timeRangeEndTime) = '$endDateMonth'  AND choose1 = 'monthly') or (date(timeRangeStartTime) = '$startDateWeek' AND  date(timeRangeEndTime) = '$endDateWeek' AND choose1 = 'weekly') )
                AND ((betweenStartTime >= $betweenStartTime AND betweenStartTime < $betweenEndTime) or (betweenEndTime > $betweenStartTime AND betweenEndTime < $betweenEndTime))
                AND isDelete = '0'
                AND machineId = ".$machineId;    
        }else
        {
             $sql = "SELECT * FROM customMachineState
                WHERE ((date(timeRangeStartTime) = '$startDate' AND  date(timeRangeEndTime) = '$endDate' AND choose1 = 'day' ) or (year(timeRangeStartTime) = '$startDateYear' AND  year(timeRangeEndTime) = '$endDateYear'  AND choose1 = 'yearly') or (month(timeRangeStartTime) = '$startDateMonth' AND  month(timeRangeEndTime) = '$endDateMonth'  AND choose1 = 'monthly') or (date(timeRangeStartTime) = '$startDateWeek' AND  date(timeRangeEndTime) = '$endDateWeek' AND choose1 = 'weekly') )
                AND ((betweenStartTime >= $betweenStartTime AND betweenStartTime < $betweenEndTime) or (betweenEndTime > $betweenStartTime AND betweenEndTime < $betweenEndTime))
                AND isDelete = '0'
                AND machineId = ".$machineId; 
        }
       
        //echo $sql;exit;
        $query = $this->factory->query($sql)->result_array(); 
        return $query; 
    } 

    function getByFilterStateTransform($timeRangeStartTime,$timeRangeEndTime,$machineId,$choose1)
    {
        $startDate = date('Y-m-d',strtotime($timeRangeStartTime));
        $endDate = date('Y-m-d',strtotime($timeRangeEndTime));
        $startDateMonth = date('m',strtotime($timeRangeStartTime));
        $endDateMonth = date('m',strtotime($timeRangeEndTime));
        $startDateYear = date('Y',strtotime($timeRangeStartTime));
        $endDateYear = date('Y',strtotime($timeRangeEndTime));
        $weekFirstDay = date('l',strtotime($timeRangeStartTime));
        if ($weekFirstDay == "Monday") 
        {
            $startDateWeek = date('Y-m-d',strtotime($timeRangeStartTime));
        }else
        {
            $startDateWeek = date('Y-m-d',strtotime($timeRangeStartTime." last monday"));
        }
        
        

        $weekEndDay = date('l',strtotime($timeRangeEndTime));
        if ($weekEndDay == "Sunday") 
        {
            $endDateWeek = date('Y-m-d',strtotime($timeRangeEndTime));
        }else
        {
            $endDateWeek = date('Y-m-d',strtotime($timeRangeEndTime." next sunday"));
        }

        if ($choose1 == "yearly") 
        {
            $sql = "SELECT * FROM customMachineState
                WHERE ((date(timeRangeStartTime) = '$startDate' AND  date(timeRangeEndTime) = '$endDate' AND choose1 = 'day' ) or (year(timeRangeStartTime) = '$startDateYear' AND  year(timeRangeEndTime) = '$endDateYear') or (month(timeRangeStartTime) = '$startDateMonth' AND  month(timeRangeEndTime) = '$endDateMonth'  AND choose1 = 'monthly') or (date(timeRangeStartTime) = '$startDateWeek' AND  date(timeRangeEndTime) = '$endDateWeek' AND choose1 = 'weekly') )
                AND isDelete = '0'
                AND machineId = ".$machineId; 
        }else
        {
            $sql = "SELECT * FROM customMachineState
                WHERE ((date(timeRangeStartTime) = '$startDate' AND  date(timeRangeEndTime) = '$endDate' AND choose1 = 'day' ) or (year(timeRangeStartTime) = '$startDateYear' AND  year(timeRangeEndTime) = '$endDateYear'  AND choose1 = 'yearly') or (month(timeRangeStartTime) = '$startDateMonth' AND  month(timeRangeEndTime) = '$endDateMonth'  AND choose1 = 'monthly') or (date(timeRangeStartTime) = '$startDateWeek' AND  date(timeRangeEndTime) = '$endDateWeek' AND choose1 = 'weekly') )
                AND isDelete = '0'
                AND machineId = ".$machineId; 
        }
        
        //echo $sql;exit;
        $query = $this->factory->query($sql)->result_array(); 
        return $query; 
    } 

    public function getTaskMaintenaceL($date,$machineId,$type,$searchTaskText)
    {
        $compareDateCheck = $date;
        $compareWeekCheck = date('W',strtotime($date));
        $compareMonthCheck = date('m',strtotime($date));
        $compareYearCheck = date('Y',strtotime($date));

        $currentDate = date('Y-m-d');
        $dayCheck = date('l',strtotime($currentDate));

        if ($dayCheck == "Saturday") 
        {
            $currentDate = date('Y-m-d',strtotime("+2 day"));
        }
        else if ($dayCheck == "Sunday") 
        {
            $currentDate = date('Y-m-d',strtotime("+1 day"));
        }

        $currentWeek = date('W',strtotime($currentDate));
        $currentMonth = date('m',strtotime($currentDate));
        $currentYear = date('Y',strtotime($currentDate));

       

        $dateQ = '';
        $dateQuery = "";
        
        if($type == "everyWeek") 
        {
            $day = date('l',strtotime($date));

            if ($day == "Monday") 
            {
                $startDate = date('Y-m-d',strtotime($date));
            }else
            {
                $startDate = date('Y-m-d',strtotime($date." last monday"));
            }

            if ($day == "Sunday") 
            {
                $endDate = date('Y-m-d',strtotime($date));
            }else
            {
                $endDate = date('Y-m-d',strtotime($date." next sunday"));
            }

            if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
            {
                $dateQ = "and date(taskMaintenace.dueDate) >= '$startDate' and date(taskMaintenace.dueDate) <= '$endDate' and `repeat` = 'everyWeek'";
                $dateQuery = "and date(dueDate) >= '$date'";
            }else
            {
                $dateQ = "and mainTaskId = 0 and `repeat` = 'everyWeek'";
            }
        }
        else if($type == "everyMonth") 
        {
            if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
            {
                $dateQ = "and MONTH(taskMaintenace.dueDate) = '". date('m',strtotime($date)) ."' and date(taskMaintenace.dueDate) >= '$date' and `repeat` = 'everyMonth'";
                $dateQuery = "and date(dueDate) >= '$date'";
            }else
            {
                $dateQ = "and mainTaskId = 0 and `repeat` = 'everyMonth'";
            }
        }
        else if ($type == "everyYear") 
        {
            if ($currentYear >=  $compareYearCheck) 
            {
                $dateQ = "and YEAR(taskMaintenace.dueDate) = '". date('Y',strtotime($date)) ."' and date(taskMaintenace.dueDate) >= '$date' and `repeat` = 'everyYear'";
                $dateQuery = "and date(dueDate) >= '$date'";
            }else
            {
                $dateQ = "and mainTaskId = 0 and `repeat` = 'everyYear'";
            }
        }

        
        $machineIdQ = '';
        if(!empty($machineId) && $machineId != "all") 
        {
            $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
        }

        $searchTaskTextQ = "";
        if (!empty($searchTaskText)) 
        {
            $searchTaskTextQ = "and taskMaintenace.task  LIKE '%$searchTaskText%'";
        }

        $select = "taskMaintenace.*,
                   machine.machineName
                   ";

        
         $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' and machine.isDeleted = '0' $dateQuery $dateQ $machineIdQ  and ((endTaskDate >=  '$date') or (endTaskDate  IS NULL)) $searchTaskTextQ order by status desc,taskId desc";  

        $query = $this->factory->query($sql)->result_array();
        if ($type == "everyWeek") {
           // echo  $sql;exit;
        }
        
        return $query; 
    }

    public function getTaskData($date,$machineId,$type,$repeatType,$searchTaskText)
    {
        
        $select = "taskMaintenace.*, machine.machineName";

        $machineIdQ = "and taskMaintenace.machineId = '$machineId'";
       

        if ($type == "today") 
        {

            if ($date > date('Y-m-d')) 
            {
                $dateQ = "and mainTaskId = 0 and `repeat` = '$repeatType'";
                $dateQuery = "";
            }else
            {
                $dateQ = "and `repeat` = '$repeatType'";
                $dateQuery = "and date(dueDate) >= '$date'";
            }
        }else if ($type == "month") 
        {
            $startDate = date('Y-m-d',strtotime($date));
            $endDate = date('Y-m-d',strtotime($date." +7 days"));
            $dateQ = "and date(taskMaintenace.dueDate) > '$startDate' and date(taskMaintenace.dueDate) < '$endDate' and `repeat` = 'everyMonth'";
            $dateQuery = "and date(dueDate) >= '$date'";
        }else if ($type == "year") 
        {
            $startDate = date('Y-m-d',strtotime($date));
            $endDate = date('Y-m-d',strtotime($date." +1 months"));
            $dateQ = "and date(taskMaintenace.dueDate) > '$startDate' and date(taskMaintenace.dueDate) < '$endDate' and `repeat` = 'everyYear'";
            $dateQuery = "and date(dueDate) >= '$date'";
        }

        $searchTaskTextQ = "";
        if (!empty($searchTaskText)) 
        {
            $searchTaskTextQ = "and taskMaintenace.task  LIKE '%$searchTaskText%'";
        }

        
        $sql = "SELECT $select FROM taskMaintenace left join machine on machine.machineId = taskMaintenace.machineId WHERE isDelete = '0' and machine.isDeleted = '0' $dateQuery  $dateQ $machineIdQ and ((endTaskDate >=  '$date') or (endTaskDate  IS NULL)) $searchTaskTextQ order by status desc,taskId desc";  
         
        //echo $sql;exit;
        $query = $this->factory->query($sql)->result_array();
        return $query; 
    }

    public function getAllSetAppErrANDLogCount()
    {
        $sql = "SELECT count(*) as totalCount FROM setAppErrorLog where factoryId = ".$this->factoryId;
        $query = $this->db->query($sql); 
        return $query;  
    }


    public function getSearchSetAppErrorLogCount($machineId,$dueDateValS,$dueDateValE,$searchQuery)
    { 
        $dueDateQ = "";
        if (!empty($dueDateValS) && !empty($dueDateValE) && $dueDateValS != "2013-01-12") 
        {
            $dueDateQ = "and setAppErrorLog.logTime >= '".strtotime($dueDateValS .'00:00:00')."' and setAppErrorLog.logTime <= '".strtotime($dueDateValE .'23:59:59')."'";
            $mainTaskQ = "";
        }

        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and setAppErrorLog.machineId IN (".implode(",", $machineId).")";
        }

        // $sql = "SELECT count(*) as totalCount FROM translations WHERE 1=1 $typeQ $searchQuery order by languageId desc"; 
        
        $sql = "SELECT count(*) as totalCount FROM setAppErrorLog WHERE 1=1 $dueDateQ $machineIdQ $searchQuery order by id desc"; 
        $query = $this->db->query($sql); 
        // echo $sql;exit;
        return $query; 

    }


    public function getAllSetAppErrorLogLogs($start, $limit,$machineId,$dueDateValS,$dueDateValE,$searchQuery)
    {
        $dueDateQ = "";
        if (!empty($dueDateValS) && !empty($dueDateValE) && $dueDateValS != "2013-01-12") 
        {
            $dueDateQ = "and setAppErrorLog.logTime >= '".strtotime($dueDateValS .'00:00:00')."' and setAppErrorLog.logTime <= '".strtotime($dueDateValE .'23:59:59')."'";
            $mainTaskQ = "";
        }
        
        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and setAppErrorLog.machineId IN (".implode(",", $machineId).")";
        }


        $sql = "SELECT * FROM setAppErrorLog WHERE 1=1 $dueDateQ  $machineIdQ  $searchQuery order by id desc limit $start, $limit ";

        // $sql = "SELECT setAppErrorLog.* FROM setAppErrorLog WHERE setAppErrorLog.factoryId = ".$this->factoryId." $dueDateQ  $machineIdQ  $searchQuery order by id desc limit $start, $limit "; 
        $query = $this->db->query($sql);
        // echo $sql;exit;
        return $query; 
    }

    public function getAllMtConnectLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and mtConnect.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime , State FROM mtConnect where 1=1  $machineQ UNION ALL 
            SELECT machineId, insertTime as originalTime, machineStatus FROM machineStatusLog where 1=1  $machineStatusQ )betaTable";
       // $sql = "SELECT count(*) as totalCount FROM mtConnect WHERE 1=1 $machineQ $machineStatusQ";

        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchMtConnectLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and mtConnect.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        //$sql = "SELECT count(*) as totalCount FROM mtConnect left join machine on mtConnect.machineId = machine.machineId WHERE 1=1 $dateQ  $machineQ $searchQuery "; 

         $sql = "SELECT count(*) as totalCount FROM
            (SELECT mtConnectId, machineId, originalTime, State FROM mtConnect where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery";
        $query = $this->factory->query($sql); 
        return $query;  
    }


    public function getallMtConnectLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and mtConnect.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and mtConnect.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT * FROM
            (SELECT mtConnectId, machineId, originalTime, State FROM mtConnect where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.mtConnectId desc  limit $start, $limit";
            //echo  $sql;exit;
       // $sql = "SELECT mtConnect.*,machine.machineName FROM mtConnect left join machine on mtConnect.machineId = machine.machineId WHERE 1=1 $dateQ $machineQ $searchQuery  order by mtConnectId desc  limit $start, $limit";         
        $query = $this->factory->query($sql); 
        return $query; 
    }

    public function getAllMtconnectLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and mtConnect.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and mtConnect.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  

       // $sql = "SELECT mtConnect.*,machine.machineName FROM mtConnect left join machine on mtConnect.machineId = machine.machineId WHERE 1=1 $dateQ $searchQuery $machineQ  order by mtConnectId desc  limit 1, 1000";   
        $sql = "SELECT * FROM
            (SELECT mtConnectId, machineId, originalTime, State FROM mtConnect where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.mtConnectId desc limit  0, 1000";
        $query = $this->factory->query($sql); 
        return $query; 
    }

    public function getAllVirtualMachineBLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and virtualMachineLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";
        }

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT machineId, originalTime , signalType FROM virtualMachineLog where 1=1  $machineQ UNION ALL 
            SELECT machineId, insertTime as originalTime, machineStatus FROM machineStatusLog where 1=1  $machineStatusQ )betaTable";

        $query = $this->factory->query($sql); 
        return $query;  
    }

    //In use
    public function getSearchVirtualMachineBLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and virtualMachineLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT count(*) as totalCount FROM
            (SELECT logId, machineId, originalTime, signalType FROM virtualMachineLog where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery";
        $query = $this->factory->query($sql); 
        return $query;  
    }


    public function getallVirtualMachineBLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and virtualMachineLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and virtualMachineLog.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 

        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, signalType, 'vMachine' as signalTypeStatus  FROM virtualMachineLog where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus, 'sMachine' as signalTypeStatus  FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.logId desc  limit $start, $limit";       
        //echo $sql;exit; 
        $query = $this->factory->query($sql); 
        return $query; 
    }

    public function getAllVirtualMachineBLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)
    {
        $machineQ = '';
        $machineStatusQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and virtualMachineLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";  
        }

      
        $dateQ = " and virtualMachineLog.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $dateStatusQ = " and machineStatusLog.insertTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
 
        $sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, signalType, 'vMachine' as signalTypeStatus FROM virtualMachineLog where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus, 'sMachine' as signalTypeStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ )betaTable  left join machine on betaTable.machineId = machine.machineId where 1 = 1 $searchQuery order by betaTable.originalTime desc, betaTable.logId desc limit  0, 1000";
            //echo $sql;exit;
        $query = $this->factory->query($sql); 
        return $query; 
    }


    // public function SetAppErrorLogDetailsList($machineId)
    // {
    //     $factory->select('*');
    //     $factory->from('setAppErrorLog');
    //     $factory->where('machineId', $machineId);
    //     $query= $factory->get();
    //     return $query;


    //     // $query = $this->factory->query("select * from setAppErrorLog where machineId");
    //     // return $query->result();
    // }

      //In use

    public function SetAppErrorLogDetailsList($machineId)
    {

        $factory->select('*');
        $factory->from('setAppErrorLog');
        $factory->where('machineId', $machineId);
        $query= $factory->get();
        return $query;


        // $query = $this->factory->where('machineId',$machineId)->get('setAppErrorLog');
        // return $query->result();
    } 

    public function getAllAlphaImageLogCount()
    {
        $sql = "SELECT count(*) as totalCount FROM tempLogImageFrame";
        $query = $this->factory->query($sql); 
        return $query;  
    }


    public function getSearchAlphaImageLogCount($machineId,$searchQuery)
    { 
        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and tempLogImageFrame.machineId IN (".implode(",", $machineId).")";
        }
        
        $sql = "SELECT count(*) as totalCount FROM tempLogImageFrame left join machine on tempLogImageFrame.machineId = machine.machineId WHERE machine.isDeleted = '0' $dueDateQ $machineIdQ $searchQuery order by logId desc"; 
        $query = $this->factory->query($sql); 
        return $query; 

    }


    public function getAllAlphaImageLogLogs($start, $limit,$machineId,$searchQuery)
    {
        $machineIdQ = '';
        if(!empty($machineId)) 
        {
            $machineIdQ = "and tempLogImageFrame.machineId IN (".implode(",", $machineId).")";
        }


        $sql = "SELECT * FROM tempLogImageFrame left join machine on tempLogImageFrame.machineId = machine.machineId WHERE machine.isDeleted = '0' $machineIdQ  $searchQuery order by logId desc limit $start, $limit ";

        $query = $this->factory->query($sql);
        return $query; 
    }

}   
?>

