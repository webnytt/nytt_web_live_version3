<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Analytics_model extends CI_Model
{ 
    public function validate()
    {
        $username = $this->security->xss_clean($this->input->post('username'));
        $password = $this->security->xss_clean($this->input->post('password'));
        $this->db->where('userName', $username);
        $this->db->where('userPassword', $password); 
        $this->db->where('isDeleted', '0'); 
        $query = $this->db->get('user');
        if($query->num_rows() == 1) {
            $row = $query->row();
            $data = array(
                    'userId' => $row->userId,
                    'userName' => $row->userName,
                    'userImage' => $row->userImage,
                    'role' => $row->userRole,
                    'validated' => true,
                    'url3'=>'version2',
                    'userTheme'=> $row->theme, 
                    );
            if($row->userRole < 2 ) {
                $data['factoryId'] = $row->factoryId; 
            }
            $this->session->set_userdata($data);
            
            if($row->userRole >= 2 ) {
                redirect('admin/selectFactory');
            } 
            return true;
        }
        return false;
    }

    function getUserPassword($userName) 
    {
        if($userName != '' ) 
        {
            $sql = "SELECT userPassword from user where userName = '".$userName."' and isDeleted  = '0'";    
            $query = $this->db->query($sql);
            if ($query->num_rows() > 0) 
            {
                $users = $query->row_array();   
                return $users['userPassword'];
            } 
            else 
            {
                return false;
            }
        } 
        else 
        {
            return false;
        }
    }
    
    public function check_isvalidated()
    {
        if($this->session->userdata('validated') && $this->session->userdata('url3') == 'version2' && $this->session->userdata('factoryId') != '') 
        {   
            return true;
        } 
        else 
        {
            return false;
        }
    }
    
    public function check_isadmin()
    {
        if($this->session->userdata('role') == '1')
        { 
            return true;
        } 
        else 
        {
            return false;
        }
    }

    public function machineExists($factory, $name, $machineId=0)
    { 
        $factory->select('*');
        $factory->from('machine');
        $factory->where('machineName', $name);
        if($machineId != 0) 
        {
            $factory->where('machineId != ', $machineId); 
        }
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        $row = $query->row();
        return $row;  
    }
    
    public function checkUserRole()
    {
        return (int)$this->session->userdata('role') ; 
    }
    
    public function checkUserFactory()
    {
        return (int)$this->session->userdata('factoryId');   
        
    }
    
    public function checkUserRoleName()
    {
        if($this->session->userdata('role') == '0') 
        {
            $roleName = 'Operator';
        } 
        else if($this->session->userdata('role') == '1') 
        {
            $roleName = 'Factory Manager';
        } 
        else if($this->session->userdata('role') == '2') 
        {
            $roleName = 'Administrator';
        } 
        else if($this->session->userdata('role') == '3') 
        {
            $roleName = 'Developer';
        }
        return $roleName;
    }
    
    public function do_logout()
    {
        $this->session->sess_destroy();
        redirect('admin/login'); 
    }
    
    public function getUserMachines($userId)
    {
        $this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }   
    public function getallMachines($factory)
    {
        $factory->select('*');
        $factory->from('machine');
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        return $query;
    }
    public function getAssignedMachines($factory, $userId)
    {
        $this->db->select('machines');
        $this->db->from('user');
        $this->db->where('userId', $userId);
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        $rowUser = $query->row();
        
        if($rowUser->machines != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId in (".$rowUser->machines.") AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine WHERE 1 != 1"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getMachineDetail($factory, $machineId)
    {
        if($machineId != '') 
        {
            $sql = "SELECT * FROM machine WHERE machineId = ".$machineId." AND isDeleted = '0'";   
        } 
        else 
        {
            $sql = "SELECT * FROM machine WHERE 1 != 1"; 
        }
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getallMachineStates()
    {
        $this->db->select('machineId');
        $this->db->from('machine');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $response = $query->result();
        $machines = $response[0]->machineId;
        for($x=1;$x<count($response);$x++) 
        {
            $machines .= ','.$response[$x]->machineId; 
        }
        
        $sql = "SELECT machine.*, temp_logimagecolor.* FROM temp_logimagecolor left join machine on machine.machineId = temp_logimagecolor.machineId WHERE temp_logimagecolor.machineId in (".$machines.") and temp_logimagecolor.logImageId in ( select MAX(logImageId) from temp_logimagecolor group by machineId ) order by temp_logimagecolor.machineId asc ";
        $query = $this->db->query($sql); 
        $response = $query->result();
        return $response;
    }
    
    public function getAssignedMachineStates($factory, $userId)
    {
        $factory->select('machines');
        $factory->from('user');
        $factory->where('userId', $userId);
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        $rowUser = $query->row();
        
        $sql = "SELECT machine.*, temp_logimagecolor.* FROM temp_logimagecolor left join machine on machine.machineId = temp_logimagecolor.machineId WHERE temp_logimagecolor.machineId in (".$rowUser->machines.") and temp_logimagecolor.logImageId in ( select MAX(logImageId) from temp_logimagecolor group by machineId ) order by temp_logimagecolor.machineId asc ";
        $query = $factory->query($sql); 
        $response = $query->result();
        return $response; 
    }  
    
    public function getallMachinesGreenColorData($factory, $duration)
    {
        $factory->select('machine.machineId, sum(green.timeDiff) as totalGreen' );
        $factory->from('machine');
        $factory->where('machine.isDeleted', '0');
        
        $factory->join('temp_logimagecolor as green','green.machineId = machine.machineId');  
        $factory->where('green.greenStatus', '1');
        $factory->where('green.yellowStatus', '0');
        $factory->where('green.redStatus', '0');
        $factory->where('green.whiteStatus', '0');
        
        $factory->group_by('machine.machineId'); 
        
        if($duration > 0 ) 
        { 
            $end_date = date('Y-m-d H:i:s');
            $start_date = date('Y-m-d H:i:s', strtotime($end_date . ' -'.$duration.' day'));  
            $factory->where('logTime >=', $start_date); 
            $factory->where('logTime <=', $end_date);
        }
        $query= $factory->get();
        $result = $query->result();
        return $result;
    }
    
    public function getallMachinesYellowColorData($factory, $duration)
    {
        $factory->select('machine.machineId, sum(yellow.timeDiff) as totalYellow' );
        $factory->from('machine');
        $factory->where('machine.isDeleted', '0');
        
        $factory->join('temp_logimagecolor as yellow','yellow.machineId = machine.machineId'); 
        $factory->where("(yellow.yellowStatus = '1' or yellow.whiteStatus = '1') ", null);
        $factory->where('yellow.greenStatus', '0');
        $factory->where('yellow.redStatus', '0');
        
        $factory->group_by('machine.machineId');
        $query= $factory->get();
        $result = $query->result();
        return $result;
    }
    
    public function getallMachinesRedColorData($factory, $duration)
    {
        $factory->select('machine.machineId, sum(red.timeDiff) as totalRed' );
        $factory->from('machine');
        $factory->where('machine.isDeleted', '0');
        $factory->join('temp_logimagecolor as red','red.machineId = machine.machineId'); 
        $factory->where('red.greenStatus', '0');
        $factory->where('red.yellowStatus', '0');
        $factory->where('red.redStatus', '1');
        $factory->where('red.whiteStatus', '0');
        
        $factory->group_by('machine.machineId'); 
        $query= $factory->get();
        $result = $query->result();
        return $result;
    }
    
    public function machineCodeExists($factory, $code, $machineId=0)
    { 
        $factory->select('*');
        $factory->from('machine');
        $factory->where('machineNumber', $code);
        if($machineId != 0) {
            $factory->where('machineId != ', $machineId); 
        }
        $factory->where('isDeleted', '0');
        $query= $factory->get();
        $row = $query->row();
        return $row;  
    }
    
    public function newMachine($factory, $insert)
    { 
        $factory->insert('machine',$insert);
        return $factory->insert_id();
    }
    
    public function updateMachine($factory, $update, $machineId)
    { 
        $factory->where('machineId',$machineId);
        $factory->update('machine',$update);
    } 
    public function deleteMachine($factory, $machineId)
    { 
        $factory->where('machineId',$machineId);
        $update = array('isDeleted'=>'1');
        $factory->update('machine',$update); 
    } 
    public function unlockMachine($factory, $machineId)
    { 
        $factory->where('machineId',$machineId);
        $update = array('machineSelected'=>'0');
        $factory->update('machine',$update); 
    } 
    public function getallFactories()
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    public function newFactory($insert)
    { 
        $this->db->insert('factory',$insert);
        return $this->db->insert_id();
    } 
    public function updateFactory($update, $factoryId)
    { 
        $this->db->where('factoryId',$factoryId);
        $this->db->update('factory',$update); 
    } 
    public function deleteFactory($factoryId)
    { 
        $this->db->where('factoryId',$factoryId);
        $update = array('isDeleted'=>'1');
        $this->db->update('factory',$update); 
    } 
    public function getallOperators($factoryId)
    {
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('isDeleted', '0');
        $this->db->where('userRole', '0');
        $this->db->where('factoryId', $factoryId); 
        $query= $this->db->get();
        return $query;
    }
    public function userExists($userName, $userId=0)
    { 
        $this->db->select('*');
        $this->db->from('user');
        $this->db->where('userName', $userName);
        if($userId != 0) {
            $this->db->where('userId != ', $userId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    public function isValidCurrentPassword($pass, $userId)
    { 
        $this->db->select('userId');
        $this->db->from('user');
        $this->db->where('userPassword', $pass);
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    
    public function newUser($insert)
    { 
        $this->db->insert('user',$insert);
        return $this->db->insert_id();
        
    } 
    public function updateUser($update, $userId)
    { 
        $this->db->where('userId',$userId);
        $this->db->update('user',$update); 
    } 
    
    public function updateColor($factory, $update, $logImageId)
    { 
        $factory->where('logImageId',$logImageId);
        $factory->update('temp_logimagecolor',$update);
    } 
    public function deleteUser($userId)
    { 
        $this->db->where('userId',$userId);
        $update = array('isDeleted'=>'1');
        $this->db->update('user',$update); 
    } 
    function lastUserId() 
    {
       $this->db->select('max(userId) as maxId'); 
       $this->db->from('user'); 
       $va=$this->db->get()->row();
       return $va; 
    } 
    public function updateLog($factory, $update, $logImageId)
    { 
        $factory->where('logImageId',$logImageId); 
        $factory->update('temp_logimagecolor',$update);
    } 
    public function getAllColorLogsCount($factory, $machineId, $is_admin=1)
    {
       $machineQ = '';
        if($machineId > 0 ) {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } else {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1=1 $machineQ $userQ";
        $query = $factory->query($sql); 
        return $query; 
    }

    public function getSearchColorLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } else {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery ";  
        $query = $factory->query($sql); 
        return $query; 
    }
        
    public function getSearchColorLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("temp_logimagecolor.logImageId", "temp_logimagecolor.logId", "temp_logimageframe.userId", "temp_logimageframe.imageName", '', "temp_logimagecolor.clientComment", "temp_logimagecolor.originalColor", "temp_logimageframe.logTime", "temp_logimagecolor.isUnique");
        $orderCol = $columnArr[$columnIndex]; 
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT temp_logimagecolor.*, temp_logimageframe.*, client.userName as clientName, user.userName as originalColorUserName FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId left join nytt_main.user as client on temp_logimagecolor.commentClientId = client.userId left join nytt_main.user on temp_logimagecolor.originalColorId = user.userId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $factory->query($sql); 
        return $query; 
    }
    public function getColorLogs($factory, $machineId, $is_admin=1, $limit, $start=0)
    { 
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";
        } 
        else 
        {
            $machineQ = ""; 
        }
        
        $userQ = "";
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId'));
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $userQ = "and temp_logimagecolor.machineId in (".$rowUser->machines.") ";   
            } 
            else 
            {
                $userQ = "and 1 != 1"; 
            }
        }
            
        $sql = "SELECT temp_logimagecolor.logImageId, temp_logimagecolor.img_dir, temp_logimageframe.logId, temp_logimageframe.userId, temp_logimageframe.imageName, temp_logimageframe.logTime FROM temp_logimagecolor join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1 = 1 $machineQ $userQ order by temp_logimageframe.logId desc LIMIT $start, $limit";  
        $queryLog = $factory->query($sql); 
        return $queryLog; 
    }
    
    public function getColorLogsCount($factory, $machineId, $is_admin=1)
    { 
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";
        } 
        else 
        {
            $machineQ = ""; 
        }
        
        $userQ = "";
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId'));
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $userQ = "and temp_logimagecolor.machineId in (".$rowUser->machines.") ";   
            } 
            else 
            {
                $userQ = "and 1 != 1"; 
            }
        }
            
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1 = 1 $machineQ $userQ";  
        $queryLog = $factory->query($sql); 
        return $queryLog;  
    }
    
    public function getImageLogs($factory, $machineId, $is_admin=1, $limit, $start=0)
    { 
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimageframe.machineId = $machineId";
        } 
        else 
        {
            $machineQ = "";
        }
        
        $userQ = "";
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId'));
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '') 
            {
                $userQ = "and temp_logimageframe.machineId in (".$rowUser->machines.") ";   
            } 
            else 
            {
                $userQ = "and 1 != 1"; 
            }
        }
            
        $sql = "SELECT  temp_logimageframe.* FROM temp_logimageframe WHERE 1 = 1 $machineQ $userQ and temp_logimageframe.logId not in ( SELECT logId from temp_logimagecolor ) order by temp_logimageframe.logId desc LIMIT $start, $limit";  
        $queryLog = $factory->query($sql); 
        return $queryLog; 
    }
    
    public function getImageLogsCount($factory, $machineId, $is_admin=1)
    { 
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimageframe.machineId = $machineId";
        } 
        else 
        {
            $machineQ = ""; 
        }
        
        $userQ = "";
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId'));
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $userQ = "and temp_logimageframe.machineId in (".$rowUser->machines.") ";   
            } 
            else 
            {
                $userQ = "and 1 != 1"; 
            }
        }
            
        $sql = "SELECT count(*) as totalCount FROM temp_logimageframe WHERE 1 = 1 $machineQ $userQ";  
        $queryLog = $factory->query($sql); 
       
       return $queryLog; 
    }
    
    public function addMaintananceSteps($userId, $machineId) 
    {
        $sql = "INSERT mSteps (userId, machineId, stepText, categoryId, stepOrder ) SELECT ".$userId.", ".$machineId.", mTCText, mTCCategory, mTCOrder FROM maintenanceTemplateContent WHERE maintenanceTemplateContent.mTemplateId = 1 and maintenanceTemplateContent.isDeleted = '0' ";    
        $queryLog = $this->db->query($sql);  
        return $queryLog; 
    }
    
    public function addMachineMaintananceSteps($factoryId, $machineId, $mTemplateId) 
    { 
        $sql = "INSERT mSteps (factoryId, machineId, stepText, categoryId, stepOrder ) SELECT ".$factoryId.",".$machineId.", mTCText, mTCCategory, mTCOrder FROM maintenanceTemplateContent WHERE maintenanceTemplateContent.mTemplateId = 1 and maintenanceTemplateContent.isDeleted = '0' ";    
        $queryLog = $this->db->query($sql);  
        return $queryLog; 
    }
    
    public function removeMaintananceSteps($factoryId, $userId, $machineId) 
    {
        $this->db->where('machineId',$machineId);
        $this->db->where('userId',$userId);
        $this->db->where('factoryId',$factoryId);
        $update = array('isDeleted'=>'1');
        $this->db->update('mSteps',$update);  
        return true;  
    }
    
    public function getMCategories()
    {
        $this->db->select('*');
        $this->db->from('maintenanceTemplateCategory'); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    
    
    public function getallColorLogs_2($dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("temp_logimagecolor_2.logImageId", "temp_logimagecolor_2.logId", "temp_logimageframe.userId", "temp_logimageframe.imageName", '', "temp_logimagecolor_2.clientComment", "temp_logimagecolor_2.originalColor", "temp_logimageframe.logTime", "temp_logimagecolor_2.isUnique");
        $orderCol = $columnArr[$columnIndex]; 
        
        $machineQ = '';
        if($machineId > 0 ) {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } else {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT temp_logimagecolor_2.*, temp_logimageframe.*, client.userName as clientName, user.userName as originalColorUserName FROM temp_logimagecolor_2 left join temp_logimageframe on temp_logimagecolor_2.logId = temp_logimageframe.logId left join user as client on temp_logimagecolor_2.commentClientId = client.userId left join user on temp_logimagecolor_2.originalColorId = user.userId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $this->db->query($sql); 
         return $query; 
    }
    
    public function getallColorLogsCount_2($machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor_2 left join temp_logimageframe on temp_logimagecolor_2.logId = temp_logimageframe.logId WHERE 1=1 $machineQ $userQ";
        $query = $this->db->query($sql); 
       return $query; 
    }
    
    public function getSearchColorLogsCount_2($dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor_2 left join temp_logimageframe on temp_logimagecolor_2.logId = temp_logimageframe.logId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery ";  
        $query = $this->db->query($sql); 
        return $query; 
    }
    
    public function get_c_d_test($machineId)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_1.machineId = $machineId";  
        }
        $sql = "SELECT temp_logimagecolor_1.* FROM temp_logimagecolor_1 WHERE 1=1 $machineQ order by temp_logimagecolor_1.logTime desc";   
        $query = $this->db->query($sql);  
        return $query->result(); 
    }
    
    function get_color_2($machineId, $dateValS, $dateValE)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sqlR = "SELECT sum(timeDiff) as totalRed FROM temp_logimagecolor_2 WHERE redStatus = '1' $machineQ $dateQ order by temp_logimagecolor_2.logTime desc";   
        $queryR = $this->db->query($sqlR); 
        $result['totalRed'] = $queryR->result()[0]->totalRed;
        
        $sqlG = "SELECT sum(timeDiff) as totalGreen FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ order by temp_logimagecolor_2.logTime desc";   
        $queryG = $this->db->query($sqlG);
        $result['totalGreen'] = $queryG->result()[0]->totalGreen;   

        $sqlY = "SELECT sum(timeDiff) as totalYellow FROM temp_logimagecolor_2 WHERE yellowStatus = '1' $machineQ $dateQ order by temp_logimagecolor_2.logTime desc";   
        $queryY = $this->db->query($sqlY);
        $result['totalYellow'] = $queryY->result()[0]->totalYellow; 
        
        $diffQ1 = " and temp_logimagecolor_2.timeDiff BETWEEN '0' and '60' "; 
        $sqlG1 = "SELECT count(*) as green1 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ1 order by temp_logimagecolor_2.logTime desc";   
        $queryG1 = $this->db->query($sqlG1);
        $result['green1'] = $queryG1->result()[0]->green1;
        
        $diffQ2 = " and temp_logimagecolor_2.timeDiff BETWEEN '61' and '180' "; 
        $sqlG2 = "SELECT count(*) as green2 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ2 order by temp_logimagecolor_2.logTime desc";   
        $queryG2 = $this->db->query($sqlG2);
        $result['green2'] = $queryG2->result()[0]->green2;
        
        $diffQ3 = " and temp_logimagecolor_2.timeDiff BETWEEN '181' and '360' "; 
        $sqlG3 = "SELECT count(*) as green3 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ3 order by temp_logimagecolor_2.logTime desc";   
        $queryG3 = $this->db->query($sqlG3);
        $result['green3'] = $queryG3->result()[0]->green3;
        
        $diffQ4 = " and temp_logimagecolor_2.timeDiff BETWEEN '361' and '600' "; 
        $sqlG4 = "SELECT count(*) as green4 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ4 order by temp_logimagecolor_2.logTime desc";   
        $queryG4 = $this->db->query($sqlG4);
        $result['green4'] = $queryG4->result()[0]->green4;
        
        $diffQ5 = " and temp_logimagecolor_2.timeDiff BETWEEN '601' and '900' "; 
        $sqlG5 = "SELECT count(*) as green5 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ5 order by temp_logimagecolor_2.logTime desc";   
        $queryG5 = $this->db->query($sqlG5);
        $result['green5'] = $queryG5->result()[0]->green5;
        
        $diffQ6 = " and temp_logimagecolor_2.timeDiff > '900' "; 
        $sqlG6 = "SELECT count(*) as green6 FROM temp_logimagecolor_2 WHERE greenStatus = '1' $machineQ $dateQ $diffQ6 order by temp_logimagecolor_2.logTime desc";   
        $queryG6 = $this->db->query($sqlG6);
        $result['green6'] = $queryG6->result()[0]->green6;
        
        
        $diffQ7 = " and temp_logimagecolor_2.timeDiff BETWEEN '0' and '30' "; 
        $sqlR1 = "SELECT count(*) as red1 FROM temp_logimagecolor_2 WHERE redStatus = '1' $machineQ $dateQ $diffQ7 order by temp_logimagecolor_2.logTime desc";   
        $queryR1 = $this->db->query($sqlR1);
        $result['red1'] = $queryR1->result()[0]->red1;
        
        $diffQ8 = " and temp_logimagecolor_2.timeDiff BETWEEN '31' and '120' "; 
        $sqlR2 = "SELECT count(*) as red2 FROM temp_logimagecolor_2 WHERE redStatus = '1' $machineQ $dateQ $diffQ8 order by temp_logimagecolor_2.logTime desc";   
        $queryR2 = $this->db->query($sqlR2);
        $result['red2'] = $queryR2->result()[0]->red2;
        
        $diffQ9 = " and temp_logimagecolor_2.timeDiff > '121' "; 
        $sqlR3 = "SELECT count(*) as red3 FROM temp_logimagecolor_2 WHERE redStatus = '1' $machineQ $dateQ $diffQ9 order by temp_logimagecolor_2.logTime desc";   
        $queryR3 = $this->db->query($sqlR3);
        $result['red3'] = $queryR3->result()[0]->red3;
        
        
        $diffQ10 = " and temp_logimagecolor_2.timeDiff BETWEEN '0' and '30' "; 
        $sqlY1 = "SELECT count(*) as yellow1 FROM temp_logimagecolor_2 WHERE yellowStatus = '1' $machineQ $dateQ $diffQ10 order by temp_logimagecolor_2.logTime desc";   
        $queryY1 = $this->db->query($sqlY1);
        $result['yellow1'] = $queryY1->result()[0]->yellow1;
        
        $diffQ11 = " and temp_logimagecolor_2.timeDiff BETWEEN '31' and '120' "; 
        $sqlY2 = "SELECT count(*) as yellow2 FROM temp_logimagecolor_2 WHERE yellowStatus = '1' $machineQ $dateQ $diffQ11 order by temp_logimagecolor_2.logTime desc";   
        $queryY2 = $this->db->query($sqlY2);
        $result['yellow2'] = $queryY2->result()[0]->yellow2;
        
        $diffQ12 = " and temp_logimagecolor_2.timeDiff > '121' "; 
        $sqlY3 = "SELECT count(*) as yellow3 FROM temp_logimagecolor_2 WHERE yellowStatus = '1' $machineQ $dateQ $diffQ12 order by temp_logimagecolor_2.logTime desc";   
        $queryY3 = $this->db->query($sqlY3);
        $result['yellow3'] = $queryY3->result()[0]->yellow3;
        
       $result['total'] = $result['totalRed']+$result['totalGreen']+$result['totalYellow']; 
       return $result; 
    }
    
    public function getallColorLogs_distribution($dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
       $orderCol = 'temp_logimagecolor_2.logTime';
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT machine.machineId, machine.machineName, case when temp_logimagecolor_2.redStatus = '1' then sum(temp_logimagecolor_2.timeDiff) else 0 end as redTime, case when temp_logimagecolor_2.greenStatus = '1' then sum(temp_logimagecolor_2.timeDiff) else 0 end as greenTime, case when temp_logimagecolor_2.yellowStatus = '1' then sum(temp_logimagecolor_2.timeDiff) else 0 end as yellowTime, date(temp_logimagecolor_2.logTime) logTimeDate FROM `temp_logimagecolor_2` left join machine on machine.machineId = temp_logimagecolor_2.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime) order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $this->db->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallColorLogsCount_distribution($machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE 1=1 $machineQ $userQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql);
        return $query; 
    }
    
    public function getSearchColorLogsCount_distribution($dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE 1=1 $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql); 
        $result = $query->result();
        return $query; 
    }
    
    public function getallColorLogs_products($dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $orderCol = 'temp_logimagecolor_2.logTime';
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT machine.machineId, machine.machineName, case when temp_logimagecolor_2.timeDiff BETWEEN '0' and '60' then count(*) else 0 end as green1, case when temp_logimagecolor_2.timeDiff BETWEEN '61' and '180' then count(*) else 0 end as green2, case when temp_logimagecolor_2.timeDiff BETWEEN '181' and '360' then count(*) else 0 end as green3, case when temp_logimagecolor_2.timeDiff BETWEEN '361' and '600' then count(*) else 0 end as green4, case when temp_logimagecolor_2.timeDiff BETWEEN '601' and '900' then count(*) else 0 end as green5, case when temp_logimagecolor_2.timeDiff > '900' then count(*) else 0 end as green6, date(temp_logimagecolor_2.logTime) logTimeDate FROM `temp_logimagecolor_2` left join machine on machine.machineId = temp_logimagecolor_2.machineId WHERE temp_logimagecolor_2.greenStatus = '1' $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime) order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $this->db->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallColorLogsCount_products($machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE temp_logimagecolor_2.greenStatus = '1' $machineQ $userQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql);
        return $query; 
    }
    
    public function getSearchColorLogsCount_products($dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE temp_logimagecolor_2.greenStatus = '1' $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql); 
        $result = $query->result();
        return $query; 
    }
    
    public function getallColorLogs_notify($dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
       $orderCol = 'temp_logimagecolor_2.logTime';
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        
        $sql = "SELECT machine.machineId, machine.machineName, case when (temp_logimagecolor_2.redStatus = '1' and temp_logimagecolor_2.timeDiff BETWEEN '0' and '30') then count(*) else 0 end as red1, case when (temp_logimagecolor_2.redStatus = '1' and temp_logimagecolor_2.timeDiff BETWEEN '31' and '120') then count(*) else 0 end as red2, case when (temp_logimagecolor_2.redStatus = '1' and temp_logimagecolor_2.timeDiff > '120' ) then count(*) else 0 end as red3, case when (temp_logimagecolor_2.yellowStatus = '1' and temp_logimagecolor_2.timeDiff BETWEEN '0' and '30') then count(*) else 0 end as yellow1, case when (temp_logimagecolor_2.yellowStatus = '1' and temp_logimagecolor_2.timeDiff BETWEEN '31' and '120') then count(*) else 0 end as yellow2, case when (temp_logimagecolor_2.yellowStatus = '1' and temp_logimagecolor_2.timeDiff > '120' ) then count(*) else 0 end as yellow3, date(temp_logimagecolor_2.logTime) logTimeDate FROM `temp_logimagecolor_2` left join machine on machine.machineId = temp_logimagecolor_2.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime) order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $this->db->query($sql);
        $result = $query->result();
        return $result; 
    }
    
    public function getallColorLogsCount_notify($machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE 1=1 $machineQ $userQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql);
        return $query; 
    }
    
    public function getSearchColorLogsCount_notify($dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
       $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor_2.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor_2.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and temp_logimagecolor_2.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `temp_logimagecolor_2` WHERE 1=1 $machineQ $userQ $searchQuery $dateQ group by date(temp_logimagecolor_2.logTime)";  
        $query = $this->db->query($sql); 
        $result = $query->result();
        return $query; 
    }
    
    public function getAllCountFactRedAnalytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_red_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_red_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `fact_red_analytics` WHERE 1=1 $machineQ $userQ ";  
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getSearchCountFactRedAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_red_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_red_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_red_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `fact_red_analytics` WHERE 1=1 $machineQ $userQ $searchQuery $dateQ";   
        $query = $factory->query($sql); 
        $result = $query->result();
        return $query; 
    }

    public function getSearchFactRedAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $orderCol = 'fact_red_analytics.logId';
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_red_analytics.machineId = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_red_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_red_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT machine.machineId, machine.machineName, fact_red_analytics.* FROM `fact_red_analytics` left join machine on machine.machineId = fact_red_analytics.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }

    public function getAllCountFactGreenAnalytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_green_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_green_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `fact_green_analytics` WHERE 1=1 $machineQ $userQ ";  
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getSearchCountFactGreenAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_green_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_green_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_green_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `fact_green_analytics` WHERE 1=1 $machineQ $userQ $searchQuery $dateQ";   
        $query = $factory->query($sql); 
        $result = $query->result();
        return $query; 
    }

    public function getSearchFactGreenAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $orderCol = 'fact_green_analytics.logId';
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_green_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_green_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_green_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT machine.machineId, machine.machineName, fact_green_analytics.* FROM `fact_green_analytics` left join machine on machine.machineId = fact_green_analytics.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }

    public function getAllCountFactYellowAnalytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_yellow_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_yellow_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `fact_yellow_analytics` WHERE 1=1 $machineQ $userQ ";  
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getSearchCountFactYellowAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_yellow_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_yellow_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_yellow_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `fact_yellow_analytics` WHERE 1=1 $machineQ $userQ $searchQuery $dateQ";   
        $query = $factory->query($sql); 
        $result = $query->result();
       return $query; 
    }

    public function getSearchFactYellowAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $orderCol = 'fact_yellow_analytics.logId';
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_yellow_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_yellow_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_yellow_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT machine.machineId, machine.machineName, fact_yellow_analytics.* FROM `fact_yellow_analytics` left join machine on machine.machineId = fact_yellow_analytics.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }

    public function getAllCountFactAllAnalytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_all_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_all_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `fact_all_analytics` WHERE 1=1 $machineQ $userQ ";  
        $query = $factory->query($sql);
        return $query; 
    }
    
    public function getSearchCountFactAllAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_all_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_all_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_all_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT 1 FROM `fact_all_analytics`left join machine on machine.machineId = fact_all_analytics.machineId left join machineStateColorLookup on machineStateColorLookup.machineId = fact_all_analytics.machineId where fact_all_analytics.cat = CONCAT(`redStatus`, '-', `yellowStatus`, '-',`greenStatus`, '-', `whiteStatus`) $machineQ $userQ $searchQuery $dateQ";    
        $query = $factory->query($sql); 
        $result = $query->result();
        return $query; 
    }

    public function getSearchFactAllAnalytics($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("fact_all_analytics.machineId", "machine.machineName", "machineStateColorLookup.machineStateVal", "fact_all_analytics.logTime", "fact_all_analytics.Prod_Cycle_Time", "fact_all_analytics.Prod_Cycle_Time_Seconds"); 
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and fact_all_analytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and fact_all_analytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and fact_all_analytics.logTime BETWEEN '".$dateValS."' and '".$dateValE."' ";  
        $sql = "SELECT fact_all_analytics.*, machineStateColorLookup.machineStateVal, machine.machineId, machine.machineName FROM `fact_all_analytics`left join machine on machine.machineId = fact_all_analytics.machineId left join machineStateColorLookup on machineStateColorLookup.machineId = fact_all_analytics.machineId where fact_all_analytics.cat = CONCAT(`redStatus`, '-', `yellowStatus`, '-',`greenStatus`, '-', `whiteStatus`) $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit";
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }

    public function getOverviewNotification($factory, $machineId)
    { 
        $factory->select('*');
        $factory->from('notification');
        $factory->where('isDeleted', '0');
        $factory->where('machineId', $machineId);
        $factory->order_by('notificationId', 'desc');
        $factory->limit(10,0); 
        $query= $factory->get();
        return $query->result();  
    } 
    
    public function restoreMStepStatus($categoryId)
    {  
        $this->db->where('categoryId',$categoryId);
        $update = array('stepStatus'=>'0');
        $this->db->update('mSteps',$update); 
    } 
    
    public function checkOutAll()
    {  
        $update = array('isActive'=>'0');
        $this->db->update('machineUser',$update); 
    } 
    
    public function addMNotificationAll($categoryId, $addedDate)
    {  
        $this->db->select('userId, machineId');
        $this->db->from('machineUser');
        $this->db->where('isActive', '1');
        $query= $this->db->get();
        $activeUsers = $query->result();  
        for($i=0;$i<count($activeUsers);$i++) 
        {
            $insert=array(
                'userId'=>$activeUsers[$i]->userId,
                'machineId'=>$activeUsers[$i]->machineId,  
                'categoryId'=>$categoryId,  
                'addedDate'=>$addedDate,  
                'respondTime'=>$addedDate,
            );
            $this->db->insert('mNotification',$insert);
        }
    } 
    
    public function getallColors()
    {
        $this->db->select('*');
        $this->db->from('color');
        $this->db->order_by('orderId','asc'); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    
    public function colorExists($colorName, $colorId=0)
    { 
        $this->db->select('*');
        $this->db->from('color');
        $this->db->where('colorName', $colorName);
        if($colorId != 0) 
        {
            $this->db->where('colorId != ', $colorId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
       return $row; 
    }
    
    public function newStackLightColor($insert)
    { 
        $this->db->insert('color',$insert);
        return $this->db->insert_id();
    } 
    
    public function updateStackLightColor($update, $colorId)
    { 
        $this->db->where('colorId',$colorId);
        $this->db->update('color',$update); 
    }
    
    public function deleteStackLightColor($colorId)
    { 
        $this->db->where('colorId',$colorId);
        $update = array('isDeleted'=>'1');
        $this->db->update('color',$update); 
     } 
    
    
    public function getFactoryData($factoryId)
    {
        $this->db->select('*');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $this->db->where('factoryId', $factoryId);
        $query= $this->db->get(); 
        return $query->row(); 
    }
    
    public function getUserDevice($userId)
    { 
        $this->db->select('deviceType, deviceToken');
        $this->db->from('user');
        $this->db->where('userId', $userId); 
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    
    public function testtt() 
    {
      $registrationIDs = array(
            'cAVPis8-8u0:APA91bGj3cQLSMsFIDmUUi-g0nKgv2KzjoSmToe9UYkgb1WF0M-XuBMWi589hc70-lvLyaNrIFLUuBZ6LlfxBnCeyhzHKoiQVjBMADpNNLyF4I9rgkmEhXILZUNNDhVz0G2_18OCnodD',
            'eNHzs2V-Rzw:APA91bFbwift1tGD27WiGY97QAXMi4KJAJSXJUWhRo0rxmtcQTqDyciRsWWAeTKL8YtH-8HULunWaxviN097fi-_P6jgDrxxnTTqd2_gU0UWleBIXGmew4oJXmYNRYcyOIvukBFJXFOx', 
            'd_k4IwPlwUs:APA91bEWen23W47bH0gba-H0E78PPGj9sUlzs7_icll06Ni3HOuFtkYRAVTqpxFH1YiNrgXPEQXvEIK9d3AgiQsVCujhP8ySKnZjMYlWVeM1SmEuscSoaZrgxShn8s4_pgN4jgO0gJ9b'
        ) ;
        $fcmMsg = array(
            'body' => 'here is a message. message',
            'title' => 'This is title #1',
            'sound' => "default",
                'color' => "#203E78" 
        );
        $fcmFields = array(
            'registration_ids'=> $registrationIDs, 
            'priority' => 'high',
            'notification' => $fcmMsg
        );

        $headers = array(
            'Authorization: key=' . API_ACCESS_KEY,
            'Content-Type: application/json'
        );
        echo "<pre>";print_r($headers);echo "</pre>";
        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fcmFields ) );
        $result = curl_exec($ch );
        curl_close( $ch );
        echo $result . "\n\n";
    }
    
    
    public function addRedNotification($date)
    {
        
        $factoryData = $this->getFactoryData($this->session->userdata('factoryId'));
        
        $sql2 = "SELECT machine.machineName, temp_logimagecolor.logId, temp_logimagecolor.machineId, TIMESTAMPDIFF(SECOND, logTime, NOW()) as colorTime FROM temp_logimagecolor left join machine on temp_logimagecolor.machineId = machine.machineId WHERE temp_logimagecolor.logId  in (SELECT MAX(logId) FROM temp_logimagecolor GROUP BY machineId) and temp_logimagecolor.redStatus = '1' ";  
        $query2 = $this->db->query($sql2);
        $redStatus = $query2->result();
        echo "<pre>"; print_r($redStatus); echo "</pre>"; 
        if(is_array($redStatus)) 
        {
            for($x=0;$x<count($redStatus);$x++) 
            {
                if($redStatus[$x]->colorTime > $factoryData->notifyRed ) 
                {
                    $this->db->select('*');
                    $this->db->from('machineUser');
                    $this->db->where('isActive', '1');
                    $this->db->where('machineId', $redStatus[$x]->machineId);
                    $this->db->like('startTime', date("Y-m-d", strtotime($date)));
                    $queryMU= $this->db->get();
                    $machineUser[$x] = $queryMU->result();  
                    if(is_array($machineUser[$x])) 
                    {
                        for($y=0;$y<count($machineUser[$x]);$y++) 
                        {
                            $this->db->select('1');
                            $this->db->from('notification');
                            $this->db->where('isDeleted', '0');
                            $this->db->where('commentFlag', '1'); 
                            //if 2 min notification already added 
                            $this->db->where('logId', $redStatus[$x]->logId);
                            $this->db->where('userId',$machineUser[$x][$y]->userId);
                            $queryN= $this->db->get();
                            $notification[$x] = $queryN->row();  
                            
                            if(!isset($notification[$x])) 
                            {
                                $userDevice = $this->getUserDevice($machineUser[$x][$y]->userId);
                                $insert=array(
                                    'userId'=>$machineUser[$x][$y]->userId,
                                    'machineId'=>$redStatus[$x]->machineId,  
                                    'logId'=>$redStatus[$x]->logId,  
                                    'notificationText'=>$redStatus[$x]->machineName." stopped. Please explain reason for breakdown.",  
                                    'addedDate'=>$date,  
                                    'type'=>'Red'
                                );
                                if($redStatus[$x]->colorTime > $factoryData->prolongedRed ) 
                                {
                                    $insert['commentFlag'] = '1'; //commentable
                                } 
                                else 
                                {
                                    $insert['commentFlag'] = '0'; //not commentable
                                }
                                
                                $this->db->insert('notification',$insert);
                                echo $notId = $this->db->insert_id();
                                 $msg = array
                                (
                                    'message'   => $insert['notificationText'],
                                    'title'     => $insert['notificationText'],
                                    'subtitle'  => $insert['notificationText'],
                                    'vibrate'   => 1,
                                    'sound'     => 1,
                                    'notId' => $notId 
                                );
                                $fields = array
                                (
                                    'registration_ids'  => array($userDevice->deviceToken),  
                                    'data' => $msg
                                );

                                $headers = array
                                (
                                    'Authorization: key=' . API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );

                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch);
                                curl_close($ch);
                            }
                        }
                        
                    }
                } 
            }
        }
    } 
    
    public function addYellowNotification($date)
    {
         $API_ACCESS_KEY = 'AAAA88yPT8I:APA91bG8nnbrTNSeWFZ54QHgScvzpO5jbAcU9A3b13aHTxLvD1xSCAHX1SpVt-ulnVe5CMcEj19Te18kfZc7knSjf7rdAO8QIeS_TX8GZxj01BdXeh-j5ziaJlJXp1e7rpC98Fh60yiJ'; 
        
        $factoryData = $this->getFactoryData($this->session->userdata('factoryId'));
        
        $sql2 = "SELECT machine.machineName, temp_logimagecolor.logId, temp_logimagecolor.machineId, TIMESTAMPDIFF(SECOND, logTime, NOW()) as colorTime FROM temp_logimagecolor left join machine on temp_logimagecolor.machineId = machine.machineId WHERE temp_logimagecolor.logId  in (SELECT MAX(logId) FROM temp_logimagecolor GROUP BY machineId) and temp_logimagecolor.yellowStatus = '1' ";  
        $query2 = $this->db->query($sql2);
        $yellowStatus = $query2->result();
        if(is_array($yellowStatus)) 
        {
            for($x=0;$x<count($yellowStatus);$x++) 
            {
                if($yellowStatus[$x]->colorTime > $factoryData->notifyYellow ) 
                {
                    $this->db->select('*');
                    $this->db->from('machineUser');
                    $this->db->where('isActive', '1');
                    $this->db->where('machineId', $yellowStatus[$x]->machineId);
                    $this->db->like('startTime', date("Y-m-d", strtotime($date)));
                    $queryMU= $this->db->get();
                    $machineUser[$x] = $queryMU->result();  
                    if(is_array($machineUser[$x])) 
                    {
                        for($y=0;$y<count($machineUser[$x]);$y++) 
                        {
                            $this->db->select('1');
                            $this->db->from('notification');
                            $this->db->where('isDeleted', '0');
                            $this->db->where('commentFlag', '1'); //if 2 min notification already added 
                            $this->db->where('logId', $yellowStatus[$x]->logId);
                            $this->db->where('userId',$machineUser[$x][$y]->userId);
                            $queryN= $this->db->get();
                            $notification[$x] = $queryN->row();  
                            
                            if(!isset($notification[$x])) 
                            {
                                $userDevice = $this->getUserDevice($machineUser[$x][$y]->userId);
                                $insert=array(
                                    'userId'=>$machineUser[$x][$y]->userId,
                                    'machineId'=>$yellowStatus[$x]->machineId,  
                                    'logId'=>$yellowStatus[$x]->logId,  
                                    'notificationText'=>$yellowStatus[$x]->machineName." has paused. Please fix and explain reason for breakdown.",  
                                    'addedDate'=>$date,  
                                    'type'=>'Yellow' 
                                );
                                if($yellowStatus[$x]->colorTime > $factoryData->prolongedYellow ) 
                                {
                                    $insert['commentFlag'] = '1'; //commentable
                                } 
                                else 
                                {
                                    $insert['commentFlag'] = '0'; //not commentable
                                }
                                
                                $this->db->insert('notification',$insert);
                                $notId = $this->db->insert_id(); 
                                $msg = array
                                (
                                    'message'   => $insert['notificationText'],
                                    'title'     => $insert['notificationText'],
                                    'subtitle'  => $insert['notificationText'],
                                    'vibrate'   => 1,
                                    'sound'     => 1,
                                    'notId' => $notId 
                                );
                                $fields = array
                                (
                                    'registration_ids'  => array($userDevice->deviceToken),  
                                    'data' => $msg
                                );

                                $headers = array
                                (
                                    'Authorization: key=' . $API_ACCESS_KEY,
                                    'Content-Type: application/json'
                                );

                                $ch = curl_init();
                                curl_setopt( $ch,CURLOPT_URL, 'https://android.googleapis.com/gcm/send' );
                                curl_setopt( $ch,CURLOPT_POST, true );
                                curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
                                curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                $result = curl_exec($ch);
                                curl_close($ch);
                            }
                        }
                        
                    }
                }
            }
        }
    }
    
    public function getallTemplates()
    { 
        $this->db->select('*');
        $this->db->from('maintenanceTemplate');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $template = $query->result(); 
        for($i=0;$i<count($template);$i++) 
        {
            $template[$i]->content = $this->getMTContent($template[$i]->mTemplateId);
        }
        return $template; 
    }
    
    public function getTemplateDetail($mTemplateId)
    { 
        $this->db->select('*');
        $this->db->from('maintenanceTemplate');
        $this->db->where('isDeleted', '0');
        $this->db->where('mTemplateId', $mTemplateId);
        $query= $this->db->get();
        $template = $query->row(); 
        $template->content = $this->getMTContent($template->mTemplateId);
        return $template; 
    }
    
    public function getMTContent($mTemplateId)
    {
        $this->db->select('*');
        $this->db->from('maintenanceTemplateContent');
        $this->db->where('isDeleted', '0');
        $this->db->where('mTemplateId', $mTemplateId);
        $this->db->order_by('mTCCategory', 'asc');
        $query= $this->db->get(); 
        return $query->result(); 
    }
    public function newTemplate($insert)
    { 
        $this->db->insert('maintenanceTemplate',$insert);
        return $this->db->insert_id();
    } 
    
    public function addTemplateContent($insert)
    {  
        $this->db->insert_batch('maintenanceTemplateContent',$insert);
    } 
    
    public function updateTemplate($update, $mTemplateId)
    { 
        $this->db->where('mTemplateId',$mTemplateId);
        $this->db->update('maintenanceTemplate',$update); 
    } 
    
    public function updateTemplateContent($update, $mTCId)
    { 
        $this->db->where('mTCId',$mTCId);
        $this->db->update('maintenanceTemplateContent',$update); 
    } 
    
    public function deleteTemplate($mTemplateId)
    { 
        $this->db->where('mTemplateId',$mTemplateId);
        $update = array('isDeleted'=>'1');
        $this->db->update('maintenanceTemplate',$update); 
    } 
    
    public function deleteTemplateContent($mTcId)
    { 
        $this->db->where('mTcId',$mTcId);
        $update = array('isDeleted'=>'1');
        $this->db->update('maintenanceTemplateContent',$update); 
    }
    
    public function getAllColorLogsCountTraining($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1=1 and isUnique = '1' $machineQ $userQ";
        $query = $factory->query($sql); 
        return $query; 
    }
    
    public function getSearchColorLogsCountTraining($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId WHERE 1=1 and isUnique = '1' $machineQ $userQ $dateQ $searchQuery ";  
        $query = $factory->query($sql); 
        return $query;  
    }
    
    public function getSearchColorLogsTraining($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        $columnArr = array("temp_logimagecolor.logImageId", "temp_logimagecolor.logId", "temp_logimageframe.imageName", '', "temp_logimagecolor.clientComment", "temp_logimagecolor.originalColor", "temp_logimageframe.logTime");
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and temp_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and temp_logimageframe.logTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT temp_logimagecolor.*, temp_logimageframe.*, client.userName as clientName, user.userName as originalColorUserName FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId left join user as client on temp_logimagecolor.commentClientId = client.userId left join user on temp_logimagecolor.originalColorId = user.userId WHERE 1=1 and isUnique = '1' $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $factory->query($sql); 
        
        return $query; 
    }
    public function removeFromTraining($logimageId)
    { 
        $this->db->where('logimageId',$logimageId);
        $this->db->update('temp_logimagecolor',array('isUnique'=>'0')); 
    } 
    
    public function processTrainingImages($logImageIds)
    { 
        $this->db->where_in('logimageId',$logImageIds); 
        $this->db->update('temp_logimagecolor',array('isUnique'=>'2'));   
    } 
    public function getallColorLogsTrainingProcess($is_admin=1) 
    {
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and temp_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 ";  
            }
        }
        $sql = "SELECT temp_logimagecolor.logImageId, temp_logimagecolor.originalColor, temp_logimagecolor.img_dir, temp_logimageframe.imageName, temp_logimageframe.userId FROM temp_logimagecolor left join temp_logimageframe on temp_logimagecolor.logId = temp_logimageframe.logId left join user as client on temp_logimagecolor.commentClientId = client.userId left join user on temp_logimagecolor.originalColorId = user.userId WHERE 1=1 and isUnique = '1' $userQ";  
        $query = $this->db->query($sql);   
        return $query; 
    }
    
    public function addLogImageFrame($factoryId, $originalTime, $file_name, $file_ext, $machineId, $userId) 
    { 
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $insert = array("imageName"=>$machineId."_".$userId ."_".$file_name.".".$file_ext, 
                    "originalTime"=>date('Y-m-d H:i:s', $originalTime),
                    "logTime"=>date('Y-m-d H:i:s', $file_name),
                    "uploadedTime"=>date('Y-m-d H:i:s'),
                    "machineId"=>$machineId,
                    "userId"=>$userId,
                    );
        $this->factory->insert('temp_logimageframe',$insert);  
        return $this->factory->insert_id(); 
    } 
    
    public function addBetaLogColorFrame($factoryId, $originalTime, $currentTime, $color, $accuracy, $machineId, $userId) 
    {  
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        $insert = array("color"=>$color, 
                    "accuracy"=>$accuracy,
                    "originalTime"=>date('Y-m-d H:i:s', $originalTime),
                    "insertTime"=>date('Y-m-d H:i:s', $currentTime),
                    "machineId"=>$machineId,
                    "userId"=>$userId,
                    ); 
        $this->factory->insert('beta_logimagecolor',$insert);  
        return $this->factory->insert_id(); 
    }
    
    function getMachineLastStatesmulti($factory, $machineId) 
    { 
        $sql = "SELECT redStatus, greenStatus, yellowStatus, whiteStatus, lightStatus FROM temp_logimagecolor WHERE machineId = $machineId order by logId desc limit 0, 20";  
        $query = $factory->query($sql);
        $content = $query->result('array');     
        return $content;  
    }
    
    function getMachineLastStatesmultiBeta($factory, $machineId) 
    { 
        $sql = "SELECT color, logId FROM beta_logimagecolor WHERE machineId = $machineId order by logId desc limit 0, 20";  
        $query = $factory->query($sql);
        $content = $query->result('array');     
        return $content;  
    } 
    
    public function getallReasons()
    {
        $this->db->select('*');
        $this->db->from('breakdownReason');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    } 
    
    public function getIssueReasonData($dateValS, $dateValE)
    {
        $this->factory->select('notification.notificationId, notification.comment' ); 
        $this->factory->from('notification');
        $this->factory->where('notification.commentFlag', '1'); // respondable notifications
        $this->factory->where('notification.type', 'Red'); 
        $this->factory->where('addedDate >=', $dateValS); 
        $this->factory->where('addedDate <=', $dateValE); 
        $query= $this->factory->get(); 
        $result = $query->result();
        return $result;
    }
    
    public function getNotifyReasonData($dateValS, $dateValE)
    {
        $this->factory->select('notification.notificationId, notification.comment' ); 
        $this->factory->from('notification');
        $this->factory->where('notification.commentFlag', '1'); // respondable notifications
        $this->factory->where('notification.type', 'Yellow'); 
        $this->factory->where('addedDate >=', $dateValS); 
        $this->factory->where('addedDate <=', $dateValE); 
        $query= $this->factory->get(); 
        $result = $query->result();
        return $result;
    } 
    
    public function getAllNotificationsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and notification.machineId = $machineId";  
        }
        
        $sql = "SELECT count(*) as totalCount FROM notification left join nytt_main.user on user.userId = notification.userId left join machine on machine.machineId = notification.machineId WHERE 1 $machineQ  and notification.commentFlag = '1'";  
        $query = $this->factory->query($sql); 
        return $query;  
    } 
    
    public function getNotifications($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        
        $columnArr = array("notification.logId", "notification.comment", "responder.userName", 'notification.respondTime', "machine.machineName", "startTime", "endTime", '', "notification.addedDate" ); 
        $orderCol = $columnArr[$columnIndex];
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and notification.machineId = $machineId";  
        }       
        $dateQ = " and notification.addedDate BETWEEN '".$dateValS."' and '".$dateValE."' ";  
            $sql = "SELECT notification.*, responder.userName as responderName, machine.machineName, bl.originalTime as startTime, (select  beta_logimagecolor.originalTime from beta_logimagecolor where logId > notification.logId and machineId = bl.machineId and userId = bl.userId order by logId asc limit 1)as endTime FROM notification left join nytt_main.user as responder on responder.userId = notification.responderId left join machine on machine.machineId = notification.machineId left join beta_logimagecolor bl on bl.logId = notification.logId WHERE 1 $machineQ $dateQ $searchQuery and notification.commentFlag = '1'  order by $orderCol $columnSortOrder limit $start, $limit";  
        $query = $this->factory->query($sql);   
        return $query;
    }
    
    public function getSearchNotificationsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and notification.machineId = $machineId"; 
        }
        $dateQ = " and notification.addedDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT count(*) as totalCount FROM notification left join nytt_main.user as responder on responder.userId = notification.responderId left join machine on machine.machineId = notification.machineId left join beta_logimagecolor bl on bl.logId = notification.logId WHERE 1 $machineQ $dateQ $searchQuery and notification.commentFlag = '1'"; 
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    public function updateNotification($factory, $update, $notificationId)
    { 
        $this->factory->where('notificationId',$notificationId); 
        $this->factory->update('notification',$update); 
        echo $this->factory->last_query();
    }

    public function getallBetaColorLogs($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        $columnArr = array("beta_logimagecolor.logId", "user.userName", "machine.machineName", "beta_logimagecolor.color", "beta_logimagecolor.accuracy", "beta_logimagecolor.originalTime"); 
        $orderCol = $columnArr[$columnIndex];  
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and beta_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and beta_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dateQ = " and beta_logimagecolor.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT beta_logimagecolor.*, user.userName, machine.machineName FROM beta_logimagecolor left join nytt_main.user on beta_logimagecolor.userId = user.userId left join machine on beta_logimagecolor.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $searchQuery $dateQ order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    public function getallBetaColorLogsCount($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and beta_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and beta_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $sql = "SELECT count(*) as totalCount FROM beta_logimagecolor WHERE 1=1 $machineQ $userQ";
        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    public function getSearchBetaColorLogsCount($factory, $dateValS, $dateValE, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and beta_logimagecolor.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and beta_logimagecolor.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $dateQ = " and beta_logimagecolor.originalTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM beta_logimagecolor left join nytt_main.user on beta_logimagecolor.userId = user.userId left join machine on beta_logimagecolor.machineId = machine.machineId WHERE 1=1 $machineQ $userQ $dateQ $searchQuery ";  
        $query = $this->factory->query($sql); 
        return $query;  
    }  
    
    public function getSearch_daily_analytics($factory, $day, $week, $month, $quarter, $year, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("dailyAnalytics.maId", "dailyAnalytics.machineId", "machine.machineName", "machineStateColorLookup.machineStateVal", "dailyAnalytics.day", "dailyAnalytics.week", "dailyAnalytics.month", "dailyAnalytics.quarter", "dailyAnalytics.year", "dailyAnalytics.duration");  
        $orderCol = $columnArr[$columnIndex];   
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and dailyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and dailyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dayQ = "";
        if($day > 0 ) 
        {
            $dayQ = " and dailyAnalytics.day = $day";  
        }
        
        $weekQ = "";
        if($week > 0 ) 
        {
            $weekQ = " and dailyAnalytics.week = $week";  
        }
        
        $monthQ = "";
        if($month > 0 ) 
        {
            $monthQ = " and dailyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and dailyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and dailyAnalytics.year = $year";  
        } 
        
        $sql = "SELECT dailyAnalytics.*, machineStateColorLookup.machineStateVal, machine.machineId, machine.machineName FROM `dailyAnalytics` left join machine on machine.machineId = dailyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = dailyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $dayQ $weekQ $monthQ $quarterQ $yearQ order by $orderCol $columnSortOrder limit $start, $limit";
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallCount_daily_analytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and dailyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and dailyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `dailyAnalytics` WHERE 1=1 $machineQ $userQ";  
        $query = $this->factory->query($sql);
        return $query; 
    }
    
    public function getSearchCount_daily_analytics($factory, $day, $week, $month, $quarter, $year, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and dailyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and dailyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $dayQ = "";
        if($day > 0 ) 
        {
            $dayQ = " and dailyAnalytics.day = $day";  
        }
        
        $weekQ = "";
        if($week > 0 ) 
        {
            $weekQ = " and dailyAnalytics.week = $week"; 
        }
        
        $monthQ = "";
        if($month > 0 ) 
        {
            $monthQ = " and dailyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and dailyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and dailyAnalytics.year = $year";  
        }

        $sql = "SELECT 1 FROM `dailyAnalytics` left join machine on machine.machineId = dailyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = dailyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $dayQ $weekQ $monthQ $quarterQ $yearQ"; 
        $query = $this->factory->query($sql); 
        $result = $query->result();
        return $query; 
    } 
    
    public function getSearch_weekly_analytics($factory, $week, $month, $quarter, $year, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("weeklyAnalytics.maId", "weeklyAnalytics.machineId", "machine.machineName", "machineStateColorLookup.machineStateVal", "weeklyAnalytics.week", "weeklyAnalytics.month", "weeklyAnalytics.quarter", "weeklyAnalytics.year", "weeklyAnalytics.duration");  
        $orderCol = $columnArr[$columnIndex];   
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and weeklyAnalytics.machineId = $machineId";  
        }

        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and weeklyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        
        $weekQ = "";
        if($week > 0 ) 

        {
            $weekQ = " and weeklyAnalytics.week = $week";  
        }
        
        $monthQ = "";
        if($month > 0 ) 

        {
            $monthQ = " and weeklyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 

        {
            $quarterQ = " and weeklyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 

        {
            $yearQ = " and weeklyAnalytics.year = $year";  
        } 
        $sql = "SELECT weeklyAnalytics.*, machineStateColorLookup.machineStateVal, machine.machineId, machine.machineName FROM `weeklyAnalytics` left join machine on machine.machineId = weeklyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = weeklyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $weekQ $monthQ $quarterQ $yearQ order by $orderCol $columnSortOrder limit $start, $limit";
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallCount_weekly_analytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and weeklyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and weeklyAnalytics.machineId in (".$rowUser->machines.")";
            } else {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        $sql = "SELECT 1 FROM `weeklyAnalytics` WHERE 1=1 $machineQ $userQ";  
        $query = $this->factory->query($sql);
        return $query; 
    }
    
    public function getSearchCount_weekly_analytics($factory, $week, $month, $quarter, $year, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and weeklyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and weeklyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $weekQ = "";
        if($week > 0 ) 
        {
            $weekQ = " and weeklyAnalytics.week = $week"; 
        }
        
        $monthQ = "";
        if($month > 0 ) 
        {
            $monthQ = " and weeklyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and weeklyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and weeklyAnalytics.year = $year";  
        }
        $sql = "SELECT 1 FROM `weeklyAnalytics` left join machine on machine.machineId = weeklyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = weeklyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $weekQ $monthQ $quarterQ $yearQ"; 
        $query = $this->factory->query($sql); 
        $result = $query->result();
        return $query; 
    } 
    
    public function getSearch_monthly_analytics($factory, $month, $quarter, $year, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("monthlyAnalytics.maId", "monthlyAnalytics.machineId", "machine.machineName", "machineStateColorLookup.machineStateVal", "monthlyAnalytics.month", "monthlyAnalytics.quarter", "monthlyAnalytics.year", "monthlyAnalytics.duration");  
        $orderCol = $columnArr[$columnIndex];   
        
        $machineQ = '';
        if($machineId > 0 ) 

        {
            $machineQ = "and monthlyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and monthlyAnalytics.machineId in (".$rowUser->machines.")";
            } else {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $monthQ = "";
        if($month > 0 ) 
        {
            $monthQ = " and monthlyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and monthlyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and monthlyAnalytics.year = $year";  
        } 
        
        $sql = "SELECT monthlyAnalytics.*, machineStateColorLookup.machineStateVal, machine.machineId, machine.machineName FROM `monthlyAnalytics` left join machine on machine.machineId = monthlyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = monthlyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $monthQ $quarterQ $yearQ order by $orderCol $columnSortOrder limit $start, $limit";
        
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallCount_monthly_analytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and monthlyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and monthlyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `monthlyAnalytics` WHERE 1=1 $machineQ $userQ";  
        $query = $this->factory->query($sql);
        return $query; 
    }
    
    public function getSearchCount_monthly_analytics($factory, $month, $quarter, $year, $machineId, $is_admin=1, $searchQuery)
    { 
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and monthlyAnalytics.machineId = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and monthlyAnalytics.machineId in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $monthQ = "";
        if($month > 0 ) 
        {
            $monthQ = " and monthlyAnalytics.month = $month";  
        }
        
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and monthlyAnalytics.quarter = $quarter";  
        }
        
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and monthlyAnalytics.year = $year";  
        }
        
        $sql = "SELECT 1 FROM `monthlyAnalytics` left join machine on machine.machineId = monthlyAnalytics.machineId left join machineStatePeriodLookup on machineStatePeriodLookup.amslId = monthlyAnalytics.amslId left join machineStateColorLookup on machineStateColorLookup.aclrId = machineStatePeriodLookup.aclrId where 1 $machineQ $userQ $searchQuery $monthQ $quarterQ $yearQ"; 
        
        $query = $this->factory->query($sql); 
        $result = $query->result();
        return $query; 
    }  
    
    public function getSearch_quarterly_analytics($factory, $quarter, $year, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("hour.machine_id", "machine.machineName", "hour.state", "hour.quarter", "hour.month", "hour.year", "hour.duration");  
        $orderCol = $columnArr[$columnIndex];   
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else
            {
                $userQ = " and 1 != 1 "; 
            }
        }

        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and hour.quarter = $quarter";  
        }
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and hour.year = $year";  
        } 

        $sql = "SELECT hour.*, sum(duration) as duration, machine.machineId, machine.machineName FROM hour left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $searchQuery $quarterQ $userQ group by month, state order by $orderCol $columnSortOrder limit $start, $limit";
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallCount_quarterly_analytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM `hour` left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $userQ";  
        $query = $this->factory->query($sql);
        return $query; 
    }
    public function getSearchCount_quarterly_analytics($factory, $quarter, $year, $machineId, $is_admin=1, $searchQuery)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0)
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $quarterQ = "";
        if($quarter > 0 ) 
        {
            $quarterQ = " and hour.quarter = $quarter";  
        }
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and hour.year = $year";  
        }
        $sql = "SELECT 1 FROM hour left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $quarterQ $searchQuery $userQ group by month, state";
        $query = $this->factory->query($sql); 
        $result = $query->result();
        return $query; 
    }
    
    public function getSearch_yearly_analytics($factory, $year, $machineId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        $columnArr = array("hour.machine_id", "machine.machineName", "hour.state", "hour.month", "hour.year", "hour.duration");   
        $orderCol = $columnArr[$columnIndex];   
        
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        
        $yearQ = ""; 
        if($year > 0 ) {
            $yearQ = " and hour.year = $year";  
        } 
        $sql = "SELECT hour.*, sum(duration) as duration, machine.machineId, machine.machineName FROM hour left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $userQ $searchQuery $yearQ group by month, state order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $factory->query($sql); 
        $result = $query->result();
        return $result; 
    }
    
    public function getallCount_yearly_analytics($factory, $machineId, $is_admin=1)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT 1 FROM hour left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $userQ group by month, state asc";
        $query = $this->factory->query($sql);
        return $query; 
    }
    
    public function getSearchCount_yearly_analytics($factory, $year, $machineId, $is_admin=1, $searchQuery)
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        $userQ = ''; 
        if($is_admin == 0) 
        {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $this->session->userdata('userId')); 
            $this->db->where('isDeleted', '0');
            $query= $this->db->get();
            $rowUser = $query->row();
            if($rowUser->machines != '' ) 
            {
                $userQ = " and hour.machine_id in (".$rowUser->machines.")";
            } 
            else 
            {
                $userQ = " and 1 != 1 "; 
            }
             
        }
        $yearQ = ""; 
        if($year > 0 ) 
        {
            $yearQ = " and hour.year = $year";  
        }
        $sql = "SELECT 1 FROM hour left join machine on machine.machineId = hour.machine_id WHERE machine.isDeleted = '0' $machineQ $userQ $searchQuery $yearQ group by month, state"; 
        $query = $this->factory->query($sql); 
        $result = $query->result();
        return $query; 
    } 
    
    public function getallMachineStateColorLookups($factory)
    {
        $factory->select('machineStateColorLookup.*, machine.machineName'); 
        $factory->from('machineStateColorLookup');
        $factory->join('machine','machineStateColorLookup.machineId = machine.machineId'); 
        $query= $factory->get();
        return $query; 
    } 
    
    function lastMachineStateColorLookupId($factory) 
    {
       $factory->select('max(aclrId) as maxId'); 
       $factory->from('machineStateColorLookup'); 
       $va=$factory->get()->row();
       return $va;
    }  
    
    public function getallMachineStatePeriodLookups($factory)
    {
        $factory->select('machineStatePeriodLookup.*, machineStateColorLookup.machineStateVal, machine.machineName, sStart.machineStateVal as machineStateValStart, sEnd.machineStateVal as machineStateValEnd');  
        $factory->from('machineStatePeriodLookup');
        $factory->join('machine','machineStatePeriodLookup.machineId = machine.machineId'); 
        $factory->join('machineStateColorLookup','machineStatePeriodLookup.aclrId = machineStateColorLookup.aclrId'); 
        $factory->join('machineStateColorLookup as sStart','machineStatePeriodLookup.machineStartAclrId = sStart.aclrId'); 
        $factory->join('machineStateColorLookup as sEnd','machineStatePeriodLookup.machineEndAclrId = sEnd.aclrId'); 
        $query= $factory->get(); 
        return $query;
    } 
    
    function lastMachineStatePeriodLookupId($factory) 
    {
       $factory->select('max(amslId) as maxId'); 
       $factory->from('machineStatePeriodLookup'); 
       $va=$factory->get()->row();
       return $va; 
    }
    
    public function newMachineColorState($factory, $insert)
    { 
        
        $factory->insert('machineStateColorLookup',$insert);
        return $factory->insert_id();
    }
    
    public function updateMachineColorState($factory, $update, $aclrId)
    { 
        
        $factory->where('aclrId',$aclrId);
        $factory->update('machineStateColorLookup',$update);
    } 
    
    public function deleteMachineColorState($factory, $aclrId)
    { 
        
        $factory->where('aclrId',$aclrId);
        $factory->delete('machineStateColorLookup');
    } 
    
    public function getAllStackLightTypes($factoryId)
    {
        $this->db->select('stackLightType.*, GROUP_CONCAT(machine.machineId SEPARATOR ",") as machines');
        $this->db->from('stackLightType');
        $this->db->join('nytt_factory'.$factoryId.'.machine','stackLightType.stackLightTypeId = machine.stackLightTypeId','left'); 
        $this->db->where('stackLightType.isDeleted', '0');
        $this->db->group_by('stackLightType.stackLightTypeId'); 
        $query= $this->db->get();
        return $query; 
    }
    
    public function stackLightTypeExists($stackLightTypeName, $stackLightTypeId=0)
    { 
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('stackLightTypeName', $stackLightTypeName);
        if($stackLightTypeId != 0) 
        { 
            $this->db->where('stackLightTypeId != ', $stackLightTypeId); 
        }
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        $row = $query->row();
        return $row; 
    }
    
    public function newStackLightType($insert)
    {   
        $this->db->insert('stackLightType',$insert);
        return $this->db->insert_id();
    } 
    
    public function updateStackLightType($update, $stackLightTypeId)
    { 
        $this->db->where('stackLightTypeId',$stackLightTypeId);
        $this->db->update('stackLightType',$update); 
    }
    
    public function deleteStackLightType($stackLightTypeId)
    { 
        $this->db->where('stackLightTypeId',$stackLightTypeId);
        $update = array('isDeleted'=>'1');
        $this->db->update('stackLightType',$update); 
    }
    
    function lastStackLightTypeId() 
    {
       $this->db->select('max(stackLightTypeId) as maxId'); 
       $this->db->from('stackLightType'); 
       $va=$this->db->get()->row();
       return $va; 
    } 
    
    public function getAllColorGroups()
    {
        $this->db->select('*');
        $this->db->from('colorGroup');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    
    public function getVisulizationData($factory, $machineId, $type, $state, $choose1='', $choose2='', $choose3='', $choose4='') 
    {
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        
        $yearQ = '';
        if($type == 'year' ) 
        {
            $yearQ = "and hour.year = $choose1";  
            $period_value = 'month';
        }
        
        $monthQ = '';
        if($type == 'month' ) 
        {
            $monthQ = "and hour.year = $choose1 and hour.month = $choose2";  
            $period_value = 'day';  
        }
        
        $weekQ = '';
        if($type == 'week' ) 
        {
            $weekQ = "and hour.year = $choose1 and hour.week = $choose3 and hour.week != 0  "; //and hour.weekday != 0 
            $period_value = 'weekday'; 
        }
        
        $dayQ = '';
        if($type == 'day' ) 
        {
            $dayQ = "and hour.year = $choose1 and hour.month = $choose2 and hour.day = $choose4"; 
            $period_value = 'hour'; 
        }
        
        $sql = "SELECT sum(duration) as countVal, $period_value FROM hour left join machine on hour.machine_id = machine.machineId WHERE machine.isDeleted = '0' $machineQ and state='".$state."' $yearQ $monthQ $weekQ $dayQ group by $period_value order by $period_value asc"; 
        
        $query = $factory->query($sql); 
        return $query;  
    }

    public function getAllVisulizationData($factory, $machineId, $type, $choose1='', $choose2='', $choose3='', $choose4='') {
        // Prep the query
       
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and hour.machine_id = $machineId";  
        }
        
        $yearQ = '';
        if($type == 'year' ) 
        {
            $yearQ = "and hour.year = $choose1";  
            $period_value = 'month';
        }
        
        $monthQ = '';
        if($type == 'month' ) 
        {
            $monthQ = "and hour.year = $choose1 and hour.month = $choose2";  
            $period_value = 'week';  
        }
        
        $weekQ = '';
        if($type == 'week' ) 
        {
            $weekQ = "and hour.year = $choose1 and hour.week = $choose3 and hour.week != 0  "; //and hour.weekday != 0 
            $period_value = 'weekday'; 
        }
        
        $dayQ = '';
        if($type == 'day' ) 
        {
            $dayQ = "and hour.year = $choose1 and hour.month = $choose2 and hour.day = $choose4"; 
            $period_value = 'hour'; 
        }
        
        $state = " and state in('running', 'waiting', 'stopped', 'off', 'nodet') ";
        $sql = "SELECT sum(duration) as countVal, $period_value FROM hour left join machine on hour.machine_id = machine.machineId WHERE machine.isDeleted = '0' $machineQ $state $yearQ $monthQ $weekQ $dayQ group by $period_value order by $period_value asc"; 
        
        $query = $factory->query($sql); 
        return $query;  
    }
    
    public function getTodayAvgRunningHour($factory) 
    {
        // Prep the query
        $todayD = ltrim(date("d"), '0');
        $todayM = date("m");
        $todayY = date("Y");
        $sql = "SELECT sum(duration) as countVal FROM hour WHERE hour.day = $todayD and hour.month = $todayM and hour.year = $todayY and state='run' "; 
        $query = $factory->query($sql);
        return $query;  
    }
    
    public function weekAvgRunningHourPerDay($factory) 
    {
        // Prep the query
        $todayW = date("W");
        $todayY = date("Y");
        $sql = "SELECT sum(duration) as countVal FROM hour WHERE hour.week = $todayW and hour.year = $todayY and state='run' "; 
        $query = $factory->query($sql);
        return $query;  
    }
    
    
    public function getAllMNotificationsCount($factory, $userId, $is_admin=1)
    {
        // Prep the query 
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and mNotification.userId = $userId";  
        }
        $sql = "SELECT count(*) as totalCount FROM mNotification left join nytt_main.user on user.userId = mNotification.userId left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotification.categoryId WHERE 1 $userQ ";  
        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    public function getMNotifications($factory, $dateValS, $dateValE, $userId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        $columnArr = array("mNotification.notificationId", "mNotification.status", "user.userName", "maintenanceTemplateCategory.mTCategoryName", "machineUser.startTime", "machineUser.endTime" ); 
        $orderCol = $columnArr[$columnIndex]; 
        
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and mNotification.userId = $userId";  
        }       
        $dateQ = " and mNotification.addedDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT machineUser.startTime, machineUser.endTime, mNotification.*, user.userName, maintenanceTemplateCategory.mTCategoryName FROM mNotification left join nytt_main.user on user.userId = mNotification.userId left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotification.categoryId left join machineUser on machineUser.userId = mNotification.userId WHERE 1 and TIMESTAMPDIFF(SECOND, machineUser.endTime, mNotification.addedDate) < 60 &&  TIMESTAMPDIFF(SECOND, machineUser.endTime, mNotification.addedDate) >= 0 $userQ $dateQ $searchQuery group by mNotification.notificationId order by $orderCol $columnSortOrder limit $start, $limit"; 
        $query = $this->factory->query($sql);
        return $query; 
    }
    
    public function getSearchMNotificationsCount($factory, $dateValS, $dateValE, $userId, $is_admin=1, $searchQuery)
    { 
        // Prep the query
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and mNotification.userId = $userId";
        }
        $dateQ = " and mNotification.addedDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        $sql = "SELECT count(*) as totalCount FROM mNotification left join nytt_main.user on user.userId = mNotification.userId left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotification.categoryId left join machineUser on machineUser.userId = mNotification.userId WHERE 1 and TIMESTAMPDIFF(SECOND, machineUser.endTime, mNotification.addedDate) < 60 &&  TIMESTAMPDIFF(SECOND, machineUser.endTime, mNotification.addedDate) >= 0 $userQ $dateQ $searchQuery ";  
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    public function getAllMachineUsersCount($factory, $userId, $is_admin=1)
    {
        // Prep the query
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUser.userId = $userId";  
        }
        
        $sql = "SELECT count(machineUser.startTime) as totalCount FROM machineUser left join nytt_main.user on user.userId = machineUser.userId left join machine on machine.machineId = machineUser.machineId WHERE 1 $userQ group by machineUser.startTime";   
        $query = $this->factory->query($sql); 
        
        return $query;  
    } 
    
    public function getMachineUsers($factory, $dateValS, $dateValE, $userId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        $columnArr = array("machineUser.activeId", "user.userName", "machine.machineName", "machineUser.isActive", "machineUser.startTime", "machineUser.endTime" ); 
        $orderCol = $columnArr[$columnIndex];
        
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUser.userId = $userId";  
        }       
        $dateQ = " and machineUser.startTime BETWEEN '".$dateValS."' and '".$dateValE."' ";
        $sql = "SELECT machineUser.*, user.userName, machine.machineName, mNotification.addedDate, mNotification.status, GROUP_CONCAT( mNotification.categoryId separator ', ') as type, GROUP_CONCAT( machine.machineName separator ', ') as machines FROM machineUser left join nytt_main.user on user.userId = machineUser.userId left join machine on machine.machineId = machineUser.machineId left join mNotification on machineUser.endTime = mNotification.addedDate WHERE 1 $userQ $dateQ $searchQuery  group by machineUser.startTime order by $orderCol $columnSortOrder limit $start, $limit";   
        $query = $this->factory->query($sql); 
        
        return $query; 
    }
    
    public function getSearchMachineUsersCount($factory, $dateValS, $dateValE, $userId, $is_admin=1, $searchQuery)
    { 
        // Prep the query
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUser.userId = $userId";  
        }
        $dateQ = " and machineUser.startTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT count(machineUser.startTime) as totalCount FROM machineUser left join nytt_main.user on user.userId = machineUser.userId left join machine on machine.machineId = machineUser.machineId WHERE 1 $userQ $dateQ $searchQuery  group by machineUser.startTime";   
        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    public function getMachineStatusText($factory, $machineId, $redStatus, $greenStatus, $yellowStatus, $whiteStatus, $blueStatus)
    {
        // Prep the query
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and machineId = $machineId"; 
        }
        $sql = "SELECT machineStateVal FROM `machineStateColorLookup` where redStatus = '$redStatus' and greenStatus = '$greenStatus' and yellowStatus = '$yellowStatus' and whiteStatus = '$whiteStatus' and blueStatus = '$blueStatus' $machineQ"; 
        
        $query = $factory->query($sql); 
        if($query->num_rows() >= 1) 
        {
            $result = $query->row()->machineStateVal; 
        } 
        else 
        {
            $result = 'Off';
        }
        return $result; 
    }
    
    public function checkOutUser($factory, $userId)
    {  
        $factory->where('userId',$userId);
        $update = array('isActive'=>'0');
        $factory->update('machineUser',$update); 
    } 
    
    function getMachineTodayStatesmultiBeta($factory, $machineId) 
    { 
        $date = date("Y-m-d");
        $sql = "SELECT color, Sec_to_time(@diff), originalTime AS endtime, IF(@diff = 0, 0, Time_to_sec(originalTime) - @diff) AS colorDuration, @diff := Time_to_sec(originalTime) FROM beta_logimagecolor, (SELECT @diff := 0) AS x WHERE machineId = $machineId and originalTime like '%$date%' union SELECT color, Sec_to_time(@diff), originalTime AS endtime, IF(@diff = 0, 0, Time_to_sec(originalTime) - @diff) AS colorDuration, @diff := Time_to_sec(originalTime) FROM beta_logimagecolor_nodetection, (SELECT @diff := 0) AS x WHERE machineId = $machineId and originalTime like '%$date%' order by endtime asc";   
        $query = $factory->query($sql);  
        $content = $query->result('array');     
        return $content;  
    } 
    
    public function formatSeconds($seconds) 
    {
            $hours = floor($seconds/3600);
            $remainder_1 = ($seconds % 3600);
            $minutes = floor($remainder_1 / 60);
            $seconds = ($remainder_1 % 60);
     
    
            if(strlen($hours) == 1) 
            {
                $hours = "0".$hours;
            }
     
            if(strlen($minutes) == 1) 
            {
                $minutes = "0".$minutes;
            }
     
            if(strlen($seconds) == 1) 
            {
                $seconds = "0".$seconds;
            }
     
            return $hours.":".$minutes.":".$seconds;
     
    }
    
    public function getDailyRedData($factory, $machineId, $choose1='', $choose2='', $choose4='') 
    {
        // Prep the query
       
        $machineQ = '';
        if($machineId > 0 ) 
        {
            $machineQ = "and a.machineId = $machineId";  
        }
        $date = $choose1."-".$choose2."-".$choose4; 
        
        $sql = "SELECT ( select duration FROM beta_logimagecolor where logId = a.durationLogId )as colorDuration, TIME_TO_SEC(a.originalTime) as timeS, DATE_FORMAT(a.originalTime, '%H:%m:%s') as originalTime FROM beta_logimagecolor as a WHERE a.originalTime like '%".$date."%' and (a.color like '%Red%' or a.color like '%red%') $machineQ order by a.originalTime asc";
        
        $query = $factory->query($sql); 
        return $query;  
    }
    
    public function getStopAnalyticsData($factory, $machineId, $choose1, $choose2, $choose4) 
    {
        // Prep the query
       
        $tableName = $machineId.'_stopped_statebeta_logimagecolor_'.$choose2.$choose1;
        $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
        if($factory->table_exists($tableName) ){ 
            $date = $choose1."-".$choose2Pad."-".$choose4;
            
            $sql = "SELECT $tableName.timeDiff as colorDuration, TIME_TO_SEC($tableName.originalTime) as timeS, $tableName.userId, notification.status, notification.comment, notification.commentFlag FROM $tableName left join notification on notification.logId = $tableName.logId WHERE $tableName.originalTime like '%".$date."%' order by $tableName.originalTime asc";
            
            $query = $factory->query($sql);
            
            return $query;  
        } 
        else 
        {
            return '';
        }
    } 
    
    public function getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration, $maxDuration, $machineId=0)
    {
        
        $tableName = $machineId.'_stopped_statebeta_logimagecolor_'.$choose2.$choose1;
        if($this->factory->table_exists($tableName) ){  
            $choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
            $choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
            
            $date = $choose1."-".$choose2Pad."-".$choose4Pad;
            $this->factory->select('notification.notificationId, notification.comment, stopT.timeDiff' );  
            $this->factory->from('notification');
            $this->factory->where('notification.commentFlag', '1'); // respondable notifications
            if($machineId > 0) {
                $this->factory->where('notification.machineId', $machineId); 
            }
            $this->factory->join($tableName.' as stopT','stopT.logId = notification.logId');   
            $this->factory->where('stopT.timeDiff > ', $minDuration);
            $this->factory->where('stopT.timeDiff <= ', $maxDuration);
            $this->factory->where('notification.type', 'Red'); 
            $this->factory->where('addedDate >=', $date." 00:00:00"); 
            $this->factory->where('addedDate <=', $date." 23:59:59"); 
            $query= $this->factory->get(); 
            $result = $query->result();
            return $result;
        } 
        else
        {
            return '';
        }
    }
    
    public function notifyOperators($factoryId, $machineId, $userList) 
    { 
        
        define('API_ACCESS_KEY', 'AIzaSyAqSh9C1pNepi9rgxv5kRHfmdGNUAZn3kM'); 
        
        if(is_array($userList) && count($userList) > 0 ) 
        { 
            $userDevice = array();
            for($y=0;$y<count($userList);$y++) 
            {
                $userDevice[$y] = $this->getUserDevice($userList[$y])->deviceToken; 
            }
            
            $machineDetail = $this->AM->getMachineDetail($this->factory, $machineId)->row(); 
            $notificationText = "Reminder of ".$machineDetail->machineName." stopped. Please explain reason for breakdown."; 
            // prep the bundle
            $msg = array
            (
                'message'   => $notificationText,
                'title'     => $notificationText,
                'subtitle'  => $notificationText,
                'vibrate'   => 1,
                'sound'     => 1,
                'machineId' => $machineId,
                'factoryId' => $factoryId
            ); 
            
            $fields = array
            (
                'registration_ids'  => $userDevice,   
                'data' => $msg
            );

            $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $result = curl_exec($ch);
            curl_close($ch);
        }
    }
    
    public function configureRedBlocks($factoryId, $update)
    { 
        $this->db->where('factoryId',$factoryId);
        $this->db->update('factory',$update);
    }


    public function get_where_with_order_by_single($select,$where, $order_key, $order_by, $table)
    {   
        $this->factory->select($select);
        $this->factory->where($where);
        $this->factory->order_by($order_key,$order_by);
        $query = $this->factory->get($table);
        return $query->row();
    }

    public function getPhoneDetail($factory, $machineId)
    {
        // Prep the query
        $factory->select('phoneId');
        $factory->from('phoneDetailLogv2');
        $factory->where('machineId', $machineId);
        $factory->order_by('phoneId', 'desc');
        $query= $factory->get();
        $result = $query->row();
        if(!isset($result)) 
        {
            $result = new stdClass;
            $result->phoneId = 0;
        }
        return $result; 
    }
    
    function checkSetApp($factoryId, $machineId) 
    {
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);
        
        $sql =  $this->factory
                    ->select('*')
                    ->where('machineId', intval($machineId))
                    ->order_by('appId', 'desc')
                    ->get('setAppLive'); 
        return $sql->row_array(); 
    }
    
    function secToMin($seconds) {
      $minutes = floor($seconds / 60);
      $seconds = $seconds % 60;
      return "$minutes:$seconds";
    }
    
    //v2 
    public function getAllMaintenanceCheckinCount($factory, $userId, $is_admin=1)
    {
        // Prep the query
       
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }
        
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0'  $userQ group by machineUserv2.activeId";  
        $query = $this->factory->query($sql); 
        
        return $query; 
    } 
    
    public function getMaintenanceCheckin($factory, $dateValS, $dateValE, $userId, $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
    {
        // Prep the query 
        
        $columnArr = array("machineUserv2.activeId", "user.userName", "machineUserv2.machineNames", "machineUserv2.isActive", "machineUserv2.startTime", "machineUserv2.endTime" ); 
        $orderCol = $columnArr[$columnIndex];
        
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }       
        $dateQ = " and machineUserv2.startTime BETWEEN '".$dateValS."' and '".$dateValE."' ";
        
        $sql = "SELECT machineUserv2.*, user.userName, mNotificationv2.addedDate, mNotificationv2.status, GROUP_CONCAT( maintenanceTemplateCategory.mTCategoryName separator ', ') as type FROM machineUserv2 left join nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0' $userQ $dateQ $searchQuery group by machineUserv2.activeId order by $orderCol $columnSortOrder limit $start, $limit";   
        $query = $this->factory->query($sql); 
        return $query; 
    }
    
    public function getSearchMaintenanceCheckinCount($factory, $dateValS, $dateValE, $userId, $is_admin=1, $searchQuery)
    { 
        // Prep the query
       
        $userQ = '';
        if($userId > 0 ) 
        {
            $userQ = "and machineUserv2.userId = $userId";  
        }
        $dateQ = " and machineUserv2.startTime BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT count(machineUserv2.activeId) as totalCount FROM machineUserv2 left join nytt_main.user on user.userId = machineUserv2.userId left join mNotificationv2 on machineUserv2.activeId = mNotificationv2.activeId  left join nytt_main.maintenanceTemplateCategory on maintenanceTemplateCategory.mTCategoryId = mNotificationv2.categoryId WHERE user.isDeleted = '0' $userQ $dateQ $searchQuery  group by machineUserv2.activeId";   
        $query = $this->factory->query($sql); 
        return $query;  
    }
    
    public function getallStackLightTypeFiles()
    {
        $this->db->select('*');
        $this->db->from('stackLightTypeFile');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    
  

    public function getMachinesOverview($factory, $isOperator, $userId)
    {
        // Prep the query
        
        $machineQ = '';
        if($isOperator == 1) {
            $this->db->select('machines');
            $this->db->from('user');
            $this->db->where('userId', $userId);
            $this->db->where('isDeleted', '0');
            $query= $this->db->get(); 
            $rowUser = $query->row();
            
            if($rowUser->machines != '') 
            {
                $machineQ = " and machine.machineId in (".$rowUser->machines.") ";   
            } 
            else 



            {
                $machineQ = " and 1 != 1 "; 
            }
        }
        $sql = "SELECT machine.*, beta_logimagecolor_overview.*, phoneDetailLogv2.phoneId, phoneDetailLogv2.batteryLevel, phoneDetailLogv2.chargeFlag
            FROM machine left join beta_logimagecolor_overview on beta_logimagecolor_overview.machineId = machine.machineId 
            left join phoneDetailLogv2 on phoneDetailLogv2.machineId = machine.machineId 
            WHERE machine.isDeleted = '0' $machineQ  order by machine.machineId";  
        $query = $factory->query($sql); 
        return $query; 
    }

    public function getAllFactoriesList()


    { 
        $this->db->select('factoryId');
        $this->db->from('factory');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get(); 
        return $query->result(); 
    }
    
    function updateNotificationLiveStatus($factoryId, $notificationId, $machineId, $flag) 


    {  
        $addField = array(
                        "notificationId"=>$notificationId,
                        "flag"=>$flag, 
                        "machineId"=>$machineId,
                        "factoryId"=>$factoryId,
                        ); 
        $this->db->insert('notificationLiveStatus',$addField); 
        return $notificationId;
        
    }
    
    public function addHourRedNotificationBeta($date)

    { 
        // Prep the query
        
        $factories = $this->getAllFactoriesList();
        for($fact=0;$fact<count($factories);$fact++) 

        {
            
            $factory = $this->load->database('factory'.$factories[$fact]->factoryId, TRUE);   
            
            $factoryData = $this->getFactoryData($factories[$fact]->factoryId); 
            
            $sql2 = "SELECT beta_logimagecolor_overview.color, machine.machineName, beta_logimagecolor_overview.logId, beta_logimagecolor_overview.machineId, TIMESTAMPDIFF(SECOND, originalTime, NOW()) as colorTime FROM beta_logimagecolor_overview left join machine on beta_logimagecolor_overview.machineId = machine.machineId WHERE beta_logimagecolor_overview.logId  in (SELECT MAX(logId) FROM beta_logimagecolor_overview GROUP BY machineId) ";  
            $query2 = $factory->query($sql2); 
            $redStatus = $query2->result();
            echo "<pre>"; print_r($redStatus);echo "</pre>";
            $dur1 = $factoryData->duration1*60;
            $dur2 = $factoryData->duration2*60;
            $dur3 = $factoryData->duration3*60;
            $dur4 = $factoryData->duration4*60;
            $dur5 = $factoryData->duration5*60;
            
            if(is_array($redStatus)) 
            { 
                for($x=0;$x<count($redStatus);$x++) 
                {
                    
                    $colorArrQ = '';
                    if($redStatus[$x]->color != 'NoData' && $redStatus[$x]->color != 'off' && $redStatus[$x]->color != 'Off') 
                    {
                        $colorArr = explode(" ",$redStatus[$x]->color);
                        for($c=0;$c<count($colorArr);$c++) {
                            if($colorArr[$c] != 'and') {
                                $colorArrQ .= " and ".strtolower($colorArr[$c])."Status = '1' "; 
                            }
                        }
                        if(!strpos($colorArrQ, 'redStatus')) 
                        {
                            $colorArrQ .= " and redStatus = '0' "; 
                        }
                        if(!strpos($colorArrQ, 'yellowStatus'))
                         {
                            $colorArrQ .= " and yellowStatus = '0' "; 
                        }
                        if(!strpos($colorArrQ, 'greenStatus')) 
                        {
                            $colorArrQ .= " and greenStatus = '0' "; 
                        }
                        if(!strpos($colorArrQ, 'blueStatus')) 
                        {
                            $colorArrQ .= " and blueStatus = '0' "; 
                        }
                        if(!strpos($colorArrQ, 'whiteStatus')) 
                        {
                            $colorArrQ .= " and whiteStatus = '0' "; 
                        } 
                        
                    } 
                    else if($redStatus[$x]->color == 'off' || $redStatus[$x]->color == 'Off') 
                    {
                        $colorArrQ .= " and greenStatus = '0' and yellowStatus = '0' and redStatus = '0' and blueStatus = '0' and whiteStatus = '0'";
                    } 
                    
                    $sqlStop = "SELECT machineStateVal from machineStateColorLookup where machineId = ".$redStatus[$x]->machineId." ".$colorArrQ; 
                    $sqlStopQ = $factory->query($sqlStop); 
                    $sqlStopRes = $sqlStopQ->row();
                    
                    if(isset($sqlStopRes) && trim($sqlStopRes->machineStateVal) == 'Stopped') 

                    { 
                        
                        $stopDuration = $redStatus[$x]->colorTime;  
                        $commentFlag = '0'; 
                        if( $stopDuration >= $dur1 && $stopDuration < $dur2 &&  $factoryData->getNotified1 == '1' ) 
                        {
                            $commentFlag = '1'; 
                        } 
                        if( $stopDuration >= $dur2 && $stopDuration < $dur3  &&  $factoryData->getNotified2 == '1' ) 
                        {
                            $commentFlag = '1';
                        } 
                        if($factoryData->duration4Check == '1' && $factoryData->duration5Check == '1') 
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur4  &&  $factoryData->getNotified3 == '1' ) 
                            {
                                $commentFlag = '1';
                            } 
                        } 
                        else if($factoryData->duration4Check == '1') 
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur4  &&  $factoryData->getNotified3 == '1' ) 
                            {
                                $commentFlag = '1';
                            } 
                        } 
                        else if($factoryData->duration5Check == '1' )
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur5  &&  $factoryData->getNotified3 == '1' ) 
                            {
                                $commentFlag = '1'; 
                            } 
                        } 
                        else if($factoryData->duration4Check == '0' && $factoryData->duration5Check == '0')  
                        { 
                            if( $stopDuration >= $dur3 &&  $factoryData->getNotified3 == '1' )
                             {
                                $commentFlag = '1'; 
                            } 
                        }
                        
                        if($factoryData->duration5Check == '1') 

                        {
                            if( $stopDuration >= $dur4 && $stopDuration < $dur5  &&  $factoryData->getNotified4 == '1' ) {
                                $commentFlag = '1'; 
                            } 
                        } 
                        else 
                        {
                            if( $stopDuration >= $dur4  &&  $factoryData->getNotified4 == '1' ) {
                                $commentFlag = '1';
                            } 
                        }
                        
                        if( $stopDuration >= $dur5 && $factoryData->duration5Check == '1' &&  $factoryData->getNotified5 == '1' )

                        {
                            $commentFlag = '1'; 
                        }
                        
                        $insertFlag = '0';
                        if( $stopDuration >= $dur1 && $stopDuration < $dur2  &&  $factoryData->notified1 == '1' ) 
                        {
                            $insertFlag = '1';
                        } 
                        if( $stopDuration >= $dur2 && $stopDuration < $dur3  &&  $factoryData->notified2 == '1' ) 
                        {
                            $insertFlag = '1';
                        } 
                        if($factoryData->duration4Check == '1' && $factoryData->duration5Check == '1') 
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur4  &&  $factoryData->notified3 == '1' ) 
                            {
                                $insertFlag = '1';
                            } 
                        } 
                        else if($factoryData->duration4Check == '1') 
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur4  &&  $factoryData->notified3 == '1' ) 
                            {
                                $insertFlag = '1';
                            } 
                        } 
                        else if($factoryData->duration5Check == '1' )
                        {
                            if( $stopDuration >= $dur3 && $stopDuration < $dur5  &&  $factoryData->notified3 == '1' ) 
                            {
                                $insertFlag = '1';
                            } 
                        } 
                        else {
                            if( $stopDuration >= $dur3 &&  $factoryData->notified3 == '1' ) {
                                $insertFlag = '1';
                            } 
                        }
                        
                        if($factoryData->duration5Check == '1') {
                            if( $stopDuration >= $dur4 && $stopDuration < $dur5  &&  $factoryData->notified4 == '1' ) {
                                $insertFlag = '1';
                            } 
                        } else {
                            if( $stopDuration >= $dur4  &&  $factoryData->notified4 == '1' ) {
                                $insertFlag = '1';
                            } 
                        }
                        
                        if( $stopDuration >= $dur5 && $factoryData->duration5Check == '1' &&  $factoryData->notified5 == '1' ) {
                            $insertFlag = '1';
                        }  
                        
                        if($insertFlag == '1') 
                        {  
                            $sqlMU = "SELECT `user`.*, (SELECT machineUserv2.isActive from `machineUserv2` where `machineUserv2`.userId = `user`.userId and FIND_IN_SET('".$redStatus[$x]->machineId."', machineUserv2.machines) order by machineUserv2.activeId desc limit 1) as isActive FROM nytt_main.`user` WHERE `user`.isDeleted = '0' and factoryId = ".$factories[$fact]->factoryId; 
                            $queryMU = $factory->query($sqlMU); 
                            $machineUser[$x] = $queryMU->result();
                            
                            $userId = ''; $z=0; 
                            echo $factory->last_query();
                            echo "<pre>"; print_r($machineUser);echo "</pre>";
                            if(is_array($machineUser[$x]) && count($machineUser[$x]) > 0 ) 
                            { 
                                $userDevice = array(); $p=0; 
                                for($y=0;$y<count($machineUser[$x]);$y++) 
                                {
                                    if($machineUser[$x][$y]->isActive != null) 
                                    {
                                        if($machineUser[$x][$y]->getNotified == '1' &&  $machineUser[$x][$y]->isActive == '1') 
                                        { 
                                            if(isset($this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken)) {
                                                $userDevice[$p] = $this->getUserDevice($machineUser[$x][$y]->userId)->deviceToken;
                                            } 
                                            else 
                                            {
                                                $userDevice[$p] = '';
                                            }
                                            $p++; 
                                        }
                                        if($z==0) 

                                        {
                                            $userId = $machineUser[$x][$y]->userId;
                                        } 
                                        else 
                                        {
                                            $userId .= ','.$machineUser[$x][$y]->userId; 
                                        }
                                        $z++;  
                                    }
                                } 
                                
                                $factory->select('notificationId, commentFlag');
                                $factory->from('notification');
                                $factory->where('isDeleted', '0');
                                $factory->where('logId', $redStatus[$x]->logId);
                                $queryN= $factory->get();
                                echo "<br />".$factory->last_query();
                                $notification[$x] = $queryN->row();  
                                
                                $notificationText = $redStatus[$x]->machineName." stopped.";
                                if($commentFlag == '1') 
                                {
                                    $notificationText .= " Please explain reason for breakdown.";
                                }
                                
                                if(!isset($notification[$x]) || (isset($notification[$x]) && $notification[$x]->commentFlag == '0' && $commentFlag == '1' )) 
                                {
                                    
                                    if(!isset($notification[$x])) 

                                    {
                                        $insert=array(
                                            'userId'=>$userId, 
                                            'machineId'=>$redStatus[$x]->machineId,  
                                            'logId'=>$redStatus[$x]->logId,  
                                            'notificationText'=>$notificationText, 
                                            'addedDate'=>$date,  
                                            'respondTime'=>$date,  
                                            'type'=>'Red',
                                            'commentFlag'=>$commentFlag
                                        );
                                            
                                        $factory->insert('notification',$insert); 
                                        echo "<br />".$factory->last_query(); 
                                        $notId = $factory->insert_id();
                                    }
                                    else 
                                    {
                                        $updateTime = $factory->where('notificationId', $notification[$x]->notificationId)->update('notification', array('commentFlag'=>$commentFlag,'notificationText'=>$redStatus[$x]->machineName." stopped.  Please explain reason for breakdown."));  
                                        $notId = $notification[$x]->notificationId; 
                                    }
                                    if($commentFlag == '1' ) 
                                    {
                                        $statusResponse  = $this->updateNotificationLiveStatus($factories[$fact]->factoryId, $notId, $redStatus[$x]->machineId, '0'); //0 for }
                                    
                                    // prep the bundle
                                    $msg = array
                                    (
                                        'message'   => $notificationText,
                                        'title'     => $notificationText,
                                        'subtitle'  => $anotificationText,
                                        'vibrate'   => 1,
                                        'sound'     => 1,
                                        'machineId' => $redStatus[$x]->machineId,
                                        'factoryId' => $factories[$fact]->factoryId,
                                        'commentFlag' => $commentFlag
                                    ); 
                                    $fields = array
                                    (
                                        'registration_ids'  => $userDevice, 
                                        'data' => $msg
                                    );

                                    $headers = array
                                    (
                                        'Authorization: key=' . API_ACCESS_KEY,
                                        'Content-Type: application/json'
                                    );
                                    echo "<pre>"; print_r($headers);echo "</pre>"; 
                                    $ch = curl_init();
                                    curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
                                    curl_setopt( $ch,CURLOPT_POST, true );
                                    curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
                                    curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
                                    curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
                                    curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
                                    curl_exec($ch);
                                    curl_close($ch);
                                    echo "<pre>"; print_r($result);echo "</pre>";
                                } 
                            }
                        }
                    } 
                }
            }
        }
    } 
    
   public function getWhere($where, $table)
    {   
        $this->factory->where($where);
        $query = $this->factory->get($table);
        return $query->result_array();
    }

    public function getWhereDB($where, $table)
    {   
        $this->db->where($where);
        $query = $this->db->get($table);
        return $query->result_array();
    }

    public function updateData($where,$data, $table)
    {   
        $this->factory->where($where);
        $this->factory->update($table,$data);
    }

    public function insertData($data, $table)
    {   
        $this->factory->insert($table,$data);
    }

    public function getallStackLightType(){
        $this->db->select('*');
        $this->db->from('stackLightType');
        $this->db->where('isDeleted', '0');
        $query= $this->db->get();
        return $query;
    }
    

     public function getallStacklighttypeVersionsLogs($factory, $dateValS, $dateValE, $stackLightTypeId , $is_admin=1, $start, $limit, $searchQuery, $columnIndex, $columnSortOrder)
     {
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }
       
      
        $dateQ = " and stackLightTypeFile.versionDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT * FROM stackLightTypeFile left join nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE nytt_main.stackLightType.isDeleted = '0' $machineQ  $dateQ $searchQuery order by $columnIndex $columnSortOrder limit $start, $limit ";  


        $query = $this->db->query($sql); 
      return $query; 
    }
    
    public function getallStacklighttypeVersionsCount($factory, $stackLightTypeId, $is_admin=1)
    {
        // Prep the query
       
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }
        
        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile WHERE 1=1 $machineQ";
        $query = $this->db->query($sql); 
         return $query;  
   
    }
    
    public function getSearchStacklighttypeVersionsCount($factory, $dateValS, $dateValE, $stackLightTypeId, $is_admin=1, $searchQuery)
    { 
      // Prep the query
       
        $machineQ = '';
        if($stackLightTypeId > 0 ) 
        {
            $machineQ = "and stackLightTypeFile.stackLightTypeId = $stackLightTypeId";  
        }
       
        $dateQ = " and stackLightTypeFile.versionDate BETWEEN '".$dateValS."' and '".$dateValE."' "; 
        
        $sql = "SELECT count(*) as totalCount FROM stackLightTypeFile left join nytt_main.stackLightType on stackLightType.stackLightTypeId = stackLightTypeFile.stackLightTypeId  WHERE nytt_main.stackLightType.isDeleted = '0' $machineQ  $dateQ $searchQuery ";  
        $query = $this->db->query($sql); 
        
        return $query; 
       
    } 

    public function getAllStackLightVersion($stackLightTypeId)
    {
        $this->db->where(array("stackLightTypeId" => $stackLightTypeId));
        $query = $this->db->get('stackLightTypeFile');
        return $query->result_array();
    }


    public function getMaxVersion($stackLightTypeId)
    {
        $this->db->where(array("stackLightTypeId" => $stackLightTypeId));
        $query = $this->db->get('stackLightTypeFile');
        return $query->num_rows();
    }


    function userLogOutOpApp($userId) 
    { 
        
        $checkIn = $this->AM->isUserCheckedIn($this->factoryId, $userId);
        if(isset($checkIn) && is_array($checkIn)) {
            $data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s')); 
            $this->AM->updateCheckOut($this->factoryId,$checkIn['activeId'],$data); 
        }
        $logKeyOpApp = '0';
        $update = array("logKeyOpApp"=>$logKeyOpApp); 
        //"logDate"=>date('Y-m-d H:i:s', $logDate), 
        $this->db->where('userId',$userId);
        $this->db->update('user',$update); 
        return $logKeyOpApp; 
    }

    function isUserCheckedIn($factoryId, $userId) 
    { 
    //final_version_1 
        
        $sql = "SELECT activeId FROM machineUserv2 WHERE userId = $userId and isActive = '1' order by activeId desc";  
        $query = $this->factory->query($sql);
        if ($query->num_rows() > 0) 
        {
            $result = $query->row_array();   
            return $result;
        }
        else 
        {
            return false;
        }   
    }

    function updateCheckOut($factoryId,$activeId,$data) 
    { 
        $this->factory->where("activeId",$activeId);
        $this->factory->update("machineUserv2",$data);
    }


    function sendNotification($message,$title,$fcmToken,$type)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=' . API_ACCESS_KEY,
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, false );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }

    function getLastActiveMachine($machineId)
    {
        $this->factory->where("machineId",$machineId);
        $this->factory->order_by('activeId','desc');
        $query = $this->factory->get('activeMachine')->row();
        return $query;
    }

    function sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId)
    {
        $data = array
            (
                'message'   => $message,
                'title'     => $title,
                'vibrate'   => 1,
                'sound'     => 1,
                'type'      => $type,
                'factoryId'      => $factoryId,
                'machineId'      => $machineId,
                'activeId'      => $activeId
            );
        $fields = array
            (
                'registration_ids'  => $fcmToken,  
                'data' => $data
            );

        $headers = array
            (
                'Authorization: key=AAAASbM1Rus:APA91bFh-5r58sSNypzlux2b3hA1uF1Y51ATYYJBmmArGOYRGzQI_DUXgCqk1qcehqSSsoaQceL4rw6q8URV4Nrq0WLNMWds0CfAoc_ORAFU6QwQo70zIpI97SJbr-CufG7WPlSbIo-e',
                'Content-Type: application/json'
            );

        $ch = curl_init();
        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
        curl_setopt( $ch,CURLOPT_POST, true );
        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
        $result = curl_exec($ch);
        curl_close($ch);
    }
}
?>
