<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Analytics extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);

		$this->load->helper('color_helper');
		
	}

	//dashboard5 function use for analytics page

	//In use
	public function dashboard_backup() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(7);
		if ($accessPoint == true) 
        {
			$listMachines = array();
			//Check user role and get machine as par user role
			if($this->AM->checkUserRole() > 0 ) 
			{
				$is_admin = $this->AM->checkUserRole();
				$listMachines = $this->AM->getallMachines($this->factory);
			} 
			else 
			{
				$is_admin = 0;
				$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
			}
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$machineView = $this->AM->getWhere(array("isDelete" => "0"), "machineView");
			$data = array(
			    'listMachines'=>$listMachines,
				'factoryData'=>$factoryData,
				'machineView' => $machineView 
			    );
			$data['accessPointViewsEdit'] = $this->AM->accessPointEdit(10);
			$data['accessPointStopView'] = $this->AM->accessPoint(8);
			$data['accessPointWaitView'] = $this->AM->accessPointEdit(9);
			$data['accessPointViewView'] = $this->AM->accessPoint(10);
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('dashboard5_backup', $data);
			$this->load->view('footer');
			$this->load->view('script_file/dashboard5_footer_backup');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	public function dashboard() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(7);
		if ($accessPoint == true) 
        {
			$listMachines = array();
			//Check user role and get machine as par user role
			if($this->AM->checkUserRole() > 0 ) 
			{
				$is_admin = $this->AM->checkUserRole();
				$listMachines = $this->AM->getallMachines($this->factory);
			} 
			else 
			{
				$is_admin = 0;
				$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
			}
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$machineView = $this->AM->getWhere(array("isDelete" => "0"), "machineView");
			$data = array(
			    'listMachines'=>$listMachines,
				'factoryData'=>$factoryData,
				'machineView' => $machineView 
			    );
			$data['accessPointViewsEdit'] = $this->AM->accessPointEdit(10);
			$data['accessPointStopView'] = $this->AM->accessPoint(8);
			$data['accessPointWaitView'] = $this->AM->accessPointEdit(9);
			$data['accessPointViewView'] = $this->AM->accessPoint(10);
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('dashboard', $data);
			$this->load->view('footer');
			$this->load->view('script_file/dashboard_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	public function parts_analytics() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(7);
		if ($accessPoint == true) 
        {
			$listMachines = array();
			//Check user role and get machine as par user role
			if($this->AM->checkUserRole() > 0 ) 
			{
				$is_admin = $this->AM->checkUserRole();
				$listMachines = $this->AM->getallMachines($this->factory);
			} 
			else 
			{
				$is_admin = 0;
				$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
			}
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$machineView = $this->AM->getWhere(array("isDelete" => "0"), "machineView");
			$data = array(
			    'listMachines'=>$listMachines,
				'factoryData'=>$factoryData,
				'machineView' => $machineView 
			    );
			$data['accessPointViewsEdit'] = $this->AM->accessPointEdit(10);
			$data['accessPointStopView'] = $this->AM->accessPoint(8);
			$data['accessPointWaitView'] = $this->AM->accessPointEdit(9);
			$data['accessPointViewView'] = $this->AM->accessPoint(10);
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('parts_analytics', $data);
			$this->load->view('footer');
			$this->load->view('script_file/parts_analytics_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	//dashboard5_pagination use for get analytics graph data
	public function dashboard5_pagination()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		//Check show all data or specific machine data
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
				$machineNoStacklight = $this->AM->getWhereSingle(array("isDeleted" => '0','machineId' => $machineId), 'machine');
				//print_r($machineNoStacklight);exit;
				$isDataGet = $machineNoStacklight->isDataGet;
				$noStacklight = $machineNoStacklight->noStacklight;
				$machineCount = 1;
			}else
			{
				//$machineCount = $this->AM->getAnalyticsMachineCount();
				$noStacklight = "0";
			}
		}

		$viewId = $this->input->post('viewId');
		if ($viewId == "none") 
		{
			$viewId = 0;
			$viewStartTime = "";
			$viewEndTime = "";
			$breakViewHour = array();
		}else
		{
			$view = $this->AM->getWhereSingle(array("viewId" => $viewId),"machineView");
			$startTime = date('H',strtotime($view->startTime));
			if ($view->endTime == "24:00:00") 
			{
				$endTime = 24;
			}else
			{
				$endTime = date('H',strtotime($view->endTime));
			}
			$viewStartTime = intVal(($startTime == "00") ? "0" : ltrim($startTime, '0'));
			$viewEndTime = intVal(ltrim($endTime, '0'));
			$machineBreakViewData = $this->AM->getWhere(array("viewId" => $viewId),"machineBreakView");

			foreach ($machineBreakViewData as $key => $value) 
			{
				$startBreakTime = date('H',strtotime($value['startTime']));
				$endBreakTime = date('H',strtotime($value['endTime']));

				for ($p=$startBreakTime; $p < $endBreakTime; $p++) 
				{
					$breakViewHour[] = intVal(($p == "00") ? "0" : ltrim($p, '0'));
				}
			}
		}

		
		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');

		$currentOrderPart = $this->AM->getWhereSingleWithOrderBy(array("currentOrderStatus" => "1","machineId" => $machineId,"isDelete" => "0"),"updateTime","asc","machineParts");

		if (!empty($currentOrderPart)) 
		{
			$currentOrderPartName = $currentOrderPart->partsName;
		}else
		{
			$currentOrderPartName = "";
		}
		$machinePartsData = $this->AM->getMachinePartsData($machineId,$choose1,$choose2,$viewStartTime,$viewEndTime,$breakViewHour,$isDataGet);
		$machinePartsFinishData = $this->AM->getMachinePartsFinishData($machineId,$choose1,$choose2);
		$dayPartsProduce = $machinePartsData->dayPartsProduce;
		$scrapParts = !empty($machinePartsFinishData->scrapParts) ? $machinePartsFinishData->scrapParts : 0;
		
		if (!empty($machinePartsData) && $machinePartsData->dayThrouputMaxCount > 0) 
		{

			$goodParts = floor($dayPartsProduce);
			if (!empty($machinePartsData->dayProductivity)) 
			{
				if ($isDataGet == "0" && !empty($machinePartsData->dayProductivity)) 
				{
					$machinePartsData->dayProductivityText = ($machinePartsData->dayRunningTime * 100) / $machinePartsData->dayProductionTime;
				}else
				{
					$machinePartsData->dayProductivityText = 0;
				}
			}else
			{
				$machinePartsData->dayProductivityText = 0;
			}

			$machinePartsData->dayRunningTimeText = $this->secondsToHoursOnePoint($machinePartsData->dayRunningTime);
			$machinePartsData->dayProductionTimeText = $this->secondsToHoursOnePoint($machinePartsData->dayProductionTime);
		}else
		{
			$goodParts = "0";
			$machinePartsData =  new stdClass();

			$machinePartsData->dayProductionTimeText = "0";
			$machinePartsData->dayThrouputMaxText = "0";
			$machinePartsData->dayRunningTimeText = "0";
			$machinePartsData->dayThrouputMax = "0";
			$machinePartsData->dayProductionTime = "0";
			$machinePartsData->dayCycleMax = "0";
			$machinePartsData->dayThrouputActual = "0";
			$machinePartsData->dayProductivity = "0";
			$machinePartsData->dayRunningTime = "0";
			$machinePartsData->dayCycleActual = "0";
			$machinePartsData->dayProductivityText = 0;
			$machinePartsData->dayThrouputActualText = 0;
			$machinePartsData->dayThrouputMaxCount = "1";
		}

		//print_r($machinePartsData);exit;

		//Get machine production state running data as par selecte filter
		$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionRunning = $resultActualProductionRunning->countVal;
		//Get machine production state waiting data as par selecte filter
		$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
		//Get machine production state stopped data as par selecte filter
		$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionStopped = $resultActualProductionStopped->countVal;
		//Get machine production state off data as par selecte filter
		$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionOff = $resultActualProductionOff->countVal;
		//Get machine production state nodet data as par selecte filter
		$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNodet = $resultActualProductionNodet->countVal;
		//Get machine production state noStacklight data as par selecte filter
		$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;


		$ActualProductionRunningText = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
		$ActualProductionWaitingText = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
		$ActualProductionStoppedText = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
		$ActualProductionOffText = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
		$ActualProductionNodetText = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
		$ActualProductionNoStacklightText = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;

		$ActualProductionText = $ActualProductionRunningText + $ActualProductionWaitingText + $ActualProductionStoppedText + $ActualProductionOffText + $ActualProductionNodetText + $ActualProductionNoStacklightText;

		if ($isDataGet == "1")
		{
			$machinePartsData->dayProductivityText = $machinePartsData->dayProductivityIo;
			$machinePartsData->dayProductionTimeText = $this->convertValueOnePoint($ActualProductionText);
		}

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToDay($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToDay($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToDay($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToDay($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToDay($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToDay($ActualProductionNoStacklight) : 0;
		}

		$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;

		

		//Get machine setup state running data as par selecte filter
		$resultSetupRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupRunning = $resultSetupRunning->countVal;
		//Get machine setup state waiting data as par selecte filter
		$resultSetupWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupWaiting = $resultSetupWaiting->countVal;
		//Get machine setup state stopped data as par selecte filter
		$resultSetupStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupStopped = $resultSetupStopped->countVal;
		//Get machine setup state off data as par selecte filter
		$resultSetupOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupOff = $resultSetupOff->countVal;
		//Get machine setup state nodet data as par selecte filter
		$resultSetupNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		//Get machine setup state noStacklight data as par selecte filter
		$resultSetupNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupNoStacklight = $resultSetupNoStacklight->countVal;

		$SetupNodet = $resultSetupNodet->countVal;


		$SetupRunningText = !empty($SetupRunning) ? $this->secondsToHours($SetupRunning) : 0;
		$SetupWaitingText = !empty($SetupWaiting) ? $this->secondsToHours($SetupWaiting) : 0;
		$SetupStoppedText = !empty($SetupStopped) ? $this->secondsToHours($SetupStopped) : 0;
		$SetupOffText = !empty($SetupOff) ? $this->secondsToHours($SetupOff) : 0;
		$SetupNodetText = !empty($SetupNodet) ? $this->secondsToHours($SetupNodet) : 0;
		$SetupNoStacklightText = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;

		$SetupText = $SetupRunningText + $SetupWaitingText + $SetupStoppedText + $SetupOffText + $SetupNodetText + $SetupNoStacklightText;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$SetupRunning = !empty($SetupRunning) ? $this->secondsToHours($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ? $this->secondsToHours($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ? $this->secondsToHours($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ? $this->secondsToHours($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ? $this->secondsToHours($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{	
			//Converts seconds to day
			$SetupRunning = !empty($SetupRunning) ?  $this->secondsToDay($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ?  $this->secondsToDay($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ?  $this->secondsToDay($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ?  $this->secondsToDay($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ?  $this->secondsToDay($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}

		$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
		//Get machine no production state running data as par selecte filter
		$resultNoProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionRunning = $resultNoProductionRunning->countVal;
		//Get machine no production state waiting data as par selecte filter
		$resultNoProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionWaiting = $resultNoProductionWaiting->countVal;
		//Get machine no production state stopped data as par selecte filter
		$resultNoProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionStopped = $resultNoProductionStopped->countVal;
		//Get machine no production state off data as par selecte filter
		$resultNoProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionOff = $resultNoProductionOff->countVal;
		//Get machine no production state nodet data as par selecte filter
		$resultNoProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionNodet = $resultNoProductionNodet->countVal;
		//Get machine no production state noStacklight data as par selecte filter
		$resultNoProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

		$NoProductionRunningText =  !empty($NoProductionRunning) ? $this->secondsToHours($NoProductionRunning) : 0;
		$NoProductionWaitingText =  !empty($NoProductionWaiting) ? $this->secondsToHours($NoProductionWaiting) : 0;
		$NoProductionStoppedText =  !empty($NoProductionStopped) ? $this->secondsToHours($NoProductionStopped) : 0;
		$NoProductionOffText =  !empty($NoProductionOff) ? $this->secondsToHours($NoProductionOff) : 0;
		$NoProductionNodetText =  !empty($NoProductionNodet) ? $this->secondsToHours($NoProductionNodet) : 0;
		$NoProductionNoStacklightText =  !empty($NoProductionNoStacklight) ? $this->secondsToHours($NoProductionNoStacklight) : 0;

		$NoProductionText = $NoProductionRunningText + $NoProductionWaitingText + $NoProductionStoppedText + $NoProductionOffText + $NoProductionNodetText + $NoProductionNoStacklightText;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$NoProductionRunning =  !empty($NoProductionRunning) ? $this->secondsToHours($NoProductionRunning) : 0;
			$NoProductionWaiting =  !empty($NoProductionWaiting) ? $this->secondsToHours($NoProductionWaiting) : 0;
			$NoProductionStopped =  !empty($NoProductionStopped) ? $this->secondsToHours($NoProductionStopped) : 0;
			$NoProductionOff =  !empty($NoProductionOff) ? $this->secondsToHours($NoProductionOff) : 0;
			$NoProductionNodet =  !empty($NoProductionNodet) ? $this->secondsToHours($NoProductionNodet) : 0;
			$NoProductionNoStacklight =  !empty($NoProductionNoStacklight) ? $this->secondsToHours($NoProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$NoProductionRunning = !empty($NoProductionRunning) ?  $this->secondsToDay($NoProductionRunning) : 0;
			$NoProductionWaiting = !empty($NoProductionWaiting) ?  $this->secondsToDay($NoProductionWaiting) : 0;
			$NoProductionStopped = !empty($NoProductionStopped) ?  $this->secondsToDay($NoProductionStopped) : 0;
			$NoProductionOff = !empty($NoProductionOff) ?  $this->secondsToDay($NoProductionOff) : 0;
			$NoProductionNodet = !empty($NoProductionNodet) ?  $this->secondsToDay($NoProductionNodet) : 0;
			$NoProductionNoStacklight = !empty($NoProductionNoStacklight) ?  $this->secondsToDay($NoProductionNoStacklight) : 0;
		}

		$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

		$Running = array();
		$Waiting = array();
		$Stopped = array();
		if ($choose1 == "day") 
		{
			$xAxisName = Hours;
			$yAxisName = seconds;

			if ($viewId == 0) 
			{
				$xAxisData = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
			}else
			{
				$xAxisData = array();
				for ($i=$viewStartTime - 1; $i < $viewEndTime; $i++) { 
					$xAxisData[] = $i + 1;
				}
			}


			
				//Get running hour data
				if (!empty($viewEndTime)) 
				{
					for ($i=$viewStartTime; $i < $viewEndTime; $i++) 
					{
						$machineBreakView = $this->AM->getWhere(array("viewId" => $viewId,"HOUR(startTime) <=" => $i,"HOUR(endTime) >" => $i),"machineBreakView");
						if (!empty($machineBreakView)) 
						{
						//echo $this->factory->last_query();exit;
							$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$RunningBreak[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

							//Get waiting hour data
							$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
							$WaitingBreak[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

							//Get stopped hour data
							$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
							$StoppedBreak[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

							//Get off hour data
							$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
							$OffBreak[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

							//Get nodet hour data
							$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
							$NodetBreak[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

							//Get setup hour data
							$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
							$MachineSetupBreak[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

							//Get no_producation hour data
							$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
							$MachineNoProducationBreak[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

							$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$MachineActualProducationBreak[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

							$Running[] = "0";
							$Waiting[] = "0";
							$Stopped[] = "0";
							$Off[] = "0";
							$Nodet[] = "0";
							$MachineSetup[] = "0";
							$MachineNoProducation[] = "0";
							$MachineActualProducation[] = "0";
						}else
						{
							$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

							//Get waiting hour data
							$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
							$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

							//Get stopped hour data
							$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
							$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

							//Get off hour data
							$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
							$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

							//Get nodet hour data
							$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
							$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

							//Get setup hour data
							$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
							$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

							//Get no_producation hour data
							$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
							$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

							$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

							$RunningBreak[] = "0";
							$WaitingBreak[] = "0";
							$StoppedBreak[] = "0";
							$OffBreak[] = "0";
							$NodetBreak[] = "0";
							$MachineSetupBreak[] = "0";
							$MachineNoProducationBreak[] = "0";
							$MachineActualProducationBreak[] = "0";
						}
					}
				}
				else
				{
					for ($i=0; $i < 24; $i++) 
					{ 
						$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
						$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

						//Get waiting hour data
						$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
						$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

						//Get stopped hour data
						$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
						$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

						//Get off hour data
						$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
						$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

						//Get nodet hour data
						$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
						$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

						//Get setup hour data
						$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
						$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

						//Get no_producation hour data
						$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
						$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

						$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
						$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

						$RunningBreak[] = "0";
						$WaitingBreak[] = "0";
						$StoppedBreak[] = "0";
						$OffBreak[] = "0";
						$NodetBreak[] = "0";
						$MachineSetupBreak[] = "0";
						$MachineNoProducationBreak[] = "0";
						$MachineActualProducationBreak[] = "0";
					}
				}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23])
			)));
		}
		elseif ($choose1 == "weekly") 
		{
			$xAxisName = Day;
			$yAxisName = Hours;
			$xAxisData = array();
        	
			$choose2 = explode("/", $choose2);
        	$week = $choose2[0];
        	$year = $choose2[1];
        
	        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
	        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
	        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
	        //$date_for_monday = date( 'Y-m-d', strtotime($date_for_monday ."-7 days") );

	        for ($i=0; $i < 7; $i++) 
	        { 
	        	$weekDate = date('Y-m-d',strtotime($date_for_monday . "+". $i." days"));
	        	$weekDateName = date('d-M-Y',strtotime($date_for_monday . "+". $i." days"));
	        	$xAxisData[] = $weekDateName;
		
				//Get running hour data
	        	$runningQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningSecond);
				//Get waiting hour data
				$waitingQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingSecond);
		
				//Get stopped hour data
				$stoppedQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedSecond);
		
				//Get off hour data
				$offQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffSecond);
		
				//Get nodet hour data
				$nodetQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetSecond);
		
				//Get setup hour data
				$machineSetupQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupSecond);
		
				//Get no_producation hour data
				$machineNoProducationQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getWeekDataActualProducation($machineId,$weekDate,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";
	        }

	        $maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6])
			)));

		}
		elseif ($choose1 == "monthly") 
		{
			$xAxisData = array();
			$xAxisName = Day;
			$yAxisName = Hours;
			$chooseExplode = explode(" ", $choose2);
            $month = $chooseExplode[0];

        	if ($month == "Jan") 
        	{
        		$month = "1";
        	}
        	elseif ($month == "Feb") 
        	{
        		$month = "2";
        	}
        	elseif ($month == "Mar") 
        	{
        		$month = "3";
        	}
        	elseif ($month == "Apr") 
        	{
        		$month = "4";
        	}
        	elseif ($month == "May") 
        	{
        		$month = "5";
        	}
        	elseif ($month == "Jun") 
        	{
        		$month = "6";
        	}
        	elseif ($month == "Jul") 
        	{
        		$month = "7";
        	}
        	elseif ($month == "Aug") 
        	{
        		$month = "8";
        	}
        	elseif ($month == "Sep") 
        	{
        		$month = "9";
        	}
        	elseif ($month == "Oct") 
        	{
        		$month = "10";
        	}
        	elseif ($month == "Nov") 
        	{
        		$month = "11";
        	}
        	elseif ($month == "Dec") 
        	{
        		$month = "12";
        	}

            $year = $chooseExplode[1];
			$maxDays=date('t',strtotime('01-'.$month.'-'.$year));
			for($w=1;$w<=$maxDays;$w++) 
			{
				$xAxisData[] = "Day ".$w;


				$runningQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningMonthSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				/*if ($w == 8) {
				print_r($runningQuery);exit;
					echo $this->secondsToHours($RunningMonthSecond);exit;
				}*/
				$Running[] = $this->secondsToHours($RunningMonthSecond);
				
				$waitingQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingMonthSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingMonthSecond);

				$stoppedQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedMonthSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedMonthSecond);

				$offQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffMonthSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffMonthSecond);

				$nodetQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetMonthSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetMonthSecond);

				$machineSetupQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupMonthSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupMonthSecond);

				$machineNoProducationQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationMonthSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationMonthSecond);

				$machineActualProducationQuery = $this->AM->getMonthDataActualProducation($machineId,$w,$month,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";


			}
			
			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23]),
				($Running[24] + $Waiting[24] + $Stopped[24] + $Off[24] + $Nodet[24] + $MachineSetup[24] + $MachineNoProducation[24] + $MachineActualProducation[24]),
				($Running[25] + $Waiting[25] + $Stopped[25] + $Off[25] + $Nodet[25] + $MachineSetup[25] + $MachineNoProducation[25] + $MachineActualProducation[25]),
				($Running[26] + $Waiting[26] + $Stopped[26] + $Off[26] + $Nodet[26] + $MachineSetup[26] + $MachineNoProducation[26] + $MachineActualProducation[26]),
				($Running[27] + $Waiting[27] + $Stopped[27] + $Off[27] + $Nodet[27] + $MachineSetup[27] + $MachineNoProducation[27] + $MachineActualProducation[27]),
				($Running[28] + $Waiting[28] + $Stopped[28] + $Off[28] + $Nodet[28] + $MachineSetup[28] + $MachineNoProducation[28] + $MachineActualProducation[28]),
				($Running[29] + $Waiting[29] + $Stopped[29] + $Off[29] + $Nodet[29] + $MachineSetup[29] + $MachineNoProducation[29] + $MachineActualProducation[29]),
				($Running[30] + $Waiting[30] + $Stopped[30] + $Off[30] + $Nodet[30] + $MachineSetup[30] + $MachineNoProducation[30] + $MachineActualProducation[30])
			)));

		}
		elseif ($choose1 == "yearly") 
		{
			$xAxisName = MONTH;
			$yAxisName = Day;
			$xAxisData = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			for ($i=1; $i < 13; $i++) 
			{ 
				$runningQuery = $this->AM->getYearData($machineId,$i,$choose2,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToDay($RunningSecond);

				$waitingQuery = $this->AM->getYearData($machineId,$i,$choose2,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToDay($WaitingSecond);

				$stoppedQuery = $this->AM->getYearData($machineId,$i,$choose2,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToDay($StoppedSecond);

				$offQuery = $this->AM->getYearData($machineId,$i,$choose2,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToDay($OffSecond);

				$nodetQuery = $this->AM->getYearData($machineId,$i,$choose2,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToDay($NodetSecond);

				$machineSetupQuery = $this->AM->getYearData($machineId,$i,$choose2,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToDay($MachineSetupSecond);

				$machineNoProducationQuery = $this->AM->getYearData($machineId,$i,$choose2,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToDay($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getYearDataActualProducation($machineId,$i,$choose2,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToDay($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";
			}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11])
			)));
		}

		if (!empty($ActualProduction))
		{
			$ActualProductionLength = intval("-45");
		}
		else
		{
			$ActualProductionLength = intval("-20");
		}
		if (!empty($Setup))
		{
			$SetupLength = intval("-45");
		}
		else
		{
			$SetupLength = intval("-20");
		}
		if (!empty($NoProduction))
		{
			$NoProductionLength = intval("-45");
		}
		else
		{
			$NoProductionLength = intval("-20");
		}

		if ($choose1 == "day") 
		{
			$where = array("date(machinePartsFinish.insertTime) >= " => date('Y-m-d',strtotime($choose2)),"date(machinePartsFinish.partStartTime) <=" => date('Y-m-d',strtotime($choose2)),"machinePartsFinish.machineId" => $machineId);
			$whereOr = array();
			$whereOrder = array("machineId" => $machineId,"date(updateTime) <= " => date('Y-m-d',strtotime($choose2)),"currentOrderStatus" => "1");
			$maxHour = "24";
		}elseif ($choose1 == "weekly") 
		{
			$choose2 = explode("/", $this->input->post('choose2'));
            $week = $choose2[0];
            $year = $choose2[1];
			$weekDate = $this->getStartAndEndDate($week,$year);
			$where = array("YEAR(machinePartsFinish.insertTime)" => $year,"machinePartsFinish.machineId" => $machineId);
			$whereOr = array("WEEK(machinePartsFinish.insertTime)" => $week,"WEEK(machinePartsFinish.partStartTime)" => $week);
			$whereOrder = array("machineId" => $machineId,"WEEK(updateTime) <=" => $week,"YEAR(updateTime) <=" => $year,"currentOrderStatus" => "1");
			$maxHour = "168";
		}elseif ($choose1 == "monthly") 
		{
			$choose2 = explode(" ", $choose2);
            $month = $choose2[0];
            $month = (date('m',strtotime($month)) - 1);
            $year = $choose2[1];
			$where = array("YEAR(machinePartsFinish.insertTime)" => $choose2[1],"machinePartsFinish.machineId" => $machineId);
			$whereOr = array("MONTH(machinePartsFinish.insertTime)" => date('m',strtotime($choose2[0])),"MONTH(machinePartsFinish.partStartTime)" => date('m',strtotime($choose2[0])));
			$whereOrder = array("machineId" => $machineId,"MONTH(updateTime) <=" => date('m',strtotime($choose2[0])),"YEAR(updateTime) <=" => $year,"currentOrderStatus" => "1");
			$monthDayCount = cal_days_in_month(CAL_GREGORIAN,$month,$year);
			$maxHour = 24 * $monthDayCount;
		}elseif ($choose1 == "yearly") 
		{
			$where = array("machinePartsFinish.machineId" => $machineId);
			$whereOr = array("YEAR(machinePartsFinish.insertTime)" => $choose2,"YEAR(machinePartsFinish.partStartTime)" => $choose2);
			$whereOrder = array("machineId" => $machineId,"YEAR(updateTime) <=" => $choose2,"currentOrderStatus" => "1");
			$maxHour = "8760";
		}


		$totalGoodPart[] = 0;
		$totalScrapParts[] = 0;

		//print_r($whereOrder);exit;
		$machinePartsCur = $this->AM->getWhereWithOrderBy($whereOrder,'updateTime','asc',"machineParts");

		$ordersHtml = "";
		

		$select = "machineParts.*,machinePartsFinish.*";
		$join = array("machineParts" => "machineParts.machinePartsId = machinePartsFinish.machinePartsId");
		$machinePartsFinish = $this->AM->getWhereJoinWithOr($select,$where,$whereOr,$join,"machinePartsFinish");

		$count = count($machinePartsFinish) - 1;
		$CycleTimeFlag = "";
		foreach ($machinePartsFinish as $key => $value) 
		{
			$finishOrderTime = $value['insertTime'];

			$patrsCycleTime = $value['patrsCycleTime'];

			if ($patrsCycleTime > 0 && $CycleTimeFlag != true) 
			{
				$CycleTimeFlag = false;
			}else
			{
				$CycleTimeFlag = true;
			}


			if ($count > 0) 
			{	
				$nextFinishOrderTime = $machinePartsFinish[$key - 1]['insertTime'];
			}

			$dispayDate = date('Y-m-d H:i:s',strtotime($value['partStartTime']));

			if (!empty($nextFinishOrderTime)) 
			{
				$startTimeRe = $this->AM->getLastProductionTime($value['machinePartsId'],$nextFinishOrderTime,$finishOrderTime);

				$startTimeDD = $startTimeRe->insertTime;

				if (!empty($startTimeRe)) {
					$startTimeRu = date('Y-m-d H:i:s',strtotime($startTimeDD));
				}else
				{
					$startTimeRu = date('Y-m-d H:i:s');
				}

			}else
			{
				$startTimeRe = $this->AM->getLastProductionTimeFinalOrder($value['machinePartsId'],$finishOrderTime);

				$startTimeDD = $startTimeRe->insertTime;
				if (!empty($startTimeRe)) {
					$startTimeRu = date('Y-m-d H:i:s',strtotime($startTimeDD));
				}else
				{
					$startTimeRu = date('Y-m-d H:i:s');
				}
			}

			/*if (empty($value['correctParts'])) 
			{
				$runningTime = $this->AM->getMachineRunnignWaitingTimeWithEndTime($value['machineId'],$startTimeRu,$value['insertTime']);

				$totalParts = floor($runningTime->timeDiff / $value['patrsCycleTime']);
			}else
			{*/
				$totalParts = $value['correctParts'];
			/*}*/
			

			$totalGoodPart[] = $totalParts;
			$totalScrapParts[] = $value['scrapParts'];

			if (!empty($value['userId'])) 
			{
				$user = $this->AM->getWhereDBSingle(array("userId" => $value['userId']),"user");
				$userImage = $user->userImage;
				$userName = $user->userName;
			}else
			{
				$userImage = "34.jpg";
				$userName = "---";
			}
			$userImageParame = "'".$userImage."'";
			$userNameParame = "'".$userName."'";

			$setupTime = $this->AM->getMachineSetupTime($value['machineId'],$startTimeRu,$value['insertTime']);

			if (!empty($setupTime)) 
			{
				$hours = floor($setupTime->timeDiff / 3600);
				$minutes = floor(($setupTime->timeDiff / 60) % 60);
				$seconds = $setupTime->timeDiff % 60;

				$hours = str_pad($hours,2,"0",STR_PAD_LEFT);
				$minutes = str_pad($minutes,2,"0",STR_PAD_LEFT);
				$seconds = str_pad($seconds,2,"0",STR_PAD_LEFT);

				$setupTimeTook = "$hours:$minutes:$seconds";
			}else
			{
				$setupTimeTook = "00:00:00";
			}

			$ordersHtml .= '<tr> 
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsNumber'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['ManufacturingOrderId'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:700;color:#124D8D;">'.$value['partsName'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsOperation'].'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'. $dispayDate .'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['insertTime'])).'</td>
                    </tr> ';
		}

		$machinePartsCurCheckValue = true;
	
		foreach ($machinePartsCur as $key => $value) 
		{
			if ($key == "0") 
			{
				$insertTime = $value['updateTime'];
				$patrsCycleTime = $value['patrsCycleTime'];

				if ($patrsCycleTime > 0 && $CycleTimeFlag != true) 
				{
					$CycleTimeFlag = false;
				}else
				{
					$CycleTimeFlag = true;
				}
				$runningTime = $this->AM->getMachineRunnignWaitingTime($value['machineId'],$insertTime);
				if (!empty($runningTime->timeDiff) && !empty($value['patrsCycleTime'])) 
				{
					$totalParts = floor($runningTime->timeDiff / $value['patrsCycleTime']);
				}else
				{
					$totalParts = 0;
				}

				$totalGoodPart[] = $totalParts;
				$totalScrapParts[] = 0;
				if (!empty($value['userId'])) 
				{
					$user = $this->AM->getWhereDBSingle(array("userId" => $value['userId']),"user");
					$userImage = $user->userImage;
					$userName = $user->userName;
				}else
				{
					$userImage = "34.jpg";
					$userName = "---";
				}
				$userImageParame = "'".$userImage."'";
				$userNameParame = "'".$userName."'";

				$setupTime = $this->AM->getMachineSetupTime($value['machineId'],$insertTime,"");

				if (!empty($setupTime)) 
				{
					$hours = floor($setupTime->timeDiff / 3600);
					$minutes = floor(($setupTime->timeDiff / 60) % 60);
					$seconds = $setupTime->timeDiff % 60;

					$hours = str_pad($hours,2,"0",STR_PAD_LEFT);
					$minutes = str_pad($minutes,2,"0",STR_PAD_LEFT);
					$seconds = str_pad($seconds,2,"0",STR_PAD_LEFT);

					$setupTimeTook = "$hours:$minutes:$seconds";
				}else
				{
					$setupTimeTook = "00:00:00";
				}

				$partsNumberParame = "'".$value['partsNumber']."'";
				$partsNameParame = "'".$value['partsName']."'";
				$partsOperationParame = "'".$value['partsOperation']."'";
				$startDateParame = "'".date('Y-m-d H:i:s',strtotime($value['reportedPartTime']))."'";
				$endDateParame = "'---'";
				$correctPartsParame = "'".$totalParts."'";
				$scrapPartsParame = "'0'";
				$timeTookParame = "'".$setupTimeTook."'";
				
				$createDateParame = "'".date('Y-m-d',strtotime($value['updateTime']))."'";


				$ordersHtml .= '<tr>
	                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsNumber'].'</td>
	                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['ManufacturingOrderId'].'</td>
	                    <td style="font-weight:700;padding: 5px 0px 0px 5px;color:#124D8D;">'.$value['partsName'].'</td>
	                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsOperation'].'</td>
	                    <td style="font-weight:500;padding: 5px 0px 0px 0px;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['reportedPartTime'])).'</td>
	                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;text-align: center;">---</td>
	                </tr> ';

	            $machinePartsCurCheckValue = false;
            }
		}

		if (empty($machinePartsFinish) &&  $machinePartsCurCheckValue == true) 
		{
			$CycleTimeFlag = true;
			$ordersHtml .= '<tr>
                            <td colspan="8" style="padding: 10px 0px!important;text-align: center;color:black;">'.Nomatchingrecordsfound.'</td>
                           
                        </tr> ';
		}

	 	

		$json = array(
				"maxHour" => $maxHour,
				"ordersData" => $ordersHtml,
				"currentOrderPartName" => $currentOrderPartName,
				"CycleTimeFlag" => $CycleTimeFlag,
				"ActualProductionRunning" => !empty($ActualProductionRunning) ? $ActualProductionRunning : "0",
				"ActualProductionWaiting" => !empty($ActualProductionWaiting) ? $ActualProductionWaiting : "0",
				"ActualProductionStopped" => !empty($ActualProductionStopped) ? $ActualProductionStopped : "0",
				"ActualProductionOff" => !empty($ActualProductionOff) ? $ActualProductionOff : "0",
				"ActualProductionNodet" => !empty($ActualProductionNodet) ? $ActualProductionNodet : "0",
				"ActualProductionNoStacklight" => !empty($ActualProductionNoStacklight) ? $ActualProductionNoStacklight : "0",
				"ActualProduction" => !empty($ActualProduction) ? $ActualProduction : "0",
				"ActualProductionLength" => $ActualProductionLength,
				"SetupRunning" => !empty($SetupRunning) ? $SetupRunning : "0",
				"SetupWaiting" => !empty($SetupWaiting) ? $SetupWaiting : "0",
				"SetupStopped" => !empty($SetupStopped) ? $SetupStopped : "0",
				"SetupOff" => !empty($SetupOff) ? $SetupOff : "0",
				"SetupNodet" => !empty($SetupNodet) ? $SetupNodet : "0",
				"Setup" => !empty($Setup) ? $Setup : "0",
				"SetupLength" => $SetupLength,
				"NoProductionRunning" => !empty($NoProductionRunning) ? $NoProductionRunning : "0",
				"NoProductionWaiting" => !empty($NoProductionWaiting) ? $NoProductionWaiting : "0",
				"NoProductionStopped" => !empty($NoProductionStopped) ? $NoProductionStopped : "0",
				"NoProductionOff" => !empty($NoProductionOff) ? $NoProductionOff : "0",
				"NoProductionNodet" => !empty($NoProductionNodet) ? $NoProductionNodet : "0",
				"NoProduction" => !empty($NoProduction) ? $NoProduction : "0",
				"NoProductionLength" => $NoProductionLength,
				"xAxisName" => $xAxisName,
				"yAxisName" => $yAxisName,
				"xAxisData" => $xAxisData,
				"Running" => $Running,
				"Waiting" => $Waiting,
				"Stopped" => $Stopped,
				"Off" => $Off,
				"Nodet" => $Nodet,
				"MachineSetup" => $MachineSetup,
				"MachineNoProducation" => $MachineNoProducation,
				"MachineActualProducation" => $MachineActualProducation,
				"RunningBreak" => $RunningBreak,
				"WaitingBreak" => $WaitingBreak,
				"StoppedBreak" => $StoppedBreak,
				"OffBreak" => $OffBreak,
				"NodetBreak" => $NodetBreak,
				"MachineSetupBreak" => $MachineSetupBreak,
				"MachineNoProducationBreak" => $MachineNoProducationBreak,
				"MachineActualProducationBreak" => $MachineActualProducationBreak,
				"maxValue" => $maxValue,
				"noStacklight" => $noStacklight,
				"machineId" => $machineId,
				"NonProductiveTime" => $Setup + $NoProduction,
				"ActualProductionRunningText" => !empty($ActualProductionRunningText) ? $ActualProductionRunningText : "0",
				"ActualProductionWaitingText" => !empty($ActualProductionWaitingText) ? $ActualProductionWaitingText : "0",
				"ActualProductionStoppedText" => !empty($ActualProductionStoppedText) ? $ActualProductionStoppedText : "0",
				"ActualProductionOffText" => !empty($ActualProductionOffText) ? $ActualProductionOffText : "0",
				"ActualProductionNodetText" => !empty($ActualProductionNodetText) ? $ActualProductionNodetText : "0",
				"ActualProductionNoStacklightText" => !empty($ActualProductionNoStacklightText) ? $ActualProductionNoStacklightText : "0",
				"ActualProductionText" => !empty($ActualProductionText) ? $ActualProductionText : "0",
				"ActualProductionTextNew" => !empty($ActualProductionText) ? $this->convertValueOnePoint($ActualProductionText) : "0",
				"SetupText" => !empty($SetupText) ? $SetupText : "0",
				"NoProductionText" => !empty($NoProductionText) ? $NoProductionText : "0",
				"formatter" => $formatter,
				"machinePartsData" => $machinePartsData,
				"scrapParts" => $scrapParts,
				"goodParts" => $goodParts
			);
		echo json_encode($json);
	}

	//parts_analytics_pagination use for get analytics graph data
	public function parts_analytics_pagination()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		//Check show all data or specific machine data
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
				$machineNoStacklight = $this->AM->getWhereSingle(array("isDeleted" => '0','machineId' => $machineId), 'machine');
				$isDataGet = $machineNoStacklight->isDataGet;
				$noStacklight = $machineNoStacklight->noStacklight;
				$machineCount = 1;
			}else
			{
				$noStacklight = "0";
			}
		}

		$viewId = $this->input->post('viewId');
		if ($viewId == "none") 
		{
			$viewId = 0;
			$viewStartTime = "";
			$viewEndTime = "";
			$breakViewHour = array();
		}else
		{
			$view = $this->AM->getWhereSingle(array("viewId" => $viewId),"machineView");
			$startTime = date('H',strtotime($view->startTime));
			if ($view->endTime == "24:00:00") 
			{
				$endTime = 24;
			}else
			{
				$endTime = date('H',strtotime($view->endTime));
			}
			$viewStartTime = intVal(($startTime == "00") ? "0" : ltrim($startTime, '0'));
			$viewEndTime = intVal(ltrim($endTime, '0'));
			$machineBreakViewData = $this->AM->getWhere(array("viewId" => $viewId),"machineBreakView");

			foreach ($machineBreakViewData as $key => $value) 
			{
				$startBreakTime = date('H',strtotime($value['startTime']));
				$endBreakTime = date('H',strtotime($value['endTime']));

				for ($p=$startBreakTime; $p < $endBreakTime; $p++) 
				{
					$breakViewHour[] = intVal(($p == "00") ? "0" : ltrim($p, '0'));
				}
			}
		}

		
		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');


		$machinePartsData = $this->AM->getMachinePartsData($machineId,$choose1,$choose2,$viewStartTime,$viewEndTime,$breakViewHour,$isDataGet);
		$machinePartsFinishData = $this->AM->getMachinePartsFinishData($machineId,$choose1,$choose2);
		$dayPartsProduce = $machinePartsData->dayPartsProduce;
		$scrapParts = !empty($machinePartsFinishData->scrapParts) ? $machinePartsFinishData->scrapParts : 0;
		
		if (!empty($machinePartsData) && $machinePartsData->dayThrouputMaxCount > 0) 
		{

			$goodParts = floor($dayPartsProduce);
			if (!empty($machinePartsData->dayProductivity) && $isDataGet == '0') 
			{
				$machinePartsData->dayProductivityText = ($machinePartsData->dayRunningTime * 100) / $machinePartsData->dayProductionTime;
			}else
			{
				$machinePartsData->dayProductivityText = 0;
			}

			$machinePartsData->dayRunningTimeText = $this->secondsToHoursTwoPoint($machinePartsData->dayRunningTime);
			$machinePartsData->dayProductionTimeText = $this->secondsToHoursTwoPoint($machinePartsData->dayProductionTime);
		}else
		{
			$goodParts = "0";
			$machinePartsData =  new stdClass();

			$machinePartsData->dayProductionTimeText = "0";
			$machinePartsData->dayThrouputMaxText = "0";
			$machinePartsData->dayRunningTimeText = "0";
			$machinePartsData->dayThrouputMax = "0";
			$machinePartsData->dayProductionTime = "0";
			$machinePartsData->dayCycleMax = "0";
			$machinePartsData->dayThrouputActual = "0";
			$machinePartsData->dayProductivity = "0";
			$machinePartsData->dayRunningTime = "0";
			$machinePartsData->dayCycleActual = "0";
			$machinePartsData->dayProductivityText = 0;
			$machinePartsData->dayThrouputActualText = 0;
			$machinePartsData->dayThrouputMaxCount = "1";
		}


		$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionRunning = $resultActualProductionRunning->countVal;
		//Get machine production state waiting data as par selecte filter
		$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
		//Get machine production state stopped data as par selecte filter
		$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionStopped = $resultActualProductionStopped->countVal;
		//Get machine production state off data as par selecte filter
		$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionOff = $resultActualProductionOff->countVal;
		//Get machine production state nodet data as par selecte filter
		$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNodet = $resultActualProductionNodet->countVal;
		//Get machine production state noStacklight data as par selecte filter
		$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;


		$ActualProductionRunningText = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
		$ActualProductionWaitingText = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
		$ActualProductionStoppedText = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
		$ActualProductionOffText = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
		$ActualProductionNodetText = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
		$ActualProductionNoStacklightText = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;

		$ActualProductionText = $ActualProductionRunningText + $ActualProductionWaitingText + $ActualProductionStoppedText + $ActualProductionOffText + $ActualProductionNodetText + $ActualProductionNoStacklightText;

		if ($isDataGet == "1")
		{
			$machinePartsData->dayProductivityText = $machinePartsData->dayProductivityIo;
			$machinePartsData->dayProductionTimeText = $this->convertValueOnePoint($ActualProductionText);
		}else
		{
			if (!empty($ActualProductionNoStacklightText)) 
			{
				if ($choose1 == "day") 
				{
					$maxHour = "24";
				}elseif ($choose1 == "weekly") 
				{
					$maxHour = "168";
				}elseif ($choose1 == "monthly") 
				{
					$choose3 = explode(" ", $choose2);
				    $monthCount = $choose3[0];
				    $year = $choose3[1];
					$monthDayCount = cal_days_in_month(CAL_GREGORIAN,date('m',strtotime($monthCount)),$year);
					$maxHour = 24 * $monthDayCount;
				}elseif ($choose1 == "yearly") 
				{
					$maxHour = "8760";
				}

				$machinePartsData->dayProductivityText = $this->convertValueOnePoint(($ActualProductionNoStacklightText * 100) / $maxHour);
			}
			else if (!empty($ActualProductionText) && !empty($ActualProductionRunningText)) 
			{
				$machinePartsData->dayProductivityText = $this->convertValueOnePoint(($ActualProductionRunningText * 100) / $ActualProductionText);
			}else
			{
				$machinePartsData->dayProductivityText = "0";
			}
		}
		if ($choose1 == "day") 
		{
			$startDate = date('Y-m-d',strtotime("-30 days ".$choose2));
			$where = array("date(machinePartsFinish.insertTime) >= " => date('Y-m-d',strtotime($choose2)),"date(machinePartsFinish.partStartTime) <=" => date('Y-m-d',strtotime($choose2)),"machinePartsFinish.machineId" => $machineId);
			$whereOr = array();

			$firstStartDate = date('Y-m-d 00:00:00',strtotime($choose2));
			$startDateOrder = date('Y-m-d',strtotime($choose2));
			$whereOrder = array("machineId" => $machineId,"date(updateTime) <=" => $startDateOrder,"currentOrderStatus" => "1");
		}elseif ($choose1 == "weekly") 
		{
			$choose2 = explode("/", $choose2);
            $week = $choose2[0];
            $year = $choose2[1];
			$weekDate = $this->getStartAndEndDate($week,$year);
			$startDate = date('Y-m-d',strtotime("-30 days ".$weekDate['week_start']));
			$where = array("YEAR(machinePartsFinish.insertTime)" => $year,"machinePartsFinish.machineId" => $machineId);

			$whereOr = array("WEEK(machinePartsFinish.insertTime)" => $week,"WEEK(machinePartsFinish.partStartTime)" => $week);

			$firstStartDate = date('Y-m-d 00:00:00',strtotime($weekDate['week_start']));
			$whereOrder = array("machineId" => $machineId,"WEEK(updateTime) <=" => $week,"YEAR(updateTime) <=" => $year,"currentOrderStatus" => "1");
		}elseif ($choose1 == "monthly") 
		{
			$choose2 = explode(" ", $choose2);
            $month = $choose2[0];
            $monthOrder = date('m',strtotime($month));
            $month = date('m',strtotime($month));
            $year = $choose2[1];
            if ($month == date('m')) 
            {
            	$month = date('m') - 1;
            	$startDate = date($year.'-'.$month.'-d');
            	$startDate = date('Y-m-d',strtotime("+1 days ".$startDate));
            }else
            {
            	$startDate = date('Y-m-t',strtotime($year.'-'.$month.'-01'));
            	$startDate = date('Y-m-d',strtotime("-30 days ".$startDate));
            }
			$where = array("YEAR(machinePartsFinish.insertTime)" => $choose2[1],"machinePartsFinish.machineId" => $machineId);

			$whereOr = array("MONTH(machinePartsFinish.insertTime)" => date('m',strtotime($choose2[0])),"MONTH(machinePartsFinish.partStartTime)" => date('m',strtotime($choose2[0])));
			

			$firstStartDate = date('Y-m-d 00:00:00',strtotime($year.'-'.$month.'-01'));
			$whereOrder = array("machineId" => $machineId,"MONTH(updateTime) <=" => date('m',strtotime($choose2[0])),"YEAR(updateTime) <=" => $year,"currentOrderStatus" => "1");
		}elseif ($choose1 == "yearly") 
		{
			if ($choose2 == date('Y')) 
			{
				$startDate = date($choose2.'-m-d');
			}else
			{
				$startDate = date($choose2.'-12-31');
			}

			$startDate = date('Y-m-d',strtotime("-30 days ".$startDate));
			$where = array("machinePartsFinish.machineId" => $machineId);

			$whereOr = array("YEAR(machinePartsFinish.insertTime)" => $choose2,"YEAR(machinePartsFinish.partStartTime)" => $choose2);

			$firstStartDate = date($choose2.'-'.'01-01 00:00:00');
			$whereOrder = array("machineId" => $machineId,"YEAR(updateTime) <=" => $choose2,"currentOrderStatus" => "1");
		}

		$dateDataArr = array();
		$dayOpprtunityGap = array();
		$dayPartsProduce = array();
		$monthNameNext = "";
		for ($i=1; $i < 31; $i++) 
		{ 

			$monthName = date('F',strtotime("+".$i." day ".$startDate));
			if ($i == 1 || $monthName != $monthNameNext) 
			{
				
				$monthNameNext = date('F',strtotime("+".$i." day ".$startDate));

				if ($monthNameNext == "January") 
				{
				    $monthName = January;
				}else if ($monthNameNext == "February") 
				{
				    $monthName = February;
				}else if ($monthNameNext == "March") 
				{
				    $monthName = March;
				}else if ($monthNameNext == "April") 
				{
				    $monthName = April;
				}else if ($monthNameNext == "May") 
				{
				    $monthName = May;
				}else if ($monthNameNext == "June") 
				{
				    $monthName = June;
				}else if ($monthNameNext == "July") 
				{
				    $monthName = July;
				}else if ($monthNameNext == "August") 
				{
				    $monthName = august;
				}else if ($monthNameNext == "September") 
				{
				    $monthName = September;
				}else if ($monthNameNext == "October") 
				{
				    $monthName = October;
				}else if ($monthNameNext == "November") 
				{
				    $monthName = November;
				}else if ($monthNameNext == "December") 
				{
				    $monthName = December;
				}

				$dateFormat = ' d';
			}else
			{
				$monthName = "";
				$dateFormat = 'd';
				$monthNameNext = date('F',strtotime("+".$i." day ".$startDate));
			}
			$dateData = date('Y-m-d',strtotime("+".$i." day ".$startDate));
			$dateDataArr[] = $monthName.date($dateFormat,strtotime("+".$i." day ".$startDate)); 
			$opportunityAndParts = $this->AM->getOpportunityAndParts($machineId,$dateData,$viewStartTime,$viewEndTime,$breakViewHour,$isDataGet);
			if (!empty($opportunityAndParts)) 
			{
				$dayOpprtunityGapArr[] = !empty($opportunityAndParts->dayOpprtunityGap) ? "-".floor($opportunityAndParts->dayOpprtunityGap) : "-0";
				$dayPartsProduceArr[] = !empty($opportunityAndParts->dayPartsProduce) ? floor($opportunityAndParts->dayPartsProduce) : "0";	
			}else
			{
				$dayOpprtunityGapArr[] = "-0";
				$dayPartsProduceArr[] = "0";
			}
		}

		$totalGoodPart[] = 0;
		$totalScrapParts[] = 0;


		$machinePartsCur = $this->AM->getWhereWithOrderBy($whereOrder,'updateTime','asc',"machineParts");

		$ordersHtml = "";
		

		$select = "machineParts.*,machinePartsFinish.*";
		$join = array("machineParts" => "machineParts.machinePartsId = machinePartsFinish.machinePartsId");
		$machinePartsFinish = $this->AM->getWhereJoinWithOr($select,$where,$whereOr,$join,"machinePartsFinish");

		$count = count($machinePartsFinish) - 1;
		foreach ($machinePartsFinish as $key => $value) 
		{
			$finishOrderTime = $value['insertTime'];
			if ($count > 0) 
			{	
				$nextFinishOrderTime = $machinePartsFinish[$key - 1]['insertTime'];
			}

			$dispayDate = date('Y-m-d H:i:s',strtotime($value['partStartTime']));

			if (!empty($nextFinishOrderTime)) 
			{
				$startTimeRe = $this->AM->getLastProductionTime($value['machinePartsId'],$nextFinishOrderTime,$finishOrderTime);

				$startTimeDD = $startTimeRe->insertTime;

				if (!empty($startTimeRe)) {
					
					$startTimeRu = date('Y-m-d H:i:s',strtotime($startTimeDD));
				}else
				{
					$startTimeRu = date('Y-m-d H:i:s');
				}

			}else
			{
				$startTimeRe = $this->AM->getLastProductionTimeFinalOrder($value['machinePartsId'],$finishOrderTime);

				$startTimeDD = $startTimeRe->insertTime;
				if (!empty($startTimeRe)) {
					$startTimeRu = date('Y-m-d H:i:s',strtotime($startTimeDD));
				}else
				{
					$startTimeRu = date('Y-m-d H:i:s');
				}
			}

			/*if (empty($value['correctParts'])) 
			{
				$runningTime = $this->AM->getMachineRunnignWaitingTimeWithEndTime($value['machineId'],$startTimeRu,$value['insertTime']);

				$totalParts = floor($runningTime->timeDiff / $value['patrsCycleTime']);
			}else
			{*/
				$totalParts = $value['correctParts'];
			/*}*/
			

			$totalGoodPart[] = $totalParts;
			$totalScrapParts[] = $value['scrapParts'];

			if (!empty($value['userId'])) 
			{
				$user = $this->AM->getWhereDBSingle(array("userId" => $value['userId']),"user");
				$userImage = $user->userImage;
				$userName = $user->userName;
			}else
			{
				$userImage = "34.jpg";
				$userName = "---";
			}
			$userImageParame = "'".$userImage."'";
			$userNameParame = "'".$userName."'";

			$setupTime = $this->AM->getMachineSetupTime($value['machineId'],$startTimeRu,$value['insertTime']);

			if (!empty($setupTime)) 
			{
				$hours = floor($setupTime->timeDiff / 3600);
				$minutes = floor(($setupTime->timeDiff / 60) % 60);
				$seconds = $setupTime->timeDiff % 60;

				$hours = str_pad($hours,2,"0",STR_PAD_LEFT);
				$minutes = str_pad($minutes,2,"0",STR_PAD_LEFT);
				$seconds = str_pad($seconds,2,"0",STR_PAD_LEFT);

				$setupTimeTook = "$hours:$minutes:$seconds";
			}else
			{
				$setupTimeTook = "00:00:00";
			}

			$partsNumberParame = "'".$value['partsNumber']."'";
			$partsNameParame = "'".$value['partsName']."'";
			$partsOperationParame = "'".$value['partsOperation']."'";
			$startDateParame = "'".$dispayDate."'";
			$endDateParame = "'".date('Y-m-d H:i:s',strtotime($value['insertTime']))."'";
			$correctPartsParame = "'".$totalParts."'";
			$scrapPartsParame = "'".$value['scrapParts']."'";
			$timeTookParame = "'".$setupTimeTook."'";
			$ManufacturingOrderIdParame = "'".$value['ManufacturingOrderId']."'";
			$createDateParame = "'".date('Y-m-d',strtotime($value['insertTime']))."'";

			/*$this->AM->updateData(array("machinePartsFinishLogId" => $value['machinePartsFinishLogId']),array("partStartTime" => $dispayDate), "machinePartsFinish");*/

			$orderReportType = ($value['orderReportType'] == "finish") ? Finish : Report;
			if ($this->session->userdata('isMonitor') == "0") 
			{
				$ordersHtml .= '<tr onclick="orderDetails('.$value['machinePartsFinishLogId'].','.$partsNumberParame.','.$partsNameParame.','.$partsOperationParame.','.$startDateParame.','.$endDateParame.','.$correctPartsParame.','.$scrapPartsParame.','.$timeTookParame.','.$createDateParame.','.$userImageParame.','.$userNameParame.','.$ManufacturingOrderIdParame.')"> 
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsNumber'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:700;color:#124D8D;">'.$value['partsName'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsOperation'].'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'. $dispayDate .'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['insertTime'])).'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;text-align:center;">
                        	<span style="font-weight:500;color: #76BA1B;font-weight: 600;">'.$totalParts.'</span> </td>

                        <td style="padding: 5px 0px 0px 5px;font-weight:500;text-align:center;"><span style="font-weight:500;color: #76BA1B;font-weight: 600;color:red">'.$value['scrapParts'].'</span></td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'. $orderReportType .'</td>
                    </tr> ';
			}else
			{
					$ordersHtml .= '<tr onclick="orderDetails('.$value['machinePartsFinishLogId'].','.$partsNumberParame.','.$partsNameParame.','.$partsOperationParame.','.$startDateParame.','.$endDateParame.','.$correctPartsParame.','.$scrapPartsParame.','.$timeTookParame.','.$createDateParame.','.$userImageParame.','.$userNameParame.','.$ManufacturingOrderIdParame.')"> 
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsNumber'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['ManufacturingOrderId'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:700;color:#124D8D;">'.$value['partsName'].'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'.$value['partsOperation'].'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'. $dispayDate .'</td>
                        <td style="padding: 5px 0px 0px 0px;font-weight:500;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['insertTime'])).'</td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;text-align:center;">
                        	<span style="font-weight:500;color: #76BA1B;font-weight: 600;">'.$totalParts.'</span> </td>

                        <td style="padding: 5px 0px 0px 5px;font-weight:500;text-align:center;"><span style="font-weight:500;color: #76BA1B;font-weight: 600;color:red">'.$value['scrapParts'].'</span></td>
                        <td style="padding: 5px 0px 0px 5px;font-weight:500;color:black;">'. $orderReportType .'</td>
                    </tr> ';
			}
		
		}

		$machinePartsCurCheckValue = true;
		foreach ($machinePartsCur as $key => $value) 
		{
			if ($key == "0") 
			{
				/* $productionTime = $this->AM->getLastFinishOrderTime($value['machineId']);

				if (!empty($productionTime)) 
				{
					$insertTime = $productionTime->insertTime;
					$machineProductionTimeWithFinishOrder = $this->AM->getLastProductionTimeWithFinishOrder($value['machinePartsId'],$insertTime);
					if (!empty($machineProductionTimeWithFinishOrder)) 
					{
						$insertTime = $machineProductionTimeWithFinishOrder->insertTime;
					}
				}else
				{
					$productionTime = $this->AM->getLastProductionTimeEntry($value['machinePartsId']);
					$insertTime = $productionTime->insertTime;
				}


				if($firstStartDate > $insertTime)
				{
					$insertTime = $firstStartDate;
				} */
				
				$insertTime = $value['updateTime'];
				$runningTime = $this->AM->getMachineRunnignWaitingTime($value['machineId'],$insertTime);

				if (!empty($runningTime->timeDiff) && !empty($value['patrsCycleTime'])) 
				{
					$totalParts = floor($runningTime->timeDiff / $value['patrsCycleTime']);
				}else
				{
					$totalParts = 0;
				}

				//$totalParts = floor($runningTime->timeDiff / $value['patrsCycleTime']);

				$totalGoodPart[] = 0;
				$totalScrapParts[] = 0;
				if (!empty($value['userId'])) 
				{
					$user = $this->AM->getWhereDBSingle(array("userId" => $value['userId']),"user");
					$userImage = $user->userImage;
					$userName = $user->userName;
				}else
				{
					$userImage = "34.jpg";
					$userName = "---";
				}
				$userImageParame = "'".$userImage."'";
				$userNameParame = "'".$userName."'";

				$setupTime = $this->AM->getMachineSetupTime($value['machineId'],$insertTime,"");

				if (!empty($setupTime)) 
				{
					$hours = floor($setupTime->timeDiff / 3600);
					$minutes = floor(($setupTime->timeDiff / 60) % 60);
					$seconds = $setupTime->timeDiff % 60;

					$hours = str_pad($hours,2,"0",STR_PAD_LEFT);
					$minutes = str_pad($minutes,2,"0",STR_PAD_LEFT);
					$seconds = str_pad($seconds,2,"0",STR_PAD_LEFT);

					$setupTimeTook = "$hours:$minutes:$seconds";
				}else
				{
					$setupTimeTook = "00:00:00";
				}

				$partsNumberParame = "'".$value['partsNumber']."'";
				$partsNameParame = "'".$value['partsName']."'";
				$ManufacturingOrderIdParame = "'".$value['ManufacturingOrderId']."'";
				$partsOperationParame = "'".$value['partsOperation']."'";
				$startDateParame = "'".date('Y-m-d H:i:s',strtotime($value['updateTime']))."'";
				$endDateParame = "'---'";
				$correctPartsParame = "'".$totalParts."'";
				$scrapPartsParame = "'0'";
				$timeTookParame = "'".$setupTimeTook."'";
				
				$createDateParame = "'".date('Y-m-d',strtotime($value['updateTime']))."'";

				if ($this->session->userdata('isMonitor') == "0") 
				{
					$ordersHtml .= '<tr onclick="orderDetails('.$value['machinePartsId'].','.$partsNumberParame.','.$partsNameParame.','.$partsOperationParame.','.$startDateParame.','.$endDateParame.','.$correctPartsParame.','.$scrapPartsParame.','.$timeTookParame.','.$createDateParame.','.$userImageParame.','.$userNameParame.','.$ManufacturingOrderIdParame.')">
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsNumber'].'</td>
		                    <td style="font-weight:700;padding: 5px 0px 0px 5px;color:#124D8D;">'.$value['partsName'].'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsOperation'].'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 0px;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['reportedPartTime'])).'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;text-align: center;">---</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;text-align:center;">
		                    	<span style="font-weight:500;padding:0px;color: #76BA1B;font-weight: 600;text-align:center;">&nbsp;</span> </td>

		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;text-align:center;">
		                    	<span style="font-weight:500;padding: 5px 0px 0px 5px;color: #76BA1B;font-weight: 600;color:red;min-width:77px;">&nbsp;</span></td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'. Working .'</td>
		                </tr> ';
		        }else
		        {
		        	$ordersHtml .= '<tr onclick="orderDetails('.$value['machinePartsId'].','.$partsNumberParame.','.$partsNameParame.','.$partsOperationParame.','.$startDateParame.','.$endDateParame.','.$correctPartsParame.','.$scrapPartsParame.','.$timeTookParame.','.$createDateParame.','.$userImageParame.','.$userNameParame.','.$ManufacturingOrderIdParame.')">
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsNumber'].'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['ManufacturingOrderId'].'</td>
		                    <td style="font-weight:700;padding: 5px 0px 0px 5px;color:#124D8D;">'.$value['partsName'].'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'.$value['partsOperation'].'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 0px;color:black;text-align: center;">'.date('Y-m-d H:i:s',strtotime($value['reportedPartTime'])).'</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;text-align: center;">---</td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;text-align:center;">
		                    	<span style="font-weight:500;padding:0px;color: #76BA1B;font-weight: 600;text-align:center;">&nbsp;</span> </td>

		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;text-align:center;">
		                    	<span style="font-weight:500;padding: 5px 0px 0px 5px;color: #76BA1B;font-weight: 600;color:red;min-width:77px;">&nbsp;</span></td>
		                    <td style="font-weight:500;padding: 5px 0px 0px 5px;color:black;">'. Working .'</td>
		                </tr> ';
		        }

	            $machinePartsCurCheckValue = false;
            }
		}

		if (empty($machinePartsFinish) &&  $machinePartsCurCheckValue == true) 
		{
			if ($this->session->userdata('isMonitor') == "0") 
			{
				$ordersHtml .= '<tr>
	                            <td colspan="8" style="padding: 10px 0px!important;text-align: center;color:black;">'.Nomatchingrecordsfound.'</td>
	                        </tr> ';
	        }else
	        {
	        	$ordersHtml .= '<tr>
	                            <td colspan="9" style="padding: 10px 0px!important;text-align: center;color:black;">'.Nomatchingrecordsfound.'</td>
	                        </tr> ';
	        }
		}

		/*if ($this->factoryId == 2 && $machineId == 6) 
		{
			$totalGoodPart = array();
			$totalScrapParts = array();
			$partCount = $this->AM->getPartCountDataInAnalytics('GPIO15',$this->input->post('choose1'),$this->input->post('choose2'),$viewStartTime,$viewEndTime);

			$totalGoodPart[] = $partCount;
			$totalScrapParts[] = 0;
		}*/

		$json = array(
			    "dateDataArr" => $dateDataArr,
			    "partCount" => $goodParts,
			    "isDataGet" => $isDataGet,
			    "dayPartsProduceArr" => $dayPartsProduceArr,
			    "dayOpprtunityGapArr" => $dayOpprtunityGapArr,
				"noStacklight" => $noStacklight,
				"machineId" => $machineId,
				"machinePartsData" => $machinePartsData,
				"goodParts" => array_sum($totalGoodPart),
				"scrapParts" => array_sum($totalScrapParts),
				"ordersHtml" => $ordersHtml
			);

		echo json_encode($json);
	}



	function getStartAndEndDate($week, $year) {
	  $dto = new DateTime();
	  $dto->setISODate($year, $week);
	  $ret['week_start'] = $dto->format('Y-m-d');
	  $dto->modify('+6 days');
	  $ret['week_end'] = $dto->format('Y-m-d');
	  return $ret;
	}
	//In use
	function add_view()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
			$form = $this->input->post('formData');
        	$startTime = $this->input->post('startTime');
        	$stopTime = $this->input->post('stopTime');

        	$viewName = $form['viewName'];
        	$breakCount = count($form['breakStartTimes']);
        	$breakStartTimes = $form['breakStartTimes'];
        	$breakEndTimes = $form['breakEndTimes'];

        	$data = array(
        		"viewName" => $viewName,
        		"startTime" => $startTime,
        		"endTime" => $stopTime,
        	);

        	$viewId = $this->AM->insertData($data,"machineView");

        	for($i = 0; $i < $breakCount; $i++)
        	{

	        	$breakData = array(
	        		"viewId" => $viewId,
	        		"startTime" => $breakStartTimes[$i],
	        		"endTime" => $breakEndTimes[$i],
	        	);

	        	$this->AM->insertData($breakData,"machineBreakView");
        	}

        	$response = array("status" => 1,"message" => Viewaddedsuccessfully);
    
        echo json_encode($response);
	}

	function add_save_transform()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
			$form = $this->input->post('formData');
			$transformTo = $form['transformTo'];
			$transformName = $form['transformName'];
        	$machineId = $this->input->post('machineId');
        	$betweenStartTime = $this->input->post('betweenStartTime');
        	$betweenEndTime = $this->input->post('betweenEndTime');
        	$choose1 = $this->input->post('choose1');
        	$choose2 = $this->input->post('choose2');

        	if ($choose1 == "day") 
			{
				$startDate = date('Y-m-d 00:00:00',strtotime($choose2));
				$endDate = date('Y-m-d 23:59:59',strtotime($choose2));

				$messageFilter = strtolower(on_the)." ".strtolower(Day)." ".$choose2;
			}elseif ($choose1 == "weekly") 
			{
				$choose2 = explode("/", $choose2);
	            $week = $choose2[0];
	            $year = $choose2[1];
				$weekDate = $this->getStartAndEndDate($week,$year);
				$startDate = date('Y-m-d 00:00:00',strtotime($weekDate['week_start']));
				$endDate = date('Y-m-d 23:59:59',strtotime($weekDate['week_end']));

				$messageFilter = strtolower(on_the)." ".strtolower(week)." ".$week;
			}elseif ($choose1 == "monthly") 
			{
				$choose2 = explode(" ", $choose2);
	            $month = $choose2[0];
	            $month = date('m',strtotime($month));
	            $year = $choose2[1];

	            $startDate = date($year.'-'.$month.'-01 00:00:00');
				$endDate = date($year.'-'.$month.'-t 23:59:59');
				$messageFilter = strtolower(on_the)." ".strtolower(MONTH)." ".$month." ".strtolower(year)." ".$year;
			}elseif ($choose1 == "yearly") 
			{
				$startDate = date($choose2.'-01-01 00:00:00');
				$endDate = date($choose2.'-12-31 23:59:59');

				$messageFilter = strtolower(on_the)." ".strtolower(year)." ".$choose2;
			}

			$result = $this->AM->checkStateTransform($betweenStartTime,$betweenEndTime,$startDate,$endDate,$machineId,$choose1);

			if (empty($result)) 
			{
	        	$data = array(
	        		"machineId" => $machineId,
	        		"transformFrom" => "0",
	        		"transformName" => $transformName,
	        		"transformTo" => $transformTo,
	        		"betweenStartTime" => $betweenStartTime,
	        		"betweenEndTime" => $betweenEndTime,
	        		"timeRangeStartTime" => $startDate,
	        		"timeRangeEndTime" => $endDate,
	        		"choose1" => $this->input->post('choose1'),
	        		"choose2" => $this->input->post('choose2'),
	        		"timeRangeEndTime" => $endDate,
	        		"addedDate" => date('Y-m-d H:i:s')
	        	);

	        	if ($transformTo == "1") 
	        	{
	        		$stateName = strtolower(setup);
	        	}else
	        	{
	        		$stateName = strtolower(Noproduction);
	        	}

	        	$transformId = $this->AM->insertData($data,"customMachineState");

	        	$htmlData = ' <div class="row" id="transformId'. $transformId .'">
	                        <div class="col-md-11">
	                            <h5 class="h4machineHeadcolor">'. $transformName .' ('. strtolower(hour).' '.$betweenStartTime.' - '.$betweenEndTime.')  ('. $stateName .' '. $messageFilter .')</h5>
	                        </div>
	                        <div class="col-md-1">
	                            <img onclick="removeTransform('. $transformId .')" style="width: 20px;cursor: pointer;" src="'. base_url("assets/img/cross.svg") .'">
	                        </div>
	                    </div>';

	        	$response = array("status" => 1,"message" => Statetransformaddedsuccessfully,"transformData" => $htmlData);
			}else
			{
				$transformName = implode(", ",array_column($result,"transformName"));
				$response = array("status" => 0,"message" => Pleaseremoveconflicringtransformer." (". $transformName .") ". firsttoaddthis .".");
			}
    
        	echo json_encode($response);
	}

	function getStateTransformer()
	{
		$machineId = $this->input->post('machineId');

		$choose1Filter = $this->input->post('choose1');
    	$choose2Filter = $this->input->post('choose2');

    	if ($choose1Filter == "day") 
		{
			$timeRangeStartTime = date('Y-m-d 00:00:00',strtotime($choose2Filter));
			$timeRangeEndTime = date('Y-m-d 23:59:59',strtotime($choose2Filter));
		}elseif ($choose1Filter == "weekly") 
		{
			$choose2Filter = explode("/", $choose2Filter);
            $week = $choose2Filter[0];
            $year = $choose2Filter[1];
			$weekDate = $this->getStartAndEndDate($week,$year);
			$timeRangeStartTime = date('Y-m-d 00:00:00',strtotime($weekDate['week_start']));
			$timeRangeEndTime = date('Y-m-d 23:59:59',strtotime($weekDate['week_end']));
		}elseif ($choose1Filter == "monthly") 
		{
			$choose2Filter = explode(" ", $choose2Filter);
            $month = $choose2Filter[0];
            $month = date('m',strtotime($month));
            $year = $choose2Filter[1];

            $timeRangeStartTime = date($year.'-'.$month.'-01 00:00:00');
			$timeRangeEndTime = date($year.'-'.$month.'-t 23:59:59');
		}elseif ($choose1Filter == "yearly") 
		{
			$timeRangeStartTime = date($choose2Filter.'-01-01 00:00:00');
			$timeRangeEndTime = date($choose2Filter.'-12-31 23:59:59');
		}

		$result = $this->AM->getByFilterStateTransform($timeRangeStartTime,$timeRangeEndTime,$machineId,$choose1Filter);
		
		$htmlData = "";
		foreach ($result as $key => $value) 
		{

			$choose1 = $value['choose1'];
        	$choose2 = $value['choose2'];

        	if ($choose1 == "day") 
			{
				$messageFilter = strtolower(on_the)." ".strtolower(Day)." ".$choose2;
			}elseif ($choose1 == "weekly") 
			{
				$choose2 = explode("/", $choose2);
	            $week = $choose2[0];
	            $year = $choose2[1];
				$messageFilter = strtolower(on_the)." ".strtolower(week)." ".$week;
			}elseif ($choose1 == "monthly") 
			{
				$choose2 = explode(" ", $choose2);
	            $month = $choose2[0];
	            $month = date('m',strtotime($month));
	            $year = $choose2[1];
				$messageFilter = strtolower(on_the)." ".strtolower(MONTH)." ".$month." ".strtolower(year)." ".$year;
			}elseif ($choose1 == "yearly") 
			{
				$messageFilter = strtolower(on_the)." ".strtolower(year)." ".$choose2;
			}

			if ($value['transformTo'] == "1") 
        	{
        		$stateName = strtolower(setup);
        	}else
        	{
        		$stateName = strtolower(Noproduction);
        	}

			$htmlData .= ' <div class="row" id="transformId'. $value['customMachineStateTd'] .'">
                        <div class="col-md-11">
                            <h5 class="h4machineHeadcolor">'. $value['transformName'] .' ('. strtolower(hour).' '.$value['betweenStartTime'].' - '.$value['betweenEndTime'].') ('. $stateName .' '. $messageFilter .')</h5>
                        </div>
                        <div class="col-md-1">
                            <img onclick="removeTransform('. $value['customMachineStateTd'] .')" style="width: 20px;cursor: pointer;" src="'. base_url("assets/img/cross.svg") .'">
                        </div>
                    </div>';
		}

		$response = array("status" => !empty($result) ? 1 : 0,"transformData" => $htmlData);
    
        echo json_encode($response);
	}

	function deleteStateTransformer()
	{
		$transformId = $this->input->post('transformId');
		$machineId = $this->input->post('machineId');

		$this->AM->updateData(array("customMachineStateTd" => $transformId),array("isDelete" => "1"),"customMachineState");


		$choose1Filter = $this->input->post('choose1');
    	$choose2Filter = $this->input->post('choose2');

    	if ($choose1Filter == "day") 
		{
			$timeRangeStartTime = date('Y-m-d 00:00:00',strtotime($choose2Filter));
			$timeRangeEndTime = date('Y-m-d 23:59:59',strtotime($choose2Filter));
		}elseif ($choose1Filter == "weekly") 
		{
			$choose2Filter = explode("/", $choose2Filter);
            $week = $choose2Filter[0];
            $year = $choose2Filter[1];
			$weekDate = $this->getStartAndEndDate($week,$year);
			$timeRangeStartTime = date('Y-m-d 00:00:00',strtotime($weekDate['week_start']));
			$timeRangeEndTime = date('Y-m-d 23:59:59',strtotime($weekDate['week_end']));
		}elseif ($choose1Filter == "monthly") 
		{
			$choose2Filter = explode(" ", $choose2Filter);
            $month = $choose2Filter[0];
            $month = date('m',strtotime($month));
            $year = $choose2Filter[1];

            $timeRangeStartTime = date($year.'-'.$month.'-01 00:00:00');
			$timeRangeEndTime = date($year.'-'.$month.'-t 23:59:59');
		}elseif ($choose1Filter == "yearly") 
		{
			$timeRangeStartTime = date($choose2Filter.'-01-01 00:00:00');
			$timeRangeEndTime = date($choose2Filter.'-12-31 23:59:59');
		}

		$result = $this->AM->getByFilterStateTransform($timeRangeStartTime,$timeRangeEndTime,$machineId,$choose1Filter);

		

		$response = array("status" => 1,"dataCount" => count($result));
    
        echo json_encode($response);
	}

	//In use
	function edit_view()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('viewId','viewId','required'); 
		$this->form_validation->set_rules('viewName','viewName','required'); 
        $this->form_validation->set_rules('startTime','startTime','required');
        $this->form_validation->set_rules('stopTime','stopTime','required');

    	if($this->form_validation->run()==false)
        { 
        	$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
            $response = array("status" => 0,"message" => $error);
        } 
        else 
        { 
        	$viewId = $this->input->post('viewId');
        	$startTime = $this->input->post('startTime');
        	$stopTime = $this->input->post('stopTime');
        	$viewName = $this->input->post('viewName');

        	$data = array(
        		"viewName" => $viewName,
        		"startTime" => $startTime,
        		"endTime" => $stopTime,
        	);

        	$this->AM->updateData(array("viewId" => $viewId),$data,"machineView");

        	

        	$response = array("status" => 1,"message" => Viewupdatedsuccessfully);
        }
        echo json_encode($response);
	}

	//In use
	function delete_view()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		
		$this->form_validation->set_rules('deleteViewId','viewId','required'); 

    	//checking required parameters validation
    	if($this->form_validation->run()==false)
        { 
        	$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
            $response = array("status" => 0,"message" => $error);
        } 
        else 
        { 
        	$viewId = $this->input->post('deleteViewId');


        	$data = array("isDelete" => "1");
        	$this->AM->updateData(array("viewId" => $viewId),$data, "machineView");

        	$response = array("status" => 1,"message" => Viewdeletedsuccessfully);
        }
        echo json_encode($response);
	}

	//In use
	function getViewDataById()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$this->form_validation->set_rules('viewId','viewId','required'); 

    	//checking required parameters validation
    	if($this->form_validation->run()==false)
        { 
        	$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
            $response = array("status" => 0,"message" => $error);
        } 
        else 
        { 
        	$viewId = $this->input->post('viewId');


        	$result = $this->AM->getWhereSingle(array("viewId" => $viewId), "machineView");
        	$breakresult = $this->AM->getWhere(array("viewId" => $viewId), "machineBreakView");

        	$result->startTimeHour = date('H',strtotime($result->startTime));
        	$result->startTimeMinute = date('i',strtotime($result->startTime));

        	if ($result->endTime == "24:00:00") 
        	{
        		$result->stopTimeHour = "24";
				$result->stopTimeMinute = "00";
        	}else
        	{
	        	$result->stopTimeHour = date('H',strtotime($result->endTime));
	        	$result->stopTimeMinute = date('i',strtotime($result->endTime));
        	}

        	$html = "";
        	foreach ($breakresult as $key => $value) 
        	{
        		$html .= '<div class="row" id="breakRowEdit'.$value['breakViewId'].'"><div class="col-md-10"><span style="font-size: 16px;">'.Breakstartsfrom.' </span><span style="color: #FF8000;font-size: 16px;">'. date('H:i',strtotime($value['startTime'])).'</span><span style="font-size: 16px;">&nbsp;&nbsp;'.to.'&nbsp;&nbsp;</span><span style="color: #FF8000;font-size: 16px;">'. date('H:i',strtotime($value['endTime'])).'</span></div><div class="col-md-2"><img onclick="removeBreakData('.$value['breakViewId'].')" style="width: 20px;" src="'.base_url('assets/img/cross.svg').'"></div></div>';
        	}

        	$result->breakData = $html;

        	$response = array("status" => 1,"message" => Viewdata,"result" => $result);
        }
    
        echo json_encode($response);
	}

	//In use
	function removeBreakTime()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$this->form_validation->set_rules('breakViewId','breakViewId','required'); 

    	//checking required parameters validation
    	if($this->form_validation->run()==false)
        { 
        	$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
            $response = array("status" => 0,"message" => $error);
        } 
        else 
        { 
        	$breakViewId = $this->input->post('breakViewId');

        	$this->AM->deleteHard(array("breakViewId" => $breakViewId),"machineBreakView");
        	

        	$response = array("status" => 1,"message" => Viewdatadelete);
        }
    
        echo json_encode($response);
	}

	//In use
	function addBreakTime()
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$this->form_validation->set_rules('viewId','viewId','required'); 
		$this->form_validation->set_rules('startTime','startTime','required'); 
		$this->form_validation->set_rules('stopTime','stopTime','required'); 

    	//checking required parameters validation
    	if($this->form_validation->run()==false)
        { 
        	$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
            $response = array("status" => 0,"message" => $error);
        } 
        else 
        { 
        	$viewId = $this->input->post('viewId');
        	$startTime = $this->input->post('startTime');
        	$stopTime = $this->input->post('stopTime');

        	$breakData = array(
        		"viewId" => $viewId,
        		"startTime" => $startTime,
        		"endTime" => $stopTime,
        	);

        	$breakViewId = $this->AM->insertData($breakData,"machineBreakView");
        	

        	$response = array("status" => 1,"message" => ViewBreaktimeadded,"breakViewId" => $breakViewId);
        }
    
        echo json_encode($response);
	}


	//secondsToDay function use for convert seconds to day
	//In use 
	function secondsToDay($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to day
		return sprintf('%0.5f', ($seconds) / (3600 * 24));
	}
	
	//secondsToDay function use for convert seconds to hour
	//In use
	function secondsToHours($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.5f', $seconds/3600);
    }

    function secondsToHoursTwoPoint($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.2f', $seconds/3600);
    }

    function secondsToHoursOnePoint($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.1f', $seconds/3600);
    }

    function convertValueOnePoint($value) 
	{
		
		//convert seconds to hour
		return sprintf('%0.1f', $value);
    }

    //In use
    function secondsToMinutes($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.5f', $seconds/60);
    }

    

    //breakdown function use for machine stop page detail

    //in use
	public function breakdown() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(8);

        if ($accessPoint == true) 
        {
			$listMachines = array();
			//Check user role and get machine as par user role
			if($this->AM->checkUserRole() > 0 ) 
			{
				$is_admin = $this->AM->checkUserRole();
				$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
			} 
			else 
			{
				$is_admin = 0;
				$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
			}
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$data = array(
			    'listMachines'=>$listMachines,
				'factoryData'=>$factoryData 
			    );
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('breakdown', $data);
			$this->load->view('footer');
			$this->load->view('script_file/breakdown_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	public function waitBreakdown() 
    {
        if($this->AM->checkIsvalidated() == false) 
        {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
            redirect('admin/login');
        }

        $accessPoint = $this->AM->accessPoint(9);

        if ($accessPoint == true) 
        {
            $listMachines = array();
            //Check user role and get machine as par user role
            if($this->AM->checkUserRole() > 0 ) 
            {
                $is_admin = $this->AM->checkUserRole();
                $listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
            } 
            else 
            {
                $is_admin = 0;
                $listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
            }
            $factoryData = $this->AM->getFactoryData($this->factoryId); 
            $data = array(
                'listMachines'=>$listMachines,
                'factoryData'=>$factoryData 
                );
            $this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('waitBreakdown', $data);
            $this->load->view('footer');
            $this->load->view('script_file/breakdown_footer');  
        }else
        {
        	$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
        }
        
    }

	//breakdown_pagination function use for get machine breakdown details
	public function breakdown_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) {
				$machineId = $this->input->post('machineId');
			}
		} 
		$choose1 = $this->input->post('choose1'); 
		$choose2 = $this->input->post('choose2'); 
		$choose3 = $this->input->post('choose3'); 
		$choose4 = $this->input->post('choose4'); 
		
		$duration1 = $this->input->post('duration1'); 
		$duration2 = $this->input->post('duration2'); 
		$duration3 = $this->input->post('duration3'); 
		$duration4 = $this->input->post('duration4'); 
		
		$result['printQuery'] = '';
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
		$choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
		
		$result['daily']['filter1'] = $result['daily']['filter1count'] = $x1 = $y1 = $br11 = $br12 = $br13 = $br14 = $br15 = 0; 
		$result['daily']['filter2'] = $result['daily']['filter2count'] = $x2 = $y2 = $br21 = $br22 = $br23 = $br24 = $br25 = 0;
		$result['daily']['filter3'] = $result['daily']['filter3count'] = $x3 = $y3 = $br31 = $br32 = $br33 = $br34 = $br35 = 0;
		$result['daily']['filter4'] = $result['daily']['filter4count'] = $x4 = $y4 = $br41 = $br42 = $br43 = $br44 = $br45 = 0;
		
		$br61 = $br62 = $br63 = $br64 = $br65 = $br71 = $br72 = $br73 = $br74 = $br75 = $br81 = $br82 = $br83 = $br84 = $br85 = 0;  
		$br91 = $br92 = $br93 = $br94 = $br95 = $br101 = $br102 = $br103 = $br104 = $br105 = $br111 = $br112 = $br113 = $br114 = $br115 = 0;


		$result['daily']['reasonfilter1'] = array();
		$result['daily']['reasonfilter2'] = array();
		$result['daily']['reasonfilter3'] = array();
		$result['daily']['reasonfilter4'] = array();
		
		$w3End = $duration4*60;
		$w4End = 9999999999;
		
		$aT = 0;
		$bT = 0;
		$cT = 0;
		$dT = 0;

		
		$analytics = $this->input->post('analytics');
		$machineResult = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");

		$upperLimitFirst = 0;
		$upperLimitSecond = 0;
		$upperLimitThird = 0;
		$upperLimitFour = 0;

		if ($analytics == "stopAnalytics") 
		{
			$upperLimitMin = $machineResult->stopUpperLimit;
		}else
		{
			$upperLimitMin = $machineResult->waitUpperLimit;
		}

		$upperLimitSec = $upperLimitMin * 60;

		if ($duration1 < $upperLimitMin && $duration2 >= $upperLimitMin && !empty($upperLimitMin)) 
		{
			$upperLimitFirst = 1;
		}

		if ($duration2 < $upperLimitMin && $duration3 >= $upperLimitMin && !empty($upperLimitMin)) 
		{
			$upperLimitFirst = 1;
			$upperLimitSecond = 1;
		}

		if ($duration3 < $upperLimitMin && $duration4 >= $upperLimitMin && !empty($upperLimitMin)) 
		{
			$upperLimitFirst = 1;
			$upperLimitSecond = 1;
			$upperLimitThird = 1;
		}

		if ($duration4 < $upperLimitMin && !empty($upperLimitMin)) 
		{
			$upperLimitFirst = 1;
			$upperLimitSecond = 1;
			$upperLimitThird = 1;
			$upperLimitFour = 1;
		}


		$machineBreakdownReasonData = $this->AM->getWhere(array("machineId" => $machineId,"breakdownReason !=" => ""),"machineBreakdownReason");

		$listReasons = array();
		foreach ($machineBreakdownReasonData as $key => $value) 
		{
			$listReasons[$key] = new stdClass;
			$listReasons[$key]->breakdownReasonId = $key + 1;
			$listReasons[$key]->breakdownReasonText = $value['breakdownReason'];
			$listReasons[$key]->errorTypeId = $value['errorTypeId'];
		}

		for ($dd=1; $dd < 11; $dd++) { 
			$result['daily']['reasonfilter1']['colorLi'.$dd]['color'] = ""; 
			$result['daily']['reasonfilter2']['colorLi'.$dd]['color'] = ""; 
			$result['daily']['reasonfilter3']['colorLi'.$dd]['color'] = ""; 
			$result['daily']['reasonfilter4']['colorLi'.$dd]['color'] = ""; 
		}

		//print_r($result);exit;

		$resonCount = count($listReasons);
		$listReasons[$resonCount] = new stdClass;
		$listReasons[$resonCount]->breakdownReasonId = $resonCount+1;   
		$listReasons[$resonCount]->breakdownReasonText = '';
		$listReasons[$resonCount]->errorTypeId = 11;

		//print_r($listReasons);exit;
		//Get machine stop detail as par selected filter
		$resultStopsQ = $this->AM->getStopAndWaitpAnalyticsData($this->factory, $machineId, $choose1, $choose2, $analytics); 
		if($resultStopsQ == '' ) 
		{
			$resultStops = array();
		} 
		else 
		{
			$resultStops = $resultStopsQ->result();


			//Get max seconds
			if (!empty($resultStops)) 
			{
				$result['daily']['maxSecondsValue'] = max(array_column($resultStops,"colorDuration"));
			}
			else
			{
				$result['daily']['maxSecondsValue'] = "";
			}
			if(count($resultStops) > 0 ) 
			{
				$listReasonsData = array_column($listReasons, "breakdownReasonText");
				for($x=0;$x<count($resultStops);$x++) 
				{
					if($resultStops[$x]->colorDuration >= $duration1*60 && $resultStops[$x]->colorDuration < $duration2*60) 
					{ 
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									//Set reason stop details

									$para1 = 'br'.$list->breakdownReasonId.'1';
									if ($list->breakdownReasonId == 5 && $aT == 0) 
									{
										$$para1 = 0;
										$aT = 1;
									}

									if ($list->errorTypeId == 1) 
									{
										$color = "#FFCF00";
									}else if ($list->errorTypeId == 2) 
									{
										$color = "#Ff8000";
									}else if ($list->errorTypeId == 3) 
									{
										$color = "#124D8D";
									}else if ($list->errorTypeId == 4) 
									{
										$color = "#002060";
									}else if ($list->errorTypeId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#d32f2f";
									}

									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment; 
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter1']['colorLi'.$list->breakdownReasonId]['color'] = $color; 
									$flag = 1; $$para1++;
									break; 
								}else if(!in_array($resultStops[$x]->comment, $listReasonsData))
								{
									$errorTypeIdVarOne = !empty($resultStops[$x]->reasonId) ? $resultStops[$x]->reasonId : "5";
									$para1 = 'br'.$errorTypeIdVarOne.'1';
									if ($errorTypeIdVarOne == 5 && $aT == 0) 
									{
										$$para1 = 0;
										$aT = 1;
									}

									if ($resultStops[$x]->reasonId == 1) 
									{
										$color = "#FFCF00";
									}else if ($resultStops[$x]->reasonId == 2) 
									{
										$color = "#Ff8000";
									}else if ($resultStops[$x]->reasonId == 3) 
									{
										$color = "#124D8D";
									}else if ($resultStops[$x]->reasonId == 4) 
									{
										$color = "#002060";
									}else if ($resultStops[$x]->reasonId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#CCD2DF";
									}

									$result['daily']['reasonfilter1']['li'.$errorTypeIdVarOne][$$para1][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter1']['li'.$errorTypeIdVarOne][$$para1][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter1']['li'.$errorTypeIdVarOne][$$para1][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
									$result['daily']['reasonfilter1']['li'.$errorTypeIdVarOne][$$para1][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
									$result['daily']['reasonfilter1']['li'.$errorTypeIdVarOne][$$para1][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter1']['colorLi'.$errorTypeIdVarOne]['color'] = $color; 
									$flag = 1; $$para1++;
									break; 
								}
							} 
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter1'][$x1][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter1'][$x1][1] = $resultStops[$x]->colorDuration;
							$result['daily']['stopfilter1'][$x1][2] = "";
							$result['daily']['stopfilter1'][$x1][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime));
							$result['daily']['stopfilter1'][$x1][4] = $resultStops[$x]->upperLimit;
							$x1++;
						}
						$result['daily']['filter1'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter1count']++; 
					} 
					if($resultStops[$x]->colorDuration >= $duration2*60 && $resultStops[$x]->colorDuration < $duration3*60) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									//Set reason stop details
									$para3 = 'br'.$list->errorTypeId.'2';
									if ($list->errorTypeId == 5 && $bT == 0) 
									{
										$$para3 = 0;
										$bT = 1;
									}

									if ($list->errorTypeId == 1) 
									{
										$color = "#FFCF00";
									}else if ($list->errorTypeId == 2) 
									{
										$color = "#Ff8000";
									}else if ($list->errorTypeId == 3) 
									{
										$color = "#124D8D";
									}else if ($list->errorTypeId == 4) 
									{
										$color = "#002060";
									}else if ($list->errorTypeId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#d32f2f";
									}

									$result['daily']['reasonfilter2']['li'.$list->errorTypeId][$$para3][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter2']['li'.$list->errorTypeId][$$para3][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter2']['li'.$list->errorTypeId][$$para3][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment; 
									$result['daily']['reasonfilter2']['li'.$list->errorTypeId][$$para3][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
									$result['daily']['reasonfilter2']['li'.$list->errorTypeId][$$para3][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter2']['colorLi'.$list->errorTypeId]['color'] = $color; 
									$flag = 1; $$para3++;
									break; 
								}else if(!in_array($resultStops[$x]->comment, $listReasonsData))
								{
									$errorTypeIdVarTwo = !empty($resultStops[$x]->reasonId) ? $resultStops[$x]->reasonId : "5";
									$para3 = 'br'.$errorTypeIdVarTwo.'2';
									if ($errorTypeIdVarTwo == 5 && $bT == 0) 
									{
										$$para3 = 0;
										$bT = 1;
									}

									if ($resultStops[$x]->reasonId == 1) 
									{
										$color = "#FFCF00";
									}else if ($resultStops[$x]->reasonId == 2) 
									{
										$color = "#Ff8000";
									}else if ($resultStops[$x]->reasonId == 3) 
									{
										$color = "#124D8D";
									}else if ($resultStops[$x]->reasonId == 4) 
									{
										$color = "#002060";
									}else if ($resultStops[$x]->reasonId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#CCD2DF";
									}

									$result['daily']['reasonfilter2']['li'.$errorTypeIdVarTwo][$$para3][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter2']['li'.$errorTypeIdVarTwo][$$para3][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter2']['li'.$errorTypeIdVarTwo][$$para3][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
									$result['daily']['reasonfilter2']['li'.$errorTypeIdVarTwo][$$para3][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
									$result['daily']['reasonfilter2']['li'.$errorTypeIdVarTwo][$$para3][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter2']['colorLi'.$errorTypeIdVarTwo]['color'] = $color; 
									$flag = 1; $$para3++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop detailsa
							$result['daily']['stopfilter2'][$x2][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter2'][$x2][1] = $resultStops[$x]->colorDuration;
							$result['daily']['stopfilter2'][$x2][2] = "";
							$result['daily']['stopfilter2'][$x2][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime));
							$result['daily']['stopfilter2'][$x2][4] = $resultStops[$x]->upperLimit;
							$x2++;
						}
						$result['daily']['filter2'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter2count']++;
					} 
					if($resultStops[$x]->colorDuration >= $duration3*60 && $resultStops[$x]->colorDuration < $duration4*60 ) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;

							foreach($listReasons as $list) {
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{

									$para5 = 'br'.$list->breakdownReasonId.'3';
									if ($list->breakdownReasonId == 5 && $cT == 0) {
										$$para5 = 0;
										$cT = 1;
									}

									if ($list->errorTypeId == 1) 
									{
										$color = "#FFCF00";
									}else if ($list->errorTypeId == 2) 
									{
										$color = "#Ff8000";
									}else if ($list->errorTypeId == 3) 
									{
										$color = "#124D8D";
									}else if ($list->errorTypeId == 4) 
									{
										$color = "#002060";
									}else if ($list->errorTypeId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#d32f2f";
									}

									//Set reason stop details
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime)); 
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter3']['colorLi'.$list->breakdownReasonId]['color'] = $color; 
									$flag = 1; $$para5++;
									break; 
								}else if(!in_array($resultStops[$x]->comment, $listReasonsData))
								{
									$errorTypeIdVarThree = !empty($resultStops[$x]->reasonId) ? $resultStops[$x]->reasonId : "5";
									$para5 = 'br'.$errorTypeIdVarThree.'3';
									if ($errorTypeIdVarThree == 5 && $cT == 0) {
										$$para5 = 0;
										$cT = 1;
									}

									if ($resultStops[$x]->reasonId == 1) 
									{
										$color = "#FFCF00";
									}else if ($resultStops[$x]->reasonId == 2) 
									{
										$color = "#Ff8000";
									}else if ($resultStops[$x]->reasonId == 3) 
									{
										$color = "#124D8D";
									}else if ($resultStops[$x]->reasonId == 4) 
									{
										$color = "#002060";
									}else if ($resultStops[$x]->reasonId == 5) 
									{
										$color = "#CCD2DF";
									}else
									{
										$color = "#CCD2DF";
									}
										
									$result['daily']['reasonfilter3']['li'.$errorTypeIdVarThree][$$para5][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter3']['li'.$errorTypeIdVarThree][$$para5][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
									$result['daily']['reasonfilter3']['li'.$errorTypeIdVarThree][$$para5][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
									$result['daily']['reasonfilter3']['li'.$errorTypeIdVarThree][$$para5][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime)); 
									$result['daily']['reasonfilter3']['li'.$errorTypeIdVarThree][$$para5][4] = $resultStops[$x]->upperLimit;
									$result['daily']['reasonfilter3']['colorLi'.$errorTypeIdVarThree]['color'] = $color; 
									$flag = 1; $$para5++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter3'][$x3][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter3'][$x3][1] = $resultStops[$x]->colorDuration; 
							$result['daily']['stopfilter3'][$x3][2] = ""; 
							$result['daily']['stopfilter3'][$x3][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime)); 
							$result['daily']['stopfilter3'][$x3][4] = $resultStops[$x]->upperLimit; 
							$x3++;
						}
						$result['daily']['filter3'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter3count']++;
					}
					
					if($resultStops[$x]->colorDuration >= $duration4*60 && $resultStops[$x]->colorDuration < $w4End) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
						$flag = 0;
						
						foreach($listReasons as $list) {

							
							if ($resultStops[$x]->comment == $list->breakdownReasonText) 
							{	//Set reason stop details

								$para7 = 'br'.$list->breakdownReasonId.'4';
								if ($list->breakdownReasonId == 5 && $dT == 0) 
								{
									$$para7 = 0;
									$dT = 1;
								}

								if ($list->errorTypeId == 1) 
								{
									$color = "#FFCF00";
								}else if ($list->errorTypeId == 2) 
								{
									$color = "#Ff8000";
								}else if ($list->errorTypeId == 3) 
								{
									$color = "#124D8D";
								}else if ($list->errorTypeId == 4) 
								{
									$color = "#002060";
								}else if ($list->errorTypeId == 5) 
								{
									$color = "#CCD2DF";
								}else
								{
									$color = "#d32f2f";
								}

								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][0] = $resultStops[$x]->timeS;
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration; 
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][4] = $resultStops[$x]->upperLimit; 
								$result['daily']['reasonfilter4']['colorLi'.$list->breakdownReasonId]['color'] = $color; 
								$flag = 1; $$para7++;
								break; 
							}else if(!in_array($resultStops[$x]->comment, $listReasonsData))
							{
								$errorTypeIdVarFour = !empty($resultStops[$x]->reasonId) ? $resultStops[$x]->reasonId : "5";
								$para7 = 'br'.$errorTypeIdVarFour.'4';
								if ($errorTypeIdVarFour == 5 && $dT == 0) 
								{
									$$para7 = 0;
									$dT = 1;
								}

								if ($resultStops[$x]->reasonId == 1) 
								{
									$color = "#FFCF00";
								}else if ($resultStops[$x]->reasonId == 2) 
								{
									$color = "#Ff8000";
								}else if ($resultStops[$x]->reasonId == 3) 
								{
									$color = "#124D8D";
								}else if ($resultStops[$x]->reasonId == 4) 
								{
									$color = "#002060";
								}else if ($resultStops[$x]->reasonId == 5) 
								{
									$color = "#CCD2DF";
								}else
								{
									$color = "#CCD2DF";
								}

								$result['daily']['reasonfilter4']['li'.$errorTypeIdVarFour][$$para7][0] = $resultStops[$x]->timeS;
								$result['daily']['reasonfilter4']['li'.$errorTypeIdVarFour][$$para7][1] = ($resultStops[$x]->upperLimit == "1") ? "3600" : $resultStops[$x]->colorDuration;
								$result['daily']['reasonfilter4']['li'.$errorTypeIdVarFour][$$para7][2] = ($resultStops[$x]->comment == "") ? "Unanswered" : $resultStops[$x]->comment;
								$result['daily']['reasonfilter4']['li'.$errorTypeIdVarFour][$$para7][3] = date('Y-m-d',strtotime($resultStops[$x]->addedDate)); 
								$result['daily']['reasonfilter4']['li'.$errorTypeIdVarFour][$$para7][4] = $resultStops[$x]->upperLimit;
								$result['daily']['reasonfilter4']['colorLi'.$errorTypeIdVarFour]['color'] = $color; 
								$flag = 1; $$para7++;
								break; 
							}
						}
						
					} 
					else 
					{
						//Set stop details
						$result['daily']['stopfilter4'][$x4][0] = $resultStops[$x]->timeS;
						$result['daily']['stopfilter4'][$x4][1] = $resultStops[$x]->colorDuration;
						$result['daily']['stopfilter4'][$x4][2] = "";
						$result['daily']['stopfilter4'][$x4][3] = date('Y-m-d',strtotime($resultStops[$x]->originalTime));
						$result['daily']['stopfilter4'][$x4][4] = $resultStops[$x]->upperLimit;
						 $x4++;
					}
						$result['daily']['filter4'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter4count']++;
					}
				}  
				
				if($result['daily']['filter1'] != 0) 
				{
					$result['daily']['filter1'] = sprintf('%0.2f', $result['daily']['filter1']/60);
				}
				if($result['daily']['filter2'] != 0) 
				{
					$result['daily']['filter2'] = sprintf('%0.2f', $result['daily']['filter2']/60);
				}
				if($result['daily']['filter3'] != 0) 
				{
					$result['daily']['filter3'] = sprintf('%0.2f', $result['daily']['filter3']/60);
				}
				if($result['daily']['filter4'] != 0) 
				{
					$result['daily']['filter4'] = sprintf('%0.2f', $result['daily']['filter4']/60);
				}
			}
		}
		
		$filter1Val = $this->input->post('filter1Val'); 
		$filter2Val = $this->input->post('filter2Val'); 
		$filter3Val = $this->input->post('filter3Val'); 
		$filter4Val = $this->input->post('filter4Val'); 


		$getErrorType = $this->AM->getErrorType()->result();


		$resonErrorCount = count($getErrorType);
		$getErrorType[$resonErrorCount] = new stdClass;
		$getErrorType[$resonErrorCount]->errorTypeId = $resonErrorCount+1;   
		$getErrorType[$resonErrorCount]->errorTypeText = '';
		$getErrorType[$resonErrorCount]->isDeleted = '0';

		$series = array();
		$unansweredArr = array();

		for ($q=1; $q < 101; $q++) 
		{ 
			$result['daily']['machine']['seriesName'.$q] = "";
			$result['daily']['machine']['seriesValue'.$q] = [0,0,0,0,0,0];

			$result['daily']['tool']['seriesName'.$q] = "";
			$result['daily']['tool']['seriesValue'.$q] = [0,0,0,0,0,0];

			$result['daily']['operator']['seriesName'.$q] = "";
			$result['daily']['operator']['seriesValue'.$q] = [0,0,0,0,0,0];

			$result['daily']['material']['seriesName'.$q] = "";
			$result['daily']['material']['seriesValue'.$q] = [0,0,0,0,0,0];

			$result['daily']['other']['seriesName'.$q] = "";
			$result['daily']['other']['seriesValue'.$q] = [0,0,0,0,0,0];
		}

		$result['daily']['unanswered']['seriesName1'] = "";
		$result['daily']['unanswered']['seriesValue1'] = "";
		for($i=0;$i<count($getErrorType);$i++) 
		{ 
			if ($getErrorType[$i]->errorTypeText == "Machine") 
			{
				$reasonNameArr[$i] = Machine;
			}else if ($getErrorType[$i]->errorTypeText == "Tool") 
			{
				$reasonNameArr[$i] = Tool;
			}else if ($getErrorType[$i]->errorTypeText == "Operator") 
			{
				$reasonNameArr[$i] = Operator;
			}else if ($getErrorType[$i]->errorTypeText == "Material") 
			{
				$reasonNameArr[$i] = Material;
			}else if ($getErrorType[$i]->errorTypeText == "Other") 
			{
				$reasonNameArr[$i] = Other;
			}
			
			$errorType = $this->AM->getWhere(array("errorTypeId" => $getErrorType[$i]->errorTypeId,"machineId" => $machineId),"machineBreakdownReason");
			$errorTypeBreakdownReason = array_column($errorType, "breakdownReason");
			if($filter1Val == '1') 
			{
				$minDuration1 = $duration1*60;
				$maxDuration1 = $duration2*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr1 = $this->AM->getStopAndWaitIssueReasonAnalyticsData($choose1, $choose2, $minDuration1, $maxDuration1, $machineId, $analytics);
				$reasonCountArr11[$i]['value'] = 0; 
				$reasonCountArr11Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr1)) 
				{
					for($x=0;$x<count($issueReasonArr1);$x++)
					{
						if ($issueReasonArr1[$x]->timeDiff < $upperLimitSec || empty($upperLimitSec)) 
						{
							if(in_array($issueReasonArr1[$x]->comment, $errorTypeBreakdownReason)) 
							{ 
								$reasonCountArr11[$i]['value']++;
								$reasonCountArr11Duration[$i]['value'] += $issueReasonArr1[$x]->timeDiff;
								
								if ($getErrorType[$i]->errorTypeId == "1") 
								{
									if (!empty($result['daily']['machineArr'][$issueReasonArr1[$x]->comment])) 
									{
										$result['daily']['machineArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff) + $result['daily']['machineArr'][$issueReasonArr1[$x]->comment];
									}else
									{
										$result['daily']['machineArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "2") 
								{
									if (!empty($result['daily']['toolArr'][$issueReasonArr1[$x]->comment])) 
									{
										$result['daily']['toolArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff) + $result['daily']['toolArr'][$issueReasonArr1[$x]->comment];
									}else
									{
										$result['daily']['toolArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "3") 
								{
									if (!empty($result['daily']['operatorArr'][$issueReasonArr1[$x]->comment])) 
									{
										$result['daily']['operatorArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff) + $result['daily']['operatorArr'][$issueReasonArr1[$x]->comment];
									}else
									{
										$result['daily']['operatorArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "4") 
								{
									if (!empty($result['daily']['materialArr'][$issueReasonArr1[$x]->comment])) 
									{
										$result['daily']['materialArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff) + $result['daily']['materialArr'][$issueReasonArr1[$x]->comment];
									}else
									{
										$result['daily']['materialArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff);
									}


									
								}
								

								if ($getErrorType[$i]->errorTypeId == "5") 
								{
									if (!empty($result['daily']['otherArr'][$issueReasonArr1[$x]->comment])) 
									{
										$result['daily']['otherArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff) + $result['daily']['otherArr'][$issueReasonArr1[$x]->comment];
									}else
									{
										$result['daily']['otherArr'][$issueReasonArr1[$x]->comment] = round($issueReasonArr1[$x]->timeDiff);
									}
									
								}	
								

							}elseif ($issueReasonArr1[$x]->comment == "" && $getErrorType[$i]->errorTypeText == "") 
							{
								$reasonCountArr11[$i]['value']++;
								$reasonCountArr11Duration[$i]['value'] += $issueReasonArr1[$x]->timeDiff;
								$unansweredArr[] = round($issueReasonArr1[$x]->timeDiff);
								
							}else
							{
								$otherDataArr[] = $issueReasonArr1[$x];
							}
						}

					}

					


				}
				$result['daily']['reasonCountArr1'] = $reasonCountArr11;
				$result['daily']['reasonCountArrDuration1'] = $reasonCountArr11Duration;

				
			}
			
			if($filter2Val == '1') 
			{
				$minDuration2 = $duration2*60;
				$maxDuration2 = $duration3*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr2 = $this->AM->getStopAndWaitIssueReasonAnalyticsData($choose1, $choose2, $minDuration2, $maxDuration2, $machineId, $analytics);
				$reasonCountArr21[$i]['value'] = 0;  $reasonCountArr21Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr2)) 
				{
					for($x=0;$x<count($issueReasonArr2);$x++)
					{
						if ($issueReasonArr2[$x]->timeDiff < $upperLimitSec || empty($upperLimitSec)) 
						{
							if(in_array($issueReasonArr2[$x]->comment, $errorTypeBreakdownReason)) 
							{
								$reasonCountArr21[$i]['value']++;
								$reasonCountArr21Duration[$i]['value'] += $issueReasonArr2[$x]->timeDiff;


								if ($getErrorType[$i]->errorTypeId == "1") 
								{
									if (!empty($result['daily']['machineArr'][$issueReasonArr2[$x]->comment])) 
									{
										$result['daily']['machineArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff) + $result['daily']['machineArr'][$issueReasonArr2[$x]->comment];
									}else
									{
										$result['daily']['machineArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "2") 
								{
									if (!empty($result['daily']['toolArr'][$issueReasonArr2[$x]->comment])) 
									{
										$result['daily']['toolArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff) + $result['daily']['toolArr'][$issueReasonArr2[$x]->comment];
									}else
									{
										$result['daily']['toolArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "3") 
								{
									if (!empty($result['daily']['operatorArr'][$issueReasonArr2[$x]->comment])) 
									{
										$result['daily']['operatorArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff) + $result['daily']['operatorArr'][$issueReasonArr2[$x]->comment];
									}else
									{
										$result['daily']['operatorArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "4") 
								{
									if (!empty($result['daily']['materialArr'][$issueReasonArr2[$x]->comment])) 
									{
										$result['daily']['materialArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff) + $result['daily']['materialArr'][$issueReasonArr2[$x]->comment];
									}else
									{
										$result['daily']['materialArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff);
									}


									
								}
								

								if ($getErrorType[$i]->errorTypeId == "5") 
								{
									if (!empty($result['daily']['otherArr'][$issueReasonArr2[$x]->comment])) 
									{
										$result['daily']['otherArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff) + $result['daily']['otherArr'][$issueReasonArr2[$x]->comment];
									}else
									{
										$result['daily']['otherArr'][$issueReasonArr2[$x]->comment] = round($issueReasonArr2[$x]->timeDiff);
									}
									
								}	


							}elseif ($issueReasonArr2[$x]->comment == "" && $getErrorType[$i]->errorTypeText == "") 
							{
								$reasonCountArr21[$i]['value']++;
								$reasonCountArr21Duration[$i]['value'] += $issueReasonArr2[$x]->timeDiff;

								$unansweredArr[] = round($issueReasonArr2[$x]->timeDiff);
							}else
							{
								$otherDataArr[] = $issueReasonArr2[$x];
							}
						}
					}
				}
				$result['daily']['reasonCountArr2'] = $reasonCountArr21; 
				$result['daily']['reasonCountArrDuration2'] = $reasonCountArr21Duration;




			}
			
			
			if($filter3Val == '1') 
			{
				$minDuration3 = $duration3*60;
				$maxDuration3 = $duration4*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr3 = $this->AM->getStopAndWaitIssueReasonAnalyticsData($choose1, $choose2, $minDuration3, $maxDuration3, $machineId, $analytics);
				$reasonCountArr31[$i]['value'] = 0;  $reasonCountArr31Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr3)) 
				{
					for($x=0;$x<count($issueReasonArr3);$x++)
					{
						if ($issueReasonArr3[$x]->timeDiff < $upperLimitSec || empty($upperLimitSec)) 
						{
							if(in_array($issueReasonArr3[$x]->comment, $errorTypeBreakdownReason)) 
							{
								$reasonCountArr31[$i]['value']++;
								$reasonCountArr31Duration[$i]['value'] += $issueReasonArr3[$x]->timeDiff;

								
									if ($getErrorType[$i]->errorTypeId == "1") 
									{	
										if (!empty($result['daily']['machineArr'][$issueReasonArr3[$x]->comment])) 
										{
											$result['daily']['machineArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff) + $result['daily']['machineArr'][$issueReasonArr3[$x]->comment];
										}else
										{
											$result['daily']['machineArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff);
										}
										
									}

									if ($getErrorType[$i]->errorTypeId == "2") 
									{
										if (!empty($result['daily']['toolArr'][$issueReasonArr3[$x]->comment])) 
										{
											$result['daily']['toolArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff) + $result['daily']['toolArr'][$issueReasonArr3[$x]->comment];
										}else
										{
											$result['daily']['toolArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff);
										}
										
									}

									if ($getErrorType[$i]->errorTypeId == "3") 
									{
										if (!empty($result['daily']['operatorArr'][$issueReasonArr3[$x]->comment])) 
										{
											$result['daily']['operatorArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff) + $result['daily']['operatorArr'][$issueReasonArr3[$x]->comment];
										}else
										{
											$result['daily']['operatorArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff);
										}
										
									}

									if ($getErrorType[$i]->errorTypeId == "4") 
									{
										if (!empty($result['daily']['materialArr'][$issueReasonArr3[$x]->comment])) 
										{
											$result['daily']['materialArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff) + $result['daily']['materialArr'][$issueReasonArr3[$x]->comment];
										}else
										{
											$result['daily']['materialArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff);
										}


										
									}
									

									if ($getErrorType[$i]->errorTypeId == "5") 
									{
										if (!empty($result['daily']['otherArr'][$issueReasonArr3[$x]->comment])) 
										{
											$result['daily']['otherArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff) + $result['daily']['otherArr'][$issueReasonArr3[$x]->comment];
										}else
										{
											$result['daily']['otherArr'][$issueReasonArr3[$x]->comment] = round($issueReasonArr3[$x]->timeDiff);
										}
										
									}

							}elseif ($issueReasonArr3[$x]->comment == "" && $getErrorType[$i]->errorTypeText == "") 
							{
								$reasonCountArr31[$i]['value']++;
								$reasonCountArr31Duration[$i]['value'] += $issueReasonArr3[$x]->timeDiff;

								$unansweredArr[] = round($issueReasonArr3[$x]->timeDiff);
							}else
							{
								$otherDataArr[] = $issueReasonArr3[$x];
							}
						}
					}
				}
				$result['daily']['reasonCountArr3'] = $reasonCountArr31; 
				$result['daily']['reasonCountArrDuration3'] = $reasonCountArr31Duration;
			}
			
			if($filter4Val == '1') 
			{
				$minDuration4 = $duration4*60;
				$maxDuration4 = $w4End;
				//Get reason as page time to set in graph stop count
				$issueReasonArr4 = $this->AM->getStopAndWaitIssueReasonAnalyticsData($choose1, $choose2, $minDuration4, $maxDuration4, $machineId, $analytics);
				//echo $this->factory->last_query();exit;
				$reasonCountArr41[$i]['value'] = 0;  $reasonCountArr41Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr4)) {
					for($x=0;$x<count($issueReasonArr4);$x++)
					{
						if ($issueReasonArr4[$x]->timeDiff < $upperLimitSec || empty($upperLimitSec)) 
						{
							if(in_array($issueReasonArr4[$x]->comment, $errorTypeBreakdownReason)) 
							{
								$reasonCountArr41[$i]['value']++;
								$reasonCountArr41Duration[$i]['value'] += $issueReasonArr4[$x]->timeDiff;

								if ($getErrorType[$i]->errorTypeId == "1") 
								{
									if (!empty($result['daily']['machineArr'][$issueReasonArr4[$x]->comment])) 
									{
										$result['daily']['machineArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff) + $result['daily']['machineArr'][$issueReasonArr4[$x]->comment];
									}else
									{
										$result['daily']['machineArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "2") 
								{
									if (!empty($result['daily']['toolArr'][$issueReasonArr4[$x]->comment])) 
									{
										$result['daily']['toolArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff) + $result['daily']['toolArr'][$issueReasonArr4[$x]->comment];
									}else
									{
										$result['daily']['toolArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "3") 
								{
									if (!empty($result['daily']['operatorArr'][$issueReasonArr4[$x]->comment])) 
									{
										$result['daily']['operatorArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff) + $result['daily']['operatorArr'][$issueReasonArr4[$x]->comment];
									}else
									{
										$result['daily']['operatorArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff);
									}
									
								}

								if ($getErrorType[$i]->errorTypeId == "4") 
								{
									if (!empty($result['daily']['materialArr'][$issueReasonArr4[$x]->comment])) 
									{
										$result['daily']['materialArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff) + $result['daily']['materialArr'][$issueReasonArr4[$x]->comment];
									}else
									{
										$result['daily']['materialArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff);
									}


									
								}
								

								if ($getErrorType[$i]->errorTypeId == "5") 
								{
									if (!empty($result['daily']['otherArr'][$issueReasonArr4[$x]->comment])) 
									{
										$result['daily']['otherArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff) + $result['daily']['otherArr'][$issueReasonArr4[$x]->comment];
									}else
									{
										$result['daily']['otherArr'][$issueReasonArr4[$x]->comment] = round($issueReasonArr4[$x]->timeDiff);
									}
									
								}

							}elseif ($issueReasonArr4[$x]->comment == "" && $getErrorType[$i]->errorTypeText == "") 
							{
								$reasonCountArr41[$i]['value']++;
								$reasonCountArr41Duration[$i]['value'] += $issueReasonArr4[$x]->timeDiff;

								$unansweredArr[] = round($issueReasonArr4[$x]->timeDiff);
							}else
							{
								$otherDataArr[] = $issueReasonArr4[$x];
							}
						}
					}

				//	$otherDataArr = array_unique($otherDataArr);
				}
				$result['daily']['reasonCountArr4'] = $reasonCountArr41; 
				$result['daily']['reasonCountArrDuration4'] = $reasonCountArr41Duration;
			}
		}


		if (!empty($otherDataArr)) 
		{
			$otherDataArr = array_map("unserialize", array_unique(array_map("serialize", $otherDataArr)));
			//print_r($otherDataArr);
			//print_r($listReasonsData);exit;
			if (!empty($listReasonsData)) 
			{
				foreach ($otherDataArr as $keyO => $valueO) 
				{
					if(!in_array($valueO->comment, $listReasonsData)) 
					{	
						if (!empty($result['daily']['otherArr'][$valueO->comment])) 
						{
							$result['daily']['otherArr'][$valueO->comment] = round($valueO->timeDiff) + $result['daily']['otherArr'][$valueO->comment];
						}else
						{
							$result['daily']['otherArr'][$valueO->comment] = round($valueO->timeDiff);
						}
					}
				}
			}
		}
		//print_r($otherDataArr);

		$machineBreakdownReasonData = $this->AM->getWhere(array("machineId" => $machineId,"breakdownReason !=" => ""),"machineBreakdownReason");
		$breakdownReasonArr = array_column($machineBreakdownReasonData, "breakdownReason");
		

		$maxValueMachineArr = array(0);
		$maxValueToolArr = array(0);
		$maxValueOperatorArr = array(0);
		$maxValueMaterialArr = array(0);
		$maxValueOtherArr = array(0);
		$machineColor = array("#FFCF00","#FCD219","#FFD933","#FFDD4D","#FFE266","#FFE780","#FFEC99","#FFF1B3","#FFF5CC","#FFFAE6");
		$a = 1; 
		if (!empty($result['daily']['machineArr'])) 
		{
			foreach ($result['daily']['machineArr'] as $key => $value) 
			{
				$maxValueMachineArr[] = $this->secondsToMinutes($value);
				$result['daily']['machine']['seriesName'.$a] = $key;
				$result['daily']['machine']['seriesValue'.$a] = [0,0,0,0,0,$this->secondsToMinutes($value)];
				$result['daily']['machine']['seriesColor'.$a] = $machineColor[$a-1];
				$a++;
			}
		}

		//print_r($result['daily']['machine']);exit;
		$b = 1; 
		if (!empty($result['daily']['toolArr'])) 
		{
			foreach ($result['daily']['toolArr'] as $key => $value) 
			{
				$maxValueToolArr[] = $this->secondsToMinutes($value);
				$result['daily']['tool']['seriesName'.$b] = $key;
				$result['daily']['tool']['seriesValue'.$b] = [0,0,0,0,$this->secondsToMinutes($value),0];
				$b++;
			}
		}

		$c = 1; 
		if (!empty($result['daily']['operatorArr'])) 
		{
			foreach ($result['daily']['operatorArr'] as $key => $value) 
			{
				$maxValueOperatorArr[] = $this->secondsToMinutes($value);
				$result['daily']['operator']['seriesName'.$c] = $key;
				$result['daily']['operator']['seriesValue'.$c] = [0,0,0,$this->secondsToMinutes($value),0,0];
				$c++;
			}
		}

		$d = 1;
		if (!empty($result['daily']['materialArr'])) 
		{
			foreach ($result['daily']['materialArr'] as $key => $value) 
			{
				$maxValueMaterialArr[] = $this->secondsToMinutes($value);
				$result['daily']['material']['seriesName'.$d] = $key;
				$result['daily']['material']['seriesValue'.$d] = [0,0,$this->secondsToMinutes($value),0,0,0];
				$d++;
			}
		}

		if (!empty($result['daily']['otherArr'])) 
		{
			//print_r($result['daily']['otherArr']);
			$e = 1;
			foreach ($result['daily']['otherArr'] as $key => $value) 
			{
				$checkOldReason = $this->AM->getStopAndWaitReasonOldData($machineId, $analytics, $key);

				if ($checkOldReason->reasonId == "1") 
				{

					$maxValueMachineArr[] = $this->secondsToMinutes($value);
					$result['daily']['machine']['seriesName'.$a] = $key;
					$result['daily']['machine']['seriesValue'.$a] = [0,0,0,0,0,$this->secondsToMinutes($value)];
					$a++;
				}else if ($checkOldReason->reasonId == "2") 
				{
					$maxValueToolArr[] = $this->secondsToMinutes($value);
					$result['daily']['tool']['seriesName'.$b] = $key;
					$result['daily']['tool']['seriesValue'.$b] = [0,0,0,0,$this->secondsToMinutes($value),0];
					$b++;
				}else if ($checkOldReason->reasonId == "3") 
				{
					$maxValueOperatorArr[] = $this->secondsToMinutes($value);
					$result['daily']['operator']['seriesName'.$c] = $key;
					$result['daily']['operator']['seriesValue'.$c] = [0,0,0,$this->secondsToMinutes($value),0,0];
					$c++;
				}else if ($checkOldReason->reasonId == "4") 
				{
					$maxValueMaterialArr[] = $this->secondsToMinutes($value);
					$result['daily']['material']['seriesName'.$d] = $key;
					$result['daily']['material']['seriesValue'.$d] = [0,0,$this->secondsToMinutes($value),0,0,0];
					$d++;
				}else if ($checkOldReason->reasonId == "5") 
				{
					$maxValueOtherArr[] = $this->secondsToMinutes($value);
					$result['daily']['other']['seriesName'.$e] = $key;
					$result['daily']['other']['seriesValue'.$e] = [0,$this->secondsToMinutes($value),0,0,0,0];
					
					$e++;
				}else
				{
					$maxValueOtherArr[] = $this->secondsToMinutes($value);
					$result['daily']['other']['seriesName'.$e] = $key;
					$result['daily']['other']['seriesValue'.$e] = [0,$this->secondsToMinutes($value),0,0,0,0];
					
					$e++;
				}

				
			}
			//$secondOtherData = array_sum($maxValueOtherArr);
			
		}
		$maxValueUnArr = 0;
		if (!empty($unansweredArr)) 
		{
			$unansweredTotal = array_sum($unansweredArr);
			$maxValueUnArr = $this->secondsToMinutes($unansweredTotal);
			$result['daily']['unanswered']['seriesName1'] = Unanswered;
			$result['daily']['unanswered']['seriesValue1'] =  [$this->secondsToMinutes($unansweredTotal),0,0,0,0,0];
		}
		$reasonNameArr[5] = Unanswered; 

		if ($analytics == "waitAnalytics") {
			$errorReasonNotification = ($machineResult->prolongedWait == "0") ? None." - " : $machineResult->prolongedWait ." ".min." - ";
			$errorReasonNotification .= ($machineResult->waitUpperLimit == "0") ? None : $machineResult->waitUpperLimit ." ".min;
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyWait == "0") ? None : After." ". $machineResult->notifyWait ." ".min;
            $result['machineNotifyTime']['errorReasonNotification'] = $errorReasonNotification;
        }
        else
        {
        	$errorReasonNotification = ($machineResult->prolongedStop == "0") ? None." - " : $machineResult->prolongedStop ." ".min." - ";
			$errorReasonNotification .= ($machineResult->stopUpperLimit == "0") ? None : $machineResult->stopUpperLimit ." ".min;
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyStop == "0") ? None : After." ". $machineResult->notifyStop ." ".min;
            $result['machineNotifyTime']['errorReasonNotification'] = $errorReasonNotification;
        }
        //print_r($series);exit;
      
        $maxValueArr = array(array_sum($maxValueMachineArr),array_sum($maxValueToolArr),array_sum($maxValueOperatorArr),array_sum($maxValueMaterialArr),array_sum($maxValueOtherArr),$maxValueUnArr);

       // print_r($series);exit;
		$result['daily']['upperLimit'] = $upperLimitMin * 60;
		$result['daily']['upperLimitFirst'] = $upperLimitFirst;
		$result['daily']['upperLimitSecond'] = $upperLimitSecond;
		$result['daily']['upperLimitThird'] = $upperLimitThird;
		$result['daily']['upperLimitFour'] = $upperLimitFour;
		$result['daily']['reasonNameArr'] = $reasonNameArr;
		$result['daily']['maxValueArr'] = max($maxValueArr);
		//Set array to manage data in graph
		$result['daily']['reasonCountArr1'][0]['itemStyle']['color'] = '#FFCF00'; 
		$result['daily']['reasonCountArr2'][0]['itemStyle']['color'] = '#FCD219'; 
		$result['daily']['reasonCountArr3'][0]['itemStyle']['color'] = '#FFD933'; 
		$result['daily']['reasonCountArr4'][0]['itemStyle']['color'] = '#FFDD4D';
		$result['daily']['reasonCountArr5'][0]['itemStyle']['color'] = '#FFE266';
		$result['daily']['reasonCountArr6'][0]['itemStyle']['color'] = '#FFE780';
		$result['daily']['reasonCountArr7'][0]['itemStyle']['color'] = '#FFEC99';
		$result['daily']['reasonCountArr8'][0]['itemStyle']['color'] = '#FFF1B3';
		$result['daily']['reasonCountArr9'][0]['itemStyle']['color'] = '#FFF5CC';
		$result['daily']['reasonCountArr10'][0]['itemStyle']['color'] = '#FFFAE6';


		$result['daily']['reasonCountArr1'][1]['itemStyle']['color'] = '#Ff8000';  
		$result['daily']['reasonCountArr2'][1]['itemStyle']['color'] = '#FF8D1A';  
		$result['daily']['reasonCountArr3'][1]['itemStyle']['color'] = '#Ff9933';  
		$result['daily']['reasonCountArr4'][1]['itemStyle']['color'] = '#FFA64D';
		$result['daily']['reasonCountArr5'][1]['itemStyle']['color'] = '#FFB366';
		$result['daily']['reasonCountArr6'][1]['itemStyle']['color'] = '#FFC080';
		$result['daily']['reasonCountArr7'][1]['itemStyle']['color'] = '#FFCC99';
		$result['daily']['reasonCountArr8'][1]['itemStyle']['color'] = '#FFD9B3';
		$result['daily']['reasonCountArr9'][1]['itemStyle']['color'] = '#FFE6CC';
		$result['daily']['reasonCountArr10'][1]['itemStyle']['color'] = '#FFF2E6';



		$result['daily']['reasonCountArr1'][2]['itemStyle']['color'] = '#002060'; 
		$result['daily']['reasonCountArr2'][2]['itemStyle']['color'] = '#1A3670'; 
		$result['daily']['reasonCountArr3'][2]['itemStyle']['color'] = '#334D80'; 
		$result['daily']['reasonCountArr4'][2]['itemStyle']['color'] = '#4D6390';
		$result['daily']['reasonCountArr5'][2]['itemStyle']['color'] = '#6679A0';
		$result['daily']['reasonCountArr6'][2]['itemStyle']['color'] = '#8090B0';
		$result['daily']['reasonCountArr7'][2]['itemStyle']['color'] = '#99A6BF';
		$result['daily']['reasonCountArr8'][2]['itemStyle']['color'] = '#B3BCCF';
		$result['daily']['reasonCountArr9'][2]['itemStyle']['color'] = '#CCD2DF';
		$result['daily']['reasonCountArr10'][2]['itemStyle']['color'] = '#E6E9EF';


		$result['daily']['reasonCountArr1'][3]['itemStyle']['color'] = '#124D8D'; 
		$result['daily']['reasonCountArr2'][3]['itemStyle']['color'] = '#2A5F98'; 
		$result['daily']['reasonCountArr3'][3]['itemStyle']['color'] = '#4171A4'; 
		$result['daily']['reasonCountArr4'][3]['itemStyle']['color'] = '#5982AF';
		$result['daily']['reasonCountArr5'][3]['itemStyle']['color'] = '#7194BB';
		$result['daily']['reasonCountArr6'][3]['itemStyle']['color'] = '#89A6C6';
		$result['daily']['reasonCountArr7'][3]['itemStyle']['color'] = '#A0B8D1';
		$result['daily']['reasonCountArr8'][3]['itemStyle']['color'] = '#B8CADD';
		$result['daily']['reasonCountArr9'][3]['itemStyle']['color'] = '#D0DBE8';
		$result['daily']['reasonCountArr10'][3]['itemStyle']['color'] = '#E7EDF4';



		$result['daily']['reasonCountArr1'][4]['itemStyle']['color'] = '#CCD2DF'; 
		$result['daily']['reasonCountArr2'][4]['itemStyle']['color'] = '#D1D7E2'; 
		$result['daily']['reasonCountArr3'][4]['itemStyle']['color'] = '#D6DBE5'; 
		$result['daily']['reasonCountArr4'][4]['itemStyle']['color'] = '#DBE0E9';
		$result['daily']['reasonCountArr5'][4]['itemStyle']['color'] = '#E0E4EC';
		$result['daily']['reasonCountArr6'][4]['itemStyle']['color'] = '#E6E9EF';
		$result['daily']['reasonCountArr7'][4]['itemStyle']['color'] = '#EBEDF2';
		$result['daily']['reasonCountArr8'][4]['itemStyle']['color'] = '#F0F2F5';
		$result['daily']['reasonCountArr9'][4]['itemStyle']['color'] = '#F5F6F9';
		$result['daily']['reasonCountArr10'][4]['itemStyle']['color'] = '#FAFBFC';


		$result['daily']['reasonCountArr1'][5]['itemStyle']['color'] = '#d32f2f'; 
		$result['daily']['reasonCountArr2'][5]['itemStyle']['color'] = '#d32f2f'; 
		$result['daily']['reasonCountArr3'][5]['itemStyle']['color'] = '#d32f2f'; 
		$result['daily']['reasonCountArr4'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr5'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr6'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr7'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr8'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr9'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArr10'][5]['itemStyle']['color'] = '#d32f2f';

		/*$result['daily']['reasonCountArr1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArr1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][7]['itemStyle']['color'] = '#d32f2f';*/
		
		$result['daily']['reasonCountArrDuration1'][0]['itemStyle']['color'] = '#FFCF00';
		$result['daily']['reasonCountArrDuration2'][0]['itemStyle']['color'] = '#FCD219';
		$result['daily']['reasonCountArrDuration3'][0]['itemStyle']['color'] = '#FFD933';
		$result['daily']['reasonCountArrDuration4'][0]['itemStyle']['color'] = '#FFDD4D';
		$result['daily']['reasonCountArrDuration5'][0]['itemStyle']['color'] = '#FFE266';
		$result['daily']['reasonCountArrDuration6'][0]['itemStyle']['color'] = '#FFE780';
		$result['daily']['reasonCountArrDuration7'][0]['itemStyle']['color'] = '#FFEC99';
		$result['daily']['reasonCountArrDuration8'][0]['itemStyle']['color'] = '#FFF1B3';
		$result['daily']['reasonCountArrDuration9'][0]['itemStyle']['color'] = '#FFF5CC';
		$result['daily']['reasonCountArrDuration10'][0]['itemStyle']['color'] = '#FFFAE6';

		$result['daily']['reasonCountArrDuration1'][1]['itemStyle']['color'] = '#Ff8000';
		$result['daily']['reasonCountArrDuration2'][1]['itemStyle']['color'] = '#FF8D1A';
		$result['daily']['reasonCountArrDuration3'][1]['itemStyle']['color'] = '#Ff9933';
		$result['daily']['reasonCountArrDuration4'][1]['itemStyle']['color'] = '#FFA64D';
		$result['daily']['reasonCountArrDuration5'][1]['itemStyle']['color'] = '#FFB366';
		$result['daily']['reasonCountArrDuration6'][1]['itemStyle']['color'] = '#FFC080';
		$result['daily']['reasonCountArrDuration7'][1]['itemStyle']['color'] = '#FFCC99';
		$result['daily']['reasonCountArrDuration8'][1]['itemStyle']['color'] = '#FFD9B3';
		$result['daily']['reasonCountArrDuration9'][1]['itemStyle']['color'] = '#FFE6CC';
		$result['daily']['reasonCountArrDuration10'][1]['itemStyle']['color'] = '#FFF2E6';


		$result['daily']['reasonCountArrDuration1'][2]['itemStyle']['color'] = '#002060';
		$result['daily']['reasonCountArrDuration2'][2]['itemStyle']['color'] = '#1A3670';
		$result['daily']['reasonCountArrDuration3'][2]['itemStyle']['color'] = '#334D80';
		$result['daily']['reasonCountArrDuration4'][2]['itemStyle']['color'] = '#4D6390';
		$result['daily']['reasonCountArrDuration5'][2]['itemStyle']['color'] = '#6679A0';
		$result['daily']['reasonCountArrDuration6'][2]['itemStyle']['color'] = '#8090B0';
		$result['daily']['reasonCountArrDuration7'][2]['itemStyle']['color'] = '#99A6BF';
		$result['daily']['reasonCountArrDuration8'][2]['itemStyle']['color'] = '#B3BCCF';
		$result['daily']['reasonCountArrDuration9'][2]['itemStyle']['color'] = '#CCD2DF';
		$result['daily']['reasonCountArrDuration10'][2]['itemStyle']['color'] = '#E6E9EF';


		$result['daily']['reasonCountArrDuration1'][3]['itemStyle']['color'] = '#124D8D';
		$result['daily']['reasonCountArrDuration2'][3]['itemStyle']['color'] = '#2A5F98';
		$result['daily']['reasonCountArrDuration3'][3]['itemStyle']['color'] = '#4171A4';
		$result['daily']['reasonCountArrDuration4'][3]['itemStyle']['color'] = '#5982AF';
		$result['daily']['reasonCountArrDuration5'][3]['itemStyle']['color'] = '#7194BB';
		$result['daily']['reasonCountArrDuration6'][3]['itemStyle']['color'] = '#89A6C6';
		$result['daily']['reasonCountArrDuration7'][3]['itemStyle']['color'] = '#A0B8D1';
		$result['daily']['reasonCountArrDuration8'][3]['itemStyle']['color'] = '#B8CADD';
		$result['daily']['reasonCountArrDuration9'][3]['itemStyle']['color'] = '#D0DBE8';
		$result['daily']['reasonCountArrDuration10'][3]['itemStyle']['color'] = '#E7EDF4';


		$result['daily']['reasonCountArrDuration1'][4]['itemStyle']['color'] = '#CCD2DF';
		$result['daily']['reasonCountArrDuration2'][4]['itemStyle']['color'] = '#D1D7E2';
		$result['daily']['reasonCountArrDuration3'][4]['itemStyle']['color'] = '#D6DBE5';
		$result['daily']['reasonCountArrDuration4'][4]['itemStyle']['color'] = '#DBE0E9';
		$result['daily']['reasonCountArrDuration5'][4]['itemStyle']['color'] = '#E0E4EC';
		$result['daily']['reasonCountArrDuration6'][4]['itemStyle']['color'] = '#E6E9EF';
		$result['daily']['reasonCountArrDuration7'][4]['itemStyle']['color'] = '#EBEDF2';
		$result['daily']['reasonCountArrDuration8'][4]['itemStyle']['color'] = '#F0F2F5';
		$result['daily']['reasonCountArrDuration9'][4]['itemStyle']['color'] = '#F5F6F9';
		$result['daily']['reasonCountArrDuration10'][4]['itemStyle']['color'] = '#FAFBFC';

		$result['daily']['reasonCountArrDuration1'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration2'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration3'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration4'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration5'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration6'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration7'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration8'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration9'][5]['itemStyle']['color'] = '#d32f2f';
		$result['daily']['reasonCountArrDuration10'][5]['itemStyle']['color'] = '#d32f2f';



		/*$result['daily']['reasonCountArrDuration1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArrDuration1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][7]['itemStyle']['color'] = '#d32f2f';*/
		echo json_encode($result); die;  
	}


	public function breakdown_test() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(8);

        if ($accessPoint == true) 
        {
			$listMachines = array();
			//Check user role and get machine as par user role
			if($this->AM->checkUserRole() > 0 ) 
			{
				$is_admin = $this->AM->checkUserRole();
				$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
			} 
			else 
			{
				$is_admin = 0;
				$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
			}
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$data = array(
			    'listMachines'=>$listMachines,
				'factoryData'=>$factoryData 
			    );
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('breakdown_test', $data);
			$this->load->view('footer');
			$this->load->view('script_file/breakdowntest_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	public function beta_GraphLog() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
		}
		
		$data = array(
		    'listMachines'=>$listMachines
		    );

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('beta_graphlog', $data);
		$this->load->view('footer');
		$this->load->view('script_file/beta_graphlog_footer');
	}

	//In use
	function exportBetaDetectionCsv()
    {

    	$machineId = 0; 
		if($this->input->get('machineId')) 
		{ 
			if($this->input->get('machineId') > 0 ) 
			{
				$machineId = $this->input->get('machineId');
			}
		} 
		if($this->input->get('dateValS')) 
		{ 
			$dateValS = $this->input->get('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->get('dateValE')) 
		{ 
			$dateValE = $this->input->get('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}

		$searchValue = $this->input->get('serchFiled');

		$result = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");

		if ($result->isDataGet == "4") 
		{

			if($searchValue != '')
			{
			   if ($searchValue == "Running" || $searchValue == "Active") 
			   {
			   	  $searchQuery = " and (
					virtualMachineLog.signalType like '%1%' 
					)  ";  
			   }else if ($searchValue == "Stopped" || $searchValue == "Not active") 
			   {
			   	   $searchQuery = " and (
					virtualMachineLog.signalType like '%0%' 
					) ";  
			   }
			   else
			   {
			   	$searchQuery = " and (
					virtualMachineLog.machineId like '%".$searchValue."%' or 
					virtualMachineLog.originalTime like '%".$searchValue."%' or
					machine.machineName like '%".$searchValue."%' 
					) ";  
			   }
			   
			}

			
			$listColorLogs = $this->AM->getAllVirtualMachineBLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)->result();
	    	$data = array(); 

			for ($key=0;$key<count($listColorLogs);$key++) 
			{	

				if($listColorLogs[$key]->signalType == "1" && $listColorLogs[$key]->signalTypeStatus == "vMachine")
				{
					$listColorLogs[$key]->machineStatus = Running;
					$listColorLogs[$key]->color = Active;
				}else if($listColorLogs[$key]->signalType == "0" && $listColorLogs[$key]->signalTypeStatus == "vMachine")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
					$listColorLogs[$key]->color = Notactive;
				}else
				{
					if ($listColorLogs[$key]->signalType == "0") 
					{
						$listColorLogs[$key]->machineStatus = NoData;
						$listColorLogs[$key]->color = NoData;
					}
					else if ($listColorLogs[$key]->signalType == "2") 
					{
						$listColorLogs[$key]->machineStatus = Noproduction;
						$listColorLogs[$key]->color = NoData;
					}else if ($listColorLogs[$key]->signalType == "1") 
					{
						$listColorLogs[$key]->machineStatus = Setup;
						$listColorLogs[$key]->color = NoData;
					}else{
						$listColorLogs[$key]->machineStatus = NoData;
					}
				}

				$data[] = array(
					"logId" => $listColorLogs[$key]->logId,
					"machineId" => $listColorLogs[$key]->machineId,
					"machineName" => $listColorLogs[$key]->machineName,
					"color" => $listColorLogs[$key]->color,
					"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? Invalid : $listColorLogs[$key]->machineStatus,
					"originalTime" => $listColorLogs[$key]->originalTime
				);
			}
		}
		else if ($result->isDataGet == "2") 
		{

			if($searchValue != '')
			{
			   if ($searchValue == "Running") 
			   {
			   	  $searchQuery = " and (
					mtConnect.State like '%ACTIVE%' 
					)  ";  
			   }else if ($searchValue == "Waiting") 
			   {
			   	   $searchQuery = " and (
					mtConnect.State like '%READY%' OR mtConnect.State like '%INTERRUPTED%'
					) ";   
			   }else if ($searchValue == "Stopped") 
			   {
			   	   $searchQuery = " and (
					mtConnect.State like '%STOPPED%' 
					) ";  
			   }else if ($searchValue == "ignore" || $searchValue == "Ignore") 
			   {
			   	   $searchQuery = " ";  
			   }
			   else
			   {
			   	$searchQuery = " and (
					mtConnect.machineId like '%".$searchValue."%' or 
					mtConnect.State like '%".$searchValue."%' or 
					mtConnect.originalTime like '%".$searchValue."%' or
					machine.machineName like '%".$searchValue."%' 
					) ";  
			   }
			   
			}
			$listColorLogs = $this->AM->getAllMtconnectLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)->result();
	    	$data = array(); 

			for ($key=0;$key<count($listColorLogs);$key++) 
			{	

				if($listColorLogs[$key]->State == "ACTIVE")
				{
					$listColorLogs[$key]->machineStatus = Running;
				}else if($listColorLogs[$key]->State == "INTERRUPTED" || $listColorLogs[$key]->State == "READY")
				{
					$listColorLogs[$key]->machineStatus = Waiting;
				}else if($listColorLogs[$key]->State == "STOPPED")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
				}else
				{
					
					if ($listColorLogs[$key]->State == "0") 
					{
						$listColorLogs[$key]->machineStatus = NoData;
						$listColorLogs[$key]->State = NoData;
					}
					else if ($listColorLogs[$key]->State == "2") 
					{
						$listColorLogs[$key]->machineStatus = Noproduction;
						$listColorLogs[$key]->State = NoData;
					}else if ($listColorLogs[$key]->State == "1") 
					{
						$listColorLogs[$key]->machineStatus = Setup;
						$listColorLogs[$key]->State = NoData;
					}else{
						$listColorLogs[$key]->machineStatus = NoData;
					}
				}
				$listColorLogs[$key]->logId = $key + 1;
				$listColorLogs[$key]->color = $listColorLogs[$key]->State;

				$data[] = array(
					"logId" => $listColorLogs[$key]->logId,
					"machineId" => $listColorLogs[$key]->machineId,
					"machineName" => $listColorLogs[$key]->machineName,
					"color" => $listColorLogs[$key]->color,
					"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? Invalid : $listColorLogs[$key]->machineStatus,
					"originalTime" => $listColorLogs[$key]->originalTime
				);
			}
		}else
		{
		    if ($searchValue == "No production") 
		    {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		    }else if ($searchValue == "production") 
		    {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		    }else if ($searchValue == "setup") 
		    {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		    }else if ($searchValue == "Ignore" || $searchValue == "ignore") 
		    {
		   	   $searchQuery = "  ";  
		    }else
		    {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		    }
	    	$listColorLogs = $this->AM->getallBetaGraphLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)->result();
	    	$data = array();

	    	for ($key=0;$key<count($listColorLogs);$key++) 
			{

				
				$listColorLogs[$key]->machineStatus = "Production";
				$colorS = explode(" ", $listColorLogs[$key]->color);
				$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
				$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
				$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
				$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
				$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
				$offStatus = (in_array("off", $colorS)) ? "1" : "0";
				$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";
				$NoDataStacklight = (in_array("NoDataStacklight", $colorS)) ? "1" : "0";
				$NoDataError = (in_array("NoDataError", $colorS)) ? "1" : "0";
				$NoDataForceFully = (in_array("NoDataForceFully", $colorS)) ? "1" : "0";
				$NoInternet = (in_array("NoInternet", $colorS)) ? "1" : "0";
				$NoDataHome = (in_array("NoDataHome", $colorS)) ? "1" : "0";
				$NoDataRestart = (in_array("NoDataRestart", $colorS)) ? "1" : "0";


				$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

				if (!empty($machineStateColorLookup)) 
				{
					
					if ($NoDataStatus == "1" || $NoDataStacklight == "1" || $NoDataError == "1" || $NoDataForceFully == "1" || $NoInternet == "1" || $NoDataHome == "1" || $NoDataRestart == "1") 
					{
						$listColorLogs[$key]->machineStatus = "No detection";
					}else if($offStatus == "1")
					{
						$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
					}else
					{
						$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
					}
				}

				if ($listColorLogs[$key]->color == "0") 
				{
					$listColorLogs[$key]->machineStatus = "NoData";
					$listColorLogs[$key]->color = "NoData";
				}
				else if ($listColorLogs[$key]->color == "2") 
				{
					$listColorLogs[$key]->machineStatus = "No production";
					$listColorLogs[$key]->color = "NoData";
				}else if ($listColorLogs[$key]->color == "1") 
				{
					$listColorLogs[$key]->machineStatus = "Setup";
					$listColorLogs[$key]->color = "NoData";
				}

				if ($listColorLogs[$key]->color == "NoData" || $listColorLogs[$key]->color == "NoInternet" || $listColorLogs[$key]->color == "NoDataStacklight" || $listColorLogs[$key]->color == "NoDataHome" || $listColorLogs[$key]->color == "NoDataForceFully" || $listColorLogs[$key]->color == "NoDataError" || $listColorLogs[$key]->color == "NoDataRestart") 
				{
					$seconds = strtotime($listColorLogs[$key]->originalTime) - strtotime($listColorLogs[$key + 1]->originalTime);
					if ($seconds <= 30) 
					{
						$listColorLogs[$key]->machineStatus  = 'Ignore';
						$removeArrayKey[] = $key;
					}else
					{
						$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
					}
				}else
				{
					$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
				}

				if ($listColorLogs[$key]->machineStatus == "Invalid") 
				{
					$listColorLogs[$key]->machineStatus = Invalid;
				}else if($listColorLogs[$key]->machineStatus == "Running")
				{
					$listColorLogs[$key]->machineStatus = Running;
				}else if($listColorLogs[$key]->machineStatus == "Waiting")
				{
					$listColorLogs[$key]->machineStatus = Waiting;
				}else if($listColorLogs[$key]->machineStatus == "Stopped")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
				}else if($listColorLogs[$key]->machineStatus == "Off")
				{
					$listColorLogs[$key]->machineStatus = Off;
				}else if($listColorLogs[$key]->machineStatus == "No production")
				{
					$listColorLogs[$key]->machineStatus = Noproduction;
				}else if($listColorLogs[$key]->machineStatus == "NoData")
				{
					$listColorLogs[$key]->machineStatus = NoData;
				}else if($listColorLogs[$key]->machineStatus == "Setup")
				{
					$listColorLogs[$key]->machineStatus = setup;
				}


				if ($listColorLogs[$key]->color == "NoData") 
				{
					$listColorLogs[$key]->color = NoData;
				}else
				{
					$listColorLogs[$key]->color = displayColorNameBetaTable($listColorLogs[$key]->color);
				}

				$data[] = array(
					"logId" => $listColorLogs[$key]->logId,
					"machineId" => $listColorLogs[$key]->machineId,
					"machineName" => $listColorLogs[$key]->machineName,
					"color" => $listColorLogs[$key]->color,
					"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? Invalid : $listColorLogs[$key]->machineStatus,
					"originalTime" => $listColorLogs[$key]->originalTime
				);
			}

			$data = $this->array_except($data, $removeArrayKey);
			$data = array_values($data);
		}
		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Beta detection".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
		$handle = fopen('php://output', 'w');
        fputcsv($handle, array(Betadetectionlog));
        $cnt=1;
        foreach ($data as $key) 
        {
        	if ($cnt == 1) 
        	{
        		$narray=array(LogId,MachineId,MachineName,Color,State,Time);
            	fputcsv($handle, $narray);
        	}
            $narray=array($key['logId'],$key['machineId'],$key["machineName"],$key["color"],$key["machineStatus"],$key["originalTime"]);
            fputcsv($handle, $narray);
            $cnt++;
        }
        fclose($handle);
        exit;
	}
	

	//In use
	public function beta_detection_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		$rr = $row + 1;
		$searchQuery = " ";
		$result = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");

		if ($result->isDataGet == "4") 
		{

			if($searchValue != '')
			{
			   if ($searchValue == "Running" || $searchValue == "Active") 
			   {
			   	  $searchQuery = " and (
					virtualMachineLog.signalType like '%1%' 
					)  ";  
			   }else if ($searchValue == "Stopped" || $searchValue == "Not active") 
			   {
			   	   $searchQuery = " and (
					virtualMachineLog.signalType like '%0%' 
					) ";  
			   }
			   else
			   {
			   	$searchQuery = " and (
					virtualMachineLog.machineId like '%".$searchValue."%' or 
					virtualMachineLog.originalTime like '%".$searchValue."%' or
					machine.machineName like '%".$searchValue."%' 
					) ";  
			   }
			   
			}
			$listColorLogsCount = $this->AM->getAllVirtualMachineBLogsCount($this->factory, $machineId, $is_admin)->row()->totalCount;
			$listColorLogsSearchCount = $this->AM->getSearchVirtualMachineBLogsCount($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
			$listColorLogs = $this->AM->getallVirtualMachineBLogs($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

			for ($key=0;$key<count($listColorLogs);$key++) 
			{	
				$listColorLogs[$key]->logId = $rr;
				if($listColorLogs[$key]->signalType == "1" && $listColorLogs[$key]->signalTypeStatus == "vMachine")
				{
					$listColorLogs[$key]->machineStatus = Running;
					$listColorLogs[$key]->color = Active;
				}else if($listColorLogs[$key]->signalType == "0" && $listColorLogs[$key]->signalTypeStatus == "vMachine")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
					$listColorLogs[$key]->color = Notactive;
				}else
				{
					if ($listColorLogs[$key]->signalType == "0") 
					{
						$listColorLogs[$key]->machineStatus = NoData;
						$listColorLogs[$key]->color = NoData;
					}
					else if ($listColorLogs[$key]->signalType == "2") 
					{
						$listColorLogs[$key]->machineStatus = Noproduction;
						$listColorLogs[$key]->color = NoData;
					}else if ($listColorLogs[$key]->signalType == "1") 
					{
						$listColorLogs[$key]->machineStatus = Setup;
						$listColorLogs[$key]->color = NoData;
					}else{
						$listColorLogs[$key]->machineStatus = NoData;
					}
				}
				$rr++;
			}
		}
		else if ($result->isDataGet == "2") 
		{

			if($searchValue != '')
			{
			   if ($searchValue == "Running") 
			   {
			   	  $searchQuery = " and (
					mtConnect.State like '%ACTIVE%' 
					)  ";  
			   }else if ($searchValue == "Waiting") 
			   {
			   	   $searchQuery = " and (
					mtConnect.State like '%READY%' OR mtConnect.State like '%INTERRUPTED%'
					) ";   
			   }else if ($searchValue == "Stopped") 
			   {
			   	   $searchQuery = " and (
					mtConnect.State like '%STOPPED%' 
					) ";  
			   }else if ($searchValue == "ignore" || $searchValue == "Ignore") 
			   {
			   	   $searchQuery = " ";  
			   }
			   else
			   {
			   	$searchQuery = " and (
					mtConnect.machineId like '%".$searchValue."%' or 
					mtConnect.State like '%".$searchValue."%' or 
					mtConnect.originalTime like '%".$searchValue."%' or
					machine.machineName like '%".$searchValue."%' 
					) ";  
			   }
			   
			}
			$listColorLogsCount = $this->AM->getAllMtConnectLogsCount($this->factory, $machineId, $is_admin)->row()->totalCount;
			$listColorLogsSearchCount = $this->AM->getSearchMtConnectLogsCount($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
			$listColorLogs = $this->AM->getallMtConnectLogs($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

			for ($key=0;$key<count($listColorLogs);$key++) 
			{	
				$listColorLogs[$key]->logId = $rr;
				if($listColorLogs[$key]->State == "ACTIVE")
				{
					$listColorLogs[$key]->machineStatus = Running;
				}else if($listColorLogs[$key]->State == "INTERRUPTED" || $listColorLogs[$key]->State == "READY")
				{
					$listColorLogs[$key]->machineStatus = Waiting;
				}else if($listColorLogs[$key]->State == "STOPPED")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
				}else
				{
					if ($listColorLogs[$key]->State == "0") 
					{
						$listColorLogs[$key]->machineStatus = NoData;
						$listColorLogs[$key]->State = NoData;
					}
					else if ($listColorLogs[$key]->State == "2") 
					{
						$listColorLogs[$key]->machineStatus = Noproduction;
						$listColorLogs[$key]->State = NoData;
					}else if ($listColorLogs[$key]->State == "1") 
					{
						$listColorLogs[$key]->machineStatus = Setup;
						$listColorLogs[$key]->State = NoData;
					}else{
						$listColorLogs[$key]->machineStatus = NoData;
					}
				}
				//$listColorLogs[$key]->logId = $listColorLogs[$key]->mtConnectId;
				$listColorLogs[$key]->color = $listColorLogs[$key]->State;
				$rr++;
			}
		}else
		{

			if($searchValue != '')
			{

			   if ($searchValue == "No production") 
			   {
			   	  $searchQuery = " and (
					betaTable.color like '%2%' 
					) ";  
			   }else if ($searchValue == "production") 
			   {
			   	   $searchQuery = " and (
					betaTable.color like '%0%' 
					) ";   
			   }else if ($searchValue == "setup") 
			   {
			   	   $searchQuery = " and (
					betaTable.color like '%1%' 
					) ";  
			   }else if ($searchValue == "ignore" || $searchValue == "Ignore") 
			   {
			   	   $searchQuery = " ";  
			   }
			   else
			   {
			   	$searchQuery = " and (
					betaTable.machineId like '%".$searchValue."%' or 
					betaTable.color like '%".$searchValue."%' or 
					betaTable.originalTime like '%".$searchValue."%' or
					machine.machineName like '%".$searchValue."%' 
					) ";  
			   }
			   
			}
			
			$listColorLogsCount = $this->AM->getallBeta_GraphLogsCount($this->factory, $machineId, $is_admin)->row()->totalCount;
			$listColorLogsSearchCount = $this->AM->getSearchBeta_GraphLogsCount($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
			$listColorLogs = $this->AM->getallBeta_GraphsLogs($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

			$removeArrayKey = array();

			for ($key=0;$key<count($listColorLogs);$key++) 
			{
				

				$listColorLogs[$key]->machineStatus = "Production";
				$colorS = explode(" ", $listColorLogs[$key]->color);
				$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
				$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
				$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
				$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
				$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
				$offStatus = (in_array("off", $colorS)) ? "1" : "0";
				$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";
				$NoDataStacklight = (in_array("NoDataStacklight", $colorS)) ? "1" : "0";
				$NoDataError = (in_array("NoDataError", $colorS)) ? "1" : "0";
				$NoDataForceFully = (in_array("NoDataForceFully", $colorS)) ? "1" : "0";
				$NoInternet = (in_array("NoInternet", $colorS)) ? "1" : "0";
				$NoDataHome = (in_array("NoDataHome", $colorS)) ? "1" : "0";
				$NoDataRestart = (in_array("NoDataRestart", $colorS)) ? "1" : "0";


				$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

				if (!empty($machineStateColorLookup)) 
				{
					
					if ($NoDataStatus == "1" || $NoDataStacklight == "1" || $NoDataError == "1" || $NoDataForceFully == "1" || $NoInternet == "1" || $NoDataHome == "1" || $NoDataRestart == "1") 
					{
						$listColorLogs[$key]->machineStatus = "nodet";
					}else if($offStatus == "1")
					{
						$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
					}else
					{
						$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;

					}
				}

				if ($listColorLogs[$key]->color == "0") 
				{
					$listColorLogs[$key]->machineStatus = "NoData";
					$listColorLogs[$key]->color = "NoData";
				}
				else if ($listColorLogs[$key]->color == "2") 
				{
					$listColorLogs[$key]->machineStatus = "No production";
					$listColorLogs[$key]->color = "NoData";
				}else if ($listColorLogs[$key]->color == "1") 
				{
					$listColorLogs[$key]->machineStatus = "Setup";
					$listColorLogs[$key]->color = "NoData";
				}

				if ($listColorLogs[$key]->color == "NoData" || $listColorLogs[$key]->color == "NoInternet" || $listColorLogs[$key]->color == "NoDataStacklight" || $listColorLogs[$key]->color == "NoDataHome" || $listColorLogs[$key]->color == "NoDataForceFully" || $listColorLogs[$key]->color == "NoDataError" || $listColorLogs[$key]->color == "NoDataRestart") 
				{
					$seconds = strtotime($listColorLogs[$key]->originalTime) - strtotime($listColorLogs[$key + 1]->originalTime);
					if ($seconds <= 30) 
					{
						$removeArrayKey[] = $key;
						$listColorLogs[$key]->machineStatus  = 'Ignore';
					}else
					{
						$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
						$listColorLogs[$key]->logId = $rr;
						$rr++;
					}
					$listColorLogs[$key]->color = "NoData";
				}else
				{
					$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
					$listColorLogs[$key]->logId = $rr;
					$rr++;
				}

				if ($listColorLogs[$key]->machineStatus == "Invalid") 
				{
					$listColorLogs[$key]->machineStatus = Invalid;
				}else if($listColorLogs[$key]->machineStatus == "Running")
				{
					$listColorLogs[$key]->machineStatus = Running;
				}else if($listColorLogs[$key]->machineStatus == "Waiting")
				{
					$listColorLogs[$key]->machineStatus = Waiting;
				}else if($listColorLogs[$key]->machineStatus == "Stopped")
				{
					$listColorLogs[$key]->machineStatus = Stopped;
				}else if($listColorLogs[$key]->machineStatus == "Off")
				{
					$listColorLogs[$key]->machineStatus = Off;
				}else if($listColorLogs[$key]->machineStatus == "No production")
				{
					$listColorLogs[$key]->machineStatus = Noproduction;
				}else if($listColorLogs[$key]->machineStatus == "NoData")
				{
					$listColorLogs[$key]->machineStatus = NoData;
				}else if($listColorLogs[$key]->machineStatus == "Setup")
				{
					$listColorLogs[$key]->machineStatus = setup;
				}


				if ($listColorLogs[$key]->color == "NoData") 
				{
					$listColorLogs[$key]->color = NoData;
				}else
				{
					$listColorLogs[$key]->color = displayColorNameBetaTable($listColorLogs[$key]->color);
				}

				
			}

			$listColorLogs = $this->array_except($listColorLogs, $removeArrayKey);
			$listColorLogs = array_values($listColorLogs);

			if ($columnIndex == 0) 
			{
				$keys = array_column($listColorLogs, 'logId');
				$orderBy = ($columnSortOrder == "desc") ? SORT_DESC : SORT_ASC;
				array_multisort($keys, $orderBy, $listColorLogs);
			}
		}

		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		//print_r($response);exit;
		echo json_encode($response); die;
	}

	function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 
	
}

?>