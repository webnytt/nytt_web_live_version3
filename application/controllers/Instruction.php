<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Instruction extends CI_Controller {
	
	var $factory;
	var $factoryId;
	
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

		// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}
		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);
	}

	//taskMaintenance function use for get task & maintence page 

	//In use
	function index()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$data['userRole'] = $this->session->userdata('role');
		
		$data['instructionDashbaord'] = $this->AM->getWhereDb(array("instruction_type" => "1","isDelete" => "0"),"instruction");
		$data['instructionOpaap'] = $this->AM->getWhereDb(array("instruction_type" => "2","isDelete" => "0"),"instruction");
		$data['instructionSetApp'] = $this->AM->getWhereDb(array("instruction_type" => "3","isDelete" => "0"),"instruction");
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('add_instruction',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/instruction_footer');		
	}

	//In use
	function searchInstruction()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$search_keyword = $this->input->post('search_keyword');
		$instruction_type = $this->input->post('instruction_type');
		
		$select = "instruction.*";
		$instruction = $this->AM->getWhereLikeWithLeftJoin($select,array("instruction_type" => $instruction_type,"isDelete" => "0"),array("search_keyword" => $search_keyword,"main_title" => $search_keyword,"subTitle" => $search_keyword),array("subInstruction" => "instruction.instructionId = subInstruction.instructionId"),"instruction");	
		//echo "<pre>";print_r($instruction);exit;
		//echo $this->db->last_query();exit;
		$instruction = array_unique($instruction);
		$html = "";

		$userRole = $this->session->userdata('role');
		foreach ($instruction as $key => $value) 
		{
			//echo "exit;";exit;
			$html .= '<div class = "col-md-12 m-t-30">';
        	$subInstruction = $this->AM->getWhereDB(array("instructionId" => $value['instructionId']),"subInstruction");
            $html .= '<div class = "col-md-12" >
              <div style="font-size: 18px;font-weight: 600;">'.$value['main_title'].'</div> ';
              if($userRole == "2"){
              $html .=  '<img onclick="editInstruction('.$value['instructionId'].')"  style="float:right;width: 26px;margin-top: -8px;cursor:pointer;" src="'. base_url('assets/nav_bar/create.svg') .'">  
                <img onclick="deleteInstruction('.$value['instructionId'].')"  style="float:right;width: 26px;margin-top: -8px;cursor:pointer;" src="'.base_url('assets/nav_bar/delete.svg').'">';
            }
             $html .=' </div>';

            	if(!empty($value['main_title_gif'])){ 
            	$html .=  '<br> 
  				      <img style="max-width:90%;box-shadow: grey 0px 0px 8px -2px !important;display: block;margin:0 auto;border-radius:20px;" src="'.base_url('assets/instruction/').$value['main_title_gif'].'"> ';
            	}

          		 if(is_array($subInstruction) && count($subInstruction) > 0 ) { 
                $html .= '<div class="row m-t-15">';
                  foreach ($subInstruction as $keySub => $valueSub) { 
                  $html .= ' <div class="col-md-6 m-t-30">
                      <div style="font-size:14px;min-height: 60px;">
                          '.$valueSub['subTitle'].' &nbsp; ';
                          if($userRole == "2"){
                        $html .=  '<img onclick="editSubInstruction('.$valueSub['subInstructionId'].')"  style="width: 20px;cursor: pointer;float: right;" src="'.base_url('assets/nav_bar/create.svg').'"> ';
                        } 
                      $html .= '</div>
                      <br> 
                      <img style="width:90%;box-shadow: grey 0px 0px 8px -2px !important;border-radius:20px;" src="'.base_url('assets/instruction/').$valueSub['subTitleGif'].'">
                  </div>';
            			 } 
             		$html .= '</div>';  
  			     } 
      		  $html .= '</div>';
		}

		echo $html;
	}

	//In use
	public function add_save_instruction() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('instruction_type','instruction_type','required');
		$this->form_validation->set_rules('main_title','main_title','required');
		$this->form_validation->set_rules('search_keyword','search_keyword','required');


		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            echo "Please enter required filed";exit;
		} 
        else 
        { 
        	$instruction_type = $this->input->post('instruction_type'); 
        	$main_title = $this->input->post('main_title'); 
        	$search_keyword = $this->input->post('search_keyword'); 
        	$search_position_main = $this->input->post('search_position_main'); 
        	$sub_title = $this->input->post('sub_title'); 
        	$search_position = $this->input->post('search_position'); 
   			
            $config['upload_path'] = './assets/instruction/';
            $config['allowed_types'] = 'gif|GIF|png|jpg|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('main_title_gif')) 
            {
            	$main_title_gif = "";
            }
            else 
            {
            	$main_title_gif = $this->upload->data('file_name');
            }
		
			$insert=array(
                'instruction_type' => $instruction_type,
                'main_title' => $main_title,
                'search_keyword' => $search_keyword,
                'search_position' => $search_position_main,
                'main_title_gif'=> $main_title_gif
            );

           $instructionId = $this->AM->insertDBData($insert, 'instruction');
           //echo $this->factory->last_query();exit;

           if (!empty($instructionId)) 
           {
           	

		    $files = $_FILES;
		    $cpt = count($_FILES['sub_title_gif']['name']);
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['subTitleGif']['name']= $files['sub_title_gif']['name'][$i];
		        $_FILES['subTitleGif']['type']= $files['sub_title_gif']['type'][$i];
		        $_FILES['subTitleGif']['tmp_name']= $files['sub_title_gif']['tmp_name'][$i];
		        $_FILES['subTitleGif']['error']= $files['sub_title_gif']['error'][$i];
		        $_FILES['subTitleGif']['size']= $files['sub_title_gif']['size'][$i];    

		        $this->upload->do_upload('subTitleGif');

		        $insertSub=array(
	                'instructionId' => $instructionId,
	                'subTitle' => $sub_title[$i],
	                'searchPosition' => $search_position[$i],
	                'subTitleGif'=> $this->upload->data('file_name')
	            );


	            $this->AM->insertDBData($insertSub, 'subInstruction');

		    }


			echo "1"; 
           }else
           {
           	echo "Data not save";
           }
			die;
			// }
        }
	}

	//In use
	public function edit_save_instruction() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('edit_main_title','main_title','required');
		$this->form_validation->set_rules('edit_search_keyword','search_keyword','required');


		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            echo "Please enter required filed";exit;
		} 
        else 
        { 
        	$instructionId = $this->input->post('instructionId'); 
        	$main_title = $this->input->post('edit_main_title'); 
        	$search_keyword = $this->input->post('edit_search_keyword'); 
        	$search_position_main = $this->input->post('edit_search_position_main'); 

        	$sub_title = $this->input->post('sub_title'); 
        	$search_position = $this->input->post('search_position'); 
   			
            $config['upload_path'] = './assets/instruction/';
            $config['allowed_types'] = 'gif|GIF|png|jpg|jpeg';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('edit_main_title_gif')) 
            {
            	$main_title_gif = $this->input->post('old_edit_main_title_gif');
            }
            else 
            {
            	$main_title_gif = $this->upload->data('file_name');
            }
		
			$insert=array(
                'main_title' => $main_title,
                'search_keyword' => $search_keyword,
                'search_position' => $search_position_main,
                'main_title_gif'=> $main_title_gif
            );

             $this->AM->updateDBData(array("instructionId" => $instructionId),$insert, 'instruction');
         	


		    $files = $_FILES;
		    $cpt = count($_FILES['sub_title_gif']['name']);
		    for($i=0; $i<$cpt; $i++)
		    {           
		        $_FILES['subTitleGif']['name']= $files['sub_title_gif']['name'][$i];
		        $_FILES['subTitleGif']['type']= $files['sub_title_gif']['type'][$i];
		        $_FILES['subTitleGif']['tmp_name']= $files['sub_title_gif']['tmp_name'][$i];
		        $_FILES['subTitleGif']['error']= $files['sub_title_gif']['error'][$i];
		        $_FILES['subTitleGif']['size']= $files['sub_title_gif']['size'][$i];    

		        $this->upload->do_upload('subTitleGif');

		        $insertSub=array(
	                'instructionId' => $instructionId,
	                'subTitle' => $sub_title[$i],
	                'searchPosition' => $search_position[$i],
	                'subTitleGif'=> $this->upload->data('file_name')
	            );


	            $this->AM->insertDBData($insertSub, 'subInstruction');
	         }


			echo "1"; 
          
			die;
			// }
        }
	}


	
	//In use
	function deleteInstruction()
	{
		$instructionId = $this->input->post('instructionId');

		$this->AM->updateDBData(array("instructionId" => $instructionId),array("isDelete" => "1"),"instruction");

	}

	//In use
	function getSubInstruction()
	{
		$subInstructionId = $this->input->post('subInstructionId');
		$result = $this->AM->getWhereDBSingle(array("subInstructionId" => $subInstructionId),"subInstruction");
		echo json_encode($result);
	}

	//In use
	function getInstruction()
	{
		$instructionId = $this->input->post('instructionId');
		$result = $this->AM->getWhereDBSingle(array("instructionId" => $instructionId),"instruction");
		echo json_encode($result);
	}

	//In use
	public function edit_sub_save_instruction() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('subInstructionId','subInstructionId','required');
		$this->form_validation->set_rules('edit_sub_title','edit_sub_title','required');
		$this->form_validation->set_rules('edit_sub_search_position','edit_sub_search_position','required');


		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            echo "Please enter required filed";exit;
		} 
        else 
        { 
        	$subInstructionId = $this->input->post('subInstructionId'); 
        	$edit_sub_title = $this->input->post('edit_sub_title'); 
        	$edit_sub_search_position = $this->input->post('edit_sub_search_position'); 


            $config['upload_path'] = './assets/instruction/';
            $config['allowed_types'] = 'gif|GIF';
            $config['encrypt_name'] = TRUE;

            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            
            if (!$this->upload->do_upload('edit_sub_title_gif')) 
            {
            	$edit_sub_title_gif = $this->input->post('old_edit_sub_title_gif');
            }
            else 
            {
            	$edit_sub_title_gif = $this->upload->data('file_name');
            }
		
			$insert=array(
                'subTitle' => $edit_sub_title,
                'searchPosition' => $edit_sub_search_position,
                'subTitleGif'=> $edit_sub_title_gif
            );

            $this->AM->updateDBData(array("subInstructionId" => $subInstructionId),$insert, 'subInstruction');

			echo "1"; 
         
			die;
        }
	}
}
?>