<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Task_testing extends CI_Controller {
	
	var $factory;
	var $factoryId;
	
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Task_testing_model','AM'); 
		if($this->AM->checkIsvalidated() == true) {
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 
			$this->load->database();

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}
	}

	//taskMaintenance function use for get task & maintence page 
	function taskMaintenance()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('taskandmaintenance_testing',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/taskandmain_footer');		
	}

	public function task_maintenace_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$isDelete = $this->input->post('isDelete');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$listDataLogsCount = $this->AM->getallTaskMaintenaceCount()->row()->totalCount;
		$listDataLogsSearchCount = $this->AM->getSearchTaskMaintenaceCount($isDelete, $type, $status, $dateValS, $dateValE)->row()->totalCount;
		$listDataLogs = $this->AM->getallTaskMaintenaceLogs($row, $rowperpage, $isDelete, $type, $status, $dateValS, $dateValE)->result_array();  
		
		foreach ($listDataLogs as $key => $value) 
		{

			if ($value['type'] == "1") 
			{
				$listDataLogs[$key]['type'] = "Active";
			}
			else if($value['type'] == "0") 
			{
				$listDataLogs[$key]['type'] = "In-Active";
			}
			

			if ($value['id_deleted'] == "1") 
			{
				$listDataLogsCounttaLogs[$key]['id_deleted'] = "Deleted record";
			}
			else if($value['id_deleted'] == "0") 
			{
				$listDataLogs[$key]['id_deleted'] = "Existing record";
			}
				

			if ($value['status']== "1") 
			{
				$listDataLogs[$key]['status'] = "completed";
			}
			else if($value['status'] == "0") 
			{
				$listDataLogs[$key]['status'] = "Uncompleted";
			}	

		}

		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	public function add_task() 
	{	
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('type','type','required');
        $this->form_validation->set_rules('task_name','task_name','required');
        $this->form_validation->set_rules('task_description','task_description','required');
        if($this->form_validation->run()==false) 
        { 
            $error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"message" =>  $error[0]);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$type = $this->input->post('type');
        	$task_name = $this->input->post('task_name');
			$task_description =  $this->input->post('task_description');
			$task_date = $this->input->post('task_date');
			$created_date = date('Y-m-d', strtotime(str_replace('-', '/', $this->input->post('created_date')))); 
			
			$data=array(  
		                'type' => $type,  
		                'task_name' => $task_name,  
		                'task_description' => $task_description,
		               	'created_date' => $created_date
			            );  

	           $this->AM->insertData($data, "task_testing");
	        $json = array("status" => 1,"message" =>  "Task added");
            echo json_encode($json); die;
		}
		
	}
	function getTaskById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$task_id = 	$this->input->post('task_id');
		$result = $this->AM->getTaskById($this->db,$task_id);
		$task_name = $this->input->post('task_name');
		$task_description = $this->input->post('task_description');
		$created_date = $this->input->post('created_date');
		echo json_encode($result); die;
	}

	public function configure_versions()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$listStackLightTypes = $this->AM->getallStackLightType();
		$data = array(
		    			'listStackLightTypes'=>$listStackLightTypes
		   			 );
		$data['stackLightType'] = $this->AM->getWhereDB(array("isDeleted" => "0"),"stackLightType");
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$data['listStackLightTypes'] = $listStackLightTypes;
		$this->load->view('configure_version_new',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/configure_version_new_footer');		
	}


	public function configure_version_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$versionId = $this->input->post('versionId');
		$stackLightTypeName = $this->input->post('stackLightTypeName');
		$stackLightTypeId = $this->input->post('stackLightTypeId');
		$versionName = $this->input->post('versionName');
		$fileName = $this->input->post('fileName');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$listDataLogsCount = $this->AM->getallStacklighttypeVersionsCount($this->factory, $stackLightTypeId, $is_admin)->row()->totalCount;
		
		$listDataLogsSearchCount = $this->AM->getSearchStacklighttypeVersionsCount($versionId, $stackLightTypeName, $stackLightTypeId, 
			$versionName, $fileName, $dateValS, $dateValE)->row()->totalCount;
		
		$listDataLogs = $this->AM->getallStacklighttypeVersionsLogs($row, $rowperpage, $versionId, $stackLightTypeId, $stackLightTypeName, $versionName, $fileName, $dateValS, $dateValE)->result_array();  

		foreach ($listDataLogs as $key => $value) 
		{
			$stackLightTypeId = $value['stackLightTypeId'];
			$stackLightTypeName = $value['stackLightTypeName'];
			$versionName = $value['versionName'];
			$listDataLogs[$key]['fileName'] = 
				"
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/classification.cfg')."'>classification.cfg</a><br>
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/classification.weights')."'>classification.weights</a><br>
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/detection.cfg')."'>detection.cfg</a><br>
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/detection.weights')."'>detection.weights</a><br>
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/label.txt')."'>label.txt</a><br>
					<a download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/labels.txt')."'>labels.txt</a>
				";
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}



	function getconfigureVersionById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$versionId = 	$this->input->post('versionId');
		$result = $this->AM->getconfigureVersionById($this->db,$versionId);
		$versionName = $this->input->post('versionName');
		$fileName = $this->input->post('fileName');
		$versionDate = $this->input->post('versionDate');
		
		echo json_encode($result); die;
	}

	//add_stacklighttype function use for add new stacklight
	public function add_stacklighttype() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		//check required field validation and check stacklighttype name already added or not
		$this->form_validation->set_rules('stackLightTypeName','Name','trim|required|callback_check_stacklighttype_name');
        if($this->form_validation->run()==false) 
        { 
        	//return message if required field null
            if(form_error('stackLightTypeName') )
            { 
                echo form_error('stackLightTypeName'); die; 
            } 
            else 
            {
                echo "<p>Please fill valid details for stack light type.</p>"; die;
            }
        } 
        else 
        { 
			$newId=$this->AM->lastStackLightTypeId()->maxId + 1;
            $nameext = explode(".", $_FILES['stackLightTypeImage']['name']); 
            $ext = $nameext[1];
            $config['upload_path'] =  './assets/img/stackLightType/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size']  = '5000';
            $config['file_name'] = $newId.".".$ext;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('stackLightTypeImage')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            $insert=array(
				'stackLightTypeName'=>$this->input->post('stackLightTypeName'),
				'stackLightTypeImage'=>$config['file_name'],
				'stackLightTypeVersion' => 1
			);
			
			$stackLightTypeId=$this->AM->newStackLightType($insert);
			$stacklighttypefiledata = array(
				"stackLightTypeId" => $stackLightTypeId,
				"versionName" => 1
			);
			$this->db->insert("stackLightTypeFile",$stacklighttypefiledata);
			$stacklightFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId;
			if (!file_exists($stacklightFilePath)) 
			{
			   mkdir($stacklightFilePath, 0777, true);
			}
			$versionFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId.'/1';
			if (!file_exists($versionFilePath)) 
			{
			   mkdir($versionFilePath, 0777, true);
			}
			$config1['upload_path'] = $versionFilePath;
            $config1['allowed_types'] = '*';

            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);

            $this->upload->do_upload('classification_cfg');
            $this->upload->do_upload('classification_weights');
            $this->upload->do_upload('detection_cfg');
            $this->upload->do_upload('detection_weights');
            $this->upload->do_upload('label_txt');
            $this->upload->do_upload('labels_txt');

			echo "New stacklighttype added successfully."; die;
		}
	}

	//app_versions function use for app versions list page
	public function app_versions() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$listappVersions = $this->AM->getallappVersions();
		
		$data = array(
		    			'listappVersions'=>$listappVersions
		   			 );
		$data['appVersions'] = $this->AM->getWhereDB(array("isDeleted" => "0"),"appVersions");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$data['listappVersions'] = $listappVersions;
		$this->load->view('app_version_new',$data);
		$this->load->view('footer');
		$this->load->view('script_file/app_version_new_footer');
	}


	//app_versions_pagination function use for get all added app version update details
	public function app_versions_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$app_version_id = $this->input->post('app_version_id');
		$app_version_file = $this->input->post('app_version_file');
		$app_version_name = $this->input->post('app_version_name');
		$app_version_code = $this->input->post('app_version_code');
		$update_file = $this->input->post('update_file');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$listDataLogsCount = $this->AM->getAllAppVersionCount($db)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getAppVersionCount($app_version_id, $app_version_file,$app_version_name,$app_version_code,$update_file,$dateValS,$dateValE)->row()->totalCount;

		$listDataLogs = $this->AM->getallAppVersion($row, $rowperpage, $app_version_id, $app_version_file, $app_version_name, $app_version_code, $update_file, $dateValS, $dateValE)->result_array(); 


		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['app_version_file'] = "<a download href='".base_url('../app_versions/').$value['app_version_file']."'>". $value['app_version_file'] ."</a>";

		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	function getappVersionById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$app_version_id = $this->input->post('app_version_id');
		$result = $this->AM->getappVersionById($this->db ,$app_version_id);
		$app_version_id = $this->input->post('app_version_id');
		$app_version_name = $this->input->post('app_version_name');
		$app_version_file = $this->input->post('app_version_file');
		$app_version_code = $this->input->post('app_version_code');
		$created_date = $this->input->post('created_date');
		echo json_encode($result); die;
	}

	//add_appVersion function use for add new app version
	public function add_appVersion() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$this->form_validation->set_rules('versionName','versionName','required');
		$this->form_validation->set_rules('versionCode','versionCode','required');
		
		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo "<p>Please fill valid details for App version type.</p>"; die;
        } 
        else 
        { 
        	$upload_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES;
            $config['upload_path'] =  $upload_path;
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            //Check file type and file uploaded or not
            if (!$this->upload->do_upload('versionFile')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            else
            {
				$app_versions = array(
					"app_version_name" => $this->input->post('versionName'),
					"app_version_code" => $this->input->post('versionCode'),
					"app_version_file" => $this->upload->data('file_name'),
					"app_type" => "setApp"
				);

				$this->db->insert("appVersions",$app_versions);
				
				//Add new app version
            	$upload_file = $upload_path.'/'.$this->upload->data('file_name');
            	chmod($upload_file,0777);
				$result = $this->AM->getWhereDBWithGroupBY(array("userRole" => "0","deviceToken != " => ""),"deviceToken","user");
				//Send notification to all operator for new version
				$fcmToken = array_column($result, "deviceToken");
				$type = "setApp";
				$message = $type."! New update available! Install now.";
				$title = "Update app";
				$data = array
		            (
		                'message'   => $message,
		                'title'     => $title,
		                'vibrate'   => 1,
		                'sound'     => 1
		            );
		        $fields = array
		            (
		                'registration_ids'  => $fcmToken,  
		                'data' => $data
		            );

		        $headers = array
		            (
		                'Authorization: key=' . API_ACCESS_KEY,
		                'Content-Type: application/json'
		            );

		        $ch = curl_init();
		        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		        curl_setopt( $ch,CURLOPT_POST, true );
		        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
		        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
		        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		        $result = curl_exec($ch);
		        curl_close($ch);

				echo "New version of Setapp added successfully."; die;
            }
		}
	}

	//update_appVersion function use for update version file
	public function update_appVersion() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$this->form_validation->set_rules('app_version_id','app_version_id','required');
        //checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
			echo "<p>Please fill valid details for App Version.</p>"; die;
        } 
        else 
        { 
			$app_version_id = $this->input->post('app_version_id');
			$result = $this->AM->getWhereDBSingle(array("app_version_id" => $app_version_id),"appVersions");
			$unlink_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES.'/'.$result->app_version_file;
			//Delete previous file 
        	unlink($unlink_path);
			$upload_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES;
            $config['upload_path'] =  $upload_path;
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('versionFile')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            else
            {
            	$app_versions = array(
					"app_version_file" => $this->upload->data('file_name')
				);
				$this->db->where('app_version_id',$app_version_id)->update("appVersions",$app_versions);
		    	$upload_file = $upload_path.'/'.$this->upload->data('file_name');
				chmod($upload_file,0777);
            	echo "Version file updated successfully."; die;
            }
		}
	}


	public function machineStatusLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('machineStatusLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/machineStatusLog_footer');		
	}

	public function machineStatusLog_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$logId = $this->input->post('logId');
		$machineName = $this->input->post('machineName');
		$machineStatus = $this->input->post('machineStatus');
		$insertTime = $this->input->post('insertTime');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$listDataLogsCount = $this->AM->getAllmachineStatusLogCount($factory, $is_admin)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->machineStatusLog_pagination($start, $limit, $logId, $machineId, 
			$machineStatus, $insertTime, $dateValS, $dateValE,$machineName)->row()->totalCount;

		$listDataLogs = $this->AM->getallmachineStatusLog($row, $rowperpage, $logId, $machineId, $machineStatus, 
			$insertTime, $dateValS, $dateValE,$machineName)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if ($value['machineStatus'] == "0") 
			{
				$listDataLogs[$key]['machineStatus'] = "Production time";
			}
			else if($value['machineStatus'] == "1") 
			{
				$listDataLogs[$key]['machineStatus'] = "Setup time";
			}
			else if($value['machineStatus'] == "2") 
			{
				$listDataLogs[$key]['machineStatus'] = "Noproduction time";
			}
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	public function beta_detection() 
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);

		$data['machines'] = $machines;
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('beta_detection_new', $data);
		$this->load->view('footer');
		$this->load->view('script_file/beta_detection_new_footer');
	}
	
	public function beta_detection_pagination2() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$color = $this->input->post('color');
		$machineId = $this->input->post('machineId');
		$orignialTime = $this->input->post('orignialTime');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		
		// echo "<prev>";print_r($this->input->post());exit;
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		
		$listColorLogsCount = $this->AM->getallBetaColorLogsCount($this->factory, $machineId, $dateValS, $dateValE, $is_admin)->row()->totalCount;
		$listColorLogsSearchCount = $this->AM->getSearchBetaColorLogsCount($this->factory, $dateValS, $dateValE, $color, $machineId, $is_admin, $searchQuery)->row()->totalCount;
		$listColorLogs = $this->AM->getallBetaColorLogs($this->factory, $dateValS, $dateValE, $machineId, $color, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		echo json_encode($response); die;
	}

	
	public function phoneMovementDetailLogv2()
	{
		$result['data']=$this->AM->phoneMovementDetailLogv2display_records();
		$this->load->view('phoneMovementDetailLogv2',$result);
		$this->load->view("footer.php");
	}


	public function notificationListLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('notificationListLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/notificationList_log_footer.php');		
	}

	public function notificationListLog_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}



		$notificationId 		= $this->input->post('notificationId');
		$notificationText 		= $this->input->post('notificationText');
		$userId 				= $this->input->post('userId');
		$userName 				= $this->input->post('userName');
		$machineId 				= $this->input->post('machineId');
		$machineName 			= $this->input->post('machineName');
		$addedDate 				= $this->input->post('addedDate');
		$flag 					= $this->input->post('flag');
		$logId 					= $this->input->post('logId');
		$status 				= $this->input->post('status');
		$comment 				= $this->input->post('comment');
		$respondTime 			= $this->input->post('respondTime');
		$responderId 			= $this->input->post('responderId');
		$commentFlag 			= $this->input->post('commentFlag');
		$type 					= $this->input->post('type');

		$dateValS 				= $this->input->post('dateValS');
		$dateValE 				= $this->input->post('dateValE');

		$dueDateValS 			= $this->input->post('dueDateValS');
		$dueDateValE 			= $this->input->post('dueDateValE');

		// echo "<pre>";print_r($this->input->post());exit;
		// print_r($userId);exit;

		$draw 		= $_POST['draw'];
		$row 		= $_POST['start'];
		$rowperpage = $_POST['length'];

		$listDataLogsCount = $this->AM->getAllnotificationListLogCount($factory)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getAllnotificationListLogpagination($start, $limit, $userId, $userName, $machineId, $machineName, $addedDate, $status, $respondTime, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)->row()->totalCount;

		// echo $listDataLogsSearchCount;exit;


		$listDataLogs = $this->AM->getallnotificationLog($row, $rowperpage, $userId, $userName, $machineId ,$machineName ,$addedDate , $status, $respondTime, $responderId, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{

			// $userId = explode(",", $value['userId']);

			// $operators = "";
			// foreach ($userId as $userKey => $userValue) 
			// {
			// 	$users = $this->AM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
			// 	if (!empty($users)) 
			// 	{
			// 		$operators .= '<img style="border-radius: 50%;margin-top: -3px;" width="16" height="16" src="'. base_url('assets/img/user/'.$users->userImage) .'">&nbsp;'. $users->userName."<br>";
			// 	}
			// }

			// $listDataLogs[$key]['operators'] = $operators;

			if ($value['status'] == "0") 
			{
				$listDataLogs[$key]['status'] = "Not Responded";
			}
			else if($value['status'] == "1") 
			{
				$listDataLogs[$key]['status'] = "Responded";
			}


			if ($value['flag'] == "0") 
			{
				$listDataLogs[$key]['flag'] = "Application";
			}
			else if($value['flag'] == "1") 
			{
				$listDataLogs[$key]['flag'] = "Manual";
			}


			if ($value['commentFlag'] == "0") 
			{
				$listDataLogs[$key]['commentFlag'] = "Not Respondable";
			}
			else if($value['commentFlag'] == "1") 
			{
				$listDataLogs[$key]['commentFlag'] = "Respondable";
			}
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		// print_r($response);exit;
		echo json_encode($response); die;
	}

	public function BetaDetection() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
		}
		
		$data = array(
		    'listMachines'=>$listMachines
		    );

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('betaDetectionsNewFile', $data);
		$this->load->view('footer');
		$this->load->view('script_file/betaDetectionNewFile_footer');
	}

	function exportBetaDetectionCsv()
    {

    	$machineId = 0; 
		if($this->input->get('machineId')) 
		{ 
			if($this->input->get('machineId') > 0 ) 
			{
				$machineId = $this->input->get('machineId');
			}
		} 
		if($this->input->get('dateValS')) 
		{ 
			$dateValS = $this->input->get('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->get('dateValE')) 
		{ 
			$dateValE = $this->input->get('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}

		$searchValue = $this->input->get('serchFiled');


		 if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
    	$listColorLogs = $this->AM->getallBetaColorLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)->result();
    	$data = array();

    	for ($key=0;$key<count($listColorLogs);$key++) 
		{

			
			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1") 
				{
					$listColorLogs[$key]->machineStatus = "No detection";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = "Off";
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

			$data[] = array(
				"machineId" => $listColorLogs[$key]->machineId,
				"machineName" => $listColorLogs[$key]->machineName,
				"color" => $listColorLogs[$key]->color,
				"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus,
				"originalTime" => $listColorLogs[$key]->originalTime
			);
		}


		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Beta detection".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
		$handle = fopen('php://output', 'w');
        fputcsv($handle, array("Beta detection log"));
        $cnt=1;
        foreach ($data as $key) 
        {
        	if ($cnt == 1) 
        	{
        		$narray=array("Machine id","Machine name","Color","Machine status","Time");
            	fputcsv($handle, $narray);
        	}
            $narray=array($key['machineId'],$key["machineName"],$key["color"],$key["machineStatus"],$key["originalTime"]);
            fputcsv($handle, $narray);
            $cnt++;
        }
        fclose($handle);
        exit;
	}
	
	public function beta_detection_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		
		$searchQuery = " ";
		if($searchValue != '')
		{

		   if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
		   
		}
		
		$listColorLogsCount = $this->AM->getallBetaColorLogsCount($this->factory, $machineId, $is_admin)->row()->totalCount;
		$listColorLogsSearchCount = $this->AM->getSearchBetaColorLogsCount($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
		$listColorLogs = $this->AM->getallBetaColorLogs($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

		for ($key=0;$key<count($listColorLogs);$key++) 
		{

			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

		/*	if ($listColorLogs[$key]->color == "green") 
			{
				echo $this->factory->last_query();exit;
			}
*/
			

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1") 
				{
					$listColorLogs[$key]->machineStatus = "No detection";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = "Off";
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;

				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

				$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		//print_r($response);exit;
		echo json_encode($response); die;
	}
}
?>