<?php

class Cron extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Cron_model','CM');  
    }

    public function sendCheckoutNotification()
    {
       $factoryCount = $this->db->where('isDeleted','0')->where('type','machine')->get('factory')->result_array();
      
        foreach ($factoryCount as $key1 => $value1) 
        { 
            $factoryId = $value1['factoryId'];

	        $this->factory = $this->load->database('factory'.$factoryId, TRUE);

	        $result = $this->factory->where('isActive','1')->get('machineUserv2')->result_array();
	        $message = "Checkout check";
	        $title = "Checkout check";
	        $dataNot = array
	            (
	                'message'   => $message,
	                'title'     => $title,
	                'type' => "operatorCheckout",
	                'vibrate'   => 1,
	                'sound'     => 1,
	            );


	        foreach ($result as $key => $value) 
	        {

	            $userId = $value['userId'];
	            $activeId = $value['activeId'];
	            $users = $this->db->where("userId",$userId)->get('user')->row();
	            
	            $fields = array
	                (
	                    'registration_ids'  =>  array($users->deviceToken),  
	                    'data' => $dataNot
	                );

	            $headers = array
	                (
	                    'Authorization: key=' . API_ACCESS_KEY,
	                    'Content-Type: application/json'
	                );

	            $ch = curl_init();
	            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	            curl_setopt( $ch,CURLOPT_POST, true );
	            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
	            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	            $notResult = curl_exec($ch);
	            curl_close($ch);
	           
	            $notJson =  json_decode($notResult);
	            
	            if (array_key_exists("error",$notJson->results[0]))
	            {
	                $errorMessage = $notJson->results[0]->error;
	                if ($errorMessage == "NotRegistered") 
	                { 
	                    $endTime = time();
	                    $data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s',$endTime)); 
	                    $this->CM->updateCheckOut($factoryId,$activeId,$data);
	                    
	                    $this->CM->addMNotificationAll($factoryId, $activeId, 1, $endTime); 

	                    if(date('D') == 'Fri') { 
	                        $this->CM->addMNotificationAll($factoryId, $activeId, 2, $endTime); 
	                    }

	                    if(date('d') == date('t')) { 
	                        $this->CM->addMNotificationAll($factoryId, $activeId, 3, $endTime); 
	                    }
	                    
	                    if(date('d') == date('t') && date('m') % 3 == 0) { 
	                        $this->CM->addMNotificationAll($factoryId, $activeId, 4, $endTime); 
	                    }
	                    
	                    if(date('m-d') == '12-31') { 
	                        $this->CM->addMNotificationAll($factoryId, $activeId, 5, $endTime); 
	                    }

	                }
	            }

	            unset($users);
	            unset($notJson);

	        }
	            unset($result);
	        $this->unlockMachineNotificationWithCheckout($factoryId);
	        $this->repeatTask($factoryId);
	    }

    }

    public function unlockMachineNotificationWithCheckout($factoryId)
    {
        $this->factory = $this->load->database('factory'.$factoryId, TRUE);

        $result = $this->factory->where('isActive','1')->where('deviceToken !=','')->get('activeMachine')->result_array();
        /*echo "factory - ".$factoryId;
        echo "<pre>";print_r($result);
       
         echo "Notification Data";*/
        foreach ($result as $key => $value) 
        {   
        	$machineId = $value['machineId'];
        	$activeId = $value['activeId'];
        	$deviceToken = $value['deviceToken'];

        	$dataNot = array
            (
                'message'   => "Machine unlock check",
                'title'     => "Machine unlock check",
                'type' => "machineUnlockCheck",
                'vibrate'   => 1,
                'sound'     => 1,
                'factoryId'      => $factoryId,
            	'machineId'      => $machineId,
            	'activeId'      => $activeId
            );

            $fields = array
                (
                    'registration_ids'  =>  array($deviceToken),  
                    'data' => $dataNot
                );

            $headers = array
                (
                    'Authorization: key=AAAAkTfrC6s:APA91bFC7wZWAl0cZi1kTBHT1k3XjOCqIfCVAP9woO-Lnb04hIXT2vhHgrc7TBEd-Q7WdlSfQvEI5M5DPvhVBoOeGCxmGKB6n6O5Nf_Jr8V1ZMnkHemg_25NtOiw7DaHntUoolBIlJh6',
                    'Content-Type: application/json'
                );

            $ch = curl_init();
            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
            curl_setopt( $ch,CURLOPT_POST, true );
            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
            $notResult = curl_exec($ch);
            curl_close($ch);
           
            $notJson =  json_decode($notResult);

           
          /*  echo "<pre>";print_r($notJson);*/
            
            if (array_key_exists("error",$notJson->results[0]))
            {
                $errorMessage = $notJson->results[0]->error;
                if ($errorMessage == "NotRegistered") 
                { 
                	$endTime = time();
                    $this->CM->userUnlockMachine($factoryId, $machineId);
                    $this->CM->userLeaveMachine($factoryId, $activeId, $endTime);
                }
            }

            unset($dataNot);
            unset($notJson);
            unset($deviceToken);

        }
        unset($result);
	   

    }	

    public function groupBySqlMode()
    {
    	$this->db->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");

        $factoryCount = $this->db->where('isDeleted','0')->get('factory')->num_rows();

        for ($i=1; $i <= $factoryCount; $i++) 
        { 
            $factoryId = $i;

	        $this->factory = $this->load->database('factory'.$factoryId, TRUE);

	        $this->factory->query("SET GLOBAL sql_mode=(SELECT REPLACE(@@sql_mode,'ONLY_FULL_GROUP_BY',''))");

	    }

    }

    public function unlockPhonePositionNotification()
    {
        $factoryCount = $this->db->where('isDeleted','0')->where('type','agv')->get('factory')->result_array();

     	foreach ($factoryCount as $key1 => $value1) 
		{
		
            $factoryId = $value1['factoryId'];

	        $this->factory = $this->load->database('factory'.$factoryId, TRUE);

	        $result = $this->factory->where('isActive','1')->where('deviceToken !=','')->get('activePhonePosition')->result_array();
	       
	        foreach ($result as $key => $value) 
	        {   
	        	$phoneId = $value['phoneId'];
	        	$activePhoneId = $value['activePhoneId'];
	        	$deviceToken = $value['deviceToken'];

	        	$dataNot = array
	            (
	                'message'   => "Unlock phone position check",
	                'title'     => "Unlock phone position check",
	                'type' => "phonePositionUnlockCheck",
	                'vibrate'   => 1,
	                'sound'     => 1,
	                'factoryId'      => $factoryId,
                	'phoneId'      => $phoneId,
                	'activePhoneId'      => $activePhoneId
	            );

	            $fields = array
	                (
	                    'registration_ids'  =>  array($deviceToken),  
	                    'data' => $dataNot
	                );

	            $headers = array
	                (
	                    'Authorization: key=AAAAcDKMLSc:APA91bECIHY87bZsNkgxUwx3u8Y_s8PG3NmmrkWITBf5raw1LDYtGVMdnJDZWHdR87KYzFmDuzhMH_PgJoQUN3gTfj15_bi7zsSO5nkfp45YVbW6kz0QrslL-KeEod6Yipi5JrPBIxTP',
	                    'Content-Type: application/json'
	                );

	            $ch = curl_init();
	            curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
	            curl_setopt( $ch,CURLOPT_POST, true );
	            curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
	            curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
	            curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
	            curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
	            $notResult = curl_exec($ch);
	            curl_close($ch);
	           
	            $notJson =  json_decode($notResult);

	            
	            if (array_key_exists("error",$notJson->results[0]))
	            {
	                $errorMessage = $notJson->results[0]->error;
	                if ($errorMessage == "NotRegistered") 
	                { 
	                	$endTime = time();
	                    $where = array("phoneId" => $phoneId);
						$data = array("isSelected" => "0");
						$this->CM->updateData($where,$data,"phonePosition");

	                    $this->CM->updateActivePhonePosition($activePhoneId);
	                }
	            }

	            unset($dataNot);
	            unset($notJson);
	            unset($deviceToken);

	        }
	        unset($result);
	        unset($this->factory);
	    }

    }

     public function addDemoDetection()
	{
	    $this->form_validation->set_rules('data', 'data', 'required');
	    $this->form_validation->set_rules('datetime', 'datetime', 'required');
	    $this->form_validation->set_rules('confidence', 'confidence', 'required');
	    $this->form_validation->set_rules('lable', 'lable', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"message" =>  $error[0]);
		}
		else
		{  
			$data =  json_decode($this->input->post('data'));
			$datetime = $this->input->post('datetime');
			$confidence = $this->input->post('confidence');
			$lable = $this->input->post('lable');

			foreach ($data as $key => $value) 
			{
				$insertData = array(
					"color" => $value->color,
					"rgb1" => $value->rgb1,
					"rgb2" => $value->rgb2,
					"rgb3" => $value->rgb3,
					"datetime" => $datetime,
					"confidence" => $confidence,
					"lable" => $lable,
				);


				$this->db->insert('demoDetection',$insertData);
			}

			$json =  array("status" => "1","message"=>"data added successfully");
		  
		   
		    
		}
	    
	    echo json_encode($json);
    }

    function sendRepeatNotification()
    {/*
    	$date = date('Y-m-d H:i:s');
    	$select = "notificationUserLog.notificationUserLogId,notificationUserLog.machineId,notificationUserLog.factoryId,notificationUserLog.notificationSendTime,notificationUserLog.notificationSendCount,user.receiveReminders,notificationUserLog.notificationText,user.deviceToken,user.receiveReminders";
    	$where = array("notificationFlag" => "0","notificationSendTime <" => $date);
    	$join = array("user" => "notificationUserLog.userId = user.userId");
    	$result = $this->CM->getWhereDBJoin($select,$where,$join,"notificationUserLog");
    	//echo $this->db->last_query();exit;
    	foreach ($result as $key => $value) 
    	{
    		if ($value['receiveReminders'] > 0) 
    		{

	    		$notificationUserLog = array("notificationSendTime" => date('Y-m-d H:i:s',strtotime("+".$value['receiveReminders']." min")),"notificationSendCount" => $value['notificationSendCount'] + 1);

				$this->CM->updateDBData(array("notificationUserLogId" => $value['notificationUserLogId']),$notificationUserLog,"notificationUserLog");
				$msg = array
				(
					'message' 	=> $value['notificationText'],
					'title'		=> "Reminder notification",
					'subtitle'	=> $value['notificationText'],
					'vibrate'	=> 1,
					'sound'		=> 1,
					'machineId' => $value['machineId'],
					'factoryId' => $value['factoryId'],
					'notificationUserLogId' => $value['notificationUserLogId']
				);
				$fields = array
				(
					'registration_ids' 	=> array($value['deviceToken']), 
					'data' => $msg
				);

				$headers = array
				(
					'Authorization: key=' . API_ACCESS_KEY,
					'Content-Type: application/json'
				);

				$ch = curl_init();
				curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' ); 
				curl_setopt( $ch,CURLOPT_POST, true );
				curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
				curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
				curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, false );
				curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
				curl_exec($ch);
				curl_close($ch);
	    	}
	    	else
	    	{
	    		$notificationUserLog = array("notificationFlag" => "1");

				$this->CM->updateDBData(array("notificationUserLogId" => $value['notificationUserLogId']),$notificationUserLog,"notificationUserLog");
	    	}
    	}*/
    	
    }




    /*public function repeatTask($factoryId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
	    $result = $this->factory->where(array("isDelete" => "0","repeat !=" => "never","dueDate" => date('Y-m-d',strtotime("-1 days"))))->get('taskMaintenace')->result_array();

    	foreach ($result as $key => $value) 
    	{
    		$userIds = $value['userIds'];
			$machineId =  $value['machineId'];
			$task = $value['task'];
			$repeat = $value['repeat'];
			$createdByUserId = $value['createdByUserId'];

			if ($repeat == "everyDay") 
			{
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}elseif ($repeat == "everyWeek") 
			{
				$startDate = date('Y-m-d');
				$day = date('l',strtotime($startDate));
				if ($day == "Friday") 
				{
					$startDate = date('Y-m-d',strtotime("next monday"));
				}
				$dueDate = date('Y-m-d',strtotime("next friday"));
				$repeatOrder = 3;
			}elseif ($repeat == "everyMonth") 
			{
				$startDate = date('Y-m-01');
				$dueDate = date('Y-m-t');
				$repeatOrder = 4;
			}elseif ($repeat == "everyYear") 
			{
				$startDate = date('Y-01-01');
				$dueDate = date('Y-12-t');	
				$repeatOrder = 5;	
			}
			

			if (!empty($value['mainTaskId'])) 
			{
				$mainTaskId = $value['mainTaskId'];
			}else
			{
				$mainTaskId = $value['taskId'];
			}

            $data=array(
                'userIds' => $userIds,
                'machineId' => $machineId,  
                'task' => $task,  
                'repeat' => $repeat,
                'dueDate' => $dueDate,
                'startDate' => $startDate,
                'repeatOrder' => $repeatOrder,
                'mainTaskId' => $mainTaskId,
                'createdByUserId' => $createdByUserId
            );  

            $this->factory->insert('taskMaintenace',$data);
           // $this->factory->last_query();exit;
    	}
    }
*/


    public function repeatTask($factoryId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	//$date = date('Y-m-d',strtotime("-1 days 2022-01-01"));
	    $result = $this->factory->where(array("isDelete" => "0","repeat !=" => "never","dueDate" => date('Y-m-d',strtotime("-1 days"))))->get('taskMaintenace')->result_array();
    	foreach ($result as $key => $value) 
    	{
    		$userIds = $value['userIds'];
			$machineId =  $value['machineId'];
			$task = $value['task'];
			$repeat = $value['repeat'];
			$createdByUserId = $value['createdByUserId'];

			$endTaskDate = $value['endTaskDate'];
			$EndAfteroccurances = $value['EndAfteroccurances'];
			$newTask = $value['newTask'];
			$dueWeekDay = $value['dueWeekDay'];
			$monthDueOn = $value['monthDueOn'];
			$dueMonthMonth = $value['dueMonthMonth'];
			$dueMonthWeek = $value['dueMonthWeek'];
			$dueMonthDay = $value['dueMonthDay'];
			$yearDueOn = $value['yearDueOn'];
			$dueYearMonth = $value['dueYearMonth'];
			$dueYearMonthDay = $value['dueYearMonthDay'];
			$dueYearWeek = $value['dueYearWeek'];
			$dueYearDay = $value['dueYearDay'];
			$dueYearMonthOnThe = $value['dueYearMonthOnThe'];

			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				if (!empty($dueWeekDay) && $newTask == "1") 
				{
					$startDate = date('Y-m-d',strtotime("next monday"));
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$startDate = date('Y-m-d');
					$day = date('l',strtotime($startDate));
					if ($day == "Friday") 
					{
						$startDate = date('Y-m-d',strtotime("next monday"));
					}
					$dueDate = date('Y-m-d',strtotime("next friday"));
				}
				$repeatOrder = 3;
			}elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-01');
				if ($newTask == "1") 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}

					if (date('m') == date('m',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 months".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));
					}
				}else
				{
					$dueDate = date('Y-m-t');
				}
				$repeatOrder = 4;
			}elseif ($repeat == "everyYear") 
			{
				if ($newTask == "1") 
				{
					
					if ($yearDueOn == "1") 
					{
						$monthYear = date('m',strtotime($dueYearMonth));
						if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
						}else
						{	
							$dueDate = date('Y-'.$monthYear.'-t');
							if ($dueYearMonthDay == "third_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueYearMonthDay == "second_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
							}else
							{
								$dueDate = date('Y-m-t',strtotime($dueDate));
							}
						}
					}elseif ($yearDueOn == "2") 
					{
						if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
						{
							$monthYear = date('m',strtotime($dueYearMonthOnThe));
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
							if ($monthYear != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-d',strtotime("12/31"));
						}
					}

					$startDate = date('Y-'.$monthYear.'-01');

					if (date('Y') == date('Y',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 years".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));
					}
				}else
				{
					$startDate = date('Y-01-01');
					$dueDate = date('Y-12-t');	
				}
				$repeatOrder = 5;	
			}

			if (!empty($value['mainTaskId'])) 
			{
				$mainTaskId = $value['mainTaskId'];
			}else
			{
				$mainTaskId = $value['taskId'];
			}

            $data=array(
                'userIds' => $userIds,
                'machineId' => $machineId,  
                'task' => $task,  
                'repeat' => $repeat,
                'dueDate' => $dueDate,
                'startDate' => $startDate,
                'repeatOrder' => $repeatOrder,
                'mainTaskId' => $mainTaskId,
                'createdByUserId' => $createdByUserId,
                'newTask' => !empty($newTask) ? $newTask : "0",
                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
                'endTaskDate' => !empty($endTaskDate) ? $endTaskDate : NULL,
                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
            );  

            $this->factory->insert('taskMaintenace',$data);
           // $this->factory->last_query();exit;
    	}
    }

    public function repeatTaskStatic($factoryId,$dateUrl)
	{
		$this->factory = $this->load->database('factory'.$factoryId, TRUE);
		
	    $result = $this->factory->where(array("isDelete" => "0","repeat !=" => "never","dueDate" => date('Y-m-d',strtotime("-1 days ".$dateUrl))))->get('taskMaintenace')->result_array();
	   
		foreach ($result as $key => $value) 
		{
			$userIds = $value['userIds'];
			$machineId =  $value['machineId'];
			$task = $value['task'];
			$repeat = $value['repeat'];
			$createdByUserId = $value['createdByUserId'];

			$endTaskDate = $value['endTaskDate'];
			$EndAfteroccurances = $value['EndAfteroccurances'];
			$newTask = $value['newTask'];
			$dueWeekDay = $value['dueWeekDay'];
			$monthDueOn = $value['monthDueOn'];
			$dueMonthMonth = $value['dueMonthMonth'];
			$dueMonthWeek = $value['dueMonthWeek'];
			$dueMonthDay = $value['dueMonthDay'];
			$yearDueOn = $value['yearDueOn'];
			$dueYearMonth = $value['dueYearMonth'];
			$dueYearMonthDay = $value['dueYearMonthDay'];
			$dueYearWeek = $value['dueYearWeek'];
			$dueYearDay = $value['dueYearDay'];
			$dueYearMonthOnThe = $value['dueYearMonthOnThe'];

			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d',strtotime($dateUrl));
				$dueDate = date('Y-m-d',strtotime($dateUrl));
				$repeatOrder = 2;
			}elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				if (!empty($dueWeekDay) && $newTask == "1") 
				{
					$startDate = date('Y-m-d',strtotime("next monday " . $dateUrl));
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay." ".$dateUrl));
				}else
				{
					$startDate = date('Y-m-d');
					$day = date('l',strtotime($startDate));
					if ($day == "Friday") 
					{
						$startDate = date('Y-m-d',strtotime("next monday ". $dateUrl));
					}
					$dueDate = date('Y-m-d',strtotime("next friday ". $dateUrl));
				}
				$repeatOrder = 3;
			}elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-01');
				if ($newTask == "1") 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}

					if (date('m') == date('m',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 months".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));
					}
				}else
				{
					$dueDate = date('Y-m-t');
				}
				$repeatOrder = 4;
			}elseif ($repeat == "everyYear") 
			{
				if ($newTask == "1") 
				{
					
					if ($yearDueOn == "1") 
					{
						$monthYear = date('m',strtotime($dueYearMonth));
						if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
						}else
						{	
							$dueDate = date('Y-'.$monthYear.'-t');
							if ($dueYearMonthDay == "third_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueYearMonthDay == "second_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
							}else
							{
								$dueDate = date('Y-m-t',strtotime($dueDate));
							}
						}
					}elseif ($yearDueOn == "2") 
					{
						if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
						{
							$monthYear = date('m',strtotime($dueYearMonthOnThe));
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
							if ($monthYear != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-d',strtotime("12/31"));
						}
					}

					$startDate = date('Y-'.$monthYear.'-01');

					if (date('Y') == date('Y',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 years".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));
					}
				}else
				{
					$startDate = date('Y-01-01');
					$dueDate = date('Y-12-t');	
				}
				$repeatOrder = 5;	
			}

			if (!empty($value['mainTaskId'])) 
			{
				$mainTaskId = $value['mainTaskId'];
			}else
			{
				$mainTaskId = $value['taskId'];
			}

	        $data=array(
	            'userIds' => $userIds,
	            'machineId' => $machineId,  
	            'task' => $task,  
	            'repeat' => $repeat,
	            'dueDate' => $dueDate,
	            'createdDate' => date("Y-m-d H:i:s",strtotime($dateUrl)),
	            'startDate' => $startDate,
	            'repeatOrder' => $repeatOrder,
	            'mainTaskId' => $mainTaskId,
	            'createdByUserId' => $createdByUserId,
	            'newTask' => !empty($newTask) ? $newTask : "0",
	            'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
	            'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
	            'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
	            'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
	            'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
	            'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
	            'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
	            'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
	            'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
	            'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
	            'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
	            'endTaskDate' => !empty($endTaskDate) ? $endTaskDate : NULL,
	            'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
	        );  
	         echo "<pre>";print_r($data);
	        $this->factory->insert('taskMaintenace',$data);
	       // $this->factory->last_query();exit;
		}
	}


    function getFirstandLastDate($year, $month, $week, $day) 
    {

	    $thisWeek = 1;

	    for($i = 1; $i < $week; $i++) {
	        $thisWeek = $thisWeek + 7;
	    }

	    $currentDay = date('Y-m-d',mktime(0,0,0,$month,$thisWeek,$year));

	    $monday = strtotime($day.' this week', strtotime($currentDay));;

	    $weekStart = date('Y-m-d', $monday);

	    return $weekStart;
	}

    public function addStopAndWaitNotification() 
	{	
		//echo "CronTest";
		$this->CM->addStopAndWaitNotification("Stop");
		$this->CM->addStopAndWaitNotification("Wait");
		$this->CM->addMtConnectStopAndWaitNotification();
	}

	function increaseReduceHourInServerClock()
	{
		$result = $this->CM->getIncreaseReduceHour();
		$this->AM->updateDBData(array("id" => 1),array("status" => ($status == "0") ? "1" : "0"),'increaseReduceHour');
	}

	// function restartSetapp()
	// {
	// 	$factory = $this->db->where('isDeleted','0')->where('type','machine')->get('factory')->result_array();	
	// 	foreach ($factory as $key => $value) 
	// 	{
	// 		$factoryId = $value['factoryId'];
	// 		$select = "betaLogImageColorOverview.*,machine.machineName,machine.machineId";
	// 		$where = array("machine.isDeleted" => "0","machine.machineStatus" => "0","insertTime >" => date('Y-m-d H:i:s',strtotime("-3 minutes")));
	// 		$join = array("machine" => "betaLogImageColorOverview.machineId = machine.machineId");
	// 		$whereIn = array("betaLogImageColorOverview.color" => array("NoData","NoInternet"));
	// 		$result = $this->CM->getWhereJoinWithWhereIn($factoryId, $select, $where, $join, $whereIn, "betaLogImageColorOverview");


	// 		foreach ($result as $machinekey => $machinevalue) 
	// 		{
	// 			$diff = strtotime(date('Y-m-d H:i:s')) - strtotime($machinevalue['insertTime']);

	// 			if ($diff <= 99) 
	// 			{
	// 				$machineId = $machinevalue['machineId'];
	// 				$machineUser = $this->CM->getLastActiveMachine($factoryId,$machineId);

	// 				$message = "SetApp restarted successfully";
	// 				$title = "Set app restart";
	// 				$type = "setAppReStart";
	// 				$activeId = $machineUser->activeId;
	// 				$fcmToken = array($machineUser->deviceToken);
	// 				$this->CM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId);
	// 			}else if($diff >= 100 && $diff <= 180)
	// 			{
	// 				$message = "Hello.<br>We have found ". $machinevalue['color'] ." entry in machine ".$machinevalue['machineName']." at ". base64_decode($value['factoryName']) ." factory. Please check SetApp.";
	// 				$subject = $machinevalue['color'] ." entry found";
	// 				$this->CM->sendEmail($message,$subject,"","");	
	// 			}
	// 		}
	// 	}
	// }
}