<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class MeetingNotes extends CI_Controller 
{
	var $factory;
	var $factoryId;


	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);
	}

	//meeting_notes function use for get meeting notes list page
	function meeting_notes()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(20);
		if ($accessPoint == true) 
        {
			$data['accessPointEdit'] = $this->AM->accessPointEdit(20);
			$data['machine'] = $this->AM->getallMachines($this->factory)->result_array();  
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

			$this->load->view('meeting_notes',$data);
			$this->load->view('footer');		
			$this->load->view('script_file/meeting_notes_footer',$data);	
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}	
	}

	//meeting_notes_pagination function use for get meeting notes details
	public function meeting_notes_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$noteId = $this->input->post('noteId');
		$noteName = $this->input->post('noteName');
		$notes = $this->input->post('notes');
		$createdDate = $this->input->post('createdDate');
		$filterType = $this->input->post('filterType');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');

		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		//Get all data count
		$listDataLogsCount = $this->AM->getAllMeetingNotesCount()->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchMeetingNotesCount($noteId, $noteName, $notes, $createdDate, $filterType, $dateValS, $dateValE)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getAllMeetingNotesLogs($row, $rowperpage, $noteId, $noteName, $notes, $createdDate, $filterType, $dateValS, $dateValE)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['delete'] = '<a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 5px;"  onclick="delete_meeting_notes('.$value['noteId'].')" href="javascript:void(0);" class="btn btn-primary">
				<img style="width: 23px;height: 23px;" src="' . base_url("assets/nav_bar/delete_white.svg") . '"></a>';	

			$listDataLogs[$key]['download'] = '<a style="background-color: #002060;border-color: #002060;padding: 1px 11px 4px 11px;border-radius: 6px;margin-top: -4px;"  href="'. base_url('MeetingNotes/downloadPdf/'.$value['noteId']) .'" class="btn btn-primary">
				<img style="width: 18px;height: 15px;margin-top: -5px;" src="' . base_url("assets/nav_bar/download.png") . '"><span> '. Download.' </span></a>';	

			  if ($value['filterType'] == "1") 
		      {
		        $listDataLogs[$key]['filterType'] = Daily;
		      }
		      elseif ($value['filterType'] == "2") 
		      {
		        $listDataLogs[$key]['filterType'] = Weekly;
		      }
		      elseif ($value['filterType'] == "3") 
		      {
		        $listDataLogs[$key]['filterType'] = Monthly;
		      }
		      elseif ($value['filterType'] == "4") 
		      {
		        $listDataLogs[$key]['filterType'] = Yearly;
		      } 

		      $listDataLogs[$key]['noteName'] = "<a target='_blank' style='color: #002060;' href=".base_url('MeetingNotes/meeting_note_views/'.$value['noteId']).">".$value['noteName']."</a>";
		}
 
		
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	//delete_meeting_notes function use for delete meeting notes 
	public function delete_meeting_notes() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('deleteNoteId','deleteNoteId','required');

		if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
	        echo json_encode($json); die; 
        } 
        else 
        {   
			$noteId = $this->input->post('deleteNoteId');
			$data=array(
                'isDelete' => "1"
            );  
            //Delete meeting notes
			$this->AM->updateData(array("noteId" => $noteId),$data, "meetingNotes");
			$json = array("status" => "1","message" => Notedeletedsuccessfully);
	        echo json_encode($json); die; 
		}
	}

	//add_meeting_notes function use for add new meeting notes
	public function add_meeting_notes() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('filterType','filterType','required');
        $this->form_validation->set_rules('machineIds','machineIds','required');
        $this->form_validation->set_rules('noteName','noteName','required');
        $this->form_validation->set_rules('startDate','startDate','required');
        $this->form_validation->set_rules('endDate','endDate','required');

        //checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforaddtask);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$filterType = $this->input->post('filterType');
			$machineIds =  $this->input->post('machineIds');
			$noteName = $this->input->post('noteName');

			//Set start and end date as par selected filter type
			if ($filterType == "1") 
			{
				$startDate = date('Y-m-d',strtotime($this->input->post('startDate')));
				$endDate = date('Y-m-d',strtotime($this->input->post('endDate')));
			}else if ($filterType == "2") 
			{
				$dto = new DateTime();
				$dto->setISODate(date('Y'), $this->input->post('weekStartDate'));
				$startDate = $dto->format('Y-m-d');
				$dto->setISODate(date('Y'), $this->input->post('weekEndDate'));
				$endDate = $dto->format('Y-m-d');
			}else if ($filterType == "3") 
			{
				$startDate = date('Y-m-01',strtotime($this->input->post('monthStartDate')));
				$endDate = date('Y-m-01',strtotime($this->input->post('monthEndDate')));
			}else if ($filterType == "4") 
			{
				$startDate = date($this->input->post('yearStartDate').'-01-01');
				$endDate = date($this->input->post('yearEndDate').'-01-01');
			}
			
			$data = array(
					"filterType" => $filterType,
					"machineIds" => $machineIds,
					"noteName" => $noteName,
					"startDate" => $startDate,
					"endDate" => $endDate
				);

			//Add new meeting notes
	        $noteId  = $this->AM->insertData($data, "meetingNotes");
	       	$json = array("status" => "1","noteId" => $noteId,"message" => Meetingnoteaddedsuccessfully);
	       	echo json_encode($json); die;
        }
	}

	//meeting_note_details function use for get meeting note details page
	public function meeting_note_details($noteId)
	{ 
		if (empty($noteId)) 
		{
		    redirect('MeetingNotes/meeting_notes');
		}
		
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPointEdit = $this->AM->accessPointEdit(20);
		if ($accessPointEdit == true) 
        {
		
			$data['meetingNoteDetails'] = $meetingNoteDetails = $this->AM->getMeetingNoteDetails($noteId); 
			if ($meetingNoteDetails->isDelete == "1") 
			{
				redirect('MeetingNotes/meeting_notes');	
			}
		
			//Get running hour as par start and end date
			$data['runningStartDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
			$data['runningEndDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);

			//Get no production hour as par start and end date
			$data['noProductionStartDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
			$data['noProductionEndDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);

			//Get no resports count as par start and end date
			$data['NoOfReportsStart'] = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
			$data['NoOfReportsEnd'] = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->endDate,$meetingNoteDetails->filterType);

			//Get no stop count as par start and end date
			$data['NoOfStoppagesStart'] = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
			$data['NoOfStoppagesEnd'] = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType);

			$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

			$select = "machineId,machineName";

			$whereIn = array("machineId" => explode(",", $meetingNoteDetails->machineIds));
			$data['machines'] = $this->AM->getWhereIn($select,$whereIn, 'machine');

			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('meeting_note_details',$data);
			$this->load->view('footer');		
			$this->load->view('script_file/meeting_note_details_footer');	
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('accessPoint');
			$this->load->view('footer');
		}	
	}


	//meeting_note_views function use for get meeting note details page
	public function meeting_note_views($noteId)
	{
		if (empty($noteId)) 
		{
		    redirect('MeetingNotes/meeting_notes');
		}
		
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$data['meetingNoteDetails'] = $meetingNoteDetails = $this->AM->getMeetingNoteDetails($noteId); 
		if ($meetingNoteDetails->isDelete == "1") 
		{
			redirect('MeetingNotes/meeting_notes');	
		}
		$data['runningStartDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
		$data['runningEndDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);

		//Get no production hour as par start and end date
		$data['noProductionStartDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
		$data['noProductionEndDataHours'] = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);

		//Get no resports count as par start and end date
		$data['NoOfReportsStart'] = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
		$data['NoOfReportsEnd'] = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->endDate,$meetingNoteDetails->filterType);

		//Get no stop count as par start and end date
		$data['NoOfStoppagesStart'] = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
		$data['NoOfStoppagesEnd'] = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType);

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$select = "machineId,machineName";

		$whereIn = array("machineId" => explode(",", $meetingNoteDetails->machineIds));
		$data['machines'] = $this->AM->getWhereIn($select,$whereIn, 'machine');

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('meeting_note_views',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/meeting_note_details_footer');		
	}

	//secondsToHours function use for convert seconds to hour
	function secondsToHours($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.5f', $seconds/3600);
    }

    //reportProblemMeetingNotes_pagination get report problem not resolved list
    public function reportProblemMeetingNotes_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$userName = $this->input->post('userName');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$isActive = $this->input->post('isActive');
		$filterType = $this->input->post('filterType');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		//Get all data count
		$listDataLogsCount = $this->AM->getAllReportsAProblemCountMeetingNotes($this->factory, $problemId, $is_admin,$filterType,$dateValS,$dateValE)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchReportsAProblemCountMeetingNotes($this->factory, $problemId, $is_admin,$status,$type,$userName,$dateValS,$dateValE,$isActive,$filterType)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getReportsAProblemLogsMeetingNotes($this->factory, $problemId, $is_admin, $row, $rowperpage,$status,$type,$userName,$dateValS,$dateValE,$isActive,$filterType)->result_array();  

		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['userName'] = '<img style="border-radius: 50%;" width="16" height="16" src="'. base_url('assets/img/user/'.$value['userImage']) .'">'.$value['userName'];
			if ($value['type'] == "1") 
        	{
        		$type = Accident;
        	}
        	elseif ($value['type'] == "0")
        	{
        		$type = Incident;
        	}
        	else
        	{
        		$type = Suggestion;
        	}
			$listDataLogs[$key]['type'] = $type;
			$listDataLogs[$key]['isActive'] = ($value['isActive'] == "1") ? '<img width="16" height="16" src="'. base_url('assets/img/tick.svg') .'">' : "";
			$listDataLogs[$key]['picture'] = (!empty($value['picture'])) ? "Yes" : "No";
			$listDataLogs[$key]['status'] = ($value['status'] == "1") ? '<img width="16" height="16" src="'. base_url('assets/img/tick.svg') .'">' : '<img width="16" height="16" src="'. base_url('assets/img/cross.svg') .'">';
			$descriptionCount = strlen($value['description']);
			$causeCount = strlen($value['cause']);
			$improvementCount = strlen($value['improvement']);

			$listDataLogs[$key]['description'] = ($descriptionCount > 40) ? substr($value['description'],0,40).'...' : $value['description'];			
			$listDataLogs[$key]['cause'] = ($causeCount > 40) ? substr($value['cause'],0,40).'...' : $value['cause'];			
			$listDataLogs[$key]['improvement'] = ($improvementCount > 40) ? substr($value['improvement'],0,40).'...' : $value['improvement'];			

		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);

		echo json_encode($response); die;
		
	}
	

	//update_MeetingNotes function use for update notes in meeting note 
	function update_MeetingNotes()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$noteId = $this->input->post('noteId');
		$description =  $this->input->post('description');
		$data = array(
				"notes" => $description
			);
		//Update meeting notes
        $this->AM->updateData(array("noteId" => $noteId),$data, "meetingNotes");
	}

	//downloadPdf function use for download meeting note pdf
	function downloadPdf($noteId)
	{

		$baseUrl = base_url('');
		$data['meetingNoteDetails'] = $meetingNoteDetails = $this->AM->getMeetingNoteDetails($noteId); 
		
		//Get running hour as par start and end date
		$runningStartDataHours = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
		$runningEndDataHours = $this->secondsToHours($this->AM->getMeetingMachineRunningStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);
		
		//Get no production hour as par start and end date
		$noProductionStartDataHours = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType)->countVal);
		$noProductionEndDataHours = $this->secondsToHours($this->AM->getMeetingMachineNoProductionStatusData($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType)->countVal);
		
		//Get no resports count as par start and end date
		$NoOfReportsStart = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
		$NoOfReportsEnd = $this->AM->getMeetingNoOfReportsCount($meetingNoteDetails->endDate,$meetingNoteDetails->filterType);
		
		//Get no stop count as par start and end date
		$NoOfStoppagesStart = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->startDate,$meetingNoteDetails->filterType);
		$NoOfStoppagesEnd = $this->AM->getMeetingNoOfStoppagesCount($meetingNoteDetails->machineIds,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType);

		$users = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$select = "machineId,machineName";

		$whereIn = array("machineId" => explode(",", $meetingNoteDetails->machineIds));
		$machines = $this->AM->getWhereIn($select,$whereIn, 'machine');

		$reportListLog = $this->AM->getReportsAProblemLogsMeetingNotesPdf($meetingNoteDetails->startDate,$meetingNoteDetails->endDate,$meetingNoteDetails->filterType	)->result_array();  
		
		if ($meetingNoteDetails->filterType == "1") 
        {
            $startDate = $meetingNoteDetails->startDate;
			$endDate = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$meetingNoteDetails->endDate;
			$TitleDate = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$meetingNoteDetails->startDate .' - '. $meetingNoteDetails->endDate;
			$type = Daily;
        }
        elseif ($meetingNoteDetails->filterType == "2") 
        {
            $startDate = 'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
			$endDate = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
			$TitleDate = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Week '.date('W, Y',strtotime($meetingNoteDetails->startDate)) .' - Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
			$type = Weekly;
        }
        elseif ($meetingNoteDetails->filterType == "3") 
        {
            $startDate = date('F, Y',strtotime($meetingNoteDetails->startDate));
			$endDate = date('F, Y',strtotime($meetingNoteDetails->endDate));
			$TitleDate = date('F, Y',strtotime($meetingNoteDetails->startDate)) .' - '. date('F, Y',strtotime($meetingNoteDetails->endDate));
			$type = Monthly;
        }
        elseif ($meetingNoteDetails->filterType == "4") 
        {
            $startDate =  date('Y',strtotime($meetingNoteDetails->startDate));
            $endDate =  '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('Y',strtotime($meetingNoteDetails->endDate));
          	$TitleDate = '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.date('Y',strtotime($meetingNoteDetails->startDate)) .' - '. date('Y',strtotime($meetingNoteDetails->endDate));
          	$type = Yearly;
        } 


		if ($NoOfStoppagesStart > $NoOfStoppagesEnd) 
		{ 
            $NoOfStoppagesMargin = $NoOfStoppagesStart - $NoOfStoppagesEnd;
            $NoOfStoppagesText = Decreased; 
            $NoOfStoppagesSign = "-"; 
        }
        else if ($NoOfStoppagesStart < $NoOfStoppagesEnd) 
        { 
	        $NoOfStoppagesMargin = $NoOfStoppagesEnd - $NoOfStoppagesStart;
	        $NoOfStoppagesText = Increased; 
	        $NoOfStoppagesSign = "+"; 
	    }
	    else
	    { 
	    	$NoOfStoppagesMargin = "0";
	    	$NoOfStoppagesSign = "";
	    	$NoOfStoppagesText = Stable;
	    } 

	    if ($NoOfReportsStart > $NoOfReportsEnd) 
	    { 
            $NoOfReportsMargin = $NoOfReportsStart - $NoOfReportsEnd;
            $NoOfReportsText = Decreased;
            $NoOfReportsSign = "-"; 
        }
        else if ($NoOfReportsStart < $NoOfReportsEnd) 
        { 
	        $NoOfReportsMargin = $NoOfReportsEnd - $NoOfReportsStart;
	        $NoOfReportsText = Increased; 
	        $NoOfReportsSign = "+"; 
	    }
	    else
	    { 
	    	$NoOfReportsMargin = "0";
	    	$NoOfReportsSign = "";
	    	$NoOfReportsText = Stable;
	    } 

	    if ($runningStartDataHours > $runningEndDataHours) 
	    { 
            $runningDataMargin = $runningStartDataHours - $runningEndDataHours;
            $runningDataText = Decreased; 
            $runningDataSign = "-"; 
        }
        else if ($runningStartDataHours < $runningEndDataHours) 
        { 
	        $runningDataMargin = $runningEndDataHours - $runningStartDataHours;
	        $runningDataText = Increased; 
	        $runningDataSign = "+"; 
	    }
	    else
	    { 
	    	$runningDataMargin = "0";
	    	$runningDataSign = "";
	    	$runningDataText = Stable;
	    }

	    if ($noProductionStartDataHours > $noProductionEndDataHours) 
	    { 
            $noProductionMargin = $noProductionStartDataHours - $noProductionEndDataHours;
            $noProductionText = Decreased; 
            $noProductionSign = "-"; 
        }
        else if ($noProductionStartDataHours < $noProductionEndDataHours) 
        { 
	        $noProductionMargin = $noProductionEndDataHours - $noProductionStartDataHours;
	        $noProductionText = Increased; 
	        $noProductionSign = "+"; 
	    }
	    else
	    { 
	    	$noProductionMargin = "0";
	    	$noProductionSign = "";
	    	$noProductionText = Stable;
	    } 


	    $font = "'Lato'";

		$reportCount = count($reportListLog);

		if ($reportCount < 13 && $reportCount > 3) 
		{
			$formfeed = "<formfeed>";
		}
		else
		{
			$formfeed = "";
		}

		$html  = '<style> <link rel="preconnect" href="https://fonts.gstatic.com"> <link href="https://fonts.googleapis.com/css2?family=Lato:wght@300&display=swap" rel="stylesheet"> </style>
		<body style="font-family: '.$font.', sans-serif;">
				<div style="width: 100%;">
				<div style="width: 66%;float: left;"><h3>&nbsp;&nbsp;&nbsp;'.$meetingNoteDetails->noteName.' - '. $type .'</h3></div>
				<div style="width: 33%;float: left;"><h5 style="float: right;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$TitleDate.'</h5></div>
				 </div>
				 <div style="width: 100%;margin-left: 10px;">';
		foreach ($machines as $key => $value) 
		{
			$html .=	'<div style="width: 15%;background: #002060 !important;border-color: #002060!important;color: #fff;font-weight: 600;line-height: 20px;padding: 6px 12px;border-radius: 10px;float: left;margin-left: 5px;">'. $value['machineName'] .'</div>';
		}

			$html .=	'
				</div>
				<div style="width: 100%;margin-top: 10px;font-family: "Lato", sans-serif;">
					<div style="width: 45%;float: left;background-color: red;color: white;margin-left: 10px;margin-right: 10px;padding: 10px;border-radius: 10px;"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.Noofstoppages.'</b></center></div>
					<div style="width: 45%;float: left;background-color: black;color: white;margin-left: 15px;margin-right: 10px;padding: 10px;border-radius: 10px;"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b>'.Noofreports.'</b></center></div>
				 </div>
				 <div style="width: 100%;margin-top: 10px">
					<div style="width: 45%;float: left;;margin-right: 10px;padding: 10px;border-radius: 10px;margin-left: 10px;border: 2px solid #f1f1f1;height: 130px;"> 
						<div style="width: 100%;">
			                <h4>
			                	<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$startDate.'</span>
								<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$endDate.'</span>
							</h4>
			            </div>

			            <div style="margin-left:42%;width:15%;background-color:#f60100;border-radius: 10px;margin-top: -20px;">
			            	<span style="font-size:12px;color:white;text-align: center;"> &nbsp;&nbsp;&nbsp;'.$NoOfStoppagesSign.$NoOfStoppagesMargin.'</span>
			            </div>

		       			<img style="margin-left:10%;width:75%;margin-right10%;" src="'.$baseUrl.'assets/img/blog.jpg"><br>
		       			<div style="margin-left:9%;width: 100%;">
		       				<div style="font-size:15px;color:#002060;border:2px solid #f1f1f1;width: 10%;float:left;border-radius: 10px;">&nbsp;'.$NoOfStoppagesStart.'</div>
		       				<div style="font-size:15px;color:#002060;border:2px solid white;paddind: 10x;width: 60%;float:left;"></div>
		       				<div style="font-size:15px;color:#002060;border:3px solid #f1f1f1;width: 10%;float:left;border-radius: 10px;">&nbsp;'.$NoOfStoppagesEnd.'</div>
		       			</div>

			            <div style="margin-left:38%;margin-top:-35px;">
			            	<h3 style="font-size:12px;color:#FF0000;width:38%;border-radius:10px;border:2px solid #f1f1f1;border-radius:10px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$NoOfStoppagesText.'</h3>
			        	</div>
					</div>

					<div style="width: 45%;float: left;;margin-right: 10px;padding: 10px;border-radius: 10px;margin-left: 10px;border: 2px solid #f1f1f1;height: 130px;"> 
							<div style="width: 100%;">
			                <h4>
			                	<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$startDate.'</span>
								<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$endDate.'</span>
							</h4>
			            </div>

			            <div style="margin-left:45%;width:10%;background-color:black;border-radius: 10px;margin-top: -20px;">
			            	<span style="font-size:12px;color:white;text-align: center;"> &nbsp;&nbsp;&nbsp;'.$NoOfReportsSign.$NoOfReportsMargin.'</span>
			            </div>

		       			<img style="margin-left:10%;width:75%;margin-right10%;" src="'.$baseUrl.'assets/img/blog.jpg"><br>
		       			<div style="margin-left:9%;width: 100%;">
		       				<div style="font-size:15px;color:#002060;border:2px solid #f1f1f1;width: 10%;float:left;border-radius: 10px;">&nbsp;'.$NoOfReportsStart.'</div>
		       				<div style="font-size:15px;color:#002060;border:2px solid white;paddind: 10x;width: 60%;float:left;"></div>
		       				<div style="font-size:15px;color:#002060;border:3px solid #f1f1f1;width: 10%;float:left;border-radius: 10px;">&nbsp;'.$NoOfReportsEnd.'</div>
		       			</div>

			            <div style="margin-left:38%;margin-top:-35px;">
			            	<h3 style="font-size:12px;color:black;width:38%;border-radius:10px;border:2px solid #f1f1f1;border-radius:10px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$NoOfReportsText.'</h3>
			        	</div>
					</div>

					<div style="width: 100%;margin-top:10px;">
						<div style="width: 45%;float: left;background-color: #76ba1b;color: white;margin-left: 10px;margin-right: 10px;padding: 10px;border-radius: 10px;"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;<b> '.Runningtime.'</b> </center></div>
						<div style="width: 45%;float: left;background-color: #ff8000;color: white;margin-left: 15px;margin-right: 10px;padding: 10px;border-radius: 10px;"><center>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; <b>'.Noproductiontime.'</b> </center></div>
					</div>


					<div style="width: 100%;margin-top: 10px">
						<div style="width: 45%;float: left;;margin-right: 10px;padding: 10px;border-radius: 10px;margin-left: 10px;border: 2px solid #f1f1f1;height: 130px;"> 

							<div style="width: 100%;">
				                <h4>
				                	<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$startDate.'</span>
									<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$endDate.'</span>
								</h4>
				            </div>

				            <div style="margin-left:40%;width:20%;background-color:#76ba1b;border-radius: 10px;margin-top: -20px;">
				            	<span style="font-size:12px;color:white;text-align: center;"> &nbsp;&nbsp;&nbsp;'.$runningDataSign.$runningDataMargin.'</span>
				            </div>

			       			<img style="margin-left:10%;width:75%;margin-right10%;" src="'.$baseUrl.'assets/img/blog.jpg"><br>
			       			<div style="margin-left:9%;width: 100%;">
			       				<div style="font-size:15px;color:#002060;border:2px solid #f1f1f1;width: 20%;float:left;border-radius: 10px;">&nbsp;'.$runningStartDataHours.'</div>
			       				<div style="font-size:15px;color:#002060;border:2px solid white;paddind: 10x;width: 40%;float:left;"></div>
			       				<div style="font-size:15px;color:#002060;border:3px solid #f1f1f1;width: 20%;float:left;border-radius: 10px;">&nbsp;'.$runningEndDataHours.'</div>
			       			</div>

				            <div style="margin-left:38%;margin-top:-35px;">
				            	<h3 style="font-size:12px;color:#76ba1b;width:38%;border-radius:10px;border:2px solid #f1f1f1;border-radius:10px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$runningDataText.'</h3>
				        	</div>
						</div>	
						<div style="width: 45%;float: left;;margin-right: 10px;padding: 10px;border-radius: 10px;margin-left: 10px;border: 2px solid #f1f1f1;height: 130px;"> 
								<div style="width: 100%;">
				                <h4>
				                	<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$startDate.'</span>
									<span style="color:#002060;margin-top: 15px;font-size:15px;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.$endDate.'</span>
								</h4>
				            </div>

				            <div style="margin-left:40%;width:20%;background-color:#ff8000;border-radius: 10px;margin-top: -20px;">
				            	<span style="font-size:12px;color:white;text-align: center;"> &nbsp;&nbsp;&nbsp;'.$noProductionSign.$noProductionMargin.'</span>
				            </div>

			       			<img style="margin-left:10%;width:75%;margin-right10%;" src="'.$baseUrl.'assets/img/blog.jpg"><br>
			       			<div style="margin-left:9%;width: 100%;">
			       				<div style="font-size:15px;color:#002060;border:2px solid #f1f1f1;width: 20%;float:left;border-radius: 10px;">&nbsp;'.$noProductionStartDataHours.'</div>
			       				<div style="font-size:15px;color:#002060;border:2px solid white;paddind: 10x;width: 40%;float:left;"></div>
			       				<div style="font-size:15px;color:#002060;border:3px solid #f1f1f1;width: 20%;float:left;border-radius: 10px;">&nbsp;'.$noProductionEndDataHours.'</div>
			       			</div>

				            <div style="margin-left:38%;margin-top:-35px;">
				            	<h3 style="font-size:12px;color:#ff8000;width:38%;border-radius:10px;border:2px solid #f1f1f1;border-radius:10px;">&nbsp;&nbsp;&nbsp;&nbsp;'.$noProductionText.'</h3>
				        	</div>
						</div>
				 </div>

				<div style="margin: 10px;">
				<div class="container" style="width:100%;">
	          		<div style="min-height: 160px;margin-top:15px;padding: 15px;color: white;background-color: white;width: 100%;margin-right:20px;margin-bottom: 20px;background-color: #fff;border:3px solid #f1f1f1;border-radius: 3mm / 3mm;">
					<h3 style="color:#002060;">'.Reports.'</h3>
		            <div class="panel-body">
		              <table  width="100%" cellspacing="0">
		                <thead>
		                  <tr role="row" style="border-left: none;background:rgb(255,255,255); ">
		                  	<th><div style="color:#bec6ce">'.Operator.'<div></th>
							<th><div style="color:#bec6ce">'.Date.'<div></th>
							<th><div style="color:#bec6ce">'.Type.'<div></th>
							<th><div style="color:#bec6ce">'.Description.'</div></th>
							<th><div style="color:#bec6ce">'.Cause.'<div></th>
							<th><div style="color:#bec6ce">'.Improvement.'</div></th>
							<th><div style="color:#bec6ce">'.Confidential.'</div></th>
		                  </tr>
		            </thead> 
		            <tbody>';


					foreach ($reportListLog as $key => $value) 
					{
						$userName = $value['userName'];
						if ($value['type'] == "1") 
			        	{
			        		$typeReport = Accident;
			        	}elseif ($value['type'] == "0")
			        	{
			        		$typeReport = Incident;
			        	}else
			        	{
			        		$typeReport = Suggestion;
			        	}
						$status = ($value['status'] == "1") ? '<img width="16" height="16" src="'. base_url('assets/img/tick.jpg') .'">' : '<img width="16" height="16" src="'. base_url('assets/img/cross.jpg') .'">';
						$html .='
							<tr role="row" >
					            <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$userName.'<div>
			                    </td>

			                    <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$value['date'].'<div>
			                    </td>

			                    <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$typeReport.'<div>
			                    </td>

			                     <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$value['description'].'</div>
			                    </td>

			                    <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$value['cause'].'<div>
			                    </td>

			                    <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$value['improvement'].'</div>
			                    </td>

			                    <td>
			                      <div style="color:#002060;">&nbsp;&nbsp;&nbsp;'.$status.'</div>
			                    </td>
			                  </tr>';
			              }
			           $html .= ' </tbody>

			          		</table>
			        </div>
			      </div>
				</div>        
				</div>
				'. $formfeed .'
				<div style="margin: 0 0 0 10px;">
		          <div style="min-height: 160px;padding: 15px;color: white;background-color: white;border-radius: 30px;width: 100%;margin-right:20px;margin-bottom: 20px;background-color: #fff;border:3px solid #f1f1f1;border-radius: 3mm / 3mm;">
		            	<h3 style="color:#002060;">'.Note.'</h3>
	            		<p style="color:#002060;">'.$meetingNoteDetails->notes.'</p>
		      		</div>
				</div>

				</body>
		 ';


		$filename = $meetingNoteDetails->noteName.' '.date('Y-m-d H:i:s').'.pdf';
		require(APPPATH . 'third_party/mpdf60/mpdf.php');
		$time = time();
		$mpdf=new mPDF('c','A4','','',10,10,10,25,16,13); 
		//$mpdf->SetFont('nyttfont');
		$mpdf->SetDisplayMode('fullpage');
		$mpdf->WriteHTML($html);
		//$mpdf->Output();
		$mpdf->Output($filename, 'D');
		//$mpdf->Output($filename, 'I');
	}


	function secondsToDay($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		return sprintf('%0.5f', ($seconds) / (3600 * 24));
	}
}

?>