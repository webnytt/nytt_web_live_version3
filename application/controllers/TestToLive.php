<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class TestToLive extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
	}

	function getLiveDashboard3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$post = $this->input->post();

		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/dashboard3_pagination');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);


		echo $response;
	}

	public function live_dashboard3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$listMachines = array();
		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		

		$machineView = array();
		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/machineView');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$machineView = json_decode($response);


		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData,
			'machineView' => $machineView 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_dashboard3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_dashboard3_footer');
	}

	public function live_breakdown3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/factoryData');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$liveFactoryData = json_decode($response);

		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines' => $listMachines,
			'factoryData' => $factoryData,
			'liveFactoryData' => $liveFactoryData
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_breakdown3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_breakdown3_footer');
	}


	function getLiveBreakdown3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$post = $this->input->post();
		$ch = curl_init('https://nyttcloud.host/prodapp/live/LiveToTest/breakdown3_pagination');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);

		echo $response;
	}
	
}

?>