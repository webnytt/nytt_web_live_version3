<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Devlopment extends CI_Controller 
{
	var $factory;
	var $factoryId;


	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}
		$this->load->library('encryption');
	}

	//In use
	public function virtualMachineLogList()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$data['virtualMachine'] = $this->AM->getWhere(array("IOName !=" => "0"),"virtualMachine"); 
		// print_r($data);exit;

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('virtual_machine_log_list',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/virtual_machine_log_footer');		
	}

	//In use
	public function virtual_machine_log_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		$IOName = !empty($this->input->post('IOName')) ?  implode(",", $this->input->post('IOName')) : "";
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$endDateValS = $this->input->post('endDateValS');
		$endDateValE = $this->input->post('endDateValE');
		$signal = !empty($this->input->post('signal')) ?  implode(",", $this->input->post('signal')) : "";
		//Get all data count
		$listDataLogsCount = $this->AM->getAllvirtualMachineLogCount($this->factory)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchvirtualMachineLogCount($this->factory,$IOName,$dateValS,$dateValE,$endDateValS, $endDateValE,$signal)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getvirtualMachineLogLogs($this->factory,$row,$rowperpage,$IOName,$dateValS,$dateValE,$endDateValS, $endDateValE, $signal)->result_array();  

		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['signalType'] = ($value['signalType'] == "1") ? "Active" : "Not active"; 
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	//In use
	public function virtualMachineGraph()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		
		$data['virtualMachine'] = $this->AM->getWhere(array("IOName !=" => "0"),"virtualMachine");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('virtual_machine_graph',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/virtual_machine_graph_footer');		
	}

	//In use
	function virtual_machine_graph_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$IOName = $this->input->post('IOName');
		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');
		$choose7 = $this->input->post('choose7');
		$choose8 = $this->input->post('choose8');
		$producationTime = $this->AM->getVirtualMachineLogData($IOName,$choose1,$choose2,"0",$choose7,$choose8);
		$producationTime = $producationTime->countVal;
		$noProducationTime = $this->AM->getVirtualMachineLogData($IOName,$choose1,$choose2,"1",$choose7,$choose8);
		$noProducationTime = $noProducationTime->countVal;
		$partCount = $this->AM->getPartCountData($IOName,$choose1,$choose2,$choose7,$choose8);

		if ($choose1 == "day" || $choose1 == "custom") 
		{
			$yAxisName = "SECONDS";
			$producationTime = sprintf('%0.2f',$producationTime);
			$noProducationTime = sprintf('%0.2f',$noProducationTime);
		}
		else if ($choose1 == "weekly") 
		{
			$yAxisName = "HOURS";
			$producationTime = $this->secondsToHours($producationTime);
			$noProducationTime = $this->secondsToHours($noProducationTime);
		}
		else if ($choose1 == "monthly") 
		{
			$yAxisName = "HOURS";	
			$producationTime = $this->secondsToHours($producationTime);
			$noProducationTime = $this->secondsToHours($noProducationTime);
		}
		else if ($choose1 == "yearly") 
		{
			$yAxisName = "DAY";
			$producationTime =  $this->secondsToDay($producationTime);
			$noProducationTime =  $this->secondsToDay($noProducationTime);
		}

		$result = array(
			"producationTime" => !empty($producationTime) ? $producationTime : "0",
			"noProducationTime" => !empty($noProducationTime) ? $noProducationTime : "0",
			"partCount" => $partCount,
			"yAxisName" => $yAxisName
		);
		echo json_encode($result);
	}

	//secondsToDay function use for convert seconds to day
	//In use
	function secondsToDay($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		return sprintf('%0.2f', ($seconds) / (3600 * 24));
	}

	//secondsToHours function use for convert seconds to hour
	//In use
	function secondsToHours($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.2f', $seconds/3600);
    }

    //In use
    function exportCsv()
    {
    	$IOName = !empty($this->input->get('IOName')) ?  $this->input->get('IOName') : "";
    	$dateValS = !empty($this->input->get('dateValS')) ? $this->input->get('dateValS') : "";
    	$dateValE = !empty($this->input->get('dateValE')) ? $this->input->get('dateValE') : "";
    	$signal = !empty($this->input->get('signal')) ?  $this->input->get('signal') : "";

    	$endDateValS = !empty($this->input->get('endDateValS')) ? $this->input->get('endDateValS') : "";
    	$endDateValE = !empty($this->input->get('endDateValE')) ? $this->input->get('endDateValE') : "";

		$result = $this->AM->getvirtualMachineLogLogsExport($IOName,$dateValS,$dateValE,$signal,$endDateValS,$endDateValE)->result_array(); 
		$data = array();
		foreach ($result as $key => $value) 
		{
			$data[] = array(
				"logId" => $value['logId'],
				"IOName" => $value['IOName'],
				"addedTime" => $value['addedTime'],
				"signalType" => ($value['signalType'] == "1") ? "High" : "Low",
				"originalTime" => $value['originalTime']
				);
		}
		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"VirtualMachine".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
		$handle = fopen('php://output', 'w');
        fputcsv($handle, array("Virtual machine"));
        $cnt=1;
        foreach ($data as $key) 
        {
        	if ($cnt == 1) 
        	{
        		$narray=array("Log ID ","IO Name","Added Time","Signal","Original time");
            	fputcsv($handle, $narray);
        	}
            $narray=array($key['logId'],$key["IOName"],$key["addedTime"],$key["signalType"],$key["originalTime"]);
            fputcsv($handle, $narray);
            $cnt++;
        }
        fclose($handle);
        exit;
	}
}
?>