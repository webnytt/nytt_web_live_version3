<?php

class CronDev extends CI_Controller
{
    
    function __construct()
    {
        parent::__construct();
        $this->load->model('Cron_model','CM');  
    }


    function testMail()
	{
		$config = array(
		  'protocol' => 'smtp',
		  'smtp_host' => smtp_host,
	      'smtp_user' => smtp_user,
	      'smtp_pass' => smtp_pass,
	      'smtp_port' => smtp_port,
		  'crlf' => "\r\n",
		  'newline' => "\r\n"
		);
		$this->load->library('email',$config);


		$this->email->from('root@nyttdev.com', 'Your Name');
		$this->email->to('renish.arshikweb@gmail.com');
		$this->email->subject('Email Test');
		$this->email->message('Testing the email class.');
		$this->email->send();

		echo $this->email->print_debugger();
	}

   

    public function repeatTask($factoryId)
    {
    	$this->factory = $this->load->database('factory'.$factoryId, TRUE);
    	$date = date('Y-m-d',strtotime("-1 days 2022-01-01"));
	    $result = $this->factory->where(array("isDelete" => "0","newTask" => "1","repeat !=" => "never","dueDate" => $date))->get('taskMaintenace')->result_array();

    	foreach ($result as $key => $value) 
    	{
    		$userIds = $value['userIds'];
			$machineId =  $value['machineId'];
			$task = $value['task'];
			$repeat = $value['repeat'];
			$createdByUserId = $value['createdByUserId'];

			$endTaskDate = $value['endTaskDate'];
			$EndAfteroccurances = $value['EndAfteroccurances'];
			$newTask = $value['newTask'];
			$dueWeekDay = $value['dueWeekDay'];
			$monthDueOn = $value['monthDueOn'];
			$dueMonthMonth = $value['dueMonthMonth'];
			$dueMonthWeek = $value['dueMonthWeek'];
			$dueMonthDay = $value['dueMonthDay'];
			$yearDueOn = $value['yearDueOn'];
			$dueYearMonth = $value['dueYearMonth'];
			$dueYearMonthDay = $value['dueYearMonthDay'];
			$dueYearWeek = $value['dueYearWeek'];
			$dueYearDay = $value['dueYearDay'];
			$dueYearMonthOnThe = $value['dueYearMonthOnThe'];

			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				if (!empty($dueWeekDay) && $newTask == "1") 
				{
					$startDate = date('Y-m-d',strtotime("next monday"));
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$startDate = date('Y-m-d');
					$day = date('l',strtotime($startDate));
					if ($day == "Friday") 
					{
						$startDate = date('Y-m-d',strtotime("next monday"));
					}
					$dueDate = date('Y-m-d',strtotime("next friday"));
				}
				$repeatOrder = 3;
			}elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-01');
				if ($newTask == "1") 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}

					if (date('m') == date('m',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 months".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));
					}
				}else
				{
					$dueDate = date('Y-m-t');
				}
				$repeatOrder = 4;
			}elseif ($repeat == "everyYear") 
			{
				if ($newTask == "1") 
				{
					
					if ($yearDueOn == "1") 
					{
						$monthYear = date('m',strtotime($dueYearMonth));
						if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
						}else
						{	
							$dueDate = date('Y-'.$monthYear.'-t');
							if ($dueYearMonthDay == "third_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueYearMonthDay == "second_last_day") 
							{
								$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
							}else
							{
								$dueDate = date('Y-m-t',strtotime($dueDate));
							}
						}
					}elseif ($yearDueOn == "2") 
					{
						if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
						{
							$monthYear = date('m',strtotime($dueYearMonthOnThe));
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
							if ($monthYear != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
							}
							$dueDate = $weekEndDate;
						}else
						{
							$dueDate = date('Y-m-d',strtotime("12/31"));
						}
					}

					$startDate = date('Y-'.$monthYear.'-01');

					if (date('Y') == date('Y',strtotime($value['dueDate']))) 
					{
						$startDate = date('Y-m-d',strtotime("+1 years".$startDate));
						$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));
					}
				}else
				{
					$startDate = date('Y-01-01');
					$dueDate = date('Y-12-t');	
				}
				$repeatOrder = 5;	
			}

			if (!empty($value['mainTaskId'])) 
			{
				$mainTaskId = $value['mainTaskId'];
			}else
			{
				$mainTaskId = $value['taskId'];
			}

            $data=array(
                'userIds' => $userIds,
                'machineId' => $machineId,  
                'task' => $task,  
                'repeat' => $repeat,
                'dueDate' => $dueDate,
                'startDate' => $startDate,
                'repeatOrder' => $repeatOrder,
                'mainTaskId' => $mainTaskId,
                'createdByUserId' => $createdByUserId,
                'newTask' => !empty($newTask) ? $newTask : NULL,
                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
                'endTaskDate' => !empty($endTaskDate) ? $endTaskDate : NULL,
                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
            );  

            $this->factory->insert('taskMaintenace',$data);
           // $this->factory->last_query();exit;
    	}
    }


    function getFirstandLastDate($year, $month, $week, $day) 
    {

	    $thisWeek = 1;

	    for($i = 1; $i < $week; $i++) {
	        $thisWeek = $thisWeek + 7;
	    }

	    $currentDay = date('Y-m-d',mktime(0,0,0,$month,$thisWeek,$year));

	    $monday = strtotime($day.' this week', strtotime($currentDay));;

	    $weekStart = date('Y-m-d', $monday);

	    return $weekStart;
	}
}