<?php 


defined('BASEPATH') OR exit('No direct script access allowed');

class DatabaseBackup extends CI_Controller 
{
	function __construct() 
	{
		parent::__construct();                        
		//$this->factory = $this->load->database('factory2', TRUE);  
		//$this->cloudFactory = $this->load->database('liveFactory5', TRUE);  
		//$this->load->library('encryption');
	}


	public function getLiveData()
	{
		$result = array(
							array('Addedby', 'Added by', 'Tillagd av', 'dashboard', NULL),
array('Createdon', 'Created on', 'Skapad den', 'dashboard', NULL),
array('Setapptimetaken', 'Setup time taken', 'Uppstart tidsåtgång', 'dashboard', NULL)
						);

			foreach ($result as $key => $value) 
			{
				$translations = $this->db->where('textIdentify',$value[0])->get('translations')->result_array();
				if (!empty($translations)) 
				{
					$this->db->set('type', $value[3])->where('textIdentify',$value[0])->update('translations');
				}else
				{
					$data  = array('textIdentify' => $value[0], 'englishText' => $value[1], 'swedishText' => $value[2], 'type' => $value[3]);
					
					$this->db->insert('translations',$data);
				}
				
			}

	
	}

/*
	function addBreakdownReason()
	{
		$breakdownReason = $this->db->get('breakdownReason')->result_array();

		foreach ($breakdownReason as $key => $value) 
		{
			$machine = $this->factory->where('isDeleted','0')->get('machine')->result_array();

			$position = ($value['breakdownReasonText'] == "Other") ? 10 : $key + 1;

			if ($value['breakdownReasonText'] == "Operator interference") 
			{
				$errorTypeId = 3;	
			}elseif ($value['breakdownReasonText'] == "Tool issue") 
			{
				$errorTypeId = 2;	
			}elseif ($value['breakdownReasonText'] == "Lack of material") 
			{
				$errorTypeId = 4;	
			}elseif ($value['breakdownReasonText'] == "Machine breakdown") 
			{
				$errorTypeId = 1;	
			}elseif ($value['breakdownReasonText'] == "Setup") 
			{
				$errorTypeId = 1;	
			}elseif ($value['breakdownReasonText'] == "Operator occupied") 
			{
				$errorTypeId = 3;	
			}elseif ($value['breakdownReasonText'] == "Other") 
			{
				$errorTypeId = 5;	
			}

			foreach ($machine as $machineKey => $machineValue) 
			{
				$data = array(
					"machineId" => $machineValue['machineId'],
					"errorTypeId" => $errorTypeId,
					"position" => $position,
					"breakdownReason" => $value['breakdownReasonText']
				);

				$this->factory->insert('machineBreakdownReason',$data);
				//echo $this->factory->last_query();exit;
			}
		}

		for ($i=7; $i <= 9; $i++) 
		{ 
			$machine = $this->factory->where('isDeleted','0')->get('machine')->result_array();

			foreach ($machine as $machineKey => $machineValue) 
			{
				$data = array(
					"machineId" => $machineValue['machineId'],
					"errorTypeId" => "",
					"position" => $i,
					"breakdownReason" => ""
				);

				$this->factory->insert('machineBreakdownReason',$data);
			}
		}
	}*/

/*	function encodePassword()
	{
		$result = $this->db->get('user')->result_array();

		//print_r($result);exit();
		foreach ($result as $key => $value) 
		{
			$userPassword = base64_encode($value['userPassword']);

			$this->db->where('userId', $value['userId']);
			$this->db->set('userPassword', $userPassword);
			$this->db->update('user');
		}
	}*/
	
	/*function encodeMachineCode()
	{
		$result = $this->db->get('machineCode')->result_array();

		//print_r($result);exit();
		foreach ($result as $key => $value) 
		{
			$encrypt_string = base64_encode($value['machineCode']);

			$this->db->where('machineCodeId', $value['machineCodeId']);
			$this->db->set('machineCode', $encrypt_string);
			$this->db->update('machineCode');
		}
	}

	function encodeFactoryDetails()
	{
		$result = $this->db->get('factory')->result_array();

		//echo "<pre>";print_r($result);exit();
		foreach ($result as $key => $value) 
		{
			$factoryName = base64_encode($value['factoryName']);
			$factoryLocation = base64_encode($value['factoryLocation']);

			$this->db->where('factoryId', $value['factoryId']);
			$this->db->set('factoryName', $factoryName);
			$this->db->set('factoryLocation', $factoryLocation);
			$this->db->update('factory');
		}
	} */


/*
	function activeMachine()
	{
		$liveData = $this->factory->get('activeMachine')->result_array();

		$this->cloudFactory->insert_batch('activeMachine',$liveData);
	}

	function machine()
	{
		$liveData = $this->factory->get('machine')->result_array();
		
		$this->cloudFactory->insert_batch('machine',$liveData);
	}

	function machineStateColorLookup()
	{
		$liveData = $this->factory->get('machineStateColorLookup')->result_array();
		
		$this->cloudFactory->insert_batch('machineStateColorLookup',$liveData);
	}

	function machineStatePeriodLookup()
	{
		$liveData = $this->factory->get('machineStatePeriodLookup')->result_array();
		
		$this->cloudFactory->insert_batch('machineStatePeriodLookup',$liveData);
	}

	function machineUserv2()
	{
		$liveData = $this->factory->get('machineUserv2')->result_array();
		
		$this->cloudFactory->insert_batch('machineUserv2',$liveData);
	}

	function mNotificationv2()
	{
		$liveData = $this->factory->get('mNotificationv2')->result_array();
		
		$this->cloudFactory->insert_batch('mNotificationv2',$liveData);
	}

	function noInternetLog()
	{
		$liveData = $this->factory->get('noInternetLog')->result_array();
		
		$this->cloudFactory->insert_batch('noInternetLog',$liveData);
	}

	function notification()
	{
		$liveData = $this->factory->get('notification')->result_array();
		
		$this->cloudFactory->insert_batch('notification',$liveData);
	}

	function reportedImages()
	{
		$liveData = $this->factory->get('reportedImages')->result_array();
		
		$this->cloudFactory->insert_batch('reportedImages',$liveData);
	}

	function setAppLive()
	{
		$liveData = $this->factory->get('setAppLive')->result_array();
		
		$this->cloudFactory->insert_batch('setAppLive',$liveData);
	}

	function tempLogImageFrame()
	{
		$liveData = $this->factory->get('tempLogImageFrame')->result_array();
		
		$this->cloudFactory->insert_batch('tempLogImageFrame',$liveData);
	}


	function phoneDetailLogv2()
	{
		$liveData = $this->factory->get('phoneDetailLogv2')->result_array();
		
		$this->cloudFactory->insert_batch('phoneDetailLogv2',$liveData);
	}
*/
}
