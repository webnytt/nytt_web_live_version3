<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Devlopment extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->model('Devlopment_model','APIM');
		set_time_limit(120);
    }
	
 	function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 

	function getFirstandLastDate($year, $month, $week, $day) {

	    $thisWeek = 1;

	    for($i = 1; $i < $week; $i++) {
	        $thisWeek = $thisWeek + 7;
	    }

	    $currentDay = date('Y-m-d',mktime(0,0,0,$month,$thisWeek,$year));

	    $monday = strtotime($day.' this week', strtotime($currentDay));;

	    $weekStart = date('Y-m-d', $monday);

	    return $weekStart;
	}

    public function addTaskRepeat_post()
	{
	    $this->form_validation->set_rules('userIds','userIds','required');
        $this->form_validation->set_rules('task','task','required');
        $this->form_validation->set_rules('repeat','repeat','required');
        $this->form_validation->set_rules('userId','userId','required');
        $this->form_validation->set_rules('factoryId','factoryId','required');
        $this->form_validation->set_rules('machineId','machineId','required');

        $repeat = $this->input->post('repeat');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDate','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDay','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOn');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonth','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeek','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDay','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOn');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonth','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDay','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeek','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDay','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnThe','dueYearMonthOnThe','required');
        	}
        }

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			//echo json_encode($_POST);exit;
			$userIds = $this->input->post('userIds');
			$taskId = $this->input->post('taskId');
			$machineId =  $this->input->post('machineId');
			$task = $this->input->post('task');

			$endTaskDate = $this->input->post('endTaskDate');
			$EndAfteroccurances = $this->input->post('EndAfteroccurances');

			$dueWeekDay = $this->input->post('dueWeekDay');
			$monthDueOn = $this->input->post('monthDueOn');
			$dueMonthMonth = $this->input->post('dueMonthMonth');
			$dueMonthWeek = $this->input->post('dueMonthWeek');
			$dueMonthDay = $this->input->post('dueMonthDay');
			$yearDueOn = $this->input->post('yearDueOn');
			$dueYearMonth = $this->input->post('dueYearMonth');
			$dueYearMonthDay = $this->input->post('dueYearMonthDay');
			$dueYearWeek = $this->input->post('dueYearWeek');
			$dueYearDay = $this->input->post('dueYearDay');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnThe');

			//Set task start end date as par repeat filter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}
				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDate')));
				$repeatOrder = 1;
			}


			if (!empty($taskId)) 
            {
            	$updatedByUserId = $userId;
            }else
            {
            	$updatedByUserId = 0;
            }

           

            if (!empty($endTaskDate) && $repeat != "never") 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances) && $repeat != "never") 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." month"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." year"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			if ($insertVariable == true) 
			{

				$data=array(
	                'userIds' => $userIds,
	                'machineId' => $machineId,  
	                'task' => $task,  
	                'repeat' => $repeat,
	                'dueDate' => $dueDate,
	                'startDate' => $startDate,
	                'repeatOrder' => $repeatOrder,
	                'createdByUserId' => $userId,
	                'updatedByUserId' => $updatedByUserId,
	                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
	                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
	                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
	                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
	                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
	                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
	                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
	                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
	                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
	                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
	                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
	                'endTaskDate' => !empty($endTaskDate) ? $endTaskDate : NULL,
					'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
	            );  


	            if (empty($taskId)) 
	            {
	            	$data['newTask'] = "1";
	            	$this->APIM->insertFactoryData($factoryId, $data, "taskMaintenace");
	            	$json =  array("status" => "1","message" => "Task added successfully","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	            }else
	            {
	            	$where = array("taskId" => $taskId);
	            	$this->APIM->updateFactoryData($factoryId, $where,$data, "taskMaintenace");


	            	$lastSubTask = $this->AM->getLastSubTask($taskId);
					if (!empty($lastSubTask)) 
					{
						$this->AM->updateData(array("taskId" => $lastSubTask->taskId),$data, "taskMaintenace");
					}

	            	$json =  array("status" => "1","message" => "Task updated successfully","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	            }
	        }else
	        {
	        	$json =  array("status" => "0","message" => "End task date cannot be less than due date","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	        }


				
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }



}
?>
