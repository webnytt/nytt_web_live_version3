<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class virtualMachine extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->model('Api_model','APIM');
    }

    public function addVirtualMachineLog_post()
	{
		$input_data = json_decode(trim(file_get_contents('php://input')), true);
		
	    $factoryId = $input_data['factoryId'];
		$IOName = $input_data['IOName'];
		$signal = $input_data['signal'];
		
		$seconds = $input_data['addedTime'] / 1000;
		$addedTime = date('Y-m-d H:i:s',$seconds);

		
		if (empty($factoryId) || empty($IOName) || empty($addedTime)) 
		{
			$json =  array("status" => "0","message"=> "Please enter required field data ");
		}
		else if ($factoryId == 1 || $factoryId == 2 || $factoryId == 3 || $factoryId == 4 || $factoryId == 5)
		{	
			$lastRecord = $this->APIM->virtualMachineLogLastRecord($factoryId,$IOName);

			$startDate  = strtotime($lastRecord->addedTime);
			$endDate = strtotime($addedTime);
			$duration = $endDate - $startDate;
	    	$addField = array(
					"IOName" =>  $IOName, 
					"signalType" =>  "$signal", 
					"addedTime" =>  $addedTime,
					"duration" => !empty($lastRecord) ? $duration : "0"
					);	 

            $this->APIM->insertFactoryData($factoryId,$addField,"virtualMachineLog");



	    	$json =  array("status" => "1","message"=> "IO data added successfully");
		    
		}else
		{
			$json =  array("status" => "0","message"=> "Please enter valid factoryId");
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }


    public function virtualMachineLogList_post()
	{
	    $this->form_validation->set_rules('IOName', 'IOName', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('startDate', 'startDate', 'required');
	    $this->form_validation->set_rules('endDate', 'endDate', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0]);
		}
		else
		{   	
			$factoryId = $this->input->post('factoryId');
			$IOName = $this->input->post('IOName');
			$startDate = $this->input->post('startDate');
			$endDate = $this->input->post('endDate');

			$this->factory = $this->load->database('factory'.$factoryId, TRUE);

			$this->factory->select('logId,IOName,addedTime,signalType');
			$this->factory->where(array("IOName" => $IOName,"date(addedTime) >=" => $startDate,"date(addedTime) <=" => $endDate));
			$this->factory->order_by("logId","desc");
			$result = $this->factory->get('virtualMachineLog')->result_array(); 

			foreach ($result as $key => $value) 
			{
				$result[$key]['signalType'] = ($value['signalType'] == "1") ? "Active" : "Not active"; 
			}

			$json =  array("status" => "1","data" => $result);

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }
	
}