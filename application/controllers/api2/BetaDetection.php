<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class BetaDetection extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->model('Api_model','APIM');
		ini_set('memory_limit', '-1');
    }

    //applicationCrashLog api use for upload txt file crash log
    public function betaDetectionLog_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('startDate', 'startDate', 'required');
	    $this->form_validation->set_rules('endDate', 'endDate', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0]);
		}
		else
		{   	
			$factoryId = $this->input->post('factoryId');
			$machineId = $this->input->post('machineId');
			$startDate = $this->input->post('startDate');
			$endDate = $this->input->post('endDate');

			$this->factory = $this->load->database('factory'.$factoryId, TRUE);

            $machineQ = "and betaDetectionLog.machineId = $machineId";  
            $machineStatusQ = "and machineStatusLog.machineId = $machineId";

			$dateQ = " and originalTime BETWEEN '".$startDate."' and '".$endDate."' "; 

        	$dateStatusQ = " and insertTime BETWEEN '".$startDate."' and '".$endDate."' "; 

			$sql = "SELECT * FROM
            (SELECT logId, machineId, originalTime, color FROM betaDetectionLog where 1=1   $machineQ $dateQ UNION ALL 
            SELECT logId, machineId, insertTime, machineStatus FROM machineStatusLog  where 1=1 $machineStatusQ $dateStatusQ)betaTable  left join machine on betaTable.machineId = machine.machineId order by betaTable.originalTime desc,betaTable.logId desc";   
           // echo $sql;exit;
            
        	$listColorLogs = $this->factory->query($sql)->result(); 

        	$removeArrayKey = array();

        	$result = array();
        	$rT = 1;
        	for ($key=0;$key<count($listColorLogs);$key++) 
			{
				if ($listColorLogs[$key]->color == "NoData") 
				{

					$seconds = strtotime($listColorLogs[$key]->originalTime) - strtotime($listColorLogs[$key + 1]->originalTime);
					if ($seconds <= 30) 
					{
						$removeArrayKey[] = $key;
					}
				}

				$listColorLogs[$key]->machineStatus = "Production";
				$colorS = explode(" ", $listColorLogs[$key]->color);
				$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
				$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
				$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
				$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
				$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
				$offStatus = (in_array("off", $colorS)) ? "1" : "0";
				$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";


				$machineStateColorLookup = $this->factory->where(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal))->get('machineStateColorLookup')->row();
				
				if (!empty($machineStateColorLookup)) 
				{
					
					if ($NoDataStatus == "1") 
					{
						$listColorLogs[$key]->machineStatus = "nodet";
					}else if($offStatus == "1")
					{
						$listColorLogs[$key]->machineStatus = "Off";
					}else
					{
						$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;

					}
				}

				if ($listColorLogs[$key]->color == "0") 
				{
					$listColorLogs[$key]->machineStatus = "NoData";
					$listColorLogs[$key]->color = "NoData";
				}
				else if ($listColorLogs[$key]->color == "2") 
				{
					$listColorLogs[$key]->machineStatus = "No production";
					$listColorLogs[$key]->color = "NoData";
				}else if ($listColorLogs[$key]->color == "1") 
				{
					$listColorLogs[$key]->machineStatus = "Setup";
					$listColorLogs[$key]->color = "NoData";
				}

					$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;


					$result[$key]['logId'] = $rT; 
					$result[$key]['machineId'] = $listColorLogs[$key]->machineId; 
					$result[$key]['machineName'] = $listColorLogs[$key]->machineName; 
					$result[$key]['color'] = $listColorLogs[$key]->color; 
					$result[$key]['machineStatus'] = $listColorLogs[$key]->machineStatus; 
					$result[$key]['originalTime'] = $listColorLogs[$key]->originalTime; 

					$rT++;
			}



			$result = $this->array_except($result, $removeArrayKey);
			$result = array_values($result);

			$json =  array("status" => "1","data" => $result);

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 

    
	
}