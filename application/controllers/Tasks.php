<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Tasks extends CI_Controller {
	
	var $factory;
	var $factoryId;
	
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) {
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

		// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}
		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);
	}

	

	//In use
	function getFirstandLastDate($year, $month, $week, $day) {

	    $thisWeek = 1;

	    for($i = 1; $i < $week; $i++) {
	        $thisWeek = $thisWeek + 7;
	    }

	    $currentDay = date('Y-m-d',mktime(0,0,0,$month,$thisWeek,$year));

	    $monday = strtotime($day.' this week', strtotime($currentDay));;

	    $weekStart = date('Y-m-d', $monday);

	    return $weekStart;
	}


	//taskMaintenance function use for get task & maintence page 

	//In use
	function taskMaintenance()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$accessPoint = $this->AM->accessPoint(21);
		if ($accessPoint == true) 
        {
			$data['accessPointEdit'] = $this->AM->accessPointEdit(21);
			$machines = $this->AM->getallMachines($this->factory)->result_array();
			$data['listMachines'] = $this->AM->getallMachines($this->factory);

			foreach ($machines as $key => $value) 
			{
				$machines[$key]['totalTask'] = $this->AM->getWhereCount(array("isDelete" => "0","machineId" => $value['machineId']),"taskMaintenace");
				$machines[$key]['totalCompletedTask'] = $this->AM->getWhereCount(array("isDelete" => "0","status" => "completed","machineId" => $value['machineId']),"taskMaintenace");
			}

			$data['machines'] = $machines;
			$data['totalTask'] = $this->AM->getWhereCount(array("isDelete" => "0"),"taskMaintenace");
			$data['totalCompletedTask'] = $this->AM->getWhereCount(array("isDelete" => "0","status" => "completed"),"taskMaintenace");
			$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('taskMaintenace',$data);
			$this->load->view('footer');		
			$this->load->view('script_file/task_maintenace_footer',$data);		
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	//task_maintenace_pagination function use for get task data 
	public function task_maintenace_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		
		// print_r($this->input->post());exit;

		$userIds = $this->input->post('userId');
		$repeat = $this->input->post('repeat');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$dueDateValS = $this->input->post('dueDateValS');
		$dueDateValE = $this->input->post('dueDateValE');
		$status = $this->input->post('status');
		$machineIds = $this->input->post('machineIds');
		$machineId = $this->input->post('machineId');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		// print_r($dueDateValS);exit;
		// print_r($dueDateValE);exit;


		//Get all data count
		$listDataLogsCount = $this->AM->getallTaskMaintenaceCount($this->factory, $machineIds, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchTaskMaintenaceCount($this->factory, $userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getallTaskMaintenaceLogs($this->factory,$row,$rowperpage,$userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)->result_array();  

		$removeArrayKey = array();

		foreach ($listDataLogs as $key => $value) 
		{
			$userIds = explode(",", $value['userIds']);

			$operators = "";
			foreach ($userIds as $userKey => $userValue) 
			{
				$users = $this->AM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
				if (!empty($users)) 
				{
					$operators .= '<img style="border-radius: 50%;margin-top: -3px;" width="16" height="16" src="'. base_url('assets/img/user/'.$users->userImage) .'">&nbsp;'. $users->userName."<br>";
				}
			}

			$listDataLogs[$key]['operators'] = $operators;
			

			$lastSubTask = $this->AM->getLastSubTask($value['taskId']);
			if (!empty($lastSubTask)) 
			{ 
				if (!empty($status)) 
				{
					if (in_array("'".$lastSubTask->status."'", $status)) 
					{
						$listDataLogs[$key]['status'] = ($lastSubTask->status == "uncompleted") ? Uncompleted : Completed;
						$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($lastSubTask->dueDate));	
					}
					else
					{
						$removeArrayKey[] = $key;
					}
				}
				else
				{
					$listDataLogs[$key]['status'] = ($lastSubTask->status == "uncompleted") ? Uncompleted : Completed;
					$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($lastSubTask->dueDate));	
				}
				
			}
			else
			{
				$listDataLogs[$key]['status'] = ($value['status'] == "uncompleted") ? Uncompleted : Completed;
				$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($value['dueDate']));
			}

			$listDataLogs[$key]['createdDate'] = date('Y-m-d',strtotime($value['createdDate']));



			$taskCount = strlen($value['task']);
			$listDataLogs[$key]['task'] = ($taskCount > 30) ? substr($value['task'],0,30).'...' : $value['task'];
			$listDataLogs[$key]['delete'] = '<a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 9px;"  onclick="delete_task('.$value['taskId'].')" href="javascript:void(0);" class="btn btn-primary">
				<img style="width: 23px;height: 23px;" src="' . base_url("assets/nav_bar/delete_white.svg") . '"></a>';		

			if ($value['repeat'] == "everyYear") 
			{
				$repeat = Everyyear;
			}
			elseif ($value['repeat'] == "everyMonth") 
			{
				$repeat = Everymonth;
			}
			elseif ($value['repeat'] == "everyWeek") 
			{
				$repeat = Everyweek;
			}
			elseif ($value['repeat'] == "everyDay") 
			{
				$repeat = Everyday;
			}
			else
			{
				$repeat = Never;
			}
			$listDataLogs[$key]['repeat'] = $repeat;
		}

		$listDataLogs = $this->array_except($listDataLogs, $removeArrayKey);
		$listDataLogs = array_values($listDataLogs);

		//print_r($listDataLogs);exit;
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}


	function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 

	function getEditTask()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$taskId = $this->input->post('taskId');

		$result = $this->AM->getTaskById($this->factory,$taskId);
		if (!empty($result->endTaskDate)) 
		{
			$result->endTaskDate = date('m/d/Y',strtotime($result->endTaskDate));
		}

		if (!empty($result->dueDate)) 
		{
			$result->dueDate = date('m/d/Y',strtotime($result->dueDate));
		}
		echo json_encode($result); die;
	}
	
	//getTaskById use for get task data by task id
	function getTaskById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$taskId = $this->input->post('taskId');
		$result = $this->AM->getTaskById($this->factory,$taskId);

		$subTask = $this->AM->getWhereWithOrderBy(array("mainTaskId" => $taskId),"taskId","desc","taskMaintenace");

		$statusTask = $result->status;
		$result->createdDate = date('Y-m-d',strtotime($result->createdDate));
		
		$result->status = ""; 
		foreach ($subTask as $key => $value) 
		{	
			$completed = ($value['status'] == "completed") ? "checked" : "";
			$statusName = ($value['status'] == "completed") ? Completed : Uncompleted;
			if ($value['repeat'] == "everyYear") 
			{
				$repeatName = "Every year";
				if (date('Y',strtotime($value['dueDate'])) != date('Y') || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			elseif ($value['repeat'] == "everyMonth") 
			{
				$repeatName = "Every month";
				if ((date('Y',strtotime($value['dueDate'])) != date('Y') && date('m',strtotime($value['dueDate'])) != date('m')) || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			elseif ($value['repeat'] == "everyWeek") 
			{
				$repeatName = "Every week";

				$currentDate = date('Y-m-d');
				$dayTask = date('l',strtotime($currentDate));
				if ($dayTask == "Saturday" || $dayTask == "Sunday") 
				{
					$currentDate = date('Y-m-d',strtotime("next monday"));
				}
				
				$compareWeekCheck = date('W',strtotime($value['dueDate']));
 				$currentWeek = date('W',strtotime($currentDate));

				if ($currentWeek == $compareWeekCheck) 
				{
					if ($value['status'] == "completed") {
						$disabledTask = "disabled";
					}
					else
					{
						$disabledTask = "";
					}
				}
				else
				{
					$disabledTask = "disabled";
				}


			}
			elseif ($value['repeat'] == "everyDay") 
			{
				$repeatName = "Every day";
				if ($value['dueDate'] != date('Y-m-d') || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			else
			{
				$repeatName = "Never";
				$disabledTask = "disabled";
			}
			$dueDate = $value['dueDate'];
			$result->status .= '<div class="col-md-12" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline"><input  id="taskIdCheck'.$value['taskId'].'" onclick="completeUncompleteTask('.$value['taskId'].')" '. $completed .' class="form-check-input" type="checkbox" name="taskStatus[]" '.$disabledTask.'  value="'.$value['taskId'].'"><label class="form-check-label" for="completed" style="font-size: 80% !important;">'. $statusName .' '. $dueDate .' '.'('. $repeatName .')</label></div></div>';


		}


		$completed = ($statusTask == "completed") ? "checked" : "";
		$statusName = ($statusTask == "completed") ? Completed : Uncompleted;

		if ($result->repeat == "everyYear") 
		{
			$repeatName = Everyyear;
			if (date('Y',strtotime($result->dueDate)) != date('Y') || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		elseif ($result->repeat == "everyMonth") 
		{
			$repeatName = Everymonth;
			if ((date('Y',strtotime($result->dueDate)) != date('Y') && date('m',strtotime($result->dueDate)) != date('m')) || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		elseif ($result->repeat == "everyWeek") 
		{
			$repeatName = Everyweek;

			$currentDate = date('Y-m-d');
			$dayTask = date('l',strtotime($currentDate));
			if ($dayTask == "Saturday" || $dayTask == "Sunday") 
			{
				$currentDate = date('Y-m-d',strtotime("next monday"));
			}
			
			$compareWeekCheck = date('W',strtotime($result->dueDate));
			$currentWeek = date('W',strtotime($currentDate));

			if ($currentWeek == $compareWeekCheck) 
			{
				if ($statusTask == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			else
			{
				$disabledTask = "disabled";
			}
		}
		elseif ($result->repeat == "everyDay") 
		{
			$repeatName = Everyday;

			if ($result->dueDate != date('Y-m-d') || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		else
		{
			$repeatName = Never;

			if ($result->dueDate < date('Y-m-d')) 
			{
				$disabledTask = "disabled";
			}
			else
			{
				if ($statusTask == "completed") 
				{
					$disabledTask = "disabled";
				}else
				{
					$disabledTask = "";
				}
			}
		}




		$result->status .= '<div class="col-md-12" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline"><input id="taskIdCheck'.$result->taskId.'" onclick="completeUncompleteTask('.$result->taskId.')" '. $completed .' class="form-check-input" type="checkbox" name="taskStatus[]" '.$disabledTask.' value="'.$result->taskId.'"><label class="form-check-label" for="completed" style="font-size: 80% !important;">'. $statusName .' '. $result->dueDate .' '.'('. $repeatName .')</label></div></div>';

		$never = ($result->repeat == "never") ? "checked" : "";
		$neverLabel = ($result->repeat == "never") ? "#FF8000" : "#1f2225";
		$everyDay = ($result->repeat == "everyDay") ? "checked" : "";
		$everyDayLabel = ($result->repeat == "everyDay") ? "#FF8000" : "#1f2225";
		$everyWeek = ($result->repeat == "everyWeek") ? "checked" : "";
		$everyWeekLabel = ($result->repeat == "everyWeek") ? "#FF8000" : "#1f2225";
		$everyMonth = ($result->repeat == "everyMonth") ? "checked" : "";
		$everyMonthLabel = ($result->repeat == "everyMonth") ? "#FF8000" : "#1f2225";
		$everyYear = ($result->repeat == "everyYear") ? "checked" : "";
		$everyYearLabel = ($result->repeat == "everyYear") ? "#FF8000" : "#1f2225";

		$result->repeatData = '<div class="col-md-4" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$never.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="never" value="never"><label class="form-check-label repeatAll" for="never" id="neverLabel" style="font-size: 80% !important;color:'.$neverLabel.'">Never</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyDay.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyDay" value="everyDay"><label class="form-check-label repeatAll" for="everyDay" id="everyDayLabel" style="font-size: 80% !important;color:'.$everyDayLabel.'">Every day</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyWeek.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyWeek" value="everyWeek"><label class="form-check-label repeatAll" for="everyWeek" id="everyWeekLabel" style="font-size: 80% !important;color:'.$everyWeekLabel.'">Every week</label></div></div><div class="col-md-5" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyMonth.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyMonth" value="everyMonth"><label class="form-check-label repeatAll" for="everyMonth" id="everyMonthLabel" style="font-size: 80% !important;color:'.$everyMonthLabel.'">Every month</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyYear.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyYear" value="everyYear"><label class="form-check-label repeatAll" for="everyYear" id="everyYearLabel" style="font-size: 80% !important;color:'.$everyYearLabel.'">Every year</label></div></div>';

		$users = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
		$userIds = explode(",", $result->userIds);
		$checkedAll = (count($userIds) == count($users)) ? "checked" : "";
		$checkedColorAll = (count($userIds) == count($users)) ? "#FF8000" : "#1f2225";
		$operatorsHtml = "";
		
		foreach ($users as $key => $value) 
		{
			$checked = (in_array($value['userId'], $userIds)) ? "checked" : "";
			$checkedColor = (in_array($value['userId'], $userIds)) ? "#FF8000" : "#1f2225";
			if ($checked == "checked") 
			{
				$operatorsHtml .= '<div style="float: left;padding-left: 10px;padding-top: 5px;"><img style="border-radius: 50%;" width="20" height="20" src="'. base_url('assets/img/user/').$value['userImage'].'">'.$value['userName'].'</div>';
			}
		} 
		$result->userIds = $operatorsHtml;
		$result->repeatName = $repeatName;
		if (!empty($subTask)) 
		{
			$result->dueDate = $subTask[0]['dueDate'];
		}
		echo json_encode($result); die;
	}

	//add_task function use for add new task
	public function add_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('userIds','userIds','required');
        $this->form_validation->set_rules('task','task','required');
        $this->form_validation->set_rules('repeat','repeat','required');

        $repeat = $this->input->post('repeat');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDate','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDay','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOn');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonth','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeek','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDay','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOn');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonth','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDay','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeek','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDay','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnThe','dueYearMonthOnThe','required');
        	}
        }
        //checking required parameters validation
		$machineId =  $this->input->post('machineId');
        if($this->form_validation->run()==false || empty($machineId)) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforaddtask);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$userIds = $this->input->post('userIds');
			$task = $this->input->post('task');
			$repeat = $this->input->post('repeat');
			
			$endTaskDate = $this->input->post('endTaskDate');
			$EndAfteroccurances = $this->input->post('EndAfteroccurances');

			$dueWeekDay = $this->input->post('dueWeekDay');
			$monthDueOn = $this->input->post('monthDueOn');
			$dueMonthMonth = $this->input->post('dueMonthMonth');
			$dueMonthWeek = $this->input->post('dueMonthWeek');
			$dueMonthDay = $this->input->post('dueMonthDay');
			$yearDueOn = $this->input->post('yearDueOn');
			$dueYearMonth = $this->input->post('dueYearMonth');
			$dueYearMonthDay = $this->input->post('dueYearMonthDay');
			$dueYearWeek = $this->input->post('dueYearWeek');
			$dueYearDay = $this->input->post('dueYearDay');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnThe');
			
			//Set task start and end date as par repeat fiter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}

				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDate')));
				$repeatOrder = 1;
			}

			if (!empty($endTaskDate)) 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances)) 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." month"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." year"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			if ($insertVariable == true) 
			{
				$taskIds = array();
				foreach ($machineId as $key => $value) 
				{ 
		            $data=array(
		                'userIds' => $userIds,
		                'machineId' => $value,  
		                'task' => $task,  
		                'repeat' => $repeat,
		                'startDate' => $startDate,
		                'dueDate' => $dueDate,
		                'newTask' => '1',
		                'repeatOrder' => $repeatOrder,
		                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
		                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
		                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
		                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
		                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
		                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
		                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
		                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
		                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
		                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
		                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
		                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
		                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
		                'createdByUserId' => $this->session->userdata('userId')
		            );  
					$taskIds[] = $this->AM->insertData($data, "taskMaintenace");
		        }

		        if (!empty($taskIds)) 
		        {
		        	//Send task data to opapp update in as par assign machine and operator
		        		$select = "taskMaintenace.taskId,
				        		   taskMaintenace.createdByUserId,
				        		   taskMaintenace.task,
				        		   taskMaintenace.repeat,
				        		   taskMaintenace.status,
				        		   taskMaintenace.dueDate,
				        		   taskMaintenace.userIds,
				        		   taskMaintenace.machineId,
				        		   taskMaintenace.dueWeekDay,
				        		   taskMaintenace.monthDueOn,
				        		   taskMaintenace.dueMonthMonth,
				        		   taskMaintenace.dueMonthWeek,
				        		   taskMaintenace.dueMonthDay,
				        		   taskMaintenace.yearDueOn,
				        		   taskMaintenace.dueYearMonth,
				        		   taskMaintenace.dueYearMonthDay,
				        		   taskMaintenace.dueYearWeek,
				        		   taskMaintenace.dueYearDay,
				        		   taskMaintenace.dueYearMonthOnThe,
				        		   taskMaintenace.endTaskDate,
				        		   taskMaintenace.EndAfteroccurances,
				        		   taskMaintenace.newTask
				        		   ";
					$whereIn = array("taskId" => $taskIds);
		        	$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

		        	foreach ($taskData as $key => $value) 
		            {
		            	$taskData[$key]['type'] = 'add';
		            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
		            	$userIds = explode(",", $value['userIds']);

						$taskData[$key]['isLock'] = 1;
						foreach ($userIds as $userKey => $userValue) 
						{
							$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
							$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);

							$taskData[$key]['userDeleteId'] = array();
							$taskData[$key]['userAddId'] = array();
						}
		            }

		        	$json = array("status" => "1","taskData" => $taskData,"message" => Taskaddedsuccessfully);
		        }
		        else
		        {
		        	$json = array("status" => "0","message" => Errorwhileaddingtask);
		        }
		    }else
		    {
		    	$json = array("status" => "0","message" => Endtaskdatecannotbelessthanduedate);
		    }
	        echo json_encode($json); die;
        }
		
	}

	//update_task function use for update task
	public function update_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('task','task','required');
        $count = count($this->input->post('userIds'));
		//checking required parameters validation
        if($this->form_validation->run()==false || $count == 0) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
            echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('taskId');
			$task = $this->input->post('task');
			$repeat = $this->input->post('repeat');
			$taskStatus = !empty($this->input->post('taskStatus')) ? $this->input->post('taskStatus') : array();
			$userIds = implode(",", $this->input->post('userIds'));

			if (!empty($dueDate)) 
			{
	            $data=array(
	                'userIds' =>str_replace("all,", "", $userIds),  
	                'task' => $task,  
	                'repeatOrder' => $repeatOrder,
	                'updatedByUserId' => $this->session->userdata('userId')
	            );  
			}
			else
			{
				 $data=array(
	                'userIds' =>str_replace("all,", "", $userIds),  
	                'task' => $task,  
	                'repeatOrder' => $repeatOrder,
	                'updatedByUserId' => $this->session->userdata('userId')
	            ); 
			}

            $this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");
			foreach ($taskStatus as $keyStatus => $valueStatus) 
            {
            	$this->AM->updateData(array("taskId" => $valueStatus),array("status" => "completed"), "taskMaintenace");
            }

			$taskDetail = $this->AM->getWhereSingle(array("taskId" => $taskId), "taskMaintenace");
			//Update task title as par main task
			if (!empty($taskDetail->mainTaskId)) 
        	{
        		$updateTaskData=array(  
	                'task' => $task
	            );  

	            $this->AM->updateData(array("mainTaskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");
				$this->AM->updateData(array("taskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");
        	}
        	else
        	{
        		$updateTaskData=array(  
	                'task' => $task
	            );  
			    $this->AM->updateData(array("mainTaskId" => $taskId),$updateTaskData, "taskMaintenace");
        	}

    	   //Send task data to opapp update in as par assign machine and operator
		   $select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId";

        	$whereIn = array("taskId" => array($taskId));
			$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

        	foreach ($taskData as $key => $value) 
            {
            	$taskData[$key]['type'] = 'update';
            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
            	$userIds = explode(",", $value['userIds']);

				$taskData[$key]['isLock'] = 1;
				foreach ($userIds as $userKey => $userValue) 
				{
					$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
					$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);

					$taskData[$key]['userDeleteId'] = array();
					$taskData[$key]['userAddId'] = array();
				}
            }
			$json = array("status" => "1","taskData" => $taskData,"message" => Taskupdatedsuccessfully);
        	echo json_encode($json); die; 
		}
	}

	//delete_task function use for delete task
	public function delete_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('deleteTaskId','deleteTaskId','required');
        $this->form_validation->set_rules('deleteTaskId','deleteTaskId','required');
        //checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
	        echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('deleteTaskId');
			$data=array(
                'isDelete' => "1"
            );  
            //Delete task
			$this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");

			$this->AM->updateData(array("mainTaskId" => $taskId),$data, "taskMaintenace");
			
            $select = "taskMaintenace.taskId,
	        		   taskMaintenace.createdByUserId,
	        		   taskMaintenace.task,
	        		   taskMaintenace.repeat,
	        		   taskMaintenace.status,
	        		   taskMaintenace.dueDate,
	        		   taskMaintenace.userIds,
	        		   taskMaintenace.machineId,
	        		   taskMaintenace.dueWeekDay,
	        		   taskMaintenace.monthDueOn,
	        		   taskMaintenace.dueMonthMonth,
	        		   taskMaintenace.dueMonthWeek,
	        		   taskMaintenace.dueMonthDay,
	        		   taskMaintenace.yearDueOn,
	        		   taskMaintenace.dueYearMonth,
	        		   taskMaintenace.dueYearMonthDay,
	        		   taskMaintenace.dueYearWeek,
	        		   taskMaintenace.dueYearDay,
	        		   taskMaintenace.dueYearMonthOnThe,
	        		   taskMaintenace.endTaskDate,
	        		   taskMaintenace.EndAfteroccurances,
	        		   taskMaintenace.newTask
	        		   ";
			$whereIn = array("taskId" => array($taskId));
			$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");
			
			foreach ($taskData as $key => $value) 
            {
            	$taskData[$key]['type'] = 'delete';
            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');

            	$taskData[$key]['userDeleteId'] = array();
				$taskData[$key]['userAddId'] = array();
            }

            $json = array("status" => "1","message" => Taskdeletedsuccessfully,"taskData" => $taskData);
	        echo json_encode($json); die; 
		}
	}

	//get_due_date function use for get next or previous due date
	public function get_due_date()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('type','type','required');
		$this->form_validation->set_rules('dueDate','dueDate','required');

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo "Please fill valid details for update task."; die;
        } 
        else 
        {   
			$type = $this->input->post('type');
			$dueDate = $this->input->post('dueDate');
			
			if ($type == "plus") 
			{
				//Get next due date 
				$dueDate = date('m/d/Y',strtotime($dueDate." +1 days"));
			}else
			{
				//Get previous due date 
				$dueDate = date('m/d/Y',strtotime($dueDate." -1 days"));
			}
            echo $dueDate; die; 

        }
	}


	//ecit_task function use for edit task
	public function edit_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('userIdsEdit','userIds','required');
        $this->form_validation->set_rules('taskEdit','task','required');
        $this->form_validation->set_rules('repeatEdit','repeat','required');
        $this->form_validation->set_rules('taskIdEdit','taskId','required');

        $repeat = $this->input->post('repeatEdit');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDateEdit','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDayEdit','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOnEdit');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonthEdit','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeekEdit','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDayEdit','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOnEdit');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonthEdit','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDayEdit','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeekEdit','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDayEdit','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnTheEdit','dueYearMonthOnThe','required');
        	}
        }
        //checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforedittask);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$taskId = $this->input->post('taskIdEdit');
        	$userIds = $this->input->post('userIdsEdit');
			$task = $this->input->post('taskEdit');
			$repeat = $this->input->post('repeatEdit');
			$oldUserIdsEdit = explode(",", $this->input->post('oldUserIdsEdit'));
			
			$endTaskDate = $this->input->post('endTaskDateEdit');
			$EndAfteroccurances = $this->input->post('EndAfteroccurancesEdit');

			$dueWeekDay = $this->input->post('dueWeekDayEdit');
			$monthDueOn = $this->input->post('monthDueOnEdit');
			$dueMonthMonth = $this->input->post('dueMonthMonthEdit');
			$dueMonthWeek = $this->input->post('dueMonthWeekEdit');
			$dueMonthDay = $this->input->post('dueMonthDayEdit');
			$yearDueOn = $this->input->post('yearDueOnEdit');
			$dueYearMonth = $this->input->post('dueYearMonthEdit');
			$dueYearMonthDay = $this->input->post('dueYearMonthDayEdit');
			$dueYearWeek = $this->input->post('dueYearWeekEdit');
			$dueYearDay = $this->input->post('dueYearDayEdit');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnTheEdit');
			$repeatOld = $this->input->post('repeatOldEdit');
			
			//Set task start and end date as par repeat fiter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}

				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDateEdit')));
				$repeatOrder = 1;
			}

			if (!empty($endTaskDate)) 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances)) 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." month"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." year"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			$resultTaskData = $this->AM->getWhereSingle(array("taskId" => $taskId),"taskMaintenace");

			$taskIds = array($taskId);
			$addMachineId = array();
			if ($insertVariable == true) 
			{
				$machineId =  $this->input->post('machineIdEdit');

				if (!empty($machineId)) 
				{
					foreach ($machineId as $key => $value) 
					{ 
						if ($resultTaskData->machineId != $value) 
						{

							$addMachineId[] = $value;
							$data=array(
				                'userIds' => $userIds,
				                'machineId' => $value,  
				                'task' => $task,  
				                'repeat' => $repeat,
				                'startDate' => $startDate,
				                'dueDate' => $dueDate,
				                'newTask' => '1',
				                'repeatOrder' => $repeatOrder,
				                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
				                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
				                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
				                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
				                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
				                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
				                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
				                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
				                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
				                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
				                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
				                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
				                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
				                'createdByUserId' => $this->session->userdata('userId')
				            );  
							$taskIds[] = $this->AM->insertData($data, "taskMaintenace");
						}
			        }
				}


				$editData=array(
	                'userIds' => $userIds,
	                'task' => $task,  
	                'repeat' => $repeat,
	                'startDate' => $startDate,
	                'dueDate' => $dueDate,
	                'newTask' => '1',
	                'repeatOrder' => $repeatOrder,
	                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
	                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
	                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
	                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
	                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
	                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
	                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
	                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
	                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
	                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
	                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
	                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
	                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL
	            );  
				
				$this->AM->updateData(array("taskId" => $taskId),$editData, "taskMaintenace");

				
				$lastSubTask = $this->AM->getLastSubTask($taskId);
				if (!empty($lastSubTask)) 
				{
					$this->AM->updateData(array("taskId" => $lastSubTask->taskId),$editData, "taskMaintenace");
				}

		        if (!empty($taskIds)) 
		        {
		        	//Send task data to opapp update in as par assign machine and operator
		        	$select = "taskMaintenace.taskId,
			        		   taskMaintenace.createdByUserId,
			        		   taskMaintenace.task,
			        		   taskMaintenace.repeat,
			        		   taskMaintenace.status,
			        		   taskMaintenace.dueDate,
			        		   taskMaintenace.userIds,
			        		   taskMaintenace.machineId,
			        		   taskMaintenace.dueWeekDay,
			        		   taskMaintenace.monthDueOn,
			        		   taskMaintenace.dueMonthMonth,
			        		   taskMaintenace.dueMonthWeek,
			        		   taskMaintenace.dueMonthDay,
			        		   taskMaintenace.yearDueOn,
			        		   taskMaintenace.dueYearMonth,
			        		   taskMaintenace.dueYearMonthDay,
			        		   taskMaintenace.dueYearWeek,
			        		   taskMaintenace.dueYearDay,
			        		   taskMaintenace.dueYearMonthOnThe,
			        		   taskMaintenace.endTaskDate,
			        		   taskMaintenace.EndAfteroccurances,
			        		   taskMaintenace.newTask
			        		   ";
					$whereIn = array("taskId" => $taskIds);
		        	$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

					
		        	foreach ($taskData as $key => $value) 
		            {
		            	$userDelete = array();
						$userAdd = array();
		            	if (in_array($value['machineId'], $addMachineId))
					  	{
					  		$taskData[$key]['type'] = 'add';
					    }
						else
					  	{
					  		if ($repeatOld != $repeat) 
					  		{
		            			$taskData[$key]['type'] = 'repeatTask';
					  		}else
					  		{
		            			$taskData[$key]['type'] = 'update';
					  		}
					  	}


		            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
		            	$userIds = explode(",", $value['userIds']);

		            	$array_intersect = array_intersect($oldUserIdsEdit,$userIds);

						$taskData[$key]['isLock'] = 1;
						foreach ($userIds as $userKey => $userValue) 
						{
							foreach ($oldUserIdsEdit as $oldkey => $oldvalue) 
							{
								if (in_array($oldvalue, $userIds)) 
								{
								}else
								{
									if (in_array($userValue, $oldUserIdsEdit)) 
									{
										$userDelete[] = $oldvalue;
									}else
									{
										$userAdd[] = $userValue;
									}
								}
							}

							if (!in_array($userValue, $oldUserIdsEdit)) 
							{
								$userAdd[] = $userValue;
							}
							$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
							$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);
						}

						$taskData[$key]['userDeleteId'] = !empty($array_intersect) ? array_unique($userDelete) : array_unique($oldUserIdsEdit);
						$taskData[$key]['userAddId'] = array_unique($userAdd);
						unset($userDelete);
						unset($userAdd);
		            }

		        	$json = array("status" => "1","taskData" => $taskData,"message" => Taskeditedsuccessfully);
		        }
		        else
		        {
		        	$json = array("status" => "0","message" => Errorwhileaddingtask);
		        }
		    }else
		    {
		    	$json = array("status" => "0","message" => Endtaskdatecannotbelessthanduedate);
		    }
	        echo json_encode($json); die;
        }
		
	}


	//update_task function use for update task
	public function update_task_status() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('status','status','required');
        $this->form_validation->set_rules('taskId','taskId','required');
		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 

            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
            echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('taskId');
			$status = $this->input->post('status');


			$data=array(
                'status' => $status
            );  

			$this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");
			

			$select = "taskMaintenace.taskId,
	        		   taskMaintenace.status as taskStatus,
	        		   taskMaintenace.machineId";
			$whereIn = array("taskId" => $taskId);
			$taskData = $this->AM->getWhereSingleSelect($select,$whereIn,"taskMaintenace");
			
			$taskData->factoryId = $this->session->userdata('factoryId');

            $json = array("status" => "1","taskData" => $taskData);
	        echo json_encode($json); die; 
		}
	}
}
?>