<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

class Api extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->model('api3/Api_model','APIM');
		$this->APIM->getLanguage();
    }

    

    //The login Api is used for login in the settapp by the machine code 
    public function login_post()
	{
	    $this->form_validation->set_rules('machineCode', 'machineCode', 'required');
	    $this->form_validation->set_rules('deviceToken', 'deviceToken', 'required');
	    $this->form_validation->set_rules('startTime', 'startTime', 'required');
		
		//checking required parameters validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"machineSelectedFlag" => 0,"message" =>  $error[0]);
		}
		else
		{  
			$machineCode = $this->input->post('machineCode');
			$deviceToken = $this->input->post('deviceToken');
			$startTime = $this->input->post('startTime');
			$deviceId = $this->input->post('deviceId');
			$previousMachineId = $this->input->post('machineId');
			
			//checking the machine code is valid or not
		    $result = $this->APIM->checkMachineCode($machineCode);
		    
		    //if the machine is selected in other phone in setApp
		    if ($result->machineSelected == "1") 
		    {
				$json =  array("status" => "0","machineSelectedFlag" => 1,"message"=> Machineisselectedinotherphone.".");
		    }else if (!empty($result)) 
		    {
		    	$factoryId = $result->factoryId;
		    	$machineId = $result->machineId;
		    	$machineData = array("machineSelected"=>"1");
		    	$machineWhere = array("machineId" => $machineId);
		    	//updating the machine status login
		    	$this->APIM->updateFactoryData($factoryId,$machineWhere,$machineData,"machine");
		    	$factory = $this->db->where('factoryId',$factoryId)->get('factory')->row();
		    	$addField = array(
		    			"machineId"=>$machineId,
						"startTime"=>date('Y-m-d H:i:s', $startTime), 
						"endTime"=>date('Y-m-d H:i:s', $startTime),  
						"isActive"=>'1',
						"deviceToken" => $deviceToken
						);	 

		    	//adding last log for machine 
		    	$activeId = $this->APIM->insertFactoryData($factoryId,$addField,"activeMachine");

		    	$result->activeId = $activeId;
		    	$result->factoryName = base64_decode($factory->factoryName);

		    	if(!empty($deviceId))
		    	{
		    		$phoneDetailExists = $this->APIM->phoneDetailExists($factoryId, $machineId);

			    	if (!empty($phoneDetailExists)) 
			    	{
			    		$phoneDetailData = array("deviceId"=> $deviceId);
				    	$phoneDetailWhere = array("machineId" => $machineId);
				    	$this->APIM->updateFactoryData($factoryId,$phoneDetailWhere,$phoneDetailData,"phoneDetailLogv2");
			    	}else
			    	{
			    		$phoneDetailData = array("deviceId"=> $deviceId,"machineId" => $machineId);
				    	$this->APIM->insertFactoryData($factoryId,$phoneDetailData,"phoneDetailLogv2");
			    	}
		    	}

		    	//selecting previous machine then automatically unselecting the phone
		    	if (!empty($previousMachineId)) 
		    	{
		    		$previousMachineData = array("machineSelected"=>"0");
			    	$previousMachineWhere = array("machineId" => $previousMachineId);
			    	$this->APIM->updateFactoryData($factoryId,$previousMachineWhere,$previousMachineData,"machine");
		    	}
		    	

				$json =  array("status" => "1","machineSelectedFlag" => 0,"message"=> SIGNIN_SUCCESS,"data" => $result);
		    }
		    else
		    {
				$json =  array("status" => "0","machineSelectedFlag" => 0,"message"=> Pleaseentervalidmachinecode.".");
		    }
		   
		    
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    public function forceLogin_post()
	{
	    $this->form_validation->set_rules('machineCode', 'machineCode', 'required');
	    $this->form_validation->set_rules('deviceToken', 'deviceToken', 'required');
	    $this->form_validation->set_rules('startTime', 'startTime', 'required');
		
		//checking required parameters validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"message" =>  $error[0]);
		}
		else
		{  
			$machineCode = $this->input->post('machineCode');
			$deviceToken = $this->input->post('deviceToken');
			$startTime = $this->input->post('startTime');
			$deviceId = $this->input->post('deviceId');
			$previousMachineId = $this->input->post('machineId');
			
			//checking the machine code is valid or not
		    $result = $this->APIM->checkMachineCode($machineCode);
		    
		   
	    	$factoryId = $result->factoryId;
	    	$machineId = $result->machineId;



	    	$this->APIM->unlockMachine($factoryId, $machineId);
			$machineUser = $this->APIM->getLastActiveMachine($factoryId, $machineId);
			$this->APIM->updateFactoryData($factoryId,array("activeId" => $machineUser->activeId),array("isActive" => "0","endTime" => date('Y-m-d H:i:s')),"activeMachine");
			$fcmToken = array($machineUser->deviceToken);
			$message = "Machine has been unlocked from force login. Please signin again.";
			$title = "Machine unlock";
			$type = "unlockMachine";
			//Send notification for logout setapp machine 
			$this->APIM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$machineUser->activeId);

	    	$machineData = array("machineSelected"=>"1");
	    	$machineWhere = array("machineId" => $machineId);
	    	//updating the machine status login
	    	$this->APIM->updateFactoryData($factoryId,$machineWhere,$machineData,"machine");
	    	$factory = $this->db->where('factoryId',$factoryId)->get('factory')->row();
	    	$addField = array(
	    			"machineId"=>$machineId,
					"startTime"=>date('Y-m-d H:i:s', $startTime), 
					"endTime"=>date('Y-m-d H:i:s', $startTime),  
					"isActive"=>'1',
					"deviceToken" => $deviceToken
					);	 

	    	//adding last log for machine 
	    	$activeId = $this->APIM->insertFactoryData($factoryId,$addField,"activeMachine");

	    	$result->activeId = $activeId;
	    	$result->factoryName = base64_decode($factory->factoryName);

	    	if(!empty($deviceId))
	    	{
	    		$phoneDetailExists = $this->APIM->phoneDetailExists($factoryId, $machineId);

		    	if (!empty($phoneDetailExists)) 
		    	{
		    		$phoneDetailData = array("deviceId"=> $deviceId);
			    	$phoneDetailWhere = array("machineId" => $machineId);
			    	$this->APIM->updateFactoryData($factoryId,$phoneDetailWhere,$phoneDetailData,"phoneDetailLogv2");
		    	}else
		    	{
		    		$phoneDetailData = array("deviceId"=> $deviceId,"machineId" => $machineId);
			    	$this->APIM->insertFactoryData($factoryId,$phoneDetailData,"phoneDetailLogv2");
		    	}
	    	}

	    	//selecting previous machine then automatically unselecting the phone
	    	if (!empty($previousMachineId)) 
	    	{
	    		$previousMachineData = array("machineSelected"=>"0");
		    	$previousMachineWhere = array("machineId" => $previousMachineId);
		    	$this->APIM->updateFactoryData($factoryId,$previousMachineWhere,$previousMachineData,"machine");
	    	}
	    	

			$json =  array("status" => "1","message"=> SIGNIN_SUCCESS,"data" => $result);
		    
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    //The logout Api is use for Logout in the setApp 
    public function logout_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('activeId', 'activeId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $machineId = $this->input->post('machineId');
		$activeId = $this->input->post('activeId');
		$factoryId = $this->input->post('factoryId');

		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //checking the required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"message" =>  $error[0],"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
	    	$machineData = array("machineSelected"=>"0");
	    	$machineWhere = array("machineId" => $machineId);
	    	//updating logout details
	    	$this->APIM->updateFactoryData($factoryId,$machineWhere,$machineData,"machine");

	    	$addField = array(
					"endTime"=>date('Y-m-d H:i:s', $startTime),  
					"isActive"=>'0'
					);	 

	    	$activeMachineWhere = array("activeId" => $activeId);
	    	//updating the machine last logs
	    	$this->APIM->updateFactoryData($factoryId,$activeMachineWhere,$addField,"activeMachine");

			$json =  array("status" => "1","message"=> SIGNOUT_SUCCESS,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //phoneDetail() api is use for adding phone in setApp phonedetails 
    public function phoneDetail_post()
    {  
        $postArray = $this->post();

		if(isset($postArray['currentTime'])) 
		{
			$currentTime = $postArray['currentTime'];
		} else {
			$currentTime = time();
		} 
		//checking that a machine is exists or not 
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);
		//checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//checking required parameters validation
		if (isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($postArray['model']) && isset($postArray['manufacture']) && isset($postArray['version']) && isset($postArray['batteryCapacity']) && isset($postArray['batteryLevel']) && isset($postArray['totalRam']) && isset($postArray['ramUsage']) && isset($currentTime)) {   
			
			$model = $postArray['model']; 
			$manufacture = $postArray['manufacture']; 
			$version = $postArray['version'];
			$batteryCapacity = $postArray['batteryCapacity'];
			$batteryLevel = $postArray['batteryLevel'];
			$totalRam = $postArray['totalRam'];
			$ramUsage = $postArray['ramUsage'];
			$cameraResolution = $postArray['cameraResolution'];
			$machineId = $postArray['machineId'];
			$userId = 0;
			$factoryId = $postArray['factoryId'];
			if(isset($postArray['deviceId'])) {
				$deviceId = $postArray['deviceId'];
			} else {
				$deviceId = '';
			}
			if(isset($postArray['appVersion'])) {
				$appVersion = $postArray['appVersion'];
			} else {
				$appVersion = '';
			}

			if(isset($postArray['appVersionCode'])) 
			{
				$appVersionCode = $postArray['appVersionCode'];
				$wifiStatus = $postArray['wifiStatus'];
				$networkType = $postArray['networkType'];
				$imsi = $postArray['imsi'];
				$totalStorage = $postArray['totalStorage'];
				$storageAvailable = $postArray['storageAvailable'];
				$deviceResolution = $postArray['deviceResolution'];
				$sdkVersion = $postArray['sdkVersion'];
				$batteryHealth = $postArray['batteryHealth'];
				$batteryType = $postArray['batteryType'];
				$batteryTemperature = $postArray['batteryTemperature'];
				$chargingSource = $postArray['chargingSource'];
				$chargeFlag = $postArray['chargeFlag']; 
			} else {
				$appVersionCode = '';
				$wifiStatus = '0';
				$networkType = '';
				$imsi = 0;
				$totalStorage = '';
				$storageAvailable = '';
				$deviceResolution = '';
				$sdkVersion = 0;
				$batteryHealth = '';
				$batteryType = '';
				$batteryTemperature = '';
				$chargingSource = '';
				$chargeFlag = '0'; 
			}
			
			$phoneDetailExists = $this->APIM->phoneDetailExists($factoryId, $machineId);
			//if phone detail is already added then update phone data other wise add new
			if(isset($phoneDetailExists) && isset($phoneDetailExists->phoneId)) { 
				$phoneId = $phoneDetailExists->phoneId;
				//update phone details
				$this->APIM->updatePhoneDetailLog($phoneId, $factoryId, $currentTime, $model, $manufacture, $version, $batteryCapacity, $batteryLevel, $totalRam, $ramUsage, $cameraResolution, $deviceId, $appVersion, $appVersionCode, $wifiStatus, $networkType, $imsi, $totalStorage, $storageAvailable, $deviceResolution, $sdkVersion, $batteryHealth, $batteryType, $batteryTemperature, $chargingSource, $chargeFlag);  
			} else {
				//insert phone details
				$phoneId = $this->APIM->addPhoneDetailLog($factoryId, $currentTime, $machineId, $model, $manufacture, $version, $batteryCapacity, $batteryLevel, $totalRam, $ramUsage, $cameraResolution, $deviceId, $appVersion, $appVersionCode, $wifiStatus, $networkType, $imsi, $totalStorage, $storageAvailable, $deviceResolution, $sdkVersion, $batteryHealth, $batteryType, $batteryTemperature, $chargingSource, $chargeFlag); 
			}
			
			$message = [ 'status' => "1","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"phoneId"=>$phoneId ];  
		
		} else {
			$message = [ 'status' => "0","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
    }

    //machineStackLightType use for download and detection stacklighttype file in setApp 
    public function machineStackLightType_post() 
    { 
        $postArray = $this->post();
        //checking that a machine is exists or not 
        $checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);
        //checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if (isset($postArray['factoryId']) && isset($postArray['machineId'])) {   
			
			$machineId = $postArray['machineId'];
			$factoryId = $postArray['factoryId'];
			//get factory data
			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
			if(is_array($factoryDetail)) {
				$machineStackLightType = $this->APIM->getMachineStackLightType($factoryId, $machineId); 
				if($machineStackLightType == '-1') {
					$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ];
				} else 
				{
					$listColors = $this->APIM->getallColors()->result();

					$machineLightArr = explode(",", $machineStackLightType->machineLight); 
					//print_r($machineLightArr);exit;
					$machineColor = array();
					for($z=0;$z<count($machineLightArr);$z++) 
					{	
						for($y=0;$y<count($listColors);$y++) 
						{
							if($machineLightArr[$z] == $listColors[$y]->colorId) $machineColor[$z] = $listColors[$y]->colorCode;  
						}

						if($machineColor[$z] == 'FF0000') 
						{
							$machineColor[$z] = 'red';
						}
						if($machineColor[$z] == 'FFFF00') 
						{
							$machineColor[$z] = 'yellow';
						}
						if($machineColor[$z] == '00FF00') 
						{
							$machineColor[$z] = 'green';
						}
						if($machineColor[$z] == 'FFFFFF') 
						{
							$machineColor[$z] = 'white';
						}  
						if($machineColor[$z] == '0000FF') 
						{
							$machineColor[$z] = 'blue';
						} 
					}
					$message = [ 'status' => "1","machineColor" => $machineColor,"machineColorCount" => count($machineColor),'machineStackLightType' => $machineStackLightType->stackLightTypeId, 'stackLightTypeVersion' => $machineStackLightType->stackLightTypeVersion,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
				}
			}  else { 
				$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ];
			}
			
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    } 

    //image api is use for add detection issue image in setApp
    public function image_post()  
    { 
        $postArray = $this->post();
		$fileArray = $_FILES;  

		if(isset($postArray['currentTime'])) {
			$file_name = $postArray['currentTime'];
		} else {
			$file_name = time();
		} 
		if(isset($postArray['originalTime'])) {
			$originalTime = $postArray['originalTime']/1000; 
		} else {
			$originalTime = time(); 
		}

		//checking that a machine is exists or not 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a factory is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking required parameter validation
		if (isset($fileArray['image']) && isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($file_name)) 
		{  
			$machineId = $postArray['machineId'];
			$factoryId = $postArray['factoryId'];
			
			$path_parts = pathinfo($fileArray["image"]["name"]);
			$file_ext = $path_parts['extension'];
			
			//upload detection issue in image
			move_uploaded_file($fileArray['image']['tmp_name'], "../factory".$factoryId."_repos/"  .$machineId. "_" . $file_name. "." .$file_ext);
			$insertLogId = $this->APIM->addLogImageFrame($factoryId, $originalTime, $file_name, $file_ext, $machineId);  
			
			$message = [ 'status' => "1","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ]; 
		
		} else {
			$message = [ 'status' => "0","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    public function logUpload_post()  
    { 
        $postArray = $this->post();
		$fileArray = $_FILES;  

		//checking that a machine is exists or not 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a factory is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking required parameter validation
		if (isset($fileArray['fileData'])) 
		{  
			$file_name = time();
			$machineId = $postArray['machineId'];
			$factoryId = $postArray['factoryId'];
			
			$path_parts = pathinfo($fileArray["fileData"]["name"]);
			$file_ext = $path_parts['extension'];
			
			//upload detection issue in image
			move_uploaded_file($fileArray['fileData']['tmp_name'], "../setapp_log/".$factoryId."_".$machineId."_".$file_name.".".$file_ext);

			
			$message = [ 'status' => "1","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ]; 
		
		} else {
			$message = [ 'status' => "0","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    //getLastLog() It used for getting the last detection detail in setApp 
    public function getLastLog_post() 
    { 
        $postArray = $this->post();

        //checking that a machine is exists or not 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a  factory is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking required parameter validation
		if (isset($postArray['factoryId']) && isset($postArray['machineId'])) 
		{	
			$factoryId = $postArray['factoryId']; 
			$machineId = $postArray['machineId'];
			$currentDate = date('H:i:s');
			//getting the last log entry	

			
			$logId = $this->APIM->getLastLogIdBeta($factoryId, $machineId);

			$schedule = $this->APIM->checkScheduleImageCapturingStatus($factoryId, $machineId, $currentDate);

			if (!empty($schedule)) 
			{
				$date = new DateTime(date("H:i:s") );
				$date2 = new DateTime($schedule->endTime);

				$scheduleDuration = $date2->getTimestamp() - $date->getTimestamp();
			}else
			{
				$scheduleDuration = 0;
			}

			$message = [ 'status' => "1","scheduleStartStatus" => !empty($schedule) ? 1 : 0,"scheduleDuration" => $scheduleDuration , "logID"=>$logId,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 ];
		} else {
			$message = [ 'status' => "0","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK);
    }
    //getVideoStatus() api use for get video detection status in setApp
    //The Api is use for getting the video detection Status in the SetApp
    public function getVideoStatus_post() 
    { 
    	$postArray = $this->post();
		
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		if(isset($postArray['machineId']) && isset($postArray['factoryId'])) {
			$machineId = $postArray['machineId'];
			//getting the video status
			$status = $this->APIM->getVideoStatus($postArray['factoryId'], $machineId); 

			$message = ['status'=>'1','videoStatus'=>$status['videoStatus'],'cycleTime'=>$status['cycleTime'],"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
		} else {	
			$message = ['status'=>'0','message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

		$this->set_response($message, REST_Controller::HTTP_OK);
	} 

	//noInternetLog api() It use for adding the information for no internet log in setApp  
	public function noInternetLog_post() 
	{  
		$postArray = $this->post();

		//checking that a machine is exists or not 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a factory is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking that a required parameter validation
		if(isset($postArray['startTime']) && isset($postArray['endTime']) && isset($postArray['machineId']) &&  isset($postArray['factoryId']) ) 
		{  
			//getting the factory data
			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
				if(is_array($factoryDetail)) {
					//add no internet log details
					$this->APIM->addNoInternetLog($postArray['factoryId'], $postArray['startTime'], $postArray['endTime'], $postArray['machineId'],'0');   
					$message = ['status'=>'1','message'=>LOG_SUCCESS,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				} else {
					$message = ['status'=>'0','message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				}
		} else {
			$message = ['status'=>'0','message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

   	//appCrashLog()	This Api is used when the Application has been crashed but for Inserting the appCrashLog in the setApp.
    public function appCrashLog_post() 
    {  
		$postArray = $this->post();
		

		//checking that a machine is exists or not 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a factory is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking the required parameter validation
		if(isset($postArray['startTime']) && isset($postArray['endTime']) && isset($postArray['machineId']) && isset($postArray['factoryId']) ) {  
			//checking the factory data
			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
				if(is_array($factoryDetail)) { 
					//Adding the app crash details
					$this->APIM->addNoInternetLog($postArray['factoryId'], $postArray['startTime'], $postArray['endTime'], $postArray['machineId'],'1'); 
					$message = ['status'=>'1','message'=>LOG_SUCCESS,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				} else {
					$message = ['status'=>'0','message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				}
		} else {
			$message = ['status'=>'0','message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
        $this->set_response($message, REST_Controller::HTTP_OK);
    }

    //helpReport() The api is used for the operator when they need any help in the oppApp
    public function helpReport_post() { 
		
		$postArray = $this->post();
		$fileArray = $_FILES;  

		if(isset($postArray['currentTime'])) {
			$currentTime = $postArray['currentTime'];
		} else {
			$currentTime = time();
		} 

		//checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//checking that a machine is exists or not 
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);

		//checking required parameter validation
		if (isset($postArray['problemText']) && isset($postArray['factoryId']) && isset($currentTime)) { 

			$factoryId = $postArray['factoryId'];
			$type = $postArray['type'];
			$problemText = $postArray['problemText'];
			if(isset($postArray['workDuration'])) {
				$workDuration = $postArray['workDuration'];  
			} else {
				$workDuration = ''; 
			}
			$application = $postArray['application'];  
			
			$factoryName = $this->APIM->getFactoryDetail($factoryId); 
			
			$msg = Followingmessagehasbeenreportedbyoperator." : <br>
			    ".Message.": ". $problemText;
			$subject = SetAppoperatorreportsaproblem;


			//sending an email with a attachments
			if(isset($fileArray['image']) ) { 
				$allowed =  array('jpeg','png' ,'jpg','mov','mp4','pdf','txt','doc','docx');
				$ext = pathinfo($fileArray["image"]["name"], PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) 
				{	
					$message = [ 'status' => "0",'message'=> Pleaseuploadvalidimagevideoordocument.'.']; 
				}else
				{
					$path_parts = pathinfo($fileArray["image"]["name"]);
					$file_ext = $path_parts['extension'];
					//upload attachment in the folder
					
					move_uploaded_file($fileArray['image']['tmp_name'], "../report/factory".$factoryId."/" .  $currentTime. "." .$file_ext);
					//add help report data
					$insert = $this->APIM->addHelpReport($factoryId, $currentTime, $problemText, $workDuration, $file_ext);

					$file_path =  "https://nyttcloud.host/prodapp/report/factory".$factoryId.'/' . $currentTime. '.' .$file_ext;
					
					//send email with attachment data to admin
					$this->APIM->sendEmail($msg,$subject,$file_path,$file_ext,$factoryId);
					$message = [ 'status' => "1",'message'=>PROBLEM_REPORT_SUCCESS,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"image" => $fileArray['image']['name']];
					
				} 
				
			} else {
				//add help report data
				$insert = $this->APIM->addHelpReport($factoryId, $currentTime, $problemText, $workDuration, ''); 

				//send email to admin

				$this->APIM->sendEmail($msg,$subject,"","",$factoryId);
				
				$message = [ 'status' => "1",'message'=>PROBLEM_REPORT_SUCCESS,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"image" => $fileArray['image']['name']];
				
			}
		
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }

    //updateAppVersion() Api is use for installing new version in setApp
    public function updateAppVersion_post() { 

		$postArray = $this->post();

		//checking that a machine is exists or not 
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);
		//checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if (isset($postArray['app_version_code']) && isset($postArray['app_type']) && isset($postArray['factoryId'])) 
		{ 
			$app_version_code = $postArray['app_version_code'];
			$app_type = $postArray['app_type'];

			$result = $this->APIM->checkLatestAppVersion($app_type);
			//checking the new version are already added or not 
			if ($result->app_version_code > $app_version_code) 
			{
				//new version is added
				$message = [ 'status' => "1",'message'=> NewupdateavailableInstallnow.".","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result];	
			}else
			{
				//new version is not added 
				$message = [ 'status' => "0",'message'=> Yoursetappversionisuptodate.". ","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
			
			
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }

    //batteryLowNotification() api use for sending batterylow notification on oppApp by the setApp  
    public function batteryLowNotification_post() { 
        $postArray = $this->post();

		$factoryId = $postArray['factoryId'];  
		//checking that a machine is exists or not 
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']);
		//checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

		//check required parameter validation
		if (isset($postArray['factoryId']) && isset($postArray['machineId']) && isset($postArray['batteryPercentage']) && isset($postArray['chargeFlag'])) {   
			
			$machineId = $postArray['machineId'];
			$chargeFlag = $postArray['chargeFlag'];
			$batteryPercentage = $postArray['batteryPercentage'];
			$machine = $this->APIM->machineExists($factoryId, $machineId);



			$machineOperator = $this->APIM->getMachineOperator($factoryId, $machineId);

			//print_r($machineOperator);exit;

			//checking the charger is connected or not
			if ($chargeFlag == "1") {
				$chargeMessage = andchargerisconnected.'.';
			}else
			{
				$chargeMessage = andchargerisnotconnected.'.';
			}


			//$fcmToken =  array_column($machineOperator, "deviceToken");
			
			//checking the battery percentage is less then 5%
			if ($batteryPercentage <= 5) 
			{
				//$messageNotification = "SetApp ". $machine['machineName'] . ' in ' . $checkFactoryDetail[0]['factoryName'] ." phone battery is less than 5%. SetApp phone is shutting down.";
				//$messageNotification = "Battery percentage for SetApp phone on machine ". $machine['machineName'] ." of ". base64_decode($checkFactoryDetail[0]['factoryName']) ." is 5%. SetApp is going to end detection now.";

				$messageNotification = BatterypercentageforSetAppphoneonmachine." ". $machine['machineName']. " " . of . " " .base64_decode($checkFactoryDetail[0]['factoryName']) . is ." 5%. ". SetAppisgoingtoenddetectionnow;
			}else
			{
				//$messageNotification = "SetApp running on ". $machine['machineName'] . ' in ' . $checkFactoryDetail[0]['factoryName'] ." has battery percentage less than ". $batteryPercentage ."% ".$chargeMessage;
				//$messageNotification = "Battery percentage for SetApp phone on machine ". $machine['machineName'] ." of ". base64_decode($checkFactoryDetail[0]['factoryName']) ." is ". $batteryPercentage ."% ".$chargeMessage;

				$messageNotification = BatterypercentageforSetAppphoneonmachine." ". $machine['machineName']. " " . of . " " .base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is ." ". $batteryPercentage ."% ".$chargeMessage;
			}

			//print_r($machineOperator);exit;
			//not remove this code use in reminder notification
			$newData = array("notificationText" => $messageNotification,"machineId" => $machineId,"factoryId" => $factoryId,"flag" => "0");

			$notificationId = $this->APIM->insertDBData($newData,"notificationLog");
			
			if ($batteryPercentage != 100) 
			{
				
			

				//send battery notification in oppApp

				$this->APIM->getLanguage();
				foreach ($machineOperator as $key => $value) 
				{
					if($value['getNotified'] == '1' &&  $value['isActive'] == '1') 
					{

						$userSoundConfig = $this->APIM->getWhereSingle($postArray['factoryId'], array("machineId" => $machineId,"userId" => $value['userId']), "userSoundConfig");
						
						if (!empty($userSoundConfig)) 
						{
							$machineSound = $userSoundConfig->sound;
						}else
						{
							$machineSound = "1";
						}

						if ($value['languageType'] == 2) 
						{
							$titleNew = Lowbattery2;
						}else
						{
							$titleNew = Lowbattery1;
						}

						if ($chargeFlag == "1") {
							if ($value['languageType'] == 2) 
							{
								$chargeMessageNew = andchargerisconnected2.'.';
							}else
							{
								$chargeMessageNew = andchargerisconnected1.'.';
							}
						}else
						{
							if ($value['languageType'] == 2) 
							{
								$chargeMessageNew = andchargerisnotconnected2.'.';
							}else
							{
								$chargeMessageNew = andchargerisnotconnected1.'.';
							}
						}

						if ($batteryPercentage <= 5) 
						{
							if ($value['languageType'] == 2) 
							{
								$messageNotificationNew = BatterypercentageforSetAppphoneonmachine2." ". $machine['machineName']. " " . of2 . " " .base64_decode($checkFactoryDetail[0]['factoryName']) . is2 ." 5%. ". SetAppisgoingtoenddetectionnow2;
							}else
							{
								$messageNotificationNew = BatterypercentageforSetAppphoneonmachine1." ". $machine['machineName']. " " . of1 . " " .base64_decode($checkFactoryDetail[0]['factoryName']) . is1 ." 5%. ". SetAppisgoingtoenddetectionnow1;
							}
							
						}else
						{
							if ($value['languageType'] == 2) 
							{
								$messageNotificationNew = BatterypercentageforSetAppphoneonmachine2." ". $machine['machineName']. " " . of2 . " " .base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is2 ." ". $batteryPercentage ."% ".$chargeMessageNew;
							}else
							{
								$messageNotificationNew = BatterypercentageforSetAppphoneonmachine1." ". $machine['machineName']. " " . of1 . " " .base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is1 ." ". $batteryPercentage ."% ".$chargeMessageNew;
							}
						}

						//not remove this code use in reminder notification
						/*$notificationUserLog = array("notificationId" => $notificationId,"notificationText" => $messageNotificationNew,"machineId" => $machineId,"factoryId" => $factoryId,"notificationSendTime" => date('Y-m-d H:i:s',strtotime("+".$value['receiveReminders']." min")),"notificationSendCount" => "1","userId" => $value['userId']);

						$notificationUserLogId = $this->APIM->insertDBData($notificationUserLog,"notificationUserLog");*/
						$fcmToken = array($value['deviceToken']);
						$this->APIM->sendbatteryLowNotification($messageNotificationNew,$titleNew,$fcmToken,$factoryId,$machineId,$batteryPercentage,0,$machineSound);
					}
				}


				
				$subject = SetApplowbattery;
				$this->APIM->sendEmail($messageNotification,$subject,"","",$factoryId);

			}

			$message = [ 'status' => "1","message" => $messageNotification ,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			

				
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }



    public function machineSelectedStatus_post() { 
        $postArray = $this->post();
		
		$factoryId = $postArray['factoryId'];
		//checking that a machine is exists or not
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']); 
		//checking that a factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
		
		if (isset($postArray['factoryId']) && isset($postArray['machineId']) && isset($postArray['deviceId'])) {   
			
			$machineId = $postArray['machineId'];
			$deviceId = $postArray['deviceId'];
			$result = $this->APIM->getLastMachineSelectedStatus($factoryId,$machineId,$deviceId);

			if (!empty($result)) {
				
				$message = [ 'status' => "1","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}else
			{
				$message = [ 'status' => "1","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}

				
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    public function NewmachineSelectedStatus_post() { 
        $postArray = $this->post();
		
		$factoryId = $postArray['factoryId']; 
		$checkMachineDetail = $this->APIM->checkMachineDetail($_POST['factoryId'],$_POST['machineId']); 
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
		
		if (isset($postArray['factoryId']) && isset($postArray['machineId']) && isset($postArray['deviceId'])) {   
			
			$machineId = $postArray['machineId'];
			$deviceId = $postArray['deviceId'];
			$result = $this->APIM->getLastMachineSelectedStatus($factoryId,$machineId,$deviceId);

			if (!empty($result)) {
				
				$machine = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId),"machine");	

				$machineStackLightType = $this->APIM->getMachineStackLightType($factoryId, $machineId); 
				$stackLightTypeId = ($machineStackLightType == "-1") ? 0 : $machineStackLightType->stackLightTypeId;
				$stackLightTypeVersion = ($machineStackLightType == "-1") ? 0 : $machineStackLightType->stackLightTypeVersion;

				$message = [ 'status' => "1","machineStatus" => $machine->machineStatus,"logStatus" => $machine->logStatus,'machineStackLightType' => $stackLightTypeId, 'stackLightTypeVersion' => $stackLightTypeVersion,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0 , "isTrainingFlag" => ($machine->stackLightTypeId != 0) ? 1 : 0, "message" => ($machine->stackLightTypeId != 0) ? Machineselectedstacklight : Pleasechoosestacklightfromdashboardtrainingpagetogoaheadandstartdetection."."];
			}else
			{
				$message = [ 'status' => "0","message" => "Machine selected in another phone","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  				
			}

				
		} else {
			$message = [ 'status' => "0",'message'=>Invaliddetails,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    public function batteryTemperatureExceeds_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('batteryTemperature', 'batteryTemperature', 'required');

	    $machineId = $this->input->post('machineId');
		$factoryId = $this->input->post('factoryId');

		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
	    //check required parameter validation
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => 0,"message" =>  $error[0],"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
		}
		else
		{  
			$batteryTemperature  = $this->input->post('batteryTemperature');
	    	
	    	$result = $this->APIM->machineExists($factoryId,$machineId);

	    	

	    	if ($batteryTemperature >= 45) 
			{
				//$message = "SetApp ". $result['machineName'] ." phone battery temperature is more than 45°C. SetApp phone is shutting down.";
				//$message = "Battery temperature for SetApp phone on machine ". $result['machineName'] ." of ". base64_decode($checkFactoryDetail[0]['factoryName']) ." is 45°C. SetApp is going to end detection now.";
				$message = BatterytemperatureforSetAppphoneonmachine." ". $result['machineName'] ." ".of." ". base64_decode($checkFactoryDetail[0]['factoryName']) .SetAppisgoingtoenddetectionnow;
			}else
			{
				//$message = "SetApp running on ". $result['machineName'] ." has battery temperature ". $batteryTemperature."°C";
				//$message = "Battery temperature for SetApp phone on machine ". $result['machineName'] ." of ". base64_decode($checkFactoryDetail[0]['factoryName']) ." is ". $batteryTemperature."°C.";
				$message = BatterytemperatureforSetAppphoneonmachine." ". $result['machineName'] ." ".of." ". base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is ." ". $batteryTemperature."°C.";
			}


	    	
			//not remove this code use in reminder notification
	    	$newData = array("notificationText" => $message,"machineId" => $machineId,"factoryId" => $factoryId,"flag" => "0");

			$notificationId = $this->APIM->insertDBData($newData,"notificationLog");

	    	$machineOperator = $this->APIM->getMachineOperator($factoryId, $machineId);
	    	//$title = "High temperature";
	    	$title = Hightemperature;
	    	$this->APIM->getLanguage();
	    	foreach ($machineOperator as $key => $value) 
			{
				if($value['getNotified'] == '1' &&  $value['isActive'] == '1') 
				{

					$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $value['userId']), "userSoundConfig");
					
					if (!empty($userSoundConfig)) 
					{
						$machineSound = $userSoundConfig->sound;
					}else
					{
						$machineSound = "1";
					}

					if ($value['languageType'] == 2) 
					{
						$titleNew = Hightemperature2;
					}else
					{
						$titleNew = Hightemperature1;
					}

					if ($batteryTemperature >= 45) 
					{
						if ($value['languageType'] == 2) 
						{
							$messageNew = BatterytemperatureforSetAppphoneonmachine2." ". $result['machineName'] ." ".of2." ". base64_decode($checkFactoryDetail[0]['factoryName']) .SetAppisgoingtoenddetectionnow2;
						}else
						{
							$messageNew = BatterytemperatureforSetAppphoneonmachine1." ". $result['machineName'] ." ". of1." ". base64_decode($checkFactoryDetail[0]['factoryName']) .SetAppisgoingtoenddetectionnow1;
						}
					}else
					{
						if ($value['languageType'] == 2) 
						{
							$messageNew = BatterytemperatureforSetAppphoneonmachine2." ". $result['machineName'] ." ".of2." ". base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is2 ." ". $batteryTemperature."°C.";
						}else
						{
							$messageNew = BatterytemperatureforSetAppphoneonmachine1." ". $result['machineName'] ." ".of1." ". base64_decode($checkFactoryDetail[0]['factoryName']) ." ". is1 ." ". $batteryTemperature."°C.";
						}
					}


					/*$notificationUserLog = array("notificationId" => $notificationId,"notificationText" => $message,"machineId" => $machineId,"factoryId" => $factoryId,"notificationSendTime" => date('Y-m-d H:i:s',strtotime("+".$value['receiveReminders']." min")),"notificationSendCount" => "1","userId" => $value['userId']);

					$notificationUserLogId = $this->APIM->insertDBData($notificationUserLog,"notificationUserLog");*/
					$fcmToken = array($value['deviceToken']);
					$this->APIM->sendBatteryTemperatureNotification($messageNew,$titleNew,$fcmToken,$factoryId,$machineId,$batteryTemperature,0,$machineSound);
				}
			}
			
			$subject = "Battery Temperature";
			$this->APIM->sendEmail($message,$subject,"","",$factoryId);
			
			$json =  array("status" => "1","message"=> $message,"new_message" => "Reporting email successfully","isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function storeSensorData_post()
	{
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('sensorData', 'sensorData', 'required');
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0],"isMachineDeleted" => 0,"isFactoryDeleted" => 0);
		}
		else
		{  
			$factoryId = $this->input->post('factoryId');
			$sensorData = $this->input->post('sensorData');
			$data = array("sensor_data" => $sensorData);
			$this->APIM->insertFactoryData($factoryId,$data,"sensorData");
			$json =  array("status" => "1","isMachineDeleted" => 0,"isFactoryDeleted" => 0);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    //applicationCrashLog api use for upload txt file crash log
    public function applicationCrashLog_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('phoneId', 'phoneId', 'required');

	    $machineId = $this->input->post('machineId');
		$factoryId = $this->input->post('factoryId');
		
		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
	    //check required parameter validation
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0],"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
		}
		else
		{   	
			$fileArray = $_FILES;
	    	$path_parts = pathinfo($fileArray["errorFile"]["name"]);
			$file_ext = $path_parts['extension'];
			$file_name = time();
			//upload detection issue in image
			move_uploaded_file($fileArray['errorFile']['tmp_name'], "../application_log/" .$factoryId. "_"  .$machineId. "_" . $file_name. "." .$file_ext);  

			$errorFile = $factoryId. "_"  .$machineId. "_" .$phoneId. "_" . $file_name. "." .$file_ext;

			$phoneId = $this->input->post('phoneId');
	    	$data = array(
	    			"machineId" => $machineId,
	    			"factoryId" => $factoryId,
	    			"phoneId" => $phoneId,
	    			"errorFile" => $errorFile
	    		);

	    	$this->APIM->insertDBData($data,"applicationCrashLog");

			$json =  array("status" => "1","isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
           

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function addStartSetAppLog_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('type', 'type', 'required');

	    $machineId = $this->input->post('machineId');
		$factoryId = $this->input->post('factoryId');
		
		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
	    //check required parameter validation
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0],"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
		}
		else
		{   	
			$type = $this->input->post('type');
			$userId = $this->input->post('userId');

			$data = array(
				"type" => $type,
				"userId" => !empty($userId) ? $userId : NULL,
				"machineId" => $machineId
			);

	    	$this->APIM->insertFactoryData($factoryId,$data,"startSetuppLog");

			$json =  array("status" => "1","message" => "Setapp start log successfully added","isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
        }
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function getLanguage_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('languageType', 'languageType', 'required');

	    $machineId = $this->input->post('machineId');
		$factoryId = $this->input->post('factoryId');
		
		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
	    //check required parameter validation
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0],"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
		}
		else
		{   	
			$languageType = $this->input->post('languageType');

			if (!empty($machineId)) 
			{
				$dataLanguage = array("languageType" => $languageType);
				$where = array("machineId" => $machineId,"factoryId" => $factoryId);
				$this->APIM->updateDBData($where,$dataLanguage,"machineCode");
			}

			$result = $this->db->where('find_in_set("setapp", type) <> 0')->get('translations')->result_array();

			$data = array();
			foreach ($result as $key => $value) 
			{
				$data[$value['textIdentify']] = ($languageType == "1") ? $value['englishText'] : $value['swedishText'];
			}

			$json =  array("status" => "1","message" => "Language change successfully","isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0,"languageText" => $data);
        }
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }


    public function updateDeviceToken_post()
	{
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('deviceToken', 'deviceToken', 'required');

	    $machineId = $this->input->post('machineId');
		$factoryId = $this->input->post('factoryId');
		
		//checking that a machine is exists or not 
	    $checkMachineDetail = $this->APIM->checkMachineDetail($factoryId,$machineId);
	    //checking that a factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
	    //check required parameter validation
		
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" =>  $error[0],"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
		}
		else
		{   	
			$deviceToken = $this->input->post('deviceToken');

			
			$data = array("deviceToken" => $deviceToken);
			$where = array("machineId" => $machineId,"isActive" => "1");
			$this->APIM->updateFactoryData($factoryId,$where,$data,"activeMachine");
			

			$json =  array("status" => "1","isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isMachineDeleted" => empty($checkMachineDetail) ? 1 : 0);
        }
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }
	

	public function uploadSetAppLog_post()
	{
	    
			$requestJson = json_decode(file_get_contents('php://input'));

			// foreach ($requestJson as $key => $value) 
			// {
			// 	$data = array(
			// 		"id" => $value->id,
			// 		"machineId" => $value->machineId,
			// 		"factoryId" => $value->factoryId,
			// 		"logTime" => $value->logTime,
			// 		"level" => $value->level,
			// 		"tag" => $value->tag,
			// 		"message" => $value->message,
			// 		"stacktrace" => $value->stacktrace,
			// 		"appId" => $value->appId,
			// 		"appVersion" => $value->appVersion,
			// 		"deviceName" => $value->deviceName,
			// 		"osVersion" => $value->osVersion,
			// 	); 
			// }

			$this->db->insert_batch('setAppErrorLog', $requestJson); 

			
			$json =  array("status" => "1","message" => "synced success","data" => $requestJson);
		   
		    
		
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }
}