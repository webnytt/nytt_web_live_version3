<?php
use Restserver\Libraries\REST_Controller;
defined('BASEPATH') OR exit('No direct script access allowed');

// This can be removed if you use __autoload() in config.php OR use Modular Extensions
/** @noinspection PhpIncludeInspection */
//To Solve File REST_Controller not found
require APPPATH . 'libraries/REST_Controller.php';
require APPPATH . 'libraries/Format.php';

/**
 * This is an example of a few basic user interaction methods you could use
 * all done with a hardcoded array
 *
 * @package         CodeIgniter
 * @subpackage      Rest Server
 * @category        Controller
 * @author          Phil Sturgeon, Chris Kacerguis
 * @license         MIT
 * @link            https://github.com/chriskacerguis/codeigniter-restserver
 */
class Oppapi extends REST_Controller {

    function __construct() {
        // Construct the parent class
        parent::__construct();
		$this->load->model('api3/Oppapi_model','APIM');
		set_time_limit(120);
		if (!empty($_POST['userId'])) 
		{
			$this->APIM->getLanguage($_POST['userId']);
		}
    }
	
	//signInOpApp() api use for oppApp login
	public function signInOpApp_post() 
	{ 
        $postArray = $this->post();
		
		//check required parameter validation
		if(isset($postArray['userName']) && isset($postArray['password'])) 
		{ 
			//check with userName and password for user exists or not 
			$userDetail = $this->APIM->getUser($postArray['userName'], $postArray['password']); 
			if(is_array($userDetail)) 
			{ 
				//if user role is admin then they can't login and send information message
				if ($userDetail['userRole'] == 2) 
				{
					$message = ['status'=>'0','message'=> "Administrator can not login to OpApp"];
				}else
				{
					
					$curr_time = time();
					$deviceId = !empty($postArray['deviceId']) ? $postArray['deviceId'] : "";
					$list['userId'] = intval($userDetail['userId']);
					$list['userName'] = $userDetail['userName'];
					if($userDetail['userRole'] == '0') 
					{
						$list['userRole'] = 'Operator';
					} else if($userDetail['userRole'] == '1') {
						$list['userRole'] = 'FactoryManager';
					} else {
						$roles = $this->APIM->getWhereDBSingle(array("rolesType" => $userDetail['userRole']),"roles");
						$list['userRole'] = $roles->rolesName;
					} 
					$list['userImage'] = $userDetail['userImage'];
					$list['userAge'] = $userDetail['userAge']; 	
					$list['userExperience'] = $userDetail['userExperience'];
					$list['languageType'] = $userDetail['languageType'];
					
					$list['userGetNotified'] = $userDetail['getNotified']; 
					$list['userGender'] = $userDetail['userGender'];
					$list['factoryId'] = $userDetail['factoryId'];
					$list['isMonitor'] = $userDetail['isMonitor'];
					$list['EmployeeId'] = $userDetail['EmployeeId'];
					$list['factoryName'] =  base64_decode($userDetail['factoryName']); 
					
					//sending the user detail for using in oppApp 
					$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
					
					//check deviceId exist or not 	
					if (isset($deviceId) && !empty($deviceId)) 
					{
						//check operator already login or not  
						if($userDetail['logKeyOpApp'] == "0") 
						{
							//Operator login successfully  and set deviceId and logDate
							$data = array("logDate"=>date('Y-m-d H:i:s', $curr_time),"deviceId" => $deviceId,
								"logKeyOpApp" => "1");
							$where = array("userId" => $userDetail['userId']);
							$this->APIM->updateData($where,$data,"user");
							
							$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
						
						}
						//Operator come with same deviceId and already login status then check this condition
						elseif ($deviceId == $userDetail['deviceId'] && $userDetail['logKeyOpApp'] == "1")  
						{
							//Operator login successfully  and set deviceId and logDate
							$data = array("logDate"=>date('Y-m-d H:i:s', $curr_time),"deviceId" => $deviceId,
								"logKeyOpApp" => "1");
							$where = array("userId" => $userDetail['userId']);
							$this->APIM->updateData($where,$data,"user");
							$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
						
						} 
						else 
						{
							//Operator login in other device then first logout from the FirstDevice
							$this->APIM->userLogOutOpApp($userDetail['userId'], $curr_time);

							//Operator login successfully  and set deviceId and logDate
							$data = array("logDate"=>date('Y-m-d H:i:s', $curr_time),"deviceId" => $deviceId,
								"logKeyOpApp" => "1","languageType" => "1");
							$where = array("userId" => $userDetail['userId']);
							$this->APIM->updateData($where,$data,"user");

							//sending notification for logout in the previous oppApp
							$this->APIM->sendNotification("Operator logged in from other phone","Operator logout",array($userDetail['deviceToken']),"operatorDelete",$list['factoryId']);
							
							$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
						}

					} else 
					{
						if($userDetail['logKeyOpApp'] == "0") 
						{ 	
							//Operator login successfully and setting the deviceId and logDate.
							$this->APIM->updateLogDate($userDetail['userId'], $curr_time); 
							$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
						} else 
						{
							//Operator will login in another device then they had to first logout from the Existing device
							$this->APIM->userLogOutOpApp($userDetail['userId'], $curr_time);
							//Operator login successfully  and set deviceId and logDate
							$data = array("logDate"=>date('Y-m-d H:i:s', $curr_time),"deviceId" => $deviceId,
								"logKeyOpApp" => "1");
							$where = array("userId" => $userDetail['userId']);
							$this->APIM->updateData($where,$data,"user");
							//sending the notification for logout in previous oppApp
							$this->APIM->sendNotification("Operator logged in from other phone","Operator logout",array($userDetail['deviceToken']),"operatorDelete",$list['factoryId']);
							
							$message = ['status'=>'1','message'=>SIGNIN_SUCCESS,'userDetail'=>$list];
						}
					}
					
				}

			} else 
			{
				//if the user credentials are not valid then return it to the signin page and display the error message 
				$message = ['status'=>'0','message'=>SIGNIN_ERROR];
			}
		} else 
		{
			$message = ['status'=>'0','message'=>INVALID_DETAILS];
		}

        $this->set_response($message, REST_Controller::HTTP_OK);
		
    } 
	
	//signOutOpApp() use for sign out from oppApp
	public function signOutOpApp_post() 
	{ 
        $postArray = $this->post();

		//checking for required parameter validation
		if(isset($postArray['userId'])) 
		{ 
			if(isset($_POST['endTime'])) 
			{
				$endTime = $_POST['endTime'];
			} else 
			{
				$endTime = time();
			}
			//updating the logout endTime and login flag 
			$userDetail = $this->APIM->userLogOutOpApp($postArray['userId'], $endTime);
			$message = ['status'=>'1','message'=>SIGNOUT_SUCCESS]; 
		} else 
		{
			$message = ['status'=>'0','message'=>INVALID_DETAILS];
		}

        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }
	
	//forgotPassword() api use for operator for forgot login detail for oppApp
	//forgotPassword() The Api is for the operator .When they have forgetted the Login credentials and for accessing again or for changing the password 
	
	public function forgotPassword_post() 
	{ 
        $postArray = $this->post();
        //check required parameter validation
		if(isset($postArray['userName'])) 
		{ 
			//check user is exist or not by userName
			$userDetail = $this->APIM->getUserPassword($postArray['userName']);
			if($userDetail != '') 
			{
				//get operator factory details by userName
				$factoryName = $this->APIM->getUserFactory($postArray['userName']);
				
				$msg = "Operator ".$postArray['userName']." has the following password ".$userDetail;
				$subject = "Operator forgot password";
				//sending an email to the admin for operator userName and password details
				$this->APIM->sendEmail($msg,$subject,"","",$postArray['factoryId']);

				$message = ['status'=>'1','message'=>FORGOT_EMAIL_SUCCESS]; 
				
			} else {
				$message = ['status'=>'0','message'=>INVALID_DETAILS];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS];
		} 
        $this->set_response($message, REST_Controller::HTTP_OK); 
    }
	
	//updateFCMToken api use for send notification in oppApp
	public function updateFCMToken_post() 
	{ 
        $postArray = $this->post();
		
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($postArray['userId']); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['userId']) && isset($postArray['deviceToken'])) 
		{  
			
			if(is_array($userDetail)) 
			{
				//update device token for send push notification
				$this->APIM->updateUserDevice($postArray['userId'], $postArray['deviceToken']);
				$message = ['status'=>'1',"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];

			} else 
			{
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		} 
        $this->set_response($message, REST_Controller::HTTP_OK); 
    }
	
	//updateNotificationStatus api use for update operator notification enable or disable in oppApp
	public function updateNotificationStatus_post() 
	{ 
	    $postArray = $this->post();
		
		$userId = $postArray['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['userId'])) { 
			if(is_array($userDetail)) 
			{
				//Checking that getNotified is NotNull or if null then set=1.
				if(isset($postArray['getNotified'])) 
				{
					$getNotified = $postArray['getNotified'];
				} else 
				{
					$getNotified = '1';
				}
				//Update getNotified field for operator get notification or not
				$updatedUser = $this->APIM->updateUserNotified($userId, $getNotified);
				
				$message = ['status'=>'1','message'=>NOTIFICATION_UPDATE_STATUS_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];

			} else {
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }
	

	//profileDetail api use for get operator details in oppApp
	public function profileDetail_post() 
	{ 
        $postArray = $this->post();
		
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetailCheck = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if( isset($postArray['userId'])) 
		{ 
			//get operator details
			$userDetail = $this->APIM->getUserDetail($postArray['userId']); 
			if(isset($userDetail)) 
			{	
				$list['userId'] = intval($userDetail->userId);
				$list['userName'] = $userDetail->userName;
				if($userDetail->userRole == '0') {
					$list['userRole'] = 'Operator';
				} else if($userDetail->userRole == '1') {
					$list['userRole'] = 'FactoryManager';
				} else {
					$list['userRole'] = $userDetail->userRole;
				} 
				$list['userImage'] = $userDetail->userImage;
				$list['userAge'] = $userDetail->userAge;
				$list['userExperience'] = $userDetail->userExperience;
				$list['userGetNotified'] = $userDetail->getNotified; 
				$list['userGender'] = $userDetail->userGender;
				$list['receiveReminders'] = $userDetail->receiveReminders;
				$list['receiveErrorNotifiction'] = $userDetail->receiveErrorNotifiction;
				$list['provideReasonForErrors'] = $userDetail->provideReasonForErrors;
				$list['receiveWaitingNotification'] = $userDetail->receiveWaitingNotification;
				$list['provideReasonForWaiting'] = $userDetail->provideReasonForWaiting;
				$list['warningMachineId'] = $userDetail->warningMachineId;
				
				$message = ['status'=>'1','userDetail'=>$list,'message'=>USER_ACCESS_SUCCESS,"isUserDeleted" => empty($userDetailCheck) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			} else 
			{
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetailCheck) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetailCheck) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
		$this->set_response($message, REST_Controller::HTTP_OK);
		
    }
	
	//updateProfile api use for updating operator details in oppApp
	public function updateProfile_post() 
	{ 
        $postArray = $this->post();
		
		$userId = $postArray['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['userId'])) 
		{ 
			//check user exists or not 
			$userNameExists = $this->APIM->userNameExists($postArray['userName'],$userId);

			//if user exist then update user details
			if (empty($userNameExists)) 
			{
				if(is_array($userDetail)) 
				{
					if(isset($postArray['userName'])) {
						$userName = $postArray['userName'];
					} else {
						$userName = '';
					}
					if(isset($postArray['userGender'])) {
						$userGender = $postArray['userGender'];
					} else {
						$userGender = '';
					} 
					if(isset($postArray['userAge'])) {
						$userAge = $postArray['userAge'];
					} else {
						$userAge = '';
					}
					if(isset($postArray['userExperience'])) {
						$userExperience = $postArray['userExperience'];
					} else {
						$userExperience = '';
					}
					if(isset($_FILES['userImage'])) {  
						if (file_exists(realpath(APPPATH . '../assets/img/user/'.$userDetail['userImage']))) {
							unlink(realpath(APPPATH . '../assets/img/user/'.$userDetail['userImage'])); 
						}
						$path_parts = pathinfo($_FILES["userImage"]["name"]);
						$file_ext = $path_parts['extension'];
						
						$config = array(
							'upload_path' =>  realpath(APPPATH . '../assets/img/user/'), 
							'allowed_types' => "jpg|png|jpeg",
							'overwrite' => TRUE,
							'max_size' => "2048000", 
							'max_height' => "2048",
							'max_width' => "2048",
							'file_name'=>$userId. "." .$file_ext 
							); 
						$this->load->library('upload', $config);
						if($this->upload->do_upload('userImage'))
						{  
							$data = array('upload_data' => $this->upload->data());
							$config1 = array(
								'upload_path' =>  $_SERVER['DOCUMENT_ROOT'].'/testing_env/version3/assets/img/user/', 
								'allowed_types' => "jpg|png|jpeg",
								'overwrite' => TRUE,
								'max_size' => "2048000", // Can be set to particular file size , here it is 2 MB(2048 Kb)
								'max_height' => "2048",
								'max_width' => "2048",
								'file_name'=>$userId. "." .$file_ext 
								);


			                $this->load->library('upload', $config1);
			                $this->upload->initialize($config1);
							$this->upload->do_upload('userImage');


						} else {
							$error = array('error' => $this->upload->display_errors()); 
						}
						
						$userImage = $userId. "." .$file_ext; 
					
					} else {
						$userImage = '';
					}
					$updatedUser = $this->APIM->updateUser($userId, $userName, $userGender, $userAge, $userExperience, $userImage);
					
					$message = ['status'=>'1','message'=>UPDATE_PROFILE_SUCCESS,'userImage'=>$userImage,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];

				} else {
					$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				}
			}else
			{
				$message = ['status'=>'0','message'=>USERNAME_ERROR,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		} 
        $this->set_response($message, REST_Controller::HTTP_OK);
    }
	
	public function checkIn_post() 
	{ 
        $postArray = $this->post();
		
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($postArray['userId']); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if( isset($postArray['userId']) && isset($postArray['factoryId']) ) { 
			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
			if(is_array($factoryDetail)) {
				if(isset($_POST['startTime'])) {
					$startTime = $_POST['startTime'];
				} else {
					$startTime = time();
				}
				if(is_array($userDetail) && isset($startTime)) {
					
					//get operators assign machines
					
					
					$monitorMessage = "";
					$monitorStatus = $this->input->post('monitorStatus');
					if (!empty($userDetail['EmployeeId']) && $monitorStatus == 1) 
					{

						
						$data = array(
						      'Username' => 'API_USER',
						      'Password' => '',
						      'ForceRelogin' => true,
						  );
						    
						$post_data = json_encode($data);


						$username='Laufey';
						$password='Cdee3232';

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						$headers = [
						    'Accept: application/json',
						    'Host:srv01.precima.local:8001',
						    'Content-Type: application/json',
						    'Cache-Control: no-cache',
						];
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
						curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

						$server_output = curl_exec ($ch);

						curl_close ($ch);

						$server_output = json_decode($server_output);

						$SessionId =  $server_output->SessionId;

						$ClockInData = array(
						      'EmployeeId' => $userDetail['EmployeeId'],
						      'TimeStamp' => null,
						      'Message' => null,
						      'AbsenceCodeId' => null,
						      'OvertimeTypeId' => null,
						  );
						    
						$ClockInPostData = json_encode($ClockInData);


						$username='Laufey';
						$password='Cdee3232';

						$chClockIn = curl_init();
						curl_setopt($chClockIn, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/TimeRecording/RecordingDays/ClockIn");
						curl_setopt($chClockIn, CURLOPT_POST, 1);
						curl_setopt($chClockIn, CURLOPT_POSTFIELDS,$ClockInPostData);  //Post Fields
						curl_setopt($chClockIn, CURLOPT_RETURNTRANSFER, true);

						$clockInHeaders = [
						    'Accept: application/json',
						    'Cache-Control:no-cache',
						    'Host: 212.16.184.90:8001',
						    'X-Monitor-SessionId: '.$SessionId,
						    'Content-Type: application/json',
						];

						curl_setopt($chClockIn, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
						curl_setopt($chClockIn, CURLOPT_USERPWD, "$username:$password");
						curl_setopt($chClockIn, CURLOPT_HTTPHEADER, $clockInHeaders);

						curl_setopt($chClockIn, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($chClockIn, CURLOPT_SSL_VERIFYPEER, false);

						$clockInOutput = curl_exec ($chClockIn);

						curl_close ($chClockIn);

						$ClockInPostData = json_decode($clockInOutput);

						if (is_object($ClockInPostData))
						{
						 	$monitorMessage = " ".Monitorcheckinsuccessfully;
						 	$monitorStatus = 1;

						 	$userMachines = $this->APIM->getAssignedMachines($postArray['factoryId'], $_POST['userId']);
							for($i=0;$i<count($userMachines);$i++) {
								$machinesArr[$i] = $userMachines[$i]->machineId;
								$machineNamesArr[$i] = $userMachines[$i]->machineName;
							}
							$machines = implode(",",$machinesArr);
							$machineNames = implode(", ",$machineNamesArr);
							$workingMachine = !empty($this->input->post('workingMachine')) ? $this->input->post('workingMachine') : "";
							$isViewIn = !empty($this->input->post('isViewIn')) ? $this->input->post('isViewIn') : "1";
							
							//add operator checkin details
							$activeId = $this->APIM->userCheckIn($postArray['factoryId'], $machines, $machineNames, $_POST['userId'], $startTime,$workingMachine,$isViewIn);
							$pendingMNotifications = $this->APIM->getPendingMNotifications($postArray['factoryId'], $_POST['userId'], $startTime);
						}else
						{
							$monitorMessage = " ".Monitornotcheckin;
							$monitorStatus = 0;
						}

					}else
					{
						$monitorStatus = 1;
						$userMachines = $this->APIM->getAssignedMachines($postArray['factoryId'], $_POST['userId']);
						for($i=0;$i<count($userMachines);$i++) {
							$machinesArr[$i] = $userMachines[$i]->machineId;
							$machineNamesArr[$i] = $userMachines[$i]->machineName;
						}
						$machines = implode(",",$machinesArr);
						$machineNames = implode(", ",$machineNamesArr);
						$workingMachine = !empty($this->input->post('workingMachine')) ? $this->input->post('workingMachine') : "";
						$isViewIn = !empty($this->input->post('isViewIn')) ? $this->input->post('isViewIn') : "1";
						
						//add operator checkin details
						$activeId = $this->APIM->userCheckIn($postArray['factoryId'], $machines, $machineNames, $_POST['userId'], $startTime,$workingMachine,$isViewIn);
						$pendingMNotifications = $this->APIM->getPendingMNotifications($postArray['factoryId'], $_POST['userId'], $startTime);
					}
					$message = ['status'=>'1','message'=>CHECKIN_SUCCESS.$monitorMessage,'activeId'=>$activeId,"monitorStatus" => $monitorStatus,"monitorMessage" => $monitorMessage ,'list'=>$pendingMNotifications,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];

				} else {
					$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				}
			} else {
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
        $this->set_response($message, REST_Controller::HTTP_OK);
		
	}



	public function checkOut_post() 
	{  
        $postArray = $this->post();
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
        if(isset($postArray['activeId']) && isset($postArray['factoryId'])) 	
        {   
			$activeId = $postArray['activeId'];
			$i = 0;

			if(isset($_POST['endTime'])) 
			{
				$endTime = $_POST['endTime'];
			} else 
			{
				$endTime = time();
			}

			$factoryId = $postArray['factoryId'];
			
			$result = $this->APIM->checkInData($factoryId, $activeId);
			//Cross verify to check operator already checkIn or not

			if (!empty($result)) 
			{

				//Update operator checkout details
				



					$monitorMessage = "";
					$monitorStatus = $this->input->post('monitorStatus');
					if (!empty($userDetail['EmployeeId']) && $monitorStatus == 1) 
					{
						
						$data = array(
						      'Username' => 'API_USER',
						      'Password' => '',
						      'ForceRelogin' => true,
						  );
						    
						$post_data = json_encode($data);


						$username='Laufey';
						$password='Cdee3232';

						$ch = curl_init();
						curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
						curl_setopt($ch, CURLOPT_POST, 1);
						curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
						curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

						$headers = [
						    'Accept: application/json',
						    'Host:srv01.precima.local:8001',
						    'Content-Type: application/json',
						    'Cache-Control: no-cache',
						];
						curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
						curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
						curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

						curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

						$server_output = curl_exec ($ch);

						curl_close ($ch);

						$server_output = json_decode($server_output);

						$SessionId =  $server_output->SessionId;

						$ClockInData = array(
						      'EmployeeId' => $userDetail['EmployeeId'],
						      'TimeStamp' => null,
						      'Message' => null,
						      'AbsenceCodeId' => null,
						      'OvertimeTypeId' => null,
						  );
						    
						$ClockInPostData = json_encode($ClockInData);


						$username='Laufey';
						$password='Cdee3232';

						$chClockIn = curl_init();
						curl_setopt($chClockIn, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/TimeRecording/RecordingDays/ClockOut");
						curl_setopt($chClockIn, CURLOPT_POST, 1);
						curl_setopt($chClockIn, CURLOPT_POSTFIELDS,$ClockInPostData);  //Post Fields
						curl_setopt($chClockIn, CURLOPT_RETURNTRANSFER, true);

						$clockInHeaders = [
						    'Accept: application/json',
						    'Cache-Control:no-cache',
						    'Host: 212.16.184.90:8001',
						    'X-Monitor-SessionId: '.$SessionId,
						    'Content-Type: application/json',
						];

						curl_setopt($chClockIn, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
						curl_setopt($chClockIn, CURLOPT_USERPWD, "$username:$password");
						curl_setopt($chClockIn, CURLOPT_HTTPHEADER, $clockInHeaders);

						curl_setopt($chClockIn, CURLOPT_SSL_VERIFYHOST, false);
						curl_setopt($chClockIn, CURLOPT_SSL_VERIFYPEER, false);

						$clockInOutput = curl_exec ($chClockIn);

						curl_close ($chClockIn);

						$ClockInPostData = json_decode($clockInOutput);

						if (is_object($ClockInPostData))
						{
						 	$monitorMessage = " ".Monitorcheckoutsuccessfully;
						 	$monitorStatus = 1;
						 	$data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s',$endTime)); 
							$this->APIM->updateCheckOut($factoryId,$activeId,$data);

							$response1 = $this->APIM->addMNotificationAll($factoryId, $activeId, 1, $endTime); 
							$response[$i] = $response1; $i++;
							
							//Adding weekly noification
							if(date('D') == 'Fri') { 
								$response2 = $this->APIM->addMNotificationAll($factoryId, $activeId, 2, $endTime); 
								$response[$i] = $response2; $i++;
							}
							
							//Adding monthly noification
							if(date('d') == date('t')) { 
								$response3 = $this->APIM->addMNotificationAll($factoryId, $activeId, 3, $endTime); 
								$response[$i] = $response3; $i++;
							}
							
							//Adding quarterly noification
							if(date('d') == date('t') && date('m') % 3 == 0) { 
								$response4 = $this->APIM->addMNotificationAll($factoryId, $activeId, 4, $endTime); 
								$response[$i] = $response4; $i++;
							}
							
							//Adding yearly noification
							if(date('m-d') == '12-31') { 
								$response5 = $this->APIM->addMNotificationAll($factoryId, $activeId, 5, $endTime); 
								$response[$i] = $response5; $i++;
							}
						}else
						{
							$monitorMessage = " ".Monitornotcheckout;
							$monitorStatus = 0;
						}

					}else
					{
						$monitorStatus = 1;
						$data = array("isActive" => "0","endTime" => date('Y-m-d H:i:s',$endTime)); 
						$this->APIM->updateCheckOut($factoryId,$activeId,$data);

						$response1 = $this->APIM->addMNotificationAll($factoryId, $activeId, 1, $endTime); 
						$response[$i] = $response1; $i++;
						
						//Adding weekly noification
						if(date('D') == 'Fri') { 
							$response2 = $this->APIM->addMNotificationAll($factoryId, $activeId, 2, $endTime); 
							$response[$i] = $response2; $i++;
						}
						
						//Adding monthly noification
						if(date('d') == date('t')) { 
							$response3 = $this->APIM->addMNotificationAll($factoryId, $activeId, 3, $endTime); 
							$response[$i] = $response3; $i++;
						}
						
						//Adding quarterly noification
						if(date('d') == date('t') && date('m') % 3 == 0) { 
							$response4 = $this->APIM->addMNotificationAll($factoryId, $activeId, 4, $endTime); 
							$response[$i] = $response4; $i++;
						}
						
						//Adding yearly noification
						if(date('m-d') == '12-31') { 
							$response5 = $this->APIM->addMNotificationAll($factoryId, $activeId, 5, $endTime); 
							$response[$i] = $response5; $i++;
						}
					}

				
				$message = ['status'=>'1','message'=>CHECKOUT_SUCCESS.$monitorMessage,"monitorStatus" => $monitorStatus,"monitorMessage" => $monitorMessage, 'list'=>$response,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			} else 
			{
				//set checkout error message
				$message = ['status'=>'0','message'=>CHECKOUT_ERROR,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			}
		} else 
		{
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}

		$this->set_response($message, REST_Controller::HTTP_OK);		
	}


	//reportAProblem api use for add any query about app in oppApp
	public function reportAProblem_post()
	{
	    $this->form_validation->set_rules('type', 'type', 'required');
	    $this->form_validation->set_rules('date', 'date', 'required');
	    $this->form_validation->set_rules('description', 'description', 'required');
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('status', 'status', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$type  = $this->input->post('type');
			$date  = date('Y-m-d',strtotime($this->input->post('date')));
			$description  = $this->input->post('description');
			$status  = $this->input->post('status');

			$picture = "";
			$pictureStatus =  ture;
			if (!empty($_FILES['picture']['name'])) 
			{
				$allowed =  array('jpeg','png','jpg');
				$ext = pathinfo($_FILES["picture"]["name"], PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) 
				{	
					$pictureStatus =  false;
					$errorMessage = 'Please upload valid image.'; 
				}else
				{

					$config = array(
				        'upload_path' => './assets/img/reportProblem',
				        'allowed_types' => "*",
				        'encrypt_name' => TRUE
				     );
			        $this->load->library('upload', $config);
			        if(!$this->upload->do_upload('picture'))
			        { 
			        	$pictureStatus =  false;
			            $errorMessage = $this->upload->display_errors();
			        }
			        else
			        {
			            $picture = $this->upload->data('file_name');
			        }
			    }
			}

			if ($pictureStatus == ture) 
			{
				$data = array(
						"userId" => $userId,
						"type" => $type,
						"date" => $date,
						"description" => $description,
						"picture" => $picture,
						"status" => $status
					);

				//Add problem log
				$this->APIM->insertProblem($factoryId,$data);
					
				$json =  array("status" => "1","message" => Problemaddedsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}else
			{
				$json =  array("status" => "0","Name" => $ext,"message" => strip_tags($errorMessage),"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //help api use for get help screen data in oppApp
    public function help_get() 
    {	
		$helpDetail = $this->APIM->getHelpText(); 
		$problemReportDetail = $this->APIM->getProblemReportText(); 
		if(is_array($helpDetail)) {
			
			$help = $helpDetail['infoText']; 
			$problemReport = $problemReportDetail['infoText']; 
			$message = ['status'=>'1','message'=>HELP_MESSAGE,'content'=>$help, 'problemReport'=>$problemReport]; 

		} else 
		{
			$message = ['status'=>'0','message'=>EMPTY_HELP];  

		}
        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }
	
	//helpReport api use for operator need help in oppApp
	public function helpReport_post() 
	{ 		
		$postArray = $this->post();
		$fileArray = $_FILES;  
		if(isset($postArray['currentTime'])) 
		{
			$currentTime = $postArray['currentTime'];
		} else {
			$currentTime = time();
		} 
		
		$userId = $postArray['userId']; 
		//check user is exists or not 
		$userDetail = $this->APIM->getUserDetail($userId); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if (isset($postArray['problemText']) && isset($postArray['userId']) && isset($postArray['factoryId']) && isset($currentTime) ) 
		{
			$factoryId = $postArray['factoryId'];
			$problemText = $postArray['problemText'];
			if(isset($postArray['workDuration'])) {
				$workDuration = $postArray['workDuration'];  
			} else {
				$workDuration = ''; 
			}
			$application = strtolower($postArray['application']);  
			$factoryName = $this->APIM->getFactoryDetail($factoryId); 
			
			
			
			$msg = "Following message has been reported by operator ".$userDetail->userName.": <br>
				    Message: ". $problemText;

			
			$subject = "Operator reports a problem";
			
			//sending an email with attachments
			if(isset($fileArray['image']) ) { 
				$allowed =  array('jpeg','png','jpg','mov','mp4','pdf','txt','doc','docx');
				$ext = pathinfo($fileArray["image"]["name"], PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed) ) 
				{	
					$message = [ 'status' => "0",'message'=>'Please upload valid image, video or document.']; 
				}else
				{
					$path_parts = pathinfo($fileArray["image"]["name"]);
					$file_ext = $path_parts['extension'];
					
					//upload attachment in folder
					move_uploaded_file($fileArray['image']['tmp_name'], "../report/factory".$factoryId."/" . $userId . "_" . $currentTime. "." .$file_ext);
					//add help report data
					$insert = $this->APIM->addHelpReport($factoryId, $userId, $currentTime, $problemText, $workDuration, $file_ext);

					$file_path =  "https://nyttcloud.host/prodapp/report/factory".$factoryId.'/' .$userId . '_' . $currentTime. '.' .$file_ext;
					
					//sending an email with attachments as a data to admin
					
					$this->APIM->sendEmail($msg,$subject,$file_path,$file_ext,$factoryId);
					$message = [ 'status' => "1",'message'=>PROBLEM_REPORT_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
					
				} 
				
			} else {
				$insert = $this->APIM->addHelpReport($factoryId, $userId, $currentTime, $problemText, $workDuration, ''); 

				//sending an email to admin
				$this->APIM->sendEmail($msg,$subject,"","",$factoryId);
				$message = [ 'status' => "1",'message'=>PROBLEM_REPORT_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
				
			}
		
		} else {
			$message = [ 'status' => "0",'message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
		
        $this->set_response($message, REST_Controller::HTTP_OK);
		
    }

    //respond() api use for checkOut time maintenance data in oppApp
    public function respond_post() 
    { 
        $postArray = $this->post();
		
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not

		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($_POST['notificationId']) && isset($_POST['star']) && isset($_POST['status']) && isset($postArray['factoryId']) ) 
		{
			//get factory details
			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']);  
			if(is_array($factoryDetail)) {
				$activeDetail = $this->APIM->mNotificationExists($postArray['factoryId'], $_POST['notificationId']); 
				if(isset($_POST['respondTime'])) {
					$respondTime = $_POST['respondTime'];
				} else {
					$respondTime = time();
				}
				if(is_array($activeDetail) && isset($respondTime)) { 
					
					$star = $_POST['star'];
					$status = $_POST['status'];
					//getting the maintenance data
					$response = $this->APIM->respondMNotification($postArray['factoryId'], $_POST['notificationId'], $respondTime, $status, $star);
					
					$message = ['status'=>'1','message'=>MNOTIFICATION_RESPOND_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,'response'=>$response]; 
				} else {
					$message = ['status'=>'0','message'=>MNOTIFICATION_RESPOND_ERROR,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];

				}
			}else {
				$message = ['status'=>'0', 'message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			}
		} else {	
			$message = ['status'=>'0', 'message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		$this->set_response($message, REST_Controller::HTTP_OK);
	} 

	//assignedListBeta() api use for getting assigned machine by operator and reviewing the data like running other detail set in oppApp 
	public function assignedListBeta_post() 
	{ 
        $postArray = $this->post();

        $userId = $_POST['userId'];
        //check user is exists or not 
		$userDetail = $this->APIM->userExists($userId); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['userId']) && isset($postArray['factoryId'])) 
		{ 

			$resultCheckIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId);

			if (!empty($resultCheckIn)) 
			{
				$checkInStartTime = $resultCheckIn['startTime'];
			}else
			{
				$checkInStartTime = "";
			}

			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
			if(is_array($factoryDetail)) {
				if(isset($postArray['oppApp']) && $postArray['oppApp'] == '1' ) { //oppapp response 
					$checkInStatus = '0';
					if(isset($_POST['currentTime'])) {
						$currentTime = date('Y-m-d', $_POST['currentTime']);
					} else {
						$currentTime = date('Y-m-d'); 
					} 
					
					
					$machines = array();
					if(intVal($userDetail['userRole']) > 0 ) { 
					//If role greater than operator
						$isOperator = 0;
					} else { //
						$isOperator = 1;
					}
					//get operator assigned machine 
					$machines = $this->APIM->getMachinesOverview($postArray['factoryId'], $isOperator, $postArray['userId'])->result();
					
					
					if( count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {
							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;
							
							//cross verify check battery details added or not
							if ($machines[$i]->chargeFlag != NULL) 
							{
								//setting the battery percentage and  charger is connected or not
								$list[$i]['phoneId'] = $machines[$i]->phoneId;
								$list[$i]['batteryLevel'] = $machines[$i]->batteryLevel;
								$list[$i]['chargeFlag'] = $machines[$i]->chargeFlag;
							} else 
							{ 
								//set default battery data 
								$list[$i]['phoneId'] = 0;
								$list[$i]['batteryLevel'] = 0;
								$list[$i]['chargeFlag'] = "0";
							}
							
							//get setApp machine current status
							$isSetAppOn = $this->APIM->checkSetApp($postArray['factoryId'], intval($machines[$i]->machineId)); 
							if(isset($isSetAppOn) && is_array($isSetAppOn)){
								$list[$i]['isSetAppOn'] = $isSetAppOn['isActive'];
								if($isSetAppOn['reason'] == 'client namespace disconnect' || $isSetAppOn['reason'] == 'transport close' || $isSetAppOn['reason'] == 'ping timeout') {
									$list[$i]['setAppAlert'] = SETAPP_TURNED_OFF;
								} else if($isSetAppOn['reason'] == 'ping timeout') { 
									$list[$i]['setAppAlert'] = SETAPP_NO_INTERNET;
								} 
							} else {
								$list[$i]['isSetAppOn'] = 0;
								$list[$i]['setAppAlert'] = SETAPP_NOT_STARTED; 
							}
							
							//get operator check in data
							$checkIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId); 
							  
								$checkInStatus = '1'; 
								$activeId = isset($checkIn['activeId'])?$checkIn['activeId']:0;
								
								$machineLightArr[$i] = explode(",", $machines[$i]->machineLight); 
								$listColors = $this->APIM->getAllColors();
								for($z=0;$z<count($machineLightArr[$i]);$z++) {
									for($y=0;$y<count($listColors);$y++) { 
										if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $list[$i]['machineLightColors'][$z] = $listColors[$y]->colorCode;
									}
													
									$machineLightStatus['redStatus'] = '0';
									$machineLightStatus['yellowStatus'] = '0';
									$machineLightStatus['greenStatus'] = '0';
									$machineLightStatus['whiteStatus'] = '0';
									$machineLightStatus['blueStatus'] = '0';
									
									$color = $machines[$i]->color; 
									
									$colorArr = explode(" ", $color);
									for($c=0;$c<count($colorArr);$c++) {
										if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
											$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
										}
									}
									
									if(strpos($color, 'Blue') !== false) {
										$machineLightStatus['blueStatus'] = '1';
									}
									if(strpos($color, 'Red') !== false) {
										$machineLightStatus['redStatus'] = '1';
									}
									if(strpos($color, 'Red and green') !== false) {
										$machineLightStatus['redStatus'] = '1';
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Yellow') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
									}
									if(strpos($color, 'Green') !== false) {
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Yellow and green') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Green and yellow') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Red and yellow') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
										$machineLightStatus['redStatus'] = '1';
									}
									if(strpos($color, 'Robot') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
									}
									if(strpos($color, 'Door') !== false) {
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Red Yellow Green') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
										$machineLightStatus['redStatus'] = '1';
										$machineLightStatus['greenStatus'] = '1';
									}
									if(strpos($color, 'Blue and yellow') !== false) {
										$machineLightStatus['yellowStatus'] = '1';
										$machineLightStatus['blueStatus'] = '1';
									}
										
									if($list[$i]['machineLightColors'][$z] == 'FF0000') {
										$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
										$list[$i]['machineLightColorsStatusNames'][$z] = 'redStatus';
									}
									if($list[$i]['machineLightColors'][$z] == 'FFFF00') {
										$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
										$list[$i]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
									}
									if($list[$i]['machineLightColors'][$z] == '00FF00') {
										$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
										$list[$i]['machineLightColorsStatusNames'][$z] = 'greenStatus';
									}
									if($list[$i]['machineLightColors'][$z] == 'FFFFFF') {
										$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
										$list[$i]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
									} 
									if($list[$i]['machineLightColors'][$z] == '0000FF') {
										$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
										$list[$i]['machineLightColorsStatusNames'][$z] = 'blueStatus';
									} 
									
									$list[$i]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
									$list[$i]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
									$list[$i]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
									$list[$i]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
									$list[$i]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
									
									$list[$i]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($postArray['factoryId'], $machines[$i]->machineId, $list[$i]['machineLightStatus']['redStatus'], $list[$i]['machineLightStatus']['greenStatus'], $list[$i]['machineLightStatus']['yellowStatus'], $list[$i]['machineLightStatus']['whiteStatus'],$list[$i]['machineLightStatus']['blueStatus']); 
									
									if($machines[$i]->logId != 0) {
										if($color == 'NoData' || $color == 'NoDataStacklight' || $color == 'NoDataError' || $color == 'NoDataHome' || $color == 'NoDataForceFully' || $color == 'NoDataRestart') { 
											$list[$i]['isSetAppOn'] = 0;
											if(!isset($list[$i]['setAppAlert'])) {
												$list[$i]['setAppAlert'] = NO_DETECTION; 
											}
										} else {
											$list[$i]['isSetAppOn'] = 1;
											$list[$i]['setAppAlert'] = '';
										}  
									} 
								}
							$message = ""; 
							if(is_array($checkIn) || $userDetail['userRole'] > 0 ) 
							{
								$message = "Factorymanager can not make any input"; 
							} else {
								$activeId = "0";
								$checkInStatus = "0";
								$message = "View only selected. Please check in to make any inputs.";
							} 
						} 
						$message = ['status'=>'1','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'activeId'=>$activeId,'message'=> $message,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'list'=>$list]; 
					} else {
						$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'message'=>NO_ASSIGN,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} 
					
				} else { 
				//setapp response 
				//get all machine for setApp 
					$machines = $this->APIM->getAssignedMachines($postArray['factoryId'], $postArray['userId']); 
					if(is_array($machines) && count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {
							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;
						}
						$message = ['status'=>'1',"checkInStartTime" => $checkInStartTime,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'list'=>$list,'message'=>MACHINE_LIST_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} else {
						$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>NO_ASSIGN,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
					}
				}
			} else {
				$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>INVALID_DETAILS];
			}
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS];
		}
        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    //setAppStart() api used for start a setApp detection by operator in oppApp 
    public function setAppStart_post() 
    { 
        $postArray = $this->post();

		$userId = $postArray['userId']; 
		$factoryId = $postArray['factoryId']; 
		//check user is exists or not 
		$userDetail = $this->APIM->getUserDetail($userId); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

		//check required parameter validation
		if (isset($postArray['factoryId']) && isset($postArray['machineId']) && isset($postArray['userId'])) 
		{   
			
			$machineId = $postArray['machineId'];
			//get last setApp machine data for sending notifiction 
			$machineUser = $this->APIM->getLastActiveMachine($factoryId,$machineId);

			$fcmToken = array($machineUser->deviceToken);
			$message = "SetApp started successfully. Camera will start in next 30 seconds for selected machine";
			$title = "Set app start";
			$type = "setAppStart";
			$activeId = $machineUser->activeId;

			if (!empty($machineUser) && $machineUser->isActive == "1") 
			{
				//sending the notification to start setApp detection
				$this->APIM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId,$userId);
				$message = [ 'status' => "1","message" => SetAppstartedsuccessfullyCamerawillstartinnext30secondsfor,"isUserDeleted" => empty($userDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}else
			{
				//if the machine is not started in any setApp
				$message = [ 'status' => "0","message" => OpAppcouldnotfindanyloggedinSetAppthathasselectedcurrentmachine,"isUserDeleted" => empty($userDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  				
			}

				
		} else {
			$message = [ 'status' => "0",'message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    //setAppReStart() api used for restart a setApp  by operator in oppApp 
    public function setAppReStart_post() 
    { 
        $postArray = $this->post();

		$userId = $postArray['userId']; 
		$factoryId = $postArray['factoryId']; 
		//check user is exists or not 
		$userDetail = $this->APIM->getUserDetail($userId); 
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

		//check required parameter validation
		if (isset($postArray['factoryId']) && isset($postArray['machineId']) && isset($postArray['userId'])) 
		{   
			
			$machineId = $postArray['machineId'];
			//get last setApp machine data for sending notifiction 
			$machineUser = $this->APIM->getLastActiveMachine($factoryId,$machineId);

			$fcmToken = array($machineUser->deviceToken);
			$message = SetApprestartedsuccessfully;
			$title = "Set app restart";
			$type = "setAppReStart";
			$activeId = $machineUser->activeId;

			if (!empty($machineUser) && $machineUser->isActive == "1") 
			{
				//sending the notification to restart setApp
				$data = array("userId" => $userId,"machineId" => $machineId,"insertTime" => date('Y-m-d H:i:s'));
				$this->APIM->insertFactoryData($factoryId,$data,'setAppReStartLog');
				$this->APIM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId,$userId);
				$message = [ 'status' => "1","message" => SetApprestartedsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}else
			{
				//if the machine is not started in any setApp
				$message = [ 'status' => "0","message" => OpAppcouldnotfindanyloggedinSetAppthathasselectedcurrentmachine,"isUserDeleted" => empty($userDetail) ? 1 : 0 ,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  				
			}

				
		} else {
			$message = [ 'status' => "0",'message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    //statistics()
    //This Ap is used for getting the Deep Overview Graph data in oppApp
    public function statistics_post()
	{
	    $this->form_validation->set_rules('choose1', 'choose1', 'required');
	    $this->form_validation->set_rules('choose2', 'choose2', 'required');
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

	    //check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
		
		//check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
		}
		else
		{  
			$choose1  = $this->input->post('choose1');
			$choose2  = $this->input->post('choose2');
			$machineId  = $this->input->post('machineId');

			//ActualProduction
			//Get Actual production data by machine state
			$resultActualProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','running');
			$ActualProductionRunning = $resultActualProductionRunning->countVal;
			$resultActualProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','waiting');
			$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
			$resultActualProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','stopped');
			$ActualProductionStopped = $resultActualProductionStopped->countVal;
			$resultActualProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','off');
			$ActualProductionOff = $resultActualProductionOff->countVal;
			$resultActualProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','nodet');
			$ActualProductionNodet = $resultActualProductionNodet->countVal;

			$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet;

			//Setup

			//Get setup data by machine state
			$resultSetupRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','running');
			$SetupRunning = $resultSetupRunning->countVal;
			$resultSetupWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','waiting');
			$SetupWaiting = $resultSetupWaiting->countVal;
			$resultSetupStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','stopped');
			$SetupStopped = $resultSetupStopped->countVal;
			$resultSetupOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','off');
			$SetupOff = $resultSetupOff->countVal;
			$resultSetupNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','nodet');
			$SetupNodet = $resultSetupNodet->countVal;

			$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet;

			//No production
			//Getting no production data by machine state
			$resultNoProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','running');
			$NoProductionRunning = $resultNoProductionRunning->countVal;
			$resultNoProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','waiting');
			$NoProductionWaiting = $resultNoProductionWaiting->countVal;
			$resultNoProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','stopped');
			$NoProductionStopped = $resultNoProductionStopped->countVal;
			$resultNoProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','off');
			$NoProductionOff = $resultNoProductionOff->countVal;
			$resultNoProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','nodet');
			$NoProductionNodet = $resultNoProductionNodet->countVal;

			$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet;

			
		
			//$total = $ActualProduction;
			$total = $ActualProduction + $Setup + $NoProduction;

			$AllActualProduction =  !empty($ActualProduction) ? ($ActualProduction * 100) / $total : 0;
			$AllSetup = !empty($Setup) ?  ($Setup * 100) / $total : 0;
			$AllNoProduction =  !empty($NoProduction) ? ($NoProduction * 100) / $total : 0;


			$ActualProductionRunning =  !empty($ActualProductionRunning) ? ($ActualProductionRunning * 100) / $total : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ?  ($ActualProductionWaiting * 100) / $total : 0;
			$ActualProductionStopped =  !empty($ActualProductionStopped) ? ($ActualProductionStopped * 100) / $total : 0;
			$ActualProductionOff =  !empty($ActualProductionOff) ? ($ActualProductionOff * 100) / $total : 0;
			$ActualProductionNodet =  !empty($ActualProductionNodet) ? ($ActualProductionNodet * 100) / $total : 0;

			$SetupRunning =  !empty($SetupRunning) ? ($SetupRunning * 100) / $total : 0;
			$SetupWaiting = !empty($SetupWaiting) ?  ($SetupWaiting * 100) / $total : 0;
			$SetupStopped =  !empty($SetupStopped) ? ($SetupStopped * 100) / $total : 0;
			$SetupOff =  !empty($SetupOff) ? ($SetupOff * 100) / $total : 0;
			$SetupNodet =  !empty($SetupNodet) ? ($SetupNodet * 100) / $total : 0;

			$NoProductionRunning =  !empty($NoProductionRunning) ? ($NoProductionRunning * 100) / $total : 0;
			$NoProductionWaiting = !empty($NoProductionWaiting) ?  ($NoProductionWaiting * 100) / $total : 0;
			$NoProductionStopped =  !empty($NoProductionStopped) ? ($NoProductionStopped * 100) / $total : 0;
			$NoProductionOff =  !empty($NoProductionOff) ? ($NoProductionOff * 100) / $total : 0;
			$NoProductionNodet =  !empty($NoProductionNodet) ? ($NoProductionNodet * 100) / $total : 0;


			if (!empty($AllActualProduction) || !empty($AllSetup) || !empty($AllNoProduction)) 
			{
				$data = array(
						"ActualProductionRunning" 		=> $ActualProductionRunning,
						"ActualProductionWaiting" 		=> $ActualProductionWaiting,
						"ActualProductionStopped" 		=> $ActualProductionStopped,
						"ActualProductionOff" 			=> $ActualProductionOff,
						"ActualProductionNodet" 		=> $ActualProductionNodet,
						"ActualProduction" 				=> $AllActualProduction,
						"SetupRunning" 					=> $SetupRunning,
						"SetupWaiting" 					=> $SetupWaiting,
						"SetupStopped" 					=> $SetupStopped,
						"SetupOff" 						=> $SetupOff,
						"SetupNodet" 					=> $SetupNodet,
						"Setup" 						=> $AllSetup,
						"NoProductionRunning" 			=> $NoProductionRunning,
						"NoProductionWaiting" 			=> $NoProductionWaiting,
						"NoProductionStopped" 			=> $NoProductionStopped,
						"NoProductionOff" 				=> $NoProductionOff,
						"NoProductionNodet" 			=> $NoProductionNodet,
						"NoProduction" 					=> $AllNoProduction				
					);
				
				
				$json =  array("status" => "1","data" => $data,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
			}else
			{
				$json =  array("status" => "0","message" => "Data not available for selected filters.","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
			}
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //breakdownReasonList()
    //This api is use for getting the breakdown reason in oppApp
    public function breakdownReasonList_post() 
    { 
		$postArray = $this->post();
		
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//get breakdown reason list
		$list = $this->APIM->getBreakdownReasonList(); 
		
		//check required parameter validation
		if($list == 0 ) {
			$message = ['status'=>'1',"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		} else {
			$message = ['status'=>'1','list'=>$list,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		}
	
		$this->set_response($message, REST_Controller::HTTP_OK);
	} 

	//latestDowntimeNotification()
	//Use for get last breakdown notification in oppApp  
	public function latestDowntimeNotification_post() 
	{ 
		$postArray = $this->post();
		
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not

		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);
		//check required parameter validation
		if(isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($postArray['userId'])) {
			$machineId = $postArray['machineId'];
			$notificationId = !empty($postArray['notificationId']) ? $postArray['notificationId'] : 0;
			
			//get last breakdown details
			$list = $this->APIM->getNotificationsSingle($postArray['factoryId'], $machineId, $userId,$notificationId);
			if(is_array($list) && count($list) > 0 ) {
				for($x=0;$x<count($list);$x++) {
					//set breakdown time
					$addedDate =  $this->APIM->getNotificationAddedDate($postArray['factoryId'], $list[$x]->logId);
					//set breakdown update time
					$changeDate =  $this->APIM->getNotificationChangeDate($postArray['factoryId'], $machineId, $list[$x]->logId);
					if(isset($changeDate) && $changeDate!= false) { 
						$list[$x]->changeDate = date("M jS | H:i A",strtotime($changeDate->originalTime)); 
						$list[$x]->present = 0; 
						$endDate = $changeDate->originalTime;
					} else {
						$list[$x]->present = 1; 
						$list[$x]->changeDate = "Present"; 
						$endDate = date('Y-m-d H:i:s');
					} 			
					if(isset($addedDate) && $addedDate!= false) {  
						$list[$x]->addedDate = date("M jS | H:i A",strtotime($addedDate->originalTime));	
						$startDate = $addedDate->originalTime;
					} else {
						$list[$x]->addedDate = date("M jS | H:i A");	
						$startDate = date('Y-m-d H:i:s'); 
					}	

					$to_time = strtotime($startDate);
					$from_time = strtotime($endDate);
					$list[$x]->durationTime = round(abs($to_time - $from_time) / 60). " min";
					$list[$x]->selectedReason = $list[$x]->comment; 

					$lastCkecingData = $this->APIM->getLastCheckInDetails($postArray['factoryId'],$userId);
					if ($lastCkecingData['isViewIn'] == "0") 
					{
						$list[$x]->isViewIn = "1";
					}else
					{
						if (!empty($lastCkecingData['workingMachine'])) 
						{
							$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
							if (in_array($machineId, $workingMachineArrData)) 
							{
								$list[$x]->isViewIn = "1";
							}else
							{
								$list[$x]->isViewIn = "0";
							}
						}else
						{
							$list[$x]->isViewIn = "0";
						}
					}	
				} 
			}
			
			if($list == 0 ) 
			{
				$message = ['status'=>'0','message'=>NOTIFICATION_LIST_EMPTY,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			} else {
				$message = ['status'=>'1','data'=>$list,'message'=>NOTIFICATION_LIST_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}
		} else {	
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	//downtimeReason use for get breakdown notification in oppApp  
	public function downtimeReason_post() 
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('machineId','machineId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  

	    	$machineId = $this->input->post('machineId');

	    	$breakdownReasonList = $this->APIM->getBreakdownReasonList(); 
	    	$breakdownReasonList = array_column($breakdownReasonList, "breakdownReasonText");

	    	//completion = 0 unanswered 
	    	//completion = 1 answered
	    	$completion = $this->input->post('completion');
			$result = $this->APIM->getWarningNotifications($factoryId, $machineId, $userId,$completion);
			//print_r($result);exit;
			foreach ($result as $key => $value) 
			{	
				$addedDate =  $this->APIM->getNotificationAddedDate($factoryId, $value->logId);
					//set breakdown update time
				$changeDate =  $this->APIM->getNotificationChangeDate($factoryId, $machineId, $value->logId);
				if($changeDate!= false ) { 
					$result[$key]->changeDate = date("M jS | H:i A",strtotime($changeDate->originalTime)); 
					$result[$key]->present = 0; 
					$endDate = $changeDate->originalTime;
				} else {
					$result[$key]->present = 1; 
					$result[$key]->changeDate = "Present"; 
					$endDate = date('Y-m-d H:i:s');
				}
				if(isset($addedDate) && $addedDate!= false) {  
					$result[$key]->addedDate = date("M jS | H:i A",strtotime($addedDate->originalTime));	
					$startDate = $addedDate->originalTime;
				} else {
					$result[$key]->addedDate = date("M jS | H:i A");	 
					$startDate = date('Y-m-d H:i:s');
				}

				$to_time = strtotime($startDate);
				$from_time = strtotime($endDate);
				$result[$key]->durationTime = round(abs($to_time - $from_time) / 60). " min";

				$result[$key]->selectedReason = $value->comment; 	
			}

			$json =  array("status" => "1","message" => Notificationdata,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"breakdownReasonList" => $breakdownReasonList,"data" => $result);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
	}


	//modifyReason()
	// use for breakdown reason in modify reason notification in  oppApp 
	public function modifyReason_post() 
	{ 
		$postArray = $this->post();

		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($postArray['userId'])) {
			$machineId = $postArray['machineId'];
			$userId = $postArray['userId'];
			//get modify reason data
			$list = $this->APIM->getRespondedNotifications($postArray['factoryId'], $machineId, $userId);
			if(is_array($list) && count($list) > 0 ) {
				for($x=0;$x<count($list);$x++) {
					//set breakdown time
					$addedDate =  $this->APIM->getNotificationAddedDate($postArray['factoryId'], $list[$x]->logId);
					//set breakdown update time
					$chnageDate =  $this->APIM->getNotificationChangeDate($postArray['factoryId'], $machineId, $list[$x]->logId);
					if($chnageDate!= false ) { 
						$list[$x]->changeDate = date("d-m-y H:i:s",strtotime($chnageDate->originalTime)); 
						$list[$x]->present = 0; 
					} else {
						$list[$x]->present = 1; 
						$list[$x]->changeDate = "Present"; 
					}
					if(isset($addedDate) && $addedDate!= false) {  
						$list[$x]->addedDate = date("d-m-y H:i:s",strtotime($addedDate->originalTime));	
					} else {
						$list[$x]->addedDate = date("d-m-y H:i:s");	 
					}
					$list[$x]->selectedReason = $list[$x]->comment; 	
					
				} 
			}

			if($list == 0 ) 
			{
				$message = ['status'=>'1','message'=>NOTIFICATION_RESPONDED_LIST_EMPTY,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			} else 
			{
				$message = ['status'=>'1','list'=>$list,'message'=>NOTIFICATION_RESPONDED_LIST_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
			}
		} else 
		{	
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	//notificationRespond use for add breakdown detail in oppApp
	public function notificationRespond_post() 
	{ 
		$postArray = $this->post();
		

		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not

		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['factoryId']) && isset($_POST['notificationId']) && !empty($_POST['notificationId']) && isset($_POST['reason']) && isset($_POST['userId']) ) {  
			$activeDetail = $this->APIM->notificationExists($postArray['factoryId'], $_POST['notificationId']); 
			if(isset($_POST['respondTime'])) {
                $respondTime = $_POST['respondTime'];
            } else {
                $respondTime = time();
            }

            //cross verify notification is already added or not
			if(is_array($activeDetail) && isset($respondTime)) {
				
				$reason = $_POST['reason'];
				$otherReason = !empty($_POST['otherReason']) ? $_POST['otherReason'] : "";
				//add breakdown reason
				$response = $this->APIM->respondNotification($postArray['factoryId'], $_POST['notificationId'], $respondTime, $reason, $postArray['userId'],$otherReason); 
				$statusResponse  = $this->APIM->updateNotificationLiveStatus($postArray['factoryId'], $_POST['notificationId'], $activeDetail['machineId'], '1'); //1 for responded notification  
				
				$message = ['status'=>'1','message'=>NOTIFICATION_RESPOND_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 

			} else {
				$message = ['status'=>'0','message'=>NOTIFICATION_RESPOND_ERROR,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			}
		} else {
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	public function notificationRespondWithWarningFlag_post() 
	{ 
		$postArray = $this->post();
		

		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not

		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['factoryId']) && isset($_POST['notificationId']) && !empty($_POST['notificationId']) && isset($_POST['reason']) && isset($_POST['userId']) && isset($_POST['machineId']) ) {  
			$activeDetail = $this->APIM->notificationExists($postArray['factoryId'], $_POST['notificationId']); 
			if(isset($_POST['respondTime'])) {
                $respondTime = $_POST['respondTime'];
            } else {
                $respondTime = time();
            }

            //cross verify notification is already added or not
			if(is_array($activeDetail) && isset($respondTime)) {
				
				$reason = $_POST['reason'];
				$breakdownReason = $this->APIM->getWhereSingle($postArray['factoryId'], array("breakdownReason" => $reason,"machineId" => $activeDetail['machineId']), "machineBreakdownReason");
				$reasonId = $breakdownReason->errorTypeId;
				$otherReason = !empty($_POST['otherReason']) ? $_POST['otherReason'] : "";
				//add breakdown reason
				$response = $this->APIM->respondNotification($postArray['factoryId'], $_POST['notificationId'], $respondTime, $reason, $postArray['userId'],$otherReason,$reasonId); 
				$statusResponse  = $this->APIM->updateNotificationLiveStatus($postArray['factoryId'], $_POST['notificationId'], $activeDetail['machineId'], '1'); //1 for responded notification  
				
				$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($postArray['factoryId'],$_POST['machineId']);

				$warningFlag = ($warningFlagResponse > 0) ? 1 : 0;
				$message = ['status'=>'1','message'=>NOTIFICATION_RESPOND_SUCCESS,"warningFlag" => $warningFlag,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 

			} else {
				$message = ['status'=>'0','message'=>NOTIFICATION_RESPOND_ERROR,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
			}
		} else {
				$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	//respondMulti() It is use for adding multipal breakdown detail in oppApp
	public function respondMulti_post() 
	{ 
		$postArray = $this->post(); 

		$userId = $postArray['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		$this->APIM->getLanguage($userId);

		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($postArray['factoryId']);

		if(isset($postArray['respondTime'])) {
			$respondTime = $postArray['respondTime'];
		} else {
			$respondTime = time();
		}
		//check required parameter validation
		if(isset($postArray['factoryId']) && isset($postArray['notificationList']) && isset($postArray['userId']) && isset($postArray['machineId']) ) {  
			
			$notificationList = $postArray['notificationList'];
			//add multipal breakdown details at same time
			for($x=0;$x<count($notificationList);$x++) 
			{
				$reason = $notificationList[$x]['selectedReason']; 
				if (!empty($reason)) 
				{
					$breakdownReason = $this->APIM->getWhereSingle($postArray['factoryId'], array("breakdownReason" => $reason,"machineId" => $postArray['machineId']), "machineBreakdownReason");
					$reasonId = $breakdownReason->errorTypeId;
					$otherReason = !empty($notificationList[$x]['otherSelectedReason']) ? $notificationList[$x]['otherSelectedReason'] : ""; 
					$response = $this->APIM->respondNotification($postArray['factoryId'], $notificationList[$x]['notificationId'], $respondTime, $reason, $postArray['userId'],$otherReason,$reasonId); 
					$statusResponse  = $this->APIM->updateNotificationLiveStatus($postArray['factoryId'], $notificationList[$x]['notificationId'], $postArray['machineId'], '1'); 
				}
			} 
			$message = ['status'=>'1','message'=> NOTIFICATION_MULTI_RESPOND_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		
		} else {
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	//downtimeReasonCount use for get count of all breakdown assign machine in oppApp
	public function downtimeReasonCount_post() 
	{ 
		$postArray = $this->post();
		
		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);
		//check factory is exists or not

		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if(isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($postArray['userId'])) {
			$machineId = $postArray['machineId'];
			//get count of assign machine breakdown 
			$notificationCount = $this->APIM->getNotificationsCount($postArray['factoryId'], $machineId, $userId);
			$message = ['status'=>'1','notificationCount'=>$notificationCount,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
		} else {	
			$message = ['status'=>'0','message'=>INVALID_DETAILS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}
		$this->set_response($message, REST_Controller::HTTP_OK);
	}

	//opappCaptureCamera() It is used for adding detection issue in oppApp
	public function opappCaptureCamera_post() 
    {
        $postArray = $this->post();
		$fileArray = $_FILES;  

		if(isset($postArray['logTime']))
		{
			$logTime = $postArray['logTime'];
		} else 
		{
			$logTime = time();
		} 
		if(isset($postArray['captureTime'])) {
			$captureTime = $postArray['captureTime']/1000; 
		} else {
			$captureTime = time(); 
		}

		$userId = $_POST['userId'];
		//check user is exists or not 
		$userDetail = $this->APIM->userExists($userId);

		//check factory is exists or not
		$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

		//check required parameter validation
		if (isset($fileArray['imageName']) && isset($postArray['userId']) && isset($postArray['machineId']) && isset($postArray['factoryId']) && isset($logTime)) {  
			$timestamp = $postArray['logTime']; 
			$machineId = $postArray['machineId'];
			$userId = $postArray['userId']; 
			$factoryId = $postArray['factoryId'];
			$color = $postArray['color'];
			$state = $postArray['state'];
			
			$path_parts = pathinfo($fileArray["imageName"]["name"]);
			$fileExt = $path_parts['extension'];
			
			//upload file in folder by machine id, user id and logtime
			move_uploaded_file($fileArray['imageName']['tmp_name'], "../reportedImages/".$factoryId."/"  .$machineId. "_". $userId . "_" . $logTime. "." .$fileExt);
			$insertLogId = $this->APIM->addReportImage($factoryId, $captureTime, $logTime, $fileExt, $machineId, $userId);  

			//file path 
			$file_path =  "https://nyttcloud.host/prodapp/reportedImages/".$factoryId."/"  .$machineId. "_". $userId . "_" . $logTime. "." .$fileExt;
			
			$machine = $this->APIM->machineExists($factoryId, $machineId);  
			if (!empty($color) && !empty($state)) 
			{
				$msg = "Please check the following detection issue.
					<br>Factory - ". base64_decode($checkFactoryDetail[0]['factoryName']) ."
					<br>Machine - ".$machine['machineName'] ."
					<br>Timestamp  - ". date('d/m/Y H:i:s',$timestamp)."
				    <br>Color - ".$color."
				    <br>State - ".$state;
			}else
			{
				$msg = "Please check the following detection issue.
					<br>Factory - ". base64_decode($checkFactoryDetail[0]['factoryName']) ."
					<br>Machine - ".$machine['machineName'];
			}
			
			$subject = "OpApp detection issue";
			$this->APIM->sendEmail($msg,$subject,$file_path,$fileExt,$factoryId);
			
			
			$message = [ 'status' => "1","message" => Detectionissuemailsendsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];
			
		} else 
		{
			$message = [ 'status' => "0","message" => pleaseproviderequiredparameter,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
		}

		$this->set_response($message, REST_Controller::HTTP_OK); 
		
    }

    //updateNotificationTime_post() api is used for reminders notification  oppApp
    public function updateNotificationTime_post()
	{
	    $this->form_validation->set_rules('receiveReminders', 'receiveReminders', 'required');
	    /*$this->form_validation->set_rules('receiveErrorNotifiction', 'receiveErrorNotifiction', 'required');
	    $this->form_validation->set_rules('provideReasonForErrors', 'provideReasonForErrors', 'required');
	    $this->form_validation->set_rules('receiveWaitingNotification', 'receiveWaitingNotification', 'required');
	    $this->form_validation->set_rules('provideReasonForWaiting', 'provideReasonForWaiting', 'required');
	    $this->form_validation->set_rules('warningMachineId', 'warningMachineId', 'required');*/
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$receiveReminders  = $this->input->post('receiveReminders');
		/*	$receiveErrorNotifiction  = $this->input->post('receiveErrorNotifiction');
			$provideReasonForErrors  = $this->input->post('provideReasonForErrors');
			$receiveWaitingNotification  = $this->input->post('receiveWaitingNotification');
			$provideReasonForWaiting  = $this->input->post('provideReasonForWaiting');
			$warningMachineId  = $this->input->post('warningMachineId');*/

			$data = array(
					"receiveReminders" => $receiveReminders
				);

			$where = array("userId" => $userId);
			//Update notification
			$this->APIM->updateData($where,$data,"user");
				
			$json =  array("status" => "1","message" => Notificationtimesuccessfullyupdated,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function updateNotificationFlag_post()
	{
	    $this->form_validation->set_rules('notificationUserLogId', 'notificationUserLogId', 'required');
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$notificationUserLogId  = $this->input->post('notificationUserLogId');

			$data = array(
					"notificationFlag" => "1"
				);

			$where = array("notificationUserLogId" => $notificationUserLogId);
			//Update notification
			$this->APIM->updateData($where,$data,"notificationUserLog");
				
			$json =  array("status" => "1","message" => "Notification flag updated successfully","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //addTask() api use for add new task in oppApp 
    public function addTask_post()
	{
	    $this->form_validation->set_rules('userIds','userIds','required');
        $this->form_validation->set_rules('task','task','required');
        $this->form_validation->set_rules('repeat','repeat','required');
        $this->form_validation->set_rules('userId','userId','required');
        $this->form_validation->set_rules('factoryId','factoryId','required');
        $this->form_validation->set_rules('machineId','machineId','required');

        $repeat = $this->input->post('repeat');
        if ($repeat == "never") {
        	$this->form_validation->set_rules('dueDate','Due date','required');
        }

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			//echo json_encode($_POST);exit;
			$userIds = $this->input->post('userIds');
			$taskId = $this->input->post('taskId');
			$machineId =  $this->input->post('machineId');
			$task = $this->input->post('task');

			//Set task start end date as par repeat filter
			if ($repeat == "everyDay") 
			{
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}elseif ($repeat == "everyWeek") 
			{
				$startDate = date('Y-m-d');
				$day = date('l');
				if ($day == "Friday") 
				{
					$dueDate = date('Y-m-d');
				}else
				{
					$dueDate = date('Y-m-d',strtotime("next friday"));
				}
				$repeatOrder = 3;
			}elseif ($repeat == "everyMonth") 
			{
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-t');
				$repeatOrder = 4;
			}elseif ($repeat == "everyYear") 
			{
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime("12/31"));	
				$repeatOrder = 5;	
			}else
			{
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDate')));
				$repeatOrder = 1;
			}


			if (!empty($taskId)) 
            {
            	$updatedByUserId = $userId;
            }else
            {
            	$updatedByUserId = 0;
            }

            $data=array(
                'userIds' => $userIds,
                'machineId' => $machineId,  
                'task' => $task,  
                'repeat' => $repeat,
                'dueDate' => $dueDate,
                'startDate' => $startDate,
                'repeatOrder' => $repeatOrder,
                'createdByUserId' => $userId,
                'updatedByUserId' => $updatedByUserId
            );  

            if (empty($taskId)) 
            {
            	$this->APIM->insertFactoryData($factoryId, $data, "taskMaintenace");
            	$json =  array("status" => "1","message" => Taskaddedsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
            }else
            {
            	$where = array("taskId" => $taskId);
            	$this->APIM->updateFactoryData($factoryId, $where,$data, "taskMaintenace");


            	$taskDetail = $this->APIM->getWhereSingle($factoryId, $where, "taskMaintenace");
            	if (!empty($taskDetail->mainTaskId)) 
            	{
            		$updateTaskData=array(  
		                'task' => $task
		            );  

		            $this->APIM->updateFactoryData($factoryId, array("mainTaskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");

		            $this->APIM->updateFactoryData($factoryId, array("taskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");
            	}else
            	{
            		$updateTaskData=array(  
		                'task' => $task
		            );  

		            $this->APIM->updateFactoryData($factoryId, array("mainTaskId" => $taskId),$updateTaskData, "taskMaintenace");
            	}

            	$json =  array("status" => "1","message" => Taskupdatedsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
            }


				
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //Use for detete task in opApp
    public function deleteTask_post()
	{
	    $this->form_validation->set_rules('taskId','taskId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$taskId = $this->input->post('taskId');

            $data=array(
                'isDelete' => "1"
            );  
            $this->APIM->updateFactoryData($factoryId,array("taskId" => $taskId),$data,"taskMaintenace");
				
			$json =  array("status" => "1","message" => Taskdeletedsuccessfully,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    //listTask() api use for get task list as par machine and operator
    public function listTask_post()
	{
	    $this->form_validation->set_rules('machineId','machineId','required');
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$userIds = $this->input->post('userIds');
			$date = $this->input->post('date');
			$status = $this->input->post('status');
  
            $result = $this->APIM->getTaskMaintenace($factoryId,$userId,$userIds,$date,$status,$machineId)->result_array();
            foreach ($result as $key => $value) 
            {
            	$userIds = explode(",", $value['userIds']);

            	if ($value['repeat'] == "everyDay") 
				{
					$result[$key]['color'] = "#26B4C640";
				}
				elseif ($value['repeat'] == "everyWeek") 
				{
					$result[$key]['color'] = "#59B4C640";	
				}
				elseif ($value['repeat'] == "everyMonth") 
				{
					$result[$key]['color'] = "#8CB4C640";	
				}
				elseif ($value['repeat'] == "everyYear") 
				{
					$result[$key]['color'] = "#BFB4C640";		
				}
				else
				{
					$result[$key]['color'] = "#FFFFFF";	
				}

				$result[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
				$i = 0;
				foreach ($userIds as $userKey => $userValue) 
				{
					$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
					if (!empty($users)) 
					{
						$result[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
						$i++;
					}
				}


				

            }
			
			if (!empty($result)) 
			{
				$json =  array("status" => "1","message" => "Task data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
			}else
			{
				$json =  array("status" => "0","message" => "Task data not available for this filter","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
			}	
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

  	  //getOperators api use for get all operators 
    public function getOperators_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('machineId','machineId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$select = "userId,userName,concat('" . base_url('assets/img/user/') . "',userImage) as userImage";
			$result = $this->APIM->getWhereDBSelect($select,array("userRole" => "0","isDeleted" => "0","factoryId" => $factoryId),"user");
            
            $data = array(); 
            $i = 1;	
            foreach ($result as $key => $value) 
            {
            	if ($userId == $value['userId']) 
            	{
            		$data[0]['userId'] = $value['userId'];
            		$data[0]['userName'] = $value['userName'];
            		$data[0]['userImage'] = $value['userImage'];
            	}else
            	{
            		$data[$i]['userId'] = $value['userId'];
            		$data[$i]['userName'] = $value['userName'];
            		$data[$i]['userImage'] = $value['userImage'];
            		$i++;
            	}
            }
            ksort($data);
			$json =  array("status" => "1","message" => "User data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $data);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

     //getOperators api use for get all operators 
    public function getOperatorsByAssignMachine_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('machineId','machineId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$select = "userId,userName,concat('" . base_url('assets/img/user/') . "',userImage) as userImage";
			$result = $this->APIM->getWhereDBSelect($select,array("userRole" => "0","isDeleted" => "0","factoryId" => $factoryId,"find_in_set($machineId,machines) <> " => 0),"user");
            
            $data = array(); 
            $userIds = array_column($result, "userId");
            if (in_array($userId, $userIds)) 
            {
            	$i = 1;	
            }else
            {
            	$i = 0;	
            }
            foreach ($result as $key => $value) 
            {
            	if ($userId == $value['userId']) 
            	{
            		$data[0]['userId'] = $value['userId'];
            		$data[0]['userName'] = $value['userName'];
            		$data[0]['userImage'] = $value['userImage'];
            	}else
            	{
            		$data[$i]['userId'] = $value['userId'];
            		$data[$i]['userName'] = $value['userName'];
            		$data[$i]['userImage'] = $value['userImage'];
            		$i++;
            	}
            }
            ksort($data);
			$json =  array("status" => "1","message" => "User data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $data);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }


    //Get task maintenace list by day,week,month and year 
    public function listTaskMaintenace_post()
	{
	    $this->form_validation->set_rules('machineId','machineId','required');
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$userIds = $this->input->post('userIds');
			$date = date('Y-m-d',strtotime($this->input->post('date')));
			$status = $this->input->post('status');
			$day = date('l',strtotime($date));

			if ($day == "Saturday") 
			{
				$date = date('Y-m-d',strtotime($date ."+2 day"));
			}
			else if ($day == "Sunday") 
			{
				$date = date('Y-m-d',strtotime($date ."+1 day"));
			}

			$compareDateCheck = $date;
	        $compareWeekCheck = date('W',strtotime($date));
	        $compareMonthCheck = date('m',strtotime($date));
	        $compareYearCheck = date('Y',strtotime($date));

	        $currentDate = date('Y-m-d');
	        $dayCheck = date('l',strtotime($currentDate));

	    	if ($dayCheck == "Saturday") 
			{
				$currentDate = date('Y-m-d',strtotime("+2 day"));
			}
			else if ($dayCheck == "Sunday") 
			{
				$currentDate = date('Y-m-d',strtotime("+1 day"));
			}

	        $currentWeek = date('W',strtotime($currentDate));
	        $currentMonth = date('m',strtotime($currentDate));
	        $currentYear = date('Y',strtotime($currentDate));
	      

            //Get never task as par assign machine and operator
	        $resultNever = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'never')->result_array();
            //Get every day task as par assign machine and operator
            $resultEveryDay = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyDay')->result_array();
            $resultNever = array_merge($resultNever,$resultEveryDay);

            foreach ($resultNever as $key => $value) 
            {
            	$userIdsNever = explode(",", $value['userIds']);

            	if ($value['repeat'] == "everyDay") 
				{
					$resultNever[$key]['color'] = "#26B4C640";
				}
				elseif ($value['repeat'] == "everyWeek") 
				{
					$resultNever[$key]['color'] = "#59B4C640";	
				}
				elseif ($value['repeat'] == "everyMonth") 
				{
					$resultNever[$key]['color'] = "#8CB4C640";	
				}
				elseif ($value['repeat'] == "everyYear") 
				{
					$resultNever[$key]['color'] = "#BFB4C640";		
				}
				else
				{
					$resultNever[$key]['color'] = "#FFFFFF";	
				}

				if ($currentDate >=  $compareDateCheck) 
	        	{
	        		if ($currentDate ==  $compareDateCheck) 
	        		{
	        			$resultNever[$key]['isEdit'] = 1;
	        			$resultNever[$key]['message'] = "";
	        		}else
	        		{
	        			$resultNever[$key]['isEdit'] = 0;
	        			$resultNever[$key]['message'] = Youcannotupdatepasttask;
	        		}
	        	}else
	        	{	
	        		$resultNever[$key]['isEdit'] = 0;
	        		$resultNever[$key]['message'] = Youcannotupdatefuturetask;
	        		$resultNever[$key]['status'] = "uncompleted";
	        		if ($resultNever[$key]['repeat'] != "never") {
	        			$resultNever[$key]['dueDate'] = date('Y-m-d',strtotime($date));
	        		}
	        	}


				$resultNever[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
				$i = 0;
				foreach ($userIdsNever as $userKey => $userValue) 
				{
					$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
					if (!empty($users)) 
					{
						$resultNever[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
						$i++;
					}
				}

            }

            //Get week task as par assign machine and operator
            $resultWeek = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyWeek')->result_array();

            foreach ($resultWeek as $key => $value) 
            {
            	$userIdsWeek = explode(",", $value['userIds']);

            	if ($value['repeat'] == "everyDay") 
				{
					$resultWeek[$key]['color'] = "#26B4C640";
				}
				elseif ($value['repeat'] == "everyWeek") 
				{
					$resultWeek[$key]['color'] = "#59B4C640";	
				}
				elseif ($value['repeat'] == "everyMonth") 
				{
					$resultWeek[$key]['color'] = "#8CB4C640";	
				}
				elseif ($value['repeat'] == "everyYear") 
				{
					$resultWeek[$key]['color'] = "#BFB4C640";		
				}
				else
				{
					$resultWeek[$key]['color'] = "#FFFFFF";	
				}

				if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
	        	{	
	        		if ($currentWeek ==  $compareWeekCheck) 
	        		{
	        			$resultWeek[$key]['isEdit'] = 1;
		        		$resultWeek[$key]['message'] = "";
	        		}else
	        		{
	        			$resultWeek[$key]['isEdit'] = 0;
	        			$resultWeek[$key]['message'] = Youcannotupdatepasttask;
	        		}
	        	}else
	        	{	
	        		$resultWeek[$key]['isEdit'] = 0;
	        		$resultWeek[$key]['message'] = Youcannotupdatefuturetask;
	        		$resultWeek[$key]['status'] = "uncompleted";

	        		if ($day == "Friday") 
					{
						$endDate = date('Y-m-d',strtotime($date));
					}else
					{
						$endDate = date('Y-m-d',strtotime($date. "next friday"));
					}

		        	$resultWeek[$key]['dueDate'] = $endDate;
	        	}

				
	        	$resultWeek[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
				$i = 0;
				foreach ($userIdsWeek as $userKey => $userValue) 
				{
					$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
					if (!empty($users)) 
					{
						$resultWeek[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
						$i++;
					}
				}

            }

             //Get month task as par assign machine and operator
            $resultMonth = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyMonth')->result_array();

            foreach ($resultMonth as $key => $value) 
            {
            	$userIdsMonth = explode(",", $value['userIds']);

            	if ($value['repeat'] == "everyDay") 
				{
					$resultMonth[$key]['color'] = "#26B4C640";
				}
				elseif ($value['repeat'] == "everyWeek") 
				{
					$resultMonth[$key]['color'] = "#59B4C640";	
				}
				elseif ($value['repeat'] == "everyMonth") 
				{
					$resultMonth[$key]['color'] = "#8CB4C640";	
				}
				elseif ($value['repeat'] == "everyYear") 
				{
					$resultMonth[$key]['color'] = "#BFB4C640";		
				}
				else
				{
					$resultMonth[$key]['color'] = "#FFFFFF";	
				}

				if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
	        	{	
	        		if ($currentMonth ==  $compareMonthCheck) 
	        		{
		        		$resultMonth[$key]['isEdit'] = 1;
		        		$resultMonth[$key]['message'] = "";
	        		}else
	        		{
	        			$resultMonth[$key]['isEdit'] = 0;
	        			$resultMonth[$key]['message'] = Youcannotupdatepasttask;
	        		}
	        	}else
	        	{	
	        		$resultMonth[$key]['isEdit'] = 0;
	        		$resultMonth[$key]['message'] = Youcannotupdatefuturetask;
	        		$resultMonth[$key]['status'] = "uncompleted";

	        		$resultMonth[$key]['dueDate'] = date('Y-m-t',strtotime($date));
	        	}

	        	$resultMonth[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
				$i = 0;
				foreach ($userIdsMonth as $userKey => $userValue) 
				{
					$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
					if (!empty($users)) 
					{
						$resultMonth[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
						$i++;
					}
				}

            }

             //Get year task as par assign machine and operator
            $resultYear = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyYear')->result_array();

            foreach ($resultYear as $key => $value) 
            {
            	$userIdsYear = explode(",", $value['userIds']);

            	if ($value['repeat'] == "everyDay") 
				{
					$resultYear[$key]['color'] = "#26B4C640";
				}
				elseif ($value['repeat'] == "everyWeek") 
				{
					$resultYear[$key]['color'] = "#59B4C640";	
				}
				elseif ($value['repeat'] == "everyMonth") 
				{
					$resultYear[$key]['color'] = "#8CB4C640";	
				}
				elseif ($value['repeat'] == "everyYear") 
				{
					$resultYear[$key]['color'] = "#BFB4C640";		
				}
				else
				{
					$resultYear[$key]['color'] = "#FFFFFF";	
				}

				$resultYear[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;

				if ($currentYear >=  $compareYearCheck) 
	        	{	
	        		if ($currentYear ==  $compareYearCheck) 
	        		{
		        		$resultYear[$key]['isEdit'] = 1;
		        		$resultYear[$key]['message'] = "";
	        		}else
	        		{
	        			$resultYear[$key]['isEdit'] = 0;
	        			$resultYear[$key]['message'] = Youcannotupdatepasttask;
	        		}
	        	}else
	        	{	
	        		$resultYear[$key]['isEdit'] = 0;
	        		$resultYear[$key]['message'] = Youcannotupdatefuturetask;
	        		$resultYear[$key]['status'] = "uncompleted";
	        		$resultYear[$key]['dueDate'] = date('Y-12-31',strtotime($date));
	        	}
	        	
				$i = 0;
				foreach ($userIdsYear as $userKey => $userValue) 
				{
					$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
					if (!empty($users)) 
					{
						$resultYear[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
						$i++;
					}
				}

				

            }


			$result = array(
						"dayTitle" => DAY." - ".date('jS',strtotime($date)).' of '.date('F',strtotime($date)),
						"weekTitle" => WEEK." - ". date('W',strtotime($date)),
						"monthTitle" => MONTH." - ". date('F',strtotime($date)),
						"yearTitle" => YEAR." - ". date('Y',strtotime($date)),
						"day" => $resultNever,
						"week" => $resultWeek,
						"month" => $resultMonth,
						"year" => $resultYear);
			//Check all  response data to send response in api
			if (!empty($resultNever) || !empty($resultWeek) || !empty($resultMonth) || !empty($resultYear)) 
			{
				$json =  array("status" => "1","message" => "Task data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
			}else
			{
				$result = array(
						"dayTitle" => DAY." - ".date('jS',strtotime($date)).' of '.date('F',strtotime($date)),
						"weekTitle" => WEEK." - ". date('W',strtotime($date)),
						"monthTitle" => MONTH." - ". date('F',strtotime($date)),
						"yearTitle" => YEAR." - ". date('Y',strtotime($date)),
						"day" => array(),
						"week" => array(),
						"month" => array(),
						"year" => array() ); 
				$json =  array("status" => "1","message" => "Task data not available for this filter","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
			}	
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    //get user assign and all machine 
	 public function getUserAssignMachine_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('assign','assign','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$assign = $this->input->post('assign');

	        $select = "machineId,machineName";
	        $where = array("isDeleted" => "0");
	        //check user need assign machine or all
			if ($assign == 0) 
			{
				$users = $this->APIM->getWhereDBSingle(array("userId" => $userId),"user");
	            
	            $machines = $users->machines;

	            $whereIn = array("machineId" => explode(",", $machines));
	            //get user assign machine
				$result = $this->APIM->getWhereIn($factoryId,$select,$where,$whereIn,"machine");
			}else
			{
				//get user all machine
				$result = $this->APIM->getWhereSelect($factoryId, $select, $where,"machine");
			}

			$workingMachineData = $this->APIM->checkWorkingMachineStatus($factoryId,$userId);
			//print_r($workingMachineData);exit;
			if (!empty($workingMachineData)) 
			{
				$workingMachineDataArr = explode(",", $workingMachineData['workingMachine']);
			}else
			{
				$workingMachineDataArr = array(0);
			}
			foreach ($result as $key => $value) 
			{ 
				$result[$key]['workingMachineStatus'] = in_array($value['machineId'], $workingMachineDataArr) ? 1 : 0;
			}
			$json =  array("status" => "1","message" => "Machine data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }


    public function workingMachineList_post() 
	{ 
        $postArray = $this->post();

        

		//check required parameter validation
		if(isset($postArray['userId']) && isset($postArray['factoryId'])) 
		{ 

       		$userId = $_POST['userId'];
			//check user is exists or not 
			$userDetail = $this->APIM->userExists($userId); 
			//$accessPointCheckInExecute = $this->APIM->accessPointEdit('22',$userDetail['userRole']);
			//$accessPointViewInExecute = $this->APIM->accessPoint('22',$userDetail['userRole']);

			//check factory is exists or not
			$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

			$resultCheckIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId);

			if (!empty($resultCheckIn)) 
			{
				$checkInStartTime = $resultCheckIn['startTime'];
			}else
			{
				$checkInStartTime = "";
			}

			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
			if(is_array($factoryDetail)) {
				if(isset($postArray['oppApp']) && $postArray['oppApp'] == '1' ) { //oppapp response 
					$checkInStatus = '0';
					if(isset($_POST['currentTime'])) {
						$currentTime = date('Y-m-d', $_POST['currentTime']);
					} else {
						$currentTime = date('Y-m-d'); 
					} 
					
					
					$machines = array();
					if(intVal($userDetail['userRole']) == 1) { 
					//If role greater than operator
						$isOperator = 0;
					} else { //
						$isOperator = 1;
					}

					$viewOnly = !empty($this->input->post('viewOnly')) ? $this->input->post('viewOnly') : "0";
					//get operator assigned machine 
					$machines = $this->APIM->getWorkingMachine($postArray['factoryId'], $isOperator, $postArray['userId'],$viewOnly)->result();
					
					
					if( count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {



							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['isDataGet'] = $machines[$i]->isDataGet;
							$list[$i]['isSetAppConnect'] = 1;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;
							$list[$i]['workcenterId'] = $machines[$i]->workcenterId;
							/*$list[$i]['noProductionTime'] = !empty($machines[$i]->noProduction) ? $machines[$i]->noProduction : "";
							$list[$i]['noProductionTimeIsActive'] = !empty($machines[$i]->noProductionTimeIsActive) ? $machines[$i]->noProductionTimeIsActive : "0";*/
							$list[$i]['noStacklight'] = $machines[$i]->noStacklight;
							//$list[$i]['setAppAlert'] = "";

							$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($postArray['factoryId'],$machines[$i]->machineId);

							$list[$i]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;


							$lastCkecingData = $this->APIM->getLastCheckInDetails($postArray['factoryId'],$postArray['userId']);
							if ($lastCkecingData['isViewIn'] == "0") 
							{
								$list[$i]['isViewIn'] = "1";
							}else
							{
								if (!empty($lastCkecingData['workingMachine'])) 
								{
									$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
									if (in_array($machines[$i]->machineId, $workingMachineArrData)) 
									{
										$list[$i]['isViewIn'] = "1";
									}else
									{
										$list[$i]['isViewIn'] = "0";
									}
								}else
								{
									$list[$i]['isViewIn'] = "0";
								}
							}

							$userSoundConfig = $this->APIM->getWhereSingle($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"userId" => $userId), "userSoundConfig");
							
							if (!empty($userSoundConfig)) 
							{
								$list[$i]['sound'] = $userSoundConfig->sound;
							}else
							{
								$list[$i]['sound'] = "1";
							}

							$machineNoProductionDateTime = $this->APIM->getWhereDBSingle(array("machineId" => $machines[$i]->machineId,"factoryId" => $postArray['factoryId']), "machineNoProductionDateTime");

							$list[$i]['noProductionTime'] = !empty($machineNoProductionDateTime) ? $machineNoProductionDateTime->noProduction : "";

							if (!empty($machineNoProductionDateTime) && $machineNoProductionDateTime->isActive == "1") {
								$noProductionTimeIsActive = "1";
							}else
							{
								$noProductionTimeIsActive = "0";
							}
							$list[$i]['noProductionTimeIsActive'] = $noProductionTimeIsActive;

							if (!empty($machines[$i]->workcenterId)) 
							{
								$machineParts = $this->getMonitorPartsList($userId,$postArray['factoryId'],$machines[$i]->machineId,$machines[$i]->workcenterId);
							}else{
								$machineParts = $this->APIM->getWhere($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"isDelete" => "0"), "machineParts");
							}
							$list[$i]['machineParts'] = $machineParts;
							

							$currentOrderMachineParts = $this->APIM->getWhereWithOrderBy($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"currentOrderStatus" => "1","isDelete" => "0"), "machineParts","updateTime","asc");

							foreach ($currentOrderMachineParts as $currentKey => $currentValue) 
							{
								$productionTime = $this->APIM->getLastFinishOrderTime($postArray['factoryId'],$currentValue['machineId']);

								if (!empty($productionTime)) 
								{
									$insertTime = $productionTime->insertTime;
								

									$machineProductionTimeWithFinishOrder = $this->APIM->getLastProductionTimeWithFinishOrder($postArray['factoryId'],$currentValue['machinePartsId'],$insertTime);
									if (!empty($machineProductionTimeWithFinishOrder)) 
									{
										$insertTime = $machineProductionTimeWithFinishOrder->insertTime;
									}
									$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($postArray['factoryId'],$machines[$i]->machineId,$insertTime);

									if(!empty($machineRunnignWaitingTime->timeDiff))
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = floor($machineRunnignWaitingTime->timeDiff / $currentValue['patrsCycleTime']);
										
									}else
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
									}
								}else
								{
									$productionTime = $this->APIM->getLastProductionTime($postArray['factoryId'],$currentValue['machinePartsId']);
									//print_r($productionTime);exit;
									if (!empty($productionTime)) 
									{	
										$insertTime = $productionTime->insertTime;

										$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($postArray['factoryId'],$machines[$i]->machineId,$insertTime);

										if(!empty($machineRunnignWaitingTime->timeDiff))
										{
											$currentOrderMachineParts[$currentKey]['totalParts'] = floor($machineRunnignWaitingTime->timeDiff / $currentValue['patrsCycleTime']);
										}else
										{
											$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
										}
									}else
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
									}
								}
								
							}
							$list[$i]['currentOrderMachineParts'] = $currentOrderMachineParts;
							
							//cross verify check battery details added or not
							if ($machines[$i]->chargeFlag != NULL) 
							{
								//setting the battery percentage and  charger is connected or not
								$list[$i]['phoneId'] = $machines[$i]->phoneId;
								$list[$i]['batteryLevel'] = $machines[$i]->batteryLevel;
								$list[$i]['chargeFlag'] = $machines[$i]->chargeFlag;
							} else 
							{ 
								//set default battery data 
								$list[$i]['phoneId'] = 0;
								$list[$i]['batteryLevel'] = 0;
								$list[$i]['chargeFlag'] = "0";
							}
							
							//get setApp machine current status
							$isSetAppOn = $this->APIM->checkSetApp($postArray['factoryId'], intval($machines[$i]->machineId)); 
							if(isset($isSetAppOn) && is_array($isSetAppOn)){
								$list[$i]['isSetAppOn'] = $isSetAppOn['isActive'];
								if($isSetAppOn['reason'] == 'client namespace disconnect' || $isSetAppOn['reason'] == 'transport close' || $machines[$i]->machineSelected == "0") {
									$list[$i]['setAppAlert'] = SETAPP_TURNED_OFF.'.';
								}
								else if($isSetAppOn['reason'] == 'detection disconnect') { 
									$list[$i]['setAppAlert'] = SetAppisonhomescreen.". ".Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'ping timeout') { 
									$list[$i]['setAppAlert'] = SETAPP_NO_INTERNET;
								} 
								else if($isSetAppOn['reason'] == 'app close force fully') 
								{
									$list[$i]['setAppAlert'] = SetAppclosedforcefully.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'app stop due to error') 
								{
									$list[$i]['setAppAlert'] = SetAppcrashedandstoppedduetoexception.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'setApp restart') 
								{
									$list[$i]['setAppAlert'] = SetApprestartedduetosomeexception.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
							} else {
								$list[$i]['isSetAppOn'] = 0;
								$list[$i]['isSetAppConnect'] = 0;
								$list[$i]['setAppAlert'] = Nosetappconnectedto.' '.$machines[$i]->machineName; 
							}
							
							//get operator check in data
							$checkIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId); 
							  
							$checkInStatus = '1'; 
							$activeId = isset($checkIn['activeId'])?$checkIn['activeId']:0;
							
							$machineLightArr[$i] = explode(",", $machines[$i]->machineLight); 
							$listColors = $this->APIM->getAllColors();
							for($z=0;$z<count($machineLightArr[$i]);$z++) {
								for($y=0;$y<count($listColors);$y++) { 
									if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $list[$i]['machineLightColors'][$z] = $listColors[$y]->colorCode;
								}
												
								$machineLightStatus['redStatus'] = '0';
								$machineLightStatus['yellowStatus'] = '0';
								$machineLightStatus['greenStatus'] = '0';
								$machineLightStatus['whiteStatus'] = '0';
								$machineLightStatus['blueStatus'] = '0';
								
								$color = $machines[$i]->color; 
								
								$colorArr = explode(" ", $color);
								for($c=0;$c<count($colorArr);$c++) {
									if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
										$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
									}
								}
								
								if(strpos($color, 'Blue') !== false) {
									$machineLightStatus['blueStatus'] = '1';
								}
								if(strpos($color, 'Red') !== false) {
									$machineLightStatus['redStatus'] = '1';
								}
								if(strpos($color, 'Red and green') !== false) {
									$machineLightStatus['redStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
								}
								if(strpos($color, 'Green') !== false) {
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Yellow and green') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Green and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Red and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['redStatus'] = '1';
								}
								if(strpos($color, 'Robot') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
								}
								if(strpos($color, 'Door') !== false) {
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Red Yellow Green') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['redStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Blue and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['blueStatus'] = '1';
								}
									
								if($list[$i]['machineLightColors'][$z] == 'FF0000') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'redStatus';
								}
								if($list[$i]['machineLightColors'][$z] == 'FFFF00') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
								}
								if($list[$i]['machineLightColors'][$z] == '00FF00') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'greenStatus';
								}
								if($list[$i]['machineLightColors'][$z] == 'FFFFFF') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
								} 
								if($list[$i]['machineLightColors'][$z] == '0000FF') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'blueStatus';
								} 
								
								$list[$i]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
								$list[$i]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
								$list[$i]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
								$list[$i]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
								$list[$i]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
								
								$list[$i]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($postArray['factoryId'], $machines[$i]->machineId, $list[$i]['machineLightStatus']['redStatus'], $list[$i]['machineLightStatus']['greenStatus'], $list[$i]['machineLightStatus']['yellowStatus'], $list[$i]['machineLightStatus']['whiteStatus'],$list[$i]['machineLightStatus']['blueStatus']); 
								
								if($machines[$i]->logId != 0 && $machines[$i]->machineSelected == "1") {
									if($color == 'NoData' || $color == 'NoDataStacklight' || $color == 'NoDataError' || $color == 'NoDataHome' || $color == 'NoDataForceFully' || $color == 'NoDataRestart') { 
										$list[$i]['isSetAppOn'] = 0;
										if(!isset($list[$i]['setAppAlert'])) {
											$list[$i]['setAppAlert'] = NO_DETECTION; 
										}
									} else 
									{
										if ($list[$i]['setAppAlert'] == SETAPP_NO_INTERNET)
										{
											$list[$i]['isSetAppOn'] = 0;
											$list[$i]['setAppAlert'] = SETAPP_NO_INTERNET;
										} else
										{

											$list[$i]['isSetAppOn'] = 1;
											$list[$i]['isSetAppConnect'] = 1;
											$list[$i]['setAppAlert'] = '';
										}
									}  
								} 
							}
							$list[$i]['mtConnectTotalParts'] =  0 ;
							if ($machines[$i]->isDataGet == "2") 
							{
								$mtConnectOverviewResult  = $this->APIM->getWhereSingle($postArray['factoryId'],array("machineId" => $machines[$i]->machineId),"mtConnectOverview");

								$list[$i]['mtConnectTotalParts'] =  (!empty($mtConnectOverviewResult) ? $mtConnectOverviewResult->Partcount : 0) ;
								if ($mtConnectOverviewResult->State == "ACTIVE") 
								{
									$list[$i]['machineLightStatus']['greenStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","0","1");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Running';
									$list[$i]['phoneId'] = '1';
								}else if ($mtConnectOverviewResult->State == "INTERRUPTED"  || $mtConnectOverviewResult->State == "READY") 
								{
									$list[$i]['machineLightStatus']['yellowStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","1","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Waiting';
									$list[$i]['phoneId'] = '1';
								}
								else if ($mtConnectOverviewResult->State == "STOPPED") 
								{
									$list[$i]['machineLightStatus']['redStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("1","0","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Stopped';
									$list[$i]['phoneId'] = '1';
								}else 
								{
									$list[$i]['isSetAppOn'] = (!empty($mtConnectOverviewResult->State)) ? 1 : 0;
									$list[$i]['machineLightColorsStatus'] = array("0","0","0");
									$list[$i]['machineSelected'] = "1";
									$list[$i]['setAppAlert'] = (!empty($mtConnectOverviewResult->State)) ? "nodet" : NoMTconnectconnectedto.' '.$machines[$i]->machineName;
									$list[$i]['isSetAppConnect'] = (!empty($mtConnectOverviewResult->State)) ? 1 : 0;
									$list[$i]['machineLightStatus']['statusText'] = 'nodet';
									$list[$i]['phoneId'] = '1';
								}


							}

							$list[$i]['virtualMachineTotalParts'] = 0 ;
							if ($machines[$i]->isDataGet == "4") 
							{
								$virtualMachineLogOverviewResult  = $this->APIM->getWhereSingle($postArray['factoryId'],array("machineId" => $machines[$i]->machineId),"virtualMachineLogOverview");

								$list[$i]['virtualMachineTotalParts'] = 0 ;
								if ($virtualMachineLogOverviewResult->signalType == "1") 
								{
									$list[$i]['machineLightStatus']['greenStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","1");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = IOconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Running';
									$list[$i]['phoneId'] = '1';
								}
								else if ($virtualMachineLogOverviewResult->signalType == "0") 
								{
									$list[$i]['machineLightStatus']['redStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("1","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = IOconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Stopped';
									$list[$i]['phoneId'] = '1';
								}else 
								{
									$list[$i]['isSetAppOn'] = ($virtualMachineLogOverviewResult->signalType != "") ? 1 : 0;
									$list[$i]['machineLightColorsStatus'] = array("0","0");
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = ($virtualMachineLogOverviewResult->signalType != "") ? 1 : 0;
									$list[$i]['setAppAlert'] = ($virtualMachineLogOverviewResult->signalType != "") ? "nodet" : NoIOconnectedto.' '.$machines[$i]->machineName;
									$list[$i]['machineLightStatus']['statusText'] = 'nodet';
									$list[$i]['phoneId'] = '1';
								}


							}
							$message = ""; 
							if($userDetail['userRole'] == 1 ) 
							{
								$message = Factorymanagercannotmakeanyinput.'.'; 
							} 
							else if(is_array($checkIn))
							{
								$message = Unabletoupdateasyouarenotworkingonthismachine.'.'; 	
							}

							else {
								$activeId = "0";
								$checkInStatus = "0";
								$message = ViewonlyselectedPleasecheckintomakeanyinputs.'.';
							} 
						} 

						$keysMachine = array_column($list, 'isViewIn');
						array_multisort($keysMachine, SORT_DESC, $list);
						$message = ['status'=>'1',"accessPointCheckInExecute" => true,"accessPointViewInExecute" => true,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'activeId'=>$activeId,'message'=> $message,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'list'=>$list]; 
					} else {
						$message = ['status'=>'0',"accessPointCheckInExecute" => true,"accessPointViewInExecute" => true,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'message'=> "No machines assigned, please assign machines to the operator in the dashboard","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} 
					
				} else { 
				//setapp response 
				//get all machine for setApp 
					$machines = $this->APIM->getAssignedMachines($postArray['factoryId'], $postArray['userId']); 
					if(is_array($machines) && count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {
							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;

							$userSoundConfig = $this->APIM->getWhereSingle($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"userId" => $userId), "userSoundConfig");

							if (!empty($userSoundConfig)) 
							{
								$list[$i]['sound'] = $userSoundConfig->sound;
							}else
							{
								$list[$i]['sound'] = "1";
							}

						}
						$message = ['status'=>'1',"checkInStartTime" => $checkInStartTime,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'list'=>$list,'message'=>MACHINE_LIST_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} else {
						$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>NO_ASSIGN,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
					}
				}
			} else {
				$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>Invaliddetails];
			}
		} else {
			$message = ['status'=>'0','message'=>Invaliddetails];
		}
        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }
    public function check_user_name($name) 
    {
    	//Checking the userName field for validation that they cant add any special character in the userName.It can be allowed only Numeric value and alphabets 
        if(!preg_match("/^[a-zA-Z0-9]+$/",$name) ) 
        {
            $this->form_validation->set_message('check_user_name','Username may contain only alphabets, numbers.');
            return false;  
        } else 
        { 
            if($this->input->post('userId')) 
            {
                $userId = $this->input->post('userId');
            } else 
            {
                $userId = 0;
            } 
            //checking the userName is already exist or not when it is exist send the false message else send the true message
            if($this->APIM->userExistsName($name, $userId)) 
            {  
                $this->form_validation->set_message('check_user_name','Username must be unique.');
                return false;  
            } else 
            {
                return true;
            }
        }   
    }

    //update setting data like user profile and working on machine    
	public function updateSettingData_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('userName','userName','required|callback_check_user_name');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$userName = $this->input->post('userName');
			
				$workingMachine = $this->input->post('workingMachine');

				$workingMachineData = $this->APIM->checkWorkingMachineStatus($factoryId,$userId);
				if (!empty($workingMachine)) 
				{
					$activeId = $workingMachineData['activeId'];
					//Update working on machines
					$this->APIM->updateFactoryData($factoryId,array("activeId" => $activeId),array("workingMachine" => $workingMachine),"machineUserv2");
				}
				else
				{
					$activeId = $workingMachineData['activeId'];
					$this->APIM->updateFactoryData($factoryId,array("activeId" => $activeId),array("workingMachine" => "","isViewIn" => "1"),"machineUserv2");
				}

		        
				$imageFlag = TRUE;
				$userImage = $userDetail['userImage'];
		        if(!empty($_FILES['userImage'])) 
		        {
					$file_ext = $path_parts['extension'];
					
					$config = array(
						'upload_path' =>  './assets/img/user/', 
						'allowed_types' => "jpg|png|jpeg",
						'overwrite' => TRUE,
						'max_size' => "2048000", 
						'max_height' => "2048",
						'max_width' => "2048",
						'file_name'=>$userId. ".png" 
						); 
					$this->load->library('upload', $config);
					//check profile image size and type
					if(!$this->upload->do_upload('userImage'))
					{
						$imageFlag = FALSE;
						$json =  array("status" => "0","message" => strip_tags($this->upload->display_errors()),"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);  
					}else
					{
						$userImage = $this->upload->data('file_name');
						$imageFlag = TRUE;
					}
				}

				if ($imageFlag == TRUE) 
				{
					$data = array(
						"userName" => $userName,
						"userImage" => $userImage,
					);

					//update user profile data
					$this->APIM->updateData(array("userId" => $userId),$data,"user");
					$json =  array("status" => "1","message" => Settingsupdatedsuccessfully.".","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
				}
			}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

     //statistics()
    //This Ap is used for getting the Deep Overview Graph data in oppApp
    public function workingMachineStatistics_post()
	{
	    $this->form_validation->set_rules('choose1', 'choose1', 'required');
	    $this->form_validation->set_rules('choose2', 'choose2', 'required');
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

	    //check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
		
		//check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
		}
		else
		{  
			$choose1  = $this->input->post('choose1');
			$choose2  = $this->input->post('choose2');
			

			if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} else { //
				$isOperator = 1;
			}

			//get operator assigned machine 
			$machines = $this->APIM->getWorkingMachine($factoryId, $isOperator, $userId,"0")->result_array();

			foreach ($machines as $key => $value) 
			{
				$machineId = $value['machineId'];

				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$key]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$key]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$key]['phoneId'] = $value['phoneId'];
					$machines[$key]['batteryLevel'] = $value['batteryLevel'];
					$machines[$key]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$key]['phoneId'] = "0";
					$machines[$key]['batteryLevel'] = "0";
					$machines[$key]['chargeFlag'] = "0";
				}

				$machines[$key]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$key]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";


				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$key]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$key]['machineLightColors'][$z] == 'FF0000') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == '00FF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$key]['machineLightColors'][$z] == '0000FF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$key]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$key]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$key]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$key]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$key]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$key]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$key]['machineLightStatus']['redStatus'], $machines[$key]['machineLightStatus']['greenStatus'], $machines[$key]['machineLightStatus']['yellowStatus'], $machines[$key]['machineLightStatus']['whiteStatus'],$machines[$key]['machineLightStatus']['blueStatus']); 
				}
				



				//ActualProduction
				//Get Actual production data by machine state
				$resultActualProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','running');
				$ActualProductionRunning = $resultActualProductionRunning->countVal;
				$resultActualProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','waiting');
				$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
				$resultActualProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','stopped');
				$ActualProductionStopped = $resultActualProductionStopped->countVal;
				$resultActualProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','off');
				$ActualProductionOff = $resultActualProductionOff->countVal;
				$resultActualProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','nodet');
				$ActualProductionNodet = $resultActualProductionNodet->countVal;

				$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet;

				//Setup

				//Get setup data by machine state
				$resultSetupRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','running');
				$SetupRunning = $resultSetupRunning->countVal;
				$resultSetupWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','waiting');
				$SetupWaiting = $resultSetupWaiting->countVal;
				$resultSetupStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','stopped');
				$SetupStopped = $resultSetupStopped->countVal;
				$resultSetupOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','off');
				$SetupOff = $resultSetupOff->countVal;
				$resultSetupNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','nodet');
				$SetupNodet = $resultSetupNodet->countVal;

				$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet;

				//No production
				//Getting no production data by machine state
				$resultNoProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','running');
				$NoProductionRunning = $resultNoProductionRunning->countVal;
				$resultNoProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','waiting');
				$NoProductionWaiting = $resultNoProductionWaiting->countVal;
				$resultNoProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','stopped');
				$NoProductionStopped = $resultNoProductionStopped->countVal;
				$resultNoProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','off');
				$NoProductionOff = $resultNoProductionOff->countVal;
				$resultNoProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','nodet');
				$NoProductionNodet = $resultNoProductionNodet->countVal;

				$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet;

				
			
				//$total = $ActualProduction;
				$total = $ActualProduction + $Setup + $NoProduction;

				$AllActualProduction =  !empty($ActualProduction) ? ($ActualProduction * 100) / $total : 0;
				$AllSetup = !empty($Setup) ?  ($Setup * 100) / $total : 0;
				$AllNoProduction =  !empty($NoProduction) ? ($NoProduction * 100) / $total : 0;


				$ActualProductionRunning =  !empty($ActualProductionRunning) ? ($ActualProductionRunning * 100) / $total : 0;
				$ActualProductionWaiting = !empty($ActualProductionWaiting) ?  ($ActualProductionWaiting * 100) / $total : 0;
				$ActualProductionStopped =  !empty($ActualProductionStopped) ? ($ActualProductionStopped * 100) / $total : 0;
				$ActualProductionOff =  !empty($ActualProductionOff) ? ($ActualProductionOff * 100) / $total : 0;
				$ActualProductionNodet =  !empty($ActualProductionNodet) ? ($ActualProductionNodet * 100) / $total : 0;

				$SetupRunning =  !empty($SetupRunning) ? ($SetupRunning * 100) / $total : 0;
				$SetupWaiting = !empty($SetupWaiting) ?  ($SetupWaiting * 100) / $total : 0;
				$SetupStopped =  !empty($SetupStopped) ? ($SetupStopped * 100) / $total : 0;
				$SetupOff =  !empty($SetupOff) ? ($SetupOff * 100) / $total : 0;
				$SetupNodet =  !empty($SetupNodet) ? ($SetupNodet * 100) / $total : 0;

				$NoProductionRunning =  !empty($NoProductionRunning) ? ($NoProductionRunning * 100) / $total : 0;
				$NoProductionWaiting = !empty($NoProductionWaiting) ?  ($NoProductionWaiting * 100) / $total : 0;
				$NoProductionStopped =  !empty($NoProductionStopped) ? ($NoProductionStopped * 100) / $total : 0;
				$NoProductionOff =  !empty($NoProductionOff) ? ($NoProductionOff * 100) / $total : 0;
				$NoProductionNodet =  !empty($NoProductionNodet) ? ($NoProductionNodet * 100) / $total : 0;


				if (!empty($AllActualProduction) || !empty($AllSetup) || !empty($AllNoProduction)) 
				{
					$isDataAvailable = 1;
				}else
				{
					$isDataAvailable = 0;
				}


				
				$machines[$key]['data'] = array(
							"ActualProductionRunning" => $ActualProductionRunning,
							"ActualProductionWaiting" => $ActualProductionWaiting,
							"ActualProductionStopped" => $ActualProductionStopped,
							"ActualProductionOff" => $ActualProductionOff,
							"ActualProductionNodet" => $ActualProductionNodet,
							"ActualProduction" => $AllActualProduction,
							"SetupRunning" => $SetupRunning,
							"SetupWaiting" => $SetupWaiting,
							"SetupStopped" => $SetupStopped,
							"SetupOff" => $SetupOff,
							"SetupNodet" => $SetupNodet,
							"Setup" => $AllSetup,
							"NoProductionRunning" => $NoProductionRunning,
							"NoProductionWaiting" => $NoProductionWaiting,
							"NoProductionStopped" => $NoProductionStopped,
							"NoProductionOff" => $NoProductionOff,
							"NoProductionNodet" => $NoProductionNodet,
							"NoProduction" => $AllNoProduction,			
							"isDataAvailable" => $isDataAvailable	
						);
			}
				
				
				$json =  array("status" => "1","data" => $machines,"massage" => Workingmachinestatisticsdata,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
			
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }


    //downtimeReason use for get breakdown notification in oppApp  
	public function workingMachineDowntimeReason_post() 
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');
		
		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  


	    	$breakdownReasonList = $this->APIM->getBreakdownReasonList(); 
	    	$breakdownReasonList = array_column($breakdownReasonList, "breakdownReasonText");
	    	$completion = $this->input->post('completion');

	    	if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} else { //
				$isOperator = 1;
			}

	    	$machines = $this->APIM->getWorkingMachine($factoryId, $isOperator, $userId,"0")->result_array();

			foreach ($machines as $key => $value) 
			{
				$machineId = $value['machineId'];

				$lastCkecingData = $this->APIM->getLastCheckInDetails($factoryId,$userId);
				if ($lastCkecingData['isViewIn'] == "0") 
				{
					$machines[$key]['isViewIn'] = "1";
				}else
				{
					if (!empty($lastCkecingData['workingMachine'])) 
					{
						$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
						if (in_array($machineId, $workingMachineArrData)) 
						{
							$machines[$key]['isViewIn'] = "1";
						}else
						{
							$machines[$key]['isViewIn'] = "0";
						}
					}else
					{
						$machines[$key]['isViewIn'] = "0";
					}
				}

				$machines[$key]['receiveErrorNotification'] = $value['notifyStop'];
				$machines[$key]['provideReasonForErrors'] = $value['prolongedStop'];
				$machines[$key]['receiveWaitingNotification'] = $value['notifyWait'];
				$machines[$key]['provideReasonForWaiting'] = $value['prolongedWait'];
				
				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$key]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$key]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$key]['phoneId'] = $value['phoneId'];
					$machines[$key]['batteryLevel'] = $value['batteryLevel'];
					$machines[$key]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$key]['phoneId'] = "0";
					$machines[$key]['batteryLevel'] = "0";
					$machines[$key]['chargeFlag'] = "0";
				}

				$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($factoryId,$machineId);

				$machines[$key]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;

				$machines[$key]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$key]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";


				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$key]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$key]['machineLightColors'][$z] == 'FF0000') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == '00FF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$key]['machineLightColors'][$z] == '0000FF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$key]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$key]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$key]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$key]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$key]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$key]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$key]['machineLightStatus']['redStatus'], $machines[$key]['machineLightStatus']['greenStatus'], $machines[$key]['machineLightStatus']['yellowStatus'], $machines[$key]['machineLightStatus']['whiteStatus'],$machines[$key]['machineLightStatus']['blueStatus']); 
				}
				

		    	//completion = 0 unanswered 
		    	//completion = 1 answered
		    	
				$result = $this->APIM->getWarningNotifications($factoryId, $machineId, $userId,$completion);
				//print_r($result);exit;
				$removeNotArrayKey = array();
				foreach ($result as $keyNot => $valueNot) 
				{	

					$addedDate =  $this->APIM->getNotificationAddedDateWithIsDataGet($factoryId, $valueNot->logId,$value['isDataGet']);
						//set breakdown update time
					$changeDate =  $this->APIM->getNotificationChangeDateWithIsDataGet($factoryId, $machineId, $valueNot->logId,$value['isDataGet']);
					if($changeDate!= false  && $changeDate->originalTime != $addedDate->originalTime) { 
						$result[$keyNot]->changeDate = date("M jS | H:i A",strtotime($changeDate->originalTime)); 
						$result[$keyNot]->present = 0; 
						$endDate = $changeDate->originalTime;
					} else {
						$result[$keyNot]->present = 1; 
						$result[$keyNot]->changeDate = "Present"; 
						$endDate = date('Y-m-d H:i:s');
					}
					if(isset($addedDate) && $addedDate!= false) {  
						$result[$keyNot]->addedDate = date("M jS | H:i A",strtotime($addedDate->originalTime));	
						$startDate = $addedDate->originalTime;
					} else {
						$result[$keyNot]->addedDate = date("M jS | H:i A");	 
						$startDate = date('Y-m-d H:i:s');
					}

					$to_time = strtotime($startDate);
					$from_time = strtotime($endDate);
					$result[$keyNot]->durationTime = round(abs($to_time - $from_time) / 60). " min";

					$notificationMin = round(abs($to_time - $from_time) / 60);
					if (!empty($value['stopUpperLimit']) && $valueNot->type == "Stop" && $value['stopUpperLimit'] < $notificationMin) 
					{
						$removeNotArrayKey[] = $keyNot;
					}

					if (!empty($value['waitUpperLimit']) && $valueNot->type == "Wait" && $value['waitUpperLimit'] < $notificationMin) 
					{
						$removeNotArrayKey[] = $keyNot;
					}

					$checkPosition = $this->APIM->getWhereSingle($factoryId,array("breakdownReason" => $valueNot->comment,"machineId" => $machineId),"machineBreakdownReason");
					/*if ($machineId == "10") 
					{
						echo $valueNot->comment;
						print_r($checkPosition);exit;
					}*/
					if (!empty($checkPosition) && !empty($valueNot->comment)) 
					{
						$result[$keyNot]->selectedPosition = $checkPosition->position; 	
						$result[$keyNot]->selectedReason = $valueNot->comment; 		
					}else
					{
						if (!empty($valueNot->comment)) 
						{
							$result[$keyNot]->selectedPosition = "10"; 	
							$result[$keyNot]->selectedReason = $valueNot->comment; 	
						}
						else
						{
							$result[$keyNot]->selectedPosition = "0"; 	
							$result[$keyNot]->selectedReason = $valueNot->comment; 	
						}
					}
					
				}

				$result = $this->array_except($result, $removeNotArrayKey);
        		$result = array_values($result);

				$machines[$key]['data'] = $result;

				unset($removeNotArrayKey);

				$m = 1;
				$breakdownReasonsCustom = array();
				for ($f=0; $f < 10; $f++) 
				{ 
					$select = "machineBreakdownReason.*,errorType.errorTypeText";
					$join = array("r_nytt_main.errorType" => "machineBreakdownReason.errorTypeId = errorType.errorTypeId");
					$resultMachineBreakdownReason = $this->APIM->getWhereJoinSingle($factoryId,$select,array("position" => $m,"machineId" => $machineId),$join,"machineBreakdownReason");
					if (!empty($resultMachineBreakdownReason)) 
					{
						$breakdownReasonsCustom[$f]['position'] = $m;
						$breakdownReasonsCustom[$f]['breakdownReason'] = $resultMachineBreakdownReason->breakdownReason;
						$breakdownReasonsCustom[$f]['errorTypeText'] = $resultMachineBreakdownReason->errorTypeText;

						if ($resultMachineBreakdownReason->errorTypeText == "Machine") 
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = Machine;
						}else if ($resultMachineBreakdownReason->errorTypeText == "Tool") 
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = Tool;
						}else if ($resultMachineBreakdownReason->errorTypeText == "Operator") 
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = Operator;
						}else if ($resultMachineBreakdownReason->errorTypeText == "Material") 
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = Material;
						}else if ($resultMachineBreakdownReason->errorTypeText == "Other") 
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = Other;
						}else
						{
							$breakdownReasonsCustom[$f]['errorTypeTextLan'] = $resultMachineBreakdownReason->errorTypeText;
						}

						$breakdownReasonsCustom[$f]['errorTypeId'] = $resultMachineBreakdownReason->errorTypeId;
						$breakdownReasonsCustom[$f]['type'] = "update";
					}else
					{
						$breakdownReasonsCustom[$f]['position'] = $m;
						$breakdownReasonsCustom[$f]['breakdownReason'] = "";
						$breakdownReasonsCustom[$f]['errorTypeText'] = "";
						$breakdownReasonsCustom[$f]['errorTypeId'] = "";
						$breakdownReasonsCustom[$f]['errorTypeTextLan'] = "";
						$breakdownReasonsCustom[$f]['type'] = "update";
					}
					$m++;
				}

				$machines[$key]['breakdownReasonsCustom'] = $breakdownReasonsCustom;


			}

			$errorType = $this->APIM->getWhereDB(array("isDeleted" => "0"),"errorType");
			foreach ($errorType as $key => $value) 
			{
				if ($value['errorTypeText'] == "Machine") 
				{
					$errorType[$key]['errorTypeTextLan'] = Machine;
				}else if ($value['errorTypeText'] == "Tool") 
				{
					$errorType[$key]['errorTypeTextLan'] = Tool;
				}else if ($value['errorTypeText'] == "Operator") 
				{
					$errorType[$key]['errorTypeTextLan'] = Operator;
				}else if ($value['errorTypeText'] == "Material") 
				{
					$errorType[$key]['errorTypeTextLan'] = Material;
				}else if ($value['errorTypeText'] == "Other") 
				{
					$errorType[$key]['errorTypeTextLan'] = Other;
				}
			}
			$keysMachine = array_column($machines, 'isViewIn');
			array_multisort($keysMachine, SORT_DESC, $machines);
			$json =  array("status" => "1","message" => "Notification data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"errorType" => $errorType,"breakdownReasonList" => $breakdownReasonList,"data" => $machines);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
	}

	public function workingMachineTaskMaintenance_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  


			$userIds = $this->input->post('userIds');
			$date = date('Y-m-d',strtotime($this->input->post('date')));
			$status = $this->input->post('status');
			$day = date('l',strtotime($date));

			if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} else { //
				$isOperator = 1;
			}

	    	$machines = $this->APIM->getWorkingMachine($factoryId, $isOperator, $userId,"0")->result_array();

			foreach ($machines as $machineKey => $value) 
			{


				/*$machines[$machineKey]['data'] = array(
					"isDataAvailable" => 0,
					"day" => array(),
					"week" => array(),
					"month" => array(),
					"year" => array()
				);*/
				$machineId = $value['machineId'];

				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$machineKey]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$machineKey]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$machineKey]['phoneId'] = $value['phoneId'];
					$machines[$machineKey]['batteryLevel'] = $value['batteryLevel'];
					$machines[$machineKey]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$machineKey]['phoneId'] = "0";
					$machines[$machineKey]['batteryLevel'] = "0";
					$machines[$machineKey]['chargeFlag'] = "0";
				}

				$machines[$machineKey]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$machineKey]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";


				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$machineKey]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$machineKey]['machineLightColors'][$z] == 'FF0000') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == '00FF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$machineKey]['machineLightColors'][$z] == '0000FF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$machineKey]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$machineKey]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$machineKey]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$machineKey]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$machineKey]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$machineKey]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$machineKey]['machineLightStatus']['redStatus'], $machines[$machineKey]['machineLightStatus']['greenStatus'], $machines[$machineKey]['machineLightStatus']['yellowStatus'], $machines[$machineKey]['machineLightStatus']['whiteStatus'],$machines[$machineKey]['machineLightStatus']['blueStatus']); 
				}
				
				

				if ($day == "Saturday") 
				{
					$date = date('Y-m-d',strtotime($date ."+2 day"));
				}
				else if ($day == "Sunday") 
				{
					$date = date('Y-m-d',strtotime($date ."+1 day"));
				}

				$compareDateCheck = $date;
		        $compareWeekCheck = date('W',strtotime($date));
		        $compareMonthCheck = date('m',strtotime($date));
		        $compareYearCheck = date('Y',strtotime($date));

		        $currentDate = date('Y-m-d');
		        $dayCheck = date('l',strtotime($currentDate));

		    	if ($dayCheck == "Saturday") 
				{
					$currentDate = date('Y-m-d',strtotime("+2 day"));
				}
				else if ($dayCheck == "Sunday") 
				{
					$currentDate = date('Y-m-d',strtotime("+1 day"));
				}

		        $currentWeek = date('W',strtotime($currentDate));
		        $currentMonth = date('m',strtotime($currentDate));
		        $currentYear = date('Y',strtotime($currentDate));
		      

	            //Get never task as par assign machine and operator
		        $resultNever = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'never')->result_array();
	            //Get every day task as par assign machine and operator
	            $resultEveryDay = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyDay')->result_array();
	            $resultNever = array_merge($resultNever,$resultEveryDay);

	            foreach ($resultNever as $key => $value) 
	            {
	            	$userIdsNever = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultNever[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultNever[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultNever[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultNever[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultNever[$key]['color'] = "#FFFFFF";	
					}

					if ($currentDate >=  $compareDateCheck) 
		        	{
		        		if ($currentDate ==  $compareDateCheck) 
		        		{
		        			$resultNever[$key]['isEdit'] = 1;
		        			$resultNever[$key]['message'] = "";
		        		}else
		        		{
		        			$resultNever[$key]['isEdit'] = 0;
		        			$resultNever[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultNever[$key]['isEdit'] = 0;
		        		$resultNever[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultNever[$key]['status'] = "uncompleted";
		        		if ($resultNever[$key]['repeat'] != "never") {
		        			$resultNever[$key]['dueDate'] = date('Y-m-d',strtotime($date));
		        		}
		        	}


					$resultNever[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsNever as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultNever[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

	            }

	            //Get week task as par assign machine and operator
	            $resultWeek = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyWeek')->result_array();

	            foreach ($resultWeek as $key => $value) 
	            {
	            	$userIdsWeek = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultWeek[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultWeek[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultWeek[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultWeek[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultWeek[$key]['color'] = "#FFFFFF";	
					}

					if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentWeek ==  $compareWeekCheck) 
		        		{
		        			$resultWeek[$key]['isEdit'] = 1;
			        		$resultWeek[$key]['message'] = "";
		        		}else
		        		{
		        			$resultWeek[$key]['isEdit'] = 0;
		        			$resultWeek[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultWeek[$key]['isEdit'] = 0;
		        		$resultWeek[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultWeek[$key]['status'] = "uncompleted";

		        		if ($day == "Friday") 
						{
							$endDate = date('Y-m-d',strtotime($date));
						}else
						{
							$endDate = date('Y-m-d',strtotime($date. "next friday"));
						}

			        	$resultWeek[$key]['dueDate'] = $endDate;
		        	}

					
		        	$resultWeek[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsWeek as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultWeek[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

	            }

	             //Get month task as par assign machine and operator
	            $resultMonth = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyMonth')->result_array();

	            foreach ($resultMonth as $key => $value) 
	            {
	            	$userIdsMonth = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultMonth[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultMonth[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultMonth[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultMonth[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultMonth[$key]['color'] = "#FFFFFF";	
					}

					if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentMonth ==  $compareMonthCheck) 
		        		{
			        		$resultMonth[$key]['isEdit'] = 1;
			        		$resultMonth[$key]['message'] = "";
		        		}else
		        		{
		        			$resultMonth[$key]['isEdit'] = 0;
		        			$resultMonth[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultMonth[$key]['isEdit'] = 0;
		        		$resultMonth[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultMonth[$key]['status'] = "uncompleted";

		        		$resultMonth[$key]['dueDate'] = date('Y-m-t',strtotime($date));
		        	}

		        	$resultMonth[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsMonth as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultMonth[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

	            }

	             //Get year task as par assign machine and operator
	            $resultYear = $this->APIM->getTaskMaintenaceList($factoryId,$userId,$userIds,$date,$status,$machineId,'everyYear')->result_array();

	            foreach ($resultYear as $key => $value) 
	            {
	            	$userIdsYear = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultYear[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultYear[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultYear[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultYear[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultYear[$key]['color'] = "#FFFFFF";	
					}

					$resultYear[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;

					if ($currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentYear ==  $compareYearCheck) 
		        		{
			        		$resultYear[$key]['isEdit'] = 1;
			        		$resultYear[$key]['message'] = "";
		        		}else
		        		{
		        			$resultYear[$key]['isEdit'] = 0;
		        			$resultYear[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultYear[$key]['isEdit'] = 0;
		        		$resultYear[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultYear[$key]['status'] = "uncompleted";
		        		$resultYear[$key]['dueDate'] = date('Y-12-31',strtotime($date));
		        	}
		        	
					$i = 0;
					foreach ($userIdsYear as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultYear[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}
				}

				if (!empty($resultNever) || !empty($resultWeek) || !empty($resultMonth) || !empty($resultYear)) 
				{
					$isDataAvailable = 1;
				}else
				{
					$isDataAvailable = 0;
				}

				$machines[$machineKey]['data'] = array(
						"isDataAvailable" => $isDataAvailable,
						"day" => $resultNever,
						"week" => $resultWeek,
						"month" => $resultMonth,
						"year" => $resultYear
					);


				unset($resultNever);
				unset($resultWeek);
				unset($resultMonth);
				unset($resultYear);

            }


			
			//Check all  response data to send response in api
			/*if (!empty($resultNever) || !empty($resultWeek) || !empty($resultMonth) || !empty($resultYear)) 
			{*/
				$json =  array("status" => "1","message" => "Task data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"dayTitle" => "DAY - ".date('jS',strtotime($date)).' of '.date('F',strtotime($date)),"weekTitle" => "WEEK - ". date('W',strtotime($date)),"monthTitle" => "MONTH - ". date('F',strtotime($date)),"yearTitle" => "YEAR - ". date('Y',strtotime($date)),"data" => $machines);
			/*}else
			{
				$result = array(
						"dayTitle" => "DAY - ".date('jS',strtotime($date)).' of '.date('F',strtotime($date)),
						"weekTitle" => "WEEK - ". date('W',strtotime($date)),
						"monthTitle" => "MONTH - ". date('F',strtotime($date)),
						"yearTitle" => "YEAR - ". date('Y',strtotime($date)),
						"day" => array(),
						"week" => array(),
						"month" => array(),
						"year" => array() ); 
				$json =  array("status" => "1","message" => "Task data not available for this filter","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $result);
			}	*/
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }


    public function workingMachineUpcomingTaskMaintenance_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$date = date('Y-m-d',strtotime($this->input->post('date')));
			$day = date('l',strtotime($date));

			if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} else 
			{ 
				$isOperator = 1;
			}

	    	$machines = $this->APIM->getWorkingMachine($factoryId, $isOperator, $userId,"0")->result_array();

			foreach ($machines as $machineKey => $value) 
			{
				$machineId = $value['machineId'];

				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$machineKey]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$machineKey]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$machineKey]['phoneId'] = $value['phoneId'];
					$machines[$machineKey]['batteryLevel'] = $value['batteryLevel'];
					$machines[$machineKey]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$machineKey]['phoneId'] = "0";
					$machines[$machineKey]['batteryLevel'] = "0";
					$machines[$machineKey]['chargeFlag'] = "0";
				}

				$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($factoryId,$machineId);

				$machines[$machineKey]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;

				$machines[$machineKey]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$machineKey]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";


				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$machineKey]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$machineKey]['machineLightColors'][$z] == 'FF0000') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == '00FF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$machineKey]['machineLightColors'][$z] == '0000FF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$machineKey]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$machineKey]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$machineKey]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$machineKey]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$machineKey]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$machineKey]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$machineKey]['machineLightStatus']['redStatus'], $machines[$machineKey]['machineLightStatus']['greenStatus'], $machines[$machineKey]['machineLightStatus']['yellowStatus'], $machines[$machineKey]['machineLightStatus']['whiteStatus'],$machines[$machineKey]['machineLightStatus']['blueStatus']); 
				}
				
				

			/*	if ($day == "Saturday") 
				{
					$date = date('Y-m-d',strtotime($date ."+2 day"));
				}
				else if ($day == "Sunday") 
				{
					$date = date('Y-m-d',strtotime($date ."+1 day"));
				}*/

				$compareDateCheck = $date;
		        $compareWeekCheck = date('W',strtotime($date));
		        $compareMonthCheck = date('m',strtotime($date));
		        $compareYearCheck = date('Y',strtotime($date));

		        $currentDate = date('Y-m-d');
		        $dayCheck = date('l',strtotime($currentDate));

		    	/*if ($dayCheck == "Saturday") 
				{
					$currentDate = date('Y-m-d',strtotime("+2 day"));
				}
				else if ($dayCheck == "Sunday") 
				{
					$currentDate = date('Y-m-d',strtotime("+1 day"));
				}*/

		        $currentWeek = date('W',strtotime($currentDate));
		        $currentMonth = date('m',strtotime($currentDate));
		        $currentYear = date('Y',strtotime($currentDate));
		      

	            //Get every day task as par assign machine and operator
	            $resultDueDate = $this->APIM->getTaskData($factoryId,$userId,$date,$machineId,"today")->result_array();

	            foreach ($resultDueDate as $key => $value) 
	            {
	            	$userIdsNever = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultDueDate[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultDueDate[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultDueDate[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultDueDate[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultDueDate[$key]['color'] = "#FFFFFF";	
					}

					if ($currentDate >=  $compareDateCheck) 
		        	{
		        		if ($currentDate ==  $compareDateCheck) 
		        		{
		        			$resultDueDate[$key]['isEdit'] = 1;
		        			$resultDueDate[$key]['message'] = "";
		        		}else
		        		{
		        			$resultDueDate[$key]['isEdit'] = 0;
		        			$resultDueDate[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultDueDate[$key]['isEdit'] = 0;
		        		$resultDueDate[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultDueDate[$key]['status'] = "uncompleted";
		        		if ($resultDueDate[$key]['repeat'] != "never") 
		        		{
		        			if ($value['repeat'] == "everyWeek") 
		        			{
		        				if ($value['newTask'] == "1") 
		        				{
		        					if ($day == $value['dueWeekDay']) 
		        					{
		        						$endDate = date('Y-m-d',strtotime($date));
		        					}else
		        					{
		        						$endDate = date('Y-m-d',strtotime($date. "next ".$value['dueWeekDay']));
		        					}
		        				}else
		        				{
		        					if ($day == "Friday") 
									{
										$endDate = date('Y-m-d',strtotime($date));
									}else
									{
										$endDate = date('Y-m-d',strtotime($date. "next friday"));
									}
		        				}
		        				$resultDueDate[$key]['dueDate'] = $endDate;	
		        			}elseif ($value['repeat'] == "everyMonth") 
		        			{
		        				if ($value['newTask'] == "1") 
		        				{
		        					if ($value['monthDueOn'] == "1") 
									{
										if ($value['dueMonthMonth'] != "third_last_day" && $value['dueMonthMonth'] != "second_last_day" && $value['dueMonthMonth'] != "last_day") 
										{
											$endDate = date('Y-m-'.$value['dueMonthMonth'],strtotime($date));
											$endDate = date('Y-m-d',strtotime($endDate));
										}else
										{	
											$endDate = date('Y-m-t',strtotime($date));
											if ($value['dueMonthMonth'] == "third_last_day") 
											{
												$endDate = date('Y-m-d',strtotime("-2 day ".$endDate));
											}elseif ($value['dueMonthMonth'] == "second_last_day") 
											{
												$endDate = date('Y-m-d',strtotime("-1 day ".$endDate));
											}
											
										}
									}elseif ($value['monthDueOn'] == "2") 
									{
										if (!empty($value['dueMonthWeek']) && !empty($value['dueMonthDay'])) 
										{
											$month = date('m',strtotime($date));
											$year = date('Y',strtotime($date));
											$weekEndDate = $this->getFirstandLastDate($year,$month,$value['dueMonthWeek'],$value['dueMonthDay']);
											
											if ($month != date('m',strtotime($weekEndDate))) 
											{
											
												$weekEndDate = $this->getFirstandLastDate($year,$month,'4',$value['dueMonthDay']);
											}

											
											$endDate = $weekEndDate;
										}
									}	
		        				}else
		        				{
		        					$endDate = date('Y-m-t',strtotime($date));
		        				}

		        				$resultDueDate[$key]['dueDate']  = $endDate;
		        			}elseif ($value['repeat'] == "everyYear") 
		        			{
		        				if ($value['yearDueOn'] == "1") 
								{
									$monthYear = date('m',strtotime($value['dueYearMonth']));
									if ($value['dueYearMonthDay'] != "third_last_day" && $value['dueYearMonthDay'] != "second_last_day" && $value['dueYearMonthDay'] != "last_day") 
									{
										$endDate = date('Y-'.$monthYear.'-'.$value['dueYearMonthDay'],strtotime($date));
									}else
									{	
										$endDate = date('Y-'.$monthYear.'-t',strtotime($date));
										if ($value['dueYearMonthDay'] == "third_last_day") 
										{
											$endDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$endDate));
										}elseif ($value['dueYearMonthDay'] == "second_last_day") 
										{
											$endDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$endDate));
										}else
										{
											$endDate = date('Y-m-t',strtotime($endDate));
										}

									}
									$endDate = date('Y-m-d',strtotime($endDate));

									$resultDueDate[$key]['dueDate']  = $endDate;
								}elseif ($value['yearDueOn'] == "2") 
								{
									if(!empty($value['dueYearMonthOnThe']) && !empty($value['dueYearWeek']) && !empty($value['dueYearDay']))
									{
										$year = date('Y',strtotime($date));
										$monthYear = date('m',strtotime($value['dueYearMonthOnThe']));
										$weekEndDate = $this->getFirstandLastDate($year,$monthYear,$value['dueYearWeek'],$value['dueYearDay']);
										if ($monthYear != date('m',strtotime($weekEndDate))) 
										{
											$weekEndDate = $this->getFirstandLastDate($year,$monthYear,'4',$value['dueYearDay']);
										}

										$endDate = $weekEndDate;
									}

									$resultDueDate[$key]['dueDate']  = $endDate;

								}
		        			}
		        			else
		        			{
		        				$resultDueDate[$key]['dueDate'] = date('Y-m-d',strtotime($date));
		        			}
		        		}
		        	}


					$resultDueDate[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsNever as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultDueDate[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

					if (date('Y-m-d') < $date) 
					{
						if ($resultDueDate[$key]['dueDate'] != $date) 
						{
							$removeArrayKey[] = $key;
						}
					}


	            }


	            //Get every day task as par assign machine and operator
	            $resultMonthUpcoming = $this->APIM->getTaskData($factoryId,$userId,$date,$machineId,"month")->result_array();
	            $resultYearUpcoming = $this->APIM->getTaskData($factoryId,$userId,$date,$machineId,"year")->result_array();
	            $resultUpcoming = array_merge($resultMonthUpcoming,$resultYearUpcoming);

	            foreach ($resultUpcoming as $key => $value) 
	            {
	            	$userIdsUpcoming = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultUpcoming[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultUpcoming[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultUpcoming[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultUpcoming[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultUpcoming[$key]['color'] = "#FFFFFF";	
					}

					if ($currentDate >=  $compareDateCheck) 
		        	{
		        		if ($currentDate ==  $compareDateCheck) 
		        		{
		        			$resultUpcoming[$key]['isEdit'] = 1;
		        			$resultUpcoming[$key]['message'] = "";
		        		}else
		        		{
		        			$resultUpcoming[$key]['isEdit'] = 0;
		        			$resultUpcoming[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultUpcoming[$key]['isEdit'] = 1;
		        		$resultUpcoming[$key]['message'] = "";
		        	}


					$resultUpcoming[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsUpcoming as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultUpcoming[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

	            }

	           
	            $resultDueDate = $this->array_except($resultDueDate, $removeArrayKey);
				$resultDueDate = array_values($resultDueDate);

				if (!empty($resultDueDate) || !empty($resultUpcoming)) 
				{
					$isDataAvailable = 1;
				}else
				{
					$isDataAvailable = 0;
				}

				$machines[$machineKey]['data'] = array(
						"isDataAvailable" => $isDataAvailable,
						"todayDueDate" => $resultDueDate,
						"upcoming" => $resultUpcoming,
					);
				

				unset($resultDueDate);
				unset($removeArrayKey);
            }

			$json =  array(
							"status" => "1",
							"message" => "Task data",
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"filterSelectedDate" => date('F jS, Y',strtotime($this->input->post('date'))),
							"todayDueDateTitle" => ($date == date('Y-m-d')) ? "Today's tasks" :  date('jS',strtotime($date)).' of '.date('F',strtotime($date))." tasks",
							"upcomingTitle" => Upcomingmonthlyandyearlytasks,
							"data" => $machines
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 

    public function workingMachineStatisticsWithPagination_post()
	{
	    $this->form_validation->set_rules('choose1', 'choose1', 'required');
	    $this->form_validation->set_rules('choose2', 'choose2', 'required');
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

	    //check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);
		
		//check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
		}
		else
		{  
			$choose1  = $this->input->post('choose1');
			$choose2  = $this->input->post('choose2');
			

			if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} else { //
				$isOperator = 1;
			}

			$limit = 5;
			$start = !empty($this->input->post('start')) ? $this->input->post('start') : 0;


			//get operator assigned machine 

			$machines = $this->APIM->getWorkingMachineWithPagination($factoryId, $isOperator, $userId,"0",$limit,$start)->result_array();

			foreach ($machines as $key => $value) 
			{
				$machineId = $value['machineId'];

				$lastCkecingData = $this->APIM->getLastCheckInDetails($factoryId,$userId);
				if ($lastCkecingData['isViewIn'] == "0") 
				{
					$machines[$key]['isViewIn'] = "1";
				}else
				{
					if (!empty($lastCkecingData['workingMachine'])) 
					{
						$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
						if (in_array($machineId, $workingMachineArrData)) 
						{
							$machines[$key]['isViewIn'] = "1";
						}else
						{
							$machines[$key]['isViewIn'] = "0";
						}
					}else
					{
						$machines[$key]['isViewIn'] = "0";
					}
				}

				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$key]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$key]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$key]['phoneId'] = $value['phoneId'];
					$machines[$key]['batteryLevel'] = $value['batteryLevel'];
					$machines[$key]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$key]['phoneId'] = "0";
					$machines[$key]['batteryLevel'] = "0";
					$machines[$key]['chargeFlag'] = "0";
				}

				$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($factoryId,$machineId);

				$machines[$key]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;

				$machines[$key]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$key]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";

				$machines[$key]['choose1'] = $choose1;
				$machines[$key]['choose2'] = $choose2;

				$choose3 = explode("/", $choose2);
	            $month = $choose3[0];
	            $year = $choose3[1];
	            $day = "01";
				$monthName = date('F',strtotime($month.'/'.$day.'/'.$year));
				if ($monthName == "January") 
				{
				    $monthName = January;
				}else if ($monthName == "February") 
				{
				    $monthName = February;
				}else if ($monthName == "March") 
				{
				    $monthName = March;
				}else if ($monthName == "April") 
				{
				    $monthName = April;
				}else if ($monthName == "May") 
				{
				    $monthName = May;
				}else if ($monthName == "June") 
				{
				    $monthName = June;
				}else if ($monthName == "July") 
				{
				    $monthName = July;
				}else if ($monthName == "August") 
				{
				    $monthName = august;
				}else if ($monthName == "September") 
				{
				    $monthName = September;
				}else if ($monthName == "October") 
				{
				    $monthName = October;
				}else if ($monthName == "November") 
				{
				    $monthName = November;
				}else if ($monthName == "December") 
				{
				    $monthName = December;
				}

				$machines[$key]['choose2Text'] = ($choose1 == "Monthly") ? $monthName.'/'.$year : $choose2;

				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$key]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$key]['machineLightColors'][$z] == 'FF0000') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == '00FF00') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$key]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$key]['machineLightColors'][$z] == '0000FF') {
						$machines[$key]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$key]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$key]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$key]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$key]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$key]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$key]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$key]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$key]['machineLightStatus']['redStatus'], $machines[$key]['machineLightStatus']['greenStatus'], $machines[$key]['machineLightStatus']['yellowStatus'], $machines[$key]['machineLightStatus']['whiteStatus'],$machines[$key]['machineLightStatus']['blueStatus']); 
				}
				



				//ActualProduction
				//Get Actual production data by machine state
				$resultActualProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','running');
				$ActualProductionRunning = $resultActualProductionRunning->countVal;
				$resultActualProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','waiting');
				$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
				$resultActualProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','stopped');
				$ActualProductionStopped = $resultActualProductionStopped->countVal;
				$resultActualProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','off');
				$ActualProductionOff = $resultActualProductionOff->countVal;
				$resultActualProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'0','nodet');
				$ActualProductionNodet = $resultActualProductionNodet->countVal;

				$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet;

				$ActualProductionHour = $this->secondsToHoursOnePoint($ActualProduction);

				//Setup

				//Get setup data by machine state
				$resultSetupRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','running');
				$SetupRunning = $resultSetupRunning->countVal;
				$resultSetupWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','waiting');
				$SetupWaiting = $resultSetupWaiting->countVal;
				$resultSetupStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','stopped');
				$SetupStopped = $resultSetupStopped->countVal;
				$resultSetupOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','off');
				$SetupOff = $resultSetupOff->countVal;
				$resultSetupNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'1','nodet');
				$SetupNodet = $resultSetupNodet->countVal;

				$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet;

				$SetupHour = $this->secondsToHoursOnePoint($Setup);
				//No production
				//Getting no production data by machine state
				$resultNoProductionRunning = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','running');
				$NoProductionRunning = $resultNoProductionRunning->countVal;
				$resultNoProductionWaiting = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','waiting');
				$NoProductionWaiting = $resultNoProductionWaiting->countVal;
				$resultNoProductionStopped = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','stopped');
				$NoProductionStopped = $resultNoProductionStopped->countVal;
				$resultNoProductionOff = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','off');
				$NoProductionOff = $resultNoProductionOff->countVal;
				$resultNoProductionNodet = $this->APIM->getMachineStatusData($factoryId,$machineId,$choose1,$choose2,'2','nodet');
				$NoProductionNodet = $resultNoProductionNodet->countVal;

				$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet;

				$NoProductionHour = $this->secondsToHoursOnePoint($NoProduction);
			
				//$total = $ActualProduction;
				$total = $ActualProduction + $Setup + $NoProduction;

				$AllActualProduction =  !empty($ActualProduction) ? ($ActualProduction * 100) / $total : 0;
				$AllSetup = !empty($Setup) ?  ($Setup * 100) / $total : 0;
				$AllNoProduction =  !empty($NoProduction) ? ($NoProduction * 100) / $total : 0;


				$ActualProductionRunning =  !empty($ActualProductionRunning) ? ($ActualProductionRunning * 100) / $total : 0;
				$ActualProductionWaiting = !empty($ActualProductionWaiting) ?  ($ActualProductionWaiting * 100) / $total : 0;
				$ActualProductionStopped =  !empty($ActualProductionStopped) ? ($ActualProductionStopped * 100) / $total : 0;
				$ActualProductionOff =  !empty($ActualProductionOff) ? ($ActualProductionOff * 100) / $total : 0;
				$ActualProductionNodet =  !empty($ActualProductionNodet) ? ($ActualProductionNodet * 100) / $total : 0;

				$SetupRunning =  !empty($SetupRunning) ? ($SetupRunning * 100) / $total : 0;
				$SetupWaiting = !empty($SetupWaiting) ?  ($SetupWaiting * 100) / $total : 0;
				$SetupStopped =  !empty($SetupStopped) ? ($SetupStopped * 100) / $total : 0;
				$SetupOff =  !empty($SetupOff) ? ($SetupOff * 100) / $total : 0;
				$SetupNodet =  !empty($SetupNodet) ? ($SetupNodet * 100) / $total : 0;

				$NoProductionRunning =  !empty($NoProductionRunning) ? ($NoProductionRunning * 100) / $total : 0;
				$NoProductionWaiting = !empty($NoProductionWaiting) ?  ($NoProductionWaiting * 100) / $total : 0;
				$NoProductionStopped =  !empty($NoProductionStopped) ? ($NoProductionStopped * 100) / $total : 0;
				$NoProductionOff =  !empty($NoProductionOff) ? ($NoProductionOff * 100) / $total : 0;
				$NoProductionNodet =  !empty($NoProductionNodet) ? ($NoProductionNodet * 100) / $total : 0;


				if (!empty($AllActualProduction) || !empty($AllSetup) || !empty($AllNoProduction)) 
				{
					$isDataAvailable = 1;
				}else
				{
					$isDataAvailable = 0;
				}


				
				$machines[$key]['data'] = array(
							"ActualProductionRunning" => $ActualProductionRunning,
							"ActualProductionWaiting" => $ActualProductionWaiting,
							"ActualProductionStopped" => $ActualProductionStopped,
							"ActualProductionOff" => $ActualProductionOff,
							"ActualProductionNodet" => $ActualProductionNodet,
							"ActualProduction" => $AllActualProduction,
							"SetupRunning" => $SetupRunning,
							"SetupWaiting" => $SetupWaiting,
							"SetupStopped" => $SetupStopped,
							"SetupOff" => $SetupOff,
							"SetupNodet" => $SetupNodet,
							"Setup" => $AllSetup,
							"NoProductionRunning" => $NoProductionRunning,
							"NoProductionWaiting" => $NoProductionWaiting,
							"NoProductionStopped" => $NoProductionStopped,
							"NoProductionOff" => $NoProductionOff,
							"NoProductionNodet" => $NoProductionNodet,
							"NoProduction" => $AllNoProduction,			
							"ActualProductionHour" => $ActualProductionHour,			
							"SetupHour" => $SetupHour,			
							"NoProductionHour" => $NoProductionHour,			
							"isDataAvailable" => $isDataAvailable	
						);
			}
				
			if (!empty($machines)) 
			{
				$keysMachine = array_column($machines, 'isViewIn');
				array_multisort($keysMachine, SORT_DESC, $machines);
				$json =  array("status" => "1","start" => $start + 5,"data" => $machines,"message" => "Working machine statistics data","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
			}else
			{
				$json =  array("status" => "0","message" => Nomoremachineavailable.'.',"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"isUserDeleted" => empty($userDetail) ? 1 : 0);
			}
			
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

     function secondsToHoursOnePoint($seconds) 
	{
		//convert seconds to hour
		return sprintf('%0.1f', $seconds/3600);
    }

    public function addTaskRepeat_post()
	{
	    $this->form_validation->set_rules('userIds','userIds','required');
        $this->form_validation->set_rules('task','task','required');
        $this->form_validation->set_rules('repeat','repeat','required');
        $this->form_validation->set_rules('userId','userId','required');
        $this->form_validation->set_rules('factoryId','factoryId','required');
        $this->form_validation->set_rules('machineId','machineId','required');

        $repeat = $this->input->post('repeat');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDate','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDay','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOn');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonth','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeek','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDay','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOn');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonth','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDay','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeek','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDay','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnThe','dueYearMonthOnThe','required');
        	}
        }

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			//echo json_encode($_POST);exit;
			$userIds = $this->input->post('userIds');
			$taskId = $this->input->post('taskId');
			$machineId =  $this->input->post('machineId');
			$task = $this->input->post('task');

			$endTaskDate = $this->input->post('endTaskDate');
			$EndAfteroccurances = $this->input->post('EndAfteroccurances');

			$dueWeekDay = $this->input->post('dueWeekDay');
			$monthDueOn = $this->input->post('monthDueOn');
			$dueMonthMonth = $this->input->post('dueMonthMonth');
			$dueMonthWeek = $this->input->post('dueMonthWeek');
			$dueMonthDay = $this->input->post('dueMonthDay');
			$yearDueOn = $this->input->post('yearDueOn');
			$dueYearMonth = $this->input->post('dueYearMonth');
			$dueYearMonthDay = $this->input->post('dueYearMonthDay');
			$dueYearWeek = $this->input->post('dueYearWeek');
			$dueYearDay = $this->input->post('dueYearDay');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnThe');

			//Set task start end date as par repeat filter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}
				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDate')));
				$repeatOrder = 1;
			}


			if (!empty($taskId)) 
            {
            	$updatedByUserId = $userId;
            }else
            {
            	$updatedByUserId = 0;
            }

           

            if (!empty($endTaskDate) && $repeat != "never") 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances) && $repeat != "never") 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." month"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." year"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			if ($insertVariable == true) 
			{

				$data=array(
	                'userIds' => $userIds,
	                'machineId' => $machineId,  
	                'task' => $task,  
	                'repeat' => $repeat,
	                'dueDate' => $dueDate,
	                'startDate' => $startDate,
	                'repeatOrder' => $repeatOrder,
	                'createdByUserId' => $userId,
	                'updatedByUserId' => $updatedByUserId,
	                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
	                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
	                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
	                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
	                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
	                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
	                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
	                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
	                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
	                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
	                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
	                'endTaskDate' => !empty($endTaskDate) ? $endTaskDate : NULL,
					'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
	            );  


	            if (empty($taskId)) 
	            {
	            	$data['newTask'] = "1";
	            	$this->APIM->insertFactoryData($factoryId, $data, "taskMaintenace");
	            	$json =  array("status" => "1","message" => "Task added successfully","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	            }else
	            {
	            	$where = array("taskId" => $taskId);
	            	$this->APIM->updateFactoryData($factoryId, $where,$data, "taskMaintenace");


	            	$lastSubTask = $this->APIM->getLastSubTask($taskId);
					if (!empty($lastSubTask)) 
					{
						$this->AM->updateData(array("taskId" => $lastSubTask->taskId),$data, "taskMaintenace");
					}

	            	$json =  array("status" => "1","message" => "Task updated successfully","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	            }
	        }else
	        {
	        	$json =  array("status" => "0","message" => Endtaskdatecannotbelessthanduedate,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
	        }


				
			
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }


    function getFirstandLastDate($year, $month, $week, $day) {

	    $thisWeek = 1;

	    for($i = 1; $i < $week; $i++) {
	        $thisWeek = $thisWeek + 7;
	    }

	    $currentDay = date('Y-m-d',mktime(0,0,0,$month,$thisWeek,$year));

	    $monday = strtotime($day.' this week', strtotime($currentDay));;

	    $weekStart = date('Y-m-d', $monday);

	    return $weekStart;
	}


	/*public function getErrorType_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			
			$result = $this->APIM->getWhereDB(array("isDeleted" => "0"),"errorType");
			$json =  array(
							"status" => "1",
							"message" => "Error type data",
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"data" => $result
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }*/

    public function getBreakdownReasons_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('machineId','machineId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			
			$result = $this->APIM->getWhere($factoryId,array("machineId" => $machineId,"breakdownReason !=" => ""),"machineBreakdownReason");

			$json =  array(
							"status" => "1",
							"message" => Breakdownreasonsdata,
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"breakdownReasonsCustom" => $result
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    public function workingMachineTaskMaintenanceLatest_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$userIds = "all"; 
			$date = date('Y-m-d',strtotime($this->input->post('date')));
			$day = date('l',strtotime($date));

			if(intVal($userDetail['userRole']) == 1 ) 
			{ 
				$isOperator = 0;
			} 
			else 
			{ 
				$isOperator = 1;
			}

	    	$machines = $this->APIM->getWorkingMachine($factoryId, $isOperator, $userId,"0")->result_array();

			foreach ($machines as $machineKey => $value) 
			{
				$machineId = $value['machineId'];


				$lastCkecingData = $this->APIM->getLastCheckInDetails($factoryId,$userId);
				if ($lastCkecingData['isViewIn'] == "0") 
				{
					$machines[$machineKey]['isViewIn'] = "1";
				}else
				{
					if (!empty($lastCkecingData['workingMachine'])) 
					{
						$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
						if (in_array($machineId, $workingMachineArrData)) 
						{
							$machines[$machineKey]['isViewIn'] = "1";
						}else
						{
							$machines[$machineKey]['isViewIn'] = "0";
						}
					}else
					{
						$machines[$machineKey]['isViewIn'] = "0";
					}
				}
				$userSoundConfig = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId,"userId" => $userId), "userSoundConfig");
				if (!empty($userSoundConfig)) 
				{
					$machines[$machineKey]['sound'] = $userSoundConfig->sound;
				}else
				{
					$machines[$machineKey]['sound'] = "1";
				}


				if ($value['chargeFlag'] != NULL) 
				{
					//setting the battery percentage and  charger is connected or not
					$machines[$machineKey]['phoneId'] = $value['phoneId'];
					$machines[$machineKey]['batteryLevel'] = $value['batteryLevel'];
					$machines[$machineKey]['chargeFlag'] = $value['chargeFlag'];
				} else 
				{ 
					//set default battery data 
					$machines[$machineKey]['phoneId'] = "0";
					$machines[$machineKey]['batteryLevel'] = "0";
					$machines[$machineKey]['chargeFlag'] = "0";
				}

				$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($factoryId,$machineId);

				$machines[$machineKey]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;

				$machines[$machineKey]['noProductionTime'] = !empty($value['noProduction']) ? $value['noProduction'] : "";
				$machines[$machineKey]['noProductionTimeIsActive'] = !empty($value['noProductionTimeIsActive']) ? $value['noProductionTimeIsActive'] : "0";


				$machineLightArr[$i] = explode(",", $value['machineLight']); 
				$listColors = $this->APIM->getAllColors();
				for($z=0;$z<count($machineLightArr[$i]);$z++) {
					for($y=0;$y<count($listColors);$y++) { 
						if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $machines[$machineKey]['machineLightColors'][$z] = $listColors[$y]->colorCode;
					}
									
					$machineLightStatus['redStatus'] = '0';
					$machineLightStatus['yellowStatus'] = '0';
					$machineLightStatus['greenStatus'] = '0';
					$machineLightStatus['whiteStatus'] = '0';
					$machineLightStatus['blueStatus'] = '0';
					
					$color = $value['color']; 
					
					$colorArr = explode(" ", $color);
					for($c=0;$c<count($colorArr);$c++) {
						if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
							$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
						}
					}
					
					if(strpos($color, 'Blue') !== false) {
						$machineLightStatus['blueStatus'] = '1';
					}
					if(strpos($color, 'Red') !== false) {
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Red and green') !== false) {
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Green') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Yellow and green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Green and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
					}
					if(strpos($color, 'Robot') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
					}
					if(strpos($color, 'Door') !== false) {
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Red Yellow Green') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['redStatus'] = '1';
						$machineLightStatus['greenStatus'] = '1';
					}
					if(strpos($color, 'Blue and yellow') !== false) {
						$machineLightStatus['yellowStatus'] = '1';
						$machineLightStatus['blueStatus'] = '1';
					}
						
					if($machines[$machineKey]['machineLightColors'][$z] == 'FF0000') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'redStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == '00FF00') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'greenStatus';
					}
					if($machines[$machineKey]['machineLightColors'][$z] == 'FFFFFF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
					} 
					if($machines[$machineKey]['machineLightColors'][$z] == '0000FF') {
						$machines[$machineKey]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
						$machines[$machineKey]['machineLightColorsStatusNames'][$z] = 'blueStatus';
					} 
					
					$machines[$machineKey]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
					$machines[$machineKey]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
					$machines[$machineKey]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
					$machines[$machineKey]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
					$machines[$machineKey]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
					
					$machines[$machineKey]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($factoryId, $machines[$i]->machineId, $machines[$machineKey]['machineLightStatus']['redStatus'], $machines[$machineKey]['machineLightStatus']['greenStatus'], $machines[$machineKey]['machineLightStatus']['yellowStatus'], $machines[$machineKey]['machineLightStatus']['whiteStatus'],$machines[$machineKey]['machineLightStatus']['blueStatus']); 
				}
				
				

		

				$compareDateCheck = $date;
		        $compareWeekCheck = date('W',strtotime($date));
		        $compareMonthCheck = date('m',strtotime($date));
		        $compareYearCheck = date('Y',strtotime($date));

		        $currentDate = date('Y-m-d');
		        $dayCheck = date('l',strtotime($currentDate));

		        $currentWeek = date('W',strtotime($currentDate));
		        $currentMonth = date('m',strtotime($currentDate));
		        $currentYear = date('Y',strtotime($currentDate));
		      

	            //Get every day task as par assign machine and operator
	            $resultDueDate = $this->APIM->getTaskData($factoryId,$userId,$date,$machineId,"today")->result_array();

	            foreach ($resultDueDate as $key => $value) 
	            {
	            	$userIdsNever = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultDueDate[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultDueDate[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultDueDate[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultDueDate[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultDueDate[$key]['color'] = "#FFFFFF";	
					}

					if ($currentDate >=  $compareDateCheck && $value['repeat'] == "everyDay") 
		        	{
		        		if ($currentDate ==  $compareDateCheck) 
		        		{
		        			$resultDueDate[$key]['isEdit'] = 1;
		        			$resultDueDate[$key]['message'] = "";
		        		}else
		        		{
		        			$resultDueDate[$key]['isEdit'] = 0;
		        			$resultDueDate[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck && $value['repeat'] == "everyWeek") 
		        	{	
		        		if ($currentWeek ==  $compareWeekCheck) 
		        		{
		        			$resultDueDate[$key]['isEdit'] = 1;
			        		$resultDueDate[$key]['message'] = "";
		        		}else
		        		{
		        			$resultDueDate[$key]['isEdit'] = 0;
		        			$resultDueDate[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck && $value['repeat'] == "everyMonth") 
		        	{	
		        		if ($currentMonth ==  $compareMonthCheck) 
		        		{
			        		$resultDueDate[$key]['isEdit'] = 1;
			        		$resultDueDate[$key]['message'] = "";
		        		}else
		        		{
		        			$resultDueDate[$key]['isEdit'] = 0;
		        			$resultDueDate[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else if ($currentYear >=  $compareYearCheck && $value['repeat'] == "everyYear") 
		        	{	
		        		if ($currentYear ==  $compareYearCheck) 
		        		{
			        		$resultDueDate[$key]['isEdit'] = 1;
			        		$resultDueDate[$key]['message'] = "";
		        		}else
		        		{
		        			$resultDueDate[$key]['isEdit'] = 0;
		        			$resultDueDate[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}
		        	else
		        	{	
		        		$resultDueDate[$key]['isEdit'] = 0;
		        		$resultDueDate[$key]['message'] = Youcannotupdatefuturetask;
		        		//$resultDueDate[$key]['status'] = "uncompleted";
		        		if ($resultDueDate[$key]['repeat'] != "never") 
		        		{
		        			if ($value['repeat'] == "everyWeek") 
		        			{
		        				if ($value['newTask'] == "1") 
		        				{
		        					if ($day == $value['dueWeekDay']) 
		        					{
		        						$endDate = date('Y-m-d',strtotime($date));
		        					}else
		        					{
		        						$endDate = date('Y-m-d',strtotime($date. "next ".$value['dueWeekDay']));
		        					}
		        				}else
		        				{
		        					if ($day == "Friday") 
									{
										$endDate = date('Y-m-d',strtotime($date));
									}else
									{
										$endDate = date('Y-m-d',strtotime($date. "next friday"));
									}
		        				}
		        				$resultDueDate[$key]['dueDate'] = $endDate;	
		        			}elseif ($value['repeat'] == "everyMonth") 
		        			{
		        				if ($value['newTask'] == "1") 
		        				{
		        					if ($value['monthDueOn'] == "1") 
									{
										if ($value['dueMonthMonth'] != "third_last_day" && $value['dueMonthMonth'] != "second_last_day" && $value['dueMonthMonth'] != "last_day") 
										{
											$endDate = date('Y-m-'.$value['dueMonthMonth'],strtotime($date));
											$endDate = date('Y-m-d',strtotime($endDate));
										}else
										{	
											$endDate = date('Y-m-t',strtotime($date));
											if ($value['dueMonthMonth'] == "third_last_day") 
											{
												$endDate = date('Y-m-d',strtotime("-2 day ".$endDate));
											}elseif ($value['dueMonthMonth'] == "second_last_day") 
											{
												$endDate = date('Y-m-d',strtotime("-1 day ".$endDate));
											}
											
										}
									}elseif ($value['monthDueOn'] == "2") 
									{
										if (!empty($value['dueMonthWeek']) && !empty($value['dueMonthDay'])) 
										{
											$month = date('m',strtotime($date));
											$year = date('Y',strtotime($date));
											$weekEndDate = $this->getFirstandLastDate($year,$month,$value['dueMonthWeek'],$value['dueMonthDay']);
											
											if ($month != date('m',strtotime($weekEndDate))) 
											{
											
												$weekEndDate = $this->getFirstandLastDate($year,$month,'4',$value['dueMonthDay']);
											}

											
											$endDate = $weekEndDate;
										}
									}	
		        				}else
		        				{
		        					$endDate = date('Y-m-t',strtotime($date));
		        				}

		        				$resultDueDate[$key]['dueDate']  = $endDate;
		        			}elseif ($value['repeat'] == "everyYear") 
		        			{
		        				if ($value['yearDueOn'] == "1") 
								{
									$monthYear = date('m',strtotime($value['dueYearMonth']));
									if ($value['dueYearMonthDay'] != "third_last_day" && $value['dueYearMonthDay'] != "second_last_day" && $value['dueYearMonthDay'] != "last_day") 
									{
										$endDate = date('Y-'.$monthYear.'-'.$value['dueYearMonthDay'],strtotime($date));
									}else
									{	
										$endDate = date('Y-'.$monthYear.'-t',strtotime($date));
										if ($value['dueYearMonthDay'] == "third_last_day") 
										{
											$endDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$endDate));
										}elseif ($value['dueYearMonthDay'] == "second_last_day") 
										{
											$endDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$endDate));
										}else
										{
											$endDate = date('Y-m-t',strtotime($endDate));
										}

									}
									$endDate = date('Y-m-d',strtotime($endDate));

									$resultDueDate[$key]['dueDate']  = $endDate;
								}elseif ($value['yearDueOn'] == "2") 
								{
									if(!empty($value['dueYearMonthOnThe']) && !empty($value['dueYearWeek']) && !empty($value['dueYearDay']))
									{
										$year = date('Y',strtotime($date));
										$monthYear = date('m',strtotime($value['dueYearMonthOnThe']));
										$weekEndDate = $this->getFirstandLastDate($year,$monthYear,$value['dueYearWeek'],$value['dueYearDay']);
										if ($monthYear != date('m',strtotime($weekEndDate))) 
										{
											$weekEndDate = $this->getFirstandLastDate($year,$monthYear,'4',$value['dueYearDay']);
										}

										$endDate = $weekEndDate;
									}

									$resultDueDate[$key]['dueDate']  = $endDate;

								}
		        			}
		        			else
		        			{
		        				$resultDueDate[$key]['dueDate'] = date('Y-m-d',strtotime($date));
		        			}
		        		}
		        	}


					$resultDueDate[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsNever as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultDueDate[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

					if (date('Y-m-d') < $date) 
					{
						if ($resultDueDate[$key]['dueDate'] != $date) 
						{
							$removeArrayKey[] = $key;
						}
					}


	            }


	            //Get every day task as par assign machine and operator
	            //Get week task as par assign machine and operator
	            $resultWeek = $this->APIM->getTaskMaintenaceListNew($factoryId,$userId,$userIds,$date,$status,$machineId,'everyWeek')->result_array();

	            foreach ($resultWeek as $key => $value) 
	            {
	            	$userIdsWeek = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultWeek[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultWeek[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultWeek[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultWeek[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultWeek[$key]['color'] = "#FFFFFF";	
					}

					if ($value['newTask'] == "1") 
    				{
    					if ($day == $value['dueWeekDay']) 
    					{
    						$endDate = date('Y-m-d',strtotime($date));
    					}else
    					{
    						$endDate = date('Y-m-d',strtotime($date. "next ".$value['dueWeekDay']));
    					}
    				}else
    				{
    					if ($day == "Friday") 
						{
							$endDate = date('Y-m-d',strtotime($date));
						}else
						{
							$endDate = date('Y-m-d',strtotime($date. "next friday"));
						}
    				}
    				$resultWeek[$key]['dueDate'] = $endDate;	

					if ($currentWeek >=  $compareWeekCheck && $currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentWeek ==  $compareWeekCheck) 
		        		{
		        			$resultWeek[$key]['isEdit'] = 1;
			        		$resultWeek[$key]['message'] = "";
		        		}else
		        		{
		        			$resultWeek[$key]['isEdit'] = 0;
		        			$resultWeek[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultWeek[$key]['isEdit'] = 0;
		        		$resultWeek[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultWeek[$key]['status'] = "uncompleted";
		        	}

					
		        	$resultWeek[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsWeek as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultWeek[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

					if ($resultWeek[$key]['dueDate'] == $date) 
					{
						$resultWeek[$key]['status'] = $value['status'];
						$removeArrayKeyWeek[] = $key;
						$resultDueDate[] = $resultWeek[$key];
					}

					if ($resultWeek[$key]['dueDate'] < $date) 
					{
						$removeArrayKeyWeek[] = $key;
					}

	            }

	             //Get month task as par assign machine and operator
	            $resultMonth = $this->APIM->getTaskMaintenaceListNew($factoryId,$userId,$userIds,$date,$status,$machineId,'everyMonth')->result_array();

	            foreach ($resultMonth as $key => $value) 
	            {
	            	$userIdsMonth = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultMonth[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultMonth[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultMonth[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultMonth[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultMonth[$key]['color'] = "#FFFFFF";	
					}

					if ($value['newTask'] == "1") 
    				{
    					if ($value['monthDueOn'] == "1") 
						{
							if ($value['dueMonthMonth'] != "third_last_day" && $value['dueMonthMonth'] != "second_last_day" && $value['dueMonthMonth'] != "last_day") 
							{
								$endDate = date('Y-m-'.$value['dueMonthMonth'],strtotime($date));
								$endDate = date('Y-m-d',strtotime($endDate));
							}else
							{	
								$endDate = date('Y-m-t',strtotime($date));
								if ($value['dueMonthMonth'] == "third_last_day") 
								{
									$endDate = date('Y-m-d',strtotime("-2 day ".$endDate));
								}elseif ($value['dueMonthMonth'] == "second_last_day") 
								{
									$endDate = date('Y-m-d',strtotime("-1 day ".$endDate));
								}
								
							}
						}elseif ($value['monthDueOn'] == "2") 
						{
							if (!empty($value['dueMonthWeek']) && !empty($value['dueMonthDay'])) 
							{
								$month = date('m',strtotime($date));
								$year = date('Y',strtotime($date));
								$weekEndDate = $this->getFirstandLastDate($year,$month,$value['dueMonthWeek'],$value['dueMonthDay']);
								
								if ($month != date('m',strtotime($weekEndDate))) 
								{
								
									$weekEndDate = $this->getFirstandLastDate($year,$month,'4',$value['dueMonthDay']);
								}

								
								$endDate = $weekEndDate;
							}
						}	
    				}else
    				{
    					$endDate = date('Y-m-t',strtotime($date));
    				}

    				$resultMonth[$key]['dueDate']  = $endDate;

					if ($currentMonth >=  $compareMonthCheck && $currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentMonth ==  $compareMonthCheck) 
		        		{
			        		$resultMonth[$key]['isEdit'] = 1;
			        		$resultMonth[$key]['message'] = "";
		        		}else
		        		{
		        			$resultMonth[$key]['isEdit'] = 0;
		        			$resultMonth[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultMonth[$key]['isEdit'] = 0;
		        		$resultMonth[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultMonth[$key]['status'] = "uncompleted";
		        	}

		        	$resultMonth[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					$i = 0;
					foreach ($userIdsMonth as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultMonth[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

					if ($resultMonth[$key]['dueDate'] == $date) 
					{
						$resultMonth[$key]['status'] = $value['status'];
						$removeArrayKeyMonth[] = $key;
						$resultDueDate[] = $resultMonth[$key];
					}

					if ($resultMonth[$key]['dueDate'] < $date) 
					{
						$removeArrayKeyMonth[] = $key;
					}

	            }

	             //Get year task as par assign machine and operator
	            $resultYear = $this->APIM->getTaskMaintenaceListNew($factoryId,$userId,$userIds,$date,$status,$machineId,'everyYear')->result_array();

	            foreach ($resultYear as $key => $value) 
	            {
	            	$userIdsYear = explode(",", $value['userIds']);

	            	if ($value['repeat'] == "everyDay") 
					{
						$resultYear[$key]['color'] = "#26B4C640";
					}
					elseif ($value['repeat'] == "everyWeek") 
					{
						$resultYear[$key]['color'] = "#59B4C640";	
					}
					elseif ($value['repeat'] == "everyMonth") 
					{
						$resultYear[$key]['color'] = "#8CB4C640";	
					}
					elseif ($value['repeat'] == "everyYear") 
					{
						$resultYear[$key]['color'] = "#BFB4C640";		
					}
					else
					{
						$resultYear[$key]['color'] = "#FFFFFF";	
					}


					if ($value['yearDueOn'] == "1") 
					{
						$monthYear = date('m',strtotime($value['dueYearMonth']));
						if ($value['dueYearMonthDay'] != "third_last_day" && $value['dueYearMonthDay'] != "second_last_day" && $value['dueYearMonthDay'] != "last_day") 
						{
							$endDate = date('Y-'.$monthYear.'-'.$value['dueYearMonthDay'],strtotime($date));
						}else
						{	
							$endDate = date('Y-'.$monthYear.'-t',strtotime($date));
							if ($value['dueYearMonthDay'] == "third_last_day") 
							{
								$endDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$endDate));
							}elseif ($value['dueYearMonthDay'] == "second_last_day") 
							{
								$endDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$endDate));
							}else
							{
								$endDate = date('Y-m-t',strtotime($endDate));
							}

						}
						$endDate = date('Y-m-d',strtotime($endDate));

						$resultYear[$key]['dueDate']  = $endDate;
					}elseif ($value['yearDueOn'] == "2") 
					{
						if(!empty($value['dueYearMonthOnThe']) && !empty($value['dueYearWeek']) && !empty($value['dueYearDay']))
						{
							$year = date('Y',strtotime($date));
							$monthYear = date('m',strtotime($value['dueYearMonthOnThe']));
							$weekEndDate = $this->getFirstandLastDate($year,$monthYear,$value['dueYearWeek'],$value['dueYearDay']);
							if ($monthYear != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate($year,$monthYear,'4',$value['dueYearDay']);
							}

							$endDate = $weekEndDate;
						}

						$resultYear[$key]['dueDate']  = $endDate;

					}


					if ($currentYear >=  $compareYearCheck) 
		        	{	
		        		if ($currentYear ==  $compareYearCheck) 
		        		{
			        		$resultYear[$key]['isEdit'] = 1;
			        		$resultYear[$key]['message'] = "";
		        		}else
		        		{
		        			$resultYear[$key]['isEdit'] = 0;
		        			$resultYear[$key]['message'] = Youcannotupdatepasttask;
		        		}
		        	}else
		        	{	
		        		$resultYear[$key]['isEdit'] = 0;
		        		$resultYear[$key]['message'] = Youcannotupdatefuturetask;
		        		$resultYear[$key]['status'] = "uncompleted";
		        	}
		        	
					$i = 0;
					$resultYear[$key]['isLock'] = ($value['createdByUserId'] == $userId) ? 0 : 1;
					foreach ($userIdsYear as $userKey => $userValue) 
					{
						$users = $this->APIM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
						if (!empty($users)) 
						{
							$resultYear[$key]['operators'][$i] = array("userId" => $users->userId,"userName" => $users->userName);
							$i++;
						}
					}

					if ($resultYear[$key]['dueDate'] == $date) 
					{
						$resultYear[$key]['status'] = $value['status'];
						$removeArrayKeyYear[] = $key;
						$resultDueDate[] = $resultYear[$key];
					}

					if ($resultYear[$key]['dueDate'] < $date) 
					{
						$removeArrayKeyYear[] = $key;
					}
				}

	           
	            $resultDueDate = $this->array_except($resultDueDate, $removeArrayKey);
				$resultDueDate = array_values($resultDueDate);

				$resultWeek = $this->array_except($resultWeek, $removeArrayKeyWeek);
				$resultWeek = array_values($resultWeek);

				$resultMonth = $this->array_except($resultMonth, $removeArrayKeyMonth);
				$resultMonth = array_values($resultMonth);

				$resultYear = $this->array_except($resultYear, $removeArrayKeyYear);
				$resultYear = array_values($resultYear);

				if (!empty($resultDueDate) || !empty($resultWeek) || !empty($resultMonth) || !empty($resultYear)) 
				{
					$isDataAvailable = 1;
				}else
				{
					$isDataAvailable = 0;
				}

				$machines[$machineKey]['data'] = array(
						"isDataAvailable" => $isDataAvailable,
						"todayDueDate" => array_values(array_map("unserialize", array_unique(array_map("serialize", $resultDueDate)))),
						"resultWeek" => $resultWeek,
						"resultMonth" => $resultMonth,
						"resultYear" => $resultYear,
					);
				

				unset($resultDueDate);
				unset($resultWeek);
				unset($resultMonth);
				unset($resultYear);
				unset($removeArrayKey);
            }

            $keysMachine = array_column($machines, 'isViewIn');
			array_multisort($keysMachine, SORT_DESC, $machines);

			$json =  array(
							"status" => "1",
							"message" => "Task data",
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"filterSelectedDate" => date('F jS, Y',strtotime($this->input->post('date'))),
							"todayDueDateTitle" => ($date == date('Y-m-d')) ? Todaystasks :  date('jS',strtotime($date)).' of '.date('F',strtotime($date)).tasks,
							"dayTitle" => Day." - ".date('jS',strtotime($date)).' of '.date('F',strtotime($date)),
							"weekTitle" => WEEK." - ". date('W',strtotime($date)),
							"monthTitle" => MONTH." - ". date('F',strtotime($date)),
							"yearTitle" => YEAR." - ". date('Y',strtotime($date)),
							"upcomingTitle" => Upcomingmonthlyandyearlytasks,
							"data" => $machines
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    public function getLanguage_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('languageType','languageType','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			
			//1 = english,2 = swedish
			$languageType = $this->input->post('languageType');
			
			if (!empty($userId)) 
			{
				$dataLanguage = array("languageType" => $languageType);
				$where = array("userId" => $userId);
				$this->APIM->updateData($where,$dataLanguage,"user");
			}

			//$result = $this->APIM->getWhereDB(array("type" => 'opapp'),"translations");
			$result = $this->db->where('find_in_set("opapp", type) <> 0')->get('translations')->result_array();

			$data = array();
			foreach ($result as $key => $value) 
			{
				$data[$value['textIdentify']] = ($languageType == "1") ? $value['englishText'] : $value['swedishText'];
			}

			$json =  array(
							"status" => "1",
							"message" => "Language change successfully",
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"languageText" => $data
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }


    public function getProductionPartsTime_post()
	{
	    $this->form_validation->set_rules('userId','userId','required');
	    $this->form_validation->set_rules('factoryId','factoryId','required');
	    $this->form_validation->set_rules('machinePartsId','machinePartsId','required');
	    $this->form_validation->set_rules('machineId','machineId','required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error[0],"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machinePartsId = $this->input->post('machinePartsId');
			$machineId = $this->input->post('machineId');
			//$where = array("machinePartsId" => $machinePartsId,"machineId" => $machineId);
			$where = array("machinePartsId" => $machinePartsId,"machineId" => $machineId);
			$result = $this->APIM->getWhereSingle($factoryId, $where,"machineParts");

			//print_r($result);exit;

			$productionTime = $this->APIM->getLastFinishOrderTime($factoryId,$machineId);

			if (!empty($productionTime)) 
			{
				$insertTime = $productionTime->insertTime;
			
				$machineProductionTimeWithFinishOrder = $this->APIM->getLastProductionTimeWithFinishOrder($factoryId,$machinePartsId,$insertTime);
				if (!empty($machineProductionTimeWithFinishOrder)) 
				{
					$insertTime = $machineProductionTimeWithFinishOrder->insertTime;
				}
				$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($factoryId,$machineId,$insertTime);

				if(!empty($machineRunnignWaitingTime->timeDiff))
				{
					$result->totalParts = floor($machineRunnignWaitingTime->timeDiff / $result->patrsCycleTime);
					
					$s = $machineRunnignWaitingTime->timeDiff%60;
					$m = floor(($machineRunnignWaitingTime->timeDiff%3600)/60);
					$h = floor(($machineRunnignWaitingTime->timeDiff%86400)/3600);
					$d = floor(($machineRunnignWaitingTime->timeDiff%2592000)/86400);

					$timeTookText = "";
					$timeTook = "";
					if (!empty($d)) 
					{
						$timeTookText .= $d . "d ";
						$timeTook .= $d . ":";
					}

					if (!empty($h)) 
					{
						$timeTookText .= $h . "h ";
						$timeTook .= $h . ":";
					}

					if (!empty($m)) 
					{
						$timeTookText .= $m . "m ";
						$timeTook .= $m . ":";
					}

					if (!empty($s)) 
					{
						$timeTookText .= $s . "s ";
						$timeTook .= $s;
					}


					$result->timeTookText = $timeTookText;
					$result->timeTook = $timeTook;
				}else
				{
					$result->totalParts = 0;
					$result->timeTookText = "0h 0m";
					$result->timeTook = "";
				}
			}else
			{


				$productionTime = $this->APIM->getLastProductionTime($factoryId,$machinePartsId);

				if (!empty($productionTime)) 
				{
					$insertTime = $productionTime->insertTime;
				
					$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($factoryId,$machineId,$insertTime);

					if(!empty($machineRunnignWaitingTime->timeDiff))
					{
						$result->totalParts = floor($machineRunnignWaitingTime->timeDiff / $result->patrsCycleTime);

						$s = $machineRunnignWaitingTime->timeDiff%60;
						$m = floor(($machineRunnignWaitingTime->timeDiff%3600)/60);
						$h = floor(($machineRunnignWaitingTime->timeDiff%86400)/3600);
						$d = floor(($machineRunnignWaitingTime->timeDiff%2592000)/86400);

						$timeTookText = "";
						$timeTook = "";
						if (!empty($d)) 
						{
							$timeTookText .= $d . "d ";
							$timeTook .= $d . ":";
						}

						if (!empty($h)) 
						{
							$timeTookText .= $h . "h ";
							$timeTook .= $h . ":";
						}

						if (!empty($m)) 
						{
							$timeTookText .= $m . "m ";
							$timeTook .= $m . ":";
						}

						if (!empty($s)) 
						{
							$timeTook .= $s;
						}


						$result->timeTookText = $timeTookText;
						$result->timeTook = $timeTook;
					}else
					{
						$result->totalParts = 0;
						$result->timeTookText = "0h 0m";
						$result->timeTook = "";
					}
				}else
				{
					$result->totalParts = 0;
					$result->timeTookText = "0h 0m";
					$result->timeTook = "";
				}
			}
			
			$result->reportedPartTime = !empty($result->reportedPartTime) ? $result->reportedPartTime : $result->updateTime;
			$data = array(
			      'Username' => 'API_USER',
			      'Password' => '',
			      'ForceRelogin' => true,
			  );
		    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$username='Laufey';
			$password='Cdee3232';

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Common/RejectionCodeItems");
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			
			$chManufacturingOrderOperations = curl_init();
			$q =  curl_escape($chManufacturingOrderOperations ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturingOrderOperations, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrderOperations?\$expand=Part&\$expand=OperationRow&\$filter=Id%20eq%20".$result->OperationRowId."%20AND%20isnull(ActualFinishDate)%20AND%20OperationRowId%20Neq%20''");
			curl_setopt($chManufacturingOrderOperations, CURLOPT_POST, 0);
			curl_setopt($chManufacturingOrderOperations, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingOrderOperationsHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturingOrderOperations, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturingOrderOperations, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturingOrderOperations, CURLOPT_HTTPHEADER, $ManufacturingOrderOperationsHeaders);
			curl_setopt($chManufacturingOrderOperations, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturingOrderOperations, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOrderOperationsOutput = curl_exec ($chManufacturingOrderOperations);
			curl_close ($chManufacturingOrderOperations);

			$ManufacturingOrderOperationsPostData = json_decode($ManufacturingOrderOperationsOutput);

			if (empty($ManufacturingOrderOperationsPostData)) 
			{
				$result->ReportedRestQuantity = "0";
			}

			$json =  array(
							"status" => "1",
							"isUserDeleted" => empty($userDetail) ? 1 : 0,
							"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,
							"data" => $result,
							"rejectionCodeItems" => json_decode($ManufacturingOutput)
					);
		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK);
    }

    function seconds_from_time($time) 
    {
		list($h, $m, $s) = explode(':', $time);
		return ($h * 3600) + ($m * 60) + $s;
	}

	public function AddNewIndirectWorkRecording_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('machinePartsId', 'machinePartsId', 'required');
	    $this->form_validation->set_rules('machineStatus', 'machineStatus', 'required');
	    $this->form_validation->set_rules('OperationRowId', 'OperationRowId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$machinePartsId = $this->input->post('machinePartsId');
			$machineStatus = $this->input->post('machineStatus');
			$OperationRowId = $this->input->post('OperationRowId');

			$getLastProductionTimeEntry = $this->APIM->getLastMachinePartLogTimeEntry($factoryId,$machinePartsId,$machineStatus);

			$machine = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId), "machine");

			$workcenterId = $machine->workcenterId;
			$employeeId = $machine->employeeId;
			$data = array(
			      'Username' => 'API_USER',
			      'Password' => '',
			      'ForceRelogin' => true,
			  );
			    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$dataIndirect = array(
			    "EmployeeId" =>  $employeeId,
			    "SigningEmployeeId" =>  null,
			    "RecordingDate" =>  date('Y-m-d 00:00:00'),
			    "StartTime" => $getLastProductionTimeEntry->insertTime,
			    "EndTime" =>  date('Y-m-d H:i:s'),
			    "IndirectWorkCodeId" =>  736348318200788709,
			    "Comment" =>  null,
			    "OperationId" =>  $OperationRowId,
			    "ReportingWorkCenterId" =>  null
			  );

			    
			$post_dataIndirect = json_encode($dataIndirect);


			$username='Laufey';
			$password='Cdee3232';

			$chIndirect = curl_init();
			curl_setopt($chIndirect, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/TimeRecording/RecordingDays/AddNewIndirectWorkRecording");
			curl_setopt($chIndirect, CURLOPT_POST, 1);
			curl_setopt($chIndirect, CURLOPT_POSTFIELDS,$post_dataIndirect);  //Post Fields
			curl_setopt($chIndirect, CURLOPT_RETURNTRANSFER, true);

			$headersIndirect = [
			    'X-Monitor-SessionId:'.$SessionId,
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($chIndirect, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chIndirect, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chIndirect, CURLOPT_HTTPHEADER, $headersIndirect);

			curl_setopt($chIndirect, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chIndirect, CURLOPT_SSL_VERIFYPEER, false);

			$server_outputIndirect = curl_exec ($chIndirect);
			curl_close ($chIndirect);


			$server_outputIndirect = json_decode($server_outputIndirect);
			$json = array("status" => "1","data" => $server_outputIndirect,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function AddNewOrderBoundRecording_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('machinePartsId', 'machinePartsId', 'required');
	    $this->form_validation->set_rules('machineStatus', 'machineStatus', 'required');
	    $this->form_validation->set_rules('OperationRowId', 'OperationRowId', 'required');
	    $this->form_validation->set_rules('correctParts', 'correctParts', 'required');
	    $this->form_validation->set_rules('scrapParts', 'scrapParts', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$machinePartsId = $this->input->post('machinePartsId');
			$machineStatus = $this->input->post('machineStatus');
			$OperationRowId = $this->input->post('OperationRowId');
			$correctParts = $this->input->post('correctParts');
			$scrapParts = $this->input->post('scrapParts');
			$rejectionCodeId = $this->input->post('rejectionCodeId');
			$reportType = $this->input->post('reportType');

			$partdDetailsP = $this->APIM->getWhereSingle($factoryId,array("machinePartsId" => $machinePartsId),"machineParts");
			$partId = $partdDetailsP->partId;
			$getLastFinishOrderTime = $this->APIM->getLastFinishOrderTime($factoryId,$machineId);
			if (!empty($getLastFinishOrderTime)) 
			{
				$lastTimeCheck = $getLastFinishOrderTime->partStartTime;
			}else
			{
				$lastTimeCheck = $partdDetailsP->updateTime;
			}
			$getLastProductionTimeEntry = $this->APIM->getLastProductionTimeWithFinishOrder($factoryId,$machinePartsId,$lastTimeCheck);
			//$getLastProductionTimeEntry = $this->APIM->getLastProductionTimeEntry($factoryId,$machinePartsId);
			$getLastMachinePartLogTimeEntry = $this->APIM->getLastMachinePartLogTimeEntry($factoryId,$machinePartsId,$machineStatus);
			//print_r($getLastMachinePartLogTimeEntry);exit;
			$getLastNoProductionTimeEntry = $this->APIM->getLastNoProductionTimeEntry($factoryId,$machinePartsId);

			$machine = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId), "machine");

			$workcenterId = $machine->workcenterId;
			$employeeId = $machine->employeeId;
			$data = array(
			      'Username' => 'API_USER',
			      'Password' => '',
			      'ForceRelogin' => true,
			  );
			    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Inventory/PartLocations?\$filter=PartId%20eq%20".$partId);
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			//print_r($ManufacturingOutput);exit;
			$ManufacturingPostData = json_decode($ManufacturingOutput);
			if (!empty($ManufacturingPostData[0]->Id)) 
			{	


				if (!empty($correctParts) || $reportType == "finish") 
				{
					if (!empty($scrapParts)) 
					{
						$dataIndirect = array(
					       "EmployeeId" =>  $employeeId,
						   "SigningEmployeeId" =>  null,
						   "RecordingDate" =>  date('Y-m-d 00:00:00'),
						   "StartTime" => $partdDetailsP->reportedPartTime,
						   "EndTime" =>  $getLastNoProductionTimeEntry->insertTime,
						  "BundleId" => null,
						  "IsSetupTime" => false,
						  "OperationId" => $OperationRowId,
						  "ReportedQuantity" =>  $correctParts,
						  "WipLocation" => null,
						  "ReportingWorkcenterId" => null,
						  "Rejections" =>  array(array( "RejectionCodeId" => $rejectionCodeId,
						 							  "RejectedQuantity" => $scrapParts,
						 							  "Comment" => null)),
						  "Locations" => null,
						  "Comment" => null,
						  "ExistingLocations" =>  array(array( "PartLocationId" => $ManufacturingPostData[0]->Id,
														  "ProductRecordId" => null,
														  "BalanceChange" => sprintf('%0.1f', $correctParts),
														  "SerialNumber" => null,
														  "BatchNumber" => null,
														  "BatchChargeNumber" => null))
					  );

						//print_r($dataIndirect);exit;
					}else
					{
						$dataIndirect = array(
					       "EmployeeId" =>  $employeeId,
						   "SigningEmployeeId" =>  null,
						   "RecordingDate" =>  date('Y-m-d 00:00:00'),
						   "StartTime" => $partdDetailsP->reportedPartTime,
						   "EndTime" =>  $getLastNoProductionTimeEntry->insertTime,
						  "BundleId" => null,
						  "IsSetupTime" => false,
						  "OperationId" => $OperationRowId,
						  "ReportedQuantity" => $correctParts,
						  "WipLocation" => null,
						  "ReportingWorkcenterId" => null,
						  "Rejections" =>  array(),
						  "Locations" => null,
						  "Comment" => null,
						   "ExistingLocations" =>  array(array( "PartLocationId" => $ManufacturingPostData[0]->Id,
									  "ProductRecordId" => null,
									  "BalanceChange" => sprintf('%0.1f', $correctParts),
									  "SerialNumber" => null,
									  "BatchNumber" => null,
									  "BatchChargeNumber" => null))
					  );	


					}
					
				}else
				{	
					if ($machineStatus == "1") 
					{
						
						$dataIndirect = array(
						      "EmployeeId" =>  $employeeId,
							  "SigningEmployeeId" =>  null,
							  "RecordingDate" =>  date('Y-m-d 00:00:00'),
							  "StartTime" => $getLastMachinePartLogTimeEntry->insertTime,
							  "EndTime" =>  date('Y-m-d H:i:s'),
							  "BundleId" => null,
							  "IsSetupTime" => true,
							  "OperationId" => $OperationRowId,
							  "ReportedQuantity" => 0.0,
							  "WipLocation" => null,
							  "ReportingWorkcenterId" => null,
							  "Rejections" =>  null,
							  "Locations" => null,
							  "Comment" => null,
							  "ExistingLocations" =>  null
						  );
					}else
					{
						$dataIndirect = array();
						/*$dataIndirect = array(
					      "EmployeeId" =>  $employeeId,
						  "SigningEmployeeId" =>  null,
						  "RecordingDate" =>  date('Y-m-d 00:00:00'),
						  "StartTime" => $getLastMachinePartLogTimeEntry->insertTime,
						  "EndTime" =>  date('Y-m-d H:i:s'),
						  "BundleId" => null,
						  "IsSetupTime" => false,
						  "OperationId" => $OperationRowId,
						  "ReportedQuantity" => 0,
						  "WipLocation" => null,
						  "ReportingWorkcenterId" => null,
						  "Rejections" =>  null,
						  "Locations" => null,
						  "Comment" => null,
						  "ExistingLocations" => null
					  );*/
					}
				}

				    
				$post_dataIndirect = json_encode($dataIndirect);
				//print_r($post_dataIndirect);exit;


				$username='Laufey';
				$password='Cdee3232';

				$chIndirect = curl_init();
				curl_setopt($chIndirect, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/TimeRecording/RecordingDays/AddNewOrderBoundRecording");
				curl_setopt($chIndirect, CURLOPT_POST, 1);
				curl_setopt($chIndirect, CURLOPT_POSTFIELDS,$post_dataIndirect);  //Post Fields
				curl_setopt($chIndirect, CURLOPT_RETURNTRANSFER, true);

				$headersIndirect = [
				    'X-Monitor-SessionId:'.$SessionId,
				    'Host:srv01.precima.local:8001',
				    'Content-Type: application/json',
				    'Cache-Control: no-cache',
				];
				curl_setopt($chIndirect, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($chIndirect, CURLOPT_USERPWD, "$username:$password");
				curl_setopt($chIndirect, CURLOPT_HTTPHEADER, $headersIndirect);

				curl_setopt($chIndirect, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($chIndirect, CURLOPT_SSL_VERIFYPEER, false);

				$server_outputIndirect = curl_exec ($chIndirect);
				curl_close ($chIndirect);

				$server_outputIndirect = json_decode($server_outputIndirect);
				$json = array("status" => "1","data" => $server_outputIndirect,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}else
			{
				$json = array("status" => "0","message" => "Location id not link","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function getMonitorPartsList($userId,$factoryId,$machineId,$workcenterId)
	{
			$data = array(
		      'Username' => 'API_USER',
		      'Password' => '',
		      'ForceRelogin' => true,
			  );
			    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$username='Laufey';
			$password='Cdee3232';

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrderOperations?\$expand=Part&\$expand=OperationRow&\$filter=WorkCenterId%20eq%20".$workcenterId."%20AND%20isnull(ActualFinishDate)%20AND%20OperationRowId%20Neq%20''");
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			$ManufacturingPostData = json_decode($ManufacturingOutput);
			$operationRowIds = array("0");
			foreach ($ManufacturingPostData as $key => $value) 
    		{
    			$patrsCycleTime = (!empty($value->OperationRow->UnitTime)) ? $value->OperationRow->UnitTime / 10000000: 0;

    			$monitorData = array(
    				"partsNumber" => $value->Part->PartNumber,
    				"partsName" => $value->Part->Description,
    				"partsOperation" => $value->OperationRow->Description,
    				"patrsCycleTime" => $patrsCycleTime,
    				"ReportedRestQuantity" => $value->RestQuantity,
    				"ReportedQuantity" => $value->ReportedQuantity,
    				"ManufacturingOrderId" => $this->getMonitorOrderNumber($SessionId,$value->ManufacturingOrderId),
    				"ReportNumber" => $value->ReportNumber,
    				"operationRowId" => $value->Id,
    				"PartId" => $value->PartId,
    				"userId" => $userId,
    				"machineId" => $machineId,
    				"TimeUnit" => $value->OperationRow->TimeUnit,
    				"PlannedStartDate" => $value->PlannedStartDate,
    				"PlannedFinishDate" => $value->PlannedFinishDate,
    			);
    			/*if ($value->PartId == "674769874593081697") {
    				// code...
    			     print_r($monitorData);exit;
    			}*/
    			$operationRowIds[] = $value->Id;
    			$checkPart = $this->APIM->getWhereSingle($factoryId, array("operationRowId" => $value->Id), "machineParts");

    			if (!empty($checkPart)) 
    			{
    				$this->APIM->updateFactoryData($factoryId,array("operationRowId" => $value->Id),$monitorData,"machineParts");
    			}else
    			{
    				$this->APIM->insertFactoryData($factoryId,$monitorData,"machineParts");

    			}
    		}

    		$result = $this->APIM->getWhereInWithOrderBy($factoryId,"*", array("machineId" => $machineId), array("operationRowId" => $operationRowIds),"PlannedStartDate","asc", "machineParts");
			return $result;
    }

    public function getMonitorOrderNumber($SessionId,$ManufacturingOrderId)
	{
			$username='Laufey';
			$password='Cdee3232';

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrders?\$filter=Id%20eq%20".$ManufacturingOrderId);
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			$ManufacturingPostData = json_decode($ManufacturingOutput);
			
			return $ManufacturingPostData[0]->OrderNumber;
    }

    public function reportManufacturingOrderOperation_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('machinePartsId', 'machinePartsId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('ReportedQuantity', 'ReportedQuantity', 'required');
	    $this->form_validation->set_rules('ReportedRestQuantity', 'ReportedRestQuantity', 'required');
	    $this->form_validation->set_rules('ReportNumber', 'ReportNumber', 'required');
	    $this->form_validation->set_rules('TimeUnit', 'TimeUnit', 'required');
	    $this->form_validation->set_rules('remainingQuantity', 'remainingQuantity', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			//print_r($this->form_validation->error_array());exit;
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$machinePartsId = $this->input->post('machinePartsId');
			$ReportNumber = $this->input->post('ReportNumber');
			$ReportedQuantity = $this->input->post('ReportedQuantity');
			$ReportedRestQuantity = $this->input->post('ReportedRestQuantity');
			$TimeUnit = $this->input->post('TimeUnit');
			$remainingQuantity = $this->input->post('remainingQuantity');
			$correctParts = $this->input->post('correctParts');
			$scrapParts = $this->input->post('scrapParts');
			$rejectionCodeId = $this->input->post('rejectionCodeId');

			$reportData = array("ReportedRestQuantity" => $remainingQuantity);
			$this->APIM->updateFactoryData($factoryId,array("machinePartsId" => $machinePartsId),$reportData,"machineParts");

			$data = array(
		      'Username' => 'API_USER',
		      'Password' => '',
		      'ForceRelogin' => true,
			  );
			    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$username='Laufey';
			$password='Cdee3232';

			
				$dataIndirect = array(
				      "ReportNumber" =>  $ReportNumber,
					  "ReportedQuantity" =>  0.0,
					  "ReportedRestQuantity" =>  $remainingQuantity,
					  "TimeUnit" => $TimeUnit,
					  "ReportedTime" => 0.0,
					  "ReportedSetupTime" => 0.0
				  );
			
			//print_r($dataIndirect);exit;
			$post_dataIndirect = json_encode($dataIndirect);

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/Reporting/ReportManufacturingOrderOperation");
			curl_setopt($chManufacturing, CURLOPT_POST, 1);
			curl_setopt($chManufacturing, CURLOPT_POSTFIELDS,$post_dataIndirect);  //Post Fields
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			$ManufacturingPostData = json_decode($ManufacturingOutput);

			$json = array("status" => "1","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0,"data" => $ManufacturingPostData);

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function reportParts_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('machinePartsId', 'machinePartsId', 'required');
	    $this->form_validation->set_rules('machineStatus', 'machineStatus', 'required');
	    $this->form_validation->set_rules('OperationRowId', 'OperationRowId', 'required');
	    $this->form_validation->set_rules('correctParts', 'correctParts', 'required');
	    $this->form_validation->set_rules('scrapParts', 'scrapParts', 'required');
	    $this->form_validation->set_rules('partId', 'partId', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$machinePartsId = $this->input->post('machinePartsId');
			$machineStatus = $this->input->post('machineStatus');
			$OperationRowId = $this->input->post('OperationRowId');
			$correctParts = $this->input->post('correctParts');
			$scrapParts = $this->input->post('scrapParts');
			$rejectionCodeId = $this->input->post('rejectionCodeId');
			$partId = $this->input->post('partId');


			$getLastProductionTimeEntry = $this->APIM->getWhereSingle($factoryId,array('machinePartsId' => $machinePartsId),"machineParts");

			$remainingQuantityLeft = $getLastProductionTimeEntry->ReportedRestQuantity - ($correctParts + $scrapParts);
			$reportData = array("reportedPartTime" => date('Y-m-d H:i:s'),"ReportedRestQuantity" => $remainingQuantityLeft);
			$this->APIM->updateFactoryData($factoryId,array("machinePartsId" => $machinePartsId),$reportData,"machineParts");

			$machine = $this->APIM->getWhereSingle($factoryId, array("machineId" => $machineId), "machine");

			$workcenterId = $machine->workcenterId;
			$employeeId = $machine->employeeId;
			$data = array(
			      'Username' => 'API_USER',
			      'Password' => '',
			      'ForceRelogin' => true,
			  );
			    
			$post_data = json_encode($data);


			$username='Laufey';
			$password='Cdee3232';

			$ch = curl_init();
			curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
			curl_setopt($ch, CURLOPT_POST, 1);
			curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
			curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

			$headers = [
			    'Accept: application/json',
			    'Host:srv01.precima.local:8001',
			    'Content-Type: application/json',
			    'Cache-Control: no-cache',
			];
			curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

			curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

			$server_output = curl_exec ($ch);

			curl_close ($ch);

			$server_output = json_decode($server_output);

			$SessionId =  $server_output->SessionId;

			$username='Laufey';
			$password='Cdee3232';

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Inventory/PartLocations?\$filter=PartId%20eq%20".$partId);
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			//print_r($ManufacturingOutput);exit;
			$ManufacturingPostData = json_decode($ManufacturingOutput);
			if (!empty($ManufacturingPostData[0]->Id)) 
			{
				
				$dataFinishOrder = array("machineId" => $machineId,"machinePartsId" => $machinePartsId,"timeTook" => "","correctParts" => $correctParts,"scrapParts" => $scrapParts,"scrapReasone" => "","insertTime" => date('Y-m-d H:i:s'),"userId" => $userId,"partStartTime" => $getLastProductionTimeEntry->reportedPartTime,"orderReportType" => "report"); 
				$this->APIM->insertFactoryData($factoryId,$dataFinishOrder,'machinePartsFinish');
				if (!empty($correctParts)) 
				{
					if (!empty($scrapParts)) 
					{
						$dataIndirect = array(
					      "EmployeeId" =>  $employeeId,
						  "SigningEmployeeId" =>  null,
						  "RecordingDate" =>  date('Y-m-d 00:00:00'),
						  "StartTime" =>  !empty($getLastProductionTimeEntry->reportedPartTime) ? $getLastProductionTimeEntry->reportedPartTime : $getLastProductionTimeEntry->updateTime,
						  "EndTime" =>  date('Y-m-d H:i:s'),
						  "BundleId" => null,
						  "IsSetupTime" => false,
						  "OperationId" => $OperationRowId,
						  "ReportedQuantity" => sprintf('%0.1f', $correctParts),
						  "WipLocation" => null,
						  "ReportingWorkcenterId" => null,
						  "Rejections" =>  array(array( "RejectionCodeId" => $rejectionCodeId,
						 							  "RejectedQuantity" => sprintf('%0.1f', $scrapParts),
						 							  "Comment" => null)),
						  "Locations" => null,
						  "Comment" => null,
						  "ExistingLocations" => array(array( "PartLocationId" => $ManufacturingPostData[0]->Id,
															  "ProductRecordId" => null,
															  "BalanceChange" => sprintf('%0.1f', $correctParts),
															  "SerialNumber" => null,
															  "BatchNumber" => null,
															  "BatchChargeNumber" => null))
					  );
					}else
					{
						$dataIndirect = array(
					      "EmployeeId" =>  $employeeId,
						  "SigningEmployeeId" =>  null,
						  "RecordingDate" =>  date('Y-m-d 00:00:00'),
						  "StartTime" =>  !empty($getLastProductionTimeEntry->reportedPartTime) ? $getLastProductionTimeEntry->reportedPartTime : $getLastProductionTimeEntry->updateTime,
						  "EndTime" =>  date('Y-m-d H:i:s'),
						  "BundleId" => null,
						  "IsSetupTime" => false,
						  "OperationId" => $OperationRowId,
						  "ReportedQuantity" => sprintf('%0.1f', $correctParts),
						  "WipLocation" => null,
						  "ReportingWorkcenterId" => null,
						  "Rejections" =>  array(),
						  "Locations" => null,
						  "Comment" => null,
						  "ExistingLocations" => array(array( "PartLocationId" => $ManufacturingPostData[0]->Id,
															  "ProductRecordId" => null,
															  "BalanceChange" => sprintf('%0.1f', $correctParts),
															  "SerialNumber" => null,
															  "BatchNumber" => null,
															  "BatchChargeNumber" => null))
					  );	
					}
					
				}
				

				$post_dataIndirect = json_encode($dataIndirect);
				  //print_r($post_dataIndirect);exit;


				$username='Laufey';
				$password='Cdee3232';

				$chIndirect = curl_init();
				curl_setopt($chIndirect, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/TimeRecording/RecordingDays/AddNewOrderBoundRecording");
				curl_setopt($chIndirect, CURLOPT_POST, 1);
				curl_setopt($chIndirect, CURLOPT_POSTFIELDS,$post_dataIndirect);  //Post Fields
				curl_setopt($chIndirect, CURLOPT_RETURNTRANSFER, true);

				$headersIndirect = [
				    'X-Monitor-SessionId:'.$SessionId,
				    'Host:srv01.precima.local:8001',
				    'Content-Type: application/json',
				    'Cache-Control: no-cache',
				];
				curl_setopt($chIndirect, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
				curl_setopt($chIndirect, CURLOPT_USERPWD, "$username:$password");
				curl_setopt($chIndirect, CURLOPT_HTTPHEADER, $headersIndirect);

				curl_setopt($chIndirect, CURLOPT_SSL_VERIFYHOST, false);
				curl_setopt($chIndirect, CURLOPT_SSL_VERIFYPEER, false);

				$server_outputIndirect = curl_exec ($chIndirect);
				curl_close ($chIndirect);

				$server_outputIndirect = json_decode($server_outputIndirect);



				$json = array("status" => "1","data" => $server_outputIndirect,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}else
			{
				$json = array("status" => "0","message" => "Location id not link","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			}

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }

    public function reportPartsWithoutMonitor_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('machinePartsId', 'machinePartsId', 'required');
	    $this->form_validation->set_rules('correctParts', 'correctParts', 'required');
	    $this->form_validation->set_rules('scrapParts', 'scrapParts', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$machinePartsId = $this->input->post('machinePartsId');
			$machineStatus = $this->input->post('machineStatus');
			$correctParts = $this->input->post('correctParts');
			$scrapParts = $this->input->post('scrapParts');

			$getLastProductionTimeEntry = $this->APIM->getWhereSingle($factoryId,array('machinePartsId' => $machinePartsId),"machineParts");

			$reportData = array("reportedPartTime" => date('Y-m-d H:i:s'));
			$this->APIM->updateFactoryData($factoryId,array("machinePartsId" => $machinePartsId),$reportData,"machineParts");
				
			$dataFinishOrder = array(
					"machineId" => $machineId,
					"machinePartsId" => $machinePartsId,
					"timeTook" => "",
					"correctParts" => $correctParts,
					"scrapParts" => $scrapParts,
					"scrapReasone" => "",
					"insertTime" => date('Y-m-d H:i:s'),
					"userId" => $userId,
					"partStartTime" => $getLastProductionTimeEntry->reportedPartTime,
					"orderReportType" => "report"
				); 

			$this->APIM->insertFactoryData($factoryId,$dataFinishOrder,'machinePartsFinish');
		


			$json = array("status" => "1","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
	}

	public function workingMachineListWithoutMonitor_post() 
	{ 
        $postArray = $this->post();

        

		//check required parameter validation
		if(isset($postArray['userId']) && isset($postArray['factoryId'])) 
		{ 

       		$userId = $_POST['userId'];
			//check user is exists or not 
			$userDetail = $this->APIM->userExists($userId); 
			//$accessPointCheckInExecute = $this->APIM->accessPointEdit('22',$userDetail['userRole']);
			//$accessPointViewInExecute = $this->APIM->accessPoint('22',$userDetail['userRole']);

			//check factory is exists or not
			$checkFactoryDetail = $this->APIM->checkFactoryDetail($_POST['factoryId']);

			$resultCheckIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId);

			if (!empty($resultCheckIn)) 
			{
				$checkInStartTime = $resultCheckIn['startTime'];
			}else
			{
				$checkInStartTime = "";
			}

			$factoryDetail = $this->APIM->factoryExists($postArray['factoryId']); 
			if(is_array($factoryDetail)) {
				if(isset($postArray['oppApp']) && $postArray['oppApp'] == '1' ) { //oppapp response 
					$checkInStatus = '0';
					if(isset($_POST['currentTime'])) {
						$currentTime = date('Y-m-d', $_POST['currentTime']);
					} else {
						$currentTime = date('Y-m-d'); 
					} 
					
					
					$machines = array();
					if(intVal($userDetail['userRole']) == 1) { 
					//If role greater than operator
						$isOperator = 0;
					} else { //
						$isOperator = 1;
					}

					$viewOnly = !empty($this->input->post('viewOnly')) ? $this->input->post('viewOnly') : "0";
					//get operator assigned machine 
					$machines = $this->APIM->getWorkingMachine($postArray['factoryId'], $isOperator, $postArray['userId'],$viewOnly)->result();
					
					
					if( count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {



							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['isDataGet'] = $machines[$i]->isDataGet;
							$list[$i]['isSetAppConnect'] = 1;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;
							$list[$i]['workcenterId'] = $machines[$i]->workcenterId;
							/*$list[$i]['noProductionTime'] = !empty($machines[$i]->noProduction) ? $machines[$i]->noProduction : "";
							$list[$i]['noProductionTimeIsActive'] = !empty($machines[$i]->noProductionTimeIsActive) ? $machines[$i]->noProductionTimeIsActive : "0";*/
							$list[$i]['noStacklight'] = $machines[$i]->noStacklight;
							//$list[$i]['setAppAlert'] = "";

							$warningFlagResponse = $this->APIM->getWarningFlagNotificationsStatus($postArray['factoryId'],$machines[$i]->machineId);

							$list[$i]['warningFlag'] = ($warningFlagResponse > 0) ? 1 : 0;


							$lastCkecingData = $this->APIM->getLastCheckInDetails($postArray['factoryId'],$postArray['userId']);
							if ($lastCkecingData['isViewIn'] == "0") 
							{
								$list[$i]['isViewIn'] = "1";
							}else
							{
								if (!empty($lastCkecingData['workingMachine'])) 
								{
									$workingMachineArrData = explode(",", $lastCkecingData['workingMachine']);
									if (in_array($machines[$i]->machineId, $workingMachineArrData)) 
									{
										$list[$i]['isViewIn'] = "1";
									}else
									{
										$list[$i]['isViewIn'] = "0";
									}
								}else
								{
									$list[$i]['isViewIn'] = "0";
								}
							}

							$userSoundConfig = $this->APIM->getWhereSingle($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"userId" => $userId), "userSoundConfig");
							
							if (!empty($userSoundConfig)) 
							{
								$list[$i]['sound'] = $userSoundConfig->sound;
							}else
							{
								$list[$i]['sound'] = "1";
							}

							$machineNoProductionDateTime = $this->APIM->getWhereDBSingle(array("machineId" => $machines[$i]->machineId,"factoryId" => $postArray['factoryId']), "machineNoProductionDateTime");

							$list[$i]['noProductionTime'] = !empty($machineNoProductionDateTime) ? $machineNoProductionDateTime->noProduction : "";

							if (!empty($machineNoProductionDateTime) && $machineNoProductionDateTime->isActive == "1") {
								$noProductionTimeIsActive = "1";
							}else
							{
								$noProductionTimeIsActive = "0";
							}
							$list[$i]['noProductionTimeIsActive'] = $noProductionTimeIsActive;

							if (!empty($machines[$i]->workcenterId)) 
							{
								//$machineParts = $this->getMonitorPartsList($userId,$postArray['factoryId'],$machines[$i]->machineId,$machines[$i]->workcenterId);
							}else{
								//$machineParts = $this->APIM->getWhere($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"isDelete" => "0"), "machineParts");
							}
							$machineParts = array();
							$list[$i]['machineParts'] = $machineParts;
							

							$currentOrderMachineParts = $this->APIM->getWhereWithOrderBy($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"currentOrderStatus" => "1","isDelete" => "0"), "machineParts","updateTime","asc");

							foreach ($currentOrderMachineParts as $currentKey => $currentValue) 
							{
								$productionTime = $this->APIM->getLastFinishOrderTime($postArray['factoryId'],$currentValue['machineId']);

								if (!empty($productionTime)) 
								{
									$insertTime = $productionTime->insertTime;
								

									$machineProductionTimeWithFinishOrder = $this->APIM->getLastProductionTimeWithFinishOrder($postArray['factoryId'],$currentValue['machinePartsId'],$insertTime);
									if (!empty($machineProductionTimeWithFinishOrder)) 
									{
										$insertTime = $machineProductionTimeWithFinishOrder->insertTime;
									}
									$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($postArray['factoryId'],$machines[$i]->machineId,$insertTime);

									if(!empty($machineRunnignWaitingTime->timeDiff))
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = floor($machineRunnignWaitingTime->timeDiff / $currentValue['patrsCycleTime']);
										
									}else
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
									}
								}else
								{
									$productionTime = $this->APIM->getLastProductionTime($postArray['factoryId'],$currentValue['machinePartsId']);
									//print_r($productionTime);exit;
									if (!empty($productionTime)) 
									{	
										$insertTime = $productionTime->insertTime;

										$machineRunnignWaitingTime = $this->APIM->getMachineRunnignWaitingTime($postArray['factoryId'],$machines[$i]->machineId,$insertTime);

										if(!empty($machineRunnignWaitingTime->timeDiff))
										{
											$currentOrderMachineParts[$currentKey]['totalParts'] = floor($machineRunnignWaitingTime->timeDiff / $currentValue['patrsCycleTime']);
										}else
										{
											$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
										}
									}else
									{
										$currentOrderMachineParts[$currentKey]['totalParts'] = 0;
									}
								}
								
							}
							$list[$i]['currentOrderMachineParts'] = $currentOrderMachineParts;
							
							//cross verify check battery details added or not
							if ($machines[$i]->chargeFlag != NULL) 
							{
								//setting the battery percentage and  charger is connected or not
								$list[$i]['phoneId'] = $machines[$i]->phoneId;
								$list[$i]['batteryLevel'] = $machines[$i]->batteryLevel;
								$list[$i]['chargeFlag'] = $machines[$i]->chargeFlag;
							} else 
							{ 
								//set default battery data 
								$list[$i]['phoneId'] = 0;
								$list[$i]['batteryLevel'] = 0;
								$list[$i]['chargeFlag'] = "0";
							}
							
							//get setApp machine current status
							$isSetAppOn = $this->APIM->checkSetApp($postArray['factoryId'], intval($machines[$i]->machineId)); 
							if(isset($isSetAppOn) && is_array($isSetAppOn)){
								$list[$i]['isSetAppOn'] = $isSetAppOn['isActive'];
								if($isSetAppOn['reason'] == 'client namespace disconnect' || $isSetAppOn['reason'] == 'transport close' || $machines[$i]->machineSelected == "0") {
									$list[$i]['setAppAlert'] = SETAPP_TURNED_OFF.'.';
								}
								else if($isSetAppOn['reason'] == 'detection disconnect') { 
									$list[$i]['setAppAlert'] = SetAppisonhomescreen.". ".Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'ping timeout') { 
									$list[$i]['setAppAlert'] = SETAPP_NO_INTERNET;
								} 
								else if($isSetAppOn['reason'] == 'app close force fully') 
								{
									$list[$i]['setAppAlert'] = SetAppclosedforcefully.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'app stop due to error') 
								{
									$list[$i]['setAppAlert'] = SetAppcrashedandstoppedduetoexception.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
								else if($isSetAppOn['reason'] == 'setApp restart') 
								{
									$list[$i]['setAppAlert'] = SetApprestartedduetosomeexception.'. '.Checkyourphoneorcontactinfonytttechcom;
								}  
							} else {
								$list[$i]['isSetAppOn'] = 0;
								$list[$i]['isSetAppConnect'] = 0;
								$list[$i]['setAppAlert'] = Nosetappconnectedto.' '.$machines[$i]->machineName; 
							}
							
							//get operator check in data
							$checkIn = $this->APIM->isUserCheckedIn($postArray['factoryId'], $userId); 
							  
							$checkInStatus = '1'; 
							$activeId = isset($checkIn['activeId'])?$checkIn['activeId']:0;
							
							$machineLightArr[$i] = explode(",", $machines[$i]->machineLight); 
							$listColors = $this->APIM->getAllColors();
							for($z=0;$z<count($machineLightArr[$i]);$z++) {
								for($y=0;$y<count($listColors);$y++) { 
									if($machineLightArr[$i][$z] == $listColors[$y]->colorId) $list[$i]['machineLightColors'][$z] = $listColors[$y]->colorCode;
								}
												
								$machineLightStatus['redStatus'] = '0';
								$machineLightStatus['yellowStatus'] = '0';
								$machineLightStatus['greenStatus'] = '0';
								$machineLightStatus['whiteStatus'] = '0';
								$machineLightStatus['blueStatus'] = '0';
								
								$color = $machines[$i]->color; 
								
								$colorArr = explode(" ", $color);
								for($c=0;$c<count($colorArr);$c++) {
									if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') { 
										$machineLightStatus[strtolower($colorArr[$c]).'Status'] = '1';
									}
								}
								
								if(strpos($color, 'Blue') !== false) {
									$machineLightStatus['blueStatus'] = '1';
								}
								if(strpos($color, 'Red') !== false) {
									$machineLightStatus['redStatus'] = '1';
								}
								if(strpos($color, 'Red and green') !== false) {
									$machineLightStatus['redStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
								}
								if(strpos($color, 'Green') !== false) {
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Yellow and green') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Green and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Red and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['redStatus'] = '1';
								}
								if(strpos($color, 'Robot') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
								}
								if(strpos($color, 'Door') !== false) {
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Red Yellow Green') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['redStatus'] = '1';
									$machineLightStatus['greenStatus'] = '1';
								}
								if(strpos($color, 'Blue and yellow') !== false) {
									$machineLightStatus['yellowStatus'] = '1';
									$machineLightStatus['blueStatus'] = '1';
								}
									
								if($list[$i]['machineLightColors'][$z] == 'FF0000') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['redStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'redStatus';
								}
								if($list[$i]['machineLightColors'][$z] == 'FFFF00') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['yellowStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'yellowStatus';
								}
								if($list[$i]['machineLightColors'][$z] == '00FF00') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['greenStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'greenStatus';
								}
								if($list[$i]['machineLightColors'][$z] == 'FFFFFF') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['whiteStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'whiteStatus';
								} 
								if($list[$i]['machineLightColors'][$z] == '0000FF') {
									$list[$i]['machineLightColorsStatus'][$z] = $machineLightStatus['blueStatus'];
									$list[$i]['machineLightColorsStatusNames'][$z] = 'blueStatus';
								} 
								
								$list[$i]['machineLightStatus']['redStatus'] = $machineLightStatus['redStatus'];
								$list[$i]['machineLightStatus']['greenStatus'] = $machineLightStatus['greenStatus'];
								$list[$i]['machineLightStatus']['yellowStatus'] = $machineLightStatus['yellowStatus'];
								$list[$i]['machineLightStatus']['blueStatus'] = $machineLightStatus['blueStatus']; 
								$list[$i]['machineLightStatus']['whiteStatus'] = $machineLightStatus['whiteStatus']; 
								
								$list[$i]['machineLightStatus']['statusText'] = $this->APIM->getMachineStatusText($postArray['factoryId'], $machines[$i]->machineId, $list[$i]['machineLightStatus']['redStatus'], $list[$i]['machineLightStatus']['greenStatus'], $list[$i]['machineLightStatus']['yellowStatus'], $list[$i]['machineLightStatus']['whiteStatus'],$list[$i]['machineLightStatus']['blueStatus']); 
								
								if($machines[$i]->logId != 0 && $machines[$i]->machineSelected == "1") {
									if($color == 'NoData' || $color == 'NoDataStacklight' || $color == 'NoDataError' || $color == 'NoDataHome' || $color == 'NoDataForceFully' || $color == 'NoDataRestart') { 
										$list[$i]['isSetAppOn'] = 0;
										if(!isset($list[$i]['setAppAlert'])) {
											$list[$i]['setAppAlert'] = NO_DETECTION; 
										}
									} else 
									{
										if ($list[$i]['setAppAlert'] == SETAPP_NO_INTERNET)
										{
											$list[$i]['isSetAppOn'] = 0;
											$list[$i]['setAppAlert'] = SETAPP_NO_INTERNET;
										} else
										{

											$list[$i]['isSetAppOn'] = 1;
											$list[$i]['isSetAppConnect'] = 1;
											$list[$i]['setAppAlert'] = '';
										}
									}  
								} 
							}
							$list[$i]['mtConnectTotalParts'] =  0 ;
							if ($machines[$i]->isDataGet == "2") 
							{
								$mtConnectOverviewResult  = $this->APIM->getWhereSingle($postArray['factoryId'],array("machineId" => $machines[$i]->machineId),"mtConnectOverview");

								$list[$i]['mtConnectTotalParts'] =  (!empty($mtConnectOverviewResult) ? $mtConnectOverviewResult->Partcount : 0) ;
								if ($mtConnectOverviewResult->State == "ACTIVE") 
								{
									$list[$i]['machineLightStatus']['greenStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","0","1");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Running';
									$list[$i]['phoneId'] = '1';
								}else if ($mtConnectOverviewResult->State == "INTERRUPTED"  || $mtConnectOverviewResult->State == "READY") 
								{
									$list[$i]['machineLightStatus']['yellowStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","1","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Waiting';
									$list[$i]['phoneId'] = '1';
								}
								else if ($mtConnectOverviewResult->State == "STOPPED") 
								{
									$list[$i]['machineLightStatus']['redStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("1","0","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = MTconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Stopped';
									$list[$i]['phoneId'] = '1';
								}else 
								{
									$list[$i]['isSetAppOn'] = (!empty($mtConnectOverviewResult->State)) ? 1 : 0;
									$list[$i]['machineLightColorsStatus'] = array("0","0","0");
									$list[$i]['machineSelected'] = "1";
									$list[$i]['setAppAlert'] = (!empty($mtConnectOverviewResult->State)) ? "nodet" : NoMTconnectconnectedto.' '.$machines[$i]->machineName;
									$list[$i]['isSetAppConnect'] = (!empty($mtConnectOverviewResult->State)) ? 1 : 0;
									$list[$i]['machineLightStatus']['statusText'] = 'nodet';
									$list[$i]['phoneId'] = '1';
								}


							}

							$list[$i]['virtualMachineTotalParts'] = 0 ;
							if ($machines[$i]->isDataGet == "4") 
							{
								$virtualMachineLogOverviewResult  = $this->APIM->getWhereSingle($postArray['factoryId'],array("machineId" => $machines[$i]->machineId),"virtualMachineLogOverview");

								$list[$i]['virtualMachineTotalParts'] = 0 ;
								if ($virtualMachineLogOverviewResult->signalType == "1") 
								{
									$list[$i]['machineLightStatus']['greenStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("0","1");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = IOconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Running';
									$list[$i]['phoneId'] = '1';
								}
								else if ($virtualMachineLogOverviewResult->signalType == "0") 
								{
									$list[$i]['machineLightStatus']['redStatus'] =  "1";
									$list[$i]['machineLightColorsStatus'] = array("1","0");
									$list[$i]['isSetAppOn'] = 1;
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = 1;
									$list[$i]['setAppAlert'] = IOconnectlink;
									$list[$i]['machineLightStatus']['statusText'] = 'Stopped';
									$list[$i]['phoneId'] = '1';
								}else 
								{
									$list[$i]['isSetAppOn'] = ($virtualMachineLogOverviewResult->signalType != "") ? 1 : 0;
									$list[$i]['machineLightColorsStatus'] = array("0","0");
									$list[$i]['machineSelected'] = "1";
									$list[$i]['isSetAppConnect'] = ($virtualMachineLogOverviewResult->signalType != "") ? 1 : 0;
									$list[$i]['setAppAlert'] = ($virtualMachineLogOverviewResult->signalType != "") ? "nodet" : NoIOconnectedto.' '.$machines[$i]->machineName;
									$list[$i]['machineLightStatus']['statusText'] = 'nodet';
									$list[$i]['phoneId'] = '1';
								}


							}
							$message = ""; 
							if($userDetail['userRole'] == 1 ) 
							{
								$message = Factorymanagercannotmakeanyinput.'.'; 
							} 
							else if(is_array($checkIn))
							{
								$message = Unabletoupdateasyouarenotworkingonthismachine.'.'; 	
							}

							else {
								$activeId = "0";
								$checkInStatus = "0";
								$message = ViewonlyselectedPleasecheckintomakeanyinputs.'.';
							} 
						} 

						$keysMachine = array_column($list, 'isViewIn');
						array_multisort($keysMachine, SORT_DESC, $list);
						$message = ['status'=>'1',"accessPointCheckInExecute" => true,"accessPointViewInExecute" => true,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'activeId'=>$activeId,'message'=> $message,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'list'=>$list]; 
					} else {
						$message = ['status'=>'0',"accessPointCheckInExecute" => true,"accessPointViewInExecute" => true,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'checkInStatus'=>$checkInStatus,"checkInStartTime" => $checkInStartTime,'message'=> "No machines assigned, please assign machines to the operator in the dashboard","isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} 
					
				} else { 
				//setapp response 
				//get all machine for setApp 
					$machines = $this->APIM->getAssignedMachines($postArray['factoryId'], $postArray['userId']); 
					if(is_array($machines) && count($machines) > 0 ) {
						
						for($i=0;$i<count($machines);$i++) {
							$list[$i]['machineId'] = intval($machines[$i]->machineId);
							$list[$i]['machineName'] = $machines[$i]->machineName;
							$list[$i]['machineSelected'] = $machines[$i]->machineSelected;
							$list[$i]['machineStatus'] = $machines[$i]->machineStatus;
							$list[$i]['receiveErrorNotification'] = $machines[$i]->notifyStop;
							$list[$i]['provideReasonForErrors'] = $machines[$i]->prolongedStop;
							$list[$i]['receiveWaitingNotification'] = $machines[$i]->notifyWait;
							$list[$i]['provideReasonForWaiting'] = $machines[$i]->prolongedWait;

							$userSoundConfig = $this->APIM->getWhereSingle($postArray['factoryId'], array("machineId" => $machines[$i]->machineId,"userId" => $userId), "userSoundConfig");

							if (!empty($userSoundConfig)) 
							{
								$list[$i]['sound'] = $userSoundConfig->sound;
							}else
							{
								$list[$i]['sound'] = "1";
							}

						}
						$message = ['status'=>'1',"checkInStartTime" => $checkInStartTime,'isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'list'=>$list,'message'=>MACHINE_LIST_SUCCESS,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0];  
					} else {
						$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>NO_ASSIGN,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0]; 
					}
				}
			} else {
				$message = ['status'=>'0','isUserDeleted' => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => 0,'message'=>Invaliddetails];
			}
		} else {
			$message = ['status'=>'0','message'=>Invaliddetails];
		}
        $this->set_response($message, REST_Controller::HTTP_OK); 
		
    }


    public function getMachinePartList_post()
	{
	    $this->form_validation->set_rules('userId', 'userId', 'required');
	    $this->form_validation->set_rules('factoryId', 'factoryId', 'required');
	    $this->form_validation->set_rules('machineId', 'machineId', 'required');
	    $this->form_validation->set_rules('isMonitor', 'isMonitor', 'required');

	    $userId = $this->input->post('userId');
		$factoryId = $this->input->post('factoryId');

		//check user is exists or not 
	    $userDetail = $this->APIM->userExists($userId); 
	    //check factory is exists or not
	    $checkFactoryDetail = $this->APIM->checkFactoryDetail($factoryId);

	    //check required parameter validation
		if ($this->form_validation->run() == FALSE) 
		{
			$error = implode(",", $this->form_validation->error_array());
			$error = explode(",", $error);
			$json = array("status" => "0","message" => $error,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
		}
		else
		{  
			$machineId = $this->input->post('machineId');
			$workcenterId = $this->input->post('workcenterId');
			$isMonitor = $this->input->post('isMonitor');


			if ($isMonitor == "1") 
			{
				if (!empty($workcenterId)) 
				{
					
				
					$data = array(
				      'Username' => 'API_USER',
				      'Password' => '',
				      'ForceRelogin' => true,
					  );
					    
					$post_data = json_encode($data);


					$username='Laufey';
					$password='Cdee3232';

					$ch = curl_init();
					curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
					curl_setopt($ch, CURLOPT_POST, 1);
					curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
					curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

					$headers = [
					    'Accept: application/json',
					    'Host:srv01.precima.local:8001',
					    'Content-Type: application/json',
					    'Cache-Control: no-cache',
					];
					curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
					curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
					curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

					curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

					$server_output = curl_exec ($ch);

					curl_close ($ch);

					$server_output = json_decode($server_output);

					$SessionId =  $server_output->SessionId;

					$username='Laufey';
					$password='Cdee3232';

					$chManufacturing = curl_init();
					$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
					curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrderOperations?\$expand=Part&\$expand=OperationRow&\$filter=WorkCenterId%20eq%20".$workcenterId."%20AND%20isnull(ActualFinishDate)%20AND%20OperationRowId%20Neq%20''");
					curl_setopt($chManufacturing, CURLOPT_POST, 0);
					curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

					$ManufacturingHeaders = [
					    'Accept: application/json',
					    'Cache-Control:no-cache',
					    'Host: 212.16.184.90:8001',
					    'X-Monitor-SessionId: '.$SessionId,
					    'Content-Type: application/json'
					];

					curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
					curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
					curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
					curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
					curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

					$ManufacturingOutput = curl_exec ($chManufacturing);
					curl_close ($chManufacturing);

					$ManufacturingPostData = json_decode($ManufacturingOutput);
					$operationRowIds = array("0");
					foreach ($ManufacturingPostData as $key => $value) 
		    		{
		    			$patrsCycleTime = (!empty($value->OperationRow->UnitTime)) ? $value->OperationRow->UnitTime / 10000000: 0;

		    			$monitorData = array(
		    				"partsNumber" => $value->Part->PartNumber,
		    				"partsName" => $value->Part->Description,
		    				"partsOperation" => $value->OperationRow->Description,
		    				"patrsCycleTime" => $patrsCycleTime,
		    				"ReportedRestQuantity" => $value->RestQuantity,
		    				"ReportedQuantity" => $value->ReportedQuantity,
		    				"ManufacturingOrderId" =>  $this->getMonitorOrderNumber($SessionId,$value->ManufacturingOrderId),
		    				"ReportNumber" => $value->ReportNumber,
		    				"operationRowId" => $value->Id,
		    				"PartId" => $value->PartId,
		    				"userId" => $userId,
		    				"machineId" => $machineId,
		    				"TimeUnit" => $value->OperationRow->TimeUnit,
		    				"PlannedStartDate" => $value->PlannedStartDate,
		    				"PlannedFinishDate" => $value->PlannedFinishDate,
		    			);

		    			$operationRowIds[] = $value->Id;
		    			$checkPart = $this->APIM->getWhereSingle($factoryId, array("operationRowId" => $value->Id), "machineParts");

		    			if (!empty($checkPart)) 
		    			{
		    				$this->APIM->updateFactoryData($factoryId,array("operationRowId" => $value->Id),$monitorData,"machineParts");
		    			}else
		    			{
		    				$this->APIM->insertFactoryData($factoryId,$monitorData,"machineParts");

		    			}
		    		}

		    		$result = $this->APIM->getWhereInWithOrderBy($factoryId,"*", array("machineId" => $machineId), array("operationRowId" => $operationRowIds),"PlannedStartDate","asc", "machineParts");
		    	}else
		    	{
		    		$result = array();
		    	}
			}else
			{
				$result = $this->APIM->getWhere($factoryId, array("machineId" => $machineId,"isDelete" => "0"), "machineParts");
			}

			

			
			$json = array("status" => "1","data" => $result,"isUserDeleted" => empty($userDetail) ? 1 : 0,"isFactoryDeleted" => empty($checkFactoryDetail) ? 1 : 0);
			

		}
	    
	    $this->set_response($json, REST_Controller::HTTP_OK); 
    }
}
