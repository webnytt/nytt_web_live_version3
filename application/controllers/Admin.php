<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Admin extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		$this->lang->load('content_lang','english');
		$this->load->helper('language');
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
		
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);

		$this->load->helper('color_helper');
		
	}

	// login function use for login page

	// In use
	public function login() 
	{
		
		if($this->AM->checkIsvalidated() == true) 
		{
		    redirect('admin/overview');  
		}
		$this->load->view('login'); 
	}

	// forgot_password function use for forgot password page

	// In use
	public function forgot_password() 
	{
		if($this->AM->checkIsvalidated() == true) 
		{
		    redirect('admin/overview');  
		}
		$this->load->view('forgot_password'); 
	}

	//checkForgotPassword function use for check username is valid or not and send mail for operator password

	// In use
	public function checkForgotPassword() 
	{ 
	    $userName = $this->input->post("userName");
		if(isset($userName)) 
		{ 
			//Check user name valid or not
			$userDetail = $this->AM->getUserPassword($userName);
			if($userDetail != '') 
			{
				$subject = "Operator forgot password";
				$message = 'Hello Admin, <br />You have got forgot password request from '.$userName.' Operator password is: '.$userDetail;
				$this->AM->sendEmail($message,$subject,"","");

				$this->session->set_flashdata('success_message', Emailsentsuccessfullycontactinfonytttechcom);
			} else 
			{
				$this->session->set_flashdata('error_message', Invaliddetails);
			}
		} else 
		{
			$this->session->set_flashdata('error_message',Usernamerequired);
		} 
		redirect('admin/forgot_password','refresh');
    }
	
	// selectFactory function use for select factory page

	// In use
	public function selectFactory() 
	{	
		if($this->AM->checkIsvalidated() == true) 
		{
		    redirect('admin/overview'); 
		}
		
		if($this->input->post())
		{
			$userdata = $this->session->userdata(); 
			$userdata['factoryId'] = $this->input->post('factoryId'); 
			$result = $this->AM->getWhereDBSingle(array("factoryId" => $this->input->post('factoryId')),"factory");
			$userdata['factoryType'] = $result->type; 
			$userdata['isMonitor'] = $result->isMonitor; 
			$this->session->set_userdata($userdata);
			redirect('admin/overview'); 
		}
		$listFactories = $this->AM->getallFactories();
		$data = array(
					'listFactories'=>$listFactories,
				    );
		$this->load->view('selectFactory', $data); 
	}
	
	// changeFactory function use for change factory in dashboard
	public function changeFactory() 
	{	
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		if($this->input->post())
		{
			$previousFactoryType = $this->session->userdata('factoryType'); 
			$userdata = $this->session->userdata(); 
			$userdata['factoryId'] = $this->input->post('factoryId'); 
			$result = $this->AM->getWhereDBSingle(array("factoryId" => $this->input->post('factoryId')),"factory");
			$userdata['factoryType'] = $result->type; 
			$userdata['isMonitor'] = $result->isMonitor; 
			$userdata['previousFactoryType'] = $previousFactoryType; 
			$this->session->set_userdata($userdata);
			$json = $this->session->userdata();
			echo json_encode($json);
		}
	}
	
	//process function use for check user enter proper login detail or not

	//IN use
	public function process()
	{
	    if($this->AM->checkIsvalidated() == true) 
        {
		    redirect('admin/login'); 
		}
		//Check user username and password
        $result = $this->AM->validate();
	    if(empty($result))
        {
            $this->session->set_flashdata('error_message', Pleaseentervaliddetailstosignin); 
            redirect('admin/login');
        }
        else
        {
        	if ($result->logKeyDashboard == "1") 
        	{
        		$this->session->set_flashdata('error_message', Useralreadysigninotherdevice); 
            	redirect('admin/login');
			}
			else
			{
                $userdata = $this->session->userdata(); 
				$userdata['factoryId'] = $this->session->userdata('factoryId'); 
				$result = $this->AM->getWhereDBSingle(array("factoryId" => $this->session->userdata('factoryId')),"factory");
	            $userdata['factoryType'] = $result->type;
	            $this->session->set_userdata($userdata);
	            redirect('admin/overview'); 
	        } 
        }        
    }

    //logout function use for logout in dashboard

    //In use
	public function logout() 
	{
		$data = array("logKeyDashboard" => "0");
		$where = array("userId" => $this->session->userdata('userId'));
		$this->AM->updateDBData($where,$data, 'user');
		$this->AM->doLogout(); 
	}


	//add_machine function user for add new machine 

	//In use
	public function add_machine() 
	{
		//Check user login or not
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineName','Name','required|callback_check_machine_name'); 
        $this->form_validation->set_rules('machineType','Type','required');
        $this->form_validation->set_rules('controlSystem','controlSystem','required');
        if($this->input->post('machineAutomation') == 'Automatic' || $this->input->post('machineAutomation') == 'Both') 
        {
			$this->form_validation->set_rules('machineLoading','Loading','required'); 
		} 

		$running = $this->input->post('running'); 
        $waiting = $this->input->post('waiting'); 
        $stopped = $this->input->post('stopped'); 
        $off = $this->input->post('off'); 
        $isDataGet = $this->input->post('isDataGet');
        //Check machine type no stacklight or with stacklight
        if ($isDataGet == "3") 
        {
        	//checking required parameters validation
        	if($this->form_validation->run()==false)
	        { 
	            if(form_error('machineName') ) { echo form_error('machineName'); }
	            if(form_error('machineNumber') ) { echo form_error('machineNumber'); }
	            echo Pleasefillvaliddetailsformachine ; die;
	        } 
	        else 
	        { 
	        	//get new machine code for new machine 
			    $machineCode = $this->machineCode();
	            
	            $insert=array(
	                'machineName'=>$this->input->post('machineName'),
	                'machineType'=>$this->input->post('machineType'),  
	                'machineMake'=>$this->input->post('machineMake'),  
	                'machineControlUnit'=>$this->input->post('controlSystem'),  
	                'machinePurchaseDate'=>$this->input->post('machinePurchaseDate'),  
	                'machineLoading'=>$this->input->post('machineLoading'),
	                'machineAutomation'=>$this->input->post('machineAutomation'),
					'machineMTId'=>$this->input->post('machineMTId'),  
					'machineModal'=>$this->input->post('machineModal'),
					'isDataGet'=>$this->input->post('isDataGet'),
					'videoStatus'=>'1',
					'noStacklight' => "1",
					'machineLight' => "3",
					'machineSelected' => "1",
					"machineStatus" => "2"
	            );

	            //Add new machine
			    $machineId=$this->AM->newMachine($this->factory, $insert); 

			    //Add machine steps
			    $steps = $this->AM->addMachineMaintananceSteps($this->factoryId, $machineId, $insert['machineMTId']);

			    //Add machine code for machine 
				$machineCodeData = array("factoryId" => $this->factoryId,"machineId" => $machineId,"machineCode" => base64_encode($machineCode));
			    $this->AM->insertDBData($machineCodeData,"machineCode");

				$running_insert = array(
						'machineId'=> $machineId,
						'machineStateGrp'=> "1",  
						'machineStateVal'=> "Running",  
						'greenStatus'=> "1",
						'yellowStatus'=> "0",
						'redStatus'=> "0",
						'whiteStatus'=> "0",
						'blueStatus'=> "0",
					); 

				//Add machine color state for analytics 
				$this->AM->newMachineColorState($this->factory, $running_insert);

				$machineStatusLogData = array("machineId" => $machineId,"machineStatus" => "2","insertTime" => date('Y-m-d H:i:s'));
				$this->AM->insertData($machineStatusLogData, "machineStatusLog");
				
				//Add one entry on betaDetectionLog for analytics graph
				$betaDetectionLogData = array("machineId" => $machineId,"color" => "green","duration" => "0","durationLogId" => "0","accuracy" => "0","insertTime" => date('Y-m-d H:i:s',strtotime("+1 minutes")),"originalTime" => date('Y-m-d H:i:s',strtotime("+1 minutes")),"userId" => 0);
				$this->AM->insertData($betaDetectionLogData, "betaDetectionLog");
				
				echo "1"; die; 
	        }
        }else
        {

        	if ($isDataGet == "0") 
        	{
        		if (empty($running) || empty($waiting) || empty($stopped) || empty($off)) 
        		{
        			$isValidation = false;
        		}else
        		{
        			$isValidation = true;
        		}
        	}else
        	{
        		$isValidation = true;
        	}
        	//checking required parameters validation
        	if($this->form_validation->run() == false || $isValidation == false)
	        { 
	            if(form_error('machineName') ) { echo form_error('machineName'); }
	            if(form_error('machineNumber') ) { echo form_error('machineNumber'); }
	            echo Pleasefillvaliddetailsformachine ; die;
	        } 
	        else 
	        { 
	            $machineLight = implode(",",$this->input->post('machineLight')); 
	            $nodet = explode(",", $this->input->post("nodet"));
			    $running = $this->input->post('running'); 
	            $waiting = $this->input->post('waiting'); 
	            $stopped = $this->input->post('stopped'); 
	            $off = $this->input->post('off'); 
			    $count = count($this->input->post('machineLight'));
	            $running_count = count($running); 
	            $waiting_count = count($waiting); 
	            $stopped_count = count($stopped); 
	            $off_count = count($off); 
	            $total_count = $running_count + $waiting_count + $stopped_count + $off_count;
	            //get new machine code for new machine 
			    $machineCode = $this->machineCode();
	            
	            $insert=array(
	                'machineName'=>$this->input->post('machineName'),
	                'machineType'=>$this->input->post('machineType'),  
	                'machineMake'=>$this->input->post('machineMake'),  
	                'machineControlUnit'=>$this->input->post('controlSystem'),  
	                'machinePurchaseDate'=>$this->input->post('machinePurchaseDate'),  
	                'machineLoading'=>$this->input->post('machineLoading'),
	                'machineAutomation'=>$this->input->post('machineAutomation'),
					'machineLight'=>$this->input->post('machineLightNew'),
					'machineMTId'=>$this->input->post('machineMTId'),  
					'machineModal'=>$this->input->post('machineModal'),
					'machineModal'=>$this->input->post('machineModal'),
					'isDataGet'=>$this->input->post('isDataGet'),
					'videoStatus'=>'1',
					'noStacklight' => "0"
	            );

	            //Add new machine
			    $machineId=$this->AM->newMachine($this->factory, $insert); 

			    if ($isDataGet == "2") 
			    {
			    	$this->AM->insertData(array("machineId"  => $machineId), "mtConnectOverview"); 
			    }

			    if ($isDataGet == "4") 
			    {
			    	$this->AM->insertData(array("machineId"  => $machineId), "virtualMachineLogOverview"); 
			    }


			    $breakdownReason = $this->db->get('breakdownReason')->result_array();

				foreach ($breakdownReason as $key => $value) 
				{
					$position = ($value['breakdownReasonText'] == "Other") ? 10 : $key + 1;

					if ($value['breakdownReasonText'] == "Operator interference") 
					{
						$errorTypeId = 3;	
					}elseif ($value['breakdownReasonText'] == "Tool issue") 
					{
						$errorTypeId = 2;	
					}elseif ($value['breakdownReasonText'] == "Lack of material") 
					{
						$errorTypeId = 4;	
					}elseif ($value['breakdownReasonText'] == "Machine breakdown") 
					{
						$errorTypeId = 1;	
					}elseif ($value['breakdownReasonText'] == "Setup") 
					{
						$errorTypeId = 1;	
					}elseif ($value['breakdownReasonText'] == "Operator occupied") 
					{
						$errorTypeId = 3;	
					}elseif ($value['breakdownReasonText'] == "Other") 
					{
						$errorTypeId = 5;	
					}

					$dataMachine = array(
						"machineId" => $machineId,
						"errorTypeId" => $errorTypeId,
						"position" => $position,
						"breakdownReason" => $value['breakdownReasonText']
					);

					$this->factory->insert('machineBreakdownReason',$dataMachine);
					
				}


				for ($i=7; $i <= 9; $i++) 
				{ 
					
					$dataMachine = array(
						"machineId" => $machineId,
						"errorTypeId" => "",
						"position" => $i,
						"breakdownReason" => ""
					);

					$this->factory->insert('machineBreakdownReason',$dataMachine);
					
				}

			    //Add machine steps
			    $steps = $this->AM->addMachineMaintananceSteps($this->factoryId, $machineId, $insert['machineMTId']);

			    //Add machine code for machine 
				$machineCodeData = array("factoryId" => $this->factoryId,"machineId" => $machineId,"machineCode" => base64_encode($machineCode));
			    $this->AM->insertDBData($machineCodeData,"machineCode");

			    if (!empty($running)) 
			    {
			    	//Add color detail for running state
					foreach ($running as $key => $value) 
					{
						$running_color = explode(",", $value);
						$running_insert = array(
								'machineId'=> $machineId,
								'machineStateGrp'=> "1",  
								'machineStateVal'=> "Running",  
								'greenStatus'=> in_array("Green", $running_color) ? "1" : "0",
								'yellowStatus'=> in_array("Yellow", $running_color) ? "1" : "0",
								'redStatus'=> in_array("Red", $running_color) ? "1" : "0",
								'whiteStatus'=> in_array("White", $running_color) ? "1" : "0",
								'blueStatus'=> in_array("Blue", $running_color) ? "1" : "0",
							); 

						$this->AM->newMachineColorState($this->factory, $running_insert);
					}
				}

				if (!empty($waiting)) 
			    {
					//Add color detail for waiting state
					foreach ($waiting as $key => $value) 
					{
						$waiting_color = explode(",", $value);
						$waiting_insert = array(
								'machineId'=> $machineId,
								'machineStateGrp'=> "2",  
								'machineStateVal'=> "Waiting",  
								'greenStatus'=> in_array("Green", $waiting_color) ? "1" : "0",
								'yellowStatus'=> in_array("Yellow", $waiting_color) ? "1" : "0",
								'redStatus'=> in_array("Red", $waiting_color) ? "1" : "0",
								'whiteStatus'=> in_array("White", $waiting_color) ? "1" : "0",
								'blueStatus'=> in_array("Blue", $waiting_color) ? "1" : "0",
							); 
							$this->AM->newMachineColorState($this->factory, $waiting_insert);
					}
				}

				if (!empty($stopped)) 
			    {
					//Add color detail for stopped state
					foreach ($stopped as $key => $value) 
					{
						$stopped_color = explode(",", $value);
						$stopped_insert = array(
								'machineId'=> $machineId,
								'machineStateGrp'=> "3",  
								'machineStateVal'=> "Stopped",  
								'greenStatus'=> in_array("Green", $stopped_color) ? "1" : "0",
								'yellowStatus'=> in_array("Yellow", $stopped_color) ? "1" : "0",
								'redStatus'=> in_array("Red", $stopped_color) ? "1" : "0",
								'whiteStatus'=> in_array("White", $stopped_color) ? "1" : "0",
								'blueStatus'=> in_array("Blue", $stopped_color) ? "1" : "0",
							); 
						$this->AM->newMachineColorState($this->factory, $stopped_insert);
					}
				}

				if (!empty($off)) 
			    {
					//Add color detail for off state
					foreach ($off as $key => $value) 
					{
						$off_color = explode(",", $value);

						$off_insert = array(
								'machineId'=> $machineId,
								'machineStateGrp'=> "4",  
								'machineStateVal'=> "Off",  
								'greenStatus'=> in_array("Green", $off_color) ? "1" : "0",
								'yellowStatus'=> in_array("Yellow", $off_color) ? "1" : "0",
								'redStatus'=> in_array("Red", $off_color) ? "1" : "0",
								'whiteStatus'=> in_array("White", $off_color) ? "1" : "0",
								'blueStatus'=> in_array("Blue", $off_color) ? "1" : "0",
							); 
						$this->AM->newMachineColorState($this->factory, $off_insert);
					}
				}

				//Add color detail for nodet state

				if (!empty($nodet) && $isDataGet == "0") 
			    {
					foreach ($nodet as $key => $value) 
					{
						$new_value = str_replace("-", ",", $value);
						if (!in_array($new_value, $off) && !in_array($new_value, $running) && !in_array($new_value, $waiting) && !in_array($new_value, $stopped)) 
						{
							$nodetArray = explode(",", $new_value);
								$off_insert = array(
									'machineId'=> $machineId,
									'machineStateGrp'=> "5",  
									'machineStateVal'=> "nodet",  
									'greenStatus'=> in_array("Green", $nodetArray) ? "1" : "0",
									'yellowStatus'=> in_array("Yellow", $nodetArray) ? "1" : "0",
									'redStatus'=> in_array("Red", $nodetArray) ? "1" : "0",
									'whiteStatus'=> in_array("White", $nodetArray) ? "1" : "0",
									'blueStatus'=> in_array("Blue", $nodetArray) ? "1" : "0",
								); 

							$GreenN = in_array("Green", $nodetArray) ? "1" : "0";
							$YellowN = in_array("Yellow", $nodetArray) ? "1" : "0";
							$RedN = in_array("Red", $nodetArray) ? "1" : "0";
							$WhiteN = in_array("White", $nodetArray) ? "1" : "0";
							$BlueN = in_array("Blue", $nodetArray) ? "1" : "0";
							if ($GreenN == "0" &&  $YellowN == "0" &&  $RedN == "0" &&  $WhiteN == "0" &&  $BlueN == "0") 
							{
								
							}else
							{
								$this->AM->newMachineColorState($this->factory, $off_insert);
							}
						}
					}
				}


				$result = $this->AM->getWhere(array('machineId' => $machineId), 'machineStateColorLookup');

				foreach ($result as $key => $value) 
				{

					$machineStartAclrId = $value['aclrId'];
					$aclrId = $value['machineStateGrp'];
					$result1 = $this->AM->getWhere(array('aclrId !=' => $machineStartAclrId,'machineId' => $machineId), 'machineStateColorLookup');

					foreach ($result1 as $key1 => $value1) {
						$machineEndAclrId = $value1['aclrId'];

						$statePeriodLookupData = array(
								"machineId" => $machineId,
								"aclrId" => $aclrId,
								"machineStartAclrId" => $machineStartAclrId,
								"machineEndAclrId" => $machineEndAclrId,
							);
						$this->AM->insertData($statePeriodLookupData, "machineStatePeriodLookup");
					}
				}
				echo "1"; die; 
	        }
        }
        
	}

	//machineCode function user for get new machine code
	function machineCode()
	{
		$chars = array(
	        'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm',
	        'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z',
	        'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M',
	        'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z',
	        '0', '1', '2', '3', '4', '5', '6', '7', '8', '9'
	    );
	    shuffle($chars);
	    $num_chars = count($chars) - 1;
	    $token = '';
	    $count = 4;

	    for ($i = 0; $i < $count; $i++)
	    {
	        $token .= $chars[mt_rand(0, $num_chars)];
	    }
	    $where = array("machineCode" => $token);
	    $result = $this->AM->getWhereDB($where, "machineCode");
	    if (!empty($machineCode)) 
	    {
	    	$this->machineCode();
	    }
	    else
	    {
	    	return $token;
	    }
	}
	
	//update_stacklight_configuration function use for update stacklight color details 
	public function update_stacklight_configuration() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineId','MachineId','required'); 

		//checking required parameters validation
		if($this->form_validation->run()==false)
        { 
            echo Pleasefillvaliddetailsformachine ; die;
        } 
        else 
        { 
            $machineId = $this->input->post('machineId'); 
            $machine = $this->factory->where("machineId",$machineId)->get('machine')->row();
            $machineLight = explode(",", $machine->machineLight); 
				
			$running = $waiting = $stopped = $off = $running_color = $waiting_color = $stopped_color = $off_color = array(); 
			
			$allRunningA = $allWaitingA = $allStoppedA = $allOffA = array(); 

		    $running = $this->input->post('running');
		    $waiting = $this->input->post('waiting'); 
            $stopped = $this->input->post('stopped'); 
            $off = $this->input->post('off');
            $nodet = explode(",", $this->input->post("nodet"));
			$allRunning = $this->factory->select('aclrId')->where('machineId',$machineId)->where('machineStateVal','Running')->get('machineStateColorLookup')->result();
			$allWaiting = $this->factory->select('aclrId')->where('machineId',$machineId)->where('machineStateVal','Waiting')->get('machineStateColorLookup')->result();
			$allStopped = $this->factory->select('aclrId')->where('machineId',$machineId)->where('machineStateVal','Stopped')->get('machineStateColorLookup')->result();
			$allOff = $this->factory->select('aclrId')->where('machineId',$machineId)->where('machineStateVal','Off')->get('machineStateColorLookup')->result();
			
            $count = count($machineLight);
            $running_count = count($running); 
            $waiting_count = count($waiting); 
            $stopped_count = count($stopped); 
            $off_count = count($off); 
            $total_count = $running_count + $waiting_count + $stopped_count + $off_count;
            if (!empty($running)) {

            	//Update color detail for running state
				foreach ($running as $key => $value) 
				{
					$running_color = explode(",", $value);
					if (in_array("Red", $running_color)) 
					{
						$running_where['redStatus'] = "1";
					}else
					{
						$running_where['redStatus'] = "0";
					}

					if (in_array("Yellow", $running_color)) 
					{
						$running_where['yellowStatus'] = "1";
					}else
					{
						$running_where['yellowStatus'] = "0";
					}

					if (in_array("Green", $running_color)) 
					{
						$running_where['greenStatus'] = "1";
					}else
					{
						$running_where['greenStatus'] = "0";
					}

					if (in_array("Blue", $running_color)) 
					{
						$running_where['blueStatus'] = "1";
					}else
					{
						$running_where['blueStatus'] = "0";
					}

					if (in_array("White", $running_color)) 
					{
						$running_where['whiteStatus'] = "1";
					}else
					{
						$running_where['whiteStatus'] = "0";
					}

					$running_where['machineId'] = $machineId;
					$running_machineStateColorLookup = $this->factory->where($running_where)->get('machineStateColorLookup')->row();
					$running_insert = array(
						'machineId'=> $machineId,
						'machineStateGrp'=> "1",  
						'machineStateVal'=> "Running",  
						'greenStatus'=> in_array("Green", $running_color) ? "1" : "0",
						'yellowStatus'=> in_array("Yellow", $running_color) ? "1" : "0",
						'redStatus'=> in_array("Red", $running_color) ? "1" : "0",
						'whiteStatus'=> in_array("White", $running_color) ? "1" : "0",
						'blueStatus'=> in_array("Blue", $running_color) ? "1" : "0",
					); 

					if (!empty($running_machineStateColorLookup)) 
					{
						$runningaclrId = $running_machineStateColorLookup->aclrId;
						$this->AM->updateMachineColorState($this->factory, $running_insert,$runningaclrId);
					}
					else
					{
						$this->AM->newMachineColorState($this->factory, $running_insert);
					}
				} 
			}
			if (!empty($waiting)) {

				//Update color detail for waiting state
				foreach ($waiting as $key => $value) 
				{
					$waiting_color = explode(",", $value);

					if (in_array("Red", $waiting_color)) 
					{
						$waiting_where['redStatus'] = "1";
					}else
					{
						$waiting_where['redStatus'] = "0";
					}

					if (in_array("Yellow", $waiting_color)) 
					{
						$waiting_where['yellowStatus'] = "1";
					}else
					{
						$waiting_where['yellowStatus'] = "0";
					}

					if (in_array("Green", $waiting_color)) 
					{
						$waiting_where['greenStatus'] = "1";
					}else
					{
						$waiting_where['greenStatus'] = "0";
					}

					if (in_array("Blue", $waiting_color)) 
					{
						$waiting_where['blueStatus'] = "1";
					}else
					{
						$waiting_where['blueStatus'] = "0";
					}

					if (in_array("White", $waiting_color)) 
					{
						$waiting_where['whiteStatus'] = "1";
					}else
					{
						$waiting_where['whiteStatus'] = "0";
					}

					$waiting_where['machineId'] = $machineId;
					$waiting_machineStateColorLookup = $this->factory->where($waiting_where)->get('machineStateColorLookup')->row();

					$waiting_insert = array(
						'machineId'=> $machineId,
						'machineStateGrp'=> "2",  
						'machineStateVal'=> "Waiting",  
						'greenStatus'=> in_array("Green", $waiting_color) ? "1" : "0",
						'yellowStatus'=> in_array("Yellow", $waiting_color) ? "1" : "0",
						'redStatus'=> in_array("Red", $waiting_color) ? "1" : "0",
						'whiteStatus'=> in_array("White", $waiting_color) ? "1" : "0",
						'blueStatus'=> in_array("Blue", $waiting_color) ? "1" : "0",
					); 

					if (!empty($waiting_machineStateColorLookup)) 
					{
						$waiting_aclrId = $waiting_machineStateColorLookup->aclrId;
						
						$this->AM->updateMachineColorState($this->factory, $waiting_insert,$waiting_aclrId);
					}
					else
					{
						$this->AM->newMachineColorState($this->factory, $waiting_insert);
					}
				}
			}	
			if (!empty($stopped)) 
			{
				//Update color detail for stopped state
				foreach ($stopped as $key => $value) 
				{
					$stopped_color = explode(",", $value);
					if (in_array("Red", $stopped_color)) 
					{
						$stopped_where['redStatus'] = "1";
					}else
					{
						$stopped_where['redStatus'] = "0";
					}

					if (in_array("Yellow", $stopped_color)) 
					{
						$stopped_where['yellowStatus'] = "1";
					}else
					{
						$stopped_where['yellowStatus'] = "0";
					}

					if (in_array("Green", $stopped_color)) 
					{
						$stopped_where['greenStatus'] = "1";
					}else
					{
						$stopped_where['greenStatus'] = "0";
					}

					if (in_array("Blue", $stopped_color)) 
					{
						$stopped_where['blueStatus'] = "1";
					}else
					{
						$stopped_where['blueStatus'] = "0";
					}

					if (in_array("White", $stopped_color)) 
					{
						$stopped_where['whiteStatus'] = "1";
					}else
					{
						$stopped_where['whiteStatus'] = "0";
					}

					$stopped_where['machineId'] = $machineId;
					$stopped_machineStateColorLookup = $this->factory->where($stopped_where)->get('machineStateColorLookup')->row();
					$stopped_insert = array(
						    'machineId'=> $machineId,
							'machineStateGrp'=> "3",  
							'machineStateVal'=> "Stopped",  
							'greenStatus'=> in_array("Green", $stopped_color) ? "1" : "0",
							'yellowStatus'=> in_array("Yellow", $stopped_color) ? "1" : "0",
							'redStatus'=> in_array("Red", $stopped_color) ? "1" : "0",
							'whiteStatus'=> in_array("White", $stopped_color) ? "1" : "0",
							'blueStatus'=> in_array("Blue", $stopped_color) ? "1" : "0",
						); 
					if (!empty($stopped_machineStateColorLookup)) 
					{
						$stopped_aclrId = $stopped_machineStateColorLookup->aclrId;
						
						$this->AM->updateMachineColorState($this->factory, $stopped_insert,$stopped_aclrId);
					}else
					{
						$this->AM->newMachineColorState($this->factory, $stopped_insert);
					}
				}
			}
			if (!empty($off)) 
			{

				//Update color detail for off state
				foreach ($off as $key => $value) 
				{
					$off_color = explode(",", $value);

					if (in_array("Red", $off_color)) 
					{
						$off_where['redStatus'] = "1";
					}else
					{
						$off_where['redStatus'] = "0";
					}

					if (in_array("Yellow", $off_color)) 
					{
						$off_where['yellowStatus'] = "1";
					}else
					{
						$off_where['yellowStatus'] = "0";
					}

					if (in_array("Green", $off_color)) 
					{
						$off_where['greenStatus'] = "1";
					}else
					{
						$off_where['greenStatus'] = "0";
					}

					if (in_array("Blue", $off_color)) 
					{
						$off_where['blueStatus'] = "1";
					}else
					{
						$off_where['blueStatus'] = "0";
					}

					if (in_array("White", $off_color)) 
					{
						$off_where['whiteStatus'] = "1";
					}else
					{
						$off_where['whiteStatus'] = "0";
					}

					$off_where['machineId'] = $machineId;
					$off_machineStateColorLookup = $this->factory->where($off_where)->get('machineStateColorLookup')->row();
					$off_insert = array(
							'machineId'=> $machineId,
							'machineStateGrp'=> "4",  
							'machineStateVal'=> "Off",  
							'greenStatus'=> in_array("Green", $off_color) ? "1" : "0",
							'yellowStatus'=> in_array("Yellow", $off_color) ? "1" : "0",
							'redStatus'=> in_array("Red", $off_color) ? "1" : "0",
							'whiteStatus'=> in_array("White", $off_color) ? "1" : "0",
							'blueStatus'=> in_array("Blue", $off_color) ? "1" : "0",
						);

					if (!empty($off_machineStateColorLookup)) 
					{
						$off_aclrId = $off_machineStateColorLookup->aclrId;
						
						$this->AM->updateMachineColorState($this->factory, $off_insert,$off_aclrId);
					}else
					{
						$this->AM->newMachineColorState($this->factory, $off_insert);
					}
				}
			}

			$running[] = "Off";
			$off[] = "Off";
			$waiting[] = "Off";
			$stopped[] = "Off";

			//Update color detail for nodet state
			foreach ($nodet as $key => $value) 
			{
				$new_value = str_replace("-", ",", $value);
				if (!in_array($new_value, $off) && !in_array($new_value, $running) && !in_array($new_value, $waiting) && !in_array($new_value, $stopped)) 
				{
					$nodetArray = explode(",", $new_value);
					$nodet_insert = array(
						'machineId'=> $machineId,
						'machineStateGrp'=> "5",  
						'machineStateVal'=> "nodet",  
						'greenStatus'=> in_array("Green", $nodetArray) ? "1" : "0",
						'yellowStatus'=> in_array("Yellow", $nodetArray) ? "1" : "0",
						'redStatus'=> in_array("Red", $nodetArray) ? "1" : "0",
						'whiteStatus'=> in_array("White", $nodetArray) ? "1" : "0",
						'blueStatus'=> in_array("Blue", $nodetArray) ? "1" : "0",
					); 

					$nodet_where = array(
						'machineId'=> $machineId, 
						'greenStatus'=> in_array("Green", $nodetArray) ? "1" : "0",
						'yellowStatus'=> in_array("Yellow", $nodetArray) ? "1" : "0",
						'redStatus'=> in_array("Red", $nodetArray) ? "1" : "0",
						'whiteStatus'=> in_array("White", $nodetArray) ? "1" : "0",
						'blueStatus'=> in_array("Blue", $nodetArray) ? "1" : "0",
					); 

					$nodet_machineStateColorLookup = $this->factory->where($nodet_where)->get('machineStateColorLookup')->row();

					if ($new_value != "Off") 
					{	
						if (!empty($nodet_machineStateColorLookup)) 
						{
							$nodet_aclrId = $nodet_machineStateColorLookup->aclrId;
							
							$this->AM->updateMachineColorState($this->factory, $nodet_insert,$nodet_aclrId);
						}
						else
						{
							$this->AM->newMachineColorState($this->factory, $nodet_insert);
						}
					}
				}
			}

			$result = $this->AM->getWhere(array('machineId' => $machineId), 'machineStateColorLookup');

			foreach ($result as $key => $value) 
			{
				$machineStartAclrId = $value['aclrId'];
				$aclrId = $value['machineStateGrp'];
				$result1 = $this->AM->getWhere(array('aclrId !=' => $machineStartAclrId,'machineId' => $machineId), 'machineStateColorLookup');
				foreach ($result1 as $key1 => $value1) 
				{
					$machineEndAclrId = $value1['aclrId'];
					$statePeriodLookupData = array(
							"machineId" => $machineId,
							"aclrId" => $aclrId,
							"machineStartAclrId" => $machineStartAclrId,
							"machineEndAclrId" => $machineEndAclrId,
						);
					$statePeriodLookupWhere = array(
							"machineId" => $machineId,
							"machineStartAclrId" => $machineStartAclrId,
							"machineEndAclrId" => $machineEndAclrId
						);
					
					$checkStatePeriodLookup = $this->AM->getWhere($statePeriodLookupWhere, 'machineStatePeriodLookup');
					if (!empty($checkStatePeriodLookup)) 
					{
						$this->AM->updateData($statePeriodLookupWhere,$statePeriodLookupData,$statePeriodLookupData, "machineStatePeriodLookup");
					}else
					{
						$this->AM->insertData($statePeriodLookupData, "machineStatePeriodLookup");
					}
				}
			}
			echo "1"; die; 
        }
	}

	//edit_machine function user for update machine details
	public function edit_machine() 
	{	
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineName','Name','required|callback_check_machine_name'); 
        $this->form_validation->set_rules('machineType','Type','required');
        $this->form_validation->set_rules('machineModal','Modal','required');
        if($this->input->post('machineAutomation') == 'Automatic' || $this->input->post('machineAutomation') == 'Both') 
        {
			$this->form_validation->set_rules('machineLoading','Loading','required'); 
		} 

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo Pleasefillvaliddetailsformachine ; die;
        } 
        else 
        {   
			$machineLoading = $this->input->post('machineLoading');
			$machineCapacity = $this->input->post('machineCapacity');
            $update=array(
                'machineName'=>$this->input->post('machineName'),
                'machineType'=>$this->input->post('machineType'),  
                'machineMake'=>$this->input->post('machineMake'),  
                'machineControlUnit'=>$this->input->post('machineControlUnit'),  
                'machinePurchaseDate'=>$this->input->post('machinePurchaseDate'),  
                'machineLoading'=> !empty($machineLoading) ? $machineLoading : "",
                'machineAutomation'=>$this->input->post('machineAutomation'), 
                'machineModal'=>$this->input->post('machineModal'),  
            );  
            $machineId=$this->input->post('machineId'); 
            //Update machine details
            $machine=$this->AM->updateMachine($this->factory, $update, $machineId);
            echo "Machine updated successfully"; die; 
        }
		
	}

	public function updateLogStatus() 
	{	
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('machineId','machineId','required');
        $this->form_validation->set_rules('logStatus','logStatus','required');

		
		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo Pleasefillvaliddetailsformachine ; die;
        } 
        else 
        {   
			$machineId = $this->input->post('machineId');
			$logStatus = $this->input->post('logStatus');


            $data = array(
                'logStatus' => $logStatus  
            );  
            

            $this->AM->updateData(array('machineId' => $machineId),$data,"machine");

            echo "Machine updated successfully"; die; 
        }
		
	}

	//delete_machine function use for delete machine

	//In use
	public function delete_machine() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machineId = $this->input->post('deleteMachineId'); 
		//Delete machine details
		$this->AM->deleteMachine($this->factory, $machineId); 
		$factoryId = $this->factoryId;
		//Update machine selected flag
		$this->AM->unlockMachine($this->factory, $machineId);
		//Get last machine active for update machine status
		$machineUser = $this->AM->getLastActiveMachine($machineId);
        $activeId = $machineUser->activeId;
		$this->AM->updateData(array("activeId" => $activeId),array("isActive" => "0","endTime" => date('Y-m-d H:i:s')),"activeMachine");
		$fcmToken = array($machineUser->deviceToken);
		$message = "Machine has been deleted from dashboard. Please signin again.";
		$title = "Machine delete";
		$type = "deleteMachine";
		//Send notification for logout setapp machine 
		$this->AM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId);
		echo "Machine deleted successfully"; die;
	}

	//unlock_machine use for logout setapp machine in previous setapp

	//In use
	public function unlock_machine() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machineId = $this->input->post('unlockMachineId'); 
		$factoryId = $this->factoryId;
		//Unlock machine 
		$this->AM->unlockMachine($this->factory, $machineId);
		$machineUser = $this->AM->getLastActiveMachine($machineId);
        $activeId = $machineUser->activeId;
		$this->AM->updateData(array("activeId" => $activeId),array("isActive" => "0","endTime" => date('Y-m-d H:i:s')),"activeMachine");
		$fcmToken = array($machineUser->deviceToken);
		$message = "Machine has been unlocked from dashboard. Please signin again.";
		$title = "Machine unlock";
		$type = "unlockMachine";
		//Send notification for logout setapp machine 
		$this->AM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId);
        echo "1"; die; 
	}

	//check_machine_name function use for check machine name already exist or not

	//In use
	public function check_machine_name($name) 
	{  
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->input->post('machineId')) 
        {
            $machineId = $this->input->post('machineId');
        } 
        else 
        {
            $machineId = 0;
        } 
        //Check machine name already exist or not
		if($this->AM->machineExists($this->factory,$name, $machineId)) 
        {  
            $this->form_validation->set_message('check_machine_name','Machine name must be unique.');
            return false;  
        }
        else 
        {
            return true;
        }
    }

    //check_user_name function use for check user name already exist or not

    //In use
	public function check_user_name($name) 
    {
    	if($this->AM->checkIsvalidated() == false) 
    	{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if(!preg_match("/^[a-zA-Z0-9]+$/",$name) ) 
        {
            $this->form_validation->set_message('check_user_name','Username may contain only alphabets, numbers.');
            return false;  
        } 
        else 
        { 
            if($this->input->post('userId')) 
            {
                $userId = $this->input->post('userId');
            } 
            else 
            {
                $userId = 0;
            } 
            //Check user name already exist or not
            if($this->AM->userExists($name, $userId)) 
            {  
                $this->form_validation->set_message('check_user_name','Username must be unique.');
                return false;  
            } 
            else 
            {
                return true;
            }
        }   
    }

     //check_stacklighttype_name function use for check stacklight name already exist or not
    public function check_stacklighttype_name($name) 
    {
    	if($this->AM->checkIsvalidated() == false) 
    	{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

    	if(!preg_match("/^[a-zA-Z0-9 ]+$/",$name) ) 
        {
            $this->form_validation->set_message('check_stacklighttype_name','Stack Light Type name may contain only alphabets, space or digits');  
            return false;  
        } 
        else 
        {
            if($this->input->post('stackLightTypeId')) 
            {
                $stackLightTypeId = $this->input->post('stackLightTypeId');
            } 
            else 
            {
                $stackLightTypeId = 0;
            } 
             //Check stacklight name already exist or not
            if($this->AM->stackLightTypeExists($name, $stackLightTypeId)) 
            {   
                $this->form_validation->set_message('check_stacklighttype_name','Stack Light Type name must be unique.');
                return false;  
            } 
            else 
            {
                return true;
            }
        }   
    }
	

	//dashboard3 function use for analytics page

	//not is use
	public function dashboard3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$listMachines = array();
		//Check user role and get machine as par user role
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachines($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		//print_r($listMachines->result_array);exit;
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('dashboard3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/dashboard3_footer');
	}
	
	//breakdown3 function use for machine stop page detail

	//not in use
	public function breakdown3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$listMachines = array();
		//Check user role and get machine as par user role
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('breakdown3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/breakdown3_footer');
	}

	//breakdown3_pagination function use for get machine breakdown details
	public function breakdown3_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) {
				$machineId = $this->input->post('machineId');
			}
		} 
		$choose1 = $this->input->post('choose1'); 
		$choose2 = $this->input->post('choose2'); 
		$choose3 = $this->input->post('choose3'); 
		$choose4 = $this->input->post('choose4'); 
		
		$duration1 = $this->input->post('duration1'); 
		$duration2 = $this->input->post('duration2'); 
		$duration3 = $this->input->post('duration3'); 
		$duration4 = $this->input->post('duration4'); 
		
		$result['printQuery'] = '';
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
		$choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
		
		$result['daily']['filter1'] = 0; $result['daily']['filter1count'] = 0; $x1 = 0; $y1 = 0; $br11 = 0; $br12 = 0; $br13 = 0; $br14 = 0; $br15 = 0; 
		$result['daily']['filter2'] = 0; $result['daily']['filter2count'] = 0; $x2 = 0; $y2 = 0; $br21 = 0; $br22 = 0; $br23 = 0; $br24 = 0; $br25 = 0;
		$result['daily']['filter3'] = 0; $result['daily']['filter3count'] = 0; $x3 = 0; $y3 = 0; $br31 = 0; $br32 = 0; $br33 = 0; $br34 = 0; $br35 = 0;
		$result['daily']['filter4'] = 0; $result['daily']['filter4count'] = 0; $x4 = 0; $y4 = 0; $br41 = 0; $br42 = 0; $br43 = 0; $br44 = 0; $br45 = 0;
		
		$br61 = 0; $br62 = 0; $br63 = 0; $br64 = 0; $br65 = 0; $br71 = 0; $br72 = 0; $br73 = 0; $br74 = 0; $br75 = 0; $br81 = 0; $br82 = 0; $br83 = 0; $br84 = 0; $br85 = 0;  
		
		$result['daily']['reasonfilter1'] = array();
		$result['daily']['reasonfilter2'] = array();
		$result['daily']['reasonfilter3'] = array();
		$result['daily']['reasonfilter4'] = array();
		
		$w3End = $duration4*60;
		$w4End = 9999999999;
		
		$aT = 0;
		$bT = 0;
		$cT = 0;
		$dT = 0;
		$listReasons = $this->AM->getallReasons()->result();
		$resonCount = count($listReasons);
		$listReasons[$resonCount] = new stdClass;
		$listReasons[$resonCount]->breakdownReasonId = $resonCount+1;   
		$listReasons[$resonCount]->breakdownReasonText = '';

		$analytics = $this->input->post('analytics');
		$machineResult = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");
		
		//Get machine stop detail as par selected filter
		$resultStopsQ = $this->AM->getStopAnalyticsData($this->factory, $machineId, $choose1, $choose2, $choose4Pad, $analytics); 
		if($resultStopsQ == '' ) 
		{
			$resultStops = array();
		} 
		else 
		{
			$resultStops = $resultStopsQ->result();
			//Get max seconds
			if (!empty($resultStops)) 
			{
				$result['daily']['maxSecondsValue'] = max(array_column($resultStops,"colorDuration"));
			}
			else
			{
				$result['daily']['maxSecondsValue'] = "";
			}
			if(count($resultStops) > 0 ) 
			{

				for($x=0;$x<count($resultStops);$x++) 
				{
					if($resultStops[$x]->colorDuration >= $duration1*60 && $resultStops[$x]->colorDuration < $duration2*60) 
					{ 
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para1 = 'br'.$list->breakdownReasonId.'1';
									if ($list->breakdownReasonId == 5 && $aT == 0) 
									{
										$$para1 = 0;
										$aT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para1++;
									break; 
								}
							} 
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter1'][$x1][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter1'][$x1][1] = $resultStops[$x]->colorDuration;
							$x1++;
						}
						$result['daily']['filter1'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter1count']++; 
					} 
					if($resultStops[$x]->colorDuration >= $duration2*60 && $resultStops[$x]->colorDuration < $duration3*60) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para3 = 'br'.$list->breakdownReasonId.'2';
									if ($list->breakdownReasonId == 5 && $bT == 0) 
									{
										$$para3 = 0;
										$bT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para3++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter2'][$x2][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter2'][$x2][1] = $resultStops[$x]->colorDuration;
							$x2++;
						}
						$result['daily']['filter2'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter2count']++;
					} 
					if($resultStops[$x]->colorDuration >= $duration3*60 && $resultStops[$x]->colorDuration < $duration4*60 ) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) {
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para5 = 'br'.$list->breakdownReasonId.'3';
									if ($list->breakdownReasonId == 5 && $cT == 0) {
										$$para5 = 0;
										$cT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para5++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter3'][$x3][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter3'][$x3][1] = $resultStops[$x]->colorDuration; 
							$x3++;
						}
						$result['daily']['filter3'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter3count']++;
					}
					
					if($resultStops[$x]->colorDuration >= $duration4*60 && $resultStops[$x]->colorDuration < $w4End) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
						$flag = 0;
						foreach($listReasons as $list) {
							if ($resultStops[$x]->comment == $list->breakdownReasonText) 
							{
								$para7 = 'br'.$list->breakdownReasonId.'4';
								if ($list->breakdownReasonId == 5 && $dT == 0) 
								{
									$$para7 = 0;
									$dT = 1;
								}
								//Set reason stop details
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][0] = $resultStops[$x]->timeS;
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][1] = $resultStops[$x]->colorDuration; 
								$flag = 1; $$para7++;
								break; 
							}
						}
						
					} 
					else 
					{
						//Set stop details
						$result['daily']['stopfilter4'][$x4][0] = $resultStops[$x]->timeS;
						$result['daily']['stopfilter4'][$x4][1] = $resultStops[$x]->colorDuration;
						 $x4++;
					}
						$result['daily']['filter4'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter4count']++;
					}
				}  
				
				if($result['daily']['filter1'] != 0) 
				{
					$result['daily']['filter1'] = sprintf('%0.2f', $result['daily']['filter1']/60);
				}
				if($result['daily']['filter2'] != 0) 
				{
					$result['daily']['filter2'] = sprintf('%0.2f', $result['daily']['filter2']/60);
				}
				if($result['daily']['filter3'] != 0) 
				{
					$result['daily']['filter3'] = sprintf('%0.2f', $result['daily']['filter3']/60);
				}
				if($result['daily']['filter4'] != 0) 
				{
					$result['daily']['filter4'] = sprintf('%0.2f', $result['daily']['filter4']/60);
				}
			}
		}
		
		$filter1Val = $this->input->post('filter1Val'); 
		$filter2Val = $this->input->post('filter2Val'); 
		$filter3Val = $this->input->post('filter3Val'); 
		$filter4Val = $this->input->post('filter4Val'); 
		for($i=0;$i<count($listReasons);$i++) 
		{ 
			$reasonNameArr[$i] = $listReasons[$i]->breakdownReasonText;
			if($filter1Val == '1') 
			{
				$minDuration1 = $duration1*60;
				$maxDuration1 = $duration2*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr1 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration1, $maxDuration1, $machineId, $analytics);
				$reasonCountArr11[$i]['value'] = 0; $reasonCountArr11Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr1)) 
				{
					for($x=0;$x<count($issueReasonArr1);$x++)
					{
						if($issueReasonArr1[$x]->comment == $reasonNameArr[$i]) 
						{ 
							$reasonCountArr11[$i]['value']++;
							$reasonCountArr11Duration[$i]['value'] += $issueReasonArr1[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr1'] = $reasonCountArr11;
				$result['daily']['reasonCountArrDuration1'] = $reasonCountArr11Duration;
			}
			
			if($filter2Val == '1') 
			{
				$minDuration2 = $duration2*60;
				$maxDuration2 = $duration3*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr2 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration2, $maxDuration2, $machineId, $analytics);
				$reasonCountArr21[$i]['value'] = 0;  $reasonCountArr21Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr2)) 
				{
					for($x=0;$x<count($issueReasonArr2);$x++)
					{
						if($issueReasonArr2[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr21[$i]['value']++;
							$reasonCountArr21Duration[$i]['value'] += $issueReasonArr2[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr2'] = $reasonCountArr21; 
				$result['daily']['reasonCountArrDuration2'] = $reasonCountArr21Duration;
			}
			
			if($filter3Val == '1') 
			{
				$minDuration3 = $duration3*60;
				$maxDuration3 = $duration4*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr3 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration3, $maxDuration3, $machineId, $analytics);
				$reasonCountArr31[$i]['value'] = 0;  $reasonCountArr31Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr3)) 
				{
					for($x=0;$x<count($issueReasonArr3);$x++)
					{
						if($issueReasonArr3[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr31[$i]['value']++;
							$reasonCountArr31Duration[$i]['value'] += $issueReasonArr3[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr3'] = $reasonCountArr31; 
				$result['daily']['reasonCountArrDuration3'] = $reasonCountArr31Duration;
			}
			
			if($filter4Val == '1') 
			{
				$minDuration4 = $duration4*60;
				$maxDuration4 = $w4End;
				//Get reason as page time to set in graph stop count
				$issueReasonArr4 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration4, $maxDuration4, $machineId, $analytics);
				$reasonCountArr41[$i]['value'] = 0;  $reasonCountArr41Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr4)) {
					for($x=0;$x<count($issueReasonArr4);$x++){
						if($issueReasonArr4[$x]->comment == $reasonNameArr[$i]) {
							$reasonCountArr41[$i]['value']++;
							$reasonCountArr41Duration[$i]['value'] += $issueReasonArr4[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr4'] = $reasonCountArr41; 
				$result['daily']['reasonCountArrDuration4'] = $reasonCountArr41Duration;
			}
		}
		$reasonNameArr[$resonCount] = 'Unanswered'; 

		if ($analytics == "waitAnalytics") {
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyWait == "0") ? "None" : "After ". $machineResult->notifyWait ." min";
            $result['machineNotifyTime']['errorReasonNotification'] = ($machineResult->prolongedWait == "0") ? "None" : "After ". $machineResult->prolongedWait ." min";
        }
        else
        {
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyStop == "0") ? "None" : "After ". $machineResult->notifyStop ." min";
            $result['machineNotifyTime']['errorReasonNotification'] = ($machineResult->prolongedStop == "0") ? "None" : "After ". $machineResult->prolongedStop ." min";
        }

		$result['daily']['reasonNameArr'] = $reasonNameArr;
		//Set array to manage data in graph
		$result['daily']['reasonCountArr1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArr1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArr1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArr1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArr1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArr1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArr1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArr1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][7]['itemStyle']['color'] = '#d32f2f';
		
		$result['daily']['reasonCountArrDuration1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArrDuration1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArrDuration1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArrDuration1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArrDuration1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArrDuration1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArrDuration1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArrDuration1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][7]['itemStyle']['color'] = '#d32f2f';
		echo json_encode($result); die;  
	}


	//dashboard3_pagination use for get analytics graph data
	public function dashboard3_pagination()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		//Check show all data or specific machine data
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
				$machineNoStacklight = $this->AM->getWhereSingle(array("isDeleted" => '0','machineId' => $machineId), 'machine');
				//print_r($machineNoStacklight);exit;
				$noStacklight = $machineNoStacklight->noStacklight;
			}else
			{
				$noStacklight = "0";
			}
		}

		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');
		//Get machine production state running data as par selecte filter
		$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','running');
		$ActualProductionRunning = $resultActualProductionRunning->countVal;
		//Get machine production state waiting data as par selecte filter
		$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','waiting');
		$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
		//Get machine production state stopped data as par selecte filter
		$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','stopped');
		$ActualProductionStopped = $resultActualProductionStopped->countVal;
		//Get machine production state off data as par selecte filter
		$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','off');
		$ActualProductionOff = $resultActualProductionOff->countVal;
		//Get machine production state nodet data as par selecte filter
		$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','nodet');
		$ActualProductionNodet = $resultActualProductionNodet->countVal;
		//Get machine production state noStacklight data as par selecte filter
		$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','noStacklight');
		$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToDay($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToDay($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToDay($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToDay($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToDay($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToDay($ActualProductionNoStacklight) : 0;
		}

		$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;
		//Get machine setup state running data as par selecte filter
		$resultSetupRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','running');
		$SetupRunning = $resultSetupRunning->countVal;
		//Get machine setup state waiting data as par selecte filter
		$resultSetupWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','waiting');
		$SetupWaiting = $resultSetupWaiting->countVal;
		//Get machine setup state stopped data as par selecte filter
		$resultSetupStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','stopped');
		$SetupStopped = $resultSetupStopped->countVal;
		//Get machine setup state off data as par selecte filter
		$resultSetupOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','off');
		$SetupOff = $resultSetupOff->countVal;
		//Get machine setup state nodet data as par selecte filter
		$resultSetupNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','nodet');
		//Get machine setup state noStacklight data as par selecte filter
		$resultSetupNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','noStacklight');
		$SetupNoStacklight = $resultSetupNoStacklight->countVal;

		$SetupNodet = $resultSetupNodet->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$SetupRunning = !empty($SetupRunning) ? $this->secondsToHours($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ? $this->secondsToHours($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ? $this->secondsToHours($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ? $this->secondsToHours($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ? $this->secondsToHours($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{	
			//Converts seconds to day
			$SetupRunning = !empty($SetupRunning) ?  $this->secondsToDay($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ?  $this->secondsToDay($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ?  $this->secondsToDay($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ?  $this->secondsToDay($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ?  $this->secondsToDay($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}

		$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
		//Get machine no production state running data as par selecte filter
		$resultNoProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','running');
		$NoProductionRunning = $resultNoProductionRunning->countVal;
		//Get machine no production state waiting data as par selecte filter
		$resultNoProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','waiting');
		$NoProductionWaiting = $resultNoProductionWaiting->countVal;
		//Get machine no production state stopped data as par selecte filter
		$resultNoProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','stopped');
		$NoProductionStopped = $resultNoProductionStopped->countVal;
		//Get machine no production state off data as par selecte filter
		$resultNoProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','off');
		$NoProductionOff = $resultNoProductionOff->countVal;
		//Get machine no production state nodet data as par selecte filter
		$resultNoProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','nodet');
		$NoProductionNodet = $resultNoProductionNodet->countVal;
		//Get machine no production state noStacklight data as par selecte filter
		$resultNoProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','noStacklight');
		$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$NoProductionRunning =  !empty($NoProductionRunning) ? $this->secondsToHours($NoProductionRunning) : 0;
			$NoProductionWaiting =  !empty($NoProductionWaiting) ? $this->secondsToHours($NoProductionWaiting) : 0;
			$NoProductionStopped =  !empty($NoProductionStopped) ? $this->secondsToHours($NoProductionStopped) : 0;
			$NoProductionOff =  !empty($NoProductionOff) ? $this->secondsToHours($NoProductionOff) : 0;
			$NoProductionNodet =  !empty($NoProductionNodet) ? $this->secondsToHours($NoProductionNodet) : 0;
			$NoProductionNoStacklight =  !empty($NoProductionNoStacklight) ? $this->secondsToHours($NoProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$NoProductionRunning = !empty($NoProductionRunning) ?  $this->secondsToDay($NoProductionRunning) : 0;
			$NoProductionWaiting = !empty($NoProductionWaiting) ?  $this->secondsToDay($NoProductionWaiting) : 0;
			$NoProductionStopped = !empty($NoProductionStopped) ?  $this->secondsToDay($NoProductionStopped) : 0;
			$NoProductionOff = !empty($NoProductionOff) ?  $this->secondsToDay($NoProductionOff) : 0;
			$NoProductionNodet = !empty($NoProductionNodet) ?  $this->secondsToDay($NoProductionNodet) : 0;
			$NoProductionNoStacklight = !empty($NoProductionNoStacklight) ?  $this->secondsToDay($NoProductionNoStacklight) : 0;
		}

		$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

		$Running = array();
		$Waiting = array();
		$Stopped = array();
		if ($choose1 == "day") 
		{
			$xAxisName = "HOURS";
			$yAxisName = "SECONDS";
			$xAxisData = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];

			for ($i=0; $i < 24; $i++) 
			{ 
				//Get running hour data
				
				$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running');
				$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

				//Get waiting hour data
				$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting');
				$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

				//Get stopped hour data
				$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped');
				$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

				//Get off hour data
				$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off');
				$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

				//Get nodet hour data
				$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet');
				$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

				//Get setup hour data
				$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup');
				$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

				//Get no_producation hour data
				$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation');
				$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

				$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running');
				$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				
			}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23])
			)));
		}
		elseif ($choose1 == "weekly") 
		{
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$xAxisData = array();
        	
			$choose2 = explode("/", $choose2);
        	$week = $choose2[0];
        	$year = $choose2[1];
        
	        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
	        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
	        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
	        $date_for_monday = date( 'Y-m-d', strtotime($date_for_monday ."-7 days") );

	        for ($i=0; $i < 7; $i++) 
	        { 
	        	$weekDate = date('Y-m-d',strtotime($date_for_monday . "+". $i." days"));
	        	$weekDateName = date('d-M-Y',strtotime($date_for_monday . "+". $i." days"));
	        	$xAxisData[] = $weekDateName;
		
				//Get running hour data
	        	$runningQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'running');
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningSecond);
				//Get waiting hour data
				$waitingQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'waiting');
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingSecond);
		
				//Get stopped hour data
				$stoppedQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'stopped');
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedSecond);
		
				//Get off hour data
				$offQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'off');
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffSecond);
		
				//Get nodet hour data
				$nodetQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'nodet');
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetSecond);
		
				//Get setup hour data
				$machineSetupQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'setup');
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupSecond);
		
				//Get no_producation hour data
				$machineNoProducationQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'no_producation');
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getWeekDataActualProducation($machineId,$weekDate,$year,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);
	        }

	        $maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6])
			)));

		}
		elseif ($choose1 == "monthly") 
		{
			$xAxisData = array();
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$chooseExplode = explode(" ", $choose2);
            $month = $chooseExplode[0];

        	if ($month == "Jan") 
        	{
        		$month = "1";
        	}
        	elseif ($month == "Feb") 
        	{
        		$month = "2";
        	}
        	elseif ($month == "Mar") 
        	{
        		$month = "3";
        	}
        	elseif ($month == "Apr") 
        	{
        		$month = "4";
        	}
        	elseif ($month == "May") 
        	{
        		$month = "5";
        	}
        	elseif ($month == "Jun") 
        	{
        		$month = "6";
        	}
        	elseif ($month == "Jul") 
        	{
        		$month = "7";
        	}
        	elseif ($month == "Aug") 
        	{
        		$month = "8";
        	}
        	elseif ($month == "Sep") 
        	{
        		$month = "9";
        	}
        	elseif ($month == "Oct") 
        	{
        		$month = "10";
        	}
        	elseif ($month == "Nov") 
        	{
        		$month = "11";
        	}
        	elseif ($month == "Dec") 
        	{
        		$month = "12";
        	}

            $year = $chooseExplode[1];
			$maxDays=date('t',strtotime('01-'.$month.'-'.$year));
			for($w=1;$w<=$maxDays;$w++) 
			{
				$xAxisData[] = "Day ".$w;
				$runningQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'running');
				$RunningMonthSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningMonthSecond);
				$waitingQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'waiting');
				$WaitingMonthSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingMonthSecond);

				$stoppedQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'stopped');
				$StoppedMonthSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedMonthSecond);

				$offQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'off');
				$OffMonthSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffMonthSecond);

				$nodetQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'nodet');
				$NodetMonthSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetMonthSecond);

				$machineSetupQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'setup');
				$MachineSetupMonthSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupMonthSecond);

				$machineNoProducationQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'no_producation');
				$machineNoProducationMonthSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationMonthSecond);

				$machineActualProducationQuery = $this->AM->getMonthDataActualProducation($machineId,$w,$month,$year,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);
			}
			
			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23]),
				($Running[24] + $Waiting[24] + $Stopped[24] + $Off[24] + $Nodet[24] + $MachineSetup[24] + $MachineNoProducation[24] + $MachineActualProducation[24]),
				($Running[25] + $Waiting[25] + $Stopped[25] + $Off[25] + $Nodet[25] + $MachineSetup[25] + $MachineNoProducation[25] + $MachineActualProducation[25]),
				($Running[26] + $Waiting[26] + $Stopped[26] + $Off[26] + $Nodet[26] + $MachineSetup[26] + $MachineNoProducation[26] + $MachineActualProducation[26]),
				($Running[27] + $Waiting[27] + $Stopped[27] + $Off[27] + $Nodet[27] + $MachineSetup[27] + $MachineNoProducation[27] + $MachineActualProducation[27]),
				($Running[28] + $Waiting[28] + $Stopped[28] + $Off[28] + $Nodet[28] + $MachineSetup[28] + $MachineNoProducation[28] + $MachineActualProducation[28]),
				($Running[29] + $Waiting[29] + $Stopped[29] + $Off[29] + $Nodet[29] + $MachineSetup[29] + $MachineNoProducation[29] + $MachineActualProducation[29]),
				($Running[30] + $Waiting[30] + $Stopped[30] + $Off[30] + $Nodet[30] + $MachineSetup[30] + $MachineNoProducation[30] + $MachineActualProducation[30])
			)));

		}
		elseif ($choose1 == "yearly") 
		{
			$xAxisName = "MONTH";
			$yAxisName = "DAY";
			$xAxisData = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			for ($i=1; $i < 13; $i++) 
			{ 
				$runningQuery = $this->AM->getYearData($machineId,$i,$choose2,'running');
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToDay($RunningSecond);

				$waitingQuery = $this->AM->getYearData($machineId,$i,$choose2,'waiting');
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToDay($WaitingSecond);

				$stoppedQuery = $this->AM->getYearData($machineId,$i,$choose2,'stopped');
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToDay($StoppedSecond);

				$offQuery = $this->AM->getYearData($machineId,$i,$choose2,'off');
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToDay($OffSecond);

				$nodetQuery = $this->AM->getYearData($machineId,$i,$choose2,'nodet');
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToDay($NodetSecond);

				$machineSetupQuery = $this->AM->getYearData($machineId,$i,$choose2,'setup');
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToDay($MachineSetupSecond);

				$machineNoProducationQuery = $this->AM->getYearData($machineId,$i,$choose2,'no_producation');
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToDay($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getYearDataActualProducation($machineId,$i,$choose2,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToDay($machineActualProducationQuerySecond);
			}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11])
			)));
		}

		if (!empty($ActualProduction))
		{
			$ActualProductionLength = intval("-45");
		}
		else
		{
			$ActualProductionLength = intval("-20");
		}
		if (!empty($Setup))
		{
			$SetupLength = intval("-45");
		}
		else
		{
			$SetupLength = intval("-20");
		}
		if (!empty($NoProduction))
		{
			$NoProductionLength = intval("-45");
		}
		else
		{
			$NoProductionLength = intval("-20");
		}

		$json = array(
				"ActualProductionRunning" => !empty($ActualProductionRunning) ? $ActualProductionRunning : "0",
				"ActualProductionWaiting" => !empty($ActualProductionWaiting) ? $ActualProductionWaiting : "0",
				"ActualProductionStopped" => !empty($ActualProductionStopped) ? $ActualProductionStopped : "0",
				"ActualProductionOff" => !empty($ActualProductionOff) ? $ActualProductionOff : "0",
				"ActualProductionNodet" => !empty($ActualProductionNodet) ? $ActualProductionNodet : "0",
				"ActualProductionNoStacklight" => !empty($ActualProductionNoStacklight) ? $ActualProductionNoStacklight : "0",
				"ActualProduction" => !empty($ActualProduction) ? $ActualProduction : "0",
				"ActualProductionLength" => $ActualProductionLength,
				"SetupRunning" => !empty($SetupRunning) ? $SetupRunning : "0",
				"SetupWaiting" => !empty($SetupWaiting) ? $SetupWaiting : "0",
				"SetupStopped" => !empty($SetupStopped) ? $SetupStopped : "0",
				"SetupOff" => !empty($SetupOff) ? $SetupOff : "0",
				"SetupNodet" => !empty($SetupNodet) ? $SetupNodet : "0",
				"Setup" => !empty($Setup) ? $Setup : "0",
				"SetupLength" => $SetupLength,
				"NoProductionRunning" => !empty($NoProductionRunning) ? $NoProductionRunning : "0",
				"NoProductionWaiting" => !empty($NoProductionWaiting) ? $NoProductionWaiting : "0",
				"NoProductionStopped" => !empty($NoProductionStopped) ? $NoProductionStopped : "0",
				"NoProductionOff" => !empty($NoProductionOff) ? $NoProductionOff : "0",
				"NoProductionNodet" => !empty($NoProductionNodet) ? $NoProductionNodet : "0",
				"NoProduction" => !empty($NoProduction) ? $NoProduction : "0",
				"NoProductionLength" => $NoProductionLength,
				"xAxisName" => $xAxisName,
				"yAxisName" => $yAxisName,
				"xAxisData" => $xAxisData,
				"Running" => $Running,
				"Waiting" => $Waiting,
				"Stopped" => $Stopped,
				"Off" => $Off,
				"Nodet" => $Nodet,
				"MachineSetup" => $MachineSetup,
				"MachineNoProducation" => $MachineNoProducation,
				"MachineActualProducation" => $MachineActualProducation,
				"maxValue" => $maxValue,
				"noStacklight" => $noStacklight,
				"machineId" => $machineId,
				"NonProductiveTime" => $Setup + $NoProduction
			);
		echo json_encode($json);
	}

	function getLiveDashboard3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$post = $this->input->post();
		$ch = curl_init(LIVE_ENV_URL.'admin/dashboard3_paginationLiveTest');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);

		echo $response;
	}

	public function live_dashboard3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		/*$listMachines = array();
		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init(LIVE_ENV_URL.'admin/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);*/

		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$machineView = $this->AM->getWhere(array("isDelete" => "0"), "machineView");

		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachines($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
		}
		
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData,
			'machineView' => $machineView 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_dashboard3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_dashboard3_footer');
	}
	
	public function live_breakdown3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init(LIVE_ENV_URL.'admin/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$ch = curl_init(LIVE_ENV_URL.'testadmin/factoryData');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$liveFactoryData = json_decode($response);

		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines' => $listMachines,
			'factoryData' => $factoryData,
			'liveFactoryData' => $liveFactoryData
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_breakdown3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_breakdown3_footer');
	}

	function getLiveBreakdown3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$post = $this->input->post();
		$ch = curl_init(LIVE_ENV_URL.'testadmin/breakdown3_paginationLiveTest');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);

		echo $response;
	}

	//In use
	public function configure_stacklighttype() 
	{
		//echo date('Y-m-d H:i:s');exit;
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(15);
		if ($accessPoint == true) 
        {		
			$listTypes = $this->AM->getAllStackLightTypes($this->factoryId); 
			$listTypes = $listTypes->result();
			foreach ($listTypes as $key => $value) 
			{
				$listTypes[$key]->versions = $this->AM->getAllStackLightVersion($value->stackLightTypeId);
			}
			$listMachines = $this->AM->getallMachines($this->factory);
			$data = array(
			    'listTypes'=>$listTypes,
				'listMachines'=>$listMachines
			    );
			$data['stacklighttypeList'] = $this->AM->accessPoint(16);
			$data['stacklighttypeListEdit'] = $this->AM->accessPointEdit(16);
			$data['VersionList'] = $this->AM->accessPoint(17);

			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('configure_stacklighttype', $data);
			$this->load->view('footer');
			$this->load->view('script_file/configure_stacklighttype_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	//add_stacklighttype function use for add new stacklight
	//In use
	public function add_stacklighttype() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		//check required field validation and check stacklighttype name already added or not
		$this->form_validation->set_rules('stackLightTypeName','Name','trim|required|callback_check_stacklighttype_name');
        if($this->form_validation->run()==false) 
        { 
        	//return message if required field null
            if(form_error('stackLightTypeName') )
            { 
                echo form_error('stackLightTypeName'); die; 
            } 
            else 
            {
                echo Pleasefillvaliddetailsforstacklighttype; die;
            }
        } 
        else 
        { 
			$newId=$this->AM->lastStackLightTypeId()->maxId + 1;
            $nameext = explode(".", $_FILES['stackLightTypeImage']['name']); 
            $ext = $nameext[1];
            $config['upload_path'] =  './assets/img/stackLightType/';
            $config['allowed_types'] = 'jpg|jpeg|png';
            $config['max_size']  = '5000';
            $config['file_name'] = $newId.".".$ext;
            
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('stackLightTypeImage')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            $insert=array(
				'stackLightTypeName'=>$this->input->post('stackLightTypeName'),
				'stackLightTypeImage'=>$config['file_name'],
				'stackLightTypeVersion' => 1
			);
			
			$stackLightTypeId=$this->AM->newStackLightType($insert);
			$stacklighttypefiledata = array(
				"stackLightTypeId" => $stackLightTypeId,
				"versionName" => 1
			);
			$this->db->insert("stackLightTypeFile",$stacklighttypefiledata);
			$stacklightFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId;
			if (!file_exists($stacklightFilePath)) 
			{
			   mkdir($stacklightFilePath, 0777, true);
			}
			$versionFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId.'/1';
			if (!file_exists($versionFilePath)) 
			{
			   mkdir($versionFilePath, 0777, true);
			}
			$config1['upload_path'] = $versionFilePath;
            $config1['allowed_types'] = '*';

            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);

            $this->upload->do_upload('classification_cfg');
            $this->upload->do_upload('classification_weights');
            $this->upload->do_upload('detection_cfg');
            $this->upload->do_upload('detection_weights');
            $this->upload->do_upload('label_txt');
            $this->upload->do_upload('labels_txt');

			echo Newstacklighttypeaddedsuccessfully; die;
		}
	}

	//edit_stacklighttype function use for update new stacklight
	public function edit_stacklighttype() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//check required field validation and check stacklighttype name already added or not
		$this->form_validation->set_rules('stackLightTypeName','Name','trim|required|callback_check_stacklighttype_name');
        if($this->form_validation->run()==false) 
        { 
            if(form_error('stackLightTypeName') ) 
            { 
                echo form_error('stackLightTypeName'); die;
            } 
            else 
            {
                echo Pleasefillvaliddetailsforstacklighttype; die;
            }
        } 
        else 
        { 
            $stackLightTypeId = $this->input->post('stackLightTypeId');
            $stackLightTypeVersion = $this->input->post('stackLightTypeVersion');
            $stackLightType = $this->AM->getWhereDBSingle(array("stackLightTypeId" => $stackLightTypeId),"stackLightType");

            $machine = $this->AM->getWhere(array("stackLightTypeId" => $stackLightTypeId),"machine");
            $machineIds = !empty($machine) ? array_column($machine, "machineId") : array();
            if ($stackLightType->stackLightTypeVersion == $stackLightTypeVersion) 
            {
            	$updateStatus = 0;
            }else
            {
            	$updateStatus = 1;
            }

			$update=array(
				'stackLightTypeName'=>$this->input->post('stackLightTypeName'),
				'stackLightTypeVersion'=> $stackLightTypeVersion,
			); 
			
			if(!empty($_FILES['stackLightTypeImage']['name'])) 
			{
                $nameext = explode(".", $_FILES['stackLightTypeImage']['name']); 
	            $ext = $nameext[1];
                $config['overwrite'] = TRUE;
                $config['upload_path'] = './assets/img/stackLightType/'; 
                $config['allowed_types'] = 'jpg|jpeg|png';
                $config['max_size']  = '5000';
                $config['file_name'] = $stackLightTypeId.".".$ext;  
                $update['stackLightTypeImage'] = $config['file_name'];
                $this->load->library('upload', $config);
                $this->upload->initialize($config);
                if (!$this->upload->do_upload('stackLightTypeImage')) 
                {
                    $response = array("status" => 1,"updateStatus" => 0,"message" => "<p>Please upload valid picture.(jpg,jpeg and png only, max 5 MB)</p>");
           			echo json_encode($response);
                    die;
                }
            }
           $color=$this->AM->updateStackLightType($update, $stackLightTypeId);

           $response = array("status" => 1,"factoryId" => $this->factoryId,"machineIds" => $machineIds,"updateStatus" => $updateStatus,'machineStackLightType' => $stackLightTypeId, 'stackLightTypeVersion' => $stackLightTypeVersion,"message" => StackLightTypeupdatedsuccessfully);
           echo json_encode($response);
           die;
        }
	}

	//delete_stacklighttype function use for delete stacklight
	//In use
	public function delete_stacklighttype() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$stackLightTypeId = $this->input->post('deleteStackLightTypeId'); 
		$this->AM->deleteStackLightType($stackLightTypeId); 
		echo StackLightTypedeletedsuccessfully; die;
	}

	//app_versions function use for app versions list page

	//In use
	public function app_versions() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(18);
		if ($accessPoint == true) 
        {
			$data['accessPointEdit'] = $this->AM->accessPointEdit(18);
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('app_versions',$data);
			$this->load->view('footer');
			$this->load->view('script_file/app_version_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}


	//app_versions_pagination function use for get all added app version update details

	//In use
	public function app_versions_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$listMachines = array();
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column'];
		$columnName = $_POST['columns'][$columnIndex]['data']; 
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		$searchQuery = " ";
		if($searchValue != '')
		{
		   $searchQuery = " and (appVersions.app_version_id like '%".$searchValue."%' or 
				appVersions.app_version_name like '%".$searchValue."%' or
				appVersions.app_version_code like '%".$searchValue."%' or
				appVersions.created_date like '%".$searchValue."%'
				) ";  
		}

		//Get all data count
		$listColorLogsCount = $this->AM->getAllAppVersionCount($this->factory)->row()->totalCount;
		//Get count by selected filter
		$listColorLogsSearchCount = $this->AM->getAppVersionCount($this->factory,$searchQuery)->row()->totalCount;
		//Get all data
		$listColorLogs = $this->AM->getallAppVersion($this->factory, $row, $rowperpage, $searchQuery, $columnName, $columnSortOrder)->result_array();  
		foreach ($listColorLogs as $key => $value) 
		{
			$listColorLogs[$key]['app_version_file'] = "<a style='color:#002060' download href='".base_url('../app_versions/').$value['app_version_file']."'>". $value['app_version_file'] ."</a>";
			$listColorLogs[$key]['update_file'] = '<a href="javascript:void(0);" onclick="updateAppVersion('. $value['app_version_id'] .')" class="btn btn-primary" data-toggle="modal" >'.Update.'</a>';

		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		echo json_encode($response); die;
	}

	//add_appVersion function use for add new app version

	//In use
	public function add_appVersion() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$this->form_validation->set_rules('versionName','versionName','required');
		$this->form_validation->set_rules('versionCode','versionCode','required');

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo "<p>Please fill valid details for stack light type.</p>"; die;
        } 
        else 
        { 
        	$upload_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES;
            $config['upload_path'] =  $upload_path;
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            //Check file type and file uploaded or not
            if (!$this->upload->do_upload('versionFile')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            else
            {
				$app_versions = array(
					"app_version_name" => $this->input->post('versionName'),
					"app_version_code" => $this->input->post('versionCode'),
					"app_version_file" => $this->upload->data('file_name'),
					"app_type" => "setApp"
				);
				$this->db->insert("appVersions",$app_versions);
				//Add new app version
            	$upload_file = $upload_path.'/'.$this->upload->data('file_name');
            	chmod($upload_file,0777);
				$result = $this->AM->getWhereDBWithGroupBY(array("userRole" => "0","deviceToken != " => ""),"deviceToken","user");
				//Send notification to all operator for new version
				$fcmToken = array_column($result, "deviceToken");
				$type = "setApp";
				$message = $type."! New update available! Install now.";
				$title = "Update app";
				$data = array
		            (
		                'message'   => $message,
		                'title'     => $title,
		                'vibrate'   => 1,
		                'sound'     => 1
		            );
		        $fields = array
		            (
		                'registration_ids'  => $fcmToken,  
		                'data' => $data
		            );

		        $headers = array
		            (
		                'Authorization: key=' . API_ACCESS_KEY,
		                'Content-Type: application/json'
		            );

		        $ch = curl_init();
		        curl_setopt( $ch,CURLOPT_URL, 'https://fcm.googleapis.com/fcm/send' );
		        curl_setopt( $ch,CURLOPT_POST, true );
		        curl_setopt( $ch,CURLOPT_HTTPHEADER, $headers );
		        curl_setopt( $ch,CURLOPT_RETURNTRANSFER, 1 );
		        curl_setopt( $ch,CURLOPT_SSL_VERIFYPEER, true );
		        curl_setopt( $ch,CURLOPT_POSTFIELDS, json_encode( $fields ) );
		        $result = curl_exec($ch);
		        curl_close($ch);

				echo "New version for app added successfully."; die;
            }
		}
	}

	//update_appVersion function use for update version file

	//In use
	public function update_appVersion() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('app_version_id','app_version_id','required');
		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            echo "<p>Please fill valid details for stack light type.</p>"; die;
        } 
        else 
        { 
			$app_version_id = $this->input->post('app_version_id');
			$result = $this->AM->getWhereDBSingle(array("app_version_id" => $app_version_id),"appVersions");
			$unlink_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES.'/'.$result->app_version_file;
			//Delete previous file 
        	unlink($unlink_path);
			$upload_path = $_SERVER['DOCUMENT_ROOT'].APP_VERSIONS_FILES;
            $config['upload_path'] =  $upload_path;
            $config['allowed_types'] = '*';
            $this->load->library('upload', $config);
            $this->upload->initialize($config);
            if (!$this->upload->do_upload('versionFile')) 
            {
            	echo $this->upload->display_errors();exit;
            }
            else
            {
            	$app_versions = array(
					"app_version_file" => $this->upload->data('file_name')
				);
				$this->db->where('app_version_id',$app_version_id)->update("appVersions",$app_versions);
		    	$upload_file = $upload_path.'/'.$this->upload->data('file_name');
				chmod($upload_file,0777);
            	echo "Version file updated successfully."; die;
            }
		}
	}


	//reportProblem function use for reported note page 
	function reportProblem()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$accessPoint = $this->AM->accessPoint(19);
		if ($accessPoint == true) 
        {
			$data['accessPointEdit'] = $this->AM->accessPointEdit(19);
			$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('reportProblem',$data);
			$this->load->view('footer');		
			$this->load->view('script_file/report_problem_footer');	
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}	
	}

	//reportProblem_pagination function use for get report problem data 
	public function reportProblem_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$userName = $this->input->post('userName');
		$type = $this->input->post('type');
		$status = $this->input->post('status');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$isActive = $this->input->post('isActive');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		//Get all data count
		$listDataLogsCount = $this->AM->getalloperatordetailslistCount($this->factory, $problemId, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchoperatordetailsProblemCount($this->factory, $problemId, $is_admin,$status,$type,$userName,$dateValS,$dateValE,$isActive)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getallReportProblemLogs($this->factory, $problemId, $is_admin, $row, $rowperpage,$status,$type,$userName,$dateValS,$dateValE,$isActive)->result_array();  

		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['userName'] = '<img style="border-radius: 50%;" width="16" height="16" src="'. base_url('assets/img/user/'.$value['userImage']) .'">'.$value['userName'];
			if ($value['type'] == "1") 
        	{
        		$type = Accident;
        	}elseif ($value['type'] == "0")
        	{
        		$type = Incident;
        	}else
        	{
        		$type = Suggestion;
        	}
			$listDataLogs[$key]['type'] = $type;
			$listDataLogs[$key]['isActive'] = ($value['isActive'] == "1") ? '<img width="16" height="16" src="'. base_url('assets/img/tick.svg') .'">' : "";
			$listDataLogs[$key]['picture'] = (!empty($value['picture'])) ? "Yes" : "No";
			$listDataLogs[$key]['status'] = ($value['status'] == "1") ? '<img width="16" height="16" src="'. base_url('assets/img/tick.svg') .'">' : '<img width="16" height="16" src="'. base_url('assets/img/cross.svg') .'">';
			$descriptionCount = strlen($value['description']);
			$causeCount = strlen($value['cause']);
			$improvementCount = strlen($value['improvement']);

			$listDataLogs[$key]['description'] = ($descriptionCount > 40) ? substr($value['description'],0,40).'...' : $value['description'];			
			$listDataLogs[$key]['cause'] = ($causeCount > 40) ? substr($value['cause'],0,40).'...' : $value['cause'];			
			$listDataLogs[$key]['improvement'] = ($improvementCount > 40) ? substr($value['improvement'],0,40).'...' : $value['improvement'];			

		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);

		echo json_encode($response); die;
		
	}

	//update_reportProblem use for update report problem data 
	public function update_reportProblem() 
	{	
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('problemId','problemId','required'); ; 
		$this->form_validation->set_rules('type','type','required'); ; 

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo Pleasefillvaliddetailsformachine; die;
        } 
        else 
        { 
        	$problemId = $this->input->post('problemId');
			$type = $this->input->post('type');
			$description = $this->input->post('description');
			$cause = $this->input->post('cause');
			$improvement = $this->input->post('improvement');
			$isActive = $this->input->post('isActive');

            $update = array(
                'type' => $type,
                'isActive' => ($isActive == "1") ? "1" : "0",
                'description' => !empty($description) ? $description : "",
                'cause' => !empty($cause) ? $cause : "",
                'improvement' => !empty($improvement) ? $improvement : ""
            );  
            //Update report problem data
		    $this->AM->updateReportProblem($this->factory, $problemId, $update);
		    echo "1"; die; 
        }
	}

	//getReportProblemById function use for get report problem data by id
	function getReportProblemById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}

		$problemId = $this->input->post('problemId');
		//Get report problem data by id

		$update = array(
                'isSeen' => "1"
            );  
        //Update report problem data
	    $this->AM->updateReportProblem($this->factory, $problemId, $update);

		$result = $this->AM->getReportProblemById($this->factory,$problemId);
		if (!empty($result)) 
		{
			$result->reportedNotesCount = $this->AM->getWhereCount(array("isSeen" => "0"),"reportProblem");
		}
		echo json_encode($result); die;
	}

	//edit_profile fuction use for get user profile data update

	//In use
	public function edit_profile() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->input->post()) 
		{ 
		    $this->edit_user(1); 
		} 
		$userId = $this->session->userdata('userId');
		$data['users'] = $this->AM->getWhereDBSingle(array("userId" => $userId),'user');
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('edit_profile',$data); 
		$this->load->view('footer');
		$this->load->view('script_file/edit_profile_footer');
	}
	
	//check_current_password function use for get user current password for password validation

	//In use
	public function check_current_password($pass) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if(!$this->AM->isValidCurrentPassword($pass, $this->input->post('userId'))) 
		{
            $this->form_validation->set_message('check_current_password',Pleaseentervalidcurrentpassword); 
            return false;  
        } 
        else 
        {
            return true;
        }   
    }
	
	//check_new_password function use for check password and confirm password

	//In use
	public function check_new_password($pass) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        if($pass != $this->input->post('userNewPassword')) 
        {
            $this->form_validation->set_message('check_new_password',ConfirmPasswordmustbesameasnewpassword);
            return false;  
        } 
        else 
        {
            return true;
        }   
    }
	
	//change_password function use for set new password
	public function change_password() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->input->post()) 
		{ 
		    $this->form_validation->set_rules('userCurrentPassword','Current password','trim|required|xss_clean|callback_check_current_password');
		    $this->form_validation->set_rules('userNewPassword','New password','trim|required|xss_clean');
		    $this->form_validation->set_rules('userConfirmPassword','Confirm new password','trim|required|xss_clean|callback_check_new_password'); 
    		
    		//checking required parameters validation
    	    if($this->form_validation->run()==false) 
    	    { 
                if(form_error('userCurrentPassword') ) { echo form_error('userCurrentPassword'); }
                if(form_error('userNewPassword') ) { echo form_error('userNewPassword'); }
                if(form_error('userConfirmPassword') ) { echo form_error('userConfirmPassword'); } die;
            } 
            else 
            { 
                $userId=$this->input->post('userId');
                $update=array(
                    'userPassword'=> base64_encode($this->input->post('userNewPassword')),
                );
                //Set new password as base64 encode format
                $user=$this->AM->updateuser($update, $userId);
                echo "1"; 
                die; 
            }
		} 
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('change_password'); 
		$this->load->view('footer');
	}

	//password_check function use for password validation string
	public function password_check($str)
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

	   if (preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str) && preg_match('#[A-Z]#', $str) && preg_match('#@#', $str)) 
	   {
	     return TRUE;
	   }
	   $this->form_validation->set_message('password_check',Yourpasswordisnotmatchingourcriteriamakesureyouenter1capitalletter1smallletter1numberandsigninpassword);
	   return FALSE;
	}

	//update_profile use for update operator details

	//In use

 	public function update_profile() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		    
	    $this->form_validation->set_rules('userName','User name','required');
	    $this->form_validation->set_rules('userId','User id','required');
	    $currentPassword = $this->input->post('currentPassword');
	    $userNewPassword = $this->input->post('userNewPassword');
	    $userConfirmPassword = $this->input->post('userConfirmPassword');
	    if (!empty($currentPassword) || !empty($userNewPassword) || !empty($userConfirmPassword)) 
	    {
	    	$this->form_validation->set_rules('currentPassword','Current password','required|min_length[6]|max_length[16]|callback_check_current_password');
		    $this->form_validation->set_rules('userNewPassword','new password','required|min_length[6]|max_length[16]|callback_password_check');
		    $this->form_validation->set_rules('userConfirmPassword','confirm password','required|min_length[6]|max_length[16]|callback_check_new_password'); 
		    $this->form_validation->set_message('min_length', 'The value you have entered for %s is too short');
			$this->form_validation->set_message('max_length', 'The value you have entered for %s is too long');
	    }
	    
	    //checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
        	if (!empty($currentPassword) || !empty($userNewPassword) || !empty($userConfirmPassword)) 
		    {
	            if(form_error('currentPassword') ) { echo form_error('currentPassword');die; }
	            if(form_error('userNewPassword') ) { echo form_error('userNewPassword');die; }
	            if(form_error('userConfirmPassword') ) { echo form_error('userConfirmPassword');die; } 
	        }
	        else
	        {
	        	echo "Please fill valid details.";die;
	        }
        } 
        else 
        { 
		    $userId = $this->input->post('userId');
            $userName = $this->input->post('userName');
            $userPassword = $this->input->post('userNewPassword');
            $olduserName = $this->input->post('olduserName');
		    $return = "1";
            if ($userName == $olduserName) 
            {
            	$return = "2";
            }

            if(!empty($_FILES['userImage']['name'])) 
            {  
     		    $nameext = explode(".", $_FILES['userImage']['name']);
	            $ext = end($nameext);
	            $config['overwrite'] = TRUE;
	            $config['upload_path'] = realpath(APPPATH . '../assets/img/user/'); 
	            $config['allowed_types'] = 'png|jpg|jpeg';
	            $config['max_size']  = '5000';
	            $config['file_name'] = $userId.".".$ext; 
				
	            $update['userImage'] = $config['file_name'];
	            
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);
	            
	            if (!$this->upload->do_upload('userImage')) 
	            {
	                echo "<p>Please upload valid picture.(jpg,png and jpeg  only, max 5 MB)</p>"; die;
	            }
				$data['userImage'] = $config['file_name'];
	            $return = "1";
	        }
			$update['userName'] = $userName;
            if (!empty($userPassword)) 
            {
            	$update['userPassword'] =  base64_encode($userPassword);
            	$return = "1";
            }

            //Update user details
            $this->AM->updateuser($update, $userId);
			$data['userName'] = $userName;
            $this->session->set_userdata($data);
            echo $return;die; 
        }
	}

	//help_report function use help report list page

	//In use 
	public function help_report() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('help_report');
		$this->load->view('footer');
		$this->load->view('script_file/help_report_footer');
	}

	function testMail()
	{
		$url = "";
		$this->AM->sendEmail("Test mail check","test mail new",$url);
	}

	//add_helpReport function use for add help report and send mail to admin

	//In use
	public function add_helpReport() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('problemText','problemText','required');

		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            echo Pleasefillvaliddetailsformachine ; die;
        } 
        else 
        { 
			$factoryId = $this->session->userdata('factoryId');
			$problemText = $this->input->post('problemText');
			$currentTime = time();
			$userId = $this->session->userdata('userId');
			$factory = $this->AM->getFactoryData($factoryId);

			$msg = "Factory - " . base64_decode($factory->factoryName) . "<br>Following message was reported by dashboard user : ". $problemText ;
			$subject = "Dashboard operator reports a problem";
			
			if(!empty($_FILES['image']['name'])) 
			{ 
				$allowed =  array('jpeg','png' ,'jpg','mov','mp4','pdf','txt','doc','docx','JPEG','PNG' ,'JPG','MOV','MP4','PDF','TXT','DOC','DOCX');
				$ext = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
				if(!in_array($ext,$allowed)) 
				{	
					echo 'Please upload valid image, video or document.';exit; 
				}
				else
				{
					$path_parts = pathinfo($_FILES["image"]["name"]);
					$file_ext = $path_parts['extension'];
					move_uploaded_file($_FILES['image']['tmp_name'], "../report/factory".$factoryId."/" .  $currentTime. "." .$file_ext);
					$insert = array(
			                    "problemText"=>$problemText,
			                    "userId" => $userId,
			                    "factoryId" => $factoryId,
			                    "currentTime" => date('Y-m-d H:i:s',$currentTime) ,
			                    "image" => $userId ."_".$currentTime.".".$file_ext
			                    );

					$this->AM->addHelpReport($insert);
					
					$file_path =  ENV_URL."report/factory".$factoryId.'/' . $currentTime. '.' .$file_ext;
					

					$this->AM->sendEmail($msg,$subject,$file_path,$file_ext);
					echo "success"; die; 

				} 
			} 
			else 
			{
				$insert = array(
			                    "problemText"=>$problemText,
			                    "userId" => $userId,
			                    "factoryId" => $factoryId,
			                    "currentTime" => date('Y-m-d H:i:s',$currentTime) 
			                    );
				$this->AM->addHelpReport($insert);
				//Send mail detail for help report
				$this->AM->sendEmail($msg,$subject,"","");
				echo "success"; die; 
			}
		}
	}

	
	//Not in use
    public function maintenance_checkin() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$listMachines = $this->AM->getallMachines($this->factory);  
		$listOperators = $this->AM->getallOperators($this->factoryId);
		$data = array(
		    'listMachines'=>$listMachines,
			'listOperators'=>$listOperators, 
		    );  
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('maintenance_checkin', $data);
		$this->load->view('footer');
		$this->load->view('script_file/maintenance_checkin_footer');
	}
	
	//Not in use
	public function maintenance_checkin_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$userId = 0; 
		if($this->input->post('userId')) 
		{ 
			if($this->input->post('userId') > 0 ) 
			{
				$userId = $this->input->post('userId');
			}
		}
		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		} 
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value'];
		
		$varible = false;
		if ($searchValue == "Checked in") 
		{
			$searchValue = "1";
			$varible = true;
		}
		elseif ($searchValue == "Checked out") 
		{
			$searchValue = "0";
			$varible = true;
		}
		$searchQuery = " ";
		if($searchValue != '')
		{
			if ($varible == true) 
			{
				$searchQuery = " and (machineUserv2.activeId like '%".$searchValue."%' or
					machineUserv2.userId like '%".$searchValue."%' or 
					user.userName like '%".$searchValue."%' or 	
					machineUserv2.machineNames like '%".$searchValue."%' or 
					machineUserv2.startTime like '%".$searchValue."%' or 
					machineUserv2.endTime like'%".$searchValue."%'
					) and machineUserv2.isActive = '".$searchValue."'"; 
			}
			else
			{
				$searchQuery = " and (machineUserv2.activeId like '%".$searchValue."%' or
					machineUserv2.userId like '%".$searchValue."%' or 
					user.userName like '%".$searchValue."%' or 	
					machineUserv2.machineNames like '%".$searchValue."%' or 
					machineUserv2.startTime like '%".$searchValue."%' or 
					machineUserv2.endTime like'%".$searchValue."%'
					) "; 
			}
		     
		}

		$listColorLogsCount = $this->AM->getAllMaintenanceCheckinCount($this->factory, $userId, $is_admin)->num_rows();
		$listColorLogsSearchCount = $this->AM->getSearchMaintenanceCheckinCount($this->factory, $dateValS, $dateValE, $userId, $is_admin, $searchQuery)->num_rows();
		$listColorLogs = $this->AM->getMaintenanceCheckin($this->factory, $dateValS, $dateValE, $userId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();
		for($i=0;$i<count($listColorLogs);$i++) 
		{  
			
			if($listColorLogs[$i]->isActive == '1') 
			{
				$listColorLogs[$i]->isActive = 'Checked in';
				$listColorLogs[$i]->endTime = ''; 
			} 
			else if($listColorLogs[$i]->isActive == '0') 
			{
				$listColorLogs[$i]->isActive = 'Checked out';
			}
			if($listColorLogs[$i]->status == '0' || $listColorLogs[$i]->status == '2') 
			{
				$listColorLogs[$i]->status = '<i class="fa fa-times" aria-hidden="true"></i>';
			} 
			else if($listColorLogs[$i]->status == '1') 
			{
				$listColorLogs[$i]->status = '<i class="fa fa-check" aria-hidden="true"></i>';
			} 
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		echo json_encode($response); die;
	}

	//getallOperatorsById function use for get operator check in and check out time and operator details

	//In use
	function getallOperatorsById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$activeId = $this->input->post('activeId');
		//Get operator check in and check out time and operator details
		$result = $this->AM->getallOperatorsById($this->factory,$activeId);
		if (!empty($result)) 
		{
			$result->reportCount = $this->AM->getWhereCount(array("createdDate >=" => $result->startTime,"createdDate <=" => $result->endTime),"reportProblem");
			$result->startDate = date("Y-m-d", strtotime($result->startTime));
			$result->startTime = date("H:i:s", strtotime($result->startTime));
			$users = $this->AM->getWhereDBSingle(array("userId" => $result->userId),"user");
			$result->userName = $users->userName;
			$result->userImage = $users->userImage;
			if ($result->isActive == "0") 
			{
				$result->endDate = date("Y-m-d", strtotime($result->endTime));
				$result->endTime = date("H:i:s", strtotime($result->endTime));
			}
			else
			{
				$result->endDate = "---";
				$result->endTime = "---";
			}
		}
		echo json_encode($result); die;
	}
	
	//configure_versions function use for get stacklight version file details list

	//In use
	public function configure_versions() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(17);
		if ($accessPoint == true) 
        {
			$listStackLightTypes = $this->AM->getallStackLightType();
			$data = array(
			    			'listStackLightTypes'=>$listStackLightTypes
			   			 );
		$data['accessPointEdit'] = $this->AM->accessPointEdit(17);
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('configure_versions', $data);
			$this->load->view('footer');
			$this->load->view('script_file/configure_versions_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}
	
	//configure_versions_pagination function use for get stacklight version file details list as par stacklight

	//In use
	public function configure_versions_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$stackLightTypeId = 0; 
		if($this->input->post('stackLightTypeId')) 
		{ 
			if($this->input->post('stackLightTypeId') > 0 ) 
			{
				$stackLightTypeId = $this->input->post('stackLightTypeId');
			}
		} 

		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 365 days'))." 00:00:00";
		}
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		} 
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column'];
		$columnName = $_POST['columns'][$columnIndex]['data']; 
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		if ($columnName == "fileName") 
		{
			$columnName = "versionId";
		}
		$searchQuery = " ";
		if($searchValue != '')
		{
		   $searchQuery = " and (stackLightTypeFile.versionName like '%".$searchValue."%' or 
				stackLightType.stackLightTypeId like '%".$searchValue."%' or
				stackLightTypeFile.versionId like '%".$searchValue."%' or
				stackLightType.stackLightTypeName like '%".$searchValue."%' or
				stackLightTypeFile.versionDate like '%".$searchValue."%'
				) ";  
		}

		//Get all data count
		$listColorLogsCount = $this->AM->getallStacklighttypeVersionsCount($this->factory, $stackLightTypeId, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listColorLogsSearchCount = $this->AM->getSearchStacklighttypeVersionsCount($this->factory, $dateValS, $dateValE, $stackLightTypeId, $is_admin, $searchQuery)->row()->totalCount;
		//Get all data
		$listColorLogs = $this->AM->getallStacklighttypeVersionsLogs($this->factory, $dateValS, $dateValE, $stackLightTypeId, $is_admin, $row, $rowperpage, $searchQuery, $columnName, $columnSortOrder)->result_array();  

		foreach ($listColorLogs as $key => $value) 
		{
			$stackLightTypeId = $value['stackLightTypeId'];
			$versionName = $value['versionName'];
			$listColorLogs[$key]['fileName'] = 
				"
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/classification.cfg')."'>classification.cfg</a><br>
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/classification.weights')."'>classification.weights</a><br>
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/detection.cfg')."'>detection.cfg</a><br>
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/detection.weights')."'>detection.weights</a><br>
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/label.txt')."'>label.txt</a><br>
					<a style='color:#002060;' download href='".base_url('../detectionfiles/'.$stackLightTypeId.'/'.$versionName.'/labels.txt')."'>labels.txt</a>
				";
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		echo json_encode($response); die;
	}

	//getstackLightTypeVersion function use for get next stacklight version

	//In use
	public function getstackLightTypeVersion()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$stackLightTypeId = trim($this->input->post('stackLightTypeId'));
		//Get new stacklight version
		$result = $this->AM->getMaxVersion($stackLightTypeId);
		echo trim($result + 1);
	}

	//add_stackLightTypeVersion function use for add new stacklight version 

	//In use
	public function add_stackLightTypeVersion() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$this->form_validation->set_rules('stackLightTypeId','Name','trim|required');

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            if(form_error('stackLightTypeName') ) 
            { 
                echo form_error('stackLightTypeName'); die; 
            } 
            else 
            {
                echo "<p>Please fill valid details for stack light type.</p>"; die;
            }
        } 
        else 
        { 
			$stackLightTypeId = $this->input->post('stackLightTypeId');
			$versionName = $this->input->post('versionName');
			$stacklighttypefiledata = array(
				"stackLightTypeId" => $stackLightTypeId,
				"versionName" => $versionName
			);
			//Add new stacklight version 
			$this->db->insert("stackLightTypeFile",$stacklighttypefiledata);
			$stacklightFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId;
			if (!file_exists($stacklightFilePath)) 
			{
			   mkdir($stacklightFilePath, 0777, true);
			}
			$versionFilePath = $_SERVER['DOCUMENT_ROOT'].DETECTION_FILES.$stackLightTypeId.'/'.$versionName;
			if (!file_exists($versionFilePath)) 
			{
			   mkdir($versionFilePath, 0777, true);
			}

			$config1['upload_path'] = $versionFilePath;
            $config1['allowed_types'] = '*';

            $this->load->library('upload', $config1);
            $this->upload->initialize($config1);

            $this->upload->do_upload('classification_cfg');

            if (!$this->upload->do_upload('classification_cfg')) 
            {
				echo $this->upload->display_errors();  die;
            }

            //Upload new stacklight version files 
			$this->upload->do_upload('classification_weights');
            $this->upload->do_upload('detection_cfg');
            $this->upload->do_upload('detection_weights');
            $this->upload->do_upload('label_txt');
            $this->upload->do_upload('labels_txt');
			echo NewversionforstacklighttypeaddedsuccessfullyPleaseupdateitinyourstacklighttypesettingtoseethenewdetectionresults; die;
		}
	}
	
	//In use
	function getconfigureVersionById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$stackLightTypeId = $this->input->post('stackLightTypeId');
		$result = $this->AM->getconfigureVersionsById($this->factory,$stackLightTypeId);
		echo json_encode($result); die;
	}

	//secondsToDay function use for convert seconds to day 

	//Not in use
	function secondsToDay($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to day
		return sprintf('%0.5f', ($seconds) / (3600 * 24));
	}
	
	//secondsToDay function use for convert seconds to hour

	//Not in use
	function secondsToHours($seconds) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert seconds to hour
		return sprintf('%0.5f', $seconds/3600);
    }

    //Not in use
	public function getDays()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$month = $this->input->post('month');
		$year = $this->input->post('year');
		$day = $this->input->post('day');
		$maxDays=date('t',strtotime('01-'.$month.'-'.$year));
		$html = "";
		for($w=1;$w<=$maxDays;$w++) 
		{ 
			if ($day == $w) 
			{
				$selected = "selected";
			}
			else
			{
				if ($maxDays < $day && $w == 1) 
				{
					$selected = "selected";
				}
				else
				{
					$selected = "";
				}
			}
			$html .= '<option '. $selected .' value="'. $w .'" >Day '. $w .'</option>';
		}
		echo $html;
	}

	//In use
	
	public function beta_detection() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access wbadmin.'); 
		    redirect('admin/login');
		}
		
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
		}
		
		$data = array(
		    'listMachines'=>$listMachines
		    );

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('beta_detection', $data);
		$this->load->view('footer');
		$this->load->view('script_file/beta_detection_footer');
	}

	//In use
	function exportBetaDetectionCsv()
    {

    	$machineId = 0; 
		if($this->input->get('machineId')) 
		{ 
			if($this->input->get('machineId') > 0 ) 
			{
				$machineId = $this->input->get('machineId');
			}
		} 
		if($this->input->get('dateValS')) 
		{ 
			$dateValS = $this->input->get('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->get('dateValE')) 
		{ 
			$dateValE = $this->input->get('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}

		$searchValue = $this->input->get('serchFiled');


		 if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else if ($searchValue == "Ignore" || $searchValue == "ignore") 
		   {
		   	   $searchQuery = "  ";  
		   }else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
    	$listColorLogs = $this->AM->getallBetaColorLogsExportCsv($dateValS, $dateValE, $machineId, $searchQuery)->result();
    	$data = array();
    	$rI = 1;
    	for ($key=0;$key<count($listColorLogs);$key++) 
		{

			
			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";
			$NoDataStacklight = (in_array("NoDataStacklight", $colorS)) ? "1" : "0";
			$NoDataError = (in_array("NoDataError", $colorS)) ? "1" : "0";
			$NoDataForceFully = (in_array("NoDataForceFully", $colorS)) ? "1" : "0";
			$NoInternet = (in_array("NoInternet", $colorS)) ? "1" : "0";
			$NoDataHome = (in_array("NoDataHome", $colorS)) ? "1" : "0";
			$NoDataRestart = (in_array("NoDataRestart", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1" || $NoDataStacklight == "1" || $NoDataError == "1" || $NoDataForceFully == "1" || $NoInternet == "1" || $NoDataHome == "1" || $NoDataRestart == "1") 
				{
					$listColorLogs[$key]->machineStatus = "No detection";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

			if ($listColorLogs[$key]->color == "NoData" || $listColorLogs[$key]->color == "NoInternet" || $listColorLogs[$key]->color == "NoDataStacklight" || $listColorLogs[$key]->color == "NoDataHome" || $listColorLogs[$key]->color == "NoDataForceFully" || $listColorLogs[$key]->color == "NoDataError" || $listColorLogs[$key]->color == "NoDataRestart") 
			{
				$seconds = strtotime($listColorLogs[$key]->originalTime) - strtotime($listColorLogs[$key + 1]->originalTime);
				if ($seconds <= 30) 
				{
					$listColorLogs[$key]->machineStatus  = 'Ignore';
				}else
				{
					if ($searchValue == "ignore" || $searchValue == "Ignore")
					{
						$removeArrayKey[] = $key;
					}
					$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
				}
			}else
			{
				if ($searchValue == "ignore" || $searchValue == "Ignore")
				{
					$removeArrayKey[] = $key;
				}
				$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
			}

			if ($listColorLogs[$key]->machineStatus == "Invalid") 
			{
				$listColorLogs[$key]->machineStatus = Invalid;
			}else if($listColorLogs[$key]->machineStatus == "Running")
			{
				$listColorLogs[$key]->machineStatus = Running;
			}else if($listColorLogs[$key]->machineStatus == "Waiting")
			{
				$listColorLogs[$key]->machineStatus = Waiting;
			}else if($listColorLogs[$key]->machineStatus == "Stopped")
			{
				$listColorLogs[$key]->machineStatus = Stopped;
			}else if($listColorLogs[$key]->machineStatus == "Off")
			{
				$listColorLogs[$key]->machineStatus = Off;
			}else if($listColorLogs[$key]->machineStatus == "No production")
			{
				$listColorLogs[$key]->machineStatus = Noproduction;
			}else if($listColorLogs[$key]->machineStatus == "NoData")
			{
				$listColorLogs[$key]->machineStatus = NoData;
			}else if($listColorLogs[$key]->machineStatus == "Setup")
			{
				$listColorLogs[$key]->machineStatus = setup;
			}else if($listColorLogs[$key]->machineStatus == "Ignore")
			{
				$listColorLogs[$key]->machineStatus = Ignore;
			}


			if ($listColorLogs[$key]->color == "NoData") 
			{
				$listColorLogs[$key]->color = NoData;
			}else if($listColorLogs[$key]->color == "NoInternet")
			{
				$listColorLogs[$key]->color = NoInternet;
			}else if($listColorLogs[$key]->color == "NoDataStacklight")
			{
				$listColorLogs[$key]->color = NoDataStacklight;
			}else if($listColorLogs[$key]->color == "NoDataHome")
			{
				$listColorLogs[$key]->color = NoDataHome;
			}else if($listColorLogs[$key]->color == "NoDataForceFully")
			{
				$listColorLogs[$key]->color = NoDataForceFully;
			}else if($listColorLogs[$key]->color == "NoDataError")
			{
				$listColorLogs[$key]->color = NoDataError;
			}else if($listColorLogs[$key]->color == "NoDataRestart")
			{
				$listColorLogs[$key]->color = NoDataRestart;
			}else if($listColorLogs[$key]->color == "off")
			{
				$listColorLogs[$key]->color = Off;
			}else
			{
				$listColorLogs[$key]->color = displayColorNameBetaTable($listColorLogs[$key]->color);
			}

			$data[] = array(
				"logId" => $rI,
				"machineId" => $listColorLogs[$key]->machineId,
				"machineName" => $listColorLogs[$key]->machineName,
				"color" => $listColorLogs[$key]->color,
				"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? Invalid : $listColorLogs[$key]->machineStatus,
				"originalTime" => $listColorLogs[$key]->originalTime
			);

			$rI++;
		}

		$data = $this->array_except($data, $removeArrayKey);
		$data = array_values($data);

		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Beta detection".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
		$handle = fopen('php://output', 'w');
        fputcsv($handle, array(Betadetectionlog));
        $cnt=1;
        foreach ($data as $key) 
        {
        	if ($cnt == 1) 
        	{
        		$narray=array(LogID,MachineId,MachineName,Color,State,Time);
            	fputcsv($handle, $narray);
        	}
            $narray=array($key['logId'],$key['machineId'],$key["machineName"],$key["color"],$key["machineStatus"],$key["originalTime"]);
            fputcsv($handle, $narray);
            $cnt++;
        }
        fclose($handle);
        exit;
	}
	

	//In use
	public function beta_detection_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		$rr = $row + 1;
		$searchQuery = " ";
		if($searchValue != '')
		{

		   if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else if ($searchValue == "ignore" || $searchValue == "Ignore") 
		   {
		   	   $searchQuery = " ";  
		   }
		   else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
		   
		}
		
		$listColorLogsCount = $this->AM->getallBetaColorLogsCount($this->factory, $machineId, $is_admin)->row()->totalCount;
		$listColorLogsSearchCount = $this->AM->getSearchBetaColorLogsCount($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
		$listColorLogs = $this->AM->getallBetaColorLogs($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

		$removeArrayKey = array();
		for ($key=0;$key<count($listColorLogs);$key++) 
		{
			

			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";
			$NoDataStacklight = (in_array("NoDataStacklight", $colorS)) ? "1" : "0";
			$NoDataError = (in_array("NoDataError", $colorS)) ? "1" : "0";
			$NoDataForceFully = (in_array("NoDataForceFully", $colorS)) ? "1" : "0";
			$NoInternet = (in_array("NoInternet", $colorS)) ? "1" : "0";
			$NoDataHome = (in_array("NoDataHome", $colorS)) ? "1" : "0";
			$NoDataRestart = (in_array("NoDataRestart", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1" || $NoDataStacklight == "1" || $NoDataError == "1" || $NoDataForceFully == "1" || $NoInternet == "1" || $NoDataHome == "1" || $NoDataRestart == "1") 
				{
					$listColorLogs[$key]->machineStatus = "nodet";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;

				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

			if ($listColorLogs[$key]->color == "NoData" || $listColorLogs[$key]->color == "NoInternet" || $listColorLogs[$key]->color == "NoDataStacklight" || $listColorLogs[$key]->color == "NoDataHome" || $listColorLogs[$key]->color == "NoDataForceFully" || $listColorLogs[$key]->color == "NoDataError" || $listColorLogs[$key]->color == "NoDataRestart") 
			{
				if ($key != 0) {
					$seconds = strtotime($listColorLogs[$key - 1]->originalTime) - strtotime($listColorLogs[$key]->originalTime);
				}else{
					$seconds = 0;
				}
				if ($seconds <= 30) 
				{
					$listColorLogs[$key]->machineStatus  = 'Ignore';
				}else
				{
					if ($searchValue == "ignore" || $searchValue == "Ignore")
					{
						$removeArrayKey[] = $key;
					}
					$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
				}
			}else
			{
				if ($searchValue == "ignore" || $searchValue == "Ignore")
				{
					$removeArrayKey[] = $key;
				}
				$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
			}

			$listColorLogs[$key]->logId = $rr;
			$rr++;


			if ($listColorLogs[$key]->machineStatus == "Invalid") 
			{
				$listColorLogs[$key]->machineStatus = Invalid;
			}else if($listColorLogs[$key]->machineStatus == "Running")
			{
				$listColorLogs[$key]->machineStatus = Running;
			}else if($listColorLogs[$key]->machineStatus == "Waiting")
			{
				$listColorLogs[$key]->machineStatus = Waiting;
			}else if($listColorLogs[$key]->machineStatus == "Stopped")
			{
				$listColorLogs[$key]->machineStatus = Stopped;
			}else if($listColorLogs[$key]->machineStatus == "Off")
			{
				$listColorLogs[$key]->machineStatus = Off;
			}else if($listColorLogs[$key]->machineStatus == "No production")
			{
				$listColorLogs[$key]->machineStatus = Noproduction;
			}else if($listColorLogs[$key]->machineStatus == "NoData")
			{
				$listColorLogs[$key]->machineStatus = NoData;
			}else if($listColorLogs[$key]->machineStatus == "Setup")
			{
				$listColorLogs[$key]->machineStatus = setup;
			}else if($listColorLogs[$key]->machineStatus == "Ignore")
			{
				$listColorLogs[$key]->machineStatus = Ignore;
			}


			if ($listColorLogs[$key]->color == "NoData") 
			{
				$listColorLogs[$key]->color = NoData;
			}else if($listColorLogs[$key]->color == "NoInternet")
			{
				$listColorLogs[$key]->color = NoInternet;
			}else if($listColorLogs[$key]->color == "NoDataStacklight")
			{
				$listColorLogs[$key]->color = NoDataStacklight;
			}else if($listColorLogs[$key]->color == "NoDataHome")
			{
				$listColorLogs[$key]->color = NoDataHome;
			}else if($listColorLogs[$key]->color == "NoDataForceFully")
			{
				$listColorLogs[$key]->color = NoDataForceFully;
			}else if($listColorLogs[$key]->color == "NoDataError")
			{
				$listColorLogs[$key]->color = NoDataError;
			}else if($listColorLogs[$key]->color == "NoDataRestart")
			{
				$listColorLogs[$key]->color = NoDataRestart;
			}else if($listColorLogs[$key]->color == "off")
			{
				$listColorLogs[$key]->color = Off;
			}else
			{
				$listColorLogs[$key]->color = displayColorNameBetaTable($listColorLogs[$key]->color);
			}
			
		}

		$listColorLogs = $this->array_except($listColorLogs, $removeArrayKey);
		$listColorLogs = array_values($listColorLogs);

		if ($columnIndex == 0) 
		{
			$keys = array_column($listColorLogs, 'logId');
			$orderBy = ($columnSortOrder == "desc") ? SORT_DESC : SORT_ASC;
			array_multisort($keys, $orderBy, $listColorLogs);
		}

		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		//print_r($response);exit;
		echo json_encode($response); die;
	}

	function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 


	//update_stacklight_machine function use for update assign stacklight machine 

	//In use
	public function update_stacklight_machine() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$stackLightTypeId = $this->input->post('stackLightTypeId');
		$stackLightTypeMachine = $this->input->post('stackLightTypeMachine');
		if (!empty($stackLightTypeMachine)) 
		{
			$this->AM->updateData(array("stackLightTypeId" => $stackLightTypeId),array("stackLightTypeId" => 0),"machine");
			foreach ($stackLightTypeMachine as $key => $value) 
			{
				//Update stacklight assign machine 
				$this->AM->updateData(array("machineId" => $value),array("stackLightTypeId" => $stackLightTypeId),"machine");
			}
			echo Stacklighttypedetailsupdatedsuccessfully;
		}
		else
		{
			echo Selectatleastonemachine;
		}
	}
	
	//overview function use for get overview details of dashboard

	//In use
	public function overview() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(1);
		
		if ($accessPoint == true) 
        {
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$data = array(
				'factoryData'=>$factoryData 
			    );
			$accessPointOpApp = $this->AM->accessPointExecute(22);
			$isOperator = ($this->AM->checkUserRole() == 2 || $accessPointOpApp == "0" || $this->AM->checkUserRole() == 1) ? 0 : 1;
			$factoryId = $this->session->userdata('factoryId');
			$userId = $this->session->userdata('userId');
				$listMachines = $this->AM->getMachinesOverview($this->factory, $isOperator, $this->session->userdata('userId'),$factoryId)->result('array');

				//echo "<pre>";print_r($listMachines);exit;
				$listColors = $this->AM->getallColors()->result();
				$allOperators = $this->db->where('userRole','0')->where("factoryId",$this->factoryId)->where('isDeleted',"0")->get('user')->num_rows();
				
				$onlineOperatorsQ = "select machineUserv2.userId from machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId where machineUserv2.isActive = '1' and user.userRole = '0' and user.isDeleted = '0' group by machineUserv2.userId";
				$onlineOperators = $this->factory->query($onlineOperatorsQ)->num_rows(); 
				$yesterdayMaintenanceDateDay = date('l');
				if ($yesterdayMaintenanceDateDay == "Monday") 
				{
					$yesterdayMaintenanceDate = date('Y-m-d 00:00:00',strtotime('-3 day'));
				}
				else
				{
					$yesterdayMaintenanceDate = date('Y-m-d 00:00:00',strtotime('-1 day'));
				}

				$yesterdayMaintenanceData = $this->factory->query("SELECT machineUserv2.workingMachine FROM `mNotificationv2` left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE mNotificationv2.`status` = '1' AND date(mNotificationv2.addedDate) = '".$yesterdayMaintenanceDate."'")->result_array(); 

				$yesterdayMaintenanceData = "";
				if(!empty($yesterdayMaintenanceData))
				{
					$yesterdayMaintenanceImplode = implode(",",array_column($yesterdayMaintenanceData,"workingMachine"));
					$yesterdayMaintenance = count(array_unique(explode(",",$yesterdayMaintenanceImplode)));
				}else
				{
					$yesterdayMaintenance = 0;
				}
				
			

				$overallMood = $this->AM->getWhereJoinSingle("mNotificationv2.star",array("mNotificationv2.categoryId" => 1,"mNotificationv2.star !=" => null,"mNotificationv2.star !=" => 0,"machineUserv2.userId !=" => 158,"date(mNotificationv2.addedDate)" => date('Y-m-d',strtotime($yesterdayMaintenanceDate))),array("machineUserv2" => "machineUserv2.activeId = mNotificationv2.activeId"),"mNotificationv2");
				

				if (!empty($overallMood)) 
				{
					$overallMood->yesterdayMaintenanceDate = $yesterdayMaintenanceDate;
				}
				$choose1 = date('Y');
				$choose2 = str_pad(date('m'), 2, '0', STR_PAD_LEFT);
				$choose3 = "";
				$choose4 = str_pad(date('d'), 2, '0', STR_PAD_LEFT);
				for($x=0;$x<count($listMachines);$x++) 
				{
					if (($listMachines[$x]['machineName'] == "Parts_1035" || $listMachines[$x]['machineName'] == "Parts_1832" || $listMachines[$x]['machineName'] == "Parts_1821") && $this->factoryId == 2) {
						$removeArrayKeyListMachines[] = $x;
					}

					$machineParts = $this->AM->getWhereSingle(array("currentOrderMachineStatus" => "0","currentOrderStatus" => "1","isDelete" => "0","machineId" => $listMachines[$x]['machineId']),"machineParts");
					$listMachines[$x]['currentOrderPart'] = !empty($machineParts) ? $machineParts->partsName : "";
					$lastMaintenanceDate = $this->factory->query("SELECT mNotificationv2.addedDate from mNotificationv2 left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId where mNotificationv2.`status` = '1' AND FIND_IN_SET ('".$listMachines[$x]['machineId']."', machineUserv2.machines) order by mNotificationv2.addedDate desc")->row();
					$maintenanceDate = date("Y-m-d H:i:s",strtotime($lastMaintenanceDate->addedDate));
					$yesterdayMaintenanceUsers = $this->factory->query("SELECT machineUserv2.userId FROM `mNotificationv2` left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE mNotificationv2.`status` = '1' AND mNotificationv2.addedDate = '".$lastMaintenanceDate->addedDate."' ")->result_array(); 
					$yesterdayMaintenanceUsersList = array_unique(array_column($yesterdayMaintenanceUsers, "userId"));
					foreach ($yesterdayMaintenanceUsersList as $key => $value) 
					{
						$select1 = "SELECT userName FROM user left join r_nytt_factory".$this->factoryId.".machineUserv2 on machineUserv2.userId = user.userId WHERE user.userId = ". $value ." AND user.userRole = '0' AND user.factoryId = ". $this->factoryId ." AND user.isDeleted = '0' group by user.userId";
						$yesterdayMaintenanceUsersData = $this->db->query($select1)->row(); 
						if (!empty($yesterdayMaintenanceUsersData)) 
						{
							$userNamesArr[] = $yesterdayMaintenanceUsersData->userName;
						}
					} 
					if( date('Y-m-d', $maintenanceDate) == date('Y-m-d', strtotime('-1 day')) ) 
					{
						$maintenanceDate = "yesterday"; 
					}
					if(is_array($userNamesArr) && count($userNamesArr) > 0) 
					{
						$listMachines[$x]['maintenanceUsers'] = Maintenancedoneby;
					} 
					else 
					{
						$listMachines[$x]['maintenanceUsers'] = Maintenancenotdoneyet.'.';
					}
					unset($userNamesArr);
					$listMachines[$x]['phoneDetail'] = $this->AM->getPhoneDetail($this->factory, $listMachines[$x]['machineId']);
					$machineId = $listMachines[$x]['machineId'];
					
					$machineresponseRunY = $this->AM->getVisulizationData($this->factory, $machineId, 'day', 'running', $choose1, $choose2, $choose3, $choose4)->result(); 
					$machineresponseAllRunY = $this->AM->getAllVisulizationData($this->factory, $machineId, 'day', $choose1, $choose2, $choose3, $choose4)->result(); 

					for($xz=0;$xz<count($machineresponseRunY);$xz++) 
					{
						$machinerunAvg += $machineresponseRunY[$xz]->countVal;
					}
					for($xy=0;$xy<count($machineresponseAllRunY);$xy++) 
					{
						$machineAllrunAvg += $machineresponseAllRunY[$xy]->countVal;
					}
					$listMachines[$x]['working_per'] = !empty($machineresponseRunY) ? ($machinerunAvg * 100) / $machineAllrunAvg : 0;
					unset($machinerunAvg);
					unset($machineAllrunAvg);
					
					$select_user = 'SELECT user.userName 
							FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId 
							WHERE machineUserv2.isActive = "1" 
							AND user.userRole = "0" 
							AND user.isDeleted = "0"
							AND (
	                                (FIND_IN_SET("'. $machineId .'", machineUserv2.machines) and workingMachine =  "") 
	                                or 
	                                (FIND_IN_SET("'. $machineId .'", machineUserv2.workingMachine) and workingMachine !=  "")
	                            ) '; 				
				    $listMachines[$x]['users'] = $this->factory->query($select_user)->result_array(); 
					if(is_array($listMachines[$x]['users']) && !empty($listMachines[$x]['users'])) 
					{
						for($p=0;$p<count($listMachines[$x]['users']);$p++) 
						{
							$listMachines[$x]['user_online'][$p] = $listMachines[$x]['users'][$p]['userName']; 
						}
					}
					if ($listMachines[$x]['chargeFlag'] != NULL) 
					{ 
						$listMachines[$x]['battery_per'] = $listMachines[$x]['batteryLevel'];
						$listMachines[$x]['ChargeFlag'] = $listMachines[$x]['chargeFlag'];
					} 
					else 
					{ 
						$listMachines[$x]['battery_per'] = 0;
						$listMachines[$x]['ChargeFlag'] = "0";
					}
					$isSetAppOn = $this->AM->checkSetApp($this->factoryId, $machineId); 
					if(isset($isSetAppOn) && is_array($isSetAppOn) && count($isSetAppOn) > 0)
					{ 
						$listMachines[$x]['isSetAppOn'] = $isSetAppOn['isActive'];
						if($isSetAppOn['reason'] == 'client namespace disconnect' || $isSetAppOn['reason'] == 'transport close' || $isSetAppOn['reason'] == 'transport error') 
						{
							$listMachines[$x]['setAppAlert'] = SetAppturnedoffCheckyourphoneorcontactinfonytttechcom.'.';
						}elseif ($isSetAppOn['reason'] == 'detection disconnect') {
							$listMachines[$x]['setAppAlert'] = SetAppisonhomescreenCheckyourphoneorcontactinfonytttechcom.'.';
						}
						else if($isSetAppOn['reason'] == 'ping timeout') 
						{
							$listMachines[$x]['setAppAlert'] = NointernetorphoneturnedoffCheckyourphoneorcontactinfonytttechcom.'.';
						}  
						else if($isSetAppOn['reason'] == 'app close force fully') 
						{
							$listMachines[$x]['setAppAlert'] = SetAppclosedforcefullyCheckyourphoneorcontactinfonytttechcom.'.';
						}  
						else if($isSetAppOn['reason'] == 'app stop due to error') 
						{
							$listMachines[$x]['setAppAlert'] =SetAppcrashedandstoppedduetoexceptionCheckyourphoneorcontactinfonytttechcom.'.';
						}  
						else if($isSetAppOn['reason'] == 'setApp restart') 
						{
							$listMachines[$x]['setAppAlert'] = SetApprestatedduetosomeexceptionCheckyourphoneorcontactinfonytttechcom.'.';
						}  
					} 
					else 
					{
						$listMachines[$x]['isSetAppOn'] = 0;
						$listMachines[$x]['setAppAlert'] = SetAppnotstartedCheckyourphoneorcontactinfonytttechcom.'.'; 
					}
					$listMachines[$x]['machineLightArr'] = explode(",", $listMachines[$x]['machineLight']); 
					for($z=0;$z<count($listMachines[$x]['machineLightArr']);$z++) 
					{
						for($y=0;$y<count($listColors);$y++) 
						{
							if($listMachines[$x]['machineLightArr'][$z] == $listColors[$y]->colorId) $listMachines[$x]['machineLightNameArr'][$z] = $listColors[$y]->colorCode;  
						}
						
						$machine_light_status['redStatus'] = '0';
						$machine_light_status['yellowStatus'] = '0';
						$machine_light_status['greenStatus'] = '0';
						$machine_light_status['blueStatus'] = '0';
						$machine_light_status['whiteStatus'] = '0';
						
						$color = $listMachines[$x]['color'];  
						$colorArr = explode(" ", $color);
						for($c=0;$c<count($colorArr);$c++) 
						{
							if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') 
							{ 
								$machine_light_status[strtolower($colorArr[$c]).'Status'] = '1';
							}
						}
						if(strpos($color, 'Red Yellow Green') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['redStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Yellow and green') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Green and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Red and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['redStatus'] = '1';
						}
						if(strpos($color, 'Blue and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['whiteStatus'] = '1';
						}
						if(strpos($color, 'Red and green') !== false) 
						{
							$machine_light_status['redStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Robot') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
						}
						if(strpos($color, 'Door') !== false) 
						{
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Blue') !== false) 
						{
							$machine_light_status['blueStatus'] = '1';
						}
						if(strpos($color, 'Red') !== false) 
						{
							$machine_light_status['redStatus'] = '1';
						}
						if(strpos($color, 'Yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
						}
						if(strpos($color, 'Green') !== false) 
						{
							$machine_light_status['greenStatus'] = '1';
						}
						
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FF0000') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['redStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'redStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FFFF00') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['yellowStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'yellowStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == '00FF00') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['greenStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'greenStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FFFFFF') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['whiteStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'whiteStatus';
						}  
						if($listMachines[$x]['machineLightNameArr'][$z] == '0000FF') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['blueStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'blueStatus';
						} 

						
						$listMachines[$x]['machine_light_status']['redStatus'] = $machine_light_status['redStatus'];
						$listMachines[$x]['machine_light_status']['greenStatus'] = $machine_light_status['greenStatus'];
						$listMachines[$x]['machine_light_status']['yellowStatus'] = $machine_light_status['yellowStatus'];
						$listMachines[$x]['machine_light_status']['whiteStatus'] = $machine_light_status['whiteStatus'];  
						$listMachines[$x]['machine_light_status']['blueStatus'] = $machine_light_status['blueStatus'];  
						
						$listMachines[$x]['machine_light_status']['statusText'] = $this->AM->getMachineStatusText($this->factory, $listMachines[$x]['machineId'], $listMachines[$x]['machine_light_status']['redStatus'], $listMachines[$x]['machine_light_status']['greenStatus'], $listMachines[$x]['machine_light_status']['yellowStatus'], $listMachines[$x]['machine_light_status']['whiteStatus'], $listMachines[$x]['machine_light_status']['blueStatus']); 
					
						if($listMachines[$x]['logId'] != 0) 
						{
							if($listMachines[$x]['color'] == 'NoData' || $listMachines[$x]['color'] == 'NoDataStacklight' || $listMachines[$x]['color'] == 'NoDataError' || $listMachines[$x]['color'] == 'NoDataHome' || $listMachines[$x]['color'] == 'NoDataForceFully' || $listMachines[$x]['color'] == 'NoDataRestart') 
							{
								$listMachines[$x]['isSetAppOn'] = 0;
								if(!isset($listMachines[$x]['setAppAlert'])) 
								{
									$listMachines[$x]['setAppAlert'] = NodetectionCheckyourphoneorcontactinfonytttechcom.'.'; 
								}
							} 
							else 
							{
								if ($listMachines[$x]['setAppAlert'] == "No internet or phone turned off. Check your phone or contact : info@nytt-tech.com") 
								{
									$listMachines[$x]['isSetAppOn'] = 0;
									$listMachines[$x]['setAppAlert'] = NointernetorphoneturnedoffCheckyourphoneorcontactinfonytttechcom.'.';
								}else
								{
									$listMachines[$x]['isSetAppOn'] = 1;
									$listMachines[$x]['setAppAlert'] = '';
								}
							}
						}

						if ($listMachines[$x]['isDataGet'] == "2") 
						{
							$mtConnectOverviewResult  = $this->AM->getWhereSingle(array("machineId" => $machineId),"mtConnectOverview");
							$mtConnectS  = $this->AM->getWhereSingleWithOrderBy(array("machineId" => $machineId,"date(addedTime)" => date('Y-m-d')),"addedTime","asc","mtConnect");

							$listMachines[$x]['currentOrderPart'] =  (!empty($mtConnectOverviewResult) ? $mtConnectOverviewResult->Partcount - $mtConnectS->Partcount: 0) ;
							if ($mtConnectOverviewResult->State == "ACTIVE") 
							{
								$listMachines[$x]['machine_light_status']['greenStatus'] =  "1";
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = '';
								$listMachines[$x]['machine_light_status']['statusText'] = 'Running';
								$listMachines[$x]['phoneDetail']->phoneId = '1';
							}else if ($mtConnectOverviewResult->State == "INTERRUPTED"  || $mtConnectOverviewResult->State == "READY") 
							{
								$listMachines[$x]['machine_light_status']['yellowStatus'] =  "1";
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = '';
								$listMachines[$x]['machine_light_status']['statusText'] = 'Waiting';
								$listMachines[$x]['phoneDetail']->phoneId = '1';
							}
							else if ($mtConnectOverviewResult->State == "STOPPED") 
							{
								$listMachines[$x]['machine_light_status']['redStatus'] =  "1";
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = '';
								$listMachines[$x]['machine_light_status']['statusText'] = 'Stopped';
								$listMachines[$x]['phoneDetail']->phoneId = '1';
							}else 
							{
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = 'unavailable';
								$listMachines[$x]['machine_light_status']['statusText'] = 'nodet';
								$listMachines[$x]['phoneDetail']->phoneId = !empty($mtConnectOverviewResult->State) ? '1' : '0';
							}


						}

						if ($listMachines[$x]['isDataGet'] == "4") 
						{
							$virtualMachineLogOverviewResult  = $this->AM->getWhereSingle(array("machineId" => $machineId),"virtualMachineLogOverview");

							if ($machineId == 7  && $this->factoryId == 2) 
							{
								$listMachines[$x]['currentOrderPart'] =  $this->AM->getWhereCount(array("machineId" => 8,"signalType" => "1","date(originalTime)" => date('Y-m-d')),"virtualMachineLog");
							}elseif ($machineId == 16  && $this->factoryId == 2) 
							{
								$listMachines[$x]['currentOrderPart'] =  $this->AM->getWhereCount(array("machineId" => 17,"signalType" => "1","date(originalTime)" => date('Y-m-d')),"virtualMachineLog");
							}
							elseif ($machineId == 18  && $this->factoryId == 2) 
							{
								$listMachines[$x]['currentOrderPart'] =  $this->AM->getWhereCount(array("machineId" => 19,"signalType" => "1","date(originalTime)" => date('Y-m-d')),"virtualMachineLog");
							}
							else
							{
								$listMachines[$x]['currentOrderPart'] =  $this->AM->getWhereCount(array("machineId" => $machineId,"signalType" => "1","date(originalTime)" => date('Y-m-d')),"virtualMachineLog");
							}

							//$listMachines[$x]['currentOrderPart'] =  $this->AM->getWhereCount(array("machineId" => $machineId,"signalType" => "1","date(originalTime)" => date('Y-m-d')),"virtualMachineLog");
							if ($virtualMachineLogOverviewResult->signalType == "1") 
							{
								$listMachines[$x]['machine_light_status']['greenStatus'] =  "1";
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = '';
								$listMachines[$x]['machine_light_status']['statusText'] = 'Running';
								$listMachines[$x]['phoneDetail']->phoneId = '1';
							}else if ($virtualMachineLogOverviewResult->signalType == "0") 
							{
								$listMachines[$x]['machine_light_status']['redStatus'] =  "1";
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = '';
								$listMachines[$x]['machine_light_status']['statusText'] = 'Stopped';
								$listMachines[$x]['phoneDetail']->phoneId = '1';
							}else 
							{
								$listMachines[$x]['isSetAppOn'] = 1;
								$listMachines[$x]['machineSelected'] = "1";
								$listMachines[$x]['setAppAlert'] = 'unavailable';
								$listMachines[$x]['machine_light_status']['statusText'] = 'nodet';
								$listMachines[$x]['phoneDetail']->phoneId = ($virtualMachineLogOverviewResult->signalType != "") ? '1' : '0';;
							}


						}
					}
				} 

				$listMachines = $this->array_except($listMachines, $removeArrayKeyListMachines);
				$listMachines = array_values($listMachines);

				//echo "<pre>";print_r($listMachines);exit;
				$machineId = 0;
				$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','running');
				$ActualProductionRunning = $resultActualProductionRunning->countVal;
				$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','waiting');
				$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
				$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','stopped');
				$ActualProductionStopped = $resultActualProductionStopped->countVal;
				$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','off');
				$ActualProductionOff = $resultActualProductionOff->countVal;
				$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','nodet');
				$ActualProductionNodet = $resultActualProductionNodet->countVal;
				$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','noStacklight');
				$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;


				$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;

				$resultSetupRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','running');
				$SetupRunning = $resultSetupRunning->countVal;
				$resultSetupWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','waiting');
				$SetupWaiting = $resultSetupWaiting->countVal;
				$resultSetupStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','stopped');
				$SetupStopped = $resultSetupStopped->countVal;
				$resultSetupOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','off');
				$SetupOff = $resultSetupOff->countVal;
				$resultSetupNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','nodet');
				$SetupNodet = $resultSetupNodet->countVal;
				$resultSetupNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','noStacklight');
				$SetupNoStacklight = $resultSetupNoStacklight->countVal;


				$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
				
				$resultNoProductionRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','running');
				$NoProductionRunning = $resultNoProductionRunning->countVal;
				$resultNoProductionWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','waiting');
				$NoProductionWaiting = $resultNoProductionWaiting->countVal;
				$resultNoProductionStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','stopped');
				$NoProductionStopped = $resultNoProductionStopped->countVal;
				$resultNoProductionOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','off');
				$NoProductionOff = $resultNoProductionOff->countVal;
				$resultNoProductionNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','nodet');
				$NoProductionNodet = $resultNoProductionNodet->countVal;
				$resultNoProductionNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','noStacklight');
				$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

				$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

				$total = $ActualProduction + $Setup + $NoProduction;
				
				$overallRunningToday =  !empty($ActualProductionRunning) ? ($ActualProductionRunning * 100) / $ActualProduction : 0;
				$notificationLog = $this->AM->getNotificationLog($this->factoryId);
				$virtualMachine = $this->AM->getWhere(array("IOName !=" => "0"),"virtualMachine");  

				//echo "<pre>";print_r($listMachines);exit;
				$data = array(
				    'listMachines'=>$listMachines,
					'listColors'=>$listColors,
					'overallRunningToday'=>$overallRunningToday, 
					'allOperators'=>$allOperators,
					'onlineOperators'=>$onlineOperators, 	
					'yesterdayMaintenance'=>$yesterdayMaintenance,
					'overallMood'=>$overallMood, 
					'notificationLog'=>$notificationLog, 
					'virtualMachine'=>$virtualMachine, 
				    );

				$data['accessPointMachineView'] = $this->AM->accessPoint(3);
				$data['accessPointMachineEdit'] = $this->AM->accessPointEdit(3);

				$data['accessPointOverallView'] = $this->AM->accessPoint(2);
				$data['accessPointOverallEdit'] = $this->AM->accessPointEdit(2);

				$data['accessPointVirtualMachineView'] = $this->AM->accessPoint(28);
				$data['siteLang'] = $this->session->userdata('site_lang');
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('overview2',$data);
			$this->load->view('footer',$data);
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}


	//In use
	public function overview_development() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$accessPoint = $this->AM->accessPoint(1);
		
		if ($accessPoint == true) 
        {
			$factoryData = $this->AM->getFactoryData($this->factoryId); 
			$data = array(
				'factoryData'=>$factoryData 
			    );
			$accessPointOpApp = $this->AM->accessPointExecute(22);
			$isOperator = ($this->AM->checkUserRole() == 2 || $accessPointOpApp == "0" || $this->AM->checkUserRole() == 1) ? 0 : 1;
			$factoryId = $this->session->userdata('factoryId');
			$userId = $this->session->userdata('userId');
				$listMachines = $this->AM->getMachinesOverview($this->factory, $isOperator, $this->session->userdata('userId'),$factoryId)->result('array');

				//echo "<pre>";print_r($listMachines);exit;
				$listColors = $this->AM->getallColors()->result();
				$allOperators = $this->db->where('userRole','0')->where("factoryId",$this->factoryId)->where('isDeleted',"0")->get('user')->num_rows();
				
				$onlineOperatorsQ = "select machineUserv2.userId from machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId where machineUserv2.isActive = '1' and user.userRole = '0' and user.isDeleted = '0' group by machineUserv2.userId";
				$onlineOperators = $this->factory->query($onlineOperatorsQ)->num_rows(); 
				$yesterdayMaintenanceDateDay = date('l');
				if ($yesterdayMaintenanceDateDay == "Monday") 
				{
					$yesterdayMaintenanceDate = date('Y-m-d 00:00:00',strtotime('-3 day'));
				}
				else
				{
					$yesterdayMaintenanceDate = date('Y-m-d 00:00:00',strtotime('-1 day'));
				}

				$yesterdayMaintenance = $this->factory->query("SELECT * FROM `mNotificationv2` left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE mNotificationv2.`status` = '1' AND date(mNotificationv2.addedDate) = '".$yesterdayMaintenanceDate."' GROUP BY machineUserv2.`userId`")->num_rows(); 
				
				$overallMoodDate = $this->factory->select('addedDate')->where(array("categoryId" => 1,"star !=" => null,"star !=" => 0))->order_by("addedDate","desc")->get('mNotificationv2')->row(); 
				$overallMood = $this->factory->select_avg('star')->where(array("categoryId" => 1,"star !=" => null,"star !=" => 0,"date(addedDate)" => date('Y-m-d',strtotime($yesterdayMaintenanceDate))))->get('mNotificationv2')->row();
				if (empty($overallMood)) 
				{
					$overallMood = $this->factory->select_avg('star')->where(array("categoryId" => 1,"star !=" => null,"star !=" => 0,"date(addedDate)" => date('Y-m-d',strtotime($overallMoodDate->addedDate))))->get('mNotificationv2')->row();
					echo $this->factory->last_query();
				} 
				$overallMood->yesterdayMaintenanceDate = $yesterdayMaintenanceDate;
				$choose1 = date('Y');
				$choose2 = str_pad(date('m'), 2, '0', STR_PAD_LEFT);
				$choose3 = "";
				$choose4 = str_pad(date('d'), 2, '0', STR_PAD_LEFT);
				for($x=0;$x<count($listMachines);$x++) 
				{
					$lastMaintenanceDate = $this->factory->query("SELECT mNotificationv2.addedDate from mNotificationv2 left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId where mNotificationv2.`status` = '1' AND FIND_IN_SET ('".$listMachines[$x]['machineId']."', machineUserv2.machines) order by mNotificationv2.addedDate desc")->row();
					$maintenanceDate = date("Y-m-d H:i:s",strtotime($lastMaintenanceDate->addedDate));
					$yesterdayMaintenanceUsers = $this->factory->query("SELECT machineUserv2.userId FROM `mNotificationv2` left join machineUserv2 on machineUserv2.activeId = mNotificationv2.activeId WHERE mNotificationv2.`status` = '1' AND mNotificationv2.addedDate = '".$lastMaintenanceDate->addedDate."' ")->result_array(); 
					$yesterdayMaintenanceUsersList = array_unique(array_column($yesterdayMaintenanceUsers, "userId"));
					foreach ($yesterdayMaintenanceUsersList as $key => $value) 
					{
						$select1 = "SELECT userName FROM user left join r_nytt_factory".$this->factoryId.".machineUserv2 on machineUserv2.userId = user.userId WHERE user.userId = ". $value ." AND user.userRole = '0' AND user.factoryId = ". $this->factoryId ." AND user.isDeleted = '0' group by user.userId";
						$yesterdayMaintenanceUsersData = $this->db->query($select1)->row(); 
						if (!empty($yesterdayMaintenanceUsersData)) 
						{
							$userNamesArr[] = $yesterdayMaintenanceUsersData->userName;
						}
					} 
					if( date('Y-m-d', $maintenanceDate) == date('Y-m-d', strtotime('-1 day')) ) 
					{
						$maintenanceDate = "yesterday"; 
					}
					if(is_array($userNamesArr) && count($userNamesArr) > 0) 
					{
						$listMachines[$x]['maintenanceUsers'] = Maintenancedoneby;
					} 
					else 
					{
						$listMachines[$x]['maintenanceUsers'] = Maintenancenotdoneyet.'.';
					}
					unset($userNamesArr);
					$listMachines[$x]['phoneDetail'] = $this->AM->getPhoneDetail($this->factory, $listMachines[$x]['machineId']);
					$machineId = $listMachines[$x]['machineId'];
					
					$machineresponseRunY = $this->AM->getVisulizationData($this->factory, $machineId, 'day', 'running', $choose1, $choose2, $choose3, $choose4)->result(); 
					$machineresponseAllRunY = $this->AM->getAllVisulizationData($this->factory, $machineId, 'day', $choose1, $choose2, $choose3, $choose4)->result(); 

					for($xz=0;$xz<count($machineresponseRunY);$xz++) 
					{
						$machinerunAvg += $machineresponseRunY[$xz]->countVal;
					}
					for($xy=0;$xy<count($machineresponseAllRunY);$xy++) 
					{
						$machineAllrunAvg += $machineresponseAllRunY[$xy]->countVal;
					}
					$listMachines[$x]['working_per'] = !empty($machineresponseRunY) ? ($machinerunAvg * 100) / $machineAllrunAvg : 0;
					unset($machinerunAvg);
					unset($machineAllrunAvg);
					
					$select_user = 'SELECT user.userName 
							FROM machineUserv2 left join r_nytt_main.user on user.userId = machineUserv2.userId 
							WHERE machineUserv2.isActive = "1" 
							AND user.userRole = "0" 
							AND user.isDeleted = "0"
							AND (
	                                (FIND_IN_SET("'. $machineId .'", machineUserv2.machines) and workingMachine =  "") 
	                                or 
	                                (FIND_IN_SET("'. $machineId .'", machineUserv2.workingMachine) and workingMachine !=  "")
	                            ) '; 				
				    $listMachines[$x]['users'] = $this->factory->query($select_user)->result_array(); 
					if(is_array($listMachines[$x]['users']) && !empty($listMachines[$x]['users'])) 
					{
						for($p=0;$p<count($listMachines[$x]['users']);$p++) 
						{
							$listMachines[$x]['user_online'][$p] = $listMachines[$x]['users'][$p]['userName']; 
						}
					}
					if ($listMachines[$x]['chargeFlag'] != NULL) 
					{ 
						$listMachines[$x]['battery_per'] = $listMachines[$x]['batteryLevel'];
						$listMachines[$x]['ChargeFlag'] = $listMachines[$x]['chargeFlag'];
					} 
					else 
					{ 
						$listMachines[$x]['battery_per'] = 0;
						$listMachines[$x]['ChargeFlag'] = "0";
					}
					$isSetAppOn = $this->AM->checkSetApp($this->factoryId, $machineId); 
					if(isset($isSetAppOn) && is_array($isSetAppOn) && count($isSetAppOn) > 0)
					{ 
						$listMachines[$x]['isSetAppOn'] = $isSetAppOn['isActive'];
						if($isSetAppOn['reason'] == 'client namespace disconnect' || $isSetAppOn['reason'] == 'transport close' || $isSetAppOn['reason'] == 'transport error') 
						{
							$listMachines[$x]['setAppAlert'] = SetAppturnedoffCheckyourphoneorcontactinfonytttechcom;
						}elseif ($isSetAppOn['reason'] == 'detection disconnect') {
							$listMachines[$x]['setAppAlert'] = SetAppisonhomescreenCheckyourphoneorcontactinfonytttechcom;
						}
						else if($isSetAppOn['reason'] == 'ping timeout') 
						{
							$listMachines[$x]['setAppAlert'] = NointernetorphoneturnedoffCheckyourphoneorcontactinfonytttechcom;
						}  
						else if($isSetAppOn['reason'] == 'app close force fully') 
						{
							$listMachines[$x]['setAppAlert'] = SetAppclosedforcefullyCheckyourphoneorcontactinfonytttechcom;
						}  
						else if($isSetAppOn['reason'] == 'app stop due to error') 
						{
							$listMachines[$x]['setAppAlert'] =SetAppcrashedandstoppedduetoexceptionCheckyourphoneorcontactinfonytttechcom;
						}  
						else if($isSetAppOn['reason'] == 'setApp restart') 
						{
							$listMachines[$x]['setAppAlert'] = SetApprestatedduetosomeexceptionCheckyourphoneorcontactinfonytttechcom;
						}  
					} 
					else 
					{
						$listMachines[$x]['isSetAppOn'] = 0;
						$listMachines[$x]['setAppAlert'] = SetAppnotstartedCheckyourphoneorcontactinfonytttechcom; 
					}
					$listMachines[$x]['machineLightArr'] = explode(",", $listMachines[$x]['machineLight']); 
					for($z=0;$z<count($listMachines[$x]['machineLightArr']);$z++) 
					{
						for($y=0;$y<count($listColors);$y++) 
						{
							if($listMachines[$x]['machineLightArr'][$z] == $listColors[$y]->colorId) $listMachines[$x]['machineLightNameArr'][$z] = $listColors[$y]->colorCode;  
						}
						
						$machine_light_status['redStatus'] = '0';
						$machine_light_status['yellowStatus'] = '0';
						$machine_light_status['greenStatus'] = '0';
						$machine_light_status['blueStatus'] = '0';
						$machine_light_status['whiteStatus'] = '0';
						
						$color = $listMachines[$x]['color'];  
						$colorArr = explode(" ", $color);
						for($c=0;$c<count($colorArr);$c++) 
						{
							if(strtolower($colorArr[$c]) != 'and' && strtolower($colorArr[$c]) != 'off') 
							{ 
								$machine_light_status[strtolower($colorArr[$c]).'Status'] = '1';
							}
						}
						if(strpos($color, 'Red Yellow Green') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['redStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Yellow and green') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Green and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Red and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['redStatus'] = '1';
						}
						if(strpos($color, 'Blue and yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
							$machine_light_status['whiteStatus'] = '1';
						}
						if(strpos($color, 'Red and green') !== false) 
						{
							$machine_light_status['redStatus'] = '1';
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Robot') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
						}
						if(strpos($color, 'Door') !== false) 
						{
							$machine_light_status['greenStatus'] = '1';
						}
						if(strpos($color, 'Blue') !== false) 
						{
							$machine_light_status['blueStatus'] = '1';
						}
						if(strpos($color, 'Red') !== false) 
						{
							$machine_light_status['redStatus'] = '1';
						}
						if(strpos($color, 'Yellow') !== false) 
						{
							$machine_light_status['yellowStatus'] = '1';
						}
						if(strpos($color, 'Green') !== false) 
						{
							$machine_light_status['greenStatus'] = '1';
						}
						
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FF0000') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['redStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'redStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FFFF00') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['yellowStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'yellowStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == '00FF00') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['greenStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'greenStatus';
						}
						if($listMachines[$x]['machineLightNameArr'][$z] == 'FFFFFF') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['whiteStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'whiteStatus';
						}  
						if($listMachines[$x]['machineLightNameArr'][$z] == '0000FF') 
						{
							$listMachines[$x]['machine_light_colors_status'][$z] = $machine_light_status['blueStatus'];
							$listMachines[$x]['machine_light_colors_status_names'][$z] = 'blueStatus';
						}  
						$listMachines[$x]['machine_light_status']['redStatus'] = $machine_light_status['redStatus'];
						$listMachines[$x]['machine_light_status']['greenStatus'] = $machine_light_status['greenStatus'];
						$listMachines[$x]['machine_light_status']['yellowStatus'] = $machine_light_status['yellowStatus'];
						$listMachines[$x]['machine_light_status']['whiteStatus'] = $machine_light_status['whiteStatus'];  
						$listMachines[$x]['machine_light_status']['blueStatus'] = $machine_light_status['blueStatus'];  
						
						$listMachines[$x]['machine_light_status']['statusText'] = $this->AM->getMachineStatusText($this->factory, $listMachines[$x]['machineId'], $listMachines[$x]['machine_light_status']['redStatus'], $listMachines[$x]['machine_light_status']['greenStatus'], $listMachines[$x]['machine_light_status']['yellowStatus'], $listMachines[$x]['machine_light_status']['whiteStatus'], $listMachines[$x]['machine_light_status']['blueStatus']); 
					
						if($listMachines[$x]['logId'] != 0) 
						{
							if($listMachines[$x]['color'] == 'NoData' || $listMachines[$x]['color'] == 'NoDataStacklight' || $listMachines[$x]['color'] == 'NoDataError' || $listMachines[$x]['color'] == 'NoDataHome' || $listMachines[$x]['color'] == 'NoDataForceFully' || $listMachines[$x]['color'] == 'NoDataRestart') 
							{
								$listMachines[$x]['isSetAppOn'] = 0;
								if(!isset($listMachines[$x]['setAppAlert'])) 
								{
									$listMachines[$x]['setAppAlert'] = NodetectionCheckyourphoneorcontactinfonytttechcom; 
								}
							} 
							else 
							{
								if ($listMachines[$x]['setAppAlert'] == "No internet or phone turned off. Check your phone or contact : info@nytt-tech.com") 
								{
									$listMachines[$x]['isSetAppOn'] = 0;
									$listMachines[$x]['setAppAlert'] = NointernetorphoneturnedoffCheckyourphoneorcontactinfonytttechcom;
								}else
								{
									$listMachines[$x]['isSetAppOn'] = 1;
									$listMachines[$x]['setAppAlert'] = '';
								}
							}
						}
					}
				} 
				$machineId = 0;
				$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','running');
				$ActualProductionRunning = $resultActualProductionRunning->countVal;
				$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','waiting');
				$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
				$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','stopped');
				$ActualProductionStopped = $resultActualProductionStopped->countVal;
				$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','off');
				$ActualProductionOff = $resultActualProductionOff->countVal;
				$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','nodet');
				$ActualProductionNodet = $resultActualProductionNodet->countVal;
				$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'0','noStacklight');
				$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;


				$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;

				$resultSetupRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','running');
				$SetupRunning = $resultSetupRunning->countVal;
				$resultSetupWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','waiting');
				$SetupWaiting = $resultSetupWaiting->countVal;
				$resultSetupStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','stopped');
				$SetupStopped = $resultSetupStopped->countVal;
				$resultSetupOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','off');
				$SetupOff = $resultSetupOff->countVal;
				$resultSetupNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','nodet');
				$SetupNodet = $resultSetupNodet->countVal;
				$resultSetupNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'1','noStacklight');
				$SetupNoStacklight = $resultSetupNoStacklight->countVal;


				$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
				
				$resultNoProductionRunning = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','running');
				$NoProductionRunning = $resultNoProductionRunning->countVal;
				$resultNoProductionWaiting = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','waiting');
				$NoProductionWaiting = $resultNoProductionWaiting->countVal;
				$resultNoProductionStopped = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','stopped');
				$NoProductionStopped = $resultNoProductionStopped->countVal;
				$resultNoProductionOff = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','off');
				$NoProductionOff = $resultNoProductionOff->countVal;
				$resultNoProductionNodet = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','nodet');
				$NoProductionNodet = $resultNoProductionNodet->countVal;
				$resultNoProductionNoStacklight = $this->AM->getMachineStatusData($machineId,'day',date('m/d/Y'),'2','noStacklight');
				$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

				$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

				$total = $ActualProduction + $Setup + $NoProduction;
				
				$overallRunningToday =  !empty($ActualProductionRunning) ? ($ActualProductionRunning * 100) / $ActualProduction : 0;
				$notificationLog = $this->AM->getNotificationLog($this->factoryId);
				$virtualMachine = $this->AM->getWhere(array("IOName !=" => "0"),"virtualMachine");  

				//echo "<pre>";print_r($listMachines);exit;
				$data = array(
				    'listMachines'=>$listMachines,
					'listColors'=>$listColors,
					'overallRunningToday'=>$overallRunningToday, 
					'allOperators'=>$allOperators,
					'onlineOperators'=>$onlineOperators, 	
					'yesterdayMaintenance'=>$yesterdayMaintenance,
					'overallMood'=>$overallMood, 
					'notificationLog'=>$notificationLog, 
					'virtualMachine'=>$virtualMachine, 
				    );

				$data['accessPointMachineView'] = $this->AM->accessPoint(3);
				$data['accessPointMachineEdit'] = $this->AM->accessPointEdit(3);

				$data['accessPointOverallView'] = $this->AM->accessPoint(2);
				$data['accessPointOverallEdit'] = $this->AM->accessPointEdit(2);

				$data['accessPointVirtualMachineView'] = $this->AM->accessPoint(28);
				$data['siteLang'] = $this->session->userdata('site_lang');
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('overview3',$data);
			$this->load->view('footer',$data);
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	//setAppStart function use for start setApp by dashboard

	//In use
	public function setAppStart() 
    { 
    	if($this->AM->checkIsvalidated() == false)
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$factoryId = $this->session->userdata('factoryId'); 
		$machineId = $this->input->post('machineId');
		$machineUser = $this->AM->getLastActiveMachine($machineId);
		$fcmToken = array($machineUser->deviceToken);
		$message = "SetApp started successfully. Camera will start in next 30 seconds for selected machine";
		$title = "Set app start";
		$type = "setAppStart";
		$activeId = $machineUser->activeId;
		//Check machine is selected or not
		if (!empty($machineUser) && $machineUser->isActive == "1") 
		{
			//Send notification to start setApp
			$this->AM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId);
			$json = array("status" => 1,"message" => SetAppstartedsuccessfullyCamerawillstartinnext30secondsforselectedmachine);
		}
		else
		{
			$json = array("status" => 0,"message" => DashboardcouldnotfindanyloggedinSetAppthathasselectedcurrentmachine);
		}
		echo json_encode($json);
	}

	//setAppStart function use for start setApp by dashboard

	//In use
	public function setAppReStart() 
    { 
    	if($this->AM->checkIsvalidated() == false)
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$userId = $this->session->userdata('userId'); 
		$factoryId = $this->session->userdata('factoryId'); 
		$machineId = $this->input->post('machineId');
		$machineUser = $this->AM->getLastActiveMachine($machineId);
		$fcmToken = array($machineUser->deviceToken);
		$message = "SetApp restarted successfully";
		$title = "Set app restart";
		$type = "setAppReStart";
		$activeId = $machineUser->activeId;
		//Check machine is selected or not
		if (!empty($machineUser) && $machineUser->isActive == "1") 
		{
			//Send notification to restart setApp
			$data = array("userId" => $userId,"machineId" => $machineId,"insertTime" => date('Y-m-d H:i:s'));
			$this->AM->insertData($data, "setAppReStartLog");
			$this->AM->sendNotificationSetApp($message,$title,$fcmToken,$type,$factoryId,$machineId,$activeId);
			$json = array("status" => 1,"message" => SetApprestartedsuccessfully);
		}
		else
		{
			$json = array("status" => 0,"message" => DashboardcouldnotfindanyloggedinSetAppthathasselectedcurrentmachine);
		}
		echo json_encode($json);
	}
	
	//dashboardCaptureCamera function use for send worng detection mail

	//In use
	public function dashboardCaptureCamera() 
    {	
    	if($this->AM->checkIsvalidated() == false) 
    	{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = $this->input->post('detectionMachineId');
		$timestamp = $this->input->post('timestamp');
		$color = $this->input->post('color');
		$state = $this->input->post('state');
		$userId = $this->session->userdata('userId'); 
		$factoryId = $this->session->userdata('factoryId');
		$logTime = time();
		$captureTime = time(); 

		$checkFactoryDetail = $this->AM->checkFactoryDetail($factoryId);

		$fileExt = "png";
	    $img = $this->input->post('imageName');
	    $img = str_replace('data:image/png;base64,', '', $img);
	    $img = str_replace(' ', '+', $img);
	    $data = base64_decode($img);
	    $file =  "../reportedImages/".$factoryId."/".$machineId. "_". $userId . "_" . $logTime. "." .$fileExt;
	    file_put_contents($file, $data);
		
		$insertLogId = $this->AM->addReportImage($factoryId,$captureTime, $logTime, $fileExt, $machineId, $userId);  
		$file_path =  ENV_URL."reportedImages/".$factoryId."/"  .$machineId. "_". $userId . "_" . $logTime. "." .$fileExt;
	    
		
		$machine = $this->AM->machineExistsNew($machineId);  
		//Send mail with factory,machine,Timestamp,Color,State details
		$msg = "Please check the following detection issue.
			    <br>Factory - ". base64_decode($checkFactoryDetail[0]['factoryName']) ."
			    <br>Machine - ".$machine['machineName']."
			    <br>Timestamp  - ".$timestamp."
			    <br>Color - ".$color."
			    <br>State - ".$state;

		$subject = "Dashboard detection issue";	
		$this->AM->sendEmail($msg,$subject,$file_path,$fileExt);
		echo 1;
	}

	//notificationLog function use for get notification log for operator sended notification

	//In use
    function notificationLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$result = $this->AM->getNotificationLog($this->factoryId);
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('notificationLog',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/notificationLog_footer');		
	}

	//notificationLog_pagination function use for get notification log details

	//In use
	public function notificationLog_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$notificationId = $this->input->post('notificationId');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		//Get all data count
		$listDataLogsCount = $this->AM->getallNotificationLogCount($this->factoryId, $notificationId, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchNotificationLogCount($this->factoryId, $notificationId, $is_admin)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getallNotificationLogLogs($this->factoryId, $notificationId, $is_admin, $row, $rowperpage)->result_array();  
		foreach ($listDataLogs as $key => $value) 
		{
		
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	//In use
	public function dashboard4() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachines($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('dashboard4', $data);
		$this->load->view('footer');
		$this->load->view('script_file/dashboard4_footer');
	}

	//Not in use

	public function dashboard4_pagination()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
				$machineNoStacklight = $this->AM->getWhereSingle(array("isDeleted" => '0','machineId' => $machineId), 'machine');
				//print_r($machineNoStacklight);exit;
				$noStacklight = $machineNoStacklight->noStacklight;
			}else
			{
				$noStacklight = "0";
			}
		}

		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');
		$resultActualProductionRunning = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','running');
		$ActualProductionRunning = $resultActualProductionRunning->countVal;
		$resultActualProductionWaiting = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','waiting');
		$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
		$resultActualProductionStopped = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','stopped');
		$ActualProductionStopped = $resultActualProductionStopped->countVal;
		$resultActualProductionOff = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','off');
		$ActualProductionOff = $resultActualProductionOff->countVal;
		$resultActualProductionNodet = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','nodet');
		$ActualProductionNodet = $resultActualProductionNodet->countVal;

		$resultActualProductionNoStacklight = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'0','noStacklight');
		$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToDay($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToDay($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToDay($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToDay($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToDay($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToDay($ActualProductionNoStacklight) : 0;
		}

		$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;
		$resultSetupRunning = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','running');
		$SetupRunning = $resultSetupRunning->countVal;
		$resultSetupWaiting = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','waiting');
		$SetupWaiting = $resultSetupWaiting->countVal;
		$resultSetupStopped = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','stopped');
		$SetupStopped = $resultSetupStopped->countVal;
		$resultSetupOff = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','off');
		$SetupOff = $resultSetupOff->countVal;
		$resultSetupNodet = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','nodet');

		$resultSetupNoStacklight = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'1','noStacklight');
		$SetupNoStacklight = $resultSetupNoStacklight->countVal;

		$SetupNodet = $resultSetupNodet->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			$SetupRunning = !empty($SetupRunning) ? $this->secondsToHours($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ? $this->secondsToHours($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ? $this->secondsToHours($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ? $this->secondsToHours($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ? $this->secondsToHours($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{	
			$SetupRunning = !empty($SetupRunning) ?  $this->secondsToDay($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ?  $this->secondsToDay($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ?  $this->secondsToDay($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ?  $this->secondsToDay($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ?  $this->secondsToDay($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}

		$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
		
		$resultNoProductionRunning = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','running');
		$NoProductionRunning = $resultNoProductionRunning->countVal;
		$resultNoProductionWaiting = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','waiting');
		$NoProductionWaiting = $resultNoProductionWaiting->countVal;
		$resultNoProductionStopped = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','stopped');
		$NoProductionStopped = $resultNoProductionStopped->countVal;
		$resultNoProductionOff = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','off');
		$NoProductionOff = $resultNoProductionOff->countVal;
		$resultNoProductionNodet = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','nodet');
		$NoProductionNodet = $resultNoProductionNodet->countVal;

		$resultNoProductionNoStacklight = $this->AM->getMachineStatusDataDashboard4($machineId,$choose1,$choose2,'2','noStacklight');
		$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			$NoProductionRunning =  !empty($NoProductionRunning) ? $this->secondsToHours($NoProductionRunning) : 0;
			$NoProductionWaiting =  !empty($NoProductionWaiting) ? $this->secondsToHours($NoProductionWaiting) : 0;
			$NoProductionStopped =  !empty($NoProductionStopped) ? $this->secondsToHours($NoProductionStopped) : 0;
			$NoProductionOff =  !empty($NoProductionOff) ? $this->secondsToHours($NoProductionOff) : 0;
			$NoProductionNodet =  !empty($NoProductionNodet) ? $this->secondsToHours($NoProductionNodet) : 0;
			$NoProductionNoStacklight =  !empty($NoProductionNoStacklight) ? $this->secondsToHours($NoProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			$NoProductionRunning = !empty($NoProductionRunning) ?  $this->secondsToDay($NoProductionRunning) : 0;
			$NoProductionWaiting = !empty($NoProductionWaiting) ?  $this->secondsToDay($NoProductionWaiting) : 0;
			$NoProductionStopped = !empty($NoProductionStopped) ?  $this->secondsToDay($NoProductionStopped) : 0;
			$NoProductionOff = !empty($NoProductionOff) ?  $this->secondsToDay($NoProductionOff) : 0;
			$NoProductionNodet = !empty($NoProductionNodet) ?  $this->secondsToDay($NoProductionNodet) : 0;
			$NoProductionNoStacklight = !empty($NoProductionNoStacklight) ?  $this->secondsToDay($NoProductionNoStacklight) : 0;
		}

		$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

		$Running = array();
		$Waiting = array();
		$Stopped = array();
		if ($choose1 == "day") 
		{
			$xAxisName = "HOURS";
			$yAxisName = "SECONDS";
			$xAxisData = ['1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];

			for ($i=0; $i < 24; $i++) 
			{ 
				$runningQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'running');
				$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

				$waitingQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'waiting');
				$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

				$stoppedQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'stopped');
				$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

				$offQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'off');
				$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

				$nodetQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'nodet');
				$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

				$machineSetupQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'setup');
				$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

				$machineNoProducationQuery = $this->AM->getHourDataDashboard4($machineId,$i,$choose2,'no_producation');
				$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

				$machineActualProducationQuery = $this->AM->getHourDataActualProducationDashboard4($machineId,$i,$choose2,'running');
				$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
			}

			$maxValue = round(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23])
			)));
		}
		elseif ($choose1 == "weekly") 
		{
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$xAxisData = array();
        	
			$choose2 = explode("/", $choose2);
        	$week = $choose2[0];
        	$year = $choose2[1];
        
	        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
	        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
	        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
	        $date_for_monday = date( 'Y-m-d', strtotime($date_for_monday ."-7 days") );

	        for ($i=0; $i < 7; $i++) 
	        { 
	        	$weekDate = date('Y-m-d',strtotime($date_for_monday . "+". $i." days"));
	        	$weekDateName = date('d-M-Y',strtotime($date_for_monday . "+". $i." days"));
	        	$xAxisData[] = $weekDateName;

	        	$runningQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'running');
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningSecond);
			
				$waitingQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'waiting');
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingSecond);

				$stoppedQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'stopped');
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedSecond);

				$offQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'off');
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffSecond);

				$nodetQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'nodet');
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetSecond);

				$machineSetupQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'setup');
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupSecond);

				$machineNoProducationQuery = $this->AM->getWeekDataDashboard4($machineId,$weekDate,$year,'no_producation');
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getWeekDataActualProducationDashboard4($machineId,$weekDate,$year,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);
	        }

	        $maxValue = round(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6])
			)));

		}
		elseif ($choose1 == "monthly") 
		{
			$xAxisData = array();
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$chooseExplode = explode(" ", $choose2);
            $month = $chooseExplode[0];

        	if ($month == "Jan") 
        	{
        		$month = "1";
        	}
        	elseif ($month == "Feb") 
        	{
        		$month = "2";
        	}
        	elseif ($month == "Mar") 
        	{
        		$month = "3";
        	}
        	elseif ($month == "Apr") 
        	{
        		$month = "4";
        	}
        	elseif ($month == "May") 
        	{
        		$month = "5";
        	}
        	elseif ($month == "Jun") 
        	{
        		$month = "6";
        	}
        	elseif ($month == "Jul") 
        	{
        		$month = "7";
        	}
        	elseif ($month == "Aug") 
        	{
        		$month = "8";
        	}
        	elseif ($month == "Sep") 
        	{
        		$month = "9";
        	}
        	elseif ($month == "Oct") 
        	{
        		$month = "10";
        	}
        	elseif ($month == "Nov") 
        	{
        		$month = "11";
        	}
        	elseif ($month == "Dec") 
        	{
        		$month = "12";
        	}

            $year = $chooseExplode[1];
			$maxDays=date('t',strtotime('01-'.$month.'-'.$year));
			for($w=1;$w<=$maxDays;$w++) 
			{
				$xAxisData[] = "Day ".$w;
				$runningQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'running');
				$RunningMonthSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningMonthSecond);
				$waitingQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'waiting');
				$WaitingMonthSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingMonthSecond);

				$stoppedQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'stopped');
				$StoppedMonthSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedMonthSecond);

				$offQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'off');
				$OffMonthSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffMonthSecond);

				$nodetQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'nodet');
				$NodetMonthSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetMonthSecond);

				$machineSetupQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'setup');
				$MachineSetupMonthSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupMonthSecond);

				$machineNoProducationQuery = $this->AM->getMonthDataDashboard4($machineId,$w,$month,$year,'no_producation');
				$machineNoProducationMonthSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationMonthSecond);

				$machineActualProducationQuery = $this->AM->getMonthDataActualProducationDashboard4($machineId,$w,$month,$year,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);
			}
			
			$maxValue = round(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23]),
				($Running[24] + $Waiting[24] + $Stopped[24] + $Off[24] + $Nodet[24] + $MachineSetup[24] + $MachineNoProducation[24] + $MachineActualProducation[24]),
				($Running[25] + $Waiting[25] + $Stopped[25] + $Off[25] + $Nodet[25] + $MachineSetup[25] + $MachineNoProducation[25] + $MachineActualProducation[25]),
				($Running[26] + $Waiting[26] + $Stopped[26] + $Off[26] + $Nodet[26] + $MachineSetup[26] + $MachineNoProducation[26] + $MachineActualProducation[26]),
				($Running[27] + $Waiting[27] + $Stopped[27] + $Off[27] + $Nodet[27] + $MachineSetup[27] + $MachineNoProducation[27] + $MachineActualProducation[27]),
				($Running[28] + $Waiting[28] + $Stopped[28] + $Off[28] + $Nodet[28] + $MachineSetup[28] + $MachineNoProducation[28] + $MachineActualProducation[28]),
				($Running[29] + $Waiting[29] + $Stopped[29] + $Off[29] + $Nodet[29] + $MachineSetup[29] + $MachineNoProducation[29] + $MachineActualProducation[29]),
				($Running[30] + $Waiting[30] + $Stopped[30] + $Off[30] + $Nodet[30] + $MachineSetup[30] + $MachineNoProducation[30] + $MachineActualProducation[30])
			)));

		}
		elseif ($choose1 == "yearly") 
		{
			$xAxisName = "MONTH";
			$yAxisName = "DAY";
			$xAxisData = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			for ($i=1; $i < 13; $i++) 
			{ 
				$runningQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'running');
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToDay($RunningSecond);

				$waitingQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'waiting');
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToDay($WaitingSecond);

				$stoppedQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'stopped');
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToDay($StoppedSecond);

				$offQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'off');
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToDay($OffSecond);

				$nodetQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'nodet');
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToDay($NodetSecond);

				$machineSetupQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'setup');
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToDay($MachineSetupSecond);

				$machineNoProducationQuery = $this->AM->getYearDataDashboard4($machineId,$i,$choose2,'no_producation');
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToDay($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getYearDataActualProducationDashboard4($machineId,$i,$choose2,'running');
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToDay($machineActualProducationQuerySecond);
			}

			$maxValue = round(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11])
			)));
		}

		if (!empty($ActualProduction))
		{
			$ActualProductionLength = intval("-45");
		}
		else
		{
			$ActualProductionLength = intval("-20");
		}
		if (!empty($Setup))
		{
			$SetupLength = intval("-45");
		}
		else
		{
			$SetupLength = intval("-20");
		}
		if (!empty($NoProduction))
		{
			$NoProductionLength = intval("-45");
		}
		else
		{
			$NoProductionLength = intval("-20");
		}

		$json = array(
				"ActualProductionRunning" => !empty($ActualProductionRunning) ? $ActualProductionRunning : "0",
				"ActualProductionWaiting" => !empty($ActualProductionWaiting) ? $ActualProductionWaiting : "0",
				"ActualProductionStopped" => !empty($ActualProductionStopped) ? $ActualProductionStopped : "0",
				"ActualProductionOff" => !empty($ActualProductionOff) ? $ActualProductionOff : "0",
				"ActualProductionNodet" => !empty($ActualProductionNodet) ? $ActualProductionNodet : "0",
				"ActualProductionNoStacklight" => !empty($ActualProductionNoStacklight) ? $ActualProductionNoStacklight : "0",
				"ActualProduction" => !empty($ActualProduction) ? $ActualProduction : "0",
				"ActualProductionLength" => $ActualProductionLength,
				"SetupRunning" => !empty($SetupRunning) ? $SetupRunning : "0",
				"SetupWaiting" => !empty($SetupWaiting) ? $SetupWaiting : "0",
				"SetupStopped" => !empty($SetupStopped) ? $SetupStopped : "0",
				"SetupOff" => !empty($SetupOff) ? $SetupOff : "0",
				"SetupNodet" => !empty($SetupNodet) ? $SetupNodet : "0",
				"Setup" => !empty($Setup) ? $Setup : "0",
				"SetupLength" => $SetupLength,
				"NoProductionRunning" => !empty($NoProductionRunning) ? $NoProductionRunning : "0",
				"NoProductionWaiting" => !empty($NoProductionWaiting) ? $NoProductionWaiting : "0",
				"NoProductionStopped" => !empty($NoProductionStopped) ? $NoProductionStopped : "0",
				"NoProductionOff" => !empty($NoProductionOff) ? $NoProductionOff : "0",
				"NoProductionNodet" => !empty($NoProductionNodet) ? $NoProductionNodet : "0",
				"NoProduction" => !empty($NoProduction) ? $NoProduction : "0",
				"NoProductionLength" => $NoProductionLength,
				"xAxisName" => $xAxisName,
				"yAxisName" => $yAxisName,
				"xAxisData" => $xAxisData,
				"Running" => $Running,
				"Waiting" => $Waiting,
				"Stopped" => $Stopped,
				"Off" => $Off,
				"Nodet" => $Nodet,
				"MachineSetup" => $MachineSetup,
				"MachineNoProducation" => $MachineNoProducation,
				"MachineActualProducation" => $MachineActualProducation,
				"maxValue" => $maxValue,
				"noStacklight" => $noStacklight,
				"machineId" => $machineId,
				"NonProductiveTime" => $Setup + $NoProduction
			);
		echo json_encode($json);
	}

	public function switchLang($language = "") 
	{
		$this->session->set_userdata('site_lang', $language);
		$data = array("languageType" => $language);
		$where = array("userId" => $this->session->userdata('userId'));
		$this->AM->updateDBData($where,$data, 'user');
		redirect('Admin/overview');
	}

	public function languageTesting() 
	{
		
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('languageTesting');
		$this->load->view('footer');
	}


	public function add_save_parts() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineId','machineId','required');

        
		$partsNumber =  $this->input->post('partsNumber');
		$partsName =  $this->input->post('partsName');
		$partsOperation =  $this->input->post('partsOperation');
		$operationRowId =  $this->input->post('operationRowId');
		$patrsCycleTime =  $this->input->post('patrsCycletime');
		$ReportedRestQuantity =  $this->input->post('ReportedRestQuantity');
		$ReportedQuantity =  $this->input->post('ReportedQuantity');
		$ReportNumber =  $this->input->post('ReportNumber');
		$PartId =  $this->input->post('PartId');
		$TimeUnit =  $this->input->post('TimeUnit');
		$PlannedStartDate =  $this->input->post('PlannedStartDate');
		$PlannedFinishDate =  $this->input->post('PlannedFinishDate');
		$ManufacturingOrderId =  $this->input->post('ManufacturingOrderId');

        //checking required parameters validation
        if($this->form_validation->run()==false || empty($partsNumber) || empty($partsName) || empty($partsOperation) || empty($patrsCycleTime)) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetails);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$machineId = $this->input->post('machineId');


        	$machine = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");
        	$count = count($partsNumber);
        	$machineAddedCount = 0;
        	for ($i=0; $i < $count; $i++) 
        	{ 

	        		$monitorData = array(
	    				"partsNumber" => $partsNumber[$i],
	    				"partsName" => $partsName[$i],
	    				"partsOperation" => $partsOperation[$i],
	    				"patrsCycleTime" => $patrsCycleTime[$i],
	    				"ReportedRestQuantity" => !empty($ReportedRestQuantity) ? $ReportedRestQuantity[$i] : null,
	    				"ReportedQuantity" => !empty($ReportedQuantity) ? $ReportedQuantity[$i] : null,
	    				"ReportNumber" => !empty($ReportNumber) ? $ReportNumber[$i] : null,
	    				"operationRowId" => !empty($operationRowId) ? $operationRowId[$i] : null,
	    				"PartId" => !empty($PartId) ? $PartId[$i] : null,
	    				"machineId" => $machineId,
	    				"ManufacturingOrderId" => !empty($ManufacturingOrderId) ? $ManufacturingOrderId[$i] : null,
	    				"TimeUnit" => !empty($TimeUnit) ? $TimeUnit[$i] : null,
	    				"PlannedStartDate" => !empty($PlannedStartDate) ? $PlannedStartDate[$i] : null,
	    				"PlannedFinishDate" => !empty($PlannedFinishDate) ? $PlannedFinishDate[$i] : null,
	    			);

	    			if (!empty($operationRowId)) 
	    			{
	    				$checkPart = $this->AM->getWhereSingle( array("operationRowId" => $operationRowId[$i]), "machineParts");

		    			if (!empty($checkPart)) 
		    			{
		    				$this->AM->updateData(array("operationRowId" => $operationRowId[$i]),$monitorData,"machineParts");
		    			}else
		    			{
		    				$this->AM->insertData($monitorData,"machineParts");
		    				$machineAddedCount++;
		    			}
	    			}else
	    			{
	    				$this->AM->insertData($monitorData,"machineParts");
	    				$machineAddedCount++;
	    			}
	        		
        	}
			

        	if ($machineAddedCount == 0) 
        	{
        		$message = Latestpartsaddedalready.".";
        	}else
        	{
        		$message = $machineAddedCount." ". partsadded ." <b>". $machine->machineName ."</b> ".succesfully;
        	}

		    $json = array("status" => ($machineAddedCount == 0) ? "0" : "1","message" => $message);
	        echo json_encode($json); die;
        }
		
	}

	//edit_save_parts function use for update part data
	public function edit_save_parts() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineId','machineId','required');

        
		$machinePartsId =  $this->input->post('machinePartsId');
		$partsNumber =  $this->input->post('partsNumber');
		$partsName =  $this->input->post('partsName');
		$partsOperation =  $this->input->post('partsOperation');
		$patrsCycleTime =  $this->input->post('patrsCycletime');
        //checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetails);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$machineId = $this->input->post('machineId');

        	$whereAll = array("machineId" => $machineId);
        	$dataAll = array("isDelete" => "1");
        	$this->AM->updateData($whereAll,$dataAll,"machineParts");

        	//checking required parameters validation
        	if (!empty($partsNumber) || !empty($partsName) || !empty($partsOperation) || !empty($patrsCycleTime)) 
        	{
	        	$count = count($partsNumber);

	        	for ($i=0; $i < $count; $i++) 
	        	{ 
	        		$data = array(
	        			"machineId" => $machineId,
	        			"partsNumber" => $partsNumber[$i],
	        			"partsName" => $partsName[$i],
	        			"partsOperation" => $partsOperation[$i],
	        			"patrsCycleTime" => $patrsCycleTime[$i],
	        			"isDelete" => "0"
	        		);
	        		$where = array("machinePartsId" => $machinePartsId[$i]);
	        		$this->AM->updateData($where,$data,"machineParts");
	        	}
        	}
			
		    $json = array("status" => "1","message" => Partsupdatedsuccessfully);
	        echo json_encode($json); die;
        }
		
	}

	//getPartsList function use for get parts data by machine
	public function getPartsList() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('machineId','machineId','required');

        
        if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetails);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$machineId = $this->input->post('machineId');

        	$machine = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");
        	$result = $this->AM->getWhere(array("machineId" => $machineId,"isDelete" => "0"),"machineParts");
        	$isMonitor = $this->session->userdata("isMonitor");
        	if ($isMonitor == "0") 
        	{
        		$htmlData = '<tr><td style="border-top: none;">'. Partsnumber .'</td><td style="border-top: none;width: 25%;">'. Partsname .'</td><td style="border-top: none;">'. Operation .'</td><td style="border-top: none;">'. Cycletime .'</td><td style="border-top: none;width: 20%;"></td></tr>';
        	}else
        	{
        		$htmlData = '<tr><td style="border-top: none;">'. Ordernr .'</td><td style="border-top: none;">'. Partsnumber .'</td><td style="border-top: none;width: 25%;">'. Partsname .'</td><td style="border-top: none;">'. Operation .'</td><td style="border-top: none;">'. Cycletime .'</td></tr>';
        	}
        	foreach ($result as $key => $value) 
        	{
        		$incrementVal = $key + 1;
        		if ($value['currentOrderStatus'] == "0" && empty($value['OperationRowId'])) 
        		{
        			$editData = '<a style="background-color: white;border-color: white; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="editPartsRow('.$incrementVal.');" href="javascript:void(0);" class="btn btn-primary editPartsButton editDeleteButton'.$incrementVal.'"><img style="width: 23px;height: 23px;" src="'. base_url("assets/nav_bar/create.svg") .'"></a><a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 5px;" onclick="removePartsRow('.$incrementVal.');" href="javascript:void(0);" class="btn btn-primary editDeleteButton'.$incrementVal.'"><img style="width: 23px;height: 23px;" src="'. base_url("assets/nav_bar/delete_white.svg") .'"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="confirmPartsRow('.$incrementVal.');" href="javascript:void(0);" class="confirmDeleteButton'.$incrementVal.'"><img style="width: 23px;height: 23px;" src="'. base_url("assets/nav_bar/confirmbutton.svg") .'"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;" onclick="setOriginalValuePartsRow('.$incrementVal.');" href="javascript:void(0);" class="confirmDeleteButton'.$incrementVal.'"><img style="width: 23px;height: 23px;" src="'. base_url("assets/nav_bar/closebutton.svg") .'"></a>';
        		}else
        		{	
        			if ($value['currentOrderStatus'] == "0" && empty($value['OperationRowId'])) 
        			{
        				$editData = "";	
        			}else
        			{
        				$editData = "";
        			}
        		}

        		if ($isMonitor == "0") 
	        	{
	        		$htmlData .= '<tr id="rowPartsId'.$incrementVal.'"><input type="hidden" name="machinePartsId[]" value="'.$value['machinePartsId'].'"><input id="partsNumberOld'.$incrementVal.'" type="hidden" value="'.$value['partsNumber'].'"><input id="partsNameOld'.$incrementVal.'" type="hidden" value="'.$value['partsName'].'"><input id="patrsOperationOld'.$incrementVal.'" type="hidden" value="'.$value['partsOperation'].'"><input id="partsCycleTimeOld'.$incrementVal.'" type="hidden" value="'.$value['patrsCycleTime'].'"><td style="border-top: none;"><input id="partsNumber'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsNumber[]" value="'.$value['partsNumber'].'"></td><td style="border-top: none;"><input id="partsName'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;color: #002060;font-weight: 700;" readonly name="partsName[]" value="'.$value['partsName'].'"></td><td style="border-top: none;"><input id="patrsOperation'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsOperation[]" value="'.$value['partsOperation'].'"></td><td style="border-top: none;"><div class="placeholder2" data-placeholder="'.sec.'"><input id="partsCycleTime'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="patrsCycletime[]" value="'.$value['patrsCycleTime'].'"></div></td><td style="text-align: center;border-top: none;width: 15%;">'. $editData .'</td></tr>';
	        	}else
	        	{
	        		$htmlData .= '<tr id="rowPartsId'.$incrementVal.'"><input type="hidden" name="machinePartsId[]" value="'.$value['machinePartsId'].'"><input id="partsNumberOld'.$incrementVal.'" type="hidden" value="'.$value['partsNumber'].'"><input id="partsNameOld'.$incrementVal.'" type="hidden" value="'.$value['partsName'].'"><input id="patrsOperationOld'.$incrementVal.'" type="hidden" value="'.$value['partsOperation'].'"><input id="partsCycleTimeOld'.$incrementVal.'" type="hidden" value="'.$value['patrsCycleTime'].'"><td style="border-top: none;"><input id="ManufacturingOrderId'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="ManufacturingOrderId[]" value="'.$value['ManufacturingOrderId'].'"><br>('. $value['OperationRowId']  .')</td><td style="border-top: none;"><input id="partsNumber'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsNumber[]" value="'.$value['partsNumber'].'"></td><td style="border-top: none;"><input id="partsName'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;color: #002060;font-weight: 700;" readonly name="partsName[]" value="'.$value['partsName'].'"></td><td style="border-top: none;"><input id="patrsOperation'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsOperation[]" value="'.$value['partsOperation'].'"></td><td style="border-top: none;"><div class="placeholder2" data-placeholder="'.sec.'"><input id="partsCycleTime'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="patrsCycletime[]" value="'.$value['patrsCycleTime'].'"></div></td></tr>';
	        	}
        	}
        	
		    $json = array("status" => !empty($result) ? "1" : "0","htmlData" => $htmlData,"machine" => $machine);
	        echo json_encode($json);
        }
		
	}

	//getPartsListWithApi function use for get parts data by monitor api
	public function getPartsListWithApi()
	{

		$workcenterId = $this->input->post('workcenterId');
		$data = array(
		      'Username' => 'API_USER',
		      'Password' => '',
		      'ForceRelogin' => true,
		  );
		    
		$post_data = json_encode($data);


		$username='Laufey';
		$password='Cdee3232';

		$ch = curl_init();
		curl_setopt($ch, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/login");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post_data);  //Post Fields
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

		$headers = [
		    'Accept: application/json',
		    'Host:srv01.precima.local:8001',
		    'Content-Type: application/json',
		    'Cache-Control: no-cache',
		];
		curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($ch, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);

		$server_output = curl_exec ($ch);

		curl_close ($ch);

		$server_output = json_decode($server_output);

		$SessionId =  $server_output->SessionId;

		$username='Laufey';
		$password='Cdee3232';

		$chManufacturing = curl_init();
		$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
		curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrderOperations?\$expand=Part&\$expand=OperationRow&\$filter=WorkCenterId%20eq%20".$workcenterId."%20AND%20isnull(ActualFinishDate)%20AND%20OperationRowId%20Neq%20''");
		curl_setopt($chManufacturing, CURLOPT_POST, 0);
		curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

		$ManufacturingHeaders = [
		    'Accept: application/json',
		    'Cache-Control:no-cache',
		    'Host: 212.16.184.90:8001',
		    'X-Monitor-SessionId: '.$SessionId,
		    'Content-Type: application/json'
		];

		curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
		curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
		curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
		curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
		curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

		$ManufacturingOutput = curl_exec ($chManufacturing);
		curl_close ($chManufacturing);

		$ManufacturingPostData = json_decode($ManufacturingOutput);
		foreach ($ManufacturingPostData as $key => $value) 
    	{
    		$incrementVal = $key + 1;
    		
    		
    		$ManufacturingOrderId = $this->getMonitorOrderNumber($SessionId,$value->ManufacturingOrderId);
    		$TimeUnit = $value->OperationRow->TimeUnit;

    		$patrsCycleTime = (!empty($value->OperationRow->UnitTime)) ? $value->OperationRow->UnitTime / 10000000: 0;
    		

    		$editData = "";	
    		$htmlData .= '<tr id="rowPartsId'.$incrementVal.'"><input type="hidden" name="ReportedRestQuantity[]" value="'.$value->RestQuantity.'"><input type="hidden" name="ReportedQuantity[]" value="'.$value->ReportedQuantity.'"><input type="hidden" name="ReportNumber[]" value="'.$value->ReportNumber.'"><input type="hidden" name="PartId[]" value="'.$value->PartId.'"><input type="hidden" name="TimeUnit[]" value="'.$value->OperationRow->TimeUnit.'"><input type="hidden" name="PlannedStartDate[]" value="'.$value->PlannedStartDate.'"><input type="hidden" name="PlannedFinishDate[]" value="'.$value->PlannedFinishDate.'"><input type="hidden" name="operationRowId[]" value="'.$value->Id.'"><td style="border-top: none;"><input id="ManufacturingOrderId'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="ManufacturingOrderId[]" value="'.$ManufacturingOrderId.'">('. $value->Id .')</td><td style="border-top: none;"><input id="partsNumber'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsNumber[]" value="'.$value->Part->PartNumber.'"></td><td style="border-top: none;"><input id="partsName'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;color: #002060;font-weight: 700;" readonly name="partsName[]" value="'.$value->Part->Description.'"></td><td style="border-top: none;"><input id="patrsOperation'.$incrementVal.'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsOperation[]" value="'.$value->OperationRow->Description.'"></td><td style="border-top: none;"><div class="placeholder2" data-placeholder="'.sec.'"><input id="partsCycleTime'.$incrementVal.'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="patrsCycletime[]" value="'.$patrsCycleTime.'"></div></td></tr>';
    	}
    	
	    $json = array("status" => !empty($ManufacturingPostData) ? "1" : "0","htmlData" => $htmlData,"incrementVal" => $incrementVal);

	    echo json_encode($json);

	}

	public function getMonitorOrderNumber($SessionId,$ManufacturingOrderId)
	{
			$username='Laufey';
			$password='Cdee3232';

			$chManufacturing = curl_init();
			$q =  curl_escape($chManufacturing ,'SHOW MEASUREMENTS With spaces');
			curl_setopt($chManufacturing, CURLOPT_URL,"https://212.16.184.90:8001/sv/001.1/api/v1/Manufacturing/ManufacturingOrders?\$filter=Id%20eq%20".$ManufacturingOrderId);
			curl_setopt($chManufacturing, CURLOPT_POST, 0);
			curl_setopt($chManufacturing, CURLOPT_RETURNTRANSFER, true);

			$ManufacturingHeaders = [
			    'Accept: application/json',
			    'Cache-Control:no-cache',
			    'Host: 212.16.184.90:8001',
			    'X-Monitor-SessionId: '.$SessionId,
			    'Content-Type: application/json'
			];

			curl_setopt($chManufacturing, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			curl_setopt($chManufacturing, CURLOPT_USERPWD, "$username:$password");
			curl_setopt($chManufacturing, CURLOPT_HTTPHEADER, $ManufacturingHeaders);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYHOST, false);
			curl_setopt($chManufacturing, CURLOPT_SSL_VERIFYPEER, false);

			$ManufacturingOutput = curl_exec ($chManufacturing);
			curl_close ($chManufacturing);

			$ManufacturingPostData = json_decode($ManufacturingOutput);
			
			return $ManufacturingPostData[0]->OrderNumber;
    }

	//minutesToSeconds function use for convert minutes to seconds
	function minutesToSeconds($minutes) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert minutes to seconds
		$seconds = $minutes * 60;
		return $seconds;
    }

    //hourToSeconds function use for convert hour to seconds
    function hourToSeconds($hour) 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		//convert hour to seconds
		$seconds = $hour * 3600;
		return $seconds;
    }
}

?>