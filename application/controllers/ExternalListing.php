<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class ExternalListing extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);
	}


	public function setAppErrorLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;

		$deviceNames = $this->AM->getalldeviceName($this->factory)->result_array();
		$data['ListdeviceNames'] = $this->AM->getalldeviceName($this->factory);
		$data['deviceNames'] = $deviceNames;

		// $data['deviceName']=$this->AM->getalldeviceName($this->factory);

		// $data['deviceName'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0"),groupby("deviceName"),"user");
		
		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('setAppErrorLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/setAppErrorLog_footer');		
	}

	public function setAppErrorLog_pagination() 
	{ 

		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin.'.'); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		
		
		$dueDateValS = $this->input->post('dueDateValS');
		$dueDateValE = $this->input->post('dueDateValE');
		$machineId = $this->input->post('machineId');

		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];

		$searchValue = $_POST['search']['value']; 
		$searchQuery = "";

		if (!empty($searchValue)) 
		{
			$searchQuery = " and (
					deviceName like '%".$searchValue."%' or 
					appVersion like '%".$searchValue."%' or 
					level like '%".$searchValue."%' or
					tag like '%".$searchValue."%' or
					message like '%".$searchValue."%' or 
					stacktrace like '%".$searchValue."%' or
					osVersion like '%".$searchValue."%'
					) ";  
		}

		//Get all data count
		$listDataLogsCount = $this->AM->getAllSetAppErrANDLogCount()->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchSetAppErrorLogCount($machineId,$dueDateValS,$dueDateValE,$searchQuery)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getAllSetAppErrorLogLogs($row,$rowperpage,$machineId,$dueDateValS,$dueDateValE,$searchQuery)->result_array();  

		foreach ($listDataLogs as $key => $value) 
		{
			$deviceName = "'".$value['deviceName']."'";
			$appVersion = "'".$value['appVersion']."'";
			$level 		= "'".$value['level']."'";
			$tag 		= "'".$value['tag']."'";
			$message 	= "'".$value['message']."'";
			$stacktrace = "'".$value['stacktrace']."'";
			$osVerison 	= "'".$value['osVerison']."'";

			$listDataLogs[$key]['logTime'] = date('Y-m-d H:i:s',$value['logTime']);
			$result = $this->AM->getWhereSingle(array("machineId" => $value['machineId']),"machine");
			$listDataLogs[$key]['machineId'] = $result->machineName.'('.$value['machineId'].')';

			$listDataLogs[$key]['deviceName'] = "<a target='_blank' style='color: #002060;' href=".base_url('ExternalListing/SetAppErrorLogDetails/'.$value['machineId']).">".$value['deviceName']."</a>";
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	public function SetAppErrorLogDetails($machineId)
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$data['devices']=$this->AM->SetAppErrorLogDetailsList($machineId);
		
		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('SetAppErrorLogDetails',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/SetAppErrorLogDetails_footer.php');		
	}

	

	public function startSetAppLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('startSetAppLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/startSetAppLog_footer');		
	}

	public function startSetAppLog_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}


		$type			= $this->input->post('type');
		$userId 		= $this->input->post('userId');
		$userName 		= $this->input->post('userName');
		$machineId 		= $this->input->post('machineId');
		$machineName 	= $this->input->post('machineName');
		$startDate 		= $this->input->post('startDate');
		$dateValS 		= $this->input->post('dateValS');
		$dateValE 		= $this->input->post('dateValE');
		// print_r($machineId);exit;

		$draw 		= $_POST['draw'];
		$row 		= $_POST['start'];
		$rowperpage = $_POST['length'];


		$listDataLogsCount = $this->AM->getAllSettAppStartLogCount($factory, $is_admin)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->SettAppStartLog_pagination($start, $limit, $type, $userId, $startDate, $dateValS, $dateValE, $machineId)->row()->totalCount;


		$listDataLogs = $this->AM->getallSettAppStartLog($row, $rowperpage, $type, $userId, $startDate, $dateValS, $dateValE, $machineId)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if ($value['type'] == "0") 
			{
				$listDataLogs[$key]['type'] = Manually;
			}
			else if($value['type'] == "1") 
			{
				$listDataLogs[$key]['type'] = StartedfromOpApp;
			}
			else if($value['type'] == "2") 
			{
				$listDataLogs[$key]['type'] = StartedfromDashboard;
			}

			else if($value['type'] == "3") 
			{
				$listDataLogs[$key]['type'] = AutomaticallystartedafterTenseconds;
			}

			else if($value['type'] == "4") 
			{
				$listDataLogs[$key]['type'] = setutoproduction;
			}

			else if($value['type'] == "5") 
			{
				$listDataLogs[$key]['type'] = Noproductiontoproduction;
			}

			else if($value['type'] == "6") 
			{
				$listDataLogs[$key]['type'] = Batterytemperaturereachednormalafterhigh;
			}

			else if($value['type'] == "7") 
			{
				$listDataLogs[$key]['type'] = Batterypercentagereachednormalafterlow;
			}

			else if($value['type'] == "8") 
			{
				$listDataLogs[$key]['type'] = Ramreachedoperationvalueafterhigh;
			}
			else if($value['type'] == "9") 
			{
				$listDataLogs[$key]['type'] = Duetolowmemory;
			}
			else if($value['type'] == "10") 
			{
				$listDataLogs[$key]['type'] = RestartedfromOpApp;
			}
			else if($value['type'] == "11") 
			{
				$listDataLogs[$key]['type'] = RestartedfromDashboard;
			}
			else if($value['type'] == "12") 
			{
				$listDataLogs[$key]['type'] = SetAppcrashedandrestartedautomatically;
			}
			else if($value['type'] == "13") 
			{
				$listDataLogs[$key]['type'] = Startappwhenphonerestart;
			}
			else if($value['type'] == "14") 
			{
				$listDataLogs[$key]['type'] = SetAppautomaticallyrestartedinthirtysecondsafterunexpectedstop;
			}
			else if($value['type'] == "15") 
			{
				$listDataLogs[$key]['type'] = UnexpectedReason;
			}

			$listDataLogs[$key]['startDate'] = date('Y-m-d H:i:s',strtotime($value['startDate'] ." +1 hour"));
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		// print_r($response);exit;
		echo json_encode($response); die;
	}

	

	
	public function EmailStatus()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}


		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('emailStatus',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/emailStatus_footer');		
	}

	//EmailStatus page for information of email status. 
	public function EmailStatus_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		// print_r($this->input->post());exit;

		$dateValS 		= $this->input->post('dateValS');
		$dateValE 		= $this->input->post('dateValE');
		$emailStatusId 	= $this->input->post('emailStatusId');
		$factoryId 		= $this->input->post('factoryId');
		$MessageId 		= $this->input->post('MessageId');
		$isActive 		= $this->input->post('isActive');
		$insertTime 	= $this->input->post('insertTime');
		$message 		= $this->input->post('message');


		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		//Get all data count
		$listDataLogsCount = $this->AM->getallEmailStatusDataCount($this->db, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchEmailStatus($start, $limit, $dateValS  ,$dateValE ,$isActive ,$insertTime)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getemailStatusDataLogs($row, $rowperpage, $dateValS, $dateValE, $isActive ,$insertTime)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if ($value['isActive'] == "0") 
			{
				$listDataLogs[$key]['isActive'] = Processed;
			}
			else if($value['isActive'] == "1") 
			{
				$listDataLogs[$key]['isActive'] = Delivered;
			}
			else if($value['isActive'] == "2") 
			{
				$listDataLogs[$key]['isActive'] = Dropped;
			}
		} 
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}


	//taskMaintenance function use for get task & maintence page 
	function socketLogData()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$data['machines'] = $this->AM->getallMachines($this->factory)->result_array();
	
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('socketConnectDisConnectData',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/socket_connect_disconnect_data_footer');		
	}

	//task_maintenace_pagination function use for get task data 
	public function socket_connect_disconnect_data_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$dueDateValS = $this->input->post('dueDateValS');
		$dueDateValE = $this->input->post('dueDateValE');
		$machineId = $this->input->post('machineId');


		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		//Get all data count
		$listDataLogsCount = $this->AM->getallsocketConnectDisConnectDataCount()->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchsocketConnectDisConnectDataCount($dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getsocketConnectDisConnectDataLogs($row,$rowperpage,$dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)->result_array();  
		
		foreach ($listDataLogs as $key => $value) 
		{	
			if($value['reason'] == "client namespace disconnect" || $value['reason'] ==  "transport close" || $value['reason'] == "transport error") {
				$listDataLogs[$key]['reasonText'] = SetAppturnedoff;
			} else if($value['reason'] == 'detection disconnect') { 
				$listDataLogs[$key]['reasonText'] = SetAppisonhomescreen;
			}else if($value['reason'] == 'ping timeout') { 
				$listDataLogs[$key]['reasonText'] = Nointernetorphoneturnedoff;
			}
			 else
			{
				$listDataLogs[$key]['reasonText'] = Connected;
			}

			// if ($value['startTime'] == $value['endTime']) 
			// {
			// 	$listDataLogs[$key]['endTime'] = "";	
			// }
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	public function displaydataphone($machineId)
	{

		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$result['data']=$this->AM->display_records($machineId);
		$this->load->view('phonedirectory',$result);
		$this->load->view("footer.php");
	}

	public function machineStatusLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('machineStatusLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/machineStatusLog_footer');		
	}

	public function machineStatusLog_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$logId = $this->input->post('logId');
		$machineName = $this->input->post('machineName');
		$machineStatus = $this->input->post('machineStatus');
		$insertTime = $this->input->post('insertTime');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$listDataLogsCount = $this->AM->getAllmachineStatusLogCount($factory, $is_admin)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->machineStatusLog_pagination($start, $limit, $logId, $machineId, 
			$machineStatus, $insertTime, $dateValS, $dateValE,$machineName)->row()->totalCount;

		$listDataLogs = $this->AM->getallmachineStatusLog($row, $rowperpage, $logId, $machineId, $machineStatus, 
			$insertTime, $dateValS, $dateValE,$machineName)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if ($value['machineStatus'] == "0") 
			{
				$listDataLogs[$key]['machineStatus'] = Productiontime;
			}
			else if($value['machineStatus'] == "1") 
			{
				$listDataLogs[$key]['machineStatus'] = Setuptime;
			}
			else if($value['machineStatus'] == "2") 
			{
				$listDataLogs[$key]['machineStatus'] = Noproductiontime;
			}
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}

	public function phoneMovementDetailLogv2()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$result['data']=$this->AM->phoneMovementDetailLogv2display_records();
		$this->load->view('phoneMovementDetailLogv2',$result);
		$this->load->view("footer.php");
	}

	public function notificationListLog()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$machines = $this->AM->getallMachines($this->factory)->result_array();
		$data['listMachines'] = $this->AM->getallMachines($this->factory);
		$data['machines'] = $machines;

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('notificationListLog',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/notificationList_log_footer.php');		
	}

	public function notificationListLog_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}



		$notificationId 		= $this->input->post('notificationId');
		$notificationText 		= $this->input->post('notificationText');
		$userId 				= $this->input->post('userId');
		$userName 				= $this->input->post('userName');
		$machineId 				= $this->input->post('machineId');
		$machineName 			= $this->input->post('machineName');
		$addedDate 				= $this->input->post('addedDate');
		$flag 					= $this->input->post('flag');
		$logId 					= $this->input->post('logId');
		$status 				= $this->input->post('status');
		$comment 				= $this->input->post('comment');
		$respondTime 			= $this->input->post('respondTime');
		$responderId 			= $this->input->post('responderId');
		$commentFlag 			= $this->input->post('commentFlag');
		$type 					= $this->input->post('type');

		$dateValS 				= $this->input->post('dateValS');
		$dateValE 				= $this->input->post('dateValE');

		$dueDateValS 			= $this->input->post('dueDateValS');
		$dueDateValE 			= $this->input->post('dueDateValE');

		// echo "<pre>";print_r($this->input->post());exit;
		// print_r($userId);exit;

		$draw 		= $_POST['draw'];
		$row 		= $_POST['start'];
		$rowperpage = $_POST['length'];

		$listDataLogsCount = $this->AM->getAllnotificationListLogCount($factory)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getAllnotificationListLogpagination($start, $limit, $userId, $userName, $machineId, $machineName, $addedDate, $status, $respondTime, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)->row()->totalCount;

		// echo $listDataLogsSearchCount;exit;


		$listDataLogs = $this->AM->getallnotificationLog($row, $rowperpage, $userId, $userName, $machineId ,$machineName ,$addedDate , $status, $respondTime, $responderId, $type, $dateValS, $dateValE, $dueDateValS, $dueDateValE)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if ($value['status'] == "0") 
			{
				$listDataLogs[$key]['status'] = "Not Responded";
			}
			else if($value['status'] == "1") 
			{
				$listDataLogs[$key]['status'] = "Responded";
			}


			if ($value['flag'] == "0") 
			{
				$listDataLogs[$key]['flag'] = "Application";
			}
			else if($value['flag'] == "1") 
			{
				$listDataLogs[$key]['flag'] = "Manual";
			}


			if ($value['commentFlag'] == "0") 
			{
				$listDataLogs[$key]['commentFlag'] = "Not Respondable";
			}
			else if($value['commentFlag'] == "1") 
			{
				$listDataLogs[$key]['commentFlag'] = "Respondable";
			}
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		// print_r($response);exit;
		echo json_encode($response); die;
	}

	public function changelanguages() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('languageListing');
		$this->load->view('footer');
		$this->load->view('script_file/changelanguageListing_footer');
	}

	public function changelanguage_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}


		$type  = $this->input->post('type');

		// print_r($this->input->post());exit;



		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		$searchValue = $_POST['search']['value']; 
		$searchQuery = "";

		if (!empty($searchValue)) 
		{
			$searchQuery = " and (
					englishText like '%".$searchValue."%' or 
					swedishText like '%".$searchValue."%' or 
					type like '%".$searchValue."%' 
					) ";  
			  
		}
		
		$listDataLogsCount = $this->AM->getAllChangeLanguageCount()->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getChangeLanguageCount($start, $limit, $type, $searchQuery)->row()->totalCount;

		$listDataLogs = $this->AM->getChangeLanguageText($row, $rowperpage, $type, $searchQuery)->result_array(); 


		foreach ($listDataLogs as $key => $value) 
		{
			$englishText = "'".$value['englishText']."'";
			$swedishText = "'".$value['swedishText']."'";
			$listDataLogs[$key]['action'] = '<button class="btn btn-primary btn-sm" type="button" onclick="editLanguage('.$value['languageId'].','.$englishText.','.$swedishText.')">EDIT</button>';
			$listDataLogs[$key]['swedishText'] = '<input class="lanText" style="width: 400px!important;color: #002060;border-left: none;border-right: none;border-top: none;border-bottom-color: white;" type="text" value="'.$value['swedishText'].'" onfocusout="changeLanguage('.$value['languageId'].',this.value)" >';
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}


	// function updateSaveLanguage()
	// {
	// 	$languageId = $this->input->post('languageId');
	// 	$swedishText = $this->input->post('swedishText');

	// 	$data = array("swedishText" => $swedishText);
	// 	$where = array("languageId" => $languageId);

	// 	$this->AM->updateDBData($where,$data, "language");
	// }

	public function translations() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if ($this->session->userdata('role') == "2" ) 
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('translations');
			$this->load->view('footer');
			$this->load->view('script_file/translations_footer');
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
		
	}

	public function translations_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}


		$type  = $this->input->post('type');

		// print_r($this->input->post());exit;



		
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];

		$searchValue = $_POST['search']['value']; 
		$searchQuery = "";

		if (!empty($searchValue)) 
		{
			$searchQuery = " and (
					englishText like '%".$searchValue."%' or 
					swedishText like '%".$searchValue."%' or 
					type like '%".$searchValue."%' 
					) ";  
			  
		}
		
		$listDataLogsCount = $this->AM->getAllChangeLanguageCount_backup()->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getChangeLanguageCount_backup($start, $limit, $type, $searchQuery)->row()->totalCount;

		$listDataLogs = $this->AM->getChangeLanguageText_backup($row, $rowperpage, $type, $searchQuery, $columnName, $columnSortOrder)->result_array(); 


		foreach ($listDataLogs as $key => $value) 
		{
			$englishText = "'".$value['englishText']."'";
			$swedishText = "'".$value['swedishText']."'";
			$listDataLogs[$key]['action'] = '<button class="btn btn-primary btn-sm" type="button" onclick="editLanguage('.$value['languageId'].','.$englishText.','.$swedishText.')">EDIT</button>';
			$listDataLogs[$key]['swedishText'] = '<input class="lanText" style="width: 400px!important;color: #002060;border-left: none;border-right: none;border-top: none;border-bottom-color: white;" id="languageId'.$value['languageId'].'" type="text" value="'.$value['swedishText'].'" onkeyup="lanBorderColor('.$value['languageId'].',this.value,'.$swedishText.')"> <img onclick="changeLanguage('.$value['languageId'].')" width="16" height="16" src="https://nyttdev.com/testing_env/version4/assets/img/tick.svg">';

			$type = explode(",", $value['type']);
			$html = "";
			foreach ($type as $keyType => $valueType) 
			{
			 	

				if ($valueType == "dashboard") 
				{
					$valueType = "Dashboard";
				}
				else if($valueType == "opapp") 
				{
					$valueType = "OpApp";
				}
				else if($valueType == "opappapi") 
				{
					$valueType = "OpAppApi";
				}
				else if($valueType == "setappapi") 
				{
					$valueType = "SetAppApi";
				}else
				{
					$valueType = $valueType;
				}

				$html .= "<button class='btn btn-sm'>". ucfirst($valueType) ."</button>";
			}

			 $listDataLogs[$key]['type'] = $html;
		}

		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}


	function updateSaveLanguage_backup()
	{
		$languageId = $this->input->post('languageId');
		$swedishText = $this->input->post('swedishText');

		$data = array("swedishText" => $swedishText);
		$where = array("languageId" => $languageId);

		$this->AM->updateDBData($where,$data, "translations");
	}
}

?>