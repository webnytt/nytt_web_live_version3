<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class LiveToTest extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('LiveToTest_model','AM'); 
	}



	function getMachine()
	{
		$factoryId = $this->input->post('factoryId');

		$this->factory = $this->load->database('factory'.$factoryId, TRUE);  

		$listMachines = $this->AM->getallMachines($this->factory)->result_array();

		echo json_encode($listMachines);
	}

	function machineView()
	{
		$factoryId = $this->input->post('factoryId');

		$this->factory = $this->load->database('factory'.$factoryId, TRUE);  

		$machineView = $this->AM->getWhere(array("isDelete" => "0"), "machineView");

		echo json_encode($machineView);
	}

	//dashboard5_pagination use for get analytics graph data
	public function dashboard3_pagination()
	{

		$this->factoryId = $this->input->post('factoryId'); 
		$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
		//Check show all data or specific machine data
		$machineId = 0; 
		
		//Check show all data or specific machine data
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
				$machineNoStacklight = $this->AM->getWhereSingle(array("isDeleted" => '0','machineId' => $machineId), 'machine');
				//print_r($machineNoStacklight);exit;
				$noStacklight = $machineNoStacklight->noStacklight;
				$machineCount = 1;
			}else
			{
				//$machineCount = $this->AM->getAnalyticsMachineCount();
				$noStacklight = "0";
			}
		}

		$viewId = $this->input->post('viewId');
		if ($viewId == "none") 
		{
			$viewId = 0;
			$viewStartTime = "";
			$viewEndTime = "";
			$breakViewHour = array();
		}else
		{
			$view = $this->AM->getWhereSingle(array("viewId" => $viewId),"machineView");
			$startTime = date('H',strtotime($view->startTime));
			if ($view->endTime == "24:00:00") 
			{
				$endTime = 24;
			}else
			{
				$endTime = date('H',strtotime($view->endTime));
			}
			$viewStartTime = intVal(($startTime == "00") ? "0" : ltrim($startTime, '0'));
			$viewEndTime = intVal(ltrim($endTime, '0'));
			$machineBreakViewData = $this->AM->getWhere(array("viewId" => $viewId),"machineBreakView");

			foreach ($machineBreakViewData as $key => $value) 
			{
				$startBreakTime = date('H',strtotime($value['startTime']));
				$endBreakTime = date('H',strtotime($value['endTime']));

				for ($p=$startBreakTime; $p < $endBreakTime; $p++) 
				{
					$breakViewHour[] = intVal(($p == "00") ? "0" : ltrim($p, '0'));
				}
			}
		}

		$choose1 = $this->input->post('choose1');
		$choose2 = $this->input->post('choose2');
		//Get machine production state running data as par selecte filter
		$resultActualProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionRunning = $resultActualProductionRunning->countVal;
		//Get machine production state waiting data as par selecte filter
		$resultActualProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionWaiting = $resultActualProductionWaiting->countVal;
		//Get machine production state stopped data as par selecte filter
		$resultActualProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionStopped = $resultActualProductionStopped->countVal;
		//Get machine production state off data as par selecte filter
		$resultActualProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionOff = $resultActualProductionOff->countVal;
		//Get machine production state nodet data as par selecte filter
		$resultActualProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNodet = $resultActualProductionNodet->countVal;
		//Get machine production state noStacklight data as par selecte filter
		$resultActualProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'0','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$ActualProductionNoStacklight = $resultActualProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToHours($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToHours($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToHours($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToHours($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToHours($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToHours($ActualProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$ActualProductionRunning = !empty($ActualProductionRunning) ? $this->secondsToDay($ActualProductionRunning) : 0;
			$ActualProductionWaiting = !empty($ActualProductionWaiting) ? $this->secondsToDay($ActualProductionWaiting) : 0;
			$ActualProductionStopped = !empty($ActualProductionStopped) ? $this->secondsToDay($ActualProductionStopped) : 0;
			$ActualProductionOff = !empty($ActualProductionOff) ? $this->secondsToDay($ActualProductionOff) : 0;
			$ActualProductionNodet = !empty($ActualProductionNodet) ? $this->secondsToDay($ActualProductionNodet) : 0;
			$ActualProductionNoStacklight = !empty($ActualProductionNoStacklight) ? $this->secondsToDay($ActualProductionNoStacklight) : 0;
		}

		$ActualProduction = $ActualProductionRunning + $ActualProductionWaiting + $ActualProductionStopped + $ActualProductionOff + $ActualProductionNodet + $ActualProductionNoStacklight;
		//Get machine setup state running data as par selecte filter
		$resultSetupRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupRunning = $resultSetupRunning->countVal;
		//Get machine setup state waiting data as par selecte filter
		$resultSetupWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupWaiting = $resultSetupWaiting->countVal;
		//Get machine setup state stopped data as par selecte filter
		$resultSetupStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupStopped = $resultSetupStopped->countVal;
		//Get machine setup state off data as par selecte filter
		$resultSetupOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupOff = $resultSetupOff->countVal;
		//Get machine setup state nodet data as par selecte filter
		$resultSetupNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		//Get machine setup state noStacklight data as par selecte filter
		$resultSetupNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'1','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$SetupNoStacklight = $resultSetupNoStacklight->countVal;

		$SetupNodet = $resultSetupNodet->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$SetupRunning = !empty($SetupRunning) ? $this->secondsToHours($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ? $this->secondsToHours($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ? $this->secondsToHours($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ? $this->secondsToHours($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ? $this->secondsToHours($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{	
			//Converts seconds to day
			$SetupRunning = !empty($SetupRunning) ?  $this->secondsToDay($SetupRunning) : 0;
			$SetupWaiting = !empty($SetupWaiting) ?  $this->secondsToDay($SetupWaiting) : 0;
			$SetupStopped = !empty($SetupStopped) ?  $this->secondsToDay($SetupStopped) : 0;
			$SetupOff = !empty($SetupOff) ?  $this->secondsToDay($SetupOff) : 0;
			$SetupNodet = !empty($SetupNodet) ?  $this->secondsToDay($SetupNodet) : 0;
			$SetupNoStacklight = !empty($SetupNoStacklight) ? $this->secondsToHours($SetupNoStacklight) : 0;
		}

		$Setup = $SetupRunning + $SetupWaiting + $SetupStopped + $SetupOff + $SetupNodet + $SetupNoStacklight;
		//Get machine no production state running data as par selecte filter
		$resultNoProductionRunning = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','running',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionRunning = $resultNoProductionRunning->countVal;
		//Get machine no production state waiting data as par selecte filter
		$resultNoProductionWaiting = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','waiting',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionWaiting = $resultNoProductionWaiting->countVal;
		//Get machine no production state stopped data as par selecte filter
		$resultNoProductionStopped = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','stopped',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionStopped = $resultNoProductionStopped->countVal;
		//Get machine no production state off data as par selecte filter
		$resultNoProductionOff = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','off',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionOff = $resultNoProductionOff->countVal;
		//Get machine no production state nodet data as par selecte filter
		$resultNoProductionNodet = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','nodet',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionNodet = $resultNoProductionNodet->countVal;
		//Get machine no production state noStacklight data as par selecte filter
		$resultNoProductionNoStacklight = $this->AM->getMachineStatusData($machineId,$choose1,$choose2,'2','noStacklight',$viewStartTime,$viewEndTime,$breakViewHour);
		$NoProductionNoStacklight = $resultNoProductionNoStacklight->countVal;

		if ($choose1 == "weekly" || $choose1 == "monthly") 
		{
			//Convert seconds to hours
			$NoProductionRunning =  !empty($NoProductionRunning) ? $this->secondsToHours($NoProductionRunning) : 0;
			$NoProductionWaiting =  !empty($NoProductionWaiting) ? $this->secondsToHours($NoProductionWaiting) : 0;
			$NoProductionStopped =  !empty($NoProductionStopped) ? $this->secondsToHours($NoProductionStopped) : 0;
			$NoProductionOff =  !empty($NoProductionOff) ? $this->secondsToHours($NoProductionOff) : 0;
			$NoProductionNodet =  !empty($NoProductionNodet) ? $this->secondsToHours($NoProductionNodet) : 0;
			$NoProductionNoStacklight =  !empty($NoProductionNoStacklight) ? $this->secondsToHours($NoProductionNoStacklight) : 0;
		}
		elseif ($choose1 == "yearly") 
		{
			//Converts seconds to day
			$NoProductionRunning = !empty($NoProductionRunning) ?  $this->secondsToDay($NoProductionRunning) : 0;
			$NoProductionWaiting = !empty($NoProductionWaiting) ?  $this->secondsToDay($NoProductionWaiting) : 0;
			$NoProductionStopped = !empty($NoProductionStopped) ?  $this->secondsToDay($NoProductionStopped) : 0;
			$NoProductionOff = !empty($NoProductionOff) ?  $this->secondsToDay($NoProductionOff) : 0;
			$NoProductionNodet = !empty($NoProductionNodet) ?  $this->secondsToDay($NoProductionNodet) : 0;
			$NoProductionNoStacklight = !empty($NoProductionNoStacklight) ?  $this->secondsToDay($NoProductionNoStacklight) : 0;
		}

		$NoProduction = $NoProductionRunning + $NoProductionWaiting + $NoProductionStopped + $NoProductionOff + $NoProductionNodet + $NoProductionNoStacklight;

		$Running = array();
		$Waiting = array();
		$Stopped = array();
		if ($choose1 == "day") 
		{
			$xAxisName = "HOURS";
			$yAxisName = "SECONDS";

			if ($viewId == 0) 
			{
				$xAxisData = ['0','1','2','3','4','5','6','7','8','9','10','11','12','13','14','15','16','17','18','19','20','21','22','23','24'];
			}else
			{
				$xAxisData = array();
				for ($i=$viewStartTime - 1; $i < $viewEndTime; $i++) { 
					$xAxisData[] = $i + 1;
				}
			}


			
				//Get running hour data
				if (!empty($viewEndTime)) 
				{
					for ($i=$viewStartTime; $i < $viewEndTime; $i++) 
					{
						$machineBreakView = $this->AM->getWhere(array("viewId" => $viewId,"HOUR(startTime) <=" => $i,"HOUR(endTime) >" => $i),"machineBreakView");
						if (!empty($machineBreakView)) 
						{
						//echo $this->factory->last_query();exit;
							$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$RunningBreak[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

							//Get waiting hour data
							$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
							$WaitingBreak[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

							//Get stopped hour data
							$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
							$StoppedBreak[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

							//Get off hour data
							$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
							$OffBreak[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

							//Get nodet hour data
							$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
							$NodetBreak[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

							//Get setup hour data
							$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
							$MachineSetupBreak[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

							//Get no_producation hour data
							$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
							$MachineNoProducationBreak[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

							$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$MachineActualProducationBreak[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

							$Running[] = "0";
							$Waiting[] = "0";
							$Stopped[] = "0";
							$Off[] = "0";
							$Nodet[] = "0";
							$MachineSetup[] = "0";
							$MachineNoProducation[] = "0";
							$MachineActualProducation[] = "0";
						}else
						{
							$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

							//Get waiting hour data
							$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
							$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

							//Get stopped hour data
							$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
							$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

							//Get off hour data
							$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
							$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

							//Get nodet hour data
							$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
							$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

							//Get setup hour data
							$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
							$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

							//Get no_producation hour data
							$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
							$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

							$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
							$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

							$RunningBreak[] = "0";
							$WaitingBreak[] = "0";
							$StoppedBreak[] = "0";
							$OffBreak[] = "0";
							$NodetBreak[] = "0";
							$MachineSetupBreak[] = "0";
							$MachineNoProducationBreak[] = "0";
							$MachineActualProducationBreak[] = "0";
						}
					}
				}
				else
				{
					for ($i=0; $i < 24; $i++) 
					{ 
						$runningQuery = $this->AM->getHourData($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
						$Running[] = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";

						//Get waiting hour data
						$waitingQuery = $this->AM->getHourData($machineId,$i,$choose2,'waiting',$viewStartTime,$viewEndTime);
						$Waiting[] = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";

						//Get stopped hour data
						$stoppedQuery = $this->AM->getHourData($machineId,$i,$choose2,'stopped',$viewStartTime,$viewEndTime);
						$Stopped[] = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";

						//Get off hour data
						$offQuery = $this->AM->getHourData($machineId,$i,$choose2,'off',$viewStartTime,$viewEndTime);
						$Off[] = !empty($offQuery->countVal) ? $offQuery->countVal : "0";

						//Get nodet hour data
						$nodetQuery = $this->AM->getHourData($machineId,$i,$choose2,'nodet',$viewStartTime,$viewEndTime);
						$Nodet[] = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";

						//Get setup hour data
						$machineSetupQuery = $this->AM->getHourData($machineId,$i,$choose2,'setup',$viewStartTime,$viewEndTime);
						$MachineSetup[] = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";

						//Get no_producation hour data
						$machineNoProducationQuery = $this->AM->getHourData($machineId,$i,$choose2,'no_producation',$viewStartTime,$viewEndTime);
						$MachineNoProducation[] = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";

						$machineActualProducationQuery = $this->AM->getHourDataActualProducation($machineId,$i,$choose2,'running',$viewStartTime,$viewEndTime);
						$MachineActualProducation[] = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";

						$RunningBreak[] = "0";
						$WaitingBreak[] = "0";
						$StoppedBreak[] = "0";
						$OffBreak[] = "0";
						$NodetBreak[] = "0";
						$MachineSetupBreak[] = "0";
						$MachineNoProducationBreak[] = "0";
						$MachineActualProducationBreak[] = "0";
					}
				}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23])
			)));
		}
		elseif ($choose1 == "weekly") 
		{
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$xAxisData = array();
        	
			$choose2 = explode("/", $choose2);
        	$week = $choose2[0];
        	$year = $choose2[1];
        
	        $timestamp = mktime( 0, 0, 0, 1, 1,  $year ) + ( $week * 7 * 24 * 60 * 60 );
	        $timestamp_for_monday = $timestamp - 86400 * ( date( 'N', $timestamp ) - 1 );
	        $date_for_monday = date( 'Y-m-d', $timestamp_for_monday );
	        //$date_for_monday = date( 'Y-m-d', strtotime($date_for_monday ."-7 days") );

	        for ($i=0; $i < 7; $i++) 
	        { 
	        	$weekDate = date('Y-m-d',strtotime($date_for_monday . "+". $i." days"));
	        	$weekDateName = date('d-M-Y',strtotime($date_for_monday . "+". $i." days"));
	        	$xAxisData[] = $weekDateName;
		
				//Get running hour data
	        	$runningQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningSecond);
				//Get waiting hour data
				$waitingQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingSecond);
		
				//Get stopped hour data
				$stoppedQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedSecond);
		
				//Get off hour data
				$offQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffSecond);
		
				//Get nodet hour data
				$nodetQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetSecond);
		
				//Get setup hour data
				$machineSetupQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupSecond);
		
				//Get no_producation hour data
				$machineNoProducationQuery = $this->AM->getWeekData($machineId,$weekDate,$year,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getWeekDataActualProducation($machineId,$weekDate,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";
	        }

	        $maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6])
			)));

		}
		elseif ($choose1 == "monthly") 
		{
			$xAxisData = array();
			$xAxisName = "DAY";
			$yAxisName = "HOURS";
			$chooseExplode = explode(" ", $choose2);
            $month = $chooseExplode[0];

        	if ($month == "Jan") 
        	{
        		$month = "1";
        	}
        	elseif ($month == "Feb") 
        	{
        		$month = "2";
        	}
        	elseif ($month == "Mar") 
        	{
        		$month = "3";
        	}
        	elseif ($month == "Apr") 
        	{
        		$month = "4";
        	}
        	elseif ($month == "May") 
        	{
        		$month = "5";
        	}
        	elseif ($month == "Jun") 
        	{
        		$month = "6";
        	}
        	elseif ($month == "Jul") 
        	{
        		$month = "7";
        	}
        	elseif ($month == "Aug") 
        	{
        		$month = "8";
        	}
        	elseif ($month == "Sep") 
        	{
        		$month = "9";
        	}
        	elseif ($month == "Oct") 
        	{
        		$month = "10";
        	}
        	elseif ($month == "Nov") 
        	{
        		$month = "11";
        	}
        	elseif ($month == "Dec") 
        	{
        		$month = "12";
        	}

            $year = $chooseExplode[1];
			$maxDays=date('t',strtotime('01-'.$month.'-'.$year));
			for($w=1;$w<=$maxDays;$w++) 
			{
				$xAxisData[] = "Day ".$w;


				$runningQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningMonthSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToHours($RunningMonthSecond);
				
				$waitingQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingMonthSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToHours($WaitingMonthSecond);

				$stoppedQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedMonthSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToHours($StoppedMonthSecond);

				$offQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffMonthSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToHours($OffMonthSecond);

				$nodetQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetMonthSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToHours($NodetMonthSecond);

				$machineSetupQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupMonthSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToHours($MachineSetupMonthSecond);

				$machineNoProducationQuery = $this->AM->getMonthData($machineId,$w,$month,$year,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationMonthSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToHours($machineNoProducationMonthSecond);

				$machineActualProducationQuery = $this->AM->getMonthDataActualProducation($machineId,$w,$month,$year,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToHours($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";


			}
			
			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11]),
				($Running[12] + $Waiting[12] + $Stopped[12] + $Off[12] + $Nodet[12] + $MachineSetup[12] + $MachineNoProducation[12] + $MachineActualProducation[12]),
				($Running[13] + $Waiting[13] + $Stopped[13] + $Off[13] + $Nodet[13] + $MachineSetup[13] + $MachineNoProducation[13] + $MachineActualProducation[13]),
				($Running[14] + $Waiting[14] + $Stopped[14] + $Off[14] + $Nodet[14] + $MachineSetup[14] + $MachineNoProducation[14] + $MachineActualProducation[14]),
				($Running[15] + $Waiting[15] + $Stopped[15] + $Off[15] + $Nodet[15] + $MachineSetup[15] + $MachineNoProducation[15] + $MachineActualProducation[15]),
				($Running[16] + $Waiting[16] + $Stopped[16] + $Off[16] + $Nodet[16] + $MachineSetup[16] + $MachineNoProducation[16] + $MachineActualProducation[16]),
				($Running[17] + $Waiting[17] + $Stopped[17] + $Off[17] + $Nodet[17] + $MachineSetup[17] + $MachineNoProducation[17] + $MachineActualProducation[17]),
				($Running[18] + $Waiting[18] + $Stopped[18] + $Off[18] + $Nodet[18] + $MachineSetup[18] + $MachineNoProducation[18] + $MachineActualProducation[18]),
				($Running[19] + $Waiting[19] + $Stopped[19] + $Off[19] + $Nodet[19] + $MachineSetup[19] + $MachineNoProducation[19] + $MachineActualProducation[19]),
				($Running[20] + $Waiting[20] + $Stopped[20] + $Off[20] + $Nodet[20] + $MachineSetup[20] + $MachineNoProducation[20] + $MachineActualProducation[20]),
				($Running[21] + $Waiting[21] + $Stopped[21] + $Off[21] + $Nodet[21] + $MachineSetup[21] + $MachineNoProducation[21] + $MachineActualProducation[21]),
				($Running[22] + $Waiting[22] + $Stopped[22] + $Off[22] + $Nodet[22] + $MachineSetup[22] + $MachineNoProducation[22] + $MachineActualProducation[22]),
				($Running[23] + $Waiting[23] + $Stopped[23] + $Off[23] + $Nodet[23] + $MachineSetup[23] + $MachineNoProducation[23] + $MachineActualProducation[23]),
				($Running[24] + $Waiting[24] + $Stopped[24] + $Off[24] + $Nodet[24] + $MachineSetup[24] + $MachineNoProducation[24] + $MachineActualProducation[24]),
				($Running[25] + $Waiting[25] + $Stopped[25] + $Off[25] + $Nodet[25] + $MachineSetup[25] + $MachineNoProducation[25] + $MachineActualProducation[25]),
				($Running[26] + $Waiting[26] + $Stopped[26] + $Off[26] + $Nodet[26] + $MachineSetup[26] + $MachineNoProducation[26] + $MachineActualProducation[26]),
				($Running[27] + $Waiting[27] + $Stopped[27] + $Off[27] + $Nodet[27] + $MachineSetup[27] + $MachineNoProducation[27] + $MachineActualProducation[27]),
				($Running[28] + $Waiting[28] + $Stopped[28] + $Off[28] + $Nodet[28] + $MachineSetup[28] + $MachineNoProducation[28] + $MachineActualProducation[28]),
				($Running[29] + $Waiting[29] + $Stopped[29] + $Off[29] + $Nodet[29] + $MachineSetup[29] + $MachineNoProducation[29] + $MachineActualProducation[29]),
				($Running[30] + $Waiting[30] + $Stopped[30] + $Off[30] + $Nodet[30] + $MachineSetup[30] + $MachineNoProducation[30] + $MachineActualProducation[30])
			)));

		}
		elseif ($choose1 == "yearly") 
		{
			$xAxisName = "MONTH";
			$yAxisName = "DAY";
			$xAxisData = ['Jan','Feb','Mar','Apr','May','Jun','Jul','Aug','Sep','Oct','Nov','Dec'];
			for ($i=1; $i < 13; $i++) 
			{ 
				$runningQuery = $this->AM->getYearData($machineId,$i,$choose2,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$RunningSecond = !empty($runningQuery->countVal) ? $runningQuery->countVal : "0";
				$Running[] = $this->secondsToDay($RunningSecond);

				$waitingQuery = $this->AM->getYearData($machineId,$i,$choose2,'waiting',$breakViewHour,$viewStartTime,$viewEndTime);
				$WaitingSecond = !empty($waitingQuery->countVal) ? $waitingQuery->countVal : "0";
				$Waiting[] = $this->secondsToDay($WaitingSecond);

				$stoppedQuery = $this->AM->getYearData($machineId,$i,$choose2,'stopped',$breakViewHour,$viewStartTime,$viewEndTime);
				$StoppedSecond = !empty($stoppedQuery->countVal) ? $stoppedQuery->countVal : "0";
				$Stopped[] = $this->secondsToDay($StoppedSecond);

				$offQuery = $this->AM->getYearData($machineId,$i,$choose2,'off',$breakViewHour,$viewStartTime,$viewEndTime);
				$OffSecond = !empty($offQuery->countVal) ? $offQuery->countVal : "0";
				$Off[] = $this->secondsToDay($OffSecond);

				$nodetQuery = $this->AM->getYearData($machineId,$i,$choose2,'nodet',$breakViewHour,$viewStartTime,$viewEndTime);
				$NodetSecond = !empty($nodetQuery->countVal) ? $nodetQuery->countVal : "0";
				$Nodet[] = $this->secondsToDay($NodetSecond);

				$machineSetupQuery = $this->AM->getYearData($machineId,$i,$choose2,'setup',$breakViewHour,$viewStartTime,$viewEndTime);
				$MachineSetupSecond = !empty($machineSetupQuery->countVal) ? $machineSetupQuery->countVal : "0";
				$MachineSetup[] = $this->secondsToDay($MachineSetupSecond);

				$machineNoProducationQuery = $this->AM->getYearData($machineId,$i,$choose2,'no_producation',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineNoProducationSecond = !empty($machineNoProducationQuery->countVal) ? $machineNoProducationQuery->countVal : "0";
				$MachineNoProducation[] = $this->secondsToDay($machineNoProducationSecond);

				$machineActualProducationQuery = $this->AM->getYearDataActualProducation($machineId,$i,$choose2,'running',$breakViewHour,$viewStartTime,$viewEndTime);
				$machineActualProducationQuerySecond = !empty($machineActualProducationQuery->countVal) ? $machineActualProducationQuery->countVal : "0";
				$MachineActualProducation[] = $this->secondsToDay($machineActualProducationQuerySecond);

				$RunningBreak[] = "0";
				$WaitingBreak[] = "0";
				$StoppedBreak[] = "0";
				$OffBreak[] = "0";
				$NodetBreak[] = "0";
				$MachineSetupBreak[] = "0";
				$MachineNoProducationBreak[] = "0";
				$MachineActualProducationBreak[] = "0";
			}

			$maxValue = ceil(max(array(
				($Running[0] + $Waiting[0] + $Stopped[0] + $Off[0] + $Nodet[0] + $MachineSetup[0] + $MachineNoProducation[0] + $MachineActualProducation[0]),
				($Running[1] + $Waiting[1] + $Stopped[1] + $Off[1] + $Nodet[1] + $MachineSetup[1] + $MachineNoProducation[1] + $MachineActualProducation[1]),
				($Running[2] + $Waiting[2] + $Stopped[2] + $Off[2] + $Nodet[2] + $MachineSetup[2] + $MachineNoProducation[2] + $MachineActualProducation[2]),
				($Running[3] + $Waiting[3] + $Stopped[3] + $Off[3] + $Nodet[3] + $MachineSetup[3] + $MachineNoProducation[3] + $MachineActualProducation[3]),
				($Running[4] + $Waiting[4] + $Stopped[4] + $Off[4] + $Nodet[4] + $MachineSetup[4] + $MachineNoProducation[4] + $MachineActualProducation[4]),
				($Running[5] + $Waiting[5] + $Stopped[5] + $Off[5] + $Nodet[5] + $MachineSetup[5] + $MachineNoProducation[5] + $MachineActualProducation[5]),
				($Running[6] + $Waiting[6] + $Stopped[6] + $Off[6] + $Nodet[6] + $MachineSetup[6] + $MachineNoProducation[6] + $MachineActualProducation[6]),
				($Running[7] + $Waiting[7] + $Stopped[7] + $Off[7] + $Nodet[7] + $MachineSetup[7] + $MachineNoProducation[7] + $MachineActualProducation[7]),
				($Running[8] + $Waiting[8] + $Stopped[8] + $Off[8] + $Nodet[8] + $MachineSetup[8] + $MachineNoProducation[8] + $MachineActualProducation[8]),
				($Running[9] + $Waiting[9] + $Stopped[9] + $Off[9] + $Nodet[9] + $MachineSetup[9] + $MachineNoProducation[9] + $MachineActualProducation[9]),
				($Running[10] + $Waiting[10] + $Stopped[10] + $Off[10] + $Nodet[10] + $MachineSetup[10] + $MachineNoProducation[10] + $MachineActualProducation[10]),
				($Running[11] + $Waiting[11] + $Stopped[11] + $Off[11] + $Nodet[11] + $MachineSetup[11] + $MachineNoProducation[11] + $MachineActualProducation[11])
			)));
		}

		if (!empty($ActualProduction))
		{
			$ActualProductionLength = intval("-45");
		}
		else
		{
			$ActualProductionLength = intval("-20");
		}
		if (!empty($Setup))
		{
			$SetupLength = intval("-45");
		}
		else
		{
			$SetupLength = intval("-20");
		}
		if (!empty($NoProduction))
		{
			$NoProductionLength = intval("-45");
		}
		else
		{
			$NoProductionLength = intval("-20");
		}

	 	

		$json = array(
				"ActualProductionRunning" => !empty($ActualProductionRunning) ? $ActualProductionRunning : "0",
				"ActualProductionWaiting" => !empty($ActualProductionWaiting) ? $ActualProductionWaiting : "0",
				"ActualProductionStopped" => !empty($ActualProductionStopped) ? $ActualProductionStopped : "0",
				"ActualProductionOff" => !empty($ActualProductionOff) ? $ActualProductionOff : "0",
				"ActualProductionNodet" => !empty($ActualProductionNodet) ? $ActualProductionNodet : "0",
				"ActualProductionNoStacklight" => !empty($ActualProductionNoStacklight) ? $ActualProductionNoStacklight : "0",
				"ActualProduction" => !empty($ActualProduction) ? $ActualProduction : "0",
				"ActualProductionLength" => $ActualProductionLength,
				"SetupRunning" => !empty($SetupRunning) ? $SetupRunning : "0",
				"SetupWaiting" => !empty($SetupWaiting) ? $SetupWaiting : "0",
				"SetupStopped" => !empty($SetupStopped) ? $SetupStopped : "0",
				"SetupOff" => !empty($SetupOff) ? $SetupOff : "0",
				"SetupNodet" => !empty($SetupNodet) ? $SetupNodet : "0",
				"Setup" => !empty($Setup) ? $Setup : "0",
				"SetupLength" => $SetupLength,
				"NoProductionRunning" => !empty($NoProductionRunning) ? $NoProductionRunning : "0",
				"NoProductionWaiting" => !empty($NoProductionWaiting) ? $NoProductionWaiting : "0",
				"NoProductionStopped" => !empty($NoProductionStopped) ? $NoProductionStopped : "0",
				"NoProductionOff" => !empty($NoProductionOff) ? $NoProductionOff : "0",
				"NoProductionNodet" => !empty($NoProductionNodet) ? $NoProductionNodet : "0",
				"NoProduction" => !empty($NoProduction) ? $NoProduction : "0",
				"NoProductionLength" => $NoProductionLength,
				"xAxisName" => $xAxisName,
				"yAxisName" => $yAxisName,
				"xAxisData" => $xAxisData,
				"Running" => $Running,
				"Waiting" => $Waiting,
				"Stopped" => $Stopped,
				"Off" => $Off,
				"Nodet" => $Nodet,
				"MachineSetup" => $MachineSetup,
				"MachineNoProducation" => $MachineNoProducation,
				"MachineActualProducation" => $MachineActualProducation,
				"RunningBreak" => $RunningBreak,
				"WaitingBreak" => $WaitingBreak,
				"StoppedBreak" => $StoppedBreak,
				"OffBreak" => $OffBreak,
				"NodetBreak" => $NodetBreak,
				"MachineSetupBreak" => $MachineSetupBreak,
				"MachineNoProducationBreak" => $MachineNoProducationBreak,
				"MachineActualProducationBreak" => $MachineActualProducationBreak,
				"maxValue" => $maxValue,
				"noStacklight" => $noStacklight,
				"machineId" => $machineId,
				"NonProductiveTime" => $Setup + $NoProduction,
				"formatter" => $formatter
			);
		echo json_encode($json);
	}

	//secondsToDay function use for convert seconds to day 
	function secondsToDay($seconds) 
	{
		
		//convert seconds to day
		return sprintf('%0.5f', ($seconds) / (3600 * 24));
	}
	
	//secondsToDay function use for convert seconds to hour
	function secondsToHours($seconds) 
	{
	
		//convert seconds to hour
		return sprintf('%0.5f', $seconds/3600);
    }


    public function breakdown3_pagination() 
	{ 	

		$this->factoryId = $this->input->post('factoryId'); 
		$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
		
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) {
				$machineId = $this->input->post('machineId');
			}
		} 
		$choose1 = $this->input->post('choose1'); 
		$choose2 = $this->input->post('choose2'); 
		$choose3 = $this->input->post('choose3'); 
		$choose4 = $this->input->post('choose4'); 
		
		$duration1 = $this->input->post('duration1'); 
		$duration2 = $this->input->post('duration2'); 
		$duration3 = $this->input->post('duration3'); 
		$duration4 = $this->input->post('duration4'); 
		
		$result['printQuery'] = '';


		
		$choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
		$choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
		
		$result['daily']['filter1'] = 0; $result['daily']['filter1count'] = 0; $x1 = 0; $y1 = 0; $br11 = 0; $br12 = 0; $br13 = 0; $br14 = 0; $br15 = 0; 
		$result['daily']['filter2'] = 0; $result['daily']['filter2count'] = 0; $x2 = 0; $y2 = 0; $br21 = 0; $br22 = 0; $br23 = 0; $br24 = 0; $br25 = 0;
		$result['daily']['filter3'] = 0; $result['daily']['filter3count'] = 0; $x3 = 0; $y3 = 0; $br31 = 0; $br32 = 0; $br33 = 0; $br34 = 0; $br35 = 0;
		$result['daily']['filter4'] = 0; $result['daily']['filter4count'] = 0; $x4 = 0; $y4 = 0; $br41 = 0; $br42 = 0; $br43 = 0; $br44 = 0; $br45 = 0;
		
		$br61 = 0; $br62 = 0; $br63 = 0; $br64 = 0; $br65 = 0; $br71 = 0; $br72 = 0; $br73 = 0; $br74 = 0; $br75 = 0; $br81 = 0; $br82 = 0; $br83 = 0; $br84 = 0; $br85 = 0;  
		
		$result['daily']['reasonfilter1'] = array();
		$result['daily']['reasonfilter2'] = array();
		$result['daily']['reasonfilter3'] = array();
		$result['daily']['reasonfilter4'] = array();
		
		$w3End = $duration4*60;
		$w4End = 9999999999;
		
		$aT = 0;
		$bT = 0;
		$cT = 0;
		$dT = 0;
		$listReasons = $this->AM->getallReasons()->result();
		$resonCount = count($listReasons);
		$listReasons[$resonCount] = new stdClass;
		$listReasons[$resonCount]->breakdownReasonId = $resonCount+1;   
		$listReasons[$resonCount]->breakdownReasonText = '';

		$analytics = $this->input->post('analytics');
		$machineResult = $this->AM->getWhereSingle(array("machineId" => $machineId),"machine");
		
		//Get machine stop detail as par selected filter
		$resultStopsQ = $this->AM->getStopAnalyticsData($this->factory, $machineId, $choose1, $choose2, $choose4Pad, $analytics); 
		if($resultStopsQ == '' ) 
		{
			$resultStops = array();
		} 
		else 
		{
			$resultStops = $resultStopsQ->result();
			//Get max seconds
			if (!empty($resultStops)) 
			{
				$result['daily']['maxSecondsValue'] = max(array_column($resultStops,"colorDuration"));
			}
			else
			{
				$result['daily']['maxSecondsValue'] = "";
			}
			if(count($resultStops) > 0 ) 
			{

				for($x=0;$x<count($resultStops);$x++) 
				{
					if($resultStops[$x]->colorDuration >= $duration1*60 && $resultStops[$x]->colorDuration < $duration2*60) 
					{ 
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para1 = 'br'.$list->breakdownReasonId.'1';
									if ($list->breakdownReasonId == 5 && $aT == 0) 
									{
										$$para1 = 0;
										$aT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para1++;
									break; 
								}
							} 
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter1'][$x1][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter1'][$x1][1] = $resultStops[$x]->colorDuration;
							$x1++;
						}
						$result['daily']['filter1'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter1count']++; 
					} 
					if($resultStops[$x]->colorDuration >= $duration2*60 && $resultStops[$x]->colorDuration < $duration3*60) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para3 = 'br'.$list->breakdownReasonId.'2';
									if ($list->breakdownReasonId == 5 && $bT == 0) 
									{
										$$para3 = 0;
										$bT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para3++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter2'][$x2][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter2'][$x2][1] = $resultStops[$x]->colorDuration;
							$x2++;
						}
						$result['daily']['filter2'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter2count']++;
					} 
					if($resultStops[$x]->colorDuration >= $duration3*60 && $resultStops[$x]->colorDuration < $duration4*60 ) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) {
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para5 = 'br'.$list->breakdownReasonId.'3';
									if ($list->breakdownReasonId == 5 && $cT == 0) {
										$$para5 = 0;
										$cT = 1;
									}
									//Set reason stop details
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para5++;
									break; 
								}
							}
							
						} 
						else 
						{
							//Set stop details
							$result['daily']['stopfilter3'][$x3][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter3'][$x3][1] = $resultStops[$x]->colorDuration; 
							$x3++;
						}
						$result['daily']['filter3'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter3count']++;
					}
					
					if($resultStops[$x]->colorDuration >= $duration4*60 && $resultStops[$x]->colorDuration < $w4End) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
						$flag = 0;
						foreach($listReasons as $list) {
							if ($resultStops[$x]->comment == $list->breakdownReasonText) 
							{
								$para7 = 'br'.$list->breakdownReasonId.'4';
								if ($list->breakdownReasonId == 5 && $dT == 0) 
								{
									$$para7 = 0;
									$dT = 1;
								}
								//Set reason stop details
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][0] = $resultStops[$x]->timeS;
								$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][1] = $resultStops[$x]->colorDuration; 
								$flag = 1; $$para7++;
								break; 
							}
						}
						
					} 
					else 
					{
						//Set stop details
						$result['daily']['stopfilter4'][$x4][0] = $resultStops[$x]->timeS;
						$result['daily']['stopfilter4'][$x4][1] = $resultStops[$x]->colorDuration;
						 $x4++;
					}
						$result['daily']['filter4'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter4count']++;
					}
				}  
				
				if($result['daily']['filter1'] != 0) 
				{
					$result['daily']['filter1'] = sprintf('%0.2f', $result['daily']['filter1']/60);
				}
				if($result['daily']['filter2'] != 0) 
				{
					$result['daily']['filter2'] = sprintf('%0.2f', $result['daily']['filter2']/60);
				}
				if($result['daily']['filter3'] != 0) 
				{
					$result['daily']['filter3'] = sprintf('%0.2f', $result['daily']['filter3']/60);
				}
				if($result['daily']['filter4'] != 0) 
				{
					$result['daily']['filter4'] = sprintf('%0.2f', $result['daily']['filter4']/60);
				}
			}
		}
		
		$filter1Val = $this->input->post('filter1Val'); 
		$filter2Val = $this->input->post('filter2Val'); 
		$filter3Val = $this->input->post('filter3Val'); 
		$filter4Val = $this->input->post('filter4Val'); 
		for($i=0;$i<count($listReasons);$i++) 
		{ 
			$reasonNameArr[$i] = $listReasons[$i]->breakdownReasonText;
			if($filter1Val == '1') 
			{
				$minDuration1 = $duration1*60;
				$maxDuration1 = $duration2*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr1 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration1, $maxDuration1, $machineId, $analytics);
				$reasonCountArr11[$i]['value'] = 0; $reasonCountArr11Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr1)) 
				{
					for($x=0;$x<count($issueReasonArr1);$x++)
					{
						if($issueReasonArr1[$x]->comment == $reasonNameArr[$i]) 
						{ 
							$reasonCountArr11[$i]['value']++;
							$reasonCountArr11Duration[$i]['value'] += $issueReasonArr1[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr1'] = $reasonCountArr11;
				$result['daily']['reasonCountArrDuration1'] = $reasonCountArr11Duration;
			}
			
			if($filter2Val == '1') 
			{
				$minDuration2 = $duration2*60;
				$maxDuration2 = $duration3*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr2 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration2, $maxDuration2, $machineId, $analytics);
				$reasonCountArr21[$i]['value'] = 0;  $reasonCountArr21Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr2)) 
				{
					for($x=0;$x<count($issueReasonArr2);$x++)
					{
						if($issueReasonArr2[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr21[$i]['value']++;
							$reasonCountArr21Duration[$i]['value'] += $issueReasonArr2[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr2'] = $reasonCountArr21; 
				$result['daily']['reasonCountArrDuration2'] = $reasonCountArr21Duration;
			}
			
			if($filter3Val == '1') 
			{
				$minDuration3 = $duration3*60;
				$maxDuration3 = $duration4*60;
				//Get reason as page time to set in graph stop count
				$issueReasonArr3 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration3, $maxDuration3, $machineId, $analytics);
				$reasonCountArr31[$i]['value'] = 0;  $reasonCountArr31Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr3)) 
				{
					for($x=0;$x<count($issueReasonArr3);$x++)
					{
						if($issueReasonArr3[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr31[$i]['value']++;
							$reasonCountArr31Duration[$i]['value'] += $issueReasonArr3[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr3'] = $reasonCountArr31; 
				$result['daily']['reasonCountArrDuration3'] = $reasonCountArr31Duration;
			}
			
			if($filter4Val == '1') 
			{
				$minDuration4 = $duration4*60;
				$maxDuration4 = $w4End;
				//Get reason as page time to set in graph stop count
				$issueReasonArr4 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration4, $maxDuration4, $machineId, $analytics);
				$reasonCountArr41[$i]['value'] = 0;  $reasonCountArr41Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr4)) {
					for($x=0;$x<count($issueReasonArr4);$x++){
						if($issueReasonArr4[$x]->comment == $reasonNameArr[$i]) {
							$reasonCountArr41[$i]['value']++;
							$reasonCountArr41Duration[$i]['value'] += $issueReasonArr4[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr4'] = $reasonCountArr41; 
				$result['daily']['reasonCountArrDuration4'] = $reasonCountArr41Duration;
			}
		}
		$reasonNameArr[$resonCount] = 'Unanswered'; 

		if ($analytics == "waitAnalytics") {
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyWait == "0") ? "None" : "After ". $machineResult->notifyWait ." min";
            $result['machineNotifyTime']['errorReasonNotification'] = ($machineResult->prolongedWait == "0") ? "None" : "After ". $machineResult->prolongedWait ." min";
        }
        else
        {
            $result['machineNotifyTime']['errorNotification'] = ($machineResult->notifyStop == "0") ? "None" : "After ". $machineResult->notifyStop ." min";
            $result['machineNotifyTime']['errorReasonNotification'] = ($machineResult->prolongedStop == "0") ? "None" : "After ". $machineResult->prolongedStop ." min";
        }

		$result['daily']['reasonNameArr'] = $reasonNameArr;
		//Set array to manage data in graph
		$result['daily']['reasonCountArr1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArr1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArr1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArr1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArr1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArr1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArr1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArr1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][7]['itemStyle']['color'] = '#d32f2f';
		
		$result['daily']['reasonCountArrDuration1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArrDuration1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArrDuration1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArrDuration1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArrDuration1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArrDuration1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArrDuration1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArrDuration1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][7]['itemStyle']['color'] = '#d32f2f';
		echo json_encode($result); die;  
	}

	
}

?>