<?php

class InstructionDetail extends CI_Controller
{
	
	function __construct()
	{
		parent::__construct();
		$this->load->model('Admin_model','AM');

		
	}

	function dashboardInstruction()
	{
		$data['instruction'] = $this->AM->getWhereDb(array("instruction_type" => "1","isDelete" => "0"),"instruction");
		$this->load->view('dashbbaordInstruction',$data);
	}

	function opAppInstruction()
	{
		$data['instruction'] = $this->AM->getWhereDb(array("instruction_type" => "2","isDelete" => "0"),"instruction");
		$this->load->view('opappInstruction',$data);
	}

	function searchInstruction()
	{
		$searchKey = $this->input->post('searchKey');
		$instruction_type = $this->input->post('instruction_type');
		$html = '';

		$instruction = $this->AM->getWhereLike(array("instruction_type" => $instruction_type,"isDelete" => "0"),array("search_keyword" => $searchKey),"instruction");	
		if ($instruction_type == "2") 
		{
			$pxD = "320px";
		}else
		{
			$pxD = "420px";
		}

		foreach ($instruction as $key => $value) 
		{

			if(!empty($value['main_title_gif'])) { $imageVar = ' <br> <img style="width: '.$pxD.';" src="'. base_url('assets/instruction/').$value['main_title_gif'].'">'; }else {  $imageVar = ''; }
			$subInstruction = $this->AM->getWhereDb(array("instructionId" => $value['instructionId']),"subInstruction");
			$html .= '<div class="row" style="margin-top: 15px;">
	        <div class="col-md-4"></div>
	        <div class="col-md-4">'. $value['main_title'] . $imageVar;
	            
	            
	            foreach ($subInstruction as $keySub => $valueSub) { 

	        $html .= '<div class="row" style="margin-left: 10px;margin-top: 10px;">
	                <div class="col-md-12">
	                  '.$valueSub['subTitle'].' <br> <img style="width: '.$pxD.';" src="'.base_url('assets/instruction/').$valueSub['subTitleGif'].'">
	                </div>
	            </div>';
	          }
	        $html .= '</div>
	        <div class="col-md-4"></div>
	      </div>';
		}
		echo $html;
	}
}