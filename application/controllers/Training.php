<?php

	defined('BASEPATH') OR exit('No direct script access allowed');

	class Training extends CI_Controller {
		
		var $factory;
		var $factoryId;
		
		function __construct() 
		{
			parent::__construct();                        
			$this->load->model('Admin_model','AM'); 
			$this->lang->load('content_lang','english');
			 $this->load->helper('language');
			if($this->AM->checkIsvalidated() == true) 
			{
				$this->factoryId = $this->session->userdata('factoryId'); 
				$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
				$this->listFactories = $this->AM->getallFactories(); 

				$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
				$this->factory->query('SET SESSION sql_mode =
		                  REPLACE(REPLACE(REPLACE(
		                  @@sql_mode,
		                  "ONLY_FULL_GROUP_BY,", ""),
		                  ",ONLY_FULL_GROUP_BY", ""),
		                  "ONLY_FULL_GROUP_BY", "")');
			}

			$site_lang = $this->session->userdata('site_lang');
			if($site_lang == "english")
			{
				$this->AM->getLanguage(1);
				// echo $language;exit;	
			}
			else
			{
				$this->AM->getLanguage(1);	
				// echo $language;exit;
			}
		}

		
		

		public function displaydataphone($machineId)
		{
			// $this->load->view("header.php");
			$result['data']=$this->AM->display_records($machineId);
			$this->load->view('phonedirectory',$result);
			$this->load->view("footer.php");
		}
		
	
		public function task_testing_insert()
		{
			$task_name = $this->input->post('task_name');
    	    $task_description = $this->input->post('task_description');
    	    $created_date = $this->input->post('created_date');
    	 	
    	 	$data = array(
                    'task_name'  => $task_name, 
                    'task_description'  => $task_description, 
                    'created_date' => $created_date
                );
            
            $data=$this->AM->insertTask($data);
           	$this->load->view("task_testing_insert",$data);  
		}

		public function task_testing()
		{
			$result['data']=$this->AM->display_task();
			$this->load->view('task_testing',$result);
		}

		public function index()
		{
			$data['message'] = $this->db->select('*')->from('task_testing')->order_by('task_id','desc')->get();
			$this->load->view('task_testing',$data);
		}

		public function task_testing_final()
		{
			$this->load->view("task_testing_final");
		}
		public function insertTask()
		{
			$task_name = $this->input->post('task_name');
    	    $task_description = $this->input->post('task_description');
    	    $created_at = $this->input->post('created_at');
    	
    	    
    	    $data = array(
                    'task_name'  => $task_name, 
                    'task_description'  => $task_description, 
                    'created_at' => $created_at, 
                );
            
            $data=$this->AM->insertTask($data);  
		}

		

		//taskMaintenance function use for get task & maintence page 
	function socketLogData()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$data['machines'] = $this->AM->getallMachines($this->factory)->result_array();
	
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('socketConnectDisConnectData',$data);
		$this->load->view('footer');		
		$this->load->view('script_file/socket_connect_disconnect_data_footer');		
	}

	//task_maintenace_pagination function use for get task data 
	public function socket_connect_disconnect_data_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$dueDateValS = $this->input->post('dueDateValS');
		$dueDateValE = $this->input->post('dueDateValE');
		$machineId = $this->input->post('machineId');


		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		//Get all data count
		$listDataLogsCount = $this->AM->getallsocketConnectDisConnectDataCount()->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchsocketConnectDisConnectDataCount($dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getsocketConnectDisConnectDataLogs($row,$rowperpage,$dateValS,$dateValE,$dueDateValS,$dueDateValE,$machineId)->result_array();  
		
		foreach ($listDataLogs as $key => $value) 
		{	
			if($value['reason'] == 'client namespace disconnect' || $value['reason'] == 'transport close' || $value['reason'] == 'transport error') {
				$listDataLogs[$key]['reasonText'] = SETAPP_TURNED_OFF;
			} else if($value['reason'] == 'detection disconnect') { 
				$listDataLogs[$key]['reasonText'] = "Setapp is in home screen. Check your phone or contact : info@nytt-tech.com";
			}else if($value['reason'] == 'ping timeout') { 
				$listDataLogs[$key]['reasonText'] = SETAPP_NO_INTERNET;
			}
			 else
			{
				$listDataLogs[$key]['reasonText'] = "Connected";
			}

			if ($value['startTime'] == $value['endTime']) 
			{
				$listDataLogs[$key]['endTime'] = "";	
			}
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}
	
	public function RolesandTeams()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$roles = $this->AM->getWhereDB(array("isDelete" => "0"),"roles");
		foreach ($roles as $key => $value) 
		{
			if ($value['rolesType'] == "2") 
			{
				$where = array("isDeleted" => "0","userRole" => $value['rolesType']);
			}
			else
			{
				$where = array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => $value['rolesType']);
			}
			$roles[$key]['users'] = $this->AM->getWhereDB($where,"user");
			$roles[$key]['totalUsers'] = count($roles[$key]['users']);
		}

		$data['roles'] = $roles;

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
		$responsibilities = $this->AM->getWhereDB(array("isDelete" => "0","mainResponsibilitiesId" => "0"),"responsibilities");

		foreach ($responsibilities as $key => $value) 
		{
			$responsibilities[$key]['subResponsibilities'] = $this->AM->getWhereDB(array("mainResponsibilitiesId" => $value['responsibilitiesId'],"isDelete" => "0"),"responsibilities");
		}
		$data['responsibilities'] = $responsibilities;
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('roles_testing',$data);
		$this->load->view('footer');	
		$this->load->view('script_file/roles_testing_footer');		
	}

	function getAssignOperatorRole()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$rolesType = $this->input->post('rolesType');

		if ($rolesType == "2") 
		{
			$where = array("isDeleted" => "0","userRole" => $rolesType);
			$whereAll = array("isDeleted" => "0");
		}
		else
		{
			$where = array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => $rolesType);
			$whereAll = array("factoryId" => $this->factoryId,"isDeleted" => "0");
		}
		$rolesUsers = $this->AM->getWhereDB($where,"user");
		$rolesUsersId = array_column($rolesUsers, "userId");

		$allUsers = $this->AM->getWhereDB($whereAll,"user");

		$allUsersHtml = "";

		foreach ($allUsers as $key => $value) 
		{
			if (in_array($value['userId'], $rolesUsersId)) 
			{
				$display = "none";
			}else
			{
				$display = "block";
			}

			$allUsersHtml .= '<div class="col-md-12" style="display: '.$display.'" id="addUser'.$value['userId'].'">
                                <div class="row">
                                   <div class="col-md-2" style="padding:0px;">
                                      <img  style="width: 20px;height: 20px;border-radius: 50%;"src="'.base_url('assets/img/user/'.$value['userImage']).'">&nbsp;&nbsp;&nbsp;
                                   </div>
                                   <div class="col-md-7">
                                      <span class="OperatorNameSpanStyle">'. $value['userName'].'</span>
                                   </div>
                                   <div class="col-md-3">
                                      <i onclick="addUser('.$value['userId'].')" style="float: right;color: #ff8000;font-size: 14px" class="fas fa-plus"></i>
                                   </div>
                                </div>
                             </div>';
		}

		$assignUsersHtml = "";
		foreach ($allUsers as $key => $value) 
		{
			if (in_array($value['userId'], $rolesUsersId)) 
			{
				$display = "block";
			}else
			{
				$display = "none";
			}

			$assignUsersHtml .= '<div class="col-md-12" style="display: '.$display.'" id="userRemove'.$value['userId'].'">
                                <div class="row">
                                   <div class="col-md-2" style="padding:0px;">
                                      <img  style="width: 20px;height: 20px;border-radius: 50%;"src="'.base_url('assets/img/user/'.$value['userImage']).'">&nbsp;&nbsp;&nbsp;
                                   </div>
                                   <div class="col-md-7">
                                      <span class="OperatorNameSpanStyle">'. $value['userName'].'</span>
                                   </div>
                                   <div class="col-md-3">
                                      <i onclick="removeUser('.$value['userId'].')" style="float: right;color: red;font-size: 10px" class="fas fa-window-minimize"></i>
                                   </div>
                                </div>
                             </div>';
		}

		$assignOperator = implode(",", $rolesUsersId);
		$json = array("allUsers" => $allUsersHtml,"assignUsersHtml" => $assignUsersHtml,"assignOperator" => $assignOperator);
		echo json_encode($json);
	}

	public function RolesandTeams_pagination()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$userName 		= $this->input->post('userName');
		$userRole 		= $this->input->post('userRole');
		$dateValS 		= $this->input->post('dateValS');
		$dateValE 		= $this->input->post('dateValE');

		$draw 		= $_POST['draw'];
		$row 		= $_POST['start'];
		$rowperpage = $_POST['length'];


		$listDataLogsCount = $this->AM->getAllRolesCount($db)->row()->totalCount;

		$listDataLogsSearchCount = $this->AM->getROlesandMaincount($start, $limit, $userName, $userRole, $dateValS, $dateValE)->row()->totalCount;


		$listDataLogs = $this->AM->getallRolesAdnMain($row, $rowperpage, $userName, $userRole, $dateValS, $dateValE)->result_array();


		foreach ($listDataLogs as $key => $value) 
		{
			$listDataLogs[$key]['Edit'] = '	<a style="background-color:#002060;padding: 3px;border-radius: 10px;color:white;" onclick="Edit_user('.$value['userId'].')" href="javascript:void(0);" class="btn">
                                        		<img style="height: 18px;" src="'.base_url("assets/nav_bar/white_edit.svg").'";?>Edit
                                    		</a>';

			$listDataLogs[$key]['delete'] = '<a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 5px;"  onclick="delete_User('.$value['userId'].')" href="javascript:void(0);" class="btn btn-primary">
				<img style="width: 23px;height: 23px;" src="' . base_url("assets/nav_bar/delete_white.svg") . '"></a>';

			$roles = $this->AM->getWhereDBSingle(array("rolesType" => $value['userRole']),"roles");

			if (!empty($roles)) 
			{
				$listDataLogs[$key]['userRole'] = $roles->rolesName;
			}
			else
			{
				$listDataLogs[$key]['userRole'] = "No roles assign";
			}
		}
		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		// print_r($response);exit;
		echo json_encode($response); die;
	}

	
	function add_rolesResponsibilities()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$isView = $this->input->post('isView');
		$isEdit = $this->input->post('isEdit');
		if (!empty($isView) || !empty($isEdit)) 
		{

			$query = $this->db->query("SELECT max(rolesType) as max_id FROM roles");
	        $row = $query->row_array();

			$rolesName = $this->input->post('rolesName');
			$data = array("rolesName" => $rolesName,"rolesType" =>  $row['max_id'] + 1);

			$rolesId = $this->AM->insertDBData($data,"roles");

			if (!empty($isView)) 
			{
				foreach ($isView as $key => $value) 
				{
					$this->AM->insertDBData(array("rolesId" => $rolesId,"responsibilitiesId" => $value,"isView" => "1","isEdit" => "0","isExecute" => "0"),"rolesResponsibilities");
				}
			}

			if (!empty($isEdit)) 
			{
				foreach ($isEdit as $key => $value) 
				{
					$this->AM->insertDBData(array("rolesId" => $rolesId,"responsibilitiesId" => $value,"isView" => "0","isEdit" => "1","isExecute" => "0"),"rolesResponsibilities");
				}
			}

			$json = array("status" => true,"message" => "Roles added successfully");
		}else
		{
			$json = array("status" => false,"message" => "Please Select roles");
		}

		echo json_encode($json);

	}

	function edit_rolesResponsibilities()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}


		$rolesId = $this->input->post('rolesId');
		$rolesName = $this->input->post('rolesName');
		$isView = $this->input->post('isView');
		$isEdit = $this->input->post('isEdit');
		$data = array("rolesName" => $rolesName);

		$this->AM->updateDBData(array("rolesId" => $rolesId),$data,"roles");

		$this->db->where(array("rolesId" => $rolesId))->delete("rolesResponsibilities");

		if (!empty($isView)) 
		{
			foreach ($isView as $key => $value) 
			{
				$this->AM->insertDBData(array("rolesId" => $rolesId,"responsibilitiesId" => $value,"isView" => "1","isEdit" => "0","isExecute" => "0"),"rolesResponsibilities");
			}
		}

		if (!empty($isEdit)) 
		{
			foreach ($isEdit as $key => $value) 
			{
				$this->AM->insertDBData(array("rolesId" => $rolesId,"responsibilitiesId" => $value,"isView" => "0","isEdit" => "1","isExecute" => "0"),"rolesResponsibilities");
			}
		}

		$json = array("status" => true,"message" => "Roles edited successfully");
		echo json_encode($json);

	}

	function assignOperatorToRole()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}	

		$rolesType = $this->input->post('rolesType');
		$assignOperator = $this->input->post('assignOperator');
		$oldAssignOperator = $this->input->post('oldAssignOperator');

		if (!empty($oldAssignOperator)) 
		{
			$oldAssignOperator = explode(",", $oldAssignOperator);
			foreach ($oldAssignOperator as $key => $value) 
			{
				if (!empty($value)) 
				{
					$this->AM->updateDBData(array("userId" => $value),array("userRole" => ""), "user");
				}
			}
		}

		if (!empty($assignOperator)) 
		{
			$assignOperator = explode(",", $assignOperator);
		
			foreach ($assignOperator as $key => $value) 
			{
				if (!empty($value)) 
				{
					$this->AM->updateDBData(array("userId" => $value),array("userRole" => $rolesType), "user");
					//echo $this->db->last_query();exit;
				}
			}
		}

		$json = array("status" => true,"message" => "Role's users updated successfully");
		echo json_encode($json);
	}

	function deleteRoles()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}	

		$rolesId = $this->input->post('rolesId');
		$this->AM->updateDBData(array("rolesId" => $rolesId),array("isDelete" => "1"), "roles");

		$json = array("status" => true,"message" => "Roles  deleted successfully");
		echo json_encode($json);
	}
	public function accessPoint() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
        $this->load->view('accessPoint');
        $this->load->view('footer');
	}

	public function switchLang($language = "") 
	{
		
		$this->session->set_userdata('site_lang', $language);
		// print_r($this->session->userdata); die;
		redirect('Training/languageTesting');
	}

	public function languageTesting() 
	{
		
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));

		$this->load->view('languageTesting');
		$this->load->view('footer');
	}

	function DeleteTaskTestings()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}	



		$task_id = $this->input->post('task_id');
		$this->AM->updateDBData(array("task_id" => $task_id),array("id_deleted" => "1"), "task_testing");

		$json = array("status" => true,"message" => "Roles  deleted successfully");
		echo json_encode($json);
	}

	public function insertdataInLanguage()
	{
		$this->load->view('insert');
	
		if($this->input->post('save'))
		{
		    $data['textIdentify']=$this->input->post('textIdentify');
			$data['englishText']=$this->input->post('englishText');
			$data['swedishText']=$this->input->post('swedishText');
			$data['Type']=$this->input->post('Type');
			$response=$this->AM->InsertRecordsLanguage($data);
			if($response==true)
			{
			        echo "Records Saved Successfully";
			}
			else
			{
					echo "Insert error !";
			}
		}
	}


	//In use
	function taskMaintenance()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$accessPoint = $this->AM->accessPoint(21);
		if ($accessPoint == true) 
        {
			$data['accessPointEdit'] = $this->AM->accessPointEdit(21);
			$machines = $this->AM->getallMachines($this->factory)->result_array();
			$data['listMachines'] = $this->AM->getallMachines($this->factory);

			foreach ($machines as $key => $value) 
			{
				$machines[$key]['totalTask'] = $this->AM->getWhereCount(array("isDelete" => "0","machineId" => $value['machineId']),"taskMaintenace");
				$machines[$key]['totalCompletedTask'] = $this->AM->getWhereCount(array("isDelete" => "0","status" => "completed","machineId" => $value['machineId']),"taskMaintenace");
			}

			$data['machines'] = $machines;
			$data['totalTask'] = $this->AM->getWhereCount(array("isDelete" => "0"),"taskMaintenace");
			$data['totalCompletedTask'] = $this->AM->getWhereCount(array("isDelete" => "0","status" => "completed"),"taskMaintenace");
			$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
			$this->load->view('newtaskandmaintenance',$data);
			$this->load->view('footer');		
			$this->load->view('script_file/newtaskandmaintenance_footer',$data);		
		}else
		{
			$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
            $this->load->view('accessPoint');
            $this->load->view('footer');
		}
	}

	//task_maintenace_pagination function use for get task data 
	public function task_maintenace_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		
		// print_r($this->input->post());exit;

		$userIds = $this->input->post('userId');
		$repeat = $this->input->post('repeat');
		$dateValS = $this->input->post('dateValS');
		$dateValE = $this->input->post('dateValE');
		$dueDateValS = $this->input->post('dueDateValS');
		$dueDateValE = $this->input->post('dueDateValE');
		$status = $this->input->post('status');
		$machineIds = $this->input->post('machineIds');
		$machineId = $this->input->post('machineId');
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];

		// print_r($dueDateValS);exit;
		// print_r($dueDateValE);exit;


		//Get all data count
		$listDataLogsCount = $this->AM->getallTaskMaintenaceCount($this->factory, $machineIds, $is_admin)->row()->totalCount;
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->getSearchTaskMaintenaceCount($this->factory, $userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)->row()->totalCount;
		//Get all data
		$listDataLogs = $this->AM->getallTaskMaintenaceLogs($this->factory,$row,$rowperpage,$userIds,$repeat,$dateValS,$dateValE,$status,$machineIds,$machineId,$dueDateValS,$dueDateValE)->result_array();  

		$removeArrayKey = array();

		foreach ($listDataLogs as $key => $value) 
		{
			$userIds = explode(",", $value['userIds']);

			$operators = "";
			foreach ($userIds as $userKey => $userValue) 
			{
				$users = $this->AM->getWhereDBSingle(array("userId" => $userValue,"isDeleted" => "0"),"user");
				if (!empty($users)) 
				{
					$operators .= '<img style="border-radius: 50%;margin-top: -3px;" width="16" height="16" src="'. base_url('assets/img/user/'.$users->userImage) .'">&nbsp;'. $users->userName."<br>";
				}
			}

			$listDataLogs[$key]['operators'] = $operators;
			

			$lastSubTask = $this->AM->getLastSubTask($value['taskId']);
			if (!empty($lastSubTask)) 
			{ 
				if (!empty($status)) 
				{
					if (in_array("'".$lastSubTask->status."'", $status)) 
					{
						$listDataLogs[$key]['status'] = ($lastSubTask->status == "uncompleted") ? uncomplete : complete;
						$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($lastSubTask->dueDate));	
					}
					else
					{
						$removeArrayKey[] = $key;
					}
				}
				else
				{
					$listDataLogs[$key]['status'] = ($lastSubTask->status == "uncompleted") ? uncomplete : complete;
					$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($lastSubTask->dueDate));	
				}
				
			}
			else
			{
				$listDataLogs[$key]['status'] = ($value['status'] == "uncompleted") ? uncomplete : complete;
				$listDataLogs[$key]['dueDate'] = date('Y-m-d',strtotime($value['dueDate']));
			}

			$listDataLogs[$key]['createdDate'] = date('Y-m-d',strtotime($value['createdDate']));



			$taskCount = strlen($value['task']);
			$listDataLogs[$key]['task'] = ($taskCount > 30) ? substr($value['task'],0,30).'...' : $value['task'];
			$listDataLogs[$key]['delete'] = '<a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 9px;"  onclick="delete_task('.$value['taskId'].')" href="javascript:void(0);" class="btn btn-primary">
				<img style="width: 23px;height: 23px;" src="' . base_url("assets/nav_bar/delete_white.svg") . '"></a>';		

			if ($value['repeat'] == "everyYear") 
			{
				$repeat = Everyyear;
			}
			elseif ($value['repeat'] == "everyMonth") 
			{
				$repeat = Everymonth;
			}
			elseif ($value['repeat'] == "everyWeek") 
			{
				$repeat = Everyweek;
			}
			elseif ($value['repeat'] == "everyDay") 
			{
				$repeat = Everyday;
			}
			else
			{
				$repeat = Never;
			}
			$listDataLogs[$key]['repeat'] = $repeat;
		}

		$listDataLogs = $this->array_except($listDataLogs, $removeArrayKey);
		$listDataLogs = array_values($listDataLogs);

		//print_r($listDataLogs);exit;
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);
		echo json_encode($response); die;
	}


	function array_except($array, $keys) 
	{
	  return array_diff_key($array, array_flip((array) $keys));   
	} 

	function getEditTask()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$taskId = $this->input->post('taskId');

		$result = $this->AM->getTaskById($this->factory,$taskId);
		if (!empty($result->endTaskDate)) 
		{
			$result->endTaskDate = date('m/d/Y',strtotime($result->endTaskDate));
		}

		if (!empty($result->dueDate)) 
		{
			$result->dueDate = date('m/d/Y',strtotime($result->dueDate));
		}
		echo json_encode($result); die;
	}
	
	//getTaskById use for get task data by task id
	function getTaskById()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$taskId = $this->input->post('taskId');
		$result = $this->AM->getTaskById($this->factory,$taskId);

		$subTask = $this->AM->getWhereWithOrderBy(array("mainTaskId" => $taskId),"taskId","desc","taskMaintenace");

		$statusTask = $result->status;
		$result->createdDate = date('Y-m-d',strtotime($result->createdDate));
		
		$result->status = ""; 
		foreach ($subTask as $key => $value) 
		{	
			$completed = ($value['status'] == "completed") ? "checked" : "";
			$statusName = ($value['status'] == "completed") ? Completed : Uncompleted;
			if ($value['repeat'] == "everyYear") 
			{
				$repeatName = "Every year";
				if (date('Y',strtotime($value['dueDate'])) != date('Y') || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			elseif ($value['repeat'] == "everyMonth") 
			{
				$repeatName = "Every month";
				if ((date('Y',strtotime($value['dueDate'])) != date('Y') && date('m',strtotime($value['dueDate'])) != date('m')) || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			elseif ($value['repeat'] == "everyWeek") 
			{
				$repeatName = "Every week";

				$currentDate = date('Y-m-d');
				$dayTask = date('l',strtotime($currentDate));
				if ($dayTask == "Saturday" || $dayTask == "Sunday") 
				{
					$currentDate = date('Y-m-d',strtotime("next monday"));
				}
				
				$compareWeekCheck = date('W',strtotime($value['dueDate']));
 				$currentWeek = date('W',strtotime($currentDate));

				if ($currentWeek == $compareWeekCheck) 
				{
					if ($value['status'] == "completed") {
						$disabledTask = "disabled";
					}
					else
					{
						$disabledTask = "";
					}
				}
				else
				{
					$disabledTask = "disabled";
				}


			}
			elseif ($value['repeat'] == "everyDay") 
			{
				$repeatName = "Every day";
				if ($value['dueDate'] != date('Y-m-d') || $value['status'] == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			else
			{
				$repeatName = "Never";
				$disabledTask = "disabled";
			}
			$dueDate = $value['dueDate'];
			$result->status .= '<div class="col-md-12" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline"><input  id="taskIdCheck'.$value['taskId'].'" onclick="completeUncompleteTask('.$value['taskId'].')" '. $completed .' class="form-check-input" type="checkbox" name="taskStatus[]" '.$disabledTask.'  value="'.$value['taskId'].'"><label class="form-check-label" for="completed" style="font-size: 80% !important;">'. $statusName .' '. $dueDate .' '.'('. $repeatName .')</label></div></div>';


		}


		$completed = ($statusTask == "completed") ? "checked" : "";
		$statusName = ($statusTask == "completed") ? Completed : Uncompleted;

		if ($result->repeat == "everyYear") 
		{
			$repeatName = Everyyear;
			if (date('Y',strtotime($result->dueDate)) != date('Y') || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		elseif ($result->repeat == "everyMonth") 
		{
			$repeatName = Everymonth;
			if ((date('Y',strtotime($result->dueDate)) != date('Y') && date('m',strtotime($result->dueDate)) != date('m')) || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		elseif ($result->repeat == "everyWeek") 
		{
			$repeatName = Everyweek;

			$currentDate = date('Y-m-d');
			$dayTask = date('l',strtotime($currentDate));
			if ($dayTask == "Saturday" || $dayTask == "Sunday") 
			{
				$currentDate = date('Y-m-d',strtotime("next monday"));
			}
			
			$compareWeekCheck = date('W',strtotime($result->dueDate));
			$currentWeek = date('W',strtotime($currentDate));

			if ($currentWeek == $compareWeekCheck) 
			{
				if ($statusTask == "completed") 
				{
					$disabledTask = "disabled";
				}
				else
				{
					$disabledTask = "";
				}
			}
			else
			{
				$disabledTask = "disabled";
			}
		}
		elseif ($result->repeat == "everyDay") 
		{
			$repeatName = Everyday;

			if ($result->dueDate != date('Y-m-d') || $statusTask == "completed") 
			{
				$disabledTask = "disabled";
			}
			else
			{
				$disabledTask = "";
			}
		}
		else
		{
			$repeatName = Never;

			if ($result->dueDate < date('Y-m-d')) 
			{
				$disabledTask = "disabled";
			}
			else
			{
				if ($statusTask == "completed") 
				{
					$disabledTask = "disabled";
				}else
				{
					$disabledTask = "";
				}
			}
		}




		$result->status .= '<div class="col-md-12" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline"><input id="taskIdCheck'.$result->taskId.'" onclick="completeUncompleteTask('.$result->taskId.')" '. $completed .' class="form-check-input" type="checkbox" name="taskStatus[]" '.$disabledTask.' value="'.$result->taskId.'"><label class="form-check-label" for="completed" style="font-size: 80% !important;">'. $statusName .' '. $result->dueDate .' '.'('. $repeatName .')</label></div></div>';

		$never = ($result->repeat == "never") ? "checked" : "";
		$neverLabel = ($result->repeat == "never") ? "#FF8000" : "#1f2225";
		$everyDay = ($result->repeat == "everyDay") ? "checked" : "";
		$everyDayLabel = ($result->repeat == "everyDay") ? "#FF8000" : "#1f2225";
		$everyWeek = ($result->repeat == "everyWeek") ? "checked" : "";
		$everyWeekLabel = ($result->repeat == "everyWeek") ? "#FF8000" : "#1f2225";
		$everyMonth = ($result->repeat == "everyMonth") ? "checked" : "";
		$everyMonthLabel = ($result->repeat == "everyMonth") ? "#FF8000" : "#1f2225";
		$everyYear = ($result->repeat == "everyYear") ? "checked" : "";
		$everyYearLabel = ($result->repeat == "everyYear") ? "#FF8000" : "#1f2225";

		$result->repeatData = '<div class="col-md-4" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$never.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="never" value="never"><label class="form-check-label repeatAll" for="never" id="neverLabel" style="font-size: 80% !important;color:'.$neverLabel.'">Never</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyDay.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyDay" value="everyDay"><label class="form-check-label repeatAll" for="everyDay" id="everyDayLabel" style="font-size: 80% !important;color:'.$everyDayLabel.'">Every day</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyWeek.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyWeek" value="everyWeek"><label class="form-check-label repeatAll" for="everyWeek" id="everyWeekLabel" style="font-size: 80% !important;color:'.$everyWeekLabel.'">Every week</label></div></div><div class="col-md-5" style="float: left;padding-left: 10px;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyMonth.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyMonth" value="everyMonth"><label class="form-check-label repeatAll" for="everyMonth" id="everyMonthLabel" style="font-size: 80% !important;color:'.$everyMonthLabel.'">Every month</label></div></div><div class="col-md-4" style="float: left;"><div class="form-check form-check-inline" style="margin-right: -1.25rem !important;"><input '.$everyYear.' class="form-check-input" type="radio" name="repeat" disabled onclick="checkRepeat(this.id);" id="everyYear" value="everyYear"><label class="form-check-label repeatAll" for="everyYear" id="everyYearLabel" style="font-size: 80% !important;color:'.$everyYearLabel.'">Every year</label></div></div>';

		$users = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user");
		$userIds = explode(",", $result->userIds);
		$checkedAll = (count($userIds) == count($users)) ? "checked" : "";
		$checkedColorAll = (count($userIds) == count($users)) ? "#FF8000" : "#1f2225";
		$operatorsHtml = "";
		
		foreach ($users as $key => $value) 
		{
			$checked = (in_array($value['userId'], $userIds)) ? "checked" : "";
			$checkedColor = (in_array($value['userId'], $userIds)) ? "#FF8000" : "#1f2225";
			if ($checked == "checked") 
			{
				$operatorsHtml .= '<div style="float: left;padding-left: 10px;padding-top: 5px;"><img style="border-radius: 50%;" width="20" height="20" src="'. base_url('assets/img/user/').$value['userImage'].'">'.$value['userName'].'</div>';
			}
		} 
		$result->userIds = $operatorsHtml;
		$result->repeatName = $repeatName;
		if (!empty($subTask)) 
		{
			$result->dueDate = $subTask[0]['dueDate'];
		}
		echo json_encode($result); die;
	}

	//add_task function use for add new task
	public function add_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('userIds','userIds','required');
        $this->form_validation->set_rules('task','task','required');
        $this->form_validation->set_rules('repeat','repeat','required');

        $repeat = $this->input->post('repeat');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDate','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDay','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOn');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonth','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeek','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDay','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOn');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonth','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDay','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeek','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDay','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnThe','dueYearMonthOnThe','required');
        	}
        }
        //checking required parameters validation
		$machineId =  $this->input->post('machineId');
        if($this->form_validation->run()==false || empty($machineId)) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforaddtask);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$userIds = $this->input->post('userIds');
			$task = $this->input->post('task');
			$repeat = $this->input->post('repeat');
			
			$endTaskDate = $this->input->post('endTaskDate');
			$EndAfteroccurances = $this->input->post('EndAfteroccurances');

			$dueWeekDay = $this->input->post('dueWeekDay');
			$monthDueOn = $this->input->post('monthDueOn');
			$dueMonthMonth = $this->input->post('dueMonthMonth');
			$dueMonthWeek = $this->input->post('dueMonthWeek');
			$dueMonthDay = $this->input->post('dueMonthDay');
			$yearDueOn = $this->input->post('yearDueOn');
			$dueYearMonth = $this->input->post('dueYearMonth');
			$dueYearMonthDay = $this->input->post('dueYearMonthDay');
			$dueYearWeek = $this->input->post('dueYearWeek');
			$dueYearDay = $this->input->post('dueYearDay');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnThe');
			
			//Set task start and end date as par repeat fiter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}

				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDate')));
				$repeatOrder = 1;
			}

			if (!empty($endTaskDate)) 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances)) 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." month"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." year"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			if ($insertVariable == true) 
			{
				$taskIds = array();
				foreach ($machineId as $key => $value) 
				{ 
		            $data=array(
		                'userIds' => $userIds,
		                'machineId' => $value,  
		                'task' => $task,  
		                'repeat' => $repeat,
		                'startDate' => $startDate,
		                'dueDate' => $dueDate,
		                'newTask' => '1',
		                'repeatOrder' => $repeatOrder,
		                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
		                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
		                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
		                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
		                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
		                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
		                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
		                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
		                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
		                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
		                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
		                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
		                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
		                'createdByUserId' => $this->session->userdata('userId')
		            );  
					$taskIds[] = $this->AM->insertData($data, "taskMaintenace");
		        }

		        if (!empty($taskIds)) 
		        {
		        	//Send task data to opapp update in as par assign machine and operator
		        		$select = "taskMaintenace.taskId,
				        		   taskMaintenace.createdByUserId,
				        		   taskMaintenace.task,
				        		   taskMaintenace.repeat,
				        		   taskMaintenace.status,
				        		   taskMaintenace.dueDate,
				        		   taskMaintenace.userIds,
				        		   taskMaintenace.machineId,
				        		   taskMaintenace.dueWeekDay,
				        		   taskMaintenace.monthDueOn,
				        		   taskMaintenace.dueMonthMonth,
				        		   taskMaintenace.dueMonthWeek,
				        		   taskMaintenace.dueMonthDay,
				        		   taskMaintenace.yearDueOn,
				        		   taskMaintenace.dueYearMonth,
				        		   taskMaintenace.dueYearMonthDay,
				        		   taskMaintenace.dueYearWeek,
				        		   taskMaintenace.dueYearDay,
				        		   taskMaintenace.dueYearMonthOnThe,
				        		   taskMaintenace.endTaskDate,
				        		   taskMaintenace.EndAfteroccurances,
				        		   taskMaintenace.newTask
				        		   ";
					$whereIn = array("taskId" => $taskIds);
		        	$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

		        	foreach ($taskData as $key => $value) 
		            {
		            	$taskData[$key]['type'] = 'add';
		            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
		            	$userIds = explode(",", $value['userIds']);

						$taskData[$key]['isLock'] = 1;
						foreach ($userIds as $userKey => $userValue) 
						{
							$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
							$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);

							$taskData[$key]['userDeleteId'] = array();
							$taskData[$key]['userAddId'] = array();
						}
		            }

		        	$json = array("status" => "1","taskData" => $taskData,"message" => Taskaddedsuccessfully);
		        }
		        else
		        {
		        	$json = array("status" => "0","message" => Errorwhileaddingtask);
		        }
		    }else
		    {
		    	$json = array("status" => "0","message" => Endtaskdatecannotbelessthanduedate);
		    }
	        echo json_encode($json); die;
        }
		
	}

	//update_task function use for update task
	public function update_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('task','task','required');
        $count = count($this->input->post('userIds'));
		//checking required parameters validation
        if($this->form_validation->run()==false || $count == 0) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
            echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('taskId');
			$task = $this->input->post('task');
			$repeat = $this->input->post('repeat');
			$taskStatus = !empty($this->input->post('taskStatus')) ? $this->input->post('taskStatus') : array();
			$userIds = implode(",", $this->input->post('userIds'));

			if (!empty($dueDate)) 
			{
	            $data=array(
	                'userIds' =>str_replace("all,", "", $userIds),  
	                'task' => $task,  
	                'repeatOrder' => $repeatOrder,
	                'updatedByUserId' => $this->session->userdata('userId')
	            );  
			}
			else
			{
				 $data=array(
	                'userIds' =>str_replace("all,", "", $userIds),  
	                'task' => $task,  
	                'repeatOrder' => $repeatOrder,
	                'updatedByUserId' => $this->session->userdata('userId')
	            ); 
			}

            $this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");
			foreach ($taskStatus as $keyStatus => $valueStatus) 
            {
            	$this->AM->updateData(array("taskId" => $valueStatus),array("status" => "completed"), "taskMaintenace");
            }

			$taskDetail = $this->AM->getWhereSingle(array("taskId" => $taskId), "taskMaintenace");
			//Update task title as par main task
			if (!empty($taskDetail->mainTaskId)) 
        	{
        		$updateTaskData=array(  
	                'task' => $task
	            );  

	            $this->AM->updateData(array("mainTaskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");
				$this->AM->updateData(array("taskId" => $taskDetail->mainTaskId),$updateTaskData, "taskMaintenace");
        	}
        	else
        	{
        		$updateTaskData=array(  
	                'task' => $task
	            );  
			    $this->AM->updateData(array("mainTaskId" => $taskId),$updateTaskData, "taskMaintenace");
        	}

    	   //Send task data to opapp update in as par assign machine and operator
		   $select = "taskMaintenace.taskId,
        		   taskMaintenace.createdByUserId,
        		   taskMaintenace.task,
        		   taskMaintenace.repeat,
        		   taskMaintenace.status,
        		   taskMaintenace.dueDate,
        		   taskMaintenace.userIds,
        		   taskMaintenace.machineId";

        	$whereIn = array("taskId" => array($taskId));
			$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

        	foreach ($taskData as $key => $value) 
            {
            	$taskData[$key]['type'] = 'update';
            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
            	$userIds = explode(",", $value['userIds']);

				$taskData[$key]['isLock'] = 1;
				foreach ($userIds as $userKey => $userValue) 
				{
					$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
					$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);

					$taskData[$key]['userDeleteId'] = array();
					$taskData[$key]['userAddId'] = array();
				}
            }
			$json = array("status" => "1","taskData" => $taskData,"message" => Taskupdatedsuccessfully);
        	echo json_encode($json); die; 
		}
	}

	//delete_task function use for delete task
	public function delete_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('deleteTaskId','deleteTaskId','required');
        $this->form_validation->set_rules('deleteTaskId','deleteTaskId','required');
        //checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
	        echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('deleteTaskId');
			$data=array(
                'isDelete' => "1"
            );  
            //Delete task
			$this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");

			$this->AM->updateData(array("mainTaskId" => $taskId),$data, "taskMaintenace");
			
            $select = "taskMaintenace.taskId,
	        		   taskMaintenace.createdByUserId,
	        		   taskMaintenace.task,
	        		   taskMaintenace.repeat,
	        		   taskMaintenace.status,
	        		   taskMaintenace.dueDate,
	        		   taskMaintenace.userIds,
	        		   taskMaintenace.machineId,
	        		   taskMaintenace.dueWeekDay,
	        		   taskMaintenace.monthDueOn,
	        		   taskMaintenace.dueMonthMonth,
	        		   taskMaintenace.dueMonthWeek,
	        		   taskMaintenace.dueMonthDay,
	        		   taskMaintenace.yearDueOn,
	        		   taskMaintenace.dueYearMonth,
	        		   taskMaintenace.dueYearMonthDay,
	        		   taskMaintenace.dueYearWeek,
	        		   taskMaintenace.dueYearDay,
	        		   taskMaintenace.dueYearMonthOnThe,
	        		   taskMaintenace.endTaskDate,
	        		   taskMaintenace.EndAfteroccurances,
	        		   taskMaintenace.newTask
	        		   ";
			$whereIn = array("taskId" => array($taskId));
			$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");
			
			foreach ($taskData as $key => $value) 
            {
            	$taskData[$key]['type'] = 'delete';
            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');

            	$taskData[$key]['userDeleteId'] = array();
				$taskData[$key]['userAddId'] = array();
            }

            $json = array("status" => "1","message" => Taskdeletedsuccessfully,"taskData" => $taskData);
	        echo json_encode($json); die; 
		}
	}

	//get_due_date function use for get next or previous due date
	public function get_due_date()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('type','type','required');
		$this->form_validation->set_rules('dueDate','dueDate','required');

		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            echo "Please fill valid details for update task."; die;
        } 
        else 
        {   
			$type = $this->input->post('type');
			$dueDate = $this->input->post('dueDate');
			
			if ($type == "plus") 
			{
				//Get next due date 
				$dueDate = date('m/d/Y',strtotime($dueDate." +1 days"));
			}else
			{
				//Get previous due date 
				$dueDate = date('m/d/Y',strtotime($dueDate." -1 days"));
			}
            echo $dueDate; die; 

        }
	}


	//ecit_task function use for edit task
	public function edit_task() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('userIdsEdit','userIds','required');
        $this->form_validation->set_rules('taskEdit','task','required');
        $this->form_validation->set_rules('repeatEdit','repeat','required');
        $this->form_validation->set_rules('taskIdEdit','taskId','required');

        $repeat = $this->input->post('repeatEdit');
        if ($repeat == "never") 
        {
        	$this->form_validation->set_rules('dueDateEdit','dueDate','required');
        }elseif ($repeat == "everyWeek") 
        {
        	$this->form_validation->set_rules('dueWeekDayEdit','dueWeekDay','required');
        }elseif ($repeat == "everyMonth") 
        {
        	$monthDueOn = $this->input->post('monthDueOnEdit');
        	if ($monthDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueMonthMonthEdit','dueMonthMonth','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueMonthWeekEdit','dueMonthWeek','required');
        		$this->form_validation->set_rules('dueMonthDayEdit','dueMonthDay','required');
        	}
        }elseif ($repeat == "everyYear") 
        {
        	$yearDueOn = $this->input->post('yearDueOnEdit');
        	if ($yearDueOn == "1") 
        	{
        		$this->form_validation->set_rules('dueYearMonthEdit','dueYearMonth','required');
        		$this->form_validation->set_rules('dueYearMonthDayEdit','dueYearMonthDay','required');
        	}else
        	{
        		$this->form_validation->set_rules('dueYearWeekEdit','dueYearWeek','required');
        		$this->form_validation->set_rules('dueYearDayEdit','dueYearDay','required');
        		$this->form_validation->set_rules('dueYearMonthOnTheEdit','dueYearMonthOnThe','required');
        	}
        }
        //checking required parameters validation
        if($this->form_validation->run()==false) 
        { 
            $json = array("status" => "0","message" => Pleasefillvaliddetailsforedittask);
            echo json_encode($json); die;
        } 
        else 
        {   
        	$taskId = $this->input->post('taskIdEdit');
        	$userIds = $this->input->post('userIdsEdit');
			$task = $this->input->post('taskEdit');
			$repeat = $this->input->post('repeatEdit');
			$oldUserIdsEdit = explode(",", $this->input->post('oldUserIdsEdit'));
			
			$endTaskDate = $this->input->post('endTaskDateEdit');
			$EndAfteroccurances = $this->input->post('EndAfteroccurancesEdit');

			$dueWeekDay = $this->input->post('dueWeekDayEdit');
			$monthDueOn = $this->input->post('monthDueOnEdit');
			$dueMonthMonth = $this->input->post('dueMonthMonthEdit');
			$dueMonthWeek = $this->input->post('dueMonthWeekEdit');
			$dueMonthDay = $this->input->post('dueMonthDayEdit');
			$yearDueOn = $this->input->post('yearDueOnEdit');
			$dueYearMonth = $this->input->post('dueYearMonthEdit');
			$dueYearMonthDay = $this->input->post('dueYearMonthDayEdit');
			$dueYearWeek = $this->input->post('dueYearWeekEdit');
			$dueYearDay = $this->input->post('dueYearDayEdit');
			$dueYearMonthOnThe = $this->input->post('dueYearMonthOnTheEdit');
			$repeatOld = $this->input->post('repeatOldEdit');
			
			//Set task start and end date as par repeat fiter
			if ($repeat == "everyDay") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d');
				$repeatOrder = 2;
			}
			elseif ($repeat == "everyWeek") 
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($dueWeekDay)) 
				{
					$dueDate = date('Y-m-d',strtotime("next ".$dueWeekDay));
				}else
				{
					$day = date('l');
					if ($day == "Friday") 
					{
						$dueDate = date('Y-m-d');
					}
					else
					{
						$dueDate = date('Y-m-d',strtotime("next friday"));
					}
				}

				$repeatOrder = 3;
			}
			elseif ($repeat == "everyMonth") 
			{
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				if (!empty($monthDueOn)) 
				{
					if ($monthDueOn == "1") 
					{
						if ($dueMonthMonth != "third_last_day" && $dueMonthMonth != "second_last_day" && $dueMonthMonth != "last_day") 
						{
							$dueDate = date('Y-m-'.$dueMonthMonth);
						}else
						{	
							$dueDate = date('Y-m-t');
							if ($dueMonthMonth == "third_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-2 day ".$dueDate));
							}elseif ($dueMonthMonth == "second_last_day") 
							{
								$dueDate = date('Y-m-d',strtotime("-1 day ".$dueDate));
							}
							
						}
					}elseif ($monthDueOn == "2") 
					{
						if (!empty($dueMonthWeek) && !empty($dueMonthDay)) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),$dueMonthWeek,$dueMonthDay);
							if (date('m') != date('m',strtotime($weekEndDate))) 
							{
								$weekEndDate = $this->getFirstandLastDate(date('Y'),date('m'),'4',$dueMonthDay);
							}
							$dueDate = date('Y-m-d',strtotime($weekEndDate));
						}else
						{
							$dueDate = date('Y-m-t');
						}
					}else
					{
						$dueDate = date('Y-m-t');
					}	
				}else
				{
					$dueDate = date('Y-m-t');
				}

				if (date('Y-m-d') >=  date('Y-m-d',strtotime($dueDate))) 
				{
					$startDate = date('Y-m-01',strtotime("+1 months".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 months ".$dueDate));	
				}
				$repeatOrder = 4;
			}
			elseif ($repeat == "everyYear") 
			{
				$monthDueOn = NULL;
				
				if ($yearDueOn == "1") 
				{
					$monthYear = date('m',strtotime($dueYearMonth));
					if ($dueYearMonthDay != "third_last_day" && $dueYearMonthDay != "second_last_day" && $dueYearMonthDay != "last_day") 
					{
						$dueDate = date('Y-'.$monthYear.'-'.$dueYearMonthDay);
					}else
					{	
						$dueDate = date('Y-'.$monthYear.'-t');
						if ($dueYearMonthDay == "third_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-2 day ".$dueDate));
						}elseif ($dueYearMonthDay == "second_last_day") 
						{
							$dueDate = date('Y-'.$monthYear.'-d',strtotime("-1 day ".$dueDate));
						}else
						{
							$dueDate = date('Y-m-t',strtotime($dueDate));
						}
					}
					$startDate = date('Y-'.$monthYear.'-01');
				}elseif ($yearDueOn == "2") 
				{
					if(!empty($dueYearMonthOnThe) && !empty($dueYearWeek) && !empty($dueYearDay))
					{
						$monthYear = date('m',strtotime($dueYearMonthOnThe));
						$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,$dueYearWeek,$dueYearDay);
						if ($monthYear != date('m',strtotime($weekEndDate))) 
						{
							$weekEndDate = $this->getFirstandLastDate(date('Y'),$monthYear,'4',$dueYearDay);
						}
						$startDate = date('Y-'.$monthYear.'-01');
						$dueDate = date('Y-m-d',strtotime($weekEndDate));
					}else
					{
						$startDate = date('Y-m-d');
						$dueDate = date('Y-m-d',strtotime("12/31"));
					}
				}else
				{
					$startDate = date('Y-m-d');
					$dueDate = date('Y-m-d',strtotime("12/31"));	
				}

				if (date('Y-m-d') >=  $dueDate) 
				{
					$startDate = date('Y-01-01',strtotime("+1 years".$startDate));
					$dueDate = date('Y-m-d',strtotime("+1 years ".$dueDate));	
				}
				$repeatOrder = 5;	
			}
			else
			{
				$monthDueOn = NULL;
				$yearDueOn = NULL;
				$startDate = date('Y-m-d');
				$dueDate = date('Y-m-d',strtotime($this->input->post('dueDateEdit')));
				$repeatOrder = 1;
			}

			if (!empty($endTaskDate)) 
			{
				$endTaskDate = date('Y-m-d',strtotime($endTaskDate));
				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}elseif (!empty($EndAfteroccurances)) 
			{
				if ($repeat == "everyDay") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyWeek") 
				{
					$endTaskDate = date('Y-m-d',strtotime("+".$EndAfteroccurances." week"));
				}elseif ($repeat == "everyMonth") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." month"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}elseif ($repeat == "everyYear") 
				{
					$endTaskDate = date('Y-m-d',strtotime($dueDate." +".$EndAfteroccurances." year"));
					$endTaskDate = date('Y-m-d',strtotime($endTaskDate." +".$EndAfteroccurances." day"));
				}

				if ($endTaskDate > $dueDate) 
				{
					$insertVariable = true;
				}else
				{
					$insertVariable = false;
				}
			}
			else
			{
				$insertVariable = true;
			}


			$resultTaskData = $this->AM->getWhereSingle(array("taskId" => $taskId),"taskMaintenace");

			$taskIds = array($taskId);
			$addMachineId = array();
			if ($insertVariable == true) 
			{
				$machineId =  $this->input->post('machineIdEdit');

				if (!empty($machineId)) 
				{
					foreach ($machineId as $key => $value) 
					{ 
						if ($resultTaskData->machineId != $value) 
						{

							$addMachineId[] = $value;
							$data=array(
				                'userIds' => $userIds,
				                'machineId' => $value,  
				                'task' => $task,  
				                'repeat' => $repeat,
				                'startDate' => $startDate,
				                'dueDate' => $dueDate,
				                'newTask' => '1',
				                'repeatOrder' => $repeatOrder,
				                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
				                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
				                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
				                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
				                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
				                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
				                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
				                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
				                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
				                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
				                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
				                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
				                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL,
				                'createdByUserId' => $this->session->userdata('userId')
				            );  
							$taskIds[] = $this->AM->insertData($data, "taskMaintenace");
						}
			        }
				}


				$editData=array(
	                'userIds' => $userIds,
	                'task' => $task,  
	                'repeat' => $repeat,
	                'startDate' => $startDate,
	                'dueDate' => $dueDate,
	                'newTask' => '1',
	                'repeatOrder' => $repeatOrder,
	                'endTaskDate' => !empty($endTaskDate) ? date('Y-m-d',strtotime($endTaskDate)) : NULL,
	                'EndAfteroccurances' => !empty($EndAfteroccurances) ? $EndAfteroccurances : NULL,
	                'dueWeekDay' => !empty($dueWeekDay) ? $dueWeekDay : NULL,
	                'monthDueOn' => !empty($monthDueOn) ? $monthDueOn : NULL,
	                'dueMonthMonth' => !empty($dueMonthMonth) ? $dueMonthMonth : NULL,
	                'dueMonthWeek' => !empty($dueMonthWeek) ? $dueMonthWeek : NULL,
	                'dueMonthDay' => !empty($dueMonthDay) ? $dueMonthDay : NULL,
	                'yearDueOn' => !empty($yearDueOn) ? $yearDueOn : NULL,
	                'dueYearMonth' => !empty($dueYearMonth) ? $dueYearMonth : NULL,
	                'dueYearMonthDay' => !empty($dueYearMonthDay) ? $dueYearMonthDay : NULL,
	                'dueYearWeek' => !empty($dueYearWeek) ? $dueYearWeek : NULL,
	                'dueYearDay' => !empty($dueYearDay) ? $dueYearDay : NULL,
	                'dueYearMonthOnThe' => !empty($dueYearMonthOnThe) ? $dueYearMonthOnThe : NULL
	            );  
				
				$this->AM->updateData(array("taskId" => $taskId),$editData, "taskMaintenace");

				
				$lastSubTask = $this->AM->getLastSubTask($taskId);
				if (!empty($lastSubTask)) 
				{
					$this->AM->updateData(array("taskId" => $lastSubTask->taskId),$editData, "taskMaintenace");
				}

		        if (!empty($taskIds)) 
		        {
		        	//Send task data to opapp update in as par assign machine and operator
		        	$select = "taskMaintenace.taskId,
			        		   taskMaintenace.createdByUserId,
			        		   taskMaintenace.task,
			        		   taskMaintenace.repeat,
			        		   taskMaintenace.status,
			        		   taskMaintenace.dueDate,
			        		   taskMaintenace.userIds,
			        		   taskMaintenace.machineId,
			        		   taskMaintenace.dueWeekDay,
			        		   taskMaintenace.monthDueOn,
			        		   taskMaintenace.dueMonthMonth,
			        		   taskMaintenace.dueMonthWeek,
			        		   taskMaintenace.dueMonthDay,
			        		   taskMaintenace.yearDueOn,
			        		   taskMaintenace.dueYearMonth,
			        		   taskMaintenace.dueYearMonthDay,
			        		   taskMaintenace.dueYearWeek,
			        		   taskMaintenace.dueYearDay,
			        		   taskMaintenace.dueYearMonthOnThe,
			        		   taskMaintenace.endTaskDate,
			        		   taskMaintenace.EndAfteroccurances,
			        		   taskMaintenace.newTask
			        		   ";
					$whereIn = array("taskId" => $taskIds);
		        	$taskData = $this->AM->getWhereIn($select,$whereIn,"taskMaintenace");

					
		        	foreach ($taskData as $key => $value) 
		            {
		            	$userDelete = array();
						$userAdd = array();
		            	if (in_array($value['machineId'], $addMachineId))
					  	{
					  		$taskData[$key]['type'] = 'add';
					    }
						else
					  	{
					  		if ($repeatOld != $repeat) 
					  		{
		            			$taskData[$key]['type'] = 'repeatTask';
					  		}else
					  		{
		            			$taskData[$key]['type'] = 'update';
					  		}
					  	}


		            	$taskData[$key]['factoryId'] = $this->session->userdata('factoryId');
		            	$userIds = explode(",", $value['userIds']);

		            	$array_intersect = array_intersect($oldUserIdsEdit,$userIds);

						$taskData[$key]['isLock'] = 1;
						foreach ($userIds as $userKey => $userValue) 
						{
							foreach ($oldUserIdsEdit as $oldkey => $oldvalue) 
							{
								if (in_array($oldvalue, $userIds)) 
								{
								}else
								{
									if (in_array($userValue, $oldUserIdsEdit)) 
									{
										$userDelete[] = $oldvalue;
									}else
									{
										$userAdd[] = $userValue;
									}
								}
							}

							if (!in_array($userValue, $oldUserIdsEdit)) 
							{
								$userAdd[] = $userValue;
							}
							$users = $this->AM->getWhereDBSingle(array("userId" => $userValue),"user");
							$taskData[$key]['operators'][$userKey] = array("userId" => $users->userId,"userName" => $users->userName);
						}

						$taskData[$key]['userDeleteId'] = !empty($array_intersect) ? array_unique($userDelete) : array_unique($oldUserIdsEdit);
						$taskData[$key]['userAddId'] = array_unique($userAdd);
						unset($userDelete);
						unset($userAdd);
		            }

		        	$json = array("status" => "1","taskData" => $taskData,"message" => Taskeditedsuccessfully);
		        }
		        else
		        {
		        	$json = array("status" => "0","message" => Errorwhileaddingtask);
		        }
		    }else
		    {
		    	$json = array("status" => "0","message" => Endtaskdatecannotbelessthanduedate);
		    }
	        echo json_encode($json); die;
        }
		
	}


	//update_task function use for update task
	public function update_task_status() 
	{	
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

        $this->form_validation->set_rules('status','status','required');
        $this->form_validation->set_rules('taskId','taskId','required');
		//checking required parameters validation
        if($this->form_validation->run()==false) 
        { 

            $json = array("status" => "0","message" => Pleasefillvaliddetailsforupdatetask);
            echo json_encode($json); die; 
        } 
        else 
        {   
			$taskId = $this->input->post('taskId');
			$status = $this->input->post('status');


			$data=array(
                'status' => $status
            );  

			$this->AM->updateData(array("taskId" => $taskId),$data, "taskMaintenace");
			

			$select = "taskMaintenace.taskId,
	        		   taskMaintenace.status as taskStatus,
	        		   taskMaintenace.machineId";
			$whereIn = array("taskId" => $taskId);
			$taskData = $this->AM->getWhereSingleSelect($select,$whereIn,"taskMaintenace");
			
			$taskData->factoryId = $this->session->userdata('factoryId');

            $json = array("status" => "1","taskData" => $taskData);
	        echo json_encode($json); die; 
		}
	}

	
}

?>