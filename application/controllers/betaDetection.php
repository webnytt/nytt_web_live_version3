<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class betaDetection extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

			// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
	}

	public function index() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachinesNoStacklight($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachinesNoStacklight($this->factory, $this->session->userdata('userId')); 
		}
		
		$data = array(
		    'listMachines'=>$listMachines
		    );

		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('betaDetectionsNewFile', $data);
		$this->load->view('footer');
		$this->load->view('script_file/beta_detection_footer');
	}

	function exportBetaDetectionCsv()
    {

    	$machineId = 0; 
		if($this->input->get('machineId')) 
		{ 
			if($this->input->get('machineId') > 0 ) 
			{
				$machineId = $this->input->get('machineId');
			}
		} 
		if($this->input->get('dateValS')) 
		{ 
			$dateValS = $this->input->get('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->get('dateValE')) 
		{ 
			$dateValE = $this->input->get('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}

		$searchValue = $this->input->get('serchFiled');


		 if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
    	$listColorLogs = $this->AM->getallBetaColorLogsExportCsvs($dateValS, $dateValE, $machineId, $searchQuery)->result();
    	$data = array();

    	for ($key=0;$key<count($listColorLogs);$key++) 
		{

			
			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1") 
				{
					$listColorLogs[$key]->machineStatus = "No detection";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = "Off";
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;
				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

			$data[] = array(
				"machineId" => $listColorLogs[$key]->machineId,
				"machineName" => $listColorLogs[$key]->machineName,
				"color" => $listColorLogs[$key]->color,
				"machineStatus" => ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus,
				"originalTime" => $listColorLogs[$key]->originalTime
			);
		}


		header("Content-type: application/csv");
        header("Content-Disposition: attachment; filename=\"Beta detection".".csv\"");
        header("Pragma: no-cache");
        header("Expires: 0");
		$handle = fopen('php://output', 'w');
        fputcsv($handle, array("Beta detection log"));
        $cnt=1;
        foreach ($data as $key) 
        {
        	if ($cnt == 1) 
        	{
        		$narray=array("Machine id","Machine name","Color","Machine status","Time");
            	fputcsv($handle, $narray);
        	}
            $narray=array($key['machineId'],$key["machineName"],$key["color"],$key["machineStatus"],$key["originalTime"]);
            fputcsv($handle, $narray);
            $cnt++;
        }
        fclose($handle);
        exit;
	}
	
	public function beta_detection_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		if($this->input->post('dateValS')) 
		{ 
			$dateValS = $this->input->post('dateValS')." 00:00:00";
		} 
		else 
		{
			$dateValS = date("Y-m-d", strtotime('today - 29 days'))." 00:00:00";
		}
		
		if($this->input->post('dateValE')) 
		{ 
			$dateValE = $this->input->post('dateValE')." 23:59:59";
		}
		else 
		{
			$dateValE = date("Y-m-d", strtotime('today'))." 23:59:59";
		}
		
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length']; 
		$columnIndex = $_POST['order'][0]['column']; 
		$columnName = $_POST['columns'][$columnIndex]['data'];
		$columnSortOrder = $_POST['order'][0]['dir'];
		$searchValue = $_POST['search']['value']; 
		
		$searchQuery = " ";
		if($searchValue != '')
		{

		   if ($searchValue == "No production") 
		   {
		   	  $searchQuery = " and (
				betaTable.color like '%2%' 
				) ";  
		   }else if ($searchValue == "production") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%0%' 
				) ";   
		   }else if ($searchValue == "setup") 
		   {
		   	   $searchQuery = " and (
				betaTable.color like '%1%' 
				) ";  
		   }else
		   {
		   	$searchQuery = " and (
				betaTable.machineId like '%".$searchValue."%' or 
				betaTable.color like '%".$searchValue."%' or 
				betaTable.originalTime like '%".$searchValue."%' or
				machine.machineName like '%".$searchValue."%' 
				) ";  
		   }
		   
		}
		
		$listColorLogsCount = $this->AM->getallBetaColorLogsCounts($this->factory, $machineId, $is_admin)->row()->totalCount;
		$listColorLogsSearchCount = $this->AM->getSearchBetaColorLogsCounts($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $searchQuery)->row()->totalCount;
		$listColorLogs = $this->AM->getallBetaColorLogss($this->factory, $dateValS, $dateValE, $machineId, $is_admin, $row, $rowperpage, $searchQuery, $columnIndex, $columnSortOrder)->result();  

		for ($key=0;$key<count($listColorLogs);$key++) 
		{

			$listColorLogs[$key]->machineStatus = "Production";
			$colorS = explode(" ", $listColorLogs[$key]->color);
			$redStateVal = (in_array("red", $colorS)) ? "1" : "0";
			$greenStateVal = (in_array("green", $colorS)) ? "1" : "0";
			$yellowStateVal = (in_array("yellow", $colorS)) ? "1" : "0";
			$blueStateVal = (in_array("blue", $colorS)) ? "1" : "0";
			$whiteStateVal = (in_array("white", $colorS)) ? "1" : "0";
			$offStatus = (in_array("off", $colorS)) ? "1" : "0";
			$NoDataStatus = (in_array("NoData", $colorS)) ? "1" : "0";


			$machineStateColorLookup = $this->AM->getWhereSingle(array("machineId" => $listColorLogs[$key]->machineId,"redStatus" => $redStateVal,"greenStatus" => $greenStateVal,"yellowStatus" => $yellowStateVal,"blueStatus" => $blueStateVal,"whiteStatus" => $whiteStateVal),"machineStateColorLookup");

		/*	if ($listColorLogs[$key]->color == "green") 
			{
				echo $this->factory->last_query();exit;
			}
*/
			

			if (!empty($machineStateColorLookup)) 
			{
				
				if ($NoDataStatus == "1") 
				{
					$listColorLogs[$key]->machineStatus = "No detection";
				}else if($offStatus == "1")
				{
					$listColorLogs[$key]->machineStatus = "Off";
				}else
				{
					$listColorLogs[$key]->machineStatus = $machineStateColorLookup->machineStateVal;

				}
			}

			if ($listColorLogs[$key]->color == "0") 
			{
				$listColorLogs[$key]->machineStatus = "NoData";
				$listColorLogs[$key]->color = "NoData";
			}
			else if ($listColorLogs[$key]->color == "2") 
			{
				$listColorLogs[$key]->machineStatus = "No production";
				$listColorLogs[$key]->color = "NoData";
			}else if ($listColorLogs[$key]->color == "1") 
			{
				$listColorLogs[$key]->machineStatus = "Setup";
				$listColorLogs[$key]->color = "NoData";
			}

				$listColorLogs[$key]->machineStatus = ($listColorLogs[$key]->machineStatus == "nodet") ? "Invalid" : $listColorLogs[$key]->machineStatus;
		}
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listColorLogsCount, 
		  "iTotalDisplayRecords" => $listColorLogsSearchCount, 
		  "aaData" => $listColorLogs
		);
		//print_r($response);exit;
		echo json_encode($response); die;
	}
	
}

?>