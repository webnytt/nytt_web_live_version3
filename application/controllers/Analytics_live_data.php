<?php
defined('BASEPATH') OR exit('No direct script access allowed');
class Analytics_live_data extends CI_Controller 
{
	var $factory;
	var $factoryId;
	
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) {
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 
		}
	}
	
	function getLiveDashboard3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$post = $this->input->post();
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/testadmin/dashboard3_paginationLiveTest');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
	}

	public function live_dashboard3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$listMachines = array();
		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/admin/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_dashboard3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_dashboard3_footer');
	}
	public function breakdown() 
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
			$listMachines = $this->AM->getallMachines($this->factory);
		} 
		else 
		{
			$is_admin = 0;
			$listMachines = $this->AM->getAssignedMachines($this->factory, $this->session->userdata('userId')); 
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('breakdown', $data);
		$this->load->view('footer');
		$this->load->view('script_file/breakdown_footer');
	}
	public function breakdown_pagination() 
	{ 
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$choose1 = $this->input->post('choose1'); 
		$choose2 = $this->input->post('choose2'); 
		$choose3 = $this->input->post('choose3'); 
		$choose4 = $this->input->post('choose4'); 
		$result['printQuery'] = '';
		$listMachines = array();
		if($this->AM->checkUserRole()) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}
		$choose2Pad = str_pad($choose2, 2, '0', STR_PAD_LEFT);
		$choose4Pad = str_pad($choose4, 2, '0', STR_PAD_LEFT);
		$result['daily']['filter1'] = 0; $result['daily']['filter1count'] = 0; $x1 = 0; $y1 = 0; $br11 = 0; $br12 = 0; $br13 = 0; $br14 = 0; $br15 = 0; 
		$result['daily']['filter2'] = 0; $result['daily']['filter2count'] = 0; $x2 = 0; $y2 = 0; $br21 = 0; $br22 = 0; $br23 = 0; $br24 = 0; $br25 = 0;
		$result['daily']['filter3'] = 0; $result['daily']['filter3count'] = 0; $x3 = 0; $y3 = 0; $br31 = 0; $br32 = 0; $br33 = 0; $br34 = 0; $br35 = 0;
		$result['daily']['filter4'] = 0; $result['daily']['filter4count'] = 0; $x4 = 0; $y4 = 0; $br41 = 0; $br42 = 0; $br43 = 0; $br44 = 0; $br45 = 0;
		$result['daily']['filter5'] = 0; $result['daily']['filter5count'] = 0; $x5 = 0; $y5 = 0; $br51 = 0; $br52 = 0; $br53 = 0; $br54 = 0; $br55 = 0;
		$br61 = 0; $br62 = 0; $br63 = 0; $br64 = 0; $br65 = 0; $br71 = 0; $br72 = 0; $br73 = 0; $br74 = 0; $br75 = 0; $br81 = 0; $br82 = 0; $br83 = 0; $br84 = 0; $br85 = 0;  
		$result['daily']['reasonfilter1'] = array();
		$result['daily']['reasonfilter2'] = array();
		$result['daily']['reasonfilter3'] = array();
		$result['daily']['reasonfilter4'] = array();
		$result['daily']['reasonfilter5'] = array();
		
		if($factoryData->duration5Check == '1') 
		{
			$result['daily']['filter5'] = 0; $result['daily']['filter5count'] = 0; $x5 = 0;
			$w3End = $factoryData->duration5*60;
			$w4End = $factoryData->duration5*60;
			$w5End = 9999999999; 
		}
		if($factoryData->duration4Check == '1') 
		{
			$result['daily']['filter4'] = 0; $result['daily']['filter4count'] = 0; $x4 = 0;
			$w3End = $factoryData->duration4*60;
		}
		if($factoryData->duration5Check == '0' && $factoryData->duration4Check == '0') 
		{
			$w3End = 9999999999;
		} 
		if($factoryData->duration5Check == '0' && $factoryData->duration4Check == '1' ) 
		{
			$w4End = 9999999999;
		}
		if($factoryData->duration5Check == '1' && $factoryData->duration4Check == '0' ) 
		{
			$w5End = 9999999999;
		}
		$listReasons = $this->AM->getallReasons()->result();
		$resonCount = count($listReasons);
		$listReasons[$resonCount] = new stdClass;
		$listReasons[$resonCount]->breakdownReasonId = $resonCount+1;   
		$listReasons[$resonCount]->breakdownReasonText = '';
		$resultStopsQ = $this->AM->getStopAnalyticsData($this->factory, $machineId, $choose1, $choose2, $choose4Pad); 
		if($resultStopsQ == '' ) 
		{
			$resultStops = array();
		} 
		else 
		{
			$resultStops = $resultStopsQ->result();
			if(count($resultStops) > 0 ) 
			{
				for($x=0;$x<count($resultStops);$x++) 
				{
					if($resultStops[$x]->colorDuration >= $factoryData->duration1*60 && $resultStops[$x]->colorDuration < $factoryData->duration2*60) 
					{ 
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para1 = 'br'.$list->breakdownReasonId.'1';
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter1']['li'.$list->breakdownReasonId][$$para1][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para1++;
									break; 
								}
							} 
						} 
						else 
						{
							$result['daily']['stopfilter1'][$x1][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter1'][$x1][1] = $resultStops[$x]->colorDuration;
							$x1++;
						}
						$result['daily']['filter1'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter1count']++; 
					} 
					if($resultStops[$x]->colorDuration >= $factoryData->duration2*60 && $resultStops[$x]->colorDuration < $factoryData->duration3*60) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para3 = 'br'.$list->breakdownReasonId.'2';
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter2']['li'.$list->breakdownReasonId][$$para3][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para3++;
									break; 
								}
							}
							
						} 
						else
						{
							$result['daily']['stopfilter2'][$x2][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter2'][$x2][1] = $resultStops[$x]->colorDuration;
							$x2++;
						}
						$result['daily']['filter2'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter2count']++;
					} 
					if($resultStops[$x]->colorDuration >= $factoryData->duration3*60 && $resultStops[$x]->colorDuration < $w3End) 
					{
						if( $resultStops[$x]->commentFlag == '1') 
						{
							
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para5 = 'br'.$list->breakdownReasonId.'3';
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter3']['li'.$list->breakdownReasonId][$$para5][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para5++;
									break; 
								}
							}
							
						} 
						else
						{
							$result['daily']['stopfilter3'][$x3][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter3'][$x3][1] = $resultStops[$x]->colorDuration; 
							$x3++;
						}
						$result['daily']['filter3'] += $resultStops[$x]->colorDuration;
						$result['daily']['filter3count']++;
					}
					if($factoryData->duration4Check == '1') 
					{
						if($resultStops[$x]->colorDuration >= $factoryData->duration4*60 && $resultStops[$x]->colorDuration < $w4End) 
						{
							if( $resultStops[$x]->commentFlag == '1') {
							
							$flag = 0;
							foreach($listReasons as $list) 
							{
								if ($resultStops[$x]->comment == $list->breakdownReasonText) 
								{
									$para7 = 'br'.$list->breakdownReasonId.'4';
									$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][0] = $resultStops[$x]->timeS;
									$result['daily']['reasonfilter4']['li'.$list->breakdownReasonId][$$para7][1] = $resultStops[$x]->colorDuration; 
									$flag = 1; $$para7++;
									break; 
								}
							}
							
						} 
						else 
						{
							$result['daily']['stopfilter4'][$x4][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter4'][$x4][1] = $resultStops[$x]->colorDuration;
							 $x4++;
						}
							$result['daily']['filter4'] += $resultStops[$x]->colorDuration;
							$result['daily']['filter4count']++;
						}
					}
					if($factoryData->duration5Check == '1') 
					{
						if($resultStops[$x]->colorDuration >= $factoryData->duration5*60 && $resultStops[$x]->colorDuration < $w5End) 
						{
							if( $resultStops[$x]->commentFlag == '1') 
							{ 	
								$flag = 0;
								foreach($listReasons as $list) 
								{
									if ($resultStops[$x]->comment == $list->breakdownReasonText) 
									{
										$para9 = 'br'.$list->breakdownReasonId.'5'; 
										$result['daily']['reasonfilter5']['li'.$list->breakdownReasonId][$$para9][0] = $resultStops[$x]->timeS;
										$result['daily']['reasonfilter5']['li'.$list->breakdownReasonId][$$para9][1] = $resultStops[$x]->colorDuration; 
										$flag = 1; $$para9++;
										break; 
									}
								}
								
							} else {
							$result['daily']['stopfilter5'][$x5][0] = $resultStops[$x]->timeS;
							$result['daily']['stopfilter5'][$x5][1] = $resultStops[$x]->colorDuration; 
							$x5++;
							}
							$result['daily']['filter5'] += $resultStops[$x]->colorDuration;
							$result['daily']['filter5count']++; 
						}
					}
				}  
				if($result['daily']['filter1'] != 0) 
				{
					$result['daily']['filter1'] = sprintf('%0.2f', $result['daily']['filter1']/60);
				}
				if($result['daily']['filter2'] != 0) 
				{
					$result['daily']['filter2'] = sprintf('%0.2f', $result['daily']['filter2']/60);
				}
				if($result['daily']['filter3'] != 0) 
				{
					$result['daily']['filter3'] = sprintf('%0.2f', $result['daily']['filter3']/60);
				}
				if($factoryData->duration4Check == '1') 
				{
					if($result['daily']['filter4'] != 0) 
					{
						$result['daily']['filter4'] = sprintf('%0.2f', $result['daily']['filter4']/60);
					}
				}
				if($factoryData->duration5Check == '1') 
				{
					if($result['daily']['filter5'] != 0) 
					{
						$result['daily']['filter5'] = sprintf('%0.2f', $result['daily']['filter5']/60);
					}
				}
				
			}
		}
		
		$filter1Val = $this->input->post('filter1Val'); 
		$filter2Val = $this->input->post('filter2Val'); 
		$filter3Val = $this->input->post('filter3Val'); 
		$filter4Val = $this->input->post('filter4Val'); 
		$filter5Val = $this->input->post('filter5Val'); 
		  
		for($i=0;$i<count($listReasons);$i++) 
		{ 
			$reasonNameArr[$i] = $listReasons[$i]->breakdownReasonText;
			if($filter1Val == '1') 
			{
				$minDuration1 = $factoryData->duration1*60;
				$maxDuration1 = $factoryData->duration2*60;
				$issueReasonArr1 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration1, $maxDuration1, $machineId);
				$reasonCountArr11[$i]['value'] = 0; $reasonCountArr11Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr1)) 
				{
					for($x=0;$x<count($issueReasonArr1);$x++)
					{
						if($issueReasonArr1[$x]->comment == $reasonNameArr[$i]) 
						{ 
							$reasonCountArr11[$i]['value']++;
							$reasonCountArr11Duration[$i]['value'] += $issueReasonArr1[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr1'] = $reasonCountArr11;
				$result['daily']['reasonCountArrDuration1'] = $reasonCountArr11Duration;
			}
			
			if($filter2Val == '1') 
			{
				$minDuration2 = $factoryData->duration2*60;
				$maxDuration2 = $factoryData->duration3*60;
				$issueReasonArr2 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration2, $maxDuration2, $machineId);
				$reasonCountArr21[$i]['value'] = 0;  $reasonCountArr21Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr2)) 
				{
					for($x=0;$x<count($issueReasonArr2);$x++)
					{
						if($issueReasonArr2[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr21[$i]['value']++;
							$reasonCountArr21Duration[$i]['value'] += $issueReasonArr2[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr2'] = $reasonCountArr21; 
				$result['daily']['reasonCountArrDuration2'] = $reasonCountArr21Duration;
			}
			
			if($filter3Val == '1') 
			{
				$minDuration3 = $factoryData->duration3*60;
				$maxDuration3 = $w3End;
				$issueReasonArr3 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration3, $maxDuration3, $machineId);
				$reasonCountArr31[$i]['value'] = 0;  $reasonCountArr31Duration[$i]['value'] = 0; 
				if(is_array($issueReasonArr3)) 
				{
					for($x=0;$x<count($issueReasonArr3);$x++)
					{
						if($issueReasonArr3[$x]->comment == $reasonNameArr[$i]) 
						{
							$reasonCountArr31[$i]['value']++;
							$reasonCountArr31Duration[$i]['value'] += $issueReasonArr3[$x]->timeDiff;
						}
					}
				}
				$result['daily']['reasonCountArr3'] = $reasonCountArr31; 
				$result['daily']['reasonCountArrDuration3'] = $reasonCountArr31Duration;
			}
			
			if($factoryData->duration4Check == '1') 
			{
				if($filter4Val == '1') 
				{
					$minDuration4 = $factoryData->duration4*60;
					$maxDuration4 = $w4End;
					$issueReasonArr4 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration4, $maxDuration4, $machineId);
					$reasonCountArr41[$i]['value'] = 0;  $reasonCountArr41Duration[$i]['value'] = 0; 
					if(is_array($issueReasonArr4)) 
					{
						for($x=0;$x<count($issueReasonArr4);$x++)
						{
							if($issueReasonArr4[$x]->comment == $reasonNameArr[$i])
							{
								$reasonCountArr41[$i]['value']++;
								$reasonCountArr41Duration[$i]['value'] += $issueReasonArr4[$x]->timeDiff;
							}
						}
					}
					$result['daily']['reasonCountArr4'] = $reasonCountArr41; 
					$result['daily']['reasonCountArrDuration4'] = $reasonCountArr41Duration;
				}
			}
			
			if($factoryData->duration5Check == '1') 
			{
				if($filter5Val == '1') 
				{
					$minDuration5 = $factoryData->duration5*60;
					$maxDuration5 = $w5End;
					$issueReasonArr5 = $this->AM->getIssueReasonAnalyticsData($choose1, $choose2, $choose4, $minDuration5, $maxDuration5, $machineId);
					$reasonCountArr51[$i]['value'] = 0;  $reasonCountArr51Duration[$i]['value'] = 0;  
					if(is_array($issueReasonArr5)) 
					{
						for($x=0;$x<count($issueReasonArr5);$x++)
						{
							if($issueReasonArr5[$x]->comment == $reasonNameArr[$i]) 
							{
								$reasonCountArr51[$i]['value']++;
								$reasonCountArr51Duration[$i]['value'] += $issueReasonArr5[$x]->timeDiff;
							}
						}
					}
					$result['daily']['reasonCountArr5'] = $reasonCountArr51;
					$result['daily']['reasonCountArrDuration5'] = $reasonCountArr51Duration;
				}
			} 
		}
		$reasonNameArr[$resonCount] = 'Unanswered'; 
		$result['daily']['reasonNameArr'] = $reasonNameArr;
		$result['daily']['reasonCountArr1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][0]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArr1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][1]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArr1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][2]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArr1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][3]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArr1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][4]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArr1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][5]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArr1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][6]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArr1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr4'][7]['itemStyle']['color'] = $result['daily']['reasonCountArr5'][7]['itemStyle']['color'] = '#d32f2f';
		
		$result['daily']['reasonCountArrDuration1'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][0]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][0]['itemStyle']['color'] = '#CC6600';
		$result['daily']['reasonCountArrDuration1'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][1]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][1]['itemStyle']['color'] = '#9933CC';
		$result['daily']['reasonCountArrDuration1'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][2]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][2]['itemStyle']['color'] = '#0000FF';
		$result['daily']['reasonCountArrDuration1'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][3]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][3]['itemStyle']['color'] = '#FF00CC';
		$result['daily']['reasonCountArrDuration1'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][4]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][4]['itemStyle']['color'] = '#999900';
		$result['daily']['reasonCountArrDuration1'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][5]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][5]['itemStyle']['color'] = '#FF6600';
		$result['daily']['reasonCountArrDuration1'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][6]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][6]['itemStyle']['color'] = '#00796b';
		$result['daily']['reasonCountArrDuration1'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration2'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration3'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration4'][7]['itemStyle']['color'] = $result['daily']['reasonCountArrDuration5'][7]['itemStyle']['color'] = '#d32f2f';
		echo json_encode($result); die;  
	}

	public function live_breakdown() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
		} 
		else 
		{
			$is_admin = 0;
		}

		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/admin/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);
		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines'=>$listMachines,
			'factoryData'=>$factoryData 
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_breakdown', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_breakdown_footer');
	}

	function getLiveBreakdownData()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$post = $this->input->post();
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/admin/breakdown_paginationLiveTest');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
	}


	public function live_breakdown3() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}
		$machineId = 0; 
		if($this->input->post('machineId')) 
		{ 
			if($this->input->post('machineId') > 0 ) 
			{
				$machineId = $this->input->post('machineId');
			}
		} 
		
		$listMachines = array();
		if($this->AM->checkUserRole() > 0 ) 
		{
			$is_admin = $this->AM->checkUserRole();
		}
		else 
		{
			$is_admin = 0;
		}

		$post = array("factoryId" => $this->factoryId);
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/admin/getMachine');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$listMachines = json_decode($response);

		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/testadmin/factoryData');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		$liveFactoryData = json_decode($response);

		$factoryData = $this->AM->getFactoryData($this->factoryId); 
		$data = array(
		    'listMachines' => $listMachines,
			'factoryData' => $factoryData,
			'liveFactoryData' => $liveFactoryData
		    );
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('live_breakdown3', $data);
		$this->load->view('footer');
		$this->load->view('script_file/live_breakdown3_footer');
	}

	function getLiveBreakdown3Data()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', 'Sign in first to access webadmin.'); 
		    redirect('admin/login');
		}

		$post = $this->input->post();
		$ch = curl_init('http://nyttcloud.host/prodapp/version2/testadmin/breakdown3_paginationLiveTest');
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_POSTFIELDS, $post);
		$response = curl_exec($ch);
		curl_close($ch);
		echo $response;
	}
}
?>