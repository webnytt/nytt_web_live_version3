<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Operator extends CI_Controller 
{
	var $factory;
	var $factoryId;
	function __construct() 
	{
		parent::__construct();                        
		$this->load->model('Admin_model','AM'); 
		if($this->AM->checkIsvalidated() == true) 
		{
			$this->factoryId = $this->session->userdata('factoryId'); 
			$this->factory = $this->load->database('factory'.$this->factoryId, TRUE);  
			$this->listFactories = $this->AM->getallFactories(); 

			$this->factory->query('SET SESSION sql_mode = ""');

		// ONLY_FULL_GROUP_BY
			$this->factory->query('SET SESSION sql_mode =
	                  REPLACE(REPLACE(REPLACE(
	                  @@sql_mode,
	                  "ONLY_FULL_GROUP_BY,", ""),
	                  ",ONLY_FULL_GROUP_BY", ""),
	                  "ONLY_FULL_GROUP_BY", "")');
		}

		$this->load->library('encryption');
		$site_lang = $this->session->userdata('site_lang');
		$this->AM->getLanguage($site_lang);
	}

	//operator_details function use for operator details page
	
	//in use
	public function operator_details() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$listMachines = $this->AM->getallMachines($this->factory);  
		$listOperators = $this->AM->getallOperators($this->factoryId)->result();
		$newUserId=$this->AM->lastUserId()->maxId + 1;
		foreach ($listOperators as $key => $value) 
		{	
			$result = $this->AM->checkOperatorOnline($value->userId);
			$listOperators[$key]->isOnline = !empty($result->isActive == "1") ? "1" : "0";
			if (!empty($result->workingMachine)) 
			{
				$workingMachines = explode(",", $result->workingMachine);
				$select = "machineId,machineName";
				$listOperators[$key]->workingMachine = $this->AM->getWhereInWithWhere($select,array("isDeleted" => "0"),array("machineId" => $workingMachines),"machine"); 
			}
		}
		$data = array(
			    'listOperators'=>$listOperators,
			    'newUserId'=>$newUserId,
				'listMachines'=>$listMachines, 
    		);
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('operator_new_design', $data);
		$this->load->view('footer');
		$this->load->view('script_file/operatornewdetailslist_footer');
	}
	
	//check_new_password function use for check password and confirm password
	
	//in use
	public function 	check_new_password($pass)
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
	    if($pass != $this->input->post('operatorNewPassword')) 
        {
            $this->form_validation->set_message('check_new_password',ConfirmPasswordmustbesameasnewpassword);
            return false;  
        } 
        else 
        {
            return true;
        }   
    }
	
	//change_password function use for change password
	
    //in use
	public function change_password() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($this->input->post()) 
		{   
		    $this->form_validation->set_rules('operatorNewPassword','New password','trim|required|xss_clean');
		    $this->form_validation->set_rules('operatorConfirmPassword','Confirm new password','trim|required|xss_clean|callback_check_new_password'); 
    	    
    	    //checking required parameters validation
            if($this->form_validation->run()==false) 
            { 
                if(form_error('operatorNewPassword') ) 
                	{ 
                		echo form_error('operatorNewPassword'); 
                	}
                if(form_error('operatorConfirmPassword') ) 
                	{ 
                		echo form_error('operatorConfirmPassword'); 
                	} 
                	die;
            } 
            else 
            { 
                $userId=$this->input->post('userId');
                $update=array(
                    'userPassword'=> base64_encode($this->input->post('operatorNewPassword')),
                );
                //Update user password
                $user=$this->AM->updateuser($update, $userId);
                echo "1"; 
                die; 
            }
		    
		} 
		$this->load->view('header',array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$this->load->view('change_password'); 
		$this->load->view('footer');
	}

	//password_check function use for password validation string
	
	//in use
	public function password_check($str)
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if (preg_match('#[0-9]#', $str) && preg_match('#[a-z]#', $str) && preg_match('#[A-Z]#', $str) && preg_match('#@#', $str)) 
		{
	    	return TRUE;
	   	}
	   	$this->form_validation->set_message('password_check', Yourpasswordisnotmatchingourcriteriamakesureyouenter1capitalletter1smallletter1numberandsigninpassword);
	   return FALSE;
	}

	//update_operator_details function use for update operator details 
	
	//in use
	public function update_operator_details() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		    
	    $this->form_validation->set_rules('userName','User name','required|callback_check_user_name');
	    $this->form_validation->set_rules('userId','User id','required');
	    
	    $userId = $this->input->post('userId');
	    $operatorNewPassword = $this->input->post('operatorNewPassword');
	    $operatorConfirmPassword = $this->input->post('operatorConfirmPassword');

	    if (!empty($operatorNewPassword) || !empty($operatorConfirmPassword)) 
	    {
	        $this->form_validation->set_rules('operatorNewPassword','new password','min_length[6]|max_length[16]|callback_password_check');
		    $this->form_validation->set_rules('operatorConfirmPassword','confirm password','min_length[6]|max_length[16]|callback_check_new_password'); 
		}
	    
        if($this->form_validation->run()==false) 
        { 
	        if(form_error('userName') ) { echo form_error('userName');die; }
        	if (!empty($operatorNewPassword) || !empty($operatorConfirmPassword)) 
		    {
	            if(form_error('operatorNewPassword') ) { echo form_error('operatorNewPassword');die; }
	            if(form_error('operatorConfirmPassword') ) { echo form_error('operatorConfirmPassword');die; } 
	        }
	        else
	        {
	        	echo "Please fill valid details.";die;
	        }
        } 
        else 
        { 
			$userId = $this->input->post('userId');
            $userName = $this->input->post('userName');
            $userPassword = $this->input->post('operatorNewPassword');
            $olduserName = $this->input->post('olduserName');

            $return = "1";
            if ($userName == $olduserName)
            {
            	$return = "2";
            }
			if(!empty($_FILES['userImage']['name'])) 
            {  
     		    $nameext = explode(".", $_FILES['userImage']['name']);
	            $ext = end($nameext);
	            $config['overwrite'] = TRUE;
	            $config['upload_path'] = realpath(APPPATH . '../assets/img/user/'); 
	            $config['allowed_types'] = 'png|jpg|jpeg';
	            $config['max_size']  = '5000';
	            $config['file_name'] = $userId.".".$ext; 
				$update['userImage'] = $config['file_name'];
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);
	            if (!$this->upload->do_upload('userImage')) 
	            {
	                echo $this->upload->display_errors();  die;
	                echo "<p>Please upload valid picture.(jpg,png and jpeg  only, max 5 MB)</p>"; die;
	            }
				$return = "1";
	        }

            $update['userName'] = $userName;
            if (!empty($userPassword)) 
            {
            	$update['userPassword'] = base64_encode($userPassword);
            	$return = "1";
            }
            //Update operator details
            $this->AM->updateuser($update, $userId);
         	echo $return;die; 
        }
	}
	//check_user_name function use for check user name already exist or not
	
	//in use
	public function 	check_user_name($name) 
    {
    	if($this->AM->checkIsvalidated() == false) 
    	{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if(!preg_match("/^[a-zA-Z0-9]+$/",$name)) 
        {
            $this->form_validation->set_message('check_user_name',Usernamemaycontainonlyalphabetsnumbers);
            return false;  
        } 
        else 
        { 
            if($this->input->post('userId')) 
            {
                $userId = $this->input->post('userId');
            } else 
            {
                $userId = 0;
            } 
            //Check user name already exist or not
            if($this->AM->userExists($name, $userId)) 
            {  
                $this->form_validation->set_message('check_user_name',Usernamemustbeunique);
                return false;  
            } else 
            {
                return true;
            }
        }   
    }

    //check_new_password_operator function use for confirm password same as password
    
    //in use
    public function check_new_password_operator($pass) 
    {
    	if($this->AM->checkIsvalidated() == false) 
    	{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		if($pass != $this->input->post('userPassword')) 
        {
            $this->form_validation->set_message('check_new_password_operator',ConfirmPasswordmustbesameasnewpassword);
            return false;  
        }
        else 
        {
            return true;
        }   
    }
	
	//add_new_operator function use for added new operator
	
    //in use
	public function add_new_operator() 
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$this->form_validation->set_rules('userName','user Name','required|callback_check_user_name');
		$this->form_validation->set_rules('userPassword','new password','required|min_length[6]|max_length[16]|callback_password_check');
		$this->form_validation->set_rules('userConfirmpassword','confirm password','required|min_length[6]|max_length[16]|callback_check_new_password_operator'); 

		//checking required parameters validation
		if($this->form_validation->run()==false) 
        { 
            if(form_error('userName') ) { echo form_error('userName');die; } 
	        if(form_error('userPassword') ) { echo form_error('userPassword');die; }
            if(form_error('userConfirmpassword') ) { echo form_error('userConfirmpassword');die; } 
		} 
        else 
        { 
        	$machineArr = $this->input->post('userMachine'); 
   			//if(empty($machineArr)) 
			// {
			//     echo "Please select assign machine";die;
			// }
			// else 
			// {
	            $newUserId = $this->AM->lastUserId()->maxId + 1;
	            $nameext = explode(".", $_FILES['userImage']['name']); 
	            $ext = end($nameext);
	            $config['upload_path'] = realpath(APPPATH . '../assets/img/user/');
	            $config['allowed_types'] = 'jpg|jpeg|png';
	            $config['max_size']  = '5000';
	            $config['file_name'] = $newUserId.".".$ext; 
	            
	            $this->load->library('upload', $config);
	            $this->upload->initialize($config);
	            
	            if (!$this->upload->do_upload('userImage')) 
	            {
	            	$userImage = "default.jpg";
	            }
	            else 
	            {
	            	$userImage = $config['file_name'];
	            }
			
				// $machines = implode(",",$machineArr); 
				$insert=array(

	                'userName'=>$this->input->post('userName'),
	                'userPassword'=> base64_encode($this->input->post('userPassword')), 
	                'userImage'=> $userImage, 
					'factoryId'=>$this->session->userdata('factoryId')
	                // 'machines'=>$machines
	            );

				//Add new operator
	            $this->AM->newuser($insert);

				echo "1"; 
				die;
			// }
        }
	}

	//delete_user function use for delete operator
	
	//in use
	public function delete_user() 
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		$userId = $this->input->post('deleteuserId'); 
		//Delete operator 
		$this->AM->deleteuser($userId); 
		//Update user logout flag
		$this->AM->userLogOutOpApp($userId);

		$result = $this->AM->getWhereDB(array("userId" => $userId),"user");
		$fcmToken = array_column($result, "deviceToken");
		$message = "Operator has been deleted. Please signin with valid details.";
		$title = "Operator delete";
		$type = "operatorDelete";
		//Send notification to logout operator in opApp
		$this->AM->sendNotification($message,$title,$fcmToken,$type);
		echo "1"; die;
	}

	//operator_checkin_checkout function use for operator checkin checkout page
	
	//in use
	public function operator_checkin_checkout()
	{
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}

		

		

		$data['users'] = $this->AM->getWhereDB(array("factoryId" => $this->factoryId,"isDeleted" => "0","userRole" => "0"),"user"); 
		$data['machines'] = $this->AM->getallMachines($this->factory)->result_array();  
		$this->load->view('header',
			array('roleName'=>$this->AM->checkUserRoleName(),'factoryData'=>$this->AM->getFactoryData($this->session->userdata('factoryId')), 'listFactories'=>$this->listFactories));
		$data['userRole'] = !empty($this->input->post('userRole')) ? $this->input->post('userRole') : "0";
		$this->load->view('operator_checkin_checkout', $data);
		$this->load->view('footer');
		$this->load->view('script_file/operator_checkin_checkout_footer',$data);
	}

	//operator_checkin_checkout_pagination function use for checkin checkout data
	
	//in use
	public function operator_checkin_checkout_pagination() 
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$draw = $_POST['draw'];
		$row = $_POST['start'];
		$rowperpage = $_POST['length'];
		
		$userName = $this->input->post('userName');
		$machineIds = $this->input->post('machineIds');
		$startDateValS = $this->input->post('startDateValS');
		$startDateValE = $this->input->post('startDateValE');
		$endDateValS = $this->input->post('endDateValS');
		$endDateValE = $this->input->post('endDateValE');
		$userRole = $this->input->post('userRole');

		//Get all data count
		$listDataLogsCount = $this->AM->get_operator_checkin_checkout_count($userRole)->num_rows();
		//Get count by selected filter
		$listDataLogsSearchCount = $this->AM->get_search_operator_checkin_checkout_count($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE,$userRole)->num_rows();
		//Get all data
		$listDataLogs = $this->AM->get_search_operator_checkin_checkout_logs($userName,$machineIds,$startDateValS,$startDateValE,$endDateValS,$endDateValE, $row, $rowperpage,$userRole)->result_array(); 

		foreach ($listDataLogs as $key => $value) 
		{
			if (!empty($listDataLogs[$key]['workingMachine'])) 
			{
				$workingMachines = explode(",", $listDataLogs[$key]['workingMachine']);
				$select = "machineName";
				$result = $this->AM->getWhereInWithWhere($select,array("isDeleted" => "0"),array("machineId" => $workingMachines),"machine"); 
				$listDataLogs[$key]['workingMachineName'] = str_replace(",", "<br>", implode(",", array_column($result, "machineName")));
			}
			else
			{
				$listDataLogs[$key]['workingMachineName'] = "";
			}

			$listDataLogs[$key]['userName'] = '<img style="border-radius: 50%;    margin-top: -5px;" width="16" height="16" src="'. base_url('assets/img/user/'.$listDataLogs[$key]['userImage']) .'"> &nbsp;'.$listDataLogs[$key]['userName'];
			if ($listDataLogs[$key]['isActive'] == "0") 
			{
				$where = array("createdDate >=" => $listDataLogs[$key]['startTime'],"createdDate <=" => $listDataLogs[$key]['endTime'],"userId" => $listDataLogs[$key]['userId']);
			}
			else
			{
				$where = array("createdDate >=" => $listDataLogs[$key]['startTime'],"userId" => $listDataLogs[$key]['userId']);
			}
			$listDataLogs[$key]['reportCount'] = $this->AM->getWhereCount($where,"reportProblem");
			$listDataLogs[$key]['endTime'] = ($listDataLogs[$key]['isActive'] == "0") ? $listDataLogs[$key]['endTime'] : "";	
 		}
 		
		$response = array(
		  "draw" => intval($draw),
		  "iTotalRecords" => $listDataLogsCount, 
		  "iTotalDisplayRecords" => $listDataLogsSearchCount, 
		  "aaData" => $listDataLogs
		);

		echo json_encode($response); die;
	}

	//getOperatorById function use for get operator and check in,check out time details by active id
	function getOperatorById()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$activeId = $this->input->post('activeId');
		$result = $this->AM->getallOperatorsById($this->factory,$activeId);
		if (!empty($result)) 
		{
			$startDate = $result->startTime;
			$endDate = $result->endTime;
			$result->startDate = date("Y-m-d", strtotime($result->startTime));
			$result->startTime = date("H:i:s", strtotime($result->startTime));
			if ($result->isActive == "0") 
			{
				//Get report count between start and end date
				$result->reportCount = $this->AM->getWhereCount(array("createdDate >=" => $startDate,"createdDate <=" => $endDate,"userId" => $result->userId),"reportProblem");
				$result->endDate = date("Y-m-d", strtotime($result->endTime));
				$result->endTime = date("H:i:s", strtotime($result->endTime));
			}else
			{
				//Get report count as start date to till now
				$result->reportCount = $this->AM->getWhereCount(array("createdDate >=" => $startDate,"userId" => $result->userId),"reportProblem");
				$result->endDate = "---";
				$result->endTime = "---";
			}

			if (!empty($result->workingMachine)) 
			{
				$workingMachines = explode(",", $result->workingMachine);
				$select = "machineName";
				$resultWorkingMachine = $this->AM->getWhereInWithWhere($select,array("isDeleted" => "0"),array("machineId" => $workingMachines),"machine");
				foreach ($resultWorkingMachine as $key => $value) {
					$resultWorkingMachineName[$key]['machineName'] = '<button type="button" style="margin-bottom: 5px;border-radius: 18px;background: #002060 !important;border-color: #002060;padding: 3px 12px;height: 28px;margin-right: 5px;" class="blueButton btn btn-primary">'. $value['machineName'] .'</button>';
				}
				$result->workingMachineName = implode(" ", array_column($resultWorkingMachineName, "machineName"));
			}
			else
			{
				$result->workingMachineName = "";
			}
		}
		echo json_encode($result); die;
	}

	//getOperatorDetailById function use for get operator details by operator id
	function getOperatorDetailById()
	{
		if($this->AM->checkIsvalidated() == false) {
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		$userId = $this->input->post('userId');
		$result = $this->AM->getWhereDBSingle(array("userId" => $userId),"user");
		echo json_encode($result);
	}
	
	//assign_machine function use for assign new machine 
	
	//in use
	public function assign_machine() 
	{	
		if($this->AM->checkIsvalidated() == false) 
		{
            $this->session->set_flashdata('error_message', Signinfirsttoaccesswebadmin); 
		    redirect('admin/login');
		}
		
		$userId = $this->input->post('userId');
		$machineArr = $this->input->post('userMachine'); 
		if(!empty($machineArr)) 
		{
		    $machines = implode(",",$machineArr); 
		
	        $update=array(
	            'machines'=>$machines
	        ); 
	        //Update new assign machine
			$this->AM->updateuser($update, $userId);
			echo Machinesassignedsuccessfully; die;
		} 
		else 
		{
			$update=array(
	            'machines'=> ""
	        ); 
		    $this->AM->updateuser($update, $userId);
			echo Machinesassignedsuccessfully; die;
		}
	}

}

?>