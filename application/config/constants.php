<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
|--------------------------------------------------------------------------
| Display Debug backtrace
|--------------------------------------------------------------------------
|
| If set to TRUE, a backtrace will be displayed along with php errors. If
| error_reporting is disabled, the backtrace will not display, regardless
| of this setting
|
*/
defined('SHOW_DEBUG_BACKTRACE') OR define('SHOW_DEBUG_BACKTRACE', TRUE);

/*
|--------------------------------------------------------------------------
| File and Directory Modes
|--------------------------------------------------------------------------
|
| These prefs are used when checking and setting modes when working
| with the file system.  The defaults are fine on servers with proper
| security, but you may wish (or even need) to change the values in
| certain environments (Apache running a separate process for each
| user, PHP under CGI with Apache suEXEC, etc.).  Octal values should
| always be used to set the mode correctly.
|
*/
defined('FILE_READ_MODE')  OR define('FILE_READ_MODE', 0644);
defined('FILE_WRITE_MODE') OR define('FILE_WRITE_MODE', 0666);
defined('DIR_READ_MODE')   OR define('DIR_READ_MODE', 0755);
defined('DIR_WRITE_MODE')  OR define('DIR_WRITE_MODE', 0755);

/*
|--------------------------------------------------------------------------
| File Stream Modes
|--------------------------------------------------------------------------
|
| These modes are used when working with fopen()/popen()
|
*/
defined('FOPEN_READ')                           OR define('FOPEN_READ', 'rb');
defined('FOPEN_READ_WRITE')                     OR define('FOPEN_READ_WRITE', 'r+b');
defined('FOPEN_WRITE_CREATE_DESTRUCTIVE')       OR define('FOPEN_WRITE_CREATE_DESTRUCTIVE', 'wb'); // truncates existing file data, use with care
defined('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE')  OR define('FOPEN_READ_WRITE_CREATE_DESTRUCTIVE', 'w+b'); // truncates existing file data, use with care
defined('FOPEN_WRITE_CREATE')                   OR define('FOPEN_WRITE_CREATE', 'ab');
defined('FOPEN_READ_WRITE_CREATE')              OR define('FOPEN_READ_WRITE_CREATE', 'a+b');
defined('FOPEN_WRITE_CREATE_STRICT')            OR define('FOPEN_WRITE_CREATE_STRICT', 'xb');
defined('FOPEN_READ_WRITE_CREATE_STRICT')       OR define('FOPEN_READ_WRITE_CREATE_STRICT', 'x+b');

/*
|--------------------------------------------------------------------------
| Exit Status Codes
|--------------------------------------------------------------------------
|
| Used to indicate the conditions under which the script is exit()ing.
| While there is no universal standard for error codes, there are some
| broad conventions.  Three such conventions are mentioned below, for
| those who wish to make use of them.  The CodeIgniter defaults were
| chosen for the least overlap with these conventions, while still
| leaving room for others to be defined in future versions and user
| applications.
|
| The three main conventions used for determining exit status codes
| are as follows:
|
|    Standard C/C++ Library (stdlibc):
|       http://www.gnu.org/software/libc/manual/html_node/Exit-Status.html
|       (This link also contains other GNU-specific conventions)
|    BSD sysexits.h:
|       http://www.gsp.com/cgi-bin/man.cgi?section=3&topic=sysexits
|    Bash scripting:
|       http://tldp.org/LDP/abs/html/exitcodes.html
|
*/
defined('EXIT_SUCCESS')        OR define('EXIT_SUCCESS', 0); // no errors
defined('EXIT_ERROR')          OR define('EXIT_ERROR', 1); // generic error
defined('EXIT_CONFIG')         OR define('EXIT_CONFIG', 3); // configuration error
defined('EXIT_UNKNOWN_FILE')   OR define('EXIT_UNKNOWN_FILE', 4); // file not found
defined('EXIT_UNKNOWN_CLASS')  OR define('EXIT_UNKNOWN_CLASS', 5); // unknown class
defined('EXIT_UNKNOWN_METHOD') OR define('EXIT_UNKNOWN_METHOD', 6); // unknown class member
defined('EXIT_USER_INPUT')     OR define('EXIT_USER_INPUT', 7); // invalid user input
defined('EXIT_DATABASE')       OR define('EXIT_DATABASE', 8); // database error
defined('EXIT__AUTO_MIN')      OR define('EXIT__AUTO_MIN', 9); // lowest automatically-assigned error code
defined('EXIT__AUTO_MAX')      OR define('EXIT__AUTO_MAX', 125); // highest automatically-assigned error code

define('NYTT_EMAIL', 'info@nytt-tech.com'); //arshikweb@gmail.com 
define('API_ACCESS_KEY', 'AAAA88yPT8I:APA91bG8nnbrTNSeWFZ54QHgScvzpO5jbAcU9A3b13aHTxLvD1xSCAHX1SpVt-ulnVe5CMcEj19Te18kfZc7knSjf7rdAO8QIeS_TX8GZxj01BdXeh-j5ziaJlJXp1e7rpC98Fh60yiJ');   

define('INVALID_DETAILS', 'Invalid details.');
define('EMPTY_BENIFIT', 'No benefits listed.');
define('EMPTY_FEATURE', 'No features listed.');
define('EMPTY_WORKSTEP', 'No steps listed.');
define('ABOUT_MESSAGE', 'About fetched successfully.');
define('EMPTY_ABOUT', 'No about text available.');
define('HELP_MESSAGE', 'Help fetched successfully.');
define('EMPTY_HELP', 'No help text available.');
define('PROBLEM_REPORT_SUCCESS', 'Problem reported successfully.');
define('PROBLEM_REPORT_ERROR', 'Error occured while reporting the problem.');
define('SIGNIN_SUCCESS', 'Logged in successfully.');
define('SIGNIN_ALREADY', 'User already logged in.');
define('SIGNIN_ERROR', 'Invalid login details.');
define('SIGNOUT_SUCCESS', 'Logged out successfully.');
define('FORGOT_EMAIL_SUCCESS', 'Congragulations! email sent successfully.');
define('FORGOT_EMAIL_ERROR', 'You have encountered an error.');
define('NO_DETECTION', 'No detection. Check your phone or contact : info@nytt-tech.com');
define('SETAPP_NOT_STARTED', 'SetApp not started. Check your phone or contact : info@nytt-tech.com');
define('SETAPP_TURNED_OFF', 'Setapp is turned off, please start the detection or check the setapp phone');
define('SETAPP_NO_INTERNET', 'No internet or phone turned off. Check your phone or contact : info@nytt-tech.com');
define('NO_ASSIGN', 'No machines assigned');
define('MACHINE_LIST_SUCCESS', 'Machines listed successfully.');
define('MACHINE_SELECT_SUCCESS', 'Machine selected successfully.');
define('MACHINE_LEFT_SUCCESS', 'Machine left successfully.');
define('MACHINE_LEFT_ERROR', 'Machine already left.');
define('LOG_SUCCESS', 'Logged successfully.');
define('NOTIFICATION_UPDATE_STATUS_SUCCESS', 'Notification setting updated.');
define('UPDATE_PROFILE_SUCCESS', 'Profile updated successfully.');
define('USERNAME_ERROR', 'Username must be unique.');
define('USER_ACCESS_SUCCESS', 'User details fetched successfully.');

define('CHECKIN_SUCCESS', 'Checked in successfully.');
define('CHECKOUT_SUCCESS', 'Checked out successfully.');
define('CHECKOUT_ERROR', 'Checked out already.');

define('MNOTIFICATION_RESPOND_SUCCESS', 'Notification responded successfully.');
define('MNOTIFICATION_RESPOND_ERROR', 'Notification responded already.');

define('NOTIFICATION_RESPOND_SUCCESS', 'Notification responded successfully.');
define('NOTIFICATION_RESPOND_ERROR', 'Notification responded already.');
define('NOTIFICATION_MULTI_RESPOND_SUCCESS', 'Notifications responded successfully.');

define('NOTIFICATION_LIST_SUCCESS', 'Notifications fetched successfully.');
define('NOTIFICATION_LIST_EMPTY', 'No pending notifications.');

define('NOTIFICATION_RESPONDED_LIST_SUCCESS', 'Notifications fetched successfully.');
define('NOTIFICATION_RESPONDED_LIST_EMPTY', 'No responded notifications.');


define('smtp_host', 'ssl://smtp.sendgrid.net');
define('smtp_port', 467);
define('smtp_user', 'apikey');
define('smtp_pass', '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');

define('emailSubject', '');



define('DETECTION_FILES', '/prodapp/detectionfiles/');
define('APP_VERSIONS_FILES', '/prodapp/app_versions');
define('ENV_URL', 'https://nyttcloud.host/prodapp/');
define('SETAPP_SERVER_KEY', 'AAAASbM1Rus:APA91bFh-5r58sSNypzlux2b3hA1uF1Y51ATYYJBmmArGOYRGzQI_DUXgCqk1qcehqSSsoaQceL4rw6q8URV4Nrq0WLNMWds0CfAoc_ORAFU6QwQo70zIpI97SJbr-CufG7WPlSbIo-e');
