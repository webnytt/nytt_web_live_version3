<style>
  .card1:nth-child(1) svg circle:nth-child(2)
  {
    stroke-dashoffset:<?php echo 440 - (630 * 75) / 100; ?>;
      stroke-linecap: round;
      stroke: #FF8000;
  }
  
</style>
<link href="<?php echo base_url('assets/css/virtual_machine_graph.css');?>" rel="stylesheet" />
<div id="content" class="content">
  <h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Virtualmachinegraph; ?></h1>

  <div class="row" >
    <div class="col-md-1" style="margin-top: 8px;left:3%!important;">
      <span style=""><?php echo Showby; ?> :</span>
    </div>
    <div class="col-md-2" >
      <select  class="form-control" id="choose1" onchange="changeFilterType(this.value);">
          <option value="day"><?php echo Day; ?></option>
          <option value="weekly"><?php echo week; ?></option>
          <option value="monthly"><?php echo month; ?></option>
          <option value="yearly"><?php echo year; ?></option>
          <option value="custom"><?php echo Custom; ?></option>
      </select>
    </div>
    <div class="col-md-1" style="margin-top: 8px;left:5%!important;">
        <span style=""> For :</span>
    </div> 
    <div class="col-md-4 custom" style="display: none;">
        <input type="text" class="form-control" id="advance-daterange" style="width:100%;" onchange="changeFilterValue()" value="<?php echo date('m/d/Y'); ?>">
    </div>
    <div class="col-md-4 day">
        <input type="text" class="form-control" id="choose2" style="width:100%;float: none;" onchange="changeFilterValue()" value="<?php echo date('m/d/Y'); ?>">
    </div>
    <div class="col-md-4 monthly" style="display: none;">
      <input type="text" class="form-control" id="choose4" style="width:100%;float: none;" onchange="changeFilterValue()" value="">
    </div>

    <div class="col-md-2 weekly" style="display: none;">
      <select class="form-control" id="choose3" onchange="changeFilterValue()" style="width: 80% !important;">
          <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1"><?php echo Week1;?> </option>
          <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2"><?php echo Week2;?> </option>
          <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3"><?php echo Week3;?> </option>
          <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4"><?php echo Week4;?> </option>
          <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5"><?php echo Week5;?> </option>
          <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6"><?php echo Week6;?> </option>
          <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7"><?php echo Week7;?> </option>
          <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8"><?php echo Week8;?> </option>
          <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9"><?php echo Week9;?> </option>
          <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10"><?php echo Week10; ?> </option>
          <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11"><?php echo Week11; ?> </option>
          <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12"><?php echo Week12; ?> </option>
          <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13"><?php echo Week13; ?> </option>
          <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14"><?php echo Week14; ?> </option>
          <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15"><?php echo Week15; ?> </option>
          <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16"><?php echo Week16; ?> </option>
          <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17"><?php echo Week17; ?> </option>
          <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18"><?php echo Week18; ?> </option>
          <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19"><?php echo Week19; ?> </option>
          <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20"><?php echo Week20; ?> </option>
          <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21"><?php echo Week21; ?> </option>
          <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22"><?php echo Week22; ?> </option>
          <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23"><?php echo Week23; ?> </option>
          <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24"><?php echo Week24; ?> </option>
          <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25"><?php echo Week25; ?> </option>
          <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26"><?php echo Week26; ?> </option>
          <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27"><?php echo Week27; ?> </option>
          <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28"><?php echo Week28; ?> </option>
          <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29"><?php echo Week29; ?> </option>
          <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30"><?php echo Week30; ?> </option>
          <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31"><?php echo Week31; ?> </option>
          <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32"><?php echo Week32; ?> </option>
          <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33"><?php echo Week33; ?> </option>
          <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34"><?php echo Week34; ?> </option>
          <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35"><?php echo Week35; ?> </option>
          <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36"><?php echo Week36; ?> </option>
          <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37"><?php echo Week37; ?> </option>
          <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38"><?php echo Week38; ?> </option>
          <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39"><?php echo Week39; ?> </option>
          <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40"><?php echo Week40; ?> </option>
          <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41"><?php echo Week41; ?> </option>
          <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42"><?php echo Week42; ?> </option>
          <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43"><?php echo Week43; ?> </option>
          <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44"><?php echo Week44; ?> </option>
          <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45"><?php echo Week45; ?> </option>
          <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46"><?php echo Week46; ?> </option>
          <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47"><?php echo Week47; ?> </option>
          <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48"><?php echo Week48; ?> </option>
          <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49"><?php echo Week49; ?> </option>
          <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50"><?php echo Week50; ?> </option>
          <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51"><?php echo Week51; ?> </option>
          <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52"><?php echo Week52; ?> </option>
      </select>
    </div>

     <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
    <div class="col-md-2 weekly" style="display: none;">
        <select class="form-control"  id="choose5" onchange="changeFilterValue()" style="width: 80% !important;">
            <?php for($i=0 ; $i < $years; $i++){ 
                  $yearValue = 2018 + $i;
                  ?>
                  <option <?php if ($yearValue == date('Y')) {
                      echo "selected";
                  } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
              <?php } ?>
        </select>
    </div>
    <div class="col-md-4 yearly" style="display: none;">
        <select class="form-control"  id="choose6" onchange="changeFilterValue()">
            <?php for($i=0 ; $i < $years; $i++){ 
                  $yearValue = 2018 + $i;
                  ?>
                  <option <?php if ($yearValue == date('Y')) {
                      echo "selected";
                  } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
              <?php } ?>
        </select>
    </div>
    <div class="col-md-1" style="margin-top: 8px;left:5%;">
        <span style=""> <?php echo View; ?> :</span>
    </div> 
    <div class="col-md-1">
        <input type="text" class="form-control" id="choose7" onchange="changeFilterValue()" style="width:100%;">
    </div>
    <div class="col-md-1 colmd1Stylee">
        <span style=""> - </span>
    </div> 
    <div class="col-md-1">
        <input type="text" class="form-control" id="choose8" onchange="changeFilterValue()" style="width:100%;">
    </div>
  </div>
  <div class="graph-loader hide" ></div>
    <div class="row" style="margin-top: 13px;">
      <div class="col-md-12 colmd12Stylee">
        <input type="hidden" name="" id="IOName" value="<?php if(!empty($_POST['IOName'])) { echo $_POST['IOName']; }else { echo $virtualMachine[0]['IOName']; } ?>">
        <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false">
        <div class="carousel-inner no-padding" role="listbox">
          <div class="carousel-item active">
            <!-- <div class="col-xs-3 col-sm-3 col-md-3 active showAll" style="padding-left: 0px !important;margin-left: -1px;">
                <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="IOName0"> Show all  </a>
            </div> -->
            <?php 
            $countQuery = 1;foreach($virtualMachine as $key => $value) { ?>
              <?php if($countQuery % 4 == 0){ ?>
              </div>
              <div class="carousel-item">
            <?php } ?>
            <div class="col-xs-3 col-sm-3 col-md-3 " style="padding-left: 0px !important;margin-left: 1px;">
              <a href="javascript:;" class="btn btn-default btn-lg filter <?php if($_POST['IOName'] == $value['IOName']) { echo "active"; } ?> <?php if($key == 0 && empty($_POST['IOName'])) { echo "active"; } ?>" id="IOName<?php echo $value['IOName']; ?>"> <?php
                                if ($value['IOName'] == "GPIO15") {
                                  echo "Parts produced";
                                }else if ($value['IOName'] == "GPIO14") {
                                  echo "Robot status";
                                }else{
                                  echo $value['IOName']; 
                                } ?></a>
            </div>
            <?php $countQuery++;  } ?>
          </div>
          </div>
          <?php $count = count($virtualMachine);
          if ($count > 3) { ?>
          <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon carouselPrevIconStyle">
                <i style="font-size: 20px;" class="fa fa-angle-left" aria-hidden="true">
                </i>
              </span>
          </a>
          <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
              <span class="carousel-control-next-icon carouselNexttIconStyle">
                <i style="font-size: 20px;" class="fa fa-angle-right" aria-hidden="true">
                </i>
              </span>
          </a>
          <?php } ?>
        </div>
    </div> 
</div>
<div class="row" style="margin-top: 15px;">
  <div class="col-md-12" style="max-height: 390px !important;">
    <div class="tab-content boxShadow" style="min-height: 335px!important;" >
      <div class="tab-pane fade active show">
        <div class="row">
          <div class="col-md-6" >
            <h4 style="color: #002060;" class="textChange"><?php echo Partsproduced; ?></h4>
              <center style="margin-bottom: -30px;margin-top: 40px;">
                <div class="col">
                  <div class="card1" >
                    <center>
                      <svg>
                        <circle style="stroke:#FFFFFF"cx="100" cy="100" r="100"></circle>
                        <circle cx="100" cy="100" r="100"></circle>
                        <circle  style="stroke:#CCD2DF;" cx="100" cy="100" r="91"></circle>
                        <div class="overallStatisticscircle">
                          <h3 style="margin-bottom:0.0rem;color:#002060;" id="partCount">0</h3>
                          <h4 style="color:#002060;" class="textChange"><?php echo Partsproduced; ?></h4>
                        </div>
                      </svg>
                    </center>
                  </div>
                </div>
              </center>
            </div>
            <div class="col-md-6" >
                <center class="secondgraphmobilestyle" style="margin-bottom: -30px;margin-top: 10px;">
                    <div id="DailyGraph4" style="padding-right:20px;height:330px;"></div>
                </center>
            </div>
        </div>
    </div>
</div>
</div>
</div>
<div class="">
  <hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
  





