<script src="https://cdnjs.cloudflare.com/ajax/libs/slick-carousel/1.8.1/slick.min.js" integrity="sha512-XtmMtDEcNz2j7ekrtHvOVR4iwwaD6o/FUJe6+Zq+HgcCsk3kj4uSQQR8weQ2QVj1o0Pk6PwYLohm206ZzNfubg==" crossorigin="anonymous"></script>
<style type="text/css">
    .card2:nth-child(1) svg circle:nth-child(2)
    {
        stroke-dashoffset:<?php echo 440 - (440 * 100) / 100; ?>;
        stroke-linecap: round;
        stroke: #002060;
    }
</style>

     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script> 
        <link href="<?php echo base_url('assets/css/breakdown3.css');?>" rel="stylesheet" /> 
        <div id="content" class="content">
           
            <h1 class="page-header H1pageTitle">Analytics</h1>

            <br />
            <div class="row">
                <input type="hidden" id="analytics" value="stopAnalytics">
                <div class="col-md-3" style="padding-left: 0px;">
                   <small class="SmallStyle">
                      <a class="Alinkstyle" href="<?php echo base_url('Analytics/dashboard5'); ?>"> Analytics </a> &gt; Stop analytics
                    </small>
                </div>
                <div class="col-md-1 show_by Colmd1ShowBystyle">
                    <label>Show by :</label>
                </div>
                <div class="col-md-2 Colmd2style">
                    <select disabled="" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                        <option class="new_test" value="day">Day</option>
                    </select>
                </div>
                <div class="col-md-1 day show_by">
                    <label class="LabelForStyle"> For:&nbsp;</label>
                </div> 
                <div class="col-md-2 day colmd2Stylee">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%;" value="<?php echo date('m/d/Y'); ?>">
                </div>
                <div class="col-md-2 weekly" style="display: none;">
                    <select class="form-control" id="choose3" onchange="changeFilterValue()">
                        <option value="1">Week 1</option>
                        <option value="2">Week 2</option>
                        <option value="3">Week 3</option>
                        <option value="4">Week 4</option>
                        <option value="5">Week 5</option>
                        <option value="6">Week 6</option>
                        <option value="7">Week 7</option>
                        <option value="8">Week 8</option>
                        <option value="9">Week 9</option>
                        <option value="10">Week 10</option>
                        <option value="11">Week 11</option>
                        <option value="12">Week 12</option>
                        <option value="13">Week 13</option>
                        <option value="14">Week 14</option>
                        <option value="15">Week 15</option>
                        <option value="16">Week 16</option>
                        <option value="17">Week 17</option>
                        <option value="18">Week 18</option>
                        <option value="19">Week 19</option>
                        <option value="20" selected="">Week 20</option>
                        <option value="21">Week 21</option>
                        <option value="22">Week 22</option>
                        <option value="23">Week 23</option>
                        <option value="24">Week 24</option>
                        <option value="25">Week 25</option>
                        <option value="26">Week 26</option>
                        <option value="27">Week 27</option>
                        <option value="28">Week 28</option>
                        <option value="29">Week 29</option>
                        <option value="30">Week 30</option>
                        <option value="31">Week 31</option>
                        <option value="32">Week 32</option>
                        <option value="33">Week 33</option>
                        <option value="34">Week 34</option>
                        <option value="35">Week 35</option>
                        <option value="36">Week 36</option>
                        <option value="37">Week 37</option>
                        <option value="38">Week 38</option>
                        <option value="39">Week 39</option>
                        <option value="40">Week 40</option>
                        <option value="41">Week 41</option>
                        <option value="42">Week 42</option>
                        <option value="43">Week 43</option>
                        <option value="44">Week 44</option>
                        <option value="45">Week 45</option>
                        <option value="46">Week 46</option>
                        <option value="47">Week 47</option>
                        <option value="48">Week 48</option>
                        <option value="49">Week 49</option>
                        <option value="50">Week 50</option>
                        <option value="51">Week 51</option>
                        <option value="52">Week 52</option>
                    </select>
                </div>
                <div class="col-md-1 weekly" style="display: none;">
                </div>
                <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
                <div class="col-md-1 monthly show_by colmd1showbystyle">
                    <label> for </label>
                </div>
                <div class="col-md-2 monthly colmd2Monthly">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker1" id="choose4">
                </div>
                <div class="col-md-2 yearly" style="display: none;">
                    <select class="form-control"  id="choose5" onchange="changeFilterValue()">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option selected="" value="2020">2020</option>
                    </select>
                </div>
                <div class="col-md-1 yearly" style="display: none;"></div>
            </div>

            <div class="row" style="margin-top: 13px;">
                    <?php $listMachinesStop =  $listMachines->result(); ?>
                    <input type="hidden" name="" id="machineIdStop" value="<?php echo $listMachinesStop[0]->machineId; ?>">
                        <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false" style="width: 100%;">
                        <div class="carousel-inner no-padding">
                            <div class="carousel-item active">
                            <?php 
                                $countQuery = 1;
                                $i = 1;
                                foreach($listMachinesStop as $machine) { ?>
                                    <?php if($countQuery % 4 == 0){ 
                                               ?>
                                        </div>
                                        <div class="carousel-item">
                                      <?php   } ?>
                                    <div style="margin-left: -7px;margin-right: 3px;" class="col-xs-3 col-sm-3 col-md-3 <?php if($i == 1){ echo "active"; } ?>">
                                        <a href="javascript:;" class="btn btn-default btn-lg filter1 <?php if($i == 1){ echo "active"; } ?> machineNameText" id="machineStop<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                                    </div>
                                <?php $i++; if ($i != 4) {
                                   $countQuery++;
                                }   } ?>
                            </div>
                        </div>
                        <?php if ($i > 4) { ?>
                        <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon carouselcontrollerstyle">
                                <i class="fa fa-angle-left carouselIconStyle" aria-hidden="true"></i>
                            </span>
                        </a>
                        <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
                            <span class="carousel-control-next-icon carouselcontrollerstyle">
                                <i class="fa fa-angle-right carouselIconStyle" aria-hidden="true"></i>
                            </span>
                        </a>
                    <?php } ?>
                    </div>
            </div>
        <div class="graph-loader hide" ></div>
        <div class="row graphloaderrowstyle">
        <input type="hidden" min="0" class="inputText" id="duration1" name="" value="0">
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <div class="card-body">
                <h4 class="cardcard2h4style">0 mins - <input type="text" min="0" class="inputText" id="duration2" name="" value="5">  mins</h4>
                <div class="widget widget-stats widget1" id="filter1widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70"></circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter1count">0 stops</h3>
                                <small class="secondText" id="filter1">0 mins</small><br>
                                <small class="thirdText">Total time</small>
                            </div>
                        </svg>
                    </center>
                </div>
              </div>
            </div>
        </div>
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <div class="card-body">
                    <h4 class="cardcard2h4style">
                        <span class="duration2Text">5 mins</span> - <input type="text" min="0" class="inputText" id="duration3" name="" value="10">  mins</h4>
                    <div class="widget widget-stats widget1" id="filter2widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70">
                            </circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter2count">0 stops</h3>
                                <small class="secondText" id="filter2">0 mins</small><br>
                                <small class="thirdText">Total time</small>
                            </div>
                        </svg>
                    </center>
                  </div>
                </div>
            </div>
        </div>

        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <div class="card-body">
                    <h4 class="cardcard2h4style"><span class=
                        "duration3Text">10 mins</span> - <input type="text" min="0" class="inputText" id="duration4" name="" value="15">  mins</h4>
                    <div class="widget widget-stats widget1" id="filter3widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter3count">0 stops</h3>
                                    <small class="secondText" id="filter3">0 mins</small><br>
                                    <small class="thirdText">Total time</small>
                                </div>
                            </svg>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <div class="card-body">
                   <h4 class="cardcard2h4style"><span class=
                        "duration4Text">15 mins</span> - <img style="margin-top: -5px;" src="<?php echo base_url('assets/img/infiniti.png'); ?>"></h4>
                    <div class="widget widget-stats widget1" id="filter4widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter4count">0 stops</h3>
                                    <small class="secondText" id="filter4">0 mins</small><br>
                                    <small class="thirdText">Total time</small>
                                </div>
                            </svg>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content boxShadow row tabcontentstyle">
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle">Receive error notification: <span id="errorNotification"></span></h4>
            </div>
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle">Provide reason for errors : <span id="errorReasonNotification"></span></h4>
            </div>
        </div>
        <div class="row m-r-10">
            <div class="col-md-6">
                <div class="tab-content boxShadow">
                <div class="tabcontentMaindivStyle">
                    <button id="filter1widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">0 mins - 
                        <span class="duration2Text">5 mins</span>
                    

                    <button id="filter2widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration2Text">5 mins</span> - <span class="duration3Text">10 mins</span>
                    

                    <button id="filter3widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration3Text">10 mins</span> - <span class="duration4Text">15 mins</span>
                    

                    <button id="filter4widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration4Text">15 mins</span> - <img src="<?php echo base_url('assets/img/white_infinity.svg'); ?>">
                    

                </div>

                <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime1" style="height:350px;" ></div> 
                                <div class="TimeStampGraphStyle">TIMESTAMP(HH:MM:SS)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tab-content boxShadow" >
                    <div class="tabcontentMaindivStyle">
                        &nbsp;
                    </div>
                    <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime2" style="height:368px;" ></div> 
                                <div class="ReasonDurationGraphStyle">REASON DURATION(MIN)</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            <hr style="background: gray;">
            <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>