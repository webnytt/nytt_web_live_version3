	<?php ?>
		<div id="content" class="content">

			

			<h1 class="page-header"><?php echo Reasonsforred; ?></h1>
			<?php  ?> 
			<?php  ?>
				<input type="hidden" name="machineId" id="machineId" value="<?php if($_POST) { echo $_POST['machineId']; } else { echo $listMachines->result()[0]->machineId; } ?>" /> 
				<div id="options" class="m-b-10">
					<span class="gallery-option-set text-center" id="filter" data-option-key="filter">
					<?php foreach($listMachines->result() as $key1234 => $machine) { ?>
						<a href="javascript:;" id="machine<?php echo $machine->machineId; ?>" class="btn btn-primary btn-sm  <?php if($_POST) { if($machine->machineId == $_POST['machineId']) echo "active"; }else { if($key1234 == 0) { echo "active"; } } ?>" ><?php echo $machine->machineName; ?></a>
					<?php } ?>
					</span>
				</div>
					<?php 
					if($_POST && isset($_POST['dateValS'])) { 
						$dateValE = date("F d, Y", strtotime($_POST['dateValE']));
						$dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
					} else { 
						$dateValE =  date("F d, Y", strtotime("-29"));
						$dateValS = date("F d, Y"); 
					} 
					?>
					<div class="form-group row">
						<div class="col-md-8 offset-md-2 col-sm-12">
							<input type="hidden" name="dateValS" id="dateValS" <?php if($_POST) { ?>value="<?php echo $_POST['dateValS'];?>"<?php }  ?> />  
							<input type="hidden" name="dateValE" id="dateValE" <?php if($_POST) { ?>value="<?php echo $_POST['dateValE'];?>"<?php }  ?> />   
							<div id="advance-daterange" name="advance-daterange" class="btn btn-primary btn-block text-left f-s-12">
								<i class="fa fa-caret-down pull-right m-t-2"></i>
								<span>
								<?php echo $dateValS.' - '.$dateValE; ?>
								</span> 
							</div>
						</div>
					</div>

			<div class="row">
				<div class="col-md-12">
				  <div class="panel panel-inverse panel-primary boxShadow" style="overflow-x: scroll!important;" >
						<div class="panel-body">
							<table id="empTable" class="display table table-bordered m-b-0" width="100%" cellspacing="0">
								<thead>
									<tr> 
										<th><?php echo Log; ?></th>
										<th><?php echo Comment; ?></th>
										<th><?php echo Responder; ?></th>
										<th><?php echo Respondtime; ?></th>
										<th><?php echo Machine; ?></th>
										<th><?php echo Redstarttime; ?></th>
										<th><?php echo Redendtime; ?></th>
										<th><?php echo Durationseconds; ?></th>
										<th><?php echo Notificationaddtime; ?></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div> 
			</div>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	</div>
	

