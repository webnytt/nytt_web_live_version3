  <link rel="stylesheet" href="<?php echo base_url('assets/scroller_timepicker/picker.css'); ?>">

<style type="text/css">
    .btn-default.btn-on.active
    {
        background-color: #002060 !important;
        color: white;
    }
    .btn-default.btn-off.active
    {
        background-color: #002060 !important;
        color: white;
    }
    .btn-default.btn-on
    {
        background-color: #d8dbe0;
        color: white;
    }
    .btn-default.btn-off
    {
        background-color: #d8dbe0;
        color: white;
    }
</style>

<style type="text/css">
    
    .col-md-3{
  display: inline-block;
  margin-left:-4px;
}
.col-md-3 img{
  width:100%;
  height:auto;
}
body .carousel-indicators li{
  background-color:red;
}
body .carousel-indicators{
  bottom: 0;
}
body .carousel-control-prev-icon,
body .carousel-control-next-icon{
  background-color:red;
}
body .no-padding{
  padding-left: 7px;
  padding-right: 0;
   }


.carousel-item-next, .carousel-item-prev, .carousel-item.active 
{
    display: flex !important;
}
.slick-prev 
{
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
}
.slick-next
{
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
}

.filter.active
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: orange !important;
}
.filter
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: gray !important;
}
.filter1.active
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: orange !important;
}
.filter1
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: gray !important;
}
.datepicker.dropdown-menu 
{
    min-width: 200px !important;
}
.datepicker.datepicker-dropdown 
{
    width: 207px !important;
}
.datepicker 
{
    min-width: 207px!important;
}
.datepicker,
.table-condensed 
{
}
.datepicker table tr td, 
.datepicker table tr th 
{
    padding-left: 0px !important;
    padding-right: 0px !important;
}
.dow 
{
    font-weight: 500 !important;
}
.table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th 
{
    padding: 0px 0px !important;
}
.datepicker table tr td, .datepicker table tr th 
{
    width: 0px !important;
    height: 19px !important;
    border-radius: 15px !important;
}
.datepicker table tr td span.active.active, .datepicker table tr td.active.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active:hover:hover, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover, .open .dropdown-toggle.datepicker table tr td.active:hover 
{
    background: #FF8000!important;
}
.datepicker .prev 
{
    box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .prev:before 
{
    color: #FF8000 !important;
}
.datepicker .next 
{
    box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .next:before 
{
    color: #FF8000 !important;
}
.datepicker table tr td.old, .datepicker table tr td.new 
{
    color: #ddd2d2 !important;
}
@media screen and (min-device-width:768px)and (max-width:999px) 
    {
       .laptopsize
        {
            height: 20px;
            padding: 2px;
            float: left;
            margin-right: 10px;
            font-size: 10px!important;
        }
    }
</style>

   <style type="text/css">
        .machineNameText
        {
            display: inline-block;
            width: 60%;
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis;
        }

      input:focus,
      select:focus,
      textarea:focus,
      .form-control:focus,
      button:focus {
          outline: none;
      }

        .form-control.focus, .form-control.input-white.focus, .form-control.input-white:focus, .form-control:focus
        { 
            box-shadow : none;
            border-color: #bec6ce;
        }

    .selectedView
    {
        color: #FF8000;
    }
      
    .border-left-right-top-hide
    {
        border-left: none;
        border-top: none;
        border-right: none;
        border-radius: 0;
    }

    .picker-hours .picker-item
    {
        text-align : right !important;
    }

    .picker-hours .picker-cell__control--prev::before
    {
        margin-left: 37px !important;
    }

    .picker-hours .picker-cell__control--next::before
    {
        margin-left: 37px !important;
    }

    .picker-minutes .picker-item
    {
        text-align : left !important;
    }
    
    .picker-minutes .picker-cell__control--prev::before
    {
        margin-left: -37px !important;
    }

    .picker-minutes .picker-cell__control--next::before
    {
        margin-left: -37px !important;
    }
   </style>

<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
<div id="content" class="content">
           
            <h1 style="font-size: 22px;color: #002060!important;" class="page-header"><?php echo Analytics; ?></h1>
            <br />
            <div class="row" style="margin-top: -23px;">
                <div class="col-md-3" style="padding-left: 0px;">
                    <a href="<?php echo base_url('TestToLive/live_breakdown3'); ?>" class="btn btn-primary" style="background: #F60100;padding-left: 18px !important;padding-right: 18px !important;margin-left: 10px;" ><?php echo Stopanalytics; ?></a>
                </div>
                <div class="col-md-1 show_by">
                    <label><?php echo showby; ?> :</label>
                </div>
                <div class="col-md-2" style="padding-left: 5px;padding-right: 0px;">
                    <select  class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                        <option class="new_test" value="day"><?php echo Day; ?></option>
                        <option class="new_test" value="weekly"><?php echo week; ?></option>
                        <option class="new_test" value="monthly"><span> <?php echo MONTH; ?> </span></option>
                        <option class="new_test" value="yearly"><span> <?php echo year; ?> </span></option>
                    </select>
                </div>
                <div class="col-md-1 day show_by" style="width:100%;flex: 0 0 2% !important;padding-left:0px;padding-right: 0px !important;padding-top: 5px;">
                    <label style="padding-left:8px;padding-right: 5px;"> <?php echo Fors; ?>:&nbsp;</label>
                </div> 
                <div class="col-md-2 day" style="width:100%;padding-left: 5px;padding-right: 12px;">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%;" value="<?php echo date('m/d/Y'); ?>">
                </div>
                <div class="col-md-2 weekly" style="display: none;">
                    <select class="form-control" id="choose3" onchange="changeFilterValue()">
                        <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1"><?php echo week.' 1'; ?> </option>
                        <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2"><?php echo week.' 2'; ?> </option>
                        <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3"><?php echo week.' 3'; ?> </option>
                        <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4"><?php echo week.' 4'; ?> </option>
                        <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5"><?php echo week.' 5'; ?> </option>
                        <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6"><?php echo week.' 6'; ?> </option>
                        <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7"><?php echo week.' 7'; ?> </option>
                        <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8"><?php echo week.' 8'; ?> </option>
                        <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9"><?php echo week.' 9'; ?> </option>
                        <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10"><?php echo week.' 10'; ?> </option>
                        <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11"><?php echo week.' 11'; ?> </option>
                        <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12"><?php echo week.' 12'; ?> </option>
                        <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13"><?php echo week.' 13'; ?> </option>
                        <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14"><?php echo week.' 14'; ?> </option>
                        <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15"><?php echo week.' 15'; ?> </option>
                        <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16"><?php echo week.' 16'; ?> </option>
                        <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17"><?php echo week.' 17'; ?> </option>
                        <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18"><?php echo week.' 18'; ?> </option>
                        <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19"><?php echo week.' 19'; ?> </option>
                        <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20"><?php echo week.' 20'; ?> </option>
                        <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21"><?php echo week.' 21'; ?> </option>
                        <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22"><?php echo week.' 22'; ?> </option>
                        <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23"><?php echo week.' 23'; ?> </option>
                        <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24"><?php echo week.' 24'; ?> </option>
                        <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25"><?php echo week.' 25'; ?> </option>
                        <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26"><?php echo week.' 26'; ?> </option>
                        <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27"><?php echo week.' 27'; ?> </option>
                        <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28"><?php echo week.' 28'; ?> </option>
                        <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29"><?php echo week.' 29'; ?> </option>
                        <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30"><?php echo week.' 30'; ?> </option>
                        <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31"><?php echo week.' 31'; ?> </option>
                        <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32"><?php echo week.' 32'; ?> </option>
                        <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33"><?php echo week.' 33'; ?> </option>
                        <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34"><?php echo week.' 34'; ?> </option>
                        <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35"><?php echo week.' 35'; ?> </option>
                        <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36"><?php echo week.' 36'; ?> </option>
                        <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37"><?php echo week.' 37'; ?> </option>
                        <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38"><?php echo week.' 38'; ?> </option>
                        <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39"><?php echo week.' 39'; ?> </option>
                        <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40"><?php echo week.' 40'; ?> </option>
                        <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41"><?php echo week.' 41'; ?> </option>
                        <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42"><?php echo week.' 42'; ?> </option>
                        <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43"><?php echo week.' 43'; ?> </option>
                        <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44"><?php echo week.' 44'; ?> </option>
                        <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45"><?php echo week.' 45'; ?> </option>
                        <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46"><?php echo week.' 46'; ?> </option>
                        <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47"><?php echo week.' 47'; ?> </option>
                        <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48"><?php echo week.' 48'; ?> </option>
                        <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49"><?php echo week.' 49'; ?> </option>
                        <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50"><?php echo week.' 50'; ?> </option>
                        <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51"><?php echo week.' 51'; ?> </option>
                        <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52"><?php echo week.' 52'; ?> </option>
                    </select>
                </div>
                <div class="col-md-1 weekly" style="display: none;">
                </div>
                <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
                <div class="col-md-1 monthly show_by" style="max-width: 2% !important;flex: 0 0 2% !important;padding-left: 0px !important;padding-right: 0px !important;display: none;">
                    <label> <?php echo Fors; ?> </label>
                </div>
                <div class="col-md-2 monthly" style="max-width: 23% !important;flex: 0 0 23% !important;padding-left: 3px;padding-right: 10px;display: none;">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker1" id="choose4">
                </div>
                <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
                <div class="col-md-2 yearly" style="display: none;">
                    <select class="form-control"  id="choose5" onchange="changeFilterValue()">
                        <?php for($i=0 ; $i < $years; $i++){ 
                            $yearValue = 2018 + $i;
                            ?>
                            <option <?php if ($yearValue == date('Y')) {
                                echo "selected";
                            } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                        <?php } ?>
                    </select>
                </div>
                <div class="col-md-1 yearly" style="display: none;"></div>

                <div class="col-md-1 machineView">
                    <spna style="float: right;padding-top: 8px;"><?php echo Shifts; ?> : </spna> 
                </div>

                <style type="text/css">
                    .dropdown-toggle:after {
                        vertical-align: 1px;
                        border-width: 4px;
                        float: right;
                        margin-top: 6px;
                    }
                </style>
                <div class="col-md-2 machineView">
                    <input type="hidden" id="viewId" value="none">
                    <div class="dropdown" style="box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;border: none !important;padding: 8px;border-radius: 5px;cursor: pointer;background: white;" >
                        <div class="dropdown-toggle" data-toggle="dropdown">
                            <span style="padding-left: 8px;" id="selectedView"><?php echo None; ?> </span>
                        </div>
                        <div class="dropdown-menu" style="width: 100%;margin-left: -8px;margin-top: 17px;">
                          <div class="dropdown-item selectedView" id="viewIdnone" onclick="selectView('none','None')" style="font-weight: 600;">
                            <?php echo None; ?> 
                            <hr style="width:100%height: 3px;margin-top: 0.5rem;margin-bottom: 0rem;">
                          </div>

                          <?php foreach ($machineView as $key => $value) { ?>
                              <div class="dropdown-item" id="viewId<?php echo $value->viewId; ?>" style="font-weight: 600;"><span onclick="selectView(<?php echo $value->viewId; ?>,'<?php echo $value->viewName; ?>')">
                                <?php echo $value->viewName; ?></span><!--  <span style="float: right;"><img onclick="editView(<?php echo $value->viewId; ?>)" style="width: 20px;" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>"></span>  -->
                                <hr style="width:100%height: 3px;margin-top: 0.5rem;margin-bottom: 0rem;">
                              </div>
                          <?php } ?>

                        </div>
                      </div>
                </div>

            </div>
          
            <style type="text/css">
                
                .machineDegine 
                {
                    margin-left: -22px;
                    margin-right: 30px;
                }
            </style>
            <!-- <div class="row" style="margin-top: 13px;">
                <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>">
                <div id="demo" class="carousel slide" data-ride="carousel" style="width: 100%;" data-interval="false">

                  <div class="carousel-inner no-padding">
                  
                    <div class="carousel-item <?php if(!$_POST || $searchKeyCompar == 0) { echo "active";} ?>">
                        <div class="col-xs-3 col-sm-3 col-md-3 machineDegine">
                            <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> Show all  </a>
                        </div>
                <?php 
                
                foreach($machineNameList as $key => $machine) { ?>
                      <?php if($countQuery % 4 == 0){ 
                                               ?>
                        </div>
                        <div class="carousel-item <?php if($f == $searchKeyCompar){ echo "active"; } ?>">
                      <?php $f++;  } ?>
                      <div class="col-xs-3 col-sm-3 col-md-3 machineDegine">
                        <a href="javascript:;" class="btn btn-default btn-lg filter machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }?>" id="machine<?php echo $machine['machineId']; ?>"> <?php echo $machine['machineName']; ?> </a>
                      </div>    
                <?php $countQuery++; } ?>
                    </div>
                  </div>
                  
                  <?php if ($count > 3) { ?>
                      <a class="carousel-control-prev" href="#demo" data-slide="prev" style="background-color: #FFFFFF !important;color: #FF8000 !important;">
                        <i style="font-size: 20px;" class="fa fa-angle-left" aria-hidden="true"></i>
                      </a>
                      <a class="carousel-control-next" href="#demo" data-slide="next" style="background-color: #FFFFFF !important;color: #FF8000 !important;">
                        <i style="font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i>
                      </a>
                  <?php } ?>
                </div>
            </div> -->

            <div class="row">
                <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>">

                <section class="regular slider">
                    <div>
                        <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> <?php echo Showall; ?>  </a>
                    </div>
                <?php if (!empty($listMachines)) {
                    foreach($listMachines as $key => $machine) { ?>
                    <div>
                      <a href="javascript:;" class="btn btn-default btn-lg filter machineNameText <?php if($machine->machineId == $_POST['machineId']){  echo "active"; }?>" id="machine<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                    </div>
                <?php } } ?>
              </section>
            </div>
            <style>
                #chartwrap
                {
                    position: relative!important;
                    height:0%!important;
                    overflow: hidden!important;
                }
                @media screen and (min-device-width:320px)and (max-width: 375px)
                {
                   .DailyGraphs3
                    {
                       margin-left:5px!important;
                    }
                }
            </style>

            <div class="row" id="chartwrap" style="margin-top: 13px;display: flex;flex-wrap: wrap;justify-content:space-between;margin-left: -20px;">
                <div class="col-md-5" style="max-height: 390px !important;">
                    <div class="tab-content boxShadow" style="min-height: 335px!important;" >
                        <div class="tab-pane fade active show">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-md-12" >
                                    <h4 style="color: #002060;"><?php echo Machinemode; ?></h4>
                                    <center style="margin-bottom: -30px;margin-top: -24px;">
                                        <div id="DailyGraph3" class="DailyGraphs3" style="padding-right:20px;height:330px;"></div>
                                    </center>
                                </div>
                                <div id="DailyGraph3 " class="col-md-12 DailyGraphs3" style="height:273px;display:none;" >
                                    <h4 style="color: #002060;"><?php echo Actualproductiontime; ?></h4>
                                    <center>
                                        <div class="laptopsizegraph" style="height: 180px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 180px;" >
                                            <div class="laptopsizegraph2" style="height: 150px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 150px;" ></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-5" style="max-height: 390px !important;">
                   <div class="tab-content boxShadow" style="min-height: 335px!important;">
                        <div class="tab-pane fade active show" >
                            <div class="row m-l-0 m-r-0">
                                <div id="dataDailyGraph4" class="col-md-12">
                                    <h4 style="color: #002060;"><?php echo Productiontime; ?></h4>
                                    <center>
                                        <div id="DailyGraph4" style="height:270px;" ></div>
                                    </center>
                                </div>
                                <div id="blankDailyGraph4" class="col-md-12 graphmargin" style="height:273px;display:none;" >
                                    <h4 style="color: #002060;"><?php echo Productiontime; ?></h4>
                                    <center>
                                        <div class="laptopsizegraph" style="height: 160px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 160px;" >
                                            <div class="laptopsizegraph2" style="height: 130px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 130px;" ></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .bagerClass
                    {
                        width: 20px;
                        height: 20px;
                        padding: 2px;
                        float: left;
                        margin-right: 10px;
                    }
                    @media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .graphmargin
                        {
                            margin-left:-19px!important;
                        }
                    }@media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .laptopsizegraph
                        {
                            height: 146px!important;
                            border: 1px solid #d8cfcf!important;
                            border-radius: 50%!important;
                            margin-top: 55px!important;
                            width: 138px!important;
                        }
                    }
                    @media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .laptopsizegraph2
                        {
                            height: 114px!important;
                            border: 1px solid #d8cfcf!important;
                            border-radius: 50%!important;
                            margin-top: 14px!important;
                            width: 82%!important;
                            margin-left: 0px!important;
                        }
                    }
                </style>

                <div class="col-md-2">
                    <div class="tab-content laptopsize boxShadow" style="height: 335px;">
                        <div class="tab-pane fade active show">
                            
                           <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #76BA1B;"> </div>
                                <div class="p-l-10"> <?php echo Running; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FFCF00;"> </div>
                                <div class="p-l-10"> <?php echo Waiting; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #F60100;"> </div>
                                <div class="p-l-10"> <?php echo Stopped; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #CCD2DF;"> </div>
                                <div class="p-l-10"> <?php echo Off; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #000000;"> </div>
                                <div class="p-l-10"> <?php echo Nodet; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #002060;"> </div>
                                <div class="p-l-10"> <?php echo Productiontime; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #124D8D;"> </div>
                                <div class="p-l-10">  <?php echo Setuptime; ?> </div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FF8000;"> </div>
                                <div class="p-l-10"> <?php echo Noproduction; ?> </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
            </div>
            <div class="graph-loader hide" ></div>
                
            <div class="row"  style="margin-left: -20px;">
                <div class="col-md-12" >
                    <div class="tab-content boxShadow" style="">
                        <div class="tab-pane fade active show" style="overflow-x: scroll!important;">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-md-12">
                                    <h3 id="headerChange" style="color: #002060;"><?php echo Hourlymachineanalysis; ?></h3>
                                    <div id="DailyGraph1" style="height:350px;width:100%;" >                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
        </div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    </div>
    <div class="modal fade" id="add-view-modal" role="dialog">
    <div class="modal-dialog">

    
    
      <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
            <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title"><?php echo Addnewview; ?></h4>
            <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
        </div>
        <div class="modal-body">
            
            <form class="p-b-20" action="add_view" method="POST" id="add_view_form" style="margin-left: 10px;margin-right: 10px;">
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 0 5px;" class="form-group">
                          <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control border-left-right-top-hide" id="viewName" name="viewName" 
                          placeholder="Enter view" required > 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Machinestarts; ?> <br> <?php echo runningat; ?></center></h5>

                        <div class="js-inline-picker-start"></div>
                    </div>

                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Machinestops; ?> <br> <?php echo runningat; ?></center></h5>

                        <div class="js-inline-picker-stop"></div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                      <hr style="height: 2px;margin-top: 1rem;">
                    </div>
                </div>

                <div class="addBreakData" style="margin-bottom: 10px;display: none;">
                   
                </div>

                <div class="row addBreakButton">
                    <div class="col-md-12">
                      <span onclick="addBreak()" style="color: #FF8000;cursor: pointer;"><i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?></span>
                    </div>
                </div>

                <div class="row removeBreak" style="display: none;">
                    <div class="col-md-12">
                      <span onclick="removeBreak()" style="color: red;cursor: pointer;"><i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?></span>
                    </div>
                </div>

                <div class="row removeBreak" style="margin-top: 25px;display: none;">
                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Breakstartsat; ?></center></h5>

                        <div class="js-inline-picker-break-start"></div>
                    </div>

                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Breakendsat; ?></center></h5>

                        <div class="js-inline-picker-break-stop"></div>
                    </div>
                </div>

                <div class="row removeBreak" style="margin-left: 0px;margin-right: 0px;display: none;">
                    <div class="col-md-12">
                      <br>
                      <center>
                        <a onclick="saveBreaks()" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 15px;color: white;" class="btn btn-primary m-r-5" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                      </center>
                    </div>
                </div>

                <div class="row submitButton" style="margin-left: 0px;margin-right: 0px;">
                    <div class="col-md-12">
                      <br>
                      <center>
                        <button type="submit" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 15px;" class="btn btn-primary m-r-5" id="add_view_submit" ><i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Add; ?></button>
                      </center>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>


  <div class="modal fade" id="edit-view-modal" role="dialog">
    <div class="modal-dialog">
    <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
            <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title"><?php echo Editview; ?></h4>
            <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
        </div>
        <div class="modal-body">
            
            <form class="p-b-20" action="edit_view_form" method="POST" id="edit_view_form" style="margin-left: 10px;margin-right: 10px;">

                <input type="hidden" name="viewId" id="viewIdEdit">
                <div class="row">
                    <div class="col-md-12">
                        <div style="padding: 0 5px;" class="form-group">
                          <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control border-left-right-top-hide" id="viewNameEdit" name="viewName" 
                          placeholder="Enter view" required > 
                        </div>
                    </div>
                </div>

                <div class="row">
                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Machinestarts; ?> <br> <?php echo runningat; ?></center></h5>

                        <div class="js-inline-picker-edit-start"></div>
                    </div>

                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Machinestops; ?> <br> <?php echo runningat; ?></center></h5>

                        <div class="js-inline-picker-edit-stop"></div>
                    </div>
                </div>


                <div class="row">
                    <div class="col-md-12">
                      <hr style="height: 2px;margin-top: 1rem;">
                    </div>
                </div>

                <div class="addEditBreakData" style="margin-bottom: 10px;display: none;">
                   
                </div>

                <div class="row addEditBreakButton">
                    <div class="col-md-12">
                      <span onclick="addEditBreak()" style="color: #FF8000;cursor: pointer;"><i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?></span>
                    </div>
                </div>

                <div class="row removeEditBreak" style="display: none;">
                    <div class="col-md-12">
                      <span onclick="removeEditBreak()" style="color: red;cursor: pointer;"><i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?></span>
                    </div>
                </div>

                <div class="row removeEditBreak" style="margin-top: 25px;display: none;">
                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Breakstartsat; ?></center></h5>

                        <div class="js-inline-picker-edit-break-start"></div>
                    </div>

                    <div class="col-md-6">
                        <h5 style="color: #002060"><center><?php echo Breakendsat; ?></center></h5>

                        <div class="js-inline-picker-edit-break-stop"></div>
                    </div>
                </div>

                <div class="row removeEditBreak" style="margin-left: 0px;margin-right: 0px;display: none;">
                    <div class="col-md-12">
                      <br>
                      <center>
                        <a onclick="saveEditBreaks()" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 15px;color: white;" class="btn btn-primary m-r-5" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                      </center>
                    </div>
                </div>

                <div class="row submitEditButton" style="margin-left: 0px;margin-right: 0px;">
                    <div class="col-md-12">
                      <br>
                      <center>
                        <button type="submit" style="padding: 13px 30px !important;font-size: 15px;" class="btn btn-primary m-r-5" id="edit_view_submit" >Save</button>

                        <a style="padding: 6px 0px !important;font-size: 15px;background-color: #F60100;" class="btn btn-primary m-r-5" id="deleteView" ><img style="width: 38px;height: 34px;" src="<?php echo base_url('assets/nav_bar/delete_white.svg'); ?>"></a>
                      </center>
                    </div>
                </div>
            </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="delete-view-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content" style="max-height: 260px;max-width: 100%;">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Delete; ?> </h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div> 
            <div class="modal-body">
                <form class="p-b-20" action="delete_view_form" method="POST" id="delete_view_form" >
                    <input type="hidden" name="deleteViewId" id="deleteViewId" /> 
                    <div class="modal-footer" style="border-top: none;padding-top: 0;" >
                        <div class="row">
                            <div class="col-md-12" style="margin-top: 30px;" >
                                <center>
                                    <img style="margin-top:-8%;width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
                                </center>
                            </div>
                            <br>
                            <div id="" class="m-b-10 alert alert-success fade hide" ></div>
                            <div class="col-md-12" style="margin-top: -5px;">
                                <center>
                                    <?php echo Areyousureyouwanttodeleteview; ?> ?
                                </center>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <br>
                                    <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_view_submit"><?php echo Delete; ?></button>
                                    <br>
                                </center>
                            </div>
                        </div>          
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>