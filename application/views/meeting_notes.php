<link href="<?php echo base_url('assets/css/meeting_notes.css');?>" rel="stylesheet" />
<div id="content" class="content">
  
  <h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Meetingnotes; ?></h1>
  <style>

  .displayfilterrowstyle
  {
    display:contents!important; 
  }
  </style>
  <div class="row">
    <div class="col-md-12 table_data" style="padding-left:14px;">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >
          <div class="row" style="float:left;margin:10px;">
            <span class="displayfilterrowstyle" id="showCreatedDate"></span>
            <span class="displayfilterrowstyle" id="showType"></span>
          </div>
        <div class="panel-body">
          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>
                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">
                  <div class="" style="width:60px!important;"><?php echo NoteID; ?></div>
                  <br>
                  <small>&nbsp;</small>
                </th>
                
                <th style="width:15%;color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">
                  <div class="" style="width:175px!important;"><?php echo NoteName; ?></div>
                  <br>
                  <small>&nbsp;</small>
                </th>

                <th style="width:35%;color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">
                  <div class=""><?php echo Notes; ?></div>
                  <br>
                  <small>&nbsp;</small>
                </th>

               <th  style="color: #b8b0b0;cursor: pointer;" >
                  <input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-3000 day")); ?>" />  
                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />   
                    <div id="advance-daterange" name="advance-daterange" style="width: 110px;padding-bottom: 5px!important">
                      <span>
                          <?php echo Createddate; ?>&nbsp;&nbsp;
                      </span> 
                     <i class="fa fa-caret-down m-t-2"></i>
                    </div>
                    <small  style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
                
                <th style="color: #b8b0b0;cursor: pointer;padding: 14px 15px !important;min-width:100px;">
                  <div id="filterTypeSelected" class="dropdown filtertypeSelectesStyle">
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterTypeSelectedValue"> <?php echo ucfirst(type); ?>&nbsp;</span>
                    <div class="dropdown-menu" >
                      <div class="dropdown-item" style="display: flex;align-items: left;justify-content: left;">
                        <div class="form-check form-checkWidth" style="justify-content: end!important;">
                          <input onclick="filterType()" value="'1'" class="filterType form-check-input" type="checkbox" id="Daily" 
                           >
                          <label class="form-check-label" for="Daily">
                            <?php echo Daily; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item" style="display: flex;align-items: left;justify-content: left;">
                        <div class="form-check form-checkWidth" style="justify-content: end!important;">
                          <input onclick="filterType()" value="'2'" class="filterType form-check-input" type="checkbox" id="Weekly" 
                           >
                          <label class="form-check-label" for="Weekly">
                            <?php echo Weekly; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item" style="display: flex;align-items: left;justify-content: left;">
                        <div class="form-check form-checkWidth" style="justify-content: end!important;">
                          <input onclick="filterType()" value="'3'" class="filterType form-check-input" type="checkbox" id="Monthly" 
                           >
                          <label class="form-check-label" for="Monthly">
                            <?php echo Monthly; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item" style="display: flex;align-items: left;justify-content: left;">
                        <div class="form-check form-checkWidth" style="justify-content: end!important;">
                          <input onclick="filterType()" value="'4'" class="filterType form-check-input" type="checkbox" id="Yearly" 
                           >
                          <label class="form-check-label" for="Yearly">
                            <?php echo Yearly; ?>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                <small style="color: #FF8000;"  >&nbsp;</small>
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">&nbsp;&nbsp;&nbsp;
                  <br>
                  <small>&nbsp;</small>
                </th>

                <?php if ($accessPointEdit == true) { ?>
                  <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">
                    <br>
                    <small>&nbsp;</small>
                  </th>
                <?php } ?>



              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div> 
  </div>
<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<div class="modal fade" id="modal-add-meeting-notes" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content addnotemodelstyle">
      <div class="modal-header" style="background-color: #002060;">
          <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title machine_name" id="liveMachineName"><?php echo Addnewnote;?></h4>
          <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
      </div>
      <div class="modal-body">
        <form class="p-b-20" action="add_meeting_notes_form" method="POST" id="add_meeting_notes_form" style="margin-left: 10px;margin-right: 10px;">
          <div class="row" style="margin-left: 0px;margin-right: 0px;">
            <div style="padding: 0 5px;" class="form-group col-md-12">
              <input type="text" style="padding: 0px 4px 16px !important;height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="noteName" name="noteName" 
              placeholder="<?php echo Entermeetingnote; ?>" required > 
            </div>
           <div class="col-md-12">
                <h4><?php echo Assignmachines; ?></h4>
            </div>
            <div class="col-md-12" style="padding-left: 50px;padding-right: 50px;">
              <input type="hidden" name="machineIds" id="machineIds">
              <?php foreach ($machine as $key => $value) { ?>
                <button class="btn btn-primary bluebuttonUnSelected machineNameText" style="min-width:30%;width: max-content!important;" type="button" onclick="selectMachine(<?php echo $value['machineId']; ?>)" id="machineId<?php echo $value['machineId']; ?>"><?php echo $value['machineName']; ?> <span style="display: none;float:right;" id="removeMachine<?php echo $value['machineId']; ?>"><i class="fa fa-times" aria-hidden="true"></i></span></button>
              <?php } ?>
            </div>

            <div class="col-md-12" style="margin-top: 10px;">
                <h4><?php echo Compare; ?></h4>
            </div>
            <div class="col-md-12" style="padding-left: 50px;padding-right: 50px;">
                <input type="hidden" name="filterType" id="filterType">
                <button onclick="selectFilterType('1')" style="min-width:30%;" id="filterType1"  type="button" class="btn btn-primary bluebuttonUnSelected filterType"><?php echo Daily; ?></button>
                <button onclick="selectFilterType('2')" style="min-width:30%;" id="filterType2"  type="button" class="btn btn-primary bluebuttonUnSelected filterType"><?php echo Weekly; ?></button>
                <button onclick="selectFilterType('3')" style="min-width:30%;" id="filterType3"  type="button" class="btn btn-primary bluebuttonUnSelected filterType"><?php echo Monthly; ?></button>
                <button onclick="selectFilterType('4')" style="min-width:30%;" id="filterType4"  type="button" class="btn btn-primary bluebuttonUnSelected filterType"><?php echo Yearly; ?></button>
            </div>
            
            <div class="col-md-6 dailyDate" style="margin-top: 15px;">
              <div style="box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;border: none !important;padding: 8px;border-radius: 5px;cursor: pointer;" class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
               <input style="border: none;color: #b3acac;font-weight:600;" type="text" class="datepicker58" id="startDate" name="startDate" value="<?php echo date('m/d/Y',strtotime('-1 day')) ?>"> 
               <i style="margin-left: 15px;color: #b3acac;" class="fa fa-caret-down" aria-hidden="true"></i>
              </div>
            </div>

            <div class="col-md-6 dailyDate" style="margin-top: 15px;">
              <div style="box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;border: none !important;padding: 8px;border-radius: 5px;cursor: pointer;" class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
               <input style="border: none;color: #b3acac;font-weight: 600;" type="text" class="datepicker59" id="endDate" name="endDate" value="<?php echo date('m/d/Y') ?>"> 
               <i style="margin-left: 15px;color: #b3acac;" class="fa fa-caret-down" aria-hidden="true"></i>
              </div>
            </div>
            
            <div class="col-md-6 monthlyDate" style="margin-top: 15px;display: none;">
              <div style="box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;border: none !important;padding: 8px;border-radius: 5px;cursor: pointer;" class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
               <input style="border: none;color: #b3acac;font-weight:600;" type="text" id="monthStartDate" name="monthStartDate" value="<?php echo date('F, Y',strtotime('-1 months')) ?>"> 
               <i style="margin-left: 15px;color: #b3acac;" class="fa fa-caret-down" aria-hidden="true"></i>
              </div>
            </div>

            <div class="col-md-6 monthlyDate" style="margin-top: 15px;display: none;">
              <div style="box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;border: none !important;padding: 8px;border-radius: 5px;cursor: pointer;" class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
               <input style="border: none;color: #b3acac;font-weight:600;" type="text" id="monthEndDate" name="monthEndDate" value="<?php echo date('F, Y') ?>"> 
               <i style="margin-left: 15px;color: #b3acac;" class="fa fa-caret-down" aria-hidden="true"></i>
              </div>
            </div>

            <div class="col-md-6 weeklyDate" style="margin-top: 15px;display: none;">
              <select  style="min-width:100%;width:100%;" class="form-control select2" name="weekStartDate">
                  <option <?php if((date('W') - 1) == "1") { echo "selected"; } ?> value="1"><?php echo week.' 1'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "2") { echo "selected"; } ?> value="2"><?php echo week.' 2'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "3") { echo "selected"; } ?> value="3"><?php echo week.' 3'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "4") { echo "selected"; } ?> value="4"><?php echo week.' 4'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "5") { echo "selected"; } ?> value="5"><?php echo week.' 5'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "6") { echo "selected"; } ?> value="6"><?php echo week.' 6'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "7") { echo "selected"; } ?> value="7"><?php echo week.' 7'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "8") { echo "selected"; } ?> value="8"><?php echo week.' 8'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "9") { echo "selected"; } ?> value="9"><?php echo week.' 9'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "10") { echo "selected"; } ?> value="10"><?php echo week.' 10'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "11") { echo "selected"; } ?> value="11"><?php echo week.' 11'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "12") { echo "selected"; } ?> value="12"><?php echo week.' 12'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "13") { echo "selected"; } ?> value="13"><?php echo week.' 13'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "14") { echo "selected"; } ?> value="14"><?php echo week.' 14'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "15") { echo "selected"; } ?> value="15"><?php echo week.' 15'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "16") { echo "selected"; } ?> value="16"><?php echo week.' 16'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "17") { echo "selected"; } ?> value="17"><?php echo week.' 17'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "18") { echo "selected"; } ?> value="18"><?php echo week.' 18'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "19") { echo "selected"; } ?> value="19"><?php echo week.' 19'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "20") { echo "selected"; } ?> value="20"><?php echo week.' 20'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "21") { echo "selected"; } ?> value="21"><?php echo week.' 21'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "22") { echo "selected"; } ?> value="22"><?php echo week.' 22'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "23") { echo "selected"; } ?> value="23"><?php echo week.' 23'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "24") { echo "selected"; } ?> value="24"><?php echo week.' 24'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "25") { echo "selected"; } ?> value="25"><?php echo week.' 25'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "26") { echo "selected"; } ?> value="26"><?php echo week.' 26'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "27") { echo "selected"; } ?> value="27"><?php echo week.' 27'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "28") { echo "selected"; } ?> value="28"><?php echo week.' 28'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "29") { echo "selected"; } ?> value="29"><?php echo week.' 29'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "30") { echo "selected"; } ?> value="30"><?php echo week.' 30'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "31") { echo "selected"; } ?> value="31"><?php echo week.' 31'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "32") { echo "selected"; } ?> value="32"><?php echo week.' 32'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "33") { echo "selected"; } ?> value="33"><?php echo week.' 33'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "34") { echo "selected"; } ?> value="34"><?php echo week.' 34'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "35") { echo "selected"; } ?> value="35"><?php echo week.' 35'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "36") { echo "selected"; } ?> value="36"><?php echo week.' 36'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "37") { echo "selected"; } ?> value="37"><?php echo week.' 37'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "38") { echo "selected"; } ?> value="38"><?php echo week.' 38'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "39") { echo "selected"; } ?> value="39"><?php echo week.' 39'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "40") { echo "selected"; } ?> value="40"><?php echo week.' 40'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "41") { echo "selected"; } ?> value="41"><?php echo week.' 41'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "42") { echo "selected"; } ?> value="42"><?php echo week.' 42'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "43") { echo "selected"; } ?> value="43"><?php echo week.' 43'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "44") { echo "selected"; } ?> value="44"><?php echo week.' 44'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "45") { echo "selected"; } ?> value="45"><?php echo week.' 45'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "46") { echo "selected"; } ?> value="46"><?php echo week.' 46'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "47") { echo "selected"; } ?> value="47"><?php echo week.' 47'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "48") { echo "selected"; } ?> value="48"><?php echo week.' 48'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "49") { echo "selected"; } ?> value="49"><?php echo week.' 49'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "50") { echo "selected"; } ?> value="50"><?php echo week.' 50'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "51") { echo "selected"; } ?> value="51"><?php echo week.' 51'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if((date('W') - 1) == "52") { echo "selected"; } ?> value="52"><?php echo week.' 52'; ?>, <?php echo date('F Y'); ?></option>
              </select>
            </div>
            <div class="col-md-6 weeklyDate" style="margin-top: 15px;display: none;">
              
              <select  style="min-width:100%;width:100%;" class="form-control select2" name="weekEndDate">
                  <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1">  <?php echo week.' 1'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2">  <?php echo week.' 2'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3">  <?php echo week.' 3'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4">  <?php echo week.' 4'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5">  <?php echo week.' 5'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6">  <?php echo week.' 6'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7">  <?php echo week.' 7'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8">  <?php echo week.' 8'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9">  <?php echo week.' 9'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10"><?php echo week.' 10'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11"><?php echo week.' 11'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12"><?php echo week.' 12'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13"><?php echo week.' 13'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14"><?php echo week.' 14'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15"><?php echo week.' 15'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16"><?php echo week.' 16'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17"><?php echo week.' 17'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18"><?php echo week.' 18'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19"><?php echo week.' 19'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20"><?php echo week.' 20'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21"><?php echo week.' 21'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22"><?php echo week.' 22'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23"><?php echo week.' 23'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24"><?php echo week.' 24'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25"><?php echo week.' 25'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26"><?php echo week.' 26'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27"><?php echo week.' 27'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28"><?php echo week.' 28'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29"><?php echo week.' 29'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30"><?php echo week.' 30'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31"><?php echo week.' 31'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32"><?php echo week.' 32'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33"><?php echo week.' 33'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34"><?php echo week.' 34'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35"><?php echo week.' 35'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36"><?php echo week.' 36'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37"><?php echo week.' 37'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38"><?php echo week.' 38'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39"><?php echo week.' 39'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40"><?php echo week.' 40'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41"><?php echo week.' 41'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42"><?php echo week.' 42'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43"><?php echo week.' 43'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44"><?php echo week.' 44'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45"><?php echo week.' 45'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46"><?php echo week.' 46'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47"><?php echo week.' 47'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48"><?php echo week.' 48'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49"><?php echo week.' 49'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50"><?php echo week.' 50'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51"><?php echo week.' 51'; ?>, <?php echo date('F Y'); ?></option>
                  <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52"><?php echo week.' 52'; ?>, <?php echo date('F Y'); ?></option>
                </select>
            </div>

            <div class="col-md-6 yearlyDate" style="margin-top: 15px;display: none;">
              <select  style="min-width:100%;width:100%;" class="form-control select2" name="yearStartDate">
                  <option <?php if((date('Y') - 1) == "2019"){ echo "selected"; } ?> value="2019">2019</option>
                  <option <?php if((date('Y') - 1) == "2020"){ echo "selected"; } ?> value="2020">2020</option>
                  <option <?php if((date('Y') - 1) == "2021"){ echo "selected"; } ?> value="2021">2021</option>
                  <option <?php if((date('Y') - 1) == "2022"){ echo "selected"; } ?> value="2022">2022</option>
                  <option <?php if((date('Y') - 1) == "2023"){ echo "selected"; } ?> value="2023">2023</option>
                  <option <?php if((date('Y') - 1) == "2024"){ echo "selected"; } ?> value="2024">2024</option>
                  <option <?php if((date('Y') - 1) == "2025"){ echo "selected"; } ?> value="2025">2025</option>
                  <option <?php if((date('Y') - 1) == "2026"){ echo "selected"; } ?> value="2026">2026</option>
                  <option <?php if((date('Y') - 1) == "2027"){ echo "selected"; } ?> value="2027">2027</option>
                  <option <?php if((date('Y') - 1) == "2028"){ echo "selected"; } ?> value="2028">2028</option>
                  <option <?php if((date('Y') - 1) == "2029"){ echo "selected"; } ?> value="2029">2029</option>
                  <option <?php if((date('Y') - 1) == "2030"){ echo "selected"; } ?> value="2030">2030</option>
              </select>
            </div>

            <div class="col-md-6 yearlyDate" style="margin-top: 15px;display: none;">
              <select  style="min-width:100%;width:100%;" class="form-control select2" name="yearEndDate">
                  <option <?php if(date('Y') == "2019"){ echo "selected"; } ?> value="2019">2019</option>
                  <option <?php if(date('Y') == "2020"){ echo "selected"; } ?> value="2020">2020</option>
                  <option <?php if(date('Y') == "2021"){ echo "selected"; } ?> value="2021">2021</option>
                  <option <?php if(date('Y') == "2022"){ echo "selected"; } ?> value="2022">2022</option>
                  <option <?php if(date('Y') == "2023"){ echo "selected"; } ?> value="2023">2023</option>
                  <option <?php if(date('Y') == "2024"){ echo "selected"; } ?> value="2024">2024</option>
                  <option <?php if(date('Y') == "2025"){ echo "selected"; } ?> value="2025">2025</option>
                  <option <?php if(date('Y') == "2026"){ echo "selected"; } ?> value="2026">2026</option>
                  <option <?php if(date('Y') == "2027"){ echo "selected"; } ?> value="2027">2027</option>
                  <option <?php if(date('Y') == "2028"){ echo "selected"; } ?> value="2028">2028</option>
                  <option <?php if(date('Y') == "2029"){ echo "selected"; } ?> value="2029">2029</option>
                  <option <?php if(date('Y') == "2030"){ echo "selected"; } ?> value="2030">2030</option>
              </select>
            </div>
          </div>
          <div class="row" style="margin-left: 0px;margin-right: 0px;">
            <div class="col-md-12">
              <br>
              <center>
                <button type="submit" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 15px;" class="btn btn-primary m-r-5" id="add_meeting_notes_submit" ><i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo ADDNOTE; ?></button>
              </center>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

<div class="modal fade" id="modal-delete-meeting-notes" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="max-height: 220px;max-width: 100%;">
      <div class="modal-header" style="background-color: #002060;">
          <h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Delete; ?> </h4>
          <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
            <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
          </button>
        </div> 
        <div class="modal-body">
          <form class="p-b-20" action="delete_meeting_notes_form" method="POST" id="delete_meeting_notes_form" >
            <input type="hidden" name="deleteNoteId" id="deleteNoteId" /> 
            <div class="modal-footer" style="border-top: none;padding-top: 0;" >
              <div class="row">
                <div class="col-md-12" style="margin-top: 10px;" >
                  <center>
                      <img style="margin-top:-8%;width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
                  </center>
                </div>
                <br>
                <div id="" class="m-b-10 alert alert-success fade hide" >
                </div>
                <div class="col-md-12" style="margin-top: -5px;">
                  <center>
                    <?php echo Areyousureyouwanttodeletenote; ?>
                  </center>
                </div>
                  <div class="col-md-12">
                    <center>
                      <br>
                        <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_meeting_notes_submit"><?php echo Delete; ?></button>
                      <br>
                    </center>
                  </div>
                </div>          
            </div>
        </form>
      </div>
    </div>
  </div>
</div>