<style type="text/css">
  .table>tbody>tr>td, 
  .table>tbody>tr>th, 
  .table>tfoot>tr>td, 
  .table>tfoot>tr>th, 
  .table>thead>tr>td, 
  .table>thead>tr>th 
  {
     border: none;
  }

  .table>tbody>tr
  {
    color: #002060 !important;
    font-weight: 600 !important;
    cursor: pointer;
  } 

  .table thead th, .table>thead>tr>th 
  {
      color: #1f2225;
      font-weight: 400;
      border: none !important;
  }
  
  .last-column
  {
    
  }

  .paginate_button, .paginate_input
  {
    border: none !important;
    width: 7%;
  }

  #empTable_wrapper .col-sm-5 
  {
    flex: 0 0 27.666667% !important;
      max-width: 27.666667% !important;
  }

  #empTable_wrapper .col-sm-7 
  {
    flex: 0 0 28.333333% !important;
      max-width: 28.333333% !important;
      text-align: center !important;
      margin-top: 10px;
  }

  table.dataTable thead .sorting_asc:after 
  {
    display: none !important;
  }


.datepicker.dropdown-menu 
{
    min-width: 100px !important;
}

.dropdown-menu.show
{
    max-height: 250px;
    overflow: auto;
}
.datepicker.datepicker-dropdown 
{
    width: 207px !important;
}

.datepicker 
{
    min-width: 207px!important;
}

.datepicker,
.table-condensed {
  width: 200px !important;
  height:200px !important;
}

.datepicker table tr td, 
.datepicker table tr th 
{
  padding-left: 0px !important;
    padding-right: 0px !important;
}


.table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th {
    padding: 0px 0px !important;
}

.datepicker table tr td, .datepicker table tr th 
{
  width: 0px !important;
    height: 19px !important;
    border-radius: 15px !important;
}


.datepicker table tr td span.active.active, .datepicker table tr td.active.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active:hover:hover, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover, .open .dropdown-toggle.datepicker table tr td.active:hover 
{
  background: #FF8000!important;
}

.datepicker .prev {
  box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .prev:before {
  color: #FF8000 !important;
}
.datepicker .next {
  box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .next:before {
  color: #FF8000 !important;
}

#empTable_paginate
{
  border-radius: 15px !important;
  padding-left: 0px !important;
  box-shadow: grey 0px 0px 8px -2px !important;
}





input[type='radio'] {
  -webkit-appearance:none;
  width:13px;
  height:13px;
  border:1px solid black;
  border-radius:50%;
  outline:none;
}

input[type='radio']:hover {
  box-shadow:0 0 5px 0px #FF8000 inset;
}

input[type='radio']:before {
  content:'';
  display:block;
  width:60%;
  height:60%;
  margin: 20% auto;    
  border-radius:50%;    
}
input[type='radio']:checked:before {
    width: 15px !important;
    height: 15px !important;
    margin-top: -2px !important;
    margin-left: -2px !important;
    background-image: url(<?php echo base_url("assets/img/tick_orange.svg"); ?>);
}


input[type='checkbox'] {
  -webkit-appearance:none;
  width:13px;
  height:13px;
  border:1px solid #002060;
  border-radius:50%;
  outline:none;
}



input[type='checkbox']:before {
  content:'';
  display:block;
  width:60%;
  height:60%;
  margin: 20% auto;    
  border-radius:50%;    
}
input[type='checkbox']:checked:before {
    width: 15px !important;
    height: 15px !important;
    margin-top: -2px !important;
    margin-left: -2px !important;
    background-image: url(<?php echo base_url("assets/img/tick_orange.svg"); ?>);
}


#testetst
{
  color: #FF8000 !important;
}

.garyColor::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #b8b0b0;
  opacity: 1; /* Firefox */
}

.garyColor:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #b8b0b0;
}

.garyColor::-ms-input-placeholder { /* Microsoft Edge */
  color: #b8b0b0;
}

.orangColor::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #FF8000;
  opacity: 1; /* Firefox */
}

.orangColor:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #FF8000;
}

.orangColor::-ms-input-placeholder { /* Microsoft Edge */
  color: #FF8000;
}

.form-check-inline ~ label {
  color: blue !important;
}


svg
  {
    position: absolute;
    width:150px;
    height:150px;
    z-index: 1000px;
  }
  svg circle
  {
    width:100%;
    height:100%;
    fill:none;
    stroke:#d01c1c;
    stroke-width:8;
    stroke-linecap:8;
    transform: translate(5px,5px);

    -webkit-transform: translate(5px,5px);
  }
  svg circle:nth-child(2)
  {
    stroke-dasharray: 440;
    stroke-dashoffset:440;
  }


  <?php $allPer =   ($totalCompletedTask / $totalTask) * 100; 
  if (!empty($allPer)) { ?>
    
  .cardall:nth-child(1) svg circle:nth-child(2)
  {
    stroke-dashoffset:<?php echo 440 - (310 * $allPer) / 100; ?>;
      stroke-linecap: round;
      stroke: #FF8000;
  }
  <?php } ?>

  <?php foreach ($machines as $key => $value) { 
    $machinePer =   ($value['totalCompletedTask'] / $value['totalTask']) * 100;
    if (!empty($machinePer)) {
  ?>
    
  .card<?php echo $value['machineId']; ?>:nth-child(1) svg circle:nth-child(2)
  {
    stroke-dashoffset:<?php echo 440 - (310 * $machinePer) / 100; ?>;
      stroke-linecap: round;
      stroke: #FF8000;
  }
  <?php } } ?>

  .border-left-right-top-hide
  {
    border-left: none;
    border-top: none;
    border-right: none;
    border-radius: 0;
  }

  input:focus,
  select:focus,
  textarea:focus,
  .form-control:focus,
  button:focus {
      outline: none;
  }

  .form-control.focus, .form-control.input-white.focus, .form-control.input-white:focus, .form-control:focus
  {
    box-shadow : none;
    border-color: #bec6ce;
  }
</style>

<style type="text/css">
  . .col-md-3{
  display: inline-block;
  margin-left:-4px;
}
/*.col-md-3 img{
  width:100%;
  height:auto;
}*/
body .carousel-indicators li{
  background-color:red;
}
body .carousel-indicators{
  bottom: 0;
}
body .carousel-control-prev-icon,
body .carousel-control-next-icon{
  background-color:red;
}
body .no-padding{
  padding-left: 15px;
  padding-right: 0;
   }


.carousel-item-next, .carousel-item-prev, .carousel-item.active {
    display: block !important;
}

.carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.users-carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.users-carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}
.boxShadow
{
  box-shadow: grey 0px 0px 8px -2px !important;
}
@media (max-width:400px)
 {
  .deletemodaltask
  {
    width: 300px;
    height: 235px;
  }
 
 }

 .form-check-input:checked + .form-check-label 
 {
  color: #FF8000;
}
@media screen and (min-device-width:320px)and (max-width:375px) 
    {
      .col-md-3 
      {
        margin-top:-332px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
    @media screen and (min-device-width:376px)and (max-width:424px) 
    {
      .col-md-3 
      {
        margin-top:-332px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
    @media screen and (min-device-width:425px)and (max-width:575px) 
    {
      .col-md-3 
      {
        margin-top:-300px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
     @media screen and (min-device-width:576px)and (max-width:767px) 
    {
      .col-md-3 
      {
        margin-top:-494px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }

    @media screen and (min-device-width:768px)and (max-width:1023px) 
    {
      .col-md-3 
      {
        margin-top:-637px!important;
        width: 100%!important;
      }
    }




</style>

<!-- <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>   -->

<div id="content" class="content">
  
  <h1 style="font-size: 22px;color: #002060" class="page-header">Tasks & maintenance</h1>

  <div class="row">
  <input type="hidden" name="task_id"  value="0">

    <div class="col-md-12 table_data" style="padding-left:14px;">



      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >

      <div style="padding: 10px 0px 0 0px;overflow-x: auto;" id="showStatus" ></div>
      <div style="padding: 10px 0px 0 0px;overflow-x: auto;" id="showDate" ></div>

        <div class="panel-body">
          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Task Id<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small></th> 


                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Task Name<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>


                  <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Task Description<br>
                    <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                  </th> 

                  <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                    <div id="filterstatusSelected" class="dropdown" style="padding: 1px;border-radius: 5px;min-width: 70px;padding-bottom: 5px!important;" >
                      <span class="dropdown-toggle" data-toggle="dropdown"> Status&nbsp;</span>
                      <div class="dropdown-menu">
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterstatus()" value="'1'" class="form-check-input filterstatus" type="checkbox" id="filterstatusCompleted">
                            <label class="form-check-label" for="filterstatusCompleted">
                              Completed
                            </label>
                          </div>
                        </div>

                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterstatus()" value="'0'" class="form-check-input filterstatus" type="checkbox" id="filterstatusUncompleted">
                            <label class="form-check-label" for="filterstatusUncompleted">
                              Uncompleted
                            </label>
                          </div>
                        </div>
                    </div>
                    </div>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                   
                </th>



                  <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterIsDeleteSelected" class="dropdown" style="padding: 1px;border-radius: 5px;min-width: 70px;padding-bottom: 5px!important;" >
                    <span class="dropdown-toggle" data-toggle="dropdown"> isDeleted&nbsp;</span>
                    <div class="dropdown-menu">
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterIsDelete()" value="'1'" class="form-check-input filterIsDelete" type="checkbox" id="filterIsDeleteCompleted">
                          <label class="form-check-label" for="filterIsDeleteCompleted">
                            Deleted record
                          </label>
                        </div>
                      </div>

                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterIsDelete()" value="'0'" class="form-check-input filterIsDelete" type="checkbox" id="filterIsDeleteUncompleted">
                          <label class="form-check-label" for="filterIsDeleteUncompleted">
                            Existng record
                          </label>
                        </div>
                      </div>

                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
              
                <!--<th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Created Date<br>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small></th>  -->

                <th  style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;" >
                    <input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-29 day")); ?>" />  
                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />   
                  <div id="advance-daterange" name="advance-daterange" style="width: 87px;padding-bottom: 5px!important">
                    <span>
                        Created Date&nbsp;&nbsp;
                    </span> 
                   <i class="fa fa-caret-down m-t-2"></i>
                  </div>
                  <small  style="color: #FF8000;text-align : center;" >
                    &nbsp;
                  </small>
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filtertypeselected" class="dropdown" style="padding: 1px;border-radius: 5px;min-width: 70px;padding-bottom: 5px!important;" >
                    <span class="dropdown-toggle" data-toggle="dropdown"> Type&nbsp;</span>
                    <div class="dropdown-menu">
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filtertype()" value="'1'" class="form-check-input filtertype" type="checkbox" id="filterTypeUncompleted">
                          <label class="form-check-label" for="filterTypeUncompleted">
                            Active
                          </label>
                        </div>
                      </div>

                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filtertype()" value="'0'" class="form-check-input filtertype" type="checkbox" id="filterTypeUncompleted">
                          <label class="form-check-label" for="filterTypeUncompleted">
                            In-Active
                          </label>
                        </div>
                      </div>

                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
              </tr>
            </thead>
          </table>
        </div>

      </div>
    </div> 
  </div>
<hr style="background: gray;">
<p>@2021 nytt | All Rights Reserved</p>
</div>


<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
  


<div class="modal fade" id="modal-add-task" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">

      <div class="modal-header" style="background-color: #002060;">
            <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title machine_name" id="liveMachineName">Add new task</h4>
            <button type="button" class="close" style="padding: 12px 32px !important;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
        </div>
        <style type="text/css">
          input#task {
            background-image: url("<?php echo base_url('assets/nav_bar/machineIcon_black.png'); ?>");
            background-repeat: no-repeat;
            text-indent: 20px;
        }

        .select2-container--default .select2-selection--single 
        {
            box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;
            border: none !important;
        }

        .select2-container--default .select2-selection--multiple
        {
            box-shadow: 2px 2px 6px rgba(133, 159, 172, 0.41) !important;
            border: none !important;
        }

        </style>
      <!--     <link rel="stylesheet" href="//code.jquery.com/ui/1.12.1/themes/base/jquery-ui.css">
          <link rel="stylesheet" href="/resources/demos/style.css"> -->
      
      
      <div class="modal-body">

        <form class="p-b-20" action="add_task" method="POST" id="add_task_form" style="margin-left: 10px;margin-right: 10px;">
          <div class="row" style="margin-left: 0px;margin-right: 0px;">

            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
              <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="task_name" name="task_name" 
              placeholder="Task Name" > 
            </div>

            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
              <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="task_description" name="task_description" 
              placeholder="Task Description" > 
            </div>

            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
              <input type="text" name="created_date" placeholder="select date" id="datepicker">
            </div>

            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
             <select name="type" id="type" class="form-control">
                <option value="1">Active</option>
                <option value="0">InActive</option>
                </select>
              </div>
            </div>



          <!-- 
            <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control col-md-12" id="datepicker" name="created_date" placeholder="" >  -->

          <!--   <p>Date: <input type="text" name="created_date" id="datepicker"></p> -->
        
          <div class="row" style="margin-left: 0px;margin-right: 0px;">
            <div class="col-md-12">
              <br>
              <center>
                <button type="submit" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 15px;" class="btn btn-primary m-r-5" id="add_task_submit" ><i class="fa fa-plus" style="font-size: 15px;"></i> Add task</button>
              </center>
            </div>
          </div>
        </form>
      </div>
      
    </div>
  </div>
</div>

<input type="hidden" id="isDelete" value="0">

<div class="modal fade" id="modal-delete-task<?php echo $taskMaintenace->taskId; ?>" style="display: none;" aria-hidden="true">

  <div class="modal-dialog">
      <div class="modal-content deletemodaltask" style="max-height: 220px;max-width: 100%;">
        <div class="modal-header" style="background-color: #002060;">
           <input type="hidden" name="taskId" id="taskId<?php echo $taskMaintenace->taskId; ?>" value="<?php echo $taskMaintenace->taskId; ?>" /> 
            <h4 style="color: #FF8000;padding: 4px;" class="modal-title">Delete </h4>
            <button type="button" class="close" data-dismiss="modal" style="">
              <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
            </button>
          </div> 
         <div class="modal-body">
           <form class="p-b-20" action="delete_task_form" method="POST" id="delete_task_form" >
                  <input type="hidden" name="deleteTaskId" id="deleteTaskId" /> 
                  <div class="modal-footer" style="border-top: none;padding-top: 0;" >
                    <div class="row">
                        <div class="col-md-12" style="margin-top: 10px;" >
                          <center>
                            <img style="margin-top:-8%;width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
                          </center>
                        </div>
                        <br>
                        <div id="" class="m-b-10 alert alert-success fade hide" ></div>
                      <div class="col-md-12" style="margin-top: -5px;">
                        <center>
                          Are you sure you want to delete Task ?
                        </center>
                      </div>
                        <div class="col-md-12">
                          <center>
                            <br>
                              <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_task_submit">Delete</button>
                            <br>
                          </center>
                        </div>
                      </div>          
                  </div>
          </form>
        </div>
      </div>
    </div>
  </div>




   <div class="col-md-3 detail" style="margin-top: -873px;margin-bottom: -63px;background: #FFFFFF !important;border-bottom-left-radius: 5px;    border-bottom-right-radius: 5px;box-shadow: gray -10px 10px 10px -10px;display: none;height: 100%;position: fixed;right: 0%;width: 23%;overflow: auto;">
       <div class="panel panel-inverse panel-primary" >
        <div class="panel-body" style="padding: 0;">
            
            <div class="row" style="padding: 8px;padding-top: 0 !important;background: #002060;height: 164px; border-top-left-radius: 5px;    border-top-right-radius: 5px;margin-bottom: 30px;">
              <div class="col-md-12" style="height: 47px !important;">
                <div style="padding: 14px 0 0 0;">
                  <a href="javascript:void(0);" onclick="closeDetails();">
                    <img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
                </div>
              </div>
              <div class="col-md-12" >
               

              <!--   <center><img id="userImage" style="border-radius: 50%;" height="32" width="32" src="<?php echo base_url('assets/img/user/2.jpg'); ?>"></center>  -->
                <center><span style="color: #9c9aab;"></span> <span style="color: white;" id="userName"></span> </center>
                <center><small style="color: #FF8000;" id="created_date"></small></center>
              </div>
            </div>

            
            <form class="p-b-20" action="update_task_form" method="POST" id="update_task_form" >
              <!-- <input type="hidden" id="taskId" name="taskId"> -->
              <div class="row" style="padding-left: 15px;padding-right: 15px;">
                <div class="col-md-12">
                  <small>Task Id</small>
                  <input type="text" id="task_id" name="task_id"></input>
                  <hr style="height: 2px;margin-top: 0rem;">
                  
                  <!-- <input type="hidden" id="task_id" name="taskId"> -->
                  <small>Task Name</small>
                  <input type="text" id="task_nameDed" name="task_name"></input>
                  <hr style="height: 2px;margin-top: 0rem;">
                  
                  <small>Task Description</small>
                  <input type="text" id="task_descriptionDed" name="task_description"></input>
                  <hr style="height: 2px;margin-top: 0rem;">

                  <small>Type</small>
                  <label class="checkbox-inline"><input type="radio" value="1" name="type">Active</label>
                  <label class="checkbox-inline"><input type="radio" value="0" name="type">In-Active</label>

                  <br>
                  <br>
                   <select name="typeValue" id="typeValue">
                    <option value="1">Active</option>
                    <option value="0">In-Active</option>
                  </select>

                  <br>
                  <br>
                  <small>Status</small>
                  <label class="checkbox-inline"><input type="radio" value="1" name="status">Completed</label>
                  <label class="checkbox-inline"><input type="radio" value="0" name="status">Uncompleted</label>

                  <br>
                  <br>
                   <select name="statusValue" id="statusValue">
                    <option value="1">Completed</option>
                    <option value="0">Uncompleted</option>
                  </select>




                 
                </div>
              </div>
            </form>

          </div>
      </div>
    </div>
  </div>