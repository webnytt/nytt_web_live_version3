<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8" />
	<title>NYTT <?php echo dashboard; ?> | <?php echo Login; ?></title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/font-awesome/5.3/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/transparent/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/transparent/style-responsive.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/added-style.css" rel="stylesheet" />
	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/png" sizes="16x16">
	
	<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script>
</head>
<body class="pace-top bg-black-darker" style="background-color: #002060 !important;">
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<div id="page-container" class="fade">
		<div class="login login-with-news-feed">
			<div class="news-feed">
				<div class="news-image" style="background-image: url(<?php echo base_url(); ?>assets/img/login-bg/bg1.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title" style="color: #FF8000 !important"><div class="widget-img widget-img-md m-r-5 m-t-5" style="width: 119px;height: 43px;background-image: url(<?php echo base_url(); ?>assets/img/app_logo_orange.png);float:left;"></div> <?php echo dashboard; ?></h4>
					<p>
						<?php echo Simpleandeasysolutiontounderstandandimproveyourmachineutilization; ?>.
					</p>
				</div>
			</div>
			<div class="right-content">
				<div class="login-header">
					<div class="brand" style="color: #FF8000 !important">
						<div class="widget-img widget-img-md" style="width: 119px;height: 43px;background-image: url(<?php echo base_url(); ?>assets/img/app_logo_orange.png);float:left;"></div> <?php echo dashboard; ?>
						<small><?php echo Trackyourmachines; ?></small>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<div class="login-content">
					<?php ?>
					<form action="<?php echo base_url(); ?>admin/process" method="post" class="margin-bottom-0">
						<div class="form-group m-b-15">
							<input type="text" class="form-control form-control-lg" name="username" id="username" placeholder="<?php echo Username; ?>" required />
						</div>
						<div class="form-group m-b-15">
							<input type="password" class="form-control form-control-lg" name="password" id="password" placeholder="<?php echo Password; ?>" required />
						</div>
						<div class="login-buttons">
							<button type="submit" class="btn btn-primary btn-block btn-lg"  style="background: #FF8000;">
								<?php echo Signmein; ?></button>
						</div>
						<?php ?> 
						<hr />
						<p class="text-center">
							<a style="color: rgba(255,255,255,.6);" href="<?php echo base_url('admin/forgot_password'); ?>"> <?php echo Forgotpassword; ?></a>
						</p>

						<p class="text-center">
							&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?>
						</p>
					</form>
				</div>
			</div>
		</div>
	<?php  ?>
	</div>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/theme/transparent.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<script> COLOR_BLACK = 'rgba(255,255,255,0.75)'; </script>
	<script src="<?php echo base_url(); ?>assets/plugins/chart-js/Chart.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script> 
	<script>
		$(document).ready(function() {
			App.init();
		});
	</script>
	<?php if($this->session->flashdata('error_message') != '' ||  $this->session->flashdata('success_message') != '') { ?>
	<script>
		$(document).ready(function() {
			<?php if($this->session->flashdata('error_message') != '') { ?> var titleG = '<?php echo Error; ?>'; <?php } ?>
			<?php if($this->session->flashdata('success_message') != '') { ?> var titleG = '<?php echo Success; ?>'; <?php } ?>
			$.gritter.add({
				title: '<?php echo Error; ?>',
				text: '<?php echo $this->session->flashdata("error_message"); ?> '
			});
		}); 
	</script>
	<?php } ?> 
</body>
</html>
