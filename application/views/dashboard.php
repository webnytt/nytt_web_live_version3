<link rel="stylesheet" href="<?php echo base_url('assets/scroller_timepicker/picker.css'); ?>">
<link href="<?php echo base_url('assets/css/dashboard5.css');?>" rel="stylesheet" /> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  

<style>
    .btn-primary.active, .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .btn-primary:focus, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .open > .dropdown-toggle.btn-primary, .open > .dropdown-toggle.btn-primary:focus, .open > .dropdown-toggle.btn-primary:hover, .show > .btn-primary.dropdown-toggle
{
    background-color: #FF8000 !important;
    border-color: #FF8000 !important;
}
   
@media screen and (min-device-width: 320px) and (max-width: 768px) 
{
    .margintopSmall
    {
        margin-top: 0px!important;
    }
}
@media screen and (min-device-width: 768px) and (max-width: 999px) 
{
    .minwidthcolmdStyle
    {
    	min-width: 50%!important;   
    }
    .minwidthcolmdStylee
    {
    	min-width: 30%!important;   
    }
    .minwidthcolmdStyleeshowby
    {
    	min-width: 15%!important;
    	margin-top: 10px;   
    }
    .minwidthCOLMDTODAY
    {
    	min-width: 45%!important;
    	margin-top: 10px;
    }
    .minwidthshiftsStyle
    {
    	min-width: 12%!important;
    	margin-top: 10px;
    }
    .machineviewShiftsStyle
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    }
    .machineWeeklyStylees
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    } 

    .machineYearlyStylees
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    }
}
       @media screen and (min-device-width: 768px) and (max-width: 999px) 
        {
            .colmd4chartsstyle
            {
                min-width: 100%!important;
            }
        }
        @media screen and (min-device-width: 1000px) and (max-width: 1320px) 
        {
            .colmd4chartsstyle
            {
                min-width: 50%!important;
            }
        }

[data-tooltip] {
  position: relative;
  z-index: 1300;
  cursor: pointer;
}

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
    position: relative;
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
  position: absolute;
  /*bottom: 150%;*/
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 5px;
  min-width: 175px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
  z-index: 1;
  /*background-color: #FF9999;*/
/*  display: none;
  position: fixed;
*/  transform:translate(50px,-50px);
  /*left: 100%;
  top: 0%;*/
  /*white-space: nowrap;*/
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
  display: inline-block;
}
.table td, .table th
{
    border-top: 0px!important;
}
.slick-prev
{
    box-shadow: grey 0 0 8px -2px!important;
}
.slick-next
{
    box-shadow: grey 0 0 8px -2px!important;
}

.select2-container
{
    /*display: block!important;*/
    height: 34px;
}
.select2-container--default .select2-selection--single
{
    border: 0px;
    border: none;
    box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;
    border-radius: 5px;
}
@media screen and (min-device-width: 320px) and (max-width: 374px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
} 
@media screen and (min-device-width: 375px) and (max-width: 424px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
}
@media screen and (min-device-width: 425px) and (max-width: 767px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 100%;
    }  
}
@media screen and (min-device-width: 768px) and (max-width: 1023px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container
    {
        min-width: 90%!important;
        width: 9    0%!important;
    }
    .oppgapstyle
    {
        min-width: 130px!important;
    }

}
@media screen and (min-device-width: 1320px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
    }  
    .select2-container
    {
        min-width: 90%!important;
        /*width: 90%!important;*/
    }
}
</style>

<div id="content" class="content">
    

        <h1 style="" class="page-header h1headerpagestyle"><?php echo Analytics; ?></h1>
    <br />

    <div class="row">
        <div class="col-md-3 minwidthcolmdStyle" style="display:flex;padding-left: 0px;max-width:33%!important;">
            <?php if ($accessPointStopView == true) { ?>

            <form style="float: right;"action="<?php echo base_url('Analytics/breakdown'); ?>"method="post">  

                <input type="hidden" name="partsChoose1" id="partsChooseStop1"> 
                <input type="hidden" name="partsChoose2" id="partsChooseStop2"> 
                <input type="hidden" name="machineId" id="machineIdStop" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $machineNameList[0]['machineId']; } ?>"> 
                <button class="btn btn-danger stopAnalyticsButtonStyle" type="submit"><?php echo Stopanalytics ;?></button>
            </form>
            
            <?php } ?>
            <?php if ($accessPointWaitView == true) { ?>
            <form style="float: right;"action="<?php echo base_url('Analytics/waitBreakdown'); ?>"method="post">  

                <input type="hidden" name="partsChoose1" id="partsChooseWait1"> 
                <input type="hidden" name="partsChoose2" id="partsChooseWait2"> 
                <input type="hidden" name="machineId" id="machineIdWait" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $machineNameList[0]['machineId']; } ?>"> 
                    <button style="background-color: #FFCF00 !important;border-color: #FFCF00 !important;" href="<?php echo base_url('Analytics/waitBreakdown'); ?>"class="btn btn-primary stopAnalyticsButtonStyle"><?php echo Waitanalytics; ?></button>
            </form>
            <?php } ?>
        </div>

        <div class="col-md-2 minwidthcolmdStylee" style="padding-left: 5px;padding-right: 0px;">
            <select name="showcalender" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                <option class="new_test" <?php echo ($_POST['mainChoose1'] == "day") ? "selected" : ""; ?> value="day"><?php echo Day; ?></option>
                <option class="new_test" <?php echo ($_POST['mainChoose1'] == "weekly") ? "selected" : ""; ?> value="weekly"><?php echo week; ?></option>
                <option class="new_test" <?php echo ($_POST['mainChoose1'] == "monthly") ? "selected" : ""; ?> value="monthly"><span> <?php echo MONTH; ?> </span></option>
                <option class="new_test" <?php echo ($_POST['mainChoose1'] == "yearly") ? "selected" : ""; ?> value="yearly"><span> <?php echo year; ?> </span></option>
            </select>
        </div>

        <div class="col-md-2 day colmd2day minwidthCOLMDTODAY yearwidthstyle" style="padding-left: 10px!important;max-width: 23% !important;flex: 0 0 23% !important;">
            <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:70%!important;margin-right:0%!important;height: 34px!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;float: left!important;" value="<?php if($_POST['mainChoose1'] == "day") { echo $_POST['mainChoose2']; }else{  echo date('m/d/Y'); } ?>">
        </div>

         <?php 
            if($_POST['mainChoose1'] == "weekly") {
            $mainChoose2  = explode("/", $_POST['mainChoose2']);
              $weekPost = $mainChoose2[0];
            }else
            {
                $weekPost = "";
            }
         ?>

        <div class="col-md-2 weekly machineWeeklyStylees yearwidthstyle" style="display: none;max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="Weekly" style="display: block!important;min-width:200px!important;" class="form-control margintopstyle" id="choose3" onchange="changeFilterValue()">
                <?php for ($iS=1; $iS < 53; $iS++) {  ?>
                    <option  <?php if($weekPost == $iS){ echo "selected"; }else if(date('W') == $iS && empty($weekPost)) { echo "selected"; } ?> value="<?php echo $iS; ?>"><?php echo week.' '.$iS; ?></option>
                <?php } ?>
            </select>
        </div>

        <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
        
        <div class="col-md-2 monthly colmdmonthlyStyle" style="display: none;padding-left: 10px!important;">
            <input onchange="changeFilterValue()" type="text" style="height:34px!important;width:80%!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;float: left!important;" class="form-control datepicker1 yearwidthstyle" id="choose4" value="<?php if($_POST['mainChoose1'] == "monthly") { echo $_POST['mainChoose2']; }else{ echo date('M Y'); } ?>">
        </div>

        <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
        <div class="col-md-2 yearly machineYearlyStylees yearwidthstyle" style="display: none; max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="showcalenderyear" class="form-control" style="border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" id="choose5" onchange="changeFilterValue()">
                <?php for($i=0 ; $i < $years; $i++){ 
                    $yearValue = 2018 + $i;
                    ?>
                    <option <?php if($_POST['mainChoose2'] == $yearValue && $_POST['mainChoose1'] == "yearly") { echo "selected"; } else if ($yearValue == date('Y') && $_POST['mainChoose1'] != "yearly") {
                                echo "selected";
                            } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                <?php } ?>
            </select>
        </div>

        <!-- <div class="col-md-1 yearly" style="display: none;"></div> -->

        <div class="col-md-1 machineView minwidthshiftsStyle"><span class="viewddLabelstyle"><?php if ($accessPointViewView == true) { ?> <?php echo shifts; ?> : <?php } ?> </span></div>

        <div class="col-md-2 machineView machineviewShiftsStyle">
            <input type="hidden" id="viewId" value="none">
            <?php if ($accessPointViewView == true) { ?>
                <div class="dropdown viewdropdownstyle" id="viewFilter">
                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <span style="padding-left: 8px;" id="selectedView">24 <?php echo Hours; ?> </span>
                    </div>
                    <div class="dropdown-menu dropdownMenustyle">
                        <div class="dropdown-item selectedView" id="viewIdnone" onclick="selectView('none','24 <?php echo Hours; ?>')" style="font-weight: 600;">
                            24 <?php echo Hours; ?> 
                        <hr class="dropdownHRstyle">
                        </div>

                        <?php foreach ($machineView as $key => $value) { ?>
                            <div class="dropdown-item" id="viewId<?php echo $value['viewId']; ?>" style="font-weight: 600;">
                                <span class="machineNameText" style="width:80%;" onclick="selectView(<?php echo $value['viewId']; ?>,'<?php echo $value['viewName']; ?>')">
                                    <?php echo $value['viewName']; ?>
                                </span> 
                                <?php if ($accessPointViewsEdit == true) { ?>
                                    <span style="float: right;">
                                        <img onclick="editView(<?php echo $value['viewId']; ?>)" style="width: 20px;" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                    </span> 
                                <?php } ?>
                                <hr class="dropdownHRstyle">
                            </div>

                        <?php } ?>

                        <?php if ($accessPointViewsEdit == true) { ?>
                            <div class="dropdown-item dropdownItemstyle">
                                <a data-toggle="modal" data-target="#add-view-modal">+ <?php echo Addnewshift; ?></a> 
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php $machineNameList = $listMachines->result_array(); ?>

    <div class="row">
        <!-- <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>"> -->

        <section class="regular slider">
           <?php foreach($machineNameList as $key => $machine) { 



              if($machine['machineName'] != "Parts_1035" && $machine['machineName'] != "Parts_1832" && $machine['machineName'] != "Parts_1821"){ ?>
            <?php if($machine['machineId'] == $_POST['machineId']){ ?>
            <div>
                <button type="button" class="btn btn-default btn-lg filter boxShadow machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }  if(!$_POST && $key == 0) { echo "active";} ?>" id="machine<?php echo $machine['machineId']; ?>"> 

                    <?php 
                        if($machine['machineName'] == "Status_1035"  && $this->factoryId == 2) {
                            echo "IO- Robot 1035";
                        }else if ($machine['machineName'] == "Status_1832"  && $this->factoryId == 2) {
                            echo "IO- Robot 1832";
                        }else if ($machine['machineName'] == "Status_1821"  && $this->factoryId == 2) {
                            echo "IO- Robot 1821";
                        }else {
                            echo $machine['machineName'];
                        } 
                    ?> 
                </button>
            </div>
            <?php } } } ?>
            <?php foreach($machineNameList as $key => $machine) { 
                if($machine['machineName'] != "Parts_1035" && $machine['machineName'] != "Parts_1832" && $machine['machineName'] != "Parts_1821"){ ?>
            <?php if($machine['machineId'] != $_POST['machineId']){ 


                ?>
            <div>
                <button type="button" class="btn btn-default btn-lg filter boxShadow machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }  if(!$_POST && $key == 0) { echo "active";} ?>" id="machine<?php echo $machine['machineId']; ?>"> 
                    <?php 
                        if($machine['machineName'] == "Status_1035"  && $this->factoryId == 2) {
                            echo "IO- Robot 1035";
                        }else if ($machine['machineName'] == "Status_1832"  && $this->factoryId == 2) {
                            echo "IO- Robot 1832";
                        }else if ($machine['machineName'] == "Status_1821"  && $this->factoryId == 2) {
                            echo "IO- Robot 1821";
                        }else {
                            echo $machine['machineName'];
                        } 
                    ?> 
                </button>
            </div>
            <?php } } } ?>
        </section>
    </div>
    
    <div class="row" id="chartwrap" class="machineModerowMainStyle">
        

        <div class="col-md-4 colmd4chartsstyle colomd4machineModeStyle">
            <div class="tab-content boxShadow machineModeStyle">
                <div class="tab-pane fade active show" >
                    <div class="row m-l-0 m-r-0">


                        <div class="col-md-12">
                            <h4 class="h4machineHeadcolor">
                                <img src="<?php echo base_url('assets/nav_bar/production-time.svg'); ?>">
                                <?php echo Productiontime; ?> 
                                <span  id="ActualProductionText" style="color: #002060 !important;float: right;font-size: 9px;margin-top: 7px;"></span>
                            </h4>
                        </div>

                        <div class="col-md-12" style="margin-top: 40px;">
                            <span ></span>
                           <div class="actualProductionRunning" data-tooltip="" data-placement="bottom" data-html="true" style="background-color: #76BA1B;height: 10px;float: left;"></div>
                           <div class="actualProductionWaiting" data-tooltip="" style="background-color: #FFCF00;height: 10px;float: left;"></div>
                           <div class="actualProductionStopped" data-tooltip="" style="background-color: #F60100;height: 10px;float: left;"></div>
                           <div class="actualProductionOff" data-tooltip="" style="background-color: #CCD2DF;height: 10px;float: left;"></div>
                           <div class="actualProductionNodet" data-tooltip="" style="background-color: #000000;height: 10px;float: left;"></div>
                           <div class="actualProductionNoStacklightText" data-tooltip="" style="background-color: #002060;height: 10px;float: left;"></div>
                        </div>

                        <div class="col-md-12" style="margin-top: 10px;">
                            <div class="actualProductionRunning" style="float: left;">
                                <center><span id="actualProductionRunningText"></span></center>
                            </div>
                            <div class="actualProductionWaiting" style="float: left;">
                                <center><span id="actualProductionWaitingText"></span></center>
                            </div>
                            <div class="actualProductionStopped" style="float: left;">
                                <center><span id="actualProductionStoppedText"></span></center>
                            </div>
                            <div class="actualProductionOff"style="float: left;">
                                <center><span id="actualProductionOffText"></span></center>
                            </div>
                            <div class="actualProductionNodet" style="float: left;">
                                <center><span id="actualProductionNodetText"></span></center>
                            </div>
                            <div class="actualProductionNoStacklightText" style="float: left;">
                                <center><span id="actualProductionNoStacklightTextText"></span></center>
                            </div>
                        </div>

                        <div class="col-md-12">
                           <hr>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px;">
                            <h4 class="h4machineHeadcolor" style="color: #124D8D !important;">
                                <img src="<?php echo base_url('assets/nav_bar/setup-time.svg'); ?>">
                                <?php echo Setuptime; ?> 
                                <span id="SetupText" style="float: right;font-size: 9px;margin-top: 7px;line-height: 2;"></span>
                            </h4>
                        </div>

                        <div class="col-md-12" style="margin-top: 15px;">
                            <h4 class="h4machineHeadcolor" style="color: #FF8000 !important;">
                                <img src="<?php echo base_url('assets/nav_bar/no-production-time.svg'); ?>">
                                <?php echo Noproduction; ?> 
                                <span id="NoProductionText" style="float: right;font-size: 9px;margin-top: 7px;line-height: 2;"></span>
                            </h4>
                        </div>

                        

                        
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-4 colmd4chartsstyle colomd4machineModeStyle" style="height: 335px !important;">
            <div class="tab-content boxShadow machineModeStyle">
                <div class="tab-pane fade active show" >
                     <div class="row noPartMessage" style="position: absolute;top: 45%;right: 0%;font-size: 15px;font-weight: 600;text-align: center;display: none;margin: 0px !important;color: #124D8D;">
                        <div class="col-md-12">
                            <?php echo Partsanalyticswillbeavailablewhenpartsarechosenforproduction.'.'; ?>
                         </div>
                     </div>
                     <div class="row noCycleTimeMessage" style="position: absolute;top: 45%;right: 0%;font-size: 15px;font-weight: 600;text-align: center;display: none;margin: 0px !important;color: #124D8D;">
                        <div class="col-md-12">
                            <?php echo Partsanalyticswillbeavailablewhenpartswithcycletimemorethansecondsarechosenforproduction.'.'; ?>
                         </div>
                     </div>
                    <div class="row m-l-0 m-r-0 partData">
                        <div class="col-md-12">
                            <h4 class="h4machineHeadcolor">
                                <?php echo Orders; ?>
                            </h4>
                        </div>
                        
                       

                        <div class="col-md-12"> 
                            <div class="panel-body" style="padding: 0px 0px 0px 0px !important;overflow: auto;width: 100%;height: 217px;border-radius: 8px;">
                                <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0"> 
                                    
                                    <tbody id="ordersData" style="height: 117px;">

                                        
                                    </tbody>
                                </table>
                            </div>
                        </div>

                        <style type="text/css">
                            
                            .partsAnalytics:hover {
                              background-color: #f2f2f2  !important;
                              cursor: pointer;
                            }
                        </style>

                     
                        <div class="col-md-6" style="margin-top: 35px;">
                            <form style="float: right;"action="<?php echo base_url('Analytics/parts_analytics'); ?>"method="post">  

                                <input type="hidden" name="partsChoose1" id="partsChoose1"> 
                                <input type="hidden" name="partsChoose2" id="partsChoose2"> 
                                <input type="hidden" name="machineId" id="machineIdParts" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $machineNameList[0]['machineId']; } ?>"> 
                                <button type="submit" style="background-color: white;border: none;" class="partsAnalytics">
                                    <Center style="color:#002060;font-weight:700;"><?php echo Partsanalytics; ?> 
                                    <img style="width:10%" src="<?php echo base_url('assets/img/Partsanalytics');?>"></Center>
                                </button> 

                            </form>
                            
                            
                        </div>

                        <div class="col-md-6" style="margin-top: 35px;">
                            <Center style="color:#FF8000;font-weight:700;"><?php echo Orderlist; ?> 
                                <img style="width:10%" src="<?php echo base_url('assets/img/orderlist');?>"></Center>
                            </Center>
                        </div>

                        
                    </div>
                </div>
            </div>
        </div>


        <div class="col-md-4 colmd4chartsstyle colomd4machineModeStyle">
            <div class="tab-content boxShadow machineModeStyle">
                <div class="tab-pane fade active show" >
                    <div class="row noPartMessage" style="position: absolute;top: 43%;right: 0%;font-size: 15px;font-weight: 600;text-align: center;display: none;margin: 0px !important;color: #124D8D;"><div class="col-md-12"><?php echo Partsanalyticswillbeavailablewhenpartsarechosenforproduction.'.'; ?></div></div>
                    <div class="row noCycleTimeMessage" style="position: absolute;top: 43%;right: 0%;font-size: 15px;font-weight: 600;text-align: center;display: none;margin: 0px !important;color: #124D8D;">
                        <div class="col-md-12">
                            <?php echo Partsanalyticswillbeavailablewhenpartswithcycletimemorethansecondsarechosenforproduction.'.'; ?>
                         </div>
                     </div>
                    <div class="row partData">


                        <div>
                            <h4 class="h4machineHeadcolor m-l-10">
                                <?php echo KPIanalysis; ?>
                            </h4>
                        </div>


                        <div class="col-md-12">
                           <div id="DailyGraph5" style="height:270px;width: 100%;" >  </div>
                        </div>

                     
                        

                        
                    </div>
                </div>
            </div>
        </div>

    
        
    </div>
    <div class="graph-loader hide" ></div>
                
    <div class="row">
        <div class="col-md-12" >
            <div class="tab-content boxShadow" style="">
                <div class="tab-pane fade active show" style="overflow-x: scroll!important;">

                      <div class="row m-l-0 m-r-0">
                        <div class="col-md-12" style="z-index: 1;margin-bottom: -35px;">
                            <form style="float: right;"action="<?php echo base_url('Analytics/beta_GraphLog'); ?>"method="post">  <input type="hidden" name="machineId" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $machineNameList[0]['machineId']; } ?>"> <button class="btn btn-primary"><?php echo Log; ?></button> 
                               <button onclick="stateTransformer()" type="button" class="btn btn-primary"><?php echo Statetransformer; ?></button>
                            </form>
                        </div>

                        <div class="col-md-10">
                            <h3 id="headerChange" style="color: #002060;"><?php echo Hourlymachineanalysis; ?> </h3>
                            <div id="DailyGraph1" style="height:350px;width:100%;" >                        
                            </div> 
                        </div>


                        <div class="col-md-2">
                            <div class="p-b-20 margintopSmall" style="margin-top: 65px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FF8000;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Noproduction; ?> </div>
                            </div>

                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #124D8D;"> </div>
                                <div class="p-l-10" style="min-width: 115px;">  <?php echo Setuptime; ?> </div>
                            </div>

                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #000000;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Nodet; ?></div>
                            </div>
                        
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #CCD2DF;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Off; ?></div>
                            </div>

                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #F60100;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Stopped; ?></div>
                            </div>

                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FFCF00;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Waiting; ?></div>
                            </div>

                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #76BA1B;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Running; ?></div>
                            </div>
                            
                            <div class="p-b-20">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #002060;"> </div>
                                <div class="p-l-10" style="min-width: 115px;"> <?php echo Productiontime; ?></div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<!-- <style>
    .img-size{
    height: 450px;
    width: 700px;
    background-size: cover;
    overflow: hidden;
}
.modal-content {
   width: 700px;
  border:none;
}
.carousel-item
{
    padding-left: 40px;
    /*width: 280px;*/
}

.carosuleitemstyle
{
    border: 3px solid #002060!important;
    background-color: white!important;
    color: #002060!important;
    font-weight: 600;
    border-radius: 12px;
}

.carosuleitemstylestartfrom
{
    border: 3px solid #002060!important;
    background-color: white!important;
    color: #002060!important;
    font-weight: 600;
    border-radius: 12px;
    margin-left: 15px;
}

.carousel-control-prev-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='white' viewBox='0 0 8 8'%3E%3Cpath d='M5.25 0l-4 4 4 4 1.5-1.5-2.5-2.5 2.5-2.5-1.5-1.5z'/%3E%3C/svg%3E");
    width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: gray;
    border: 5px solid gray;
}
.carousel-control-next-icon {
    background-image: url("data:image/svg+xml;charset=utf8,%3Csvg xmlns='http://www.w3.org/2000/svg' fill='white' viewBox='0 0 8 8'%3E%3Cpath d='M2.75 0l-1.5 1.5 2.5 2.5-2.5 2.5 1.5 1.5 4-4-4-4z'/%3E%3C/svg%3E");
   width: 30px;
    height: 30px;
    border-radius: 50%;
    background-color: gray;
    border: 5px solid gray;
}
</style> -->
<style>
input[type='radio']:after {
        width: 13px;
    height: 13px;
    border-radius: 15px;
    top: 1px;
    left: 0px;
    position: relative;
    /* background-color: #d1d3d1; */
    content: '';
    display: inline-block;
    /* visibility: visible; */
    /*border: 1px solid #002060;*/
    border-color:#002060;
    }

    input[type='radio']:checked:after {
        width: 15px;
        height: 15px;
        border-radius: 50%;
        top: -1px;
        left: -1px;
        position: relative;
        background-color: #FF8000;
        content: '';
        display: inline-block;
        /*visibility: visible;*/
        /*border: 2px solid #002060;*/
    }
</style>


<div class="modal fade" id="add-state-transformer" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
            <h4 class="modal-title" style="color: #ff8000;"><?php echo Statetransformer; ?></h4>
            <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
              <img width="16"height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
            </button>
        </div>
        <div class="modal-body">
          <form class="p-b-20" action="add_transform" method="POST" id="add_transform_form" style="margin-left: 10px;margin-right: 10px;">

            <div class="row">
                <div class="col-md-12">
                    <div style="padding: 0 5px;" class="form-group p-5">
                        <img style="position: absolute;left: 10px;top: 12px;color: #CCD2E3;" src="<?php echo base_url('assets/img/tranformer_small.png');?>">
                        <input type="text" style="padding-left: 30px;" class="form-control border-left-right-top-hide h-29" id="transformName" name="transformName" placeholder="<?php echo Filloutthisfield; ?>" required > 
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6" style="font-size: 15px;padding-right: 0px;padding-left: 12px;font-weight: 400;"><span><?php echo Transformproductionstateto; ?> : </span></div>
               <!--  <div class="col-md-2" style="text-align: center;margin-left: -15px;">
                    <input type="radio" id="transformTo1" name="transformTo" value="1" checked>
                    <label for="#transformTo1" style="font-size: 16px;"><?php echo setup;?></label> 
                </div>

                <div class="col-md-4" style="text-align: center;margin-left: -15px;">
                    <input type="radio" id="transformTo2" name="transformTo" value="2">
                    <label for="#transformTo2" style="font-size: 16px;"><?php echo Noproduction; ?></label>
                </div> -->
                <div class="col-md-2" style="margin-top: -4px;padding-left: 0px;padding-right: 0px;">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="transformTo" id="flexRadioDefault1" value="1">
                      <label class="form-check-label" for="flexRadioDefault1">
                        <?php echo setup;?>
                      </label>
                    </div>
                </div>
                <div class="col-md-4" style="margin-top: -4px;padding-left: 0px;padding-right: 0px;">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" name="transformTo" id="flexRadioDefault2" value="2" checked>
                      <label class="form-check-label" for="flexRadioDefault2">
                        <?php echo Noproduction; ?>
                      </label>
                    </div>
                </div>
            </div>

            <div class="row m-t-20">
                <div class="col-md-6" style="text-align: center;">
                    <h5 style="color: #002060"><center style="text-transform: capitalize;"><?php echo txt_from; ?></center></h5>
                    <div class="js-inline-picker-transform-from"></div>
                </div>

                <div class="col-md-6" style="text-align: center;">
                    <h5 style="color: #002060"><center style="text-transform: capitalize;"><?php echo to; ?></center></h5>
                    <div class="js-inline-picker-transform-to"></div>
                </div>
            </div>
           
            
            <div class="row" style="margin-left: 0px;margin-right: 0px;">
                <div class="col-md-12">
                    <br>
                    <center>
                        <button style="padding-left: 50px;padding-right: 50px;" type="submit" class="btn btn-primary m-r-5" id="add_transformer_submit"><?php echo Add; ?></button>
                    </center>
                </div>
            </div> 


            <div class="row m-t-25" style="background-color: #f2f2f2;border-radius: 5px;display: none;" id="transformDataRow">
                <div class="col-md-12" style="margin-top: 15px;margin-bottom: 5px!important;" id="transformData">

                   
                    
                </div>
            </div>





         <!--    <div class="row" style="margin-left: 0px;margin-right: 0px;">
                <div class="col-md-12">
                    <br>
                    <center>
                        <button style="padding-left:65px;padding-right: 65px;" type="submit" class="btn btn-primary m-r-5 addtaskbuttonstyle" id="add_task_submit">Apply</button>
                    </center>
                </div>
            </div> -->

            
        </form>
      </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="add-view-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Addnewshift; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                
                <form class="p-b-20 m-l-10 m-r-10" action="add_view" method="POST" id="add_view_form">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group p-5">
                                <input type="text" style="padding: 0px 12px 16px !important;" class="form-control border-left-right-top-hide h-29" id="viewName" name="viewName" placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-stop"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                          <hr style="height: 2px;margin-top: 1rem;">
                        </div>
                    </div>

                    <div class="addBreakData" style="margin-bottom: 10px;display: none;"></div>

                    <div class="row addBreakButton">
                        <div class="col-md-12">
                          <span onclick="addBreak()" style="color: #FF8000;cursor: pointer;">
                            <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="display: none;">
                        <div class="col-md-12">
                          <span onclick="removeBreak()" style="color: red;cursor: pointer;">
                            <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="margin-top: 25px;display: none;">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-break-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-break-stop"></div>
                        </div>
                    </div>
                    
                    <div class="row removeBreak m-l-0 m-r-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                            <center>
                                <a onclick="saveBreaks()" class="btn btn-primary m-r-5 savebreaksstyle" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                            </center>
                        </div>
                    </div>

                    <div class="row submitButton m-l-0 m-r-0">
                        <div class="col-md-12">
                            <br>
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5 AddbreakButtonstyle" id="add_view_submit" >
                                    <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Add; ?></button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
      </div>
    </div>
</div>

<?php foreach ($machineView as $key => $value) { ?>
<div class="modal fade" id="edit-view-modal-dynamic<?php echo $value['viewId']; ?>" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Editview; ?></h4>
                <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
            </div>

            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="edit_view_form" method="POST" id="edit_view_form<?php echo $value['viewId']; ?>">
                    <input type="hidden" name="viewId" id="viewIdEdit-dynamic<?php echo $value['viewId']; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group">
                              <input type="text" class="form-control border-left-right-top-hide" id="viewNameEdit-dynamic<?php echo $value['viewId']; ?>" name="viewName" 
                              placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-edit-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-edit-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr class="h-2 m-t-10"></div>
                    </div>

                    <div class="addEditBreakData-dynamic<?php echo $value['viewId']; ?> m-b-10" style="display: none;"></div>

                    <div class="row addEditBreakButton-dynamic<?php echo $value['viewId']; ?>">
                        <div class="col-md-12">
                            <span onclick="addEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksbuttonStyle">
                                <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?>" style="display: none;">
                        <div class="col-md-12">
                            <span onclick="removeEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksRenoStyle">
                                <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div style="display: none" class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-t-25">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-r-0 m-l-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <a onclick="saveEditBreaks(<?php echo $value['viewId']; ?>)" class="btn btn-primary m-r-5 updatesavebreakbutton" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                          </center>
                        </div>
                    </div>
                    
                    <div class="row submitEditButton-dynamic<?php echo $value['viewId']; ?> m-l-0 m-r-0">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <button type="submit"class="btn btn-primary m-r-5 savebuttonstylee" id="edit_view_submit<?php echo $value['viewId']; ?>" ><?php echo save; ?></button>
                            <a class="btn btn-primary m-r-5 deleteviewbuttonstylee" id="deleteView" >
                                <img width="38px"height="34px" src="<?php echo base_url('assets/nav_bar/delete_white.svg'); ?>"></a>
                          </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>



<div class="modal fade" id="delete-view-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content deleteModalContentStyle">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title h4machineHeadcolor p-5"><?php echo Delete; ?> </h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div> 
            <div class="modal-body">
                <form class="p-b-20" action="delete_view_form" method="POST" id="delete_view_form" >
                    <input type="hidden" name="deleteViewId" id="deleteViewId" /> 
                    <div class="modal-footer border-top-0 p-t-0">
                        <div class="row">
                            <div class="col-md-12 m-t-30">
                                <center>
                                    <img class="deleteGrayButtonStyle"src="<?php echo base_url().'assets/img/delete_gray.svg'?>">
                                </center>
                            </div>
                            <br>
                            <div id="" class="m-b-10 alert alert-success fade hide" ></div>
                            <div class="col-md-12" style="margin-top: -5px;">
                                <center><?php echo Areyousureyouwanttodeleteview; ?></center>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <br>
                                    <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_view_submit"><?php echo Delete; ?></button>
                                    <br>
                                </center>
                            </div>
                        </div>          
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>