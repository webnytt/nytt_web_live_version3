<div class="col-md-12">
	<div style ='margin:0 auto; text-align:center'>
	    <a style="color:#002060;" href="<?php echo base_url("Training/switchLang/english"); ?>">English</a> |
	    <a style="color:#002060;" href="<?php echo base_url("Training/switchLang/Swedish"); ?>">Swedish</a> 
	</div> 
</div>

<div class="col-md-12">
	<div class="container" style="margin-left:18%;">
		<h1><?php echo $this->lang->line('welcome'); ?></h1>
		<?php  echo $this->lang->line('message'); ?>
	</div>
</div>

<div class="col-md-12">
	<div class="row">
		<button class="addnewUserButtonStyle" style="float:right;border-radius: 20px;background-color: #002060;border:none;color: white;padding:10px;cursor:pointer;margin-left: 50%;
    margin-top: 10%;" data-toggle="modal" data-target="#modal-delete-user">Delete User</button>



		<div class="modal fade" id="modal-delete-user" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="max-height: 230px;">
                    <div class="modal-header" style="background-color: #002060;">
                        <h4 style="color: #FF8000;padding: 4px;" class="modal-title"></h4>
                        <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                    </div>
                    <div class="modal-body">
                        <!-- <form class="p-b-20" action="" method="" class=""> -->
                            <div class="modal-footer" style="border-top: none;padding-top: 0;">
                                <div class="row">
                                    <div class="col-md-12" style="margin-top: -20px;">
                                        <center>
                                            <img style="width: 22%;" src="<?php echo base_url() . 'assets/img/delete_gray.svg' ?>" style="">
                                        </center>
                                    </div>
                                    <div class="col-md-12" style="">
                                        <center> Are you sure you want to delete</center>
                                    </div>
                                    <div class="col-md-12" style="margin-top: 5px;">
                                        <center>
                                            <button data-toggle="modal" data-target="#DeleteTypeText" class="btn btn-danger" style="width:25%;">Delete</button>
                                        </center>
                                    </div>
                                </div>
                            </div>
                        <!-- </form> -->
                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="DeleteTypeText" style="display: none;" aria-hidden="true">
            <div class="modal-dialog">
                <div class="modal-content" style="max-height: 230px;">
                    <div class="modal-header" style="background-color: #002060;">
                        <h4 style="color: #FF8000;padding: 4px;" class="modal-title">Confirmation Delete</h4>
                        <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                    </div>
                    <div class="modal-body">
                    	<div class="col-md-12">

                    		<form class="p-b-20" action="<?php echo base_url('Training/DeleteTaskTestings');?>" method="POST" id="deleteTaskTestingForm">
	                    		<div class="row">
	                    			<label>Manually Type Delete the Task</label>
	                    		</div>
	                    		<div class="col-md-12">
	                    			<!-- <input type="text" name="delete" id="delete"> -->
	                    			<input type="text"  id="Deleted" class="textbox">
	                    		</div>

	                    		<div class="col-md-12">
	                    			<button class="btn btn-danger" type="submit">Delete</button>
	                    		</div>
	                    	</form>
                    	</div>
              		</div>
              	</div>
            </div>
        </div>




	</div>
</div>


<script>

	$(document).ready(function()
	{
		$('#deleteTaskTestingForm').submit(function(e)
        {
        	var answer = $("#Deleted").val();
            if (answer != 'Deleted') 
            {
                alert("User input is not proper");
            } 
            else (answer == 'Deleted') 
            {
                var form = $(this);
                e.preventDefault();
                $('#deleteRoles_submit').text('deleteing...');
                $('#deleteRoles_submit').attr('disabled',true);
                var formData = new FormData(this);
                // alert('hello');
                $.ajax({
                    type: 'POST',
                    url: "<?php echo site_url('Training/DeleteTaskTestings'); ?>",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    dataType: "html",
                    success: function(data)
                    {
                        var response = JSON.parse(data);
                        if (response.status == true) 
                        {
                            $.gritter.add({
                                title: 'Success',
                                text: response.message
                            });
                            location.reload();
                        }
                        else
                        {
                            $('#deleteRoles_submit').attr('disabled',false);
                            $('#deleteRoles_submit').text('Save');
                            $.gritter.add({
                                title: 'Error',
                                text: response.message
                            });
                        }
                    },
                    
                });
            }
        });
	})
</script>>