<link href="<?php echo base_url('assets/css/operator_list.css');?>" rel="stylesheet" />


<style>
	.administratorImageStyle
	{
		width: 30px!important;margin: -5px 10px -5px 0!important;border-radius: 30px!important;height: 30px!important;
	}
	.#empTable_paginate
	{
		border-radius: 15px!important;
	    padding-left: 12!important;
	    box-shadow: grey 0 0 8px -2px!important;
	    max-width: 35%!important;
	    margin: auto!important;
	}

</style>

<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060;line-height: 10px !important;" class="page-header"><?php echo Operatorreports; ?></h1>
	<small style="font-size: 12px !important;font-weight: 600 !important;"><a style="color: black !important;" href="<?php echo base_url('Operator/operator_details');?>"> <?php echo Operators; ?> </a> > <?php echo Operatorreports; ?> </small>
	<br>
	<br><br><br>
	<input type="hidden" name="problemId" value="0">
	<div class="row">
					
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  	<div class="panel panel-inverse panel-primary boxShadow" >
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr>
								<center>
									<th class="width_10" style="color: #b8b0b0;cursor: pointer;padding-bottom: 27px !important;">
										<div class="dropdown" id="filterUserIdSelected" style="text-align: center;border: 1px solid #F2F2F2;padding: 4px;border-radius: 5px;text-transform: capitalize;width: 100px;margin-right: 50px;" >
											<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserIdSelected"> <?php echo UserId; ?> &nbsp;</span>
											<div class="dropdown-menu">
												<div class="dropdown-item">
													<div class="form-check" style="width: max-content;">
													  <input onclick="filterUserId('all')" class="form-check-input" type="radio" id="filterUserIdAll" 
													   name="filterUserId">
													  <label onclick="filterUserId('all')" class="form-check-label radioClick1" for="filterUserIdAll">
													    <?php echo All; ?>
													  </label>
													</div>
												</div>
												<?php foreach ($listOperators->result_array() as $key => $value) { ?>
													<div class="dropdown-item">
														
														<div class="form-check" style="width: max-content;">
														  <input onclick="filterUserId(<?php echo $value['userId']; ?>)" class="form-check-input" type="radio" id="filterUserId<?php echo $key; ?>" 
														   name="filterUserId">
														  <label onclick="filterUserId(<?php echo $value['userId']; ?>)" class="form-check-label radioClick1" for="filterUserId<?php echo $key; ?>">
														    <?php echo $value['userId']; ?>
														  </label>
														</div>
													</div>
												<?php } ?>
												<input type="hidden" id="filterUserId">
												</div><br>
											</div>
									</th>
											
									<th class="width_10" style="width:50px;color: #b8b0b0;cursor: pointer;padding-bottom: 27px !important;">
										<div class="dropdown" id="filterUserNameSelected" style="text-align: center;border: 1px solid #F2F2F2;padding: 4px;border-radius: 5px;width:100px;margin-right: 100px;" >
											<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelected"><?php echo Operator; ?> &nbsp;</span>
												<div class="dropdown-menu">

													<div class="dropdown-item">
														<div class="form-check" style="width: max-content;">
											 				 <input onclick="filterUserName('all')" class="form-check-input" type="radio" id="filterUserNameAll" 
											   					name="filterUserName">
											 					<label onclick="filterUserName('all')" class="form-check-label radioClick1" for="filterUserNameAll">
											    					<?php echo All; ?>
											  					</label>
														</div>
													</div>
										
													<?php foreach ($listOperators->result_array() as $key => $value) { ?>
														<div class="dropdown-item">
															<div class="form-check" style="width: max-content;">
												  				<input onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $user->userName; ?>')" class="form-check-input" type="radio" id="filterUserName<?php echo $key; ?>" 
												   					name="filterUserName">
												  					<label onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $value['userName']; ?>')" class="form-check-label radioClick2" for="filterUserName<?php echo $key; ?>">
												    					<?php echo $value['userName']; ?>
												  					</label>
															</div>
														</div>
												<?php } ?>
												<input type="hidden" id="filterUserName">
											</div>
											<br>
										</div>
									</th>
						
									<th style="color: #b8b0b0;cursor: pointer;padding-bottom: 27px !important; width: 100px !important;margin-right: 25px;" >
										<input id="filterStartDateSelected" type="text" class="datepicker56 garyColor" name="" placeholder="Checked in" onchange="filterStartDate(this.value)" style="height: 26px !important;border: 1px solid #F2F2F2;font-weight: 400;color: #b8b0b0;width: 100px;padding-left: 10px;border-radius: 5px;" >

										<span id="startDateArrow"><i style="margin-left: -17px;" class="fa fa-caret-down" aria-hidden="true"></i></span>

										<input type="hidden" id="filterStartDate"><br>
										<small  style="color: #FF8000;text-align : center;" ></small>
									</th>

									<th style="color: #b8b0b0;cursor: pointer;padding-bottom: 27px !important; width: 100px !important;margin-right: 25px;" >
										<input id="filterEndDateSelected" type="text" class="datepicker57 garyColor" name="" placeholder="Checked out" onchange="filterEndDate(this.value)" style="height: 26px !important;border: 1px solid #F2F2F2;font-weight: 400;color: #b8b0b0;width: 100px;padding-left: 10px;border-radius: 5px;width:100px;" >

										<span id="endDateArrow"><i style="margin-left: -17px;" class="fa fa-caret-down" aria-hidden="true"></i></span>

										<input type="hidden" id="filterEndDate"><br>
										<small  style="color: #FF8000;text-align : center;" ></small>
									</th>

									<th class="width_10" style="color: #b8b0b0;cursor: pointer;padding-bottom: 31px !important;width: 85px;">
										<?php echo Reports; ?>
									</th>
								</center>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div>

		<div class="col-md-3 detail" style="margin-top: -121px;display: none;margin-bottom: -63px;background: #FFFFFF !important;border-bottom-left-radius: 5px;    border-bottom-right-radius: 5px;box-shadow: gray -10px 10px 10px -10px;">
			 <div class="panel panel-inverse panel-primary" >
				<div class="panel-body" style="padding: 0;">
					<div class="row" style="padding: 8px;padding-top: 0 !important;background: #002060;height: 190px; border-top-left-radius: 5px;    border-top-right-radius: 5px;">
							<div class="col-md-12">
								<div style="padding: 14px 0 0 0;">
									<a href="javascript:void(0);" onclick="closeDetails();">
										<img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
								</div>
							</div>
							<div class="col-md-4" style="float: left">
								<img id="userImage" style="border-radius: 50%;border: 3px solid #FFFFFF" height="85" width="80" src="">  
							</div>
							<div class="col-md-8" style="float: left;margin-top: 27px;">
								<span id="userName" style="font-size: 17px;color: #FF8000;font-weight: 700;"></span> <br>
								<small style="color: #FFFFFF;font-size: 80%;" id="userId"></small>
							</div>
						</div>

						<form class="p-b-20" >
							<div class="row" style="margin-top: 25px;">
								<div class="col-md-12" style="padding-left: 26px;">

									<div style="font-weight: 600;font-size: 80%;">Reports</div>
									<h3 style="color: #FF8000;" id="reportCount"></h3>
									<hr style="height: 2px;margin-top: 0rem;">
								</div>
							</div>
				
							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<div style="font-weight: 600;font-size: 80%;margin-bottom: 7px;">Checked in</div>
									<div style="font-weight: 600;font-size: 12px;margin-bottom: 7px;">Date:&nbsp; <span style="color: #124D8D;" id="startDate"></span></div>
									<div style="font-weight: 600;font-size: 12px;">Time:&nbsp; <span style="color: #124D8D;" id="startTime"></span></div>
									<br>
									<hr style="height: 2px;margin-top: 0rem;">
								</div>
							</div>

							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<div style="font-weight: 600;font-size: 80%;margin-bottom: 7px;">Checked out</div>
									<div style="font-weight: 600;font-size: 12px;margin-bottom: 7px;">Date:&nbsp; <span style="color: #124D8D;" id="endDate"></span></div>
									<div style="font-weight: 600;font-size: 12px;">Time:&nbsp; <span style="color: #124D8D;" id="endTime"></span></div>
									
								</div>
							</div>
								
						</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
		<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

