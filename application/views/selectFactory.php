<!DOCTYPE html>
<!--[if IE 8]> <html lang="en" class="ie8"> <![endif]-->
<!--[if !IE]><!-->
<html lang="en">
<!--<![endif]-->
<head>
	<meta charset="utf-8" />
	<title>NYTT<?php echo dashboard; ?> | Login</title>
	<meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
	<meta content="" name="description" />
	<meta content="" name="author" />
	
	<!-- ================== BEGIN BASE CSS STYLE ================== -->
	<link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/font-awesome/5.3/css/all.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/plugins/animate/animate.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/transparent/style.min.css" rel="stylesheet" />
	<link href="<?php echo base_url(); ?>assets/css/transparent/style-responsive.min.css" rel="stylesheet" />
	<!-- <link href="<?php echo base_url(); ?>assets/css/transparent/theme/default.css" rel="stylesheet" id="theme" /> -->
	<!-- ================== END BASE CSS STYLE ================== -->
	
	<link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
	
	<link href="<?php echo base_url(); ?>assets/css/added-style.css" rel="stylesheet" />

	<link rel="icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/png" sizes="16x16">
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/pace/pace.min.js"></script>
	<!-- ================== END BASE JS ================== -->
</head>
<body class="pace-top bg-black-darker" style="background-color: #002060 !important;">
	<!-- begin #page-loader -->
	<div id="page-loader" class="fade show"><span class="spinner"></span></div>
	<!-- end #page-loader -->
	
	<!-- begin #page-container -->
	<div id="page-container" class="fade">
		<!-- begin login -->
		<div class="login login-with-news-feed">
			<!-- begin news-feed -->
			<div class="news-feed">
				<div class="news-image" style="background-image: url(<?php echo base_url(); ?>assets/img/login-bg/bg1.jpg)"></div>
				<div class="news-caption">
					<h4 class="caption-title" style="color: #FF8000 !important" ><div class="widget-img widget-img-md m-r-5 m-t-5" style="width: 119px;height: 43px;background-image: url(<?php echo base_url(); ?>assets/img/app_logo_orange.png);float:left;"></div><?php echo dashboard; ?></h4>
					<p>
						<?php echo Simpleandeasysolutiontounderstandandimproveyourmachineutilization ?>.
					</p>
				</div>
			</div>
			<!-- end news-feed -->
			<!-- begin right-content -->
			<div class="right-content">
				<!-- begin login-header -->
				<div class="login-header">
					<div class="brand" style="color: #FF8000 !important">
						<!--<span class="logo"></span>--><div class="widget-img widget-img-md m-r-5 m-b-5" style="width: 119px;height: 43px;background-image: url(<?php echo base_url(); ?>assets/img/app_logo_orange.png);float:left;"></div><?php echo dashboard; ?>
						<small><?php echo Trackyourmachines; ?></small>
					</div>
					<div class="icon">
						<i class="fa fa-sign-in"></i>
					</div>
				</div>
				<!-- end login-header -->
				<!-- begin login-content -->
				<div class="login-content">
					
					<?php if($this->session->flashdata('error_message') != '' ) { ?>
					<div>
						<div class="alert alert-danger fade show m-b-10">
							<span class="close" data-dismiss="alert">×</span>
							<?php echo $this->session->flashdata('error_message'); ?>
						</div>
					</div>
					<?php } ?> 
					<form action="<?php echo base_url(); ?>admin/selectFactory" method="post" class="margin-bottom-0"> 
						<div class="form-group m-b-15">
							<input type="text" disabled class="form-control form-control-lg" name="username" id="username" placeholder="User name" required value="<?php echo $this->session->userdata('userName'); ?>" /> 
						</div>
						
						<div class="form-group m-b-15">
							<label><?php echo Choosefactorytomonitor; ?>...</label>
							<select class="form-control form-control-lg" id="factoryId" name="factoryId" required>
							<?php foreach($listFactories->result() as $factory) { ?> 
								<option value="<?php echo $factory->factoryId; ?>"><?php echo base64_decode($factory->factoryName); ?></option>
							<?php } ?>
							</select>
						</div>
						
						
						<div class="login-buttons">
							<button type="submit" class="btn btn-primary btn-block btn-lg" style="background: #FF8000;"><?php echo Getmein; ?></button>
						</div>
						
						<hr />
						<p class="text-center">
							&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?>
						</p>
					</form>
				</div>
				<!-- end login-content -->
			</div>
			<!-- end right-container -->
		</div>
		<!-- end login -->
		
		<!-- begin theme-panel -->
		<?php  ?>
		<!-- end theme-panel -->
	</div>
	<!-- end page container -->
	
	<!-- ================== BEGIN BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-3.3.1.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
	<!--[if lt IE 9]>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/html5shiv.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/respond.min.js"></script>
		<script src="<?php echo base_url(); ?>assets/crossbrowserjs/excanvas.min.js"></script>
	<![endif]-->
	<script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/js-cookie/js.cookie.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/theme/transparent.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
	<!-- ================== END BASE JS ================== -->
	<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
	
	
	<script> COLOR_BLACK = 'rgba(255,255,255,0.75)'; </script>
	<script src="<?php echo base_url(); ?>assets/plugins/chart-js/Chart.min.js"></script>
	
	
	<script>
		$(document).ready(function() {
			App.init();
			
			$("select#factoryId").select2({  
			  tags: false,
			  minimumResultsForSearch: 4 
			}); 
		});
	</script>
</body>
</html>
