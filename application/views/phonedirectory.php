<html>
<head>
<title>Phone Directory</title>
</head>
 
<body>
<table width="600" border="1" cellspacing="5" cellpadding="5">
  <tr> 
    <td style="background : #ccc;">phoneId</td>
    <td> <?php echo $data->phoneId; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">userId</td>
    <td> <?php echo $data->userId;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">machineId</td>
    <td> <?php echo $data->machineId; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">insertTime</td>
    <td> <?php echo $data->insertTime;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">model</td>
    <td> <?php echo $data->model; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">manufacture</td>
    <td> <?php echo $data->manufacture; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">version</td>
    <td> <?php echo $data->version; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">batteryCapacity</td>
    <td> <?php echo $data->batteryCapacity; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">batteryLevel</td>
    <td> <?php echo $data->batteryLevel;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">totalRam</td>
    <td> <?php echo $data->totalRam;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">ramUsage</td>
    <td> <?php echo $data->ramUsage;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">cameraResolution</td>
    <td> <?php echo $data->cameraResolution;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">deviceId</td>
    <td> <?php echo $data->deviceId;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">appVersion</td>
    <td> <?php echo $data->appVersion;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">appVersionCode</td>
    <td> <?php echo $data->appVersionCode;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">wifiStatus</td>
    <td> <?php echo $data->wifiStatus;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">networkType</td>
    <td> <?php echo $data->networkType; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">imsi</td>
    <td> <?php echo $data->imsi;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">totalStorage</td>
    <td> <?php echo $data->totalStorage;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">storageAvailable</td>
    <td> <?php echo $data->storageAvailable;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">deviceResolution</td>
    <td> <?php echo $data->deviceResolution;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">sdkVersion</td>
    <td> <?php echo $data->sdkVersion;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">batteryHealth</td>
    <td> <?php echo $data->batteryHealth; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">batteryType</td>
    <td> <?php echo $data->batteryType; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">batteryTemperature</td>
    <td> <?php echo $data->batteryTemperature; ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">chargingSource</td>
    <td> <?php echo $data->chargingSource;  ?></td>
  </tr>
  <tr> 
    <td style="background : #ccc;">chargeFlag</td>
    <td> <?php echo $data->chargeFlag;  ?></td>
  </tr>

</table>