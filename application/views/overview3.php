<?php $firstBlock =   !empty($onlineOperators) ? ($onlineOperators / $allOperators) * 100 : 0; ?>
<?php $secondBlock =  !empty($yesterdayMaintenance) ? ($yesterdayMaintenance / $allOperators) * 100 : 0; ?>
<?php $thirdBlock =   !empty($overallRunningToday) ? number_format($overallRunningToday,2) : 0; ?>
<link href="<?php echo base_url('assets/css/overview2.css');?>" rel="stylesheet" />

<style type="text/css">
	body #gritter-notice-wrapper {
    	z-index: 1600!important;
	}

	svg circle:nth-child(2)
	{
	  stroke-dasharray: 440;
	  stroke-dashoffset:440;
	}
	<?php if($firstBlock > 0){ ?>
	.card1:nth-child(1) svg circle:nth-child(2)
	{
		stroke-dashoffset:<?php echo 440 - (440 * round($firstBlock)) / 100; ?>;
	  	stroke-linecap: round;
	  	stroke: #FF8000;
	}
	<?php } ?>
	<?php if($secondBlock > 0){ ?>
	.card2:nth-child(1) svg circle:nth-child(2)
	{
		stroke-dashoffset:<?php echo 440 - (440 * round($secondBlock)) / 100; ?>;
	  	stroke-linecap: round;
	  	stroke: #002060;
	}
	<?php } ?>
	<?php if($thirdBlock > 0){ ?>
	.card3:nth-child(1) svg circle:nth-child(2)
	{
		stroke-dashoffset:<?php echo 440 - (440 * round($thirdBlock)) / 100; ?>;
	  	stroke-linecap: round;
	  	stroke: #124D8D;
	}
	<?php } ?>

	<?php if($overallMood->star > 0){ ?>
	.card4:nth-child(1) svg circle:nth-child(2)
	{
		stroke-dashoffset:<?php echo 440 - (440 * (round($overallMood->star) * 20)) / 100; ?>;
	  	stroke-linecap: round;
	  	stroke: #FFCF00;
	}
	<?php } ?>

	  .card11:nth-child(1) svg circle:nth-child(2)
  {
    stroke-dashoffset:<?php echo 440 - (630 * 75) / 100; ?>;
      stroke-linecap: round;
      stroke: #FF8000;
  }

	.dropdown-menu.show 
	{
		top:30px!important;
	    left: 93% !important;
	    /*right: 10% !important;*/
	    transform: translateX(-50%) !important;
	}


	.arrowPoint:after {
	    vertical-align: 1px;
	    border-width: 4px;
	}


	.arrowPoint:after {
	    display: inline-block;
	    width: 0;
	    height: 0;
	    margin-left: .255em;
	    vertical-align: .255em;
	    content: "";
	    border-top: .3em solid;
	    border-right: .3em solid transparent;
	    border-bottom: 0;
	    border-left: .3em solid transparent;
	}

	@media only screen and (min-device-width: 1201px) and (max-device-width: 1310px) 
	{
	  	.smalllaptopsizeRes
	  	{
	  		min-width: 50%!important;
	  	}
	}

	
</style>


<div id="content" class="content">
	
	<!-- <i style="float:right;margin-right: -15%;margin-top: -1%;" class="fas fa-caret-down"></i> -->
	
	<?php if ($accessPointOverallView == true) { ?>
	<h1 style="font-size: 22px;color: #002060!important;" class="page-header"><?php echo Overallstatistics; ?></h1>
	<?php } ?>

</head>

<body>

	<div class="row">
		<?php if ($accessPointOverallView == true) { ?>
		<div class="col">
			<a href="<?php echo base_url('Operator/operator_checkin_checkout') ?>">
				<div class="card card1 boxShadow" style="min-height: 197px !important;">
				  	<div class="card-body">
						<center>
			              	<svg>
				                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
				                <circle cx="70" cy="70" r="70"></circle>
				                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="61"></circle>
			                 	<div class="overallStatisticscircle">
									<h3 style="margin-bottom:0.0rem;color:#002060;"><?php echo $onlineOperators; ?> / <?php echo $allOperators; ?></h3>
						            <small class="boxessize"><?php echo Operator; ?></small><br>
								  	<small style="color: #FF8000;font-size: 12px;font-weight: 600;"><?php echo Online; ?></small>
					          	</div>
							</svg>
			          	</center>
			      	</div>
				</div>
			</a>
		</div>

		<?php $yesterdayDate = date('Y-m-d',strtotime($overallMood->yesterdayMaintenanceDate)); ?>
		<div class="col">
			<a href="<?php echo base_url('Operator/operator_checkin_checkout?').'dateValE='.$yesterdayDate.'&dateValS='.$yesterdayDate; ?>">
				<div class="card card4 boxShadow" style="min-height: 197px !important;">
				  <div class="card-body">
						<center>
			              	<svg>
				                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
				                <circle cx="70" cy="70" r="70"></circle>
				                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="61"></circle>
				                 	<div class="overallStatisticscircle">
										<?php if ($overallMood->star > 0) { ?>
											<img class="widget-img widget-img-sm" src="<?php echo base_url('assets/img/moods/'. round($overallMood->star) .'.svg') ?>" alt="<?php echo round($overallMood->star); ?>">
											<br>
										<?php }else{ ?>
											<h3 style="margin-bottom:0.0rem;color:#002060;">N/A</h3>
										<?php } ?>
							            <small class="boxessize"><?php echo Overall; ?></small><br>
									  	<small class="overAllMoodStyle"><?php echo moodyesterday; ?></small>
						          	</div>
							</svg>
			          	</center>
			      	</div>
				</div>
			</a>
		</div>

		<div class="col">
			<a href="<?php echo base_url('Tasks/taskMaintenance'); ?>">
			<div class="card card2 boxShadow" style="min-height: 197px !important;">
			  <div class="card-body">
					<center>
		              	<svg>
			                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
		               		<circle cx="70" cy="70" r="70"></circle>
		                	<circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="61"></circle>
	                 		<div class="overallStatisticscircle">
								<h3 style="margin-bottom: 0.0rem;color: #002060;"><?php echo $yesterdayMaintenance; ?>/<?php echo $allOperators; ?>
								</h3>
						  		<small class="boxessize"><?php echo Yesterdays; ?></small><br>
						  		<small class="boxessize"><?php echo maintainence; ?></small>
			         		</div>
						</svg>
		          	</center>
		      	</div>
			</div>
			</a>
		</div>
	
		<div class="col">
			<a href="<?php echo base_url('Analytics/dashboard'); ?>">
				<div class="card card3 boxShadow" style="min-height: 197px !important;">
				  <div class="card-body">
						<center>
			              	<svg>
				                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
				                <circle cx="70" cy="70" r="70"></circle>
				                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="61"></circle>
				                <div class="overallStatisticscircle">
								<h3 class="overallRunningTodayh3Style"><?php echo number_format($overallRunningToday,2); ?>	%</h3>
							  		<small class="boxessize"><?php echo Overall; ?></small><br>
							  		<small class="runningTodayfontsize"><?php echo runningtoday; ?></small>
					          	</div>
							</svg>
			          	</center>
			      	</div>
				</div>
			</a>
		</div>
	
		<div class="col">
			<div class="card notificationcardGradientstyle" >
			  <div class="card-body boxShadow notificationcardstyle" >
			  	<h5 class="card-title notificationfontcolor"><?php echo Notification; ?></h5>
				  	<div id="notificationLog">	
					  	<?php foreach ($notificationLog as $notificationkey => $notificationvalue) { ?>
			    			<p class="notificationTextfontcolor"><?php echo substr($notificationvalue['notificationText'], 0,40).'....' ; ?></p>
			    			<hr class="notificationHR">
					  	<?php } ?>
			    		</div>

					  	<?php if (!empty($notificationLog)) { ?>
		    			<a style="color: white;" href="<?php echo base_url('admin/notificationLog'); ?>">
		    				<small class="notificationSmallstyleMore">
		    					<?php echo More; ?>
		    					<img style="width: 9%;" src="<?php echo base_url('assets/nav_bar/arrow.png'); ?>">
		    				</small>
		    			</a>

					  	<?php }else{ ?>
					  		<div id="notificationLog" style="height: 126px;">	
				    			<h4 style="color: white;"><?php echo Notificationnotfound; ?></h4>
				    		</div>
					  	<?php } ?>
				  	</div>
				</div>
			</div>
		<?php } ?>
		</div>
		<?php if ($accessPointMachineView == true) { ?>
		<h1 class="page-header machineStatusPageHeaderfont" onclick="AddBreakdownReason();"><?php echo MachineStatus; ?></h1>
		<?php } ?>
		<div class="row" id="refreshDiv">
			<input type="hidden" id="randomNumber" value="<?php echo mt_rand(1111111,9999999); ?>">
			<input type="hidden" id="machineIdCheckIn">
			<input type="hidden" id="machinePopupOpen" value="1">
			<input type="hidden" id="machineSetAppNotStart" value="0">
			<input type="hidden" id="compareRandomNumber" value="">
			<?php foreach ($listMachines as $key => $value) { 

						$machineLight = explode(",", $value['machineLight']);
						$machineLightcount = count($machineLight);
						foreach ($machineLight as $key1 => $value1) 
						{
							$machine_color = $this->db->where("colorId",$value1)->get('color')->row(); 
							$machine_color_name[] = $machine_color->colorName;
						}

						$colorName = implode(",", $machine_color_name);

						$runningData = array();
						$waitingData = array();
						$stoppedData = array();
						$offData = array();
						
						$machineStateColorLookupRunning = $this->factory->where(array("machineId" => $value['machineId']))->like("machineStateVal" , "Running")->get("machineStateColorLookup")->result_array();
					
						foreach ($machineStateColorLookupRunning as $key2 => $value2) 
						{
						
							if ($value2['redStatus'] == "1") 
							{
								$key2 = array_search('Red', $machine_color_name);
								$runningText[$key2] = "Red";
							}

							if ($value2['yellowStatus'] == "1") 
							{
								$key2 = array_search('Yellow', $machine_color_name);
								$runningText[$key2] = "Yellow";
							}

							if ($value2['greenStatus'] == "1") 
							{
								$key2 = array_search('Green', $machine_color_name);
								$runningText[$key2] = "Green";
							}

							if ($value2['blueStatus'] == "1") 
							{
								$key2 = array_search('Blue', $machine_color_name);
								$runningText[$key2] = "Blue";
							}

							if ($value2['whiteStatus'] == "1") 
							{
								$key2 = array_search('White', $machine_color_name);
								$runningText[$key2] = "White";
							}

							if (empty($runningText)) 
							{
								$runningText[] = "off";
							}
							ksort($runningText);
							$runningData[] = implode(",", $runningText);
							unset($runningText);
						}

						$machineStateColorLookupWaiting = $this->factory->where(array("machineId" => $value['machineId']))->like("machineStateVal" , "Waiting")->get("machineStateColorLookup")->result_array();
						
						foreach ($machineStateColorLookupWaiting as $key3 => $value3) 
						{
						
							if ($value3['redStatus'] == "1") 
							{
								$key3 = array_search('Red', $machine_color_name);
								$waitingText[$key3] = "Red";
							}

							if ($value3['yellowStatus'] == "1") 
							{
								$key3 = array_search('Yellow', $machine_color_name);
								$waitingText[$key3] = "Yellow";
							}

							if ($value3['greenStatus'] == "1") 
							{
								$key3 = array_search('Green', $machine_color_name);
								$waitingText[$key3] = "Green";
							}

							if ($value3['blueStatus'] == "1") 
							{
								$key3 = array_search('Blue', $machine_color_name);
								$waitingText[$key3] = "Blue";
							}

							if ($value3['whiteStatus'] == "1") 
							{
								$key3 = array_search('White', $machine_color_name);
								$waitingText[$key3] = "White";
							}

							
							if (empty($waitingText)) 
							{
								$waitingText[] = "off";
							}
							ksort($waitingText);
							$waitingData[] = implode(",", $waitingText);
							unset($waitingText);
						}

						
						$machineStateColorLookupStopped = $this->factory->where(array("machineId" => $value['machineId']))->like("machineStateVal" , "Stopped")->get("machineStateColorLookup")->result_array();

						foreach ($machineStateColorLookupStopped as $key4 => $value4) 
						{
						
							if ($value4['redStatus'] == "1") 
							{
								$key4 = array_search('Red', $machine_color_name);
								$stoppedText[$key4] = "Red";
							}

							if ($value4['yellowStatus'] == "1") 
							{
								$key4 = array_search('Yellow', $machine_color_name);
								$stoppedText[$key4] = "Yellow";
							}

							if ($value4['greenStatus'] == "1") 
							{
								$key4 = array_search('Green', $machine_color_name);
								$stoppedText[$key4] = "Green";
							}

							if ($value4['blueStatus'] == "1") 
							{
								$key4 = array_search('Blue', $machine_color_name);
								$stoppedText[$key4] = "Blue";
							}

							if ($value4['whiteStatus'] == "1") 
							{
								$key4 = array_search('White', $machine_color_name);
								$stoppedText[$key4] = "White";
							}

							if (empty($stoppedText)) 
							{
								$stoppedText[] = "off";
							}
							ksort($stoppedText);
							$stoppedData[] = implode(",", $stoppedText);
							unset($stoppedText);
						}
						

						$machineStateColorLookupOFF = $this->factory->where(array("machineId" => $value['machineId']))->like("machineStateVal" , "OFF")->get("machineStateColorLookup")->result_array();

						foreach ($machineStateColorLookupOFF as $key5 => $value5) 
						{
						
							if ($value5['redStatus'] == "1") 
							{
								$key5 = array_search('Red', $machine_color_name);
								$offText[$key5] = "Red";
							}

							if ($value5['yellowStatus'] == "1") 
							{
								$key5 = array_search('Yellow', $machine_color_name);
								$offText[$key5] = "Yellow";
							}

							if ($value5['greenStatus'] == "1") 
							{
								$key5 = array_search('Green', $machine_color_name);
								$offText[$key5] = "Green";
							}

							if ($value5['blueStatus'] == "1") 
							{
								$key5 = array_search('Blue', $machine_color_name);
								$offText[$key5] = "Blue";
							}

							if ($value5['whiteStatus'] == "1") 
							{
								$key5 = array_search('White', $machine_color_name);
								$offText[$key5] = "White";
							}

							if (empty($offText)) 
							{
								$offText[] = "off";
							}
							ksort($offText);
							$offData[] = implode(",", $offText);
							unset($offText);
						}
						?>

						<div class="modal fade" id="modal-update-stacklight-configured<?php echo $value['machineId']; ?>" style="display: none;" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content" style="width: 100%;" >
										
										<div class="modal-header" style="background-color: #002060;">
									        <h4 style="color: #FF8000;" class="modal-title" ><?php echo $value['machineName']; ?> <?php echo Stacklightconfiguration; ?></h4>
									        <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
									        	<img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
									     </div>
									<div class="modal-body">
										<form class="p-b-20"  id="update_stacklight_configuration<?php echo $value['machineId']; ?>" action="update_stacklight_configuration" method="POST" >

											<?php 
												if ($machineLightcount == 1) { 
													$nodet = array("Off");
													$nodet[] = $machine_color_name[0];
												}elseif($machineLightcount == 2){ 
													$nodet = array("Off");
													$nodet[] = $machine_color_name[0];
													$nodet[] = $machine_color_name[1];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1];
												}elseif ($machineLightcount == 3) {
													$nodet = array("Off");
													$nodet[] = $machine_color_name[0];
													$nodet[] = $machine_color_name[1];
													$nodet[] = $machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2];
												} elseif ($machineLightcount == 4) {
												 	$nodet = array("Off");
													$nodet[] = $machine_color_name[0];
													$nodet[] = $machine_color_name[1];
													$nodet[] = $machine_color_name[2];
													$nodet[] = $machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
												} elseif ($machineLightcount == 5) {
													$nodet = array("Off");
													$nodet[] = $machine_color_name[0];
													$nodet[] = $machine_color_name[1];
													$nodet[] = $machine_color_name[2];
													$nodet[] = $machine_color_name[3];
													$nodet[] = $machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[2].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[2].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[2].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
													$nodet[] = $machine_color_name[0].'-'.$machine_color_name[1].'-'.$machine_color_name[2].'-'.$machine_color_name[3].'-'.$machine_color_name[4];
												}
											?>
										
										<input type="hidden" name="nodet" value="<?php echo implode(",", $nodet); ?>">
											<?php unset($nodet); ?>
											<input type="hidden" name="machineId" value="<?php echo $value['machineId']; ?>">
											<div class="row">
												<div style="padding: 0 5px;" class=" col-md-12" data-toggle="tooltip" data-title="Machine" >
													<div style="padding: 0 5px;" class="form-group row" data-toggle="tooltip" data-title="Loading"  >
														<div class="col-md-3">
															<label><?php echo Running; ?></label>
														</div>
														<div class="col-md-9">
															<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="running<?php echo $value['machineId']; ?>" name="running[]" placeholder="<?php echo SelectRunning; ?>" onchange="stacklight_disabled_color_value('running',<?php echo $value['machineId']; ?>)" >
															<?php if ($machineLightcount == 1) { 
																$running1 = $machine_color_name[0];

																if (!in_array("off", $waitingData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$runningData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    											<?php } if (!in_array($running1, $waitingData) && !in_array($running1, $stoppedData) && !in_array($running1, $offData)) { ?>
																	
														    	<option <?php if (in_array($running1,$runningData)) { echo "selected"; } ?> value='<?php echo $running1; ?>'><?php echo displayColorName($running1); ?></option>
															<?php } ?>
															<?php } elseif($machineLightcount == 2){ 

																$running1 = $machine_color_name[0];
																$running2 = $machine_color_name[1];
																$running3 = $machine_color_name[0].','.$machine_color_name[1];
															if (!in_array("off", $waitingData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$runningData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    											<?php } if (!in_array($running1, $waitingData) && !in_array($running1, $stoppedData) && !in_array($running1, $offData)) { ?>
																	
														    	<option <?php if (in_array($running1,$runningData)) { echo "selected"; } ?> value='<?php echo $running1; ?>'><?php echo displayColorName($running1); ?></option>
															<?php }  if (!in_array($running2, $waitingData) && !in_array($running2, $stoppedData) && !in_array($running2, $offData)) { ?>
														    	<option <?php if (in_array($running2,$runningData)) { echo "selected"; } ?> value='<?php echo $running2; ?>'><?php echo displayColorName($running2); ?></option>
														    <?php }  if (!in_array($running3, $waitingData) && !in_array($running3, $stoppedData) && !in_array($running3, $offData)) { ?>
														    	<option <?php if (in_array($running3,$runningData)) { echo "selected";	} ?> value='<?php echo $running3; ?>'><?php echo displayColorName($running3); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 3) {

																$running1 = $machine_color_name[0];
																$running2 = $machine_color_name[1];
																$running3 = $machine_color_name[2];
																$running4 = $machine_color_name[0].','.$machine_color_name[1];
																$running5 = $machine_color_name[0].','.$machine_color_name[2];
																$running6 = $machine_color_name[1].','.$machine_color_name[2];
																$running7 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
															if (!in_array("off", $waitingData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$runningData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    											<?php }if (!in_array($running1, $waitingData) && !in_array($running1, $stoppedData) && !in_array($running1, $offData)) { ?>
																	
														    	<option <?php if (in_array($running1,$runningData)) { echo "selected"; } ?> value='<?php echo $running1; ?>'><?php echo displayColorName($running1); ?></option>
															<?php }  if (!in_array($running2, $waitingData) && !in_array($running2, $stoppedData) && !in_array($running2, $offData)) { ?>
														    	<option <?php if (in_array($running2,$runningData)) { echo "selected"; } ?> value='<?php echo $running2; ?>'><?php echo displayColorName($running2); ?></option>
														    <?php }  if (!in_array($running3, $waitingData) && !in_array($running3, $stoppedData) && !in_array($running3, $offData)) { ?>
														    	<option <?php if (in_array($running3,$runningData)) { echo "selected";	} ?> value='<?php echo $running3; ?>'><?php echo displayColorName($running3); ?></option>
														    <?php }  if (!in_array($running4, $waitingData) && !in_array($running4, $stoppedData) && !in_array($running4, $offData)) { ?>

														    	<option <?php if (in_array($running4,$runningData)) { echo "selected";	} ?> value='<?php echo $running4; ?>'><?php echo displayColorName($running4); ?></option>
														    <?php }  if (!in_array($running5, $waitingData) && !in_array($running5, $stoppedData) && !in_array($running5, $offData)) { ?>
														    	<option <?php if (in_array($running5,$runningData)) { echo "selected"; } ?> value='<?php echo $running5; ?>'><?php echo displayColorName($running5); ?></option>
														    <?php }  if (!in_array($running6, $waitingData) && !in_array($running6, $stoppedData) && !in_array($running6, $offData)) { ?>
														    	<option <?php if (in_array($running6,$runningData)) { echo "selected";	} ?> value='<?php echo $running6; ?>'><?php echo displayColorName($running6); ?></option>
														    <?php }  if (!in_array($running7, $waitingData) && !in_array($running7, $stoppedData) && !in_array($running7, $offData)) { ?>
														    	<option <?php if (in_array($running7,$runningData)) { echo "selected";	} ?> value='<?php echo $running7; ?>'><?php echo displayColorName($running7); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 4) {

																$running1 = $machine_color_name[0];
																$running2 = $machine_color_name[1];
																$running3 = $machine_color_name[2];
																$running4 = $machine_color_name[3];
																$running5 = $machine_color_name[0].','.$machine_color_name[1];
																$running6 = $machine_color_name[0].','.$machine_color_name[2];
																$running7 = $machine_color_name[0].','.$machine_color_name[3];
																$running8 = $machine_color_name[1].','.$machine_color_name[2];
																$running9 = $machine_color_name[1].','.$machine_color_name[3];
																$running10 = $machine_color_name[2].','.$machine_color_name[3];
																$running11 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$running12 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$running13 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$running14 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$running15 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
															 if (!in_array("off", $waitingData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$runningData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    											<?php } if (!in_array($running1, $waitingData) && !in_array($running1, $stoppedData) && !in_array($running1, $offData)) { ?>
																	
														    	<option <?php if (in_array($running1,$runningData)) { echo "selected"; } ?> value='<?php echo $running1; ?>'><?php echo displayColorName($running1); ?></option>
															<?php }  if (!in_array($running2, $waitingData) && !in_array($running2, $stoppedData) && !in_array($running2, $offData)) { ?>
														    	<option <?php if (in_array($running2,$runningData)) { echo "selected"; } ?> value='<?php echo $running2; ?>'><?php echo displayColorName($running2); ?></option>
														    <?php }  if (!in_array($running3, $waitingData) && !in_array($running3, $stoppedData) && !in_array($running3, $offData)) { ?>
														    	<option <?php if (in_array($running3,$runningData)) { echo "selected";	} ?> value='<?php echo $running3; ?>'><?php echo displayColorName($running3); ?></option>
														    <?php }  if (!in_array($running4, $waitingData) && !in_array($running4, $stoppedData) && !in_array($running4, $offData)) { ?>

														    	<option <?php if (in_array($running4,$runningData)) { echo "selected";	} ?> value='<?php echo $running4; ?>'><?php echo displayColorName($running4); ?></option>
														    <?php }  if (!in_array($running5, $waitingData) && !in_array($running5, $stoppedData) && !in_array($running5, $offData)) { ?>
														    	<option <?php if (in_array($running5,$runningData)) { echo "selected"; } ?> value='<?php echo $running5; ?>'><?php echo displayColorName($running5); ?></option>
														    <?php }  if (!in_array($running6, $waitingData) && !in_array($running6, $stoppedData) && !in_array($running6, $offData)) { ?>
														    	<option <?php if (in_array($running6,$runningData)) { echo "selected";	} ?> value='<?php echo $running6; ?>'><?php echo displayColorName($running6); ?></option>
														    <?php }  if (!in_array($running7, $waitingData) && !in_array($running7, $stoppedData) && !in_array($running7, $offData)) { ?>
														    	<option <?php if (in_array($running7,$runningData)) { echo "selected";	} ?> value='<?php echo $running7; ?>'><?php echo displayColorName($running7); ?></option>
														     <?php }  if (!in_array($running8, $waitingData) && !in_array($running8, $stoppedData) && !in_array($running8, $offData)) { ?>
														    	<option <?php if (in_array($running8,$runningData)) { echo "selected"; } ?> value='<?php echo $running8; ?>'><?php echo displayColorName($running8); ?></option>
														      <?php }  if (!in_array($running9, $waitingData) && !in_array($running9, $stoppedData) && !in_array($running9, $offData)) { ?>
															    <option <?php if (in_array($running9,$runningData)) { echo "selected"; } ?> value='<?php echo $running9; ?>'><?php echo displayColorName($running9); ?></option>
															  <?php }  if (!in_array($running10, $waitingData) && !in_array($running10, $stoppedData) && !in_array($running10, $offData)) { ?>
														    	<option <?php if (in_array($running10,$runningData)) { echo "selected";	} ?> value='<?php echo $running10; ?>'><?php echo displayColorName($running10); ?></option>
														      <?php }  if (!in_array($running11, $waitingData) && !in_array($running11, $stoppedData) && !in_array($running11, $offData)) { ?>
														    	<option <?php if (in_array($running11,$runningData)) { echo "selected";	} ?> value='<?php echo $running11; ?>'><?php echo displayColorName($running11); ?></option>
														      <?php }  if (!in_array($running12, $waitingData) && !in_array($running12, $stoppedData) && !in_array($running12, $offData)) { ?>
														    	<option <?php if (in_array($running12,$runningData)) { echo "selected"; } ?> value='<?php echo $running12; ?>'><?php echo displayColorName($running12); ?></option>
														      <?php }  if (!in_array($running13, $waitingData) && !in_array($running13, $stoppedData) && !in_array($running13, $offData)) { ?>
														    	<option <?php if (in_array($running13,$runningData)) { echo "selected";	} ?> value='<?php echo $running13; ?>'><?php echo displayColorName($running13); ?></option>
														      <?php }  if (!in_array($running14, $waitingData) && !in_array($running14, $stoppedData) && !in_array($running14, $offData)) { ?>
														    	<option <?php if (in_array($running14,$runningData)) { echo "selected";	} ?> value='<?php echo $running14; ?>'><?php echo displayColorName($running14); ?></option>
														      <?php }  if (!in_array($running15, $waitingData) && !in_array($running15, $stoppedData) && !in_array($running15, $offData)) { ?>
														    	<option <?php if (in_array($running15,$runningData)) { echo "selected";	} ?> value='<?php echo $running15; ?>'><?php echo displayColorName($running15); ?></option>
														      <?php }  ?>

													    	  <?php } elseif ($machineLightcount == 5) {

																$running1 = $machine_color_name[0];
																$running2 = $machine_color_name[1];
																$running3 = $machine_color_name[2];
																$running4 = $machine_color_name[3];
																$running5 = $machine_color_name[4];
																$running6 = $machine_color_name[0].','.$machine_color_name[1];
																$running7 = $machine_color_name[0].','.$machine_color_name[2];
																$running8 = $machine_color_name[0].','.$machine_color_name[3];
																$running9 = $machine_color_name[0].','.$machine_color_name[4];
																$running10 = $machine_color_name[1].','.$machine_color_name[2];
																$running11 = $machine_color_name[1].','.$machine_color_name[3];
																$running12 = $machine_color_name[1].','.$machine_color_name[4];
																$running13 = $machine_color_name[2].','.$machine_color_name[3];
																$running14 = $machine_color_name[2].','.$machine_color_name[4];
																$running15 = $machine_color_name[3].','.$machine_color_name[4];
																$running16 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$running17 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$running18 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[4];
																$running19 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$running20 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[4];
																$running21 = $machine_color_name[0].','.$machine_color_name[3].','.$machine_color_name[4];
																$running22 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$running23 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$running24 = $machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$running25 = $machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$running26 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$running27 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$running28 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$running29 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$running30 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$running31 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																
															 if (!in_array("off", $waitingData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$runningData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    											<?php } if (!in_array($running1, $waitingData) && !in_array($running1, $stoppedData) && !in_array($running1, $offData)) { ?>
																	
														    	<option <?php if (in_array($running1,$runningData)) { echo "selected"; } ?> value='<?php echo $running1; ?>'><?php echo displayColorName($running1); ?></option>
															<?php }  if (!in_array($running2, $waitingData) && !in_array($running2, $stoppedData) && !in_array($running2, $offData)) { ?>
														    	<option <?php if (in_array($running2,$runningData)) { echo "selected"; } ?> value='<?php echo $running2; ?>'><?php echo displayColorName($running2); ?></option>
														    <?php }  if (!in_array($running3, $waitingData) && !in_array($running3, $stoppedData) && !in_array($running3, $offData)) { ?>
														    	<option <?php if (in_array($running3,$runningData)) { echo "selected";	} ?> value='<?php echo $running3; ?>'><?php echo displayColorName($running3); ?></option>
														    <?php }  if (!in_array($running4, $waitingData) && !in_array($running4, $stoppedData) && !in_array($running4, $offData)) { ?>

														    	<option <?php if (in_array($running4,$runningData)) { echo "selected";	} ?> value='<?php echo $running4; ?>'><?php echo displayColorName($running4); ?></option>
														    <?php }  if (!in_array($running5, $waitingData) && !in_array($running5, $stoppedData) && !in_array($running5, $offData)) { ?>
														    	<option <?php if (in_array($running5,$runningData)) { echo "selected"; } ?> value='<?php echo $running5; ?>'><?php echo displayColorName($running5); ?></option>
														    <?php }  if (!in_array($running6, $waitingData) && !in_array($running6, $stoppedData) && !in_array($running6, $offData)) { ?>
														    	<option <?php if (in_array($running6,$runningData)) { echo "selected";	} ?> value='<?php echo $running6; ?>'><?php echo displayColorName($running6); ?></option>
														    <?php }  if (!in_array($running7, $waitingData) && !in_array($running7, $stoppedData) && !in_array($running7, $offData)) { ?>
														    	<option <?php if (in_array($running7,$runningData)) { echo "selected";	} ?> value='<?php echo $running7; ?>'><?php echo displayColorName($running7); ?></option>
														     <?php }  if (!in_array($running8, $waitingData) && !in_array($running8, $stoppedData) && !in_array($running8, $offData)) { ?>
														    	<option <?php if (in_array($running8,$runningData)) { echo "selected"; } ?> value='<?php echo $running8; ?>'><?php echo displayColorName($running8); ?></option>
														      <?php }  if (!in_array($running9, $waitingData) && !in_array($running9, $stoppedData) && !in_array($running9, $offData)) { ?>
															    <option <?php if (in_array($running9,$runningData)) { echo "selected"; } ?> value='<?php echo $running9; ?>'><?php echo displayColorName($running9); ?></option>
															  <?php }  if (!in_array($running10, $waitingData) && !in_array($running10, $stoppedData) && !in_array($running10, $offData)) { ?>
														    	<option <?php if (in_array($running10,$runningData)) { echo "selected";	} ?> value='<?php echo $running10; ?>'><?php echo displayColorName($running10); ?></option>
														      <?php }  if (!in_array($running11, $waitingData) && !in_array($running11, $stoppedData) && !in_array($running11, $offData)) { ?>
														    	<option <?php if (in_array($running11,$runningData)) { echo "selected";	} ?> value='<?php echo $running11; ?>'><?php echo displayColorName($running11); ?></option>
														      <?php }  if (!in_array($running12, $waitingData) && !in_array($running12, $stoppedData) && !in_array($running12, $offData)) { ?>
														    	<option <?php if (in_array($running12,$runningData)) { echo "selected"; } ?> value='<?php echo $running12; ?>'><?php echo displayColorName($running12); ?></option>
														      <?php }  if (!in_array($running13, $waitingData) && !in_array($running13, $stoppedData) && !in_array($running13, $offData)) { ?>
														    	<option <?php if (in_array($running13,$runningData)) { echo "selected";	} ?> value='<?php echo $running13; ?>'><?php echo displayColorName($running13); ?></option>
														      <?php }  if (!in_array($running14, $waitingData) && !in_array($running14, $stoppedData) && !in_array($running14, $offData)) { ?>
														    	<option <?php if (in_array($running14,$runningData)) { echo "selected";	} ?> value='<?php echo $running14; ?>'><?php echo displayColorName($running14); ?></option>
														      <?php }  if (!in_array($running15, $waitingData) && !in_array($running15, $stoppedData) && !in_array($running15, $offData)) { ?>
														    	<option <?php if (in_array($running15,$runningData)) { echo "selected";	} ?> value='<?php echo $running15; ?>'><?php echo displayColorName($running15); ?></option>
														      <?php }  if (!in_array($running16, $waitingData) && !in_array($running16, $stoppedData) && !in_array($running16, $offData)) { ?>
														    	<option <?php if (in_array($running16,$runningData)) { echo "selected";	} ?> value='<?php echo $running16; ?>'><?php echo displayColorName($running16); ?></option>
														      <?php }  if (!in_array($running17, $waitingData) && !in_array($running17, $stoppedData) && !in_array($running17, $offData)) { ?>
														    	<option <?php if (in_array($running17,$runningData)) { echo "selected";	} ?> value='<?php echo $running17; ?>'><?php echo displayColorName($running17); ?></option>
														      <?php }  if (!in_array($running18, $waitingData) && !in_array($running18, $stoppedData) && !in_array($running18, $offData)) { ?>
														    	<option <?php if (in_array($running18,$runningData)) { echo "selected";	} ?> value='<?php echo $running18; ?>'><?php echo displayColorName($running18); ?></option>
														      <?php }  if (!in_array($running19, $waitingData) && !in_array($running19, $stoppedData) && !in_array($running19, $offData)) { ?>
														    	<option <?php if (in_array($running19,$runningData)) { echo "selected";	} ?> value='<?php echo $running19; ?>'><?php echo displayColorName($running19); ?></option>
														      <?php }  if (!in_array($running20, $waitingData) && !in_array($running20, $stoppedData) && !in_array($running20, $offData)) { ?>
														    	<option <?php if (in_array($running20,$runningData)) { echo "selected";	} ?> value='<?php echo $running20; ?>'><?php echo displayColorName($running20); ?></option>
														      <?php }  if (!in_array($running21, $waitingData) && !in_array($running21, $stoppedData) && !in_array($running21, $offData)) { ?>
														    	<option <?php if (in_array($running21,$runningData)) { echo "selected";	} ?> value='<?php echo $running21; ?>'><?php echo displayColorName($running21); ?></option>
														      <?php }  if (!in_array($running22, $waitingData) && !in_array($running22, $stoppedData) && !in_array($running22, $offData)) { ?>
														    	<option <?php if (in_array($running22,$runningData)) { echo "selected";	} ?> value='<?php echo $running22; ?>'><?php echo displayColorName($running22); ?></option>
														      <?php }  if (!in_array($running23, $waitingData) && !in_array($running23, $stoppedData) && !in_array($running23, $offData)) { ?>
														    	<option <?php if (in_array($running23,$runningData)) { echo "selected";	} ?> value='<?php echo $running23; ?>'><?php echo displayColorName($running23); ?></option>
														      <?php }  if (!in_array($running24, $waitingData) && !in_array($running24, $stoppedData) && !in_array($running24, $offData)) { ?>
														    	<option <?php if (in_array($running24,$runningData)) { echo "selected";	} ?> value='<?php echo $running24; ?>'><?php echo displayColorName($running24); ?></option>
														      <?php }  if (!in_array($running25, $waitingData) && !in_array($running25, $stoppedData) && !in_array($running25, $offData)) { ?>
														    	<option <?php if (in_array($running25,$runningData)) { echo "selected";	} ?> value='<?php echo $running25; ?>'><?php echo displayColorName($running25); ?></option>
														      <?php }  if (!in_array($running26, $waitingData) && !in_array($running26, $stoppedData) && !in_array($running26, $offData)) { ?>
														    	<option <?php if (in_array($running26,$runningData)) { echo "selected";	} ?> value='<?php echo $running26; ?>'><?php echo displayColorName($running26); ?></option>
															<?php }   if (!in_array($running27, $waitingData) && !in_array($running27, $stoppedData) && !in_array($running27, $offData)) { ?>
														    	<option <?php if (in_array($running27,$runningData)) { echo "selected";	} ?> value='<?php echo $running27; ?>'><?php echo displayColorName($running27); ?></option>
														    <?php }   if (!in_array($running28, $waitingData) && !in_array($running28, $stoppedData) && !in_array($running28, $offData)) { ?>
														    	<option <?php if (in_array($running28,$runningData)) { echo "selected";	} ?> value='<?php echo $running28; ?>'><?php echo displayColorName($running28); ?></option>
														    <?php }   if (!in_array($running29, $waitingData) && !in_array($running29, $stoppedData) && !in_array($running29, $offData)) { ?>
														    	<option <?php if (in_array($running29,$runningData)) { echo "selected";	} ?> value='<?php echo $running29; ?>'><?php echo displayColorName($running29); ?></option>
														    <?php }   if (!in_array($running30, $waitingData) && !in_array($running30, $stoppedData) && !in_array($running30, $offData)) { ?>
														    	<option <?php if (in_array($running30,$runningData)) { echo "selected";	} ?> value='<?php echo $running30; ?>'><?php echo displayColorName($running30); ?></option>
														    <?php }  if (!in_array($running31, $waitingData) && !in_array($running31, $stoppedData) && !in_array($running31, $offData)) { ?>
														    	<option <?php if (in_array($running31,$runningData)) { echo "selected";	} ?> value='<?php echo $running31; ?>'><?php echo displayColorName($running31); ?></option>
														    <?php }?>

													    	  <?php }
													    	  ?>
															</select>
														</div>
													</div>

													<div style="padding: 0 5px;" class="form-group row" data-toggle="tooltip" data-title="Loading" >
														<div class="col-md-3">
															<label><?php echo Waiting; ?></label>
														</div>
														<div class="col-md-9">
															<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="waiting<?php echo $value['machineId']; ?>" onchange="stacklight_disabled_color_value('waiting',<?php echo $value['machineId']; ?>)" name="waiting[]" placeholder="<?php echo SelectWaiting; ?>" >
															<?php if ($machineLightcount == 1) { 
																$waiting1 = $machine_color_name[0];

															if (!in_array("off", $runningData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$waitingData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    										<?php } if (!in_array($waiting1, $runningData) && !in_array($waiting1, $stoppedData) && !in_array($waiting1, $offData)) { ?>
														    	<option <?php if (in_array($waiting1,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting1; ?>'><?php echo displayColorName($waiting1); ?></option>
														    <?php }  ?>
															<?php } elseif($machineLightcount == 2){ 

																$waiting1 = $machine_color_name[0];
																$waiting2 = $machine_color_name[1];
																$waiting3 = $machine_color_name[0].','.$machine_color_name[1];
															if (!in_array("off", $runningData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$waitingData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    										<?php }  if (!in_array($waiting1, $runningData) && !in_array($waiting1, $stoppedData) && !in_array($waiting1, $offData)) { ?>
														    	<option <?php if (in_array($waiting1,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting1; ?>'><?php echo displayColorName($waiting1); ?></option>
														    <?php } if (!in_array($waiting2, $runningData) && !in_array($waiting2, $stoppedData) && !in_array($waiting2, $offData)) { ?>
														    	<option <?php if (in_array($waiting2,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting2; ?>'><?php echo displayColorName($waiting2); ?></option>
														    <?php } if (!in_array($waiting3, $runningData) && !in_array($waiting3, $stoppedData) && !in_array($waiting3, $offData)) { ?>
														    	<option <?php if (in_array($waiting3,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting3; ?>'><?php echo displayColorName($waiting3); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 3) {

																$waiting1 = $machine_color_name[0];
																$waiting2 = $machine_color_name[1];
																$waiting3 = $machine_color_name[2];
																$waiting4 = $machine_color_name[0].','.$machine_color_name[1];
																$waiting5 = $machine_color_name[0].','.$machine_color_name[2];
																$waiting6 = $machine_color_name[1].','.$machine_color_name[2];
																$waiting7 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
															 if (!in_array("off", $runningData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$waitingData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    										<?php } if (!in_array($waiting1, $runningData) && !in_array($waiting1, $stoppedData) && !in_array($waiting1, $offData)) { ?>
														    	<option <?php if (in_array($waiting1,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting1; ?>'><?php echo displayColorName($waiting1); ?></option>
														    <?php } if (!in_array($waiting2, $runningData) && !in_array($waiting2, $stoppedData) && !in_array($waiting2, $offData)) { ?>
														    	<option <?php if (in_array($waiting2,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting2; ?>'><?php echo displayColorName($waiting2); ?></option>
														    <?php } if (!in_array($waiting3, $runningData) && !in_array($waiting3, $stoppedData) && !in_array($waiting3, $offData)) { ?>
														    	<option <?php if (in_array($waiting3,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting3; ?>'><?php echo displayColorName($waiting3); ?></option>
														    <?php } if (!in_array($waiting4, $runningData) && !in_array($waiting4, $stoppedData) && !in_array($waiting4, $offData)) { ?>

														    	<option <?php if (in_array($waiting4,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting4; ?>'><?php echo displayColorName($waiting4); ?></option>

														    <?php } if (!in_array($waiting5, $runningData) && !in_array($waiting5, $stoppedData) && !in_array($waiting5, $offData)) { ?>
														    	<option <?php if (in_array($waiting5,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting5; ?>'><?php echo displayColorName($waiting5); ?></option>
														    <?php } if (!in_array($waiting6, $runningData) && !in_array($waiting6, $stoppedData) && !in_array($waiting6, $offData)) { ?>
														    	<option <?php if (in_array($waiting6,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting6; ?>'><?php echo displayColorName($waiting6); ?></option>
														    <?php } if (!in_array($waiting7, $runningData) && !in_array($waiting7, $stoppedData) && !in_array($waiting7, $offData)) { ?>
														    	<option <?php if (in_array($waiting7,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting7; ?>'><?php echo displayColorName($waiting7); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 4) {

																$waiting1 = $machine_color_name[0];
																$waiting2 = $machine_color_name[1];
																$waiting3 = $machine_color_name[2];
																$waiting4 = $machine_color_name[3];
																$waiting5 = $machine_color_name[0].','.$machine_color_name[1];
																$waiting6 = $machine_color_name[0].','.$machine_color_name[2];
																$waiting7 = $machine_color_name[0].','.$machine_color_name[3];
																$waiting8 = $machine_color_name[1].','.$machine_color_name[2];
																$waiting9 = $machine_color_name[1].','.$machine_color_name[3];
																$waiting10 = $machine_color_name[2].','.$machine_color_name[3];
																$waiting11 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$waiting12 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$waiting13 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$waiting14 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$waiting15 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
															 if (!in_array("off", $runningData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$waitingData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    										<?php }  if (!in_array($waiting1, $runningData) && !in_array($waiting1, $stoppedData) && !in_array($waiting1, $offData)) { ?>
															    <option <?php if (in_array($waiting1,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting1; ?>'><?php echo displayColorName($waiting1); ?></option>
															 <?php } if (!in_array($waiting2, $runningData) && !in_array($waiting2, $stoppedData) && !in_array($waiting2, $offData)) { ?>
															    <option <?php if (in_array($waiting2,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting2; ?>'><?php echo displayColorName($waiting2); ?></option>
															 <?php } if (!in_array($waiting3, $runningData) && !in_array($waiting3, $stoppedData) && !in_array($waiting3, $offData)) { ?>
														    	<option <?php if (in_array($waiting3,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting3; ?>'><?php echo displayColorName($waiting3); ?></option>
														     <?php } if (!in_array($waiting4, $runningData) && !in_array($waiting4, $stoppedData) && !in_array($waiting4, $offData)) { ?>
														    	<option <?php if (in_array($waiting4,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting4; ?>'><?php echo displayColorName($waiting4); ?></option>
														     <?php } if (!in_array($waiting5, $runningData) && !in_array($waiting5, $stoppedData) && !in_array($waiting5, $offData)) { ?>
														    	<option <?php if (in_array($waiting5,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting5; ?>'><?php echo displayColorName($waiting5); ?></option>
														     <?php } if (!in_array($waiting6, $runningData) && !in_array($waiting6, $stoppedData) && !in_array($waiting6, $offData)) { ?>
														    	<option <?php if (in_array($waiting6,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting6; ?>'><?php echo displayColorName($waiting6); ?></option>
														     <?php } if (!in_array($waiting7, $runningData) && !in_array($waiting7, $stoppedData) && !in_array($waiting7, $offData)) { ?>
														    	<option <?php if (in_array($waiting7,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting7; ?>'><?php echo displayColorName($waiting7); ?></option>
														     <?php } if (!in_array($waiting8, $runningData) && !in_array($waiting8, $stoppedData) && !in_array($waiting8, $offData)) { ?>
														    	<option <?php if (in_array($waiting8,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting8; ?>'><?php echo displayColorName($waiting8); ?></option>
														     <?php } if (!in_array($waiting9, $runningData) && !in_array($waiting9, $stoppedData) && !in_array($waiting9, $offData)) { ?>
															    <option <?php if (in_array($waiting9,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting9; ?>'><?php echo displayColorName($waiting9); ?></option>
															 <?php } if (!in_array($waiting10, $runningData) && !in_array($waiting10, $stoppedData) && !in_array($waiting10, $offData)) { ?>
														    	<option <?php if (in_array($waiting10,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting10; ?>'><?php echo displayColorName($waiting10); ?></option>
														     <?php } if (!in_array($waiting11, $runningData) && !in_array($waiting11, $stoppedData) && !in_array($waiting11, $offData)) { ?>
														    	<option <?php if (in_array($waiting11,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting11; ?>'><?php echo displayColorName($waiting11); ?></option>
														     <?php } if (!in_array($waiting12, $runningData) && !in_array($waiting12, $stoppedData) && !in_array($waiting12, $offData)) { ?>
														    	<option <?php if (in_array($waiting12,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting12; ?>'><?php echo displayColorName($waiting12); ?></option>
														     <?php } if (!in_array($waiting13, $runningData) && !in_array($waiting13, $stoppedData) && !in_array($waiting13, $offData)) { ?>
														    	<option <?php if (in_array($waiting13,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting13; ?>'><?php echo displayColorName($waiting13); ?></option>
														     <?php } if (!in_array($waiting14, $runningData) && !in_array($waiting14, $stoppedData) && !in_array($waiting14, $offData)) { ?>
														    	<option <?php if (in_array($waiting14,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting14; ?>'><?php echo displayColorName($waiting14); ?></option>
														     <?php } if (!in_array($waiting15, $runningData) && !in_array($waiting15, $stoppedData) && !in_array($waiting15, $offData)) { ?>
														    	<option <?php if (in_array($waiting15,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting15; ?>'><?php echo displayColorName($waiting15); ?></option>

														    <?php } ?>

													    	  <?php } elseif ($machineLightcount == 5) {

																$waiting1 = $machine_color_name[0];
																$waiting2 = $machine_color_name[1];
																$waiting3 = $machine_color_name[2];
																$waiting4 = $machine_color_name[3];
																$waiting5 = $machine_color_name[4];
																$waiting6 = $machine_color_name[0].','.$machine_color_name[1];
																$waiting7 = $machine_color_name[0].','.$machine_color_name[2];
																$waiting8 = $machine_color_name[0].','.$machine_color_name[3];
																$waiting9 = $machine_color_name[0].','.$machine_color_name[4];
																$waiting10 = $machine_color_name[1].','.$machine_color_name[2];
																$waiting11 = $machine_color_name[1].','.$machine_color_name[3];
																$waiting12 = $machine_color_name[1].','.$machine_color_name[4];
																$waiting13 = $machine_color_name[2].','.$machine_color_name[3];
																$waiting14 = $machine_color_name[2].','.$machine_color_name[4];
																$waiting15 = $machine_color_name[3].','.$machine_color_name[4];
																$waiting16 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$waiting17 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$waiting18 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[4];
																$waiting19 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$waiting20 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[4];
																$waiting21 = $machine_color_name[0].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting22 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$waiting23 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$waiting24 = $machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting25 = $machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting26 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$waiting27 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$waiting28 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting29 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting30 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$waiting31 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																
															 if (!in_array("off", $runningData) && !in_array("off", $stoppedData) && !in_array("off", $offData)) { 
															?>
																<option <?php if (in_array("off",$waitingData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
				    										<?php }  if (!in_array($waiting1, $runningData) && !in_array($waiting1, $stoppedData) && !in_array($waiting1, $offData)) { ?>
															    <option <?php if (in_array($waiting1,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting1; ?>'><?php echo displayColorName($waiting1); ?></option>
															 <?php } if (!in_array($waiting2, $runningData) && !in_array($waiting2, $stoppedData) && !in_array($waiting2, $offData)) { ?>
															    <option <?php if (in_array($waiting2,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting2; ?>'><?php echo displayColorName($waiting2); ?></option>
															 <?php } if (!in_array($waiting3, $runningData) && !in_array($waiting3, $stoppedData) && !in_array($waiting3, $offData)) { ?>
														    	<option <?php if (in_array($waiting3,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting3; ?>'><?php echo displayColorName($waiting3); ?></option>
														     <?php } if (!in_array($waiting4, $runningData) && !in_array($waiting4, $stoppedData) && !in_array($waiting4, $offData)) { ?>
														    	<option <?php if (in_array($waiting4,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting4; ?>'><?php echo displayColorName($waiting4); ?></option>
														     <?php } if (!in_array($waiting5, $runningData) && !in_array($waiting5, $stoppedData) && !in_array($waiting5, $offData)) { ?>
														    	<option <?php if (in_array($waiting5,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting5; ?>'><?php echo displayColorName($waiting5); ?></option>
														     <?php } if (!in_array($waiting6, $runningData) && !in_array($waiting6, $stoppedData) && !in_array($waiting6, $offData)) { ?>
														    	<option <?php if (in_array($waiting6,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting6; ?>'><?php echo displayColorName($waiting6); ?></option>
														     <?php } if (!in_array($waiting7, $runningData) && !in_array($waiting7, $stoppedData) && !in_array($waiting7, $offData)) { ?>
														    	<option <?php if (in_array($waiting7,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting7; ?>'><?php echo displayColorName($waiting7); ?></option>
														     <?php } if (!in_array($waiting8, $runningData) && !in_array($waiting8, $stoppedData) && !in_array($waiting8, $offData)) { ?>
														    	<option <?php if (in_array($waiting8,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting8; ?>'><?php echo displayColorName($waiting8); ?></option>
														     <?php } if (!in_array($waiting9, $runningData) && !in_array($waiting9, $stoppedData) && !in_array($waiting9, $offData)) { ?>
															    <option <?php if (in_array($waiting9,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting9; ?>'><?php echo displayColorName($waiting9); ?></option>
															 <?php } if (!in_array($waiting10, $runningData) && !in_array($waiting10, $stoppedData) && !in_array($waiting10, $offData)) { ?>
														    	<option <?php if (in_array($waiting10,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting10; ?>'><?php echo displayColorName($waiting10); ?></option>
														     <?php } if (!in_array($waiting11, $runningData) && !in_array($waiting11, $stoppedData) && !in_array($waiting11, $offData)) { ?>
														    	<option <?php if (in_array($waiting11,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting11; ?>'><?php echo displayColorName($waiting11); ?></option>
														     <?php } if (!in_array($waiting12, $runningData) && !in_array($waiting12, $stoppedData) && !in_array($waiting12, $offData)) { ?>
														    	<option <?php if (in_array($waiting12,$waitingData)) { echo "selected"; } ?> value='<?php echo $waiting12; ?>'><?php echo displayColorName($waiting12); ?></option>
														     <?php } if (!in_array($waiting13, $runningData) && !in_array($waiting13, $stoppedData) && !in_array($waiting13, $offData)) { ?>
														    	<option <?php if (in_array($waiting13,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting13; ?>'><?php echo displayColorName($waiting13); ?></option>
														     <?php } if (!in_array($waiting14, $runningData) && !in_array($waiting14, $stoppedData) && !in_array($waiting14, $offData)) { ?>
														    	<option <?php if (in_array($waiting14,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting14; ?>'><?php echo displayColorName($waiting14); ?></option>
														     <?php } if (!in_array($waiting15, $runningData) && !in_array($waiting15, $stoppedData) && !in_array($waiting15, $offData)) { ?>
														    	<option <?php if (in_array($waiting15,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting15; ?>'><?php echo displayColorName($waiting15); ?></option>
														     <?php } if (!in_array($waiting16, $runningData) && !in_array($waiting16, $stoppedData) && !in_array($waiting16, $offData)) { ?>
														    	<option <?php if (in_array($waiting16,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting16; ?>'><?php echo displayColorName($waiting16); ?></option>
														     <?php } if (!in_array($waiting17, $runningData) && !in_array($waiting17, $stoppedData) && !in_array($waiting17, $offData)) { ?>
														    	<option <?php if (in_array($waiting17,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting17; ?>'><?php echo displayColorName($waiting17); ?></option>
														     <?php } if (!in_array($waiting18, $runningData) && !in_array($waiting18, $stoppedData) && !in_array($waiting18, $offData)) { ?>
														    	<option <?php if (in_array($waiting18,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting18; ?>'><?php echo displayColorName($waiting18); ?></option>
														     <?php } if (!in_array($waiting19, $runningData) && !in_array($waiting19, $stoppedData) && !in_array($waiting19, $offData)) { ?>
														    	<option <?php if (in_array($waiting19,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting19; ?>'><?php echo displayColorName($waiting19); ?></option>
														     <?php } if (!in_array($waiting10, $runningData) && !in_array($waiting10, $stoppedData) && !in_array($waiting10, $offData)) { ?>
														    	<option <?php if (in_array($waiting20,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting20; ?>'><?php echo displayColorName($waiting20); ?></option>
														     <?php } if (!in_array($waiting21, $runningData) && !in_array($waiting21, $stoppedData) && !in_array($waiting21, $offData)) { ?>
														    	<option <?php if (in_array($waiting21,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting21; ?>'><?php echo displayColorName($waiting21); ?></option>
														     <?php } if (!in_array($waiting22, $runningData) && !in_array($waiting22, $stoppedData) && !in_array($waiting22, $offData)) { ?>
														    	<option <?php if (in_array($waiting22,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting22; ?>'><?php echo displayColorName($waiting22); ?></option>
														     <?php } if (!in_array($waiting23, $runningData) && !in_array($waiting23, $stoppedData) && !in_array($waiting23, $offData)) { ?>
														    	<option <?php if (in_array($waiting23,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting23; ?>'><?php echo displayColorName($waiting23); ?></option>
														     <?php } if (!in_array($waiting24, $runningData) && !in_array($waiting24, $stoppedData) && !in_array($waiting24, $offData)) { ?>
														    	<option <?php if (in_array($waiting24,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting24; ?>'><?php echo displayColorName($waiting24); ?></option>
														     <?php } if (!in_array($waiting25, $runningData) && !in_array($waiting25, $stoppedData) && !in_array($waiting25, $offData)) { ?>
														    	<option <?php if (in_array($waiting25,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting25; ?>'><?php echo displayColorName($waiting25); ?></option>
														     <?php } if (!in_array($waiting26, $runningData) && !in_array($waiting26, $stoppedData) && !in_array($waiting26, $offData)) { ?>
														    	<option <?php if (in_array($waiting26,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting26; ?>'><?php echo displayColorName($waiting26); ?></option>
														    <?php }   if (!in_array($waiting27, $runningData) && !in_array($waiting27, $stoppedData) && !in_array($waiting27, $offData)) { ?>
														    	<option <?php if (in_array($waiting27,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting27; ?>'><?php echo displayColorName($waiting27); ?></option>
														    <?php }   if (!in_array($waiting28, $runningData) && !in_array($waiting28, $stoppedData) && !in_array($waiting28, $offData)) { ?>
														    	<option <?php if (in_array($waiting28,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting28; ?>'><?php echo displayColorName($waiting28); ?></option>
														    <?php }   if (!in_array($waiting29, $runningData) && !in_array($waiting29, $stoppedData) && !in_array($waiting29, $offData)) { ?>
														    	<option <?php if (in_array($waiting29,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting29; ?>'><?php echo displayColorName($waiting29); ?></option>
														    <?php }   if (!in_array($waiting30, $runningData) && !in_array($waiting30, $stoppedData) && !in_array($waiting30, $offData)) { ?>
														    	<option <?php if (in_array($waiting30,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting30; ?>'><?php echo displayColorName($waiting30); ?></option>
														    <?php }  if (!in_array($waiting31, $runningData) && !in_array($waiting31, $stoppedData) && !in_array($waiting31, $offData)) { ?>
														    	<option <?php if (in_array($waiting31,$waitingData)) { echo "selected";	} ?> value='<?php echo $waiting31; ?>'><?php echo displayColorName($waiting31); ?></option>
														    <?php }?>

													    	  <?php } ?>
															</select>
														</div>
													</div>

													<div style="padding: 0 5px;" class="form-group row" data-toggle="tooltip" data-title="Loading" >
														<div class="col-md-3">
															<label><?php echo Stopped; ?></label>
														</div>
														<div class="col-md-9">
															<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="stopped<?php echo $value['machineId']; ?>" onchange="stacklight_disabled_color_value('stopped',<?php echo $value['machineId']; ?>)" name="stopped[]" placeholder="<?php echo SelectStopped; ?>" >
																<?php if ($machineLightcount == 1) { 
																$stopped1 = $machine_color_name[0];


															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $offData)) { 

															?>
																<option <?php if (in_array("off",$stoppedData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($stopped1, $runningData) && !in_array($stopped1, $waitingData) && !in_array($stopped1, $offData)) { ?> 
				    											<option <?php if (in_array($stopped1,$stoppedData)) { echo "selected"; } ?> value='<?php echo  $machine_color_name[0]; ?>'><?php echo  $stopped1; ?></option>
				    										<?php } ?>
															<?php } elseif($machineLightcount == 2){ 

																$stopped1 = $machine_color_name[0];
																$stopped2 = $machine_color_name[1];
																$stopped3 = $machine_color_name[0].','.$machine_color_name[1];
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $offData)) { 

															?>
																<option <?php if (in_array("off",$stoppedData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($stopped1, $runningData) && !in_array($stopped1, $waitingData) && !in_array($stopped1, $offData)) { ?> 
														    	<option <?php if (in_array($stopped1,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped1; ?>'><?php echo $stopped1; ?></option>
														    <?php } if (!in_array($stopped2, $runningData) && !in_array($stopped2, $waitingData) && !in_array($stopped2, $offData)) { ?> 
														    	<option <?php if (in_array($stopped2,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped2; ?>'><?php echo $stopped2; ?></option>
														    <?php } if (!in_array($stopped3, $runningData) && !in_array($stopped3, $waitingData) && !in_array($stopped3, $offData)) { ?> 
														    	<option <?php if (in_array($stopped3,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped3; ?>'><?php echo displayColorName($stopped3); ?> </option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 3) {

																$stopped1 = $machine_color_name[0];
																$stopped2 = $machine_color_name[1];
																$stopped3 = $machine_color_name[2];
																$stopped4 = $machine_color_name[0].','.$machine_color_name[1];
																$stopped5 = $machine_color_name[0].','.$machine_color_name[2];
																$stopped6 = $machine_color_name[1].','.$machine_color_name[2];
																$stopped7 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
															 if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $offData)) { 

															?>
																<option <?php if (in_array("off",$stoppedData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($stopped1, $runningData) && !in_array($stopped1, $waitingData) && !in_array($stopped1, $offData)) { ?> 
														    	<option <?php if (in_array($stopped1,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped1; ?>'><?php echo displayColorName($stopped1); ?></option>
														    <?php } if (!in_array($stopped2, $runningData) && !in_array($stopped2, $waitingData) && !in_array($stopped2, $offData)) { ?> 
														    	<option <?php if (in_array($stopped2,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped2; ?>'><?php echo displayColorName($stopped2); ?></option>
														    <?php } if (!in_array($stopped3, $runningData) && !in_array($stopped3, $waitingData) && !in_array($stopped3, $offData)) { ?> 
														    	<option <?php if (in_array($stopped3,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped3; ?>'><?php echo displayColorName($stopped3); ?></option>
														    <?php } if (!in_array($stopped4, $runningData) && !in_array($stopped4, $waitingData) && !in_array($stopped4, $offData)) { ?> 	
														    	<option <?php if (in_array($stopped4,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped4; ?>'><?php echo displayColorName($stopped4); ?></option>
														    <?php } if (!in_array($stopped5, $runningData) && !in_array($stopped5, $waitingData) && !in_array($stopped5, $offData)) { ?> 
														    	<option <?php if (in_array($stopped5,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped5; ?>'><?php echo displayColorName($stopped5); ?></option>
														    <?php } if (!in_array($stopped6, $runningData) && !in_array($stopped6, $waitingData) && !in_array($stopped6, $offData)) { ?> 
														    	<option <?php if (in_array($stopped6,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped6; ?>'><?php echo displayColorName($stopped6); ?></option>
														    <?php } if (!in_array($stopped7, $runningData) && !in_array($stopped7, $waitingData) && !in_array($stopped7, $offData)) { ?> 
														    	<option <?php if (in_array($stopped7,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped7; ?>'><?php echo displayColorName($stopped7); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 4) {

																$stopped1 = $machine_color_name[0];
																$stopped2 = $machine_color_name[1];
																$stopped3 = $machine_color_name[2];
																$stopped4 = $machine_color_name[3];
																$stopped5 = $machine_color_name[0].','.$machine_color_name[1];
																$stopped6 = $machine_color_name[0].','.$machine_color_name[2];
																$stopped7 = $machine_color_name[0].','.$machine_color_name[3];
																$stopped8 = $machine_color_name[1].','.$machine_color_name[2];
																$stopped9 = $machine_color_name[1].','.$machine_color_name[3];
																$stopped10 = $machine_color_name[2].','.$machine_color_name[3];
																$stopped11 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$stopped12 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$stopped13 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$stopped14 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$stopped15 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
															 if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $offData)) { 

															?>
																<option <?php if (in_array("off",$stoppedData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($stopped1, $runningData) && !in_array($stopped1, $waitingData) && !in_array($stopped1, $offData)) { ?> 
															    <option <?php if (in_array($stopped1,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped1; ?>'><?php echo displayColorName($stopped1); ?></option>
															<?php } if (!in_array($stopped2, $runningData) && !in_array($stopped2, $waitingData) && !in_array($stopped2, $offData)) { ?> 
															    <option <?php if (in_array($stopped2,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped2; ?>'><?php echo displayColorName($stopped2); ?></option>
															<?php } if (!in_array($stopped3, $runningData) && !in_array($stopped3, $waitingData) && !in_array($stopped3, $offData)) { ?> 
														    	<option <?php if (in_array($stopped3,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped3; ?>'><?php echo displayColorName($stopped3); ?></option>
														    <?php } if (!in_array($stopped4, $runningData) && !in_array($stopped4, $waitingData) && !in_array($stopped4, $offData)) { ?>  	
														    	<option <?php if (in_array($stopped4,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped4; ?>'><?php echo displayColorName($stopped4); ?></option>
														    <?php } if (!in_array($stopped5, $runningData) && !in_array($stopped5, $waitingData) && !in_array($stopped5, $offData)) { ?> 
														    	<option <?php if (in_array($stopped5,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped5; ?>'><?php echo displayColorName($stopped5); ?></option>
														    <?php } if (!in_array($stopped6, $runningData) && !in_array($stopped6, $waitingData) && !in_array($stopped6, $offData)) { ?> 
														    	<option <?php if (in_array($stopped6,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped6; ?>'><?php echo displayColorName($stopped6); ?></option>
														    <?php } if (!in_array($stopped7, $runningData) && !in_array($stopped7, $waitingData) && !in_array($stopped7, $offData)) { ?> 
														    	<option <?php if (in_array($stopped7,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped7; ?>'><?php echo displayColorName($stopped7); ?></option>
														    <?php } if (!in_array($stopped8, $runningData) && !in_array($stopped8, $waitingData) && !in_array($stopped8, $offData)) { ?> 
														    	<option <?php if (in_array($stopped8,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped8; ?>'><?php echo displayColorName($stopped8); ?></option>
														    <?php } if (!in_array($stopped9, $runningData) && !in_array($stopped9, $waitingData) && !in_array($stopped9, $offData)) { ?> 
															    <option <?php if (in_array($stopped9,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped9; ?>'><?php echo displayColorName($stopped9); ?></option>
															<?php } if (!in_array($stopped10, $runningData) && !in_array($stopped10, $waitingData) && !in_array($stopped10, $offData)) { ?> 
														    	<option <?php if (in_array($stopped10,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped10; ?>'><?php echo displayColorName($stopped10); ?></option>
														    <?php } if (!in_array($stopped11, $runningData) && !in_array($stopped11, $waitingData) && !in_array($stopped11, $offData)) { ?> 
														    	<option <?php if (in_array($stopped11,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped11; ?>'><?php echo displayColorName($stopped11); ?></option>
														    <?php } if (!in_array($stopped12, $runningData) && !in_array($stopped12, $waitingData) && !in_array($stopped12, $offData)) { ?> 
														    	<option <?php if (in_array($stopped12,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped12; ?>'><?php echo displayColorName($stopped12); ?></option>
														    <?php } if (!in_array($stopped13, $runningData) && !in_array($stopped13, $waitingData) && !in_array($stopped13, $offData)) { ?> 
														    	<option <?php if (in_array($stopped13,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped13; ?>'><?php echo displayColorName($stopped13); ?></option>
														    <?php } if (!in_array($stopped14, $runningData) && !in_array($stopped14, $waitingData) && !in_array($stopped14, $offData)) { ?> 
														    	<option <?php if (in_array($stopped14,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped14; ?>'><?php echo displayColorName($stopped14); ?></option>
														    <?php } if (!in_array($stopped15, $runningData) && !in_array($stopped15, $waitingData) && !in_array($stopped15, $offData)) { ?> 
														    	<option <?php if (in_array($stopped15,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped15; ?>'><?php echo displayColorName($stopped15); ?></option>
														    <?php } ?>

													    	  <?php } elseif ($machineLightcount == 5) {

																$stopped1 = $machine_color_name[0];
																$stopped2 = $machine_color_name[1];
																$stopped3 = $machine_color_name[2];
																$stopped4 = $machine_color_name[3];
																$stopped5 = $machine_color_name[4];
																$stopped6 = $machine_color_name[0].','.$machine_color_name[1];
																$stopped7 = $machine_color_name[0].','.$machine_color_name[2];
																$stopped8 = $machine_color_name[0].','.$machine_color_name[3];
																$stopped9 = $machine_color_name[0].','.$machine_color_name[4];
																$stopped10 = $machine_color_name[1].','.$machine_color_name[2];
																$stopped11 = $machine_color_name[1].','.$machine_color_name[3];
																$stopped12 = $machine_color_name[1].','.$machine_color_name[4];
																$stopped13 = $machine_color_name[2].','.$machine_color_name[3];
																$stopped14 = $machine_color_name[2].','.$machine_color_name[4];
																$stopped15 = $machine_color_name[3].','.$machine_color_name[4];
																$stopped16 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$stopped17 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$stopped18 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[4];
																$stopped19 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$stopped20 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[4];
																$stopped21 = $machine_color_name[0].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped22 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$stopped23 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$stopped24 = $machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped25 = $machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped26 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$stopped27 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$stopped28 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped29 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped30 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$stopped31 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																
															 if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $offData)) { 

															?>
																<option <?php if (in_array("off",$stoppedData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($stopped1, $runningData) && !in_array($stopped1, $waitingData) && !in_array($stopped1, $offData)) { ?> 
															    <option <?php if (in_array($stopped1,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped1; ?>'><?php echo displayColorName($stopped1); ?></option>
															<?php } if (!in_array($stopped2, $runningData) && !in_array($stopped2, $waitingData) && !in_array($stopped2, $offData)) { ?> 
															    <option <?php if (in_array($stopped2,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped2; ?>'><?php echo displayColorName($stopped2); ?></option>
															<?php } if (!in_array($stopped3, $runningData) && !in_array($stopped3, $waitingData) && !in_array($stopped3, $offData)) { ?> 
														    	<option <?php if (in_array($stopped3,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped3; ?>'><?php echo displayColorName($stopped3); ?></option>
														    <?php } if (!in_array($stopped4, $runningData) && !in_array($stopped4, $waitingData) && !in_array($stopped4, $offData)) { ?> 

														    	<option <?php if (in_array($stopped4,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped4; ?>'><?php echo displayColorName($stopped4); ?></option>
														    <?php } if (!in_array($stopped5, $runningData) && !in_array($stopped5, $waitingData) && !in_array($stopped5, $offData)) { ?> 
														    	<option <?php if (in_array($stopped5,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped5; ?>'><?php echo displayColorName($stopped5); ?></option>
														    <?php } if (!in_array($stopped6, $runningData) && !in_array($stopped6, $waitingData) && !in_array($stopped6, $offData)) { ?> 
														    	<option <?php if (in_array($stopped6,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped6; ?>'><?php echo displayColorName($stopped6); ?></option>
														    <?php } if (!in_array($stopped7, $runningData) && !in_array($stopped7, $waitingData) && !in_array($stopped7, $offData)) { ?> 
														    	<option <?php if (in_array($stopped7,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped7; ?>'><?php echo displayColorName($stopped7); ?></option>
														    <?php } if (!in_array($stopped8, $runningData) && !in_array($stopped8, $waitingData) && !in_array($stopped8, $offData)) { ?> 
														    	<option <?php if (in_array($stopped8,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped8; ?>'><?php echo displayColorName($stopped8); ?></option>
														    <?php } if (!in_array($stopped9, $runningData) && !in_array($stopped9, $waitingData) && !in_array($stopped9, $offData)) { ?> 
															    <option <?php if (in_array($stopped9,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped9; ?>'><?php echo displayColorName($stopped9); ?></option>
															<?php } if (!in_array($stopped10, $runningData) && !in_array($stopped10, $waitingData) && !in_array($stopped10, $offData)) { ?> 
														    	<option <?php if (in_array($stopped10,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped10; ?>'><?php echo displayColorName($stopped10); ?></option>
														    <?php } if (!in_array($stopped11, $runningData) && !in_array($stopped11, $waitingData) && !in_array($stopped11, $offData)) { ?> 
														    	<option <?php if (in_array($stopped11,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped11; ?>'><?php echo displayColorName($stopped11); ?></option>
														    <?php } if (!in_array($stopped12, $runningData) && !in_array($stopped12, $waitingData) && !in_array($stopped12, $offData)) { ?> 
														    	<option <?php if (in_array($stopped12,$stoppedData)) { echo "selected"; } ?> value='<?php echo $stopped12; ?>'><?php echo displayColorName($stopped12); ?></option>
														    <?php } if (!in_array($stopped13, $runningData) && !in_array($stopped13, $waitingData) && !in_array($stopped13, $offData)) { ?> 
														    	<option <?php if (in_array($stopped13,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped13; ?>'><?php echo displayColorName($stopped13); ?></option>
														    <?php } if (!in_array($stopped14, $runningData) && !in_array($stopped14, $waitingData) && !in_array($stopped14, $offData)) { ?> 
														    	<option <?php if (in_array($stopped14,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped14; ?>'><?php echo displayColorName($stopped14); ?></option>
														    <?php } if (!in_array($stopped15, $runningData) && !in_array($stopped15, $waitingData) && !in_array($stopped15, $offData)) { ?> 
														    	<option <?php if (in_array($stopped15,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped15; ?>'><?php echo displayColorName($stopped15); ?></option>
														    <?php } if (!in_array($stopped16, $runningData) && !in_array($stopped16, $waitingData) && !in_array($stopped16, $offData)) { ?> 
														    	<option <?php if (in_array($stopped16,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped16; ?>'><?php echo displayColorName($stopped16); ?></option>
														    <?php } if (!in_array($stopped17, $runningData) && !in_array($stopped17, $waitingData) && !in_array($stopped17, $offData)) { ?> 
														    	<option <?php if (in_array($stopped17,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped17; ?>'><?php echo displayColorName($stopped17); ?></option>
														    <?php } if (!in_array($stopped18, $runningData) && !in_array($stopped18, $waitingData) && !in_array($stopped18, $offData)) { ?> 
														    	<option <?php if (in_array($stopped18,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped18; ?>'><?php echo displayColorName($stopped18); ?></option>
														    <?php } if (!in_array($stopped19, $runningData) && !in_array($stopped19, $waitingData) && !in_array($stopped19, $offData)) { ?> 
														    	<option <?php if (in_array($stopped19,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped19; ?>'><?php echo displayColorName($stopped19); ?></option>
														    <?php } if (!in_array($stopped20, $runningData) && !in_array($stopped20, $waitingData) && !in_array($stopped20, $offData)) { ?> 
														    	<option <?php if (in_array($stopped20,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped20; ?>'><?php echo displayColorName($stopped20); ?></option>
														    <?php } if (!in_array($stopped21, $runningData) && !in_array($stopped21, $waitingData) && !in_array($stopped21, $offData)) { ?> 
														    	<option <?php if (in_array($stopped21,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped21; ?>'><?php echo displayColorName($stopped21); ?></option>
														    <?php } if (!in_array($stopped22, $runningData) && !in_array($stopped22, $waitingData) && !in_array($stopped22, $offData)) { ?> 
														    	<option <?php if (in_array($stopped22,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped22; ?>'><?php echo displayColorName($stopped22); ?></option>
														    <?php } if (!in_array($stopped23, $runningData) && !in_array($stopped23, $waitingData) && !in_array($stopped23, $offData)) { ?> 
														    	<option <?php if (in_array($stopped23,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped23; ?>'><?php echo displayColorName($stopped23); ?></option>
														    <?php } if (!in_array($stopped24, $runningData) && !in_array($stopped24, $waitingData) && !in_array($stopped24, $offData)) { ?> 
														    	<option <?php if (in_array($stopped24,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped24; ?>'><?php echo displayColorName($stopped24); ?></option>
														    <?php } if (!in_array($stopped25, $runningData) && !in_array($stopped25, $waitingData) && !in_array($stopped25, $offData)) { ?> 
														    	<option <?php if (in_array($stopped25,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped25; ?>'><?php echo displayColorName($stopped25); ?></option>
														    <?php } if (!in_array($stopped26, $runningData) && !in_array($stopped26, $waitingData) && !in_array($stopped26, $offData)) { ?> 
														    	<option <?php if (in_array($stopped26,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped26; ?>'><?php echo displayColorName($stopped26); ?></option>
														    <?php }   if (!in_array($stopped27, $runningData) && !in_array($stopped27, $waitingData) && !in_array($stopped27, $offData)) { ?>
														    	<option <?php if (in_array($stopped27,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped27; ?>'><?php echo displayColorName($stopped27); ?></option>
														    <?php }   if (!in_array($stopped28, $runningData) && !in_array($stopped28, $waitingData) && !in_array($stopped28, $offData)) { ?>
														    	<option <?php if (in_array($stopped28,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped28; ?>'><?php echo displayColorName($stopped28); ?></option>
														    <?php }   if (!in_array($stopped29, $runningData) && !in_array($stopped29, $waitingData) && !in_array($stopped29, $offData)) { ?>
														    	<option <?php if (in_array($stopped29,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped29; ?>'><?php echo displayColorName($stopped29); ?></option>
														    <?php }   if (!in_array($stopped30, $runningData) && !in_array($stopped30, $waitingData) && !in_array($stopped30, $offData)) { ?>
														    	<option <?php if (in_array($stopped30,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped30; ?>'><?php echo displayColorName($stopped30); ?></option>
														    <?php }   if (!in_array($stopped31, $runningData) && !in_array($stopped31, $waitingData) && !in_array($stopped31, $offData)) { ?>
														    	<option <?php if (in_array($stopped31,$stoppedData)) { echo "selected";	} ?> value='<?php echo $stopped31; ?>'><?php echo displayColorName($stopped31); ?></option>
														    <?php } ?>
															
													    	  <?php } ?>
															</select>
														</div>
													</div>

													<div style="padding: 0 5px;" class="form-group row" data-toggle="tooltip" data-title="Loading" >
														<div class="col-md-3">
															<label><?php echo Off; ?></label>
														</div>
														<div class="col-md-9">
															<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="off<?php echo $value['machineId']; ?>" onchange="stacklight_disabled_color_value('off',<?php echo $value['machineId']; ?>)" name="off[]" placeholder="<?php echo SelectOff; ?>" >
																<?php if ($machineLightcount == 1) { 
																$off1 = $machine_color_name[0];
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $stoppedData)) {
															?>
																<option <?php if (in_array("off",$offData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($off1, $runningData) && !in_array($off1, $waitingData) && !in_array($off1, $stoppedData)) { ?> 
				    											<option <?php if (in_array($off1,$offData)) { echo "selected"; } ?> value='<?php echo  $machine_color_name[0]; ?>'><?php echo  $off1; ?></option>
				    										<?php } ?>
															<?php } elseif($machineLightcount == 2){ 

																$off1 = $machine_color_name[0];
																$off2 = $machine_color_name[1];
																$off3 = $machine_color_name[0].','.$machine_color_name[1];
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $stoppedData)) {
															?>
																<option <?php if (in_array("off",$offData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($off1, $runningData) && !in_array($off1, $waitingData) && !in_array($off1, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off1,$offData)) { echo "selected"; } ?> value='<?php echo $off1; ?>'><?php echo $off1; ?></option>
														    <?php } if (!in_array($off2, $runningData) && !in_array($off2, $waitingData) && !in_array($off2, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off2,$offData)) { echo "selected"; } ?> value='<?php echo $off2; ?>'><?php echo $off2; ?></option>
														    <?php } if (!in_array($off3, $runningData) && !in_array($off3, $waitingData) && !in_array($off3, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off3,$offData)) { echo "selected"; } ?> value='<?php echo $off3; ?>'><?php echo displayColorName($off3); ?> </option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 3) {

																$off1 = $machine_color_name[0];
																$off2 = $machine_color_name[1];
																$off3 = $machine_color_name[2];
																$off4 = $machine_color_name[0].','.$machine_color_name[1];
																$off5 = $machine_color_name[0].','.$machine_color_name[2];
																$off6 = $machine_color_name[1].','.$machine_color_name[2];
																$off7 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $stoppedData)) {
															?>
																<option <?php if (in_array("off",$offData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($off1, $runningData) && !in_array($off1, $waitingData) && !in_array($off1, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off1,$offData)) { echo "selected"; } ?> value='<?php echo $off1; ?>'><?php echo displayColorName($off1); ?></option>
														    <?php } if (!in_array($off2, $runningData) && !in_array($off2, $waitingData) && !in_array($off2, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off2,$offData)) { echo "selected"; } ?> value='<?php echo $off2; ?>'><?php echo displayColorName($off2); ?></option>
														    <?php } if (!in_array($off3, $runningData) && !in_array($off3, $waitingData) && !in_array($off3, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off3,$offData)) { echo "selected";	} ?> value='<?php echo $off3; ?>'><?php echo displayColorName($off3); ?></option>
														    <?php } if (!in_array($off4, $runningData) && !in_array($off4, $waitingData) && !in_array($off4, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off4,$offData)) { echo "selected";	} ?> value='<?php echo $off4; ?>'><?php echo displayColorName($off4); ?></option>
														    <?php } if (!in_array($off5, $runningData) && !in_array($off5, $waitingData) && !in_array($off5, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off5,$offData)) { echo "selected"; } ?> value='<?php echo $off5; ?>'><?php echo displayColorName($off5); ?></option>
														    <?php } if (!in_array($off6, $runningData) && !in_array($off6, $waitingData) && !in_array($off6, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off6,$offData)) { echo "selected";	} ?> value='<?php echo $off6; ?>'><?php echo displayColorName($off6); ?></option>
														    <?php } if (!in_array($off7, $runningData) && !in_array($off7, $waitingData) && !in_array($off7, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off7,$offData)) { echo "selected";	} ?> value='<?php echo $off7; ?>'><?php echo displayColorName($off7); ?></option>
														    <?php } ?>
															<?php } elseif ($machineLightcount == 4) {

																$off1 = $machine_color_name[0];
																$off2 = $machine_color_name[1];
																$off3 = $machine_color_name[2];
																$off4 = $machine_color_name[3];
																$off5 = $machine_color_name[0].','.$machine_color_name[1];
																$off6 = $machine_color_name[0].','.$machine_color_name[2];
																$off7 = $machine_color_name[0].','.$machine_color_name[3];
																$off8 = $machine_color_name[1].','.$machine_color_name[2];
																$off9 = $machine_color_name[1].','.$machine_color_name[3];
																$off10 = $machine_color_name[2].','.$machine_color_name[3];
																$off11 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$off12 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$off13 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$off14 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$off15 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $stoppedData)) {
															?>
																<option <?php if (in_array("off",$offData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($off1, $runningData) && !in_array($off1, $waitingData) && !in_array($off1, $stoppedData)) { ?> 
															    <option <?php if (in_array($off1,$offData)) { echo "selected"; } ?> value='<?php echo $off1; ?>'><?php echo displayColorName($off1); ?></option>
															<?php } if (!in_array($off2, $runningData) && !in_array($off2, $waitingData) && !in_array($off2, $stoppedData)) { ?> 
															    <option <?php if (in_array($off2,$offData)) { echo "selected"; } ?> value='<?php echo $off2; ?>'><?php echo displayColorName($off2); ?></option>
															<?php } if (!in_array($off3, $runningData) && !in_array($off3, $waitingData) && !in_array($off3, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off3,$offData)) { echo "selected";	} ?> value='<?php echo $off3; ?>'><?php echo displayColorName($off3); ?></option>
														    <?php } if (!in_array($off4, $runningData) && !in_array($off4, $waitingData) && !in_array($off4, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off4,$offData)) { echo "selected";	} ?> value='<?php echo $off4; ?>'><?php echo displayColorName($off4); ?></option>
														    <?php } if (!in_array($off5, $runningData) && !in_array($off5, $waitingData) && !in_array($off5, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off5,$offData)) { echo "selected"; } ?> value='<?php echo $off5; ?>'><?php echo displayColorName($off5); ?></option>
														    <?php } if (!in_array($off6, $runningData) && !in_array($off6, $waitingData) && !in_array($off6, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off6,$offData)) { echo "selected";	} ?> value='<?php echo $off6; ?>'><?php echo displayColorName($off6); ?></option>
														    <?php } if (!in_array($off7, $runningData) && !in_array($off7, $waitingData) && !in_array($off7, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off7,$offData)) { echo "selected";	} ?> value='<?php echo $off7; ?>'><?php echo displayColorName($off7); ?></option>
														    <?php } if (!in_array($off8, $runningData) && !in_array($off8, $waitingData) && !in_array($off8, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off8,$offData)) { echo "selected"; } ?> value='<?php echo $off8; ?>'><?php echo displayColorName($off8); ?></option>
														    <?php } if (!in_array($off9, $runningData) && !in_array($off9, $waitingData) && !in_array($off9, $stoppedData)) { ?> 
															    <option <?php if (in_array($off9,$offData)) { echo "selected"; } ?> value='<?php echo $off9; ?>'><?php echo displayColorName($off9); ?></option>
															<?php } if (!in_array($off10, $runningData) && !in_array($off10, $waitingData) && !in_array($off10, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off10,$offData)) { echo "selected";	} ?> value='<?php echo $off10; ?>'><?php echo displayColorName($off10); ?></option>
														    <?php } if (!in_array($off11, $runningData) && !in_array($off11, $waitingData) && !in_array($off11, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off11,$offData)) { echo "selected";	} ?> value='<?php echo $off11; ?>'><?php echo displayColorName($off11); ?></option>
														    <?php } if (!in_array($off12, $runningData) && !in_array($off12, $waitingData) && !in_array($off12, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off12,$offData)) { echo "selected"; } ?> value='<?php echo $off12; ?>'><?php echo displayColorName($off12); ?></option>
														    <?php } if (!in_array($off13, $runningData) && !in_array($off13, $waitingData) && !in_array($off13, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off13,$offData)) { echo "selected";	} ?> value='<?php echo $off13; ?>'><?php echo displayColorName($off13); ?></option>
														    <?php } if (!in_array($off14, $runningData) && !in_array($off14, $waitingData) && !in_array($off14, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off14,$offData)) { echo "selected";	} ?> value='<?php echo $off14; ?>'><?php echo displayColorName($off14); ?></option>
														    <?php } if (!in_array($off15, $runningData) && !in_array($off15, $waitingData) && !in_array($off15, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off15,$offData)) { echo "selected";	} ?> value='<?php echo $off15; ?>'><?php echo displayColorName($off15); ?></option>
														    <?php } ?>
													    	  <?php } elseif ($machineLightcount == 5) {

																$off1 = $machine_color_name[0];
																$off2 = $machine_color_name[1];
																$off3 = $machine_color_name[2];
																$off4 = $machine_color_name[3];
																$off5 = $machine_color_name[4];
																$off6 = $machine_color_name[0].','.$machine_color_name[1];
																$off7 = $machine_color_name[0].','.$machine_color_name[2];
																$off8 = $machine_color_name[0].','.$machine_color_name[3];
																$off9 = $machine_color_name[0].','.$machine_color_name[4];
																$off10 = $machine_color_name[1].','.$machine_color_name[2];
																$off11 = $machine_color_name[1].','.$machine_color_name[3];
																$off12 = $machine_color_name[1].','.$machine_color_name[4];
																$off13 = $machine_color_name[2].','.$machine_color_name[3];
																$off14 = $machine_color_name[2].','.$machine_color_name[4];
																$off15 = $machine_color_name[3].','.$machine_color_name[4];
																$off16 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2];
																$off17 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3];
																$off18 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[4];
																$off19 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3];
																$off20 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[4];
																$off21 = $machine_color_name[0].','.$machine_color_name[3].','.$machine_color_name[4];
																$off22 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$off23 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$off24 = $machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$off25 = $machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$off26 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3];
																$off27 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[4];
																$off28 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[3].','.$machine_color_name[4];
																$off29 = $machine_color_name[0].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$off30 = $machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																$off31 = $machine_color_name[0].','.$machine_color_name[1].','.$machine_color_name[2].','.$machine_color_name[3].','.$machine_color_name[4];
																
															if (!in_array("off", $runningData) && !in_array("off", $waitingData) && !in_array("off", $stoppedData)) {
															?>
																<option <?php if (in_array("off",$offData)) { echo "selected"; } ?> value='Off'><?php echo Off; ?></option>
															<?php } if (!in_array($off1, $runningData) && !in_array($off1, $waitingData) && !in_array($off1, $stoppedData)) { ?> 
															    <option <?php if (in_array($off1,$offData)) { echo "selected"; } ?> value='<?php echo $off1; ?>'><?php echo displayColorName($off1); ?></option>
															<?php } if (!in_array($off2, $runningData) && !in_array($off2, $waitingData) && !in_array($off2, $stoppedData)) { ?> 
															    <option <?php if (in_array($off2,$offData)) { echo "selected"; } ?> value='<?php echo $off2; ?>'><?php echo displayColorName($off2); ?></option>
															<?php } if (!in_array($off3, $runningData) && !in_array($off3, $waitingData) && !in_array($off3, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off3,$offData)) { echo "selected";	} ?> value='<?php echo $off3; ?>'><?php echo displayColorName($off3); ?></option>
														    <?php } if (!in_array($off4, $runningData) && !in_array($off4, $waitingData) && !in_array($off4, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off4,$offData)) { echo "selected";	} ?> value='<?php echo $off4; ?>'><?php echo displayColorName($off4); ?></option>
														    <?php } if (!in_array($off5, $runningData) && !in_array($off5, $waitingData) && !in_array($off5, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off5,$offData)) { echo "selected"; } ?> value='<?php echo $off5; ?>'><?php echo displayColorName($off5); ?></option>
														    <?php } if (!in_array($off6, $runningData) && !in_array($off6, $waitingData) && !in_array($off6, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off6,$offData)) { echo "selected";	} ?> value='<?php echo $off6; ?>'><?php echo displayColorName($off6); ?></option>
														    <?php } if (!in_array($off7, $runningData) && !in_array($off7, $waitingData) && !in_array($off7, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off7,$offData)) { echo "selected";	} ?> value='<?php echo $off7; ?>'><?php echo displayColorName($off7); ?></option>
														    <?php } if (!in_array($off8, $runningData) && !in_array($off8, $waitingData) && !in_array($off8, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off8,$offData)) { echo "selected"; } ?> value='<?php echo $off8; ?>'><?php echo displayColorName($off8); ?></option>
														    	
														    <?php } if (!in_array($off9, $runningData) && !in_array($off9, $waitingData) && !in_array($off9, $stoppedData)) { ?>
															    <option <?php if (in_array($off9,$offData)) { echo "selected"; } ?> value='<?php echo $off9; ?>'><?php echo displayColorName($off9); ?></option>
															<?php } if (!in_array($off10, $runningData) && !in_array($off10, $waitingData) && !in_array($off10, $stoppedData)) { ?>
														    	<option <?php if (in_array($off10,$offData)) { echo "selected";	} ?> value='<?php echo $off10; ?>'><?php echo displayColorName($off10); ?></option>
														    <?php } if (!in_array($off11, $runningData) && !in_array($off11, $waitingData) && !in_array($off11, $stoppedData)) { ?>
														    	<option <?php if (in_array($off11,$offData)) { echo "selected";	} ?> value='<?php echo $off11; ?>'><?php echo displayColorName($off11); ?></option>
														    <?php } if (!in_array($off12, $runningData) && !in_array($off12, $waitingData) && !in_array($off12, $stoppedData)) { ?>
														    	<option <?php if (in_array($off12,$offData)) { echo "selected"; } ?> value='<?php echo $off12; ?>'><?php echo displayColorName($off12); ?></option>
														    <?php } if (!in_array($off13, $runningData) && !in_array($off13, $waitingData) && !in_array($off13, $stoppedData)) { ?>
														    	<option <?php if (in_array($off13,$offData)) { echo "selected";	} ?> value='<?php echo $off13; ?>'><?php echo displayColorName($off13); ?></option>
														    <?php } if (!in_array($off14, $runningData) && !in_array($off14, $waitingData) && !in_array($off14, $stoppedData)) { ?>
														    	<option <?php if (in_array($off14,$offData)) { echo "selected";	} ?> value='<?php echo $off14; ?>'><?php echo displayColorName($off14); ?></option>
														    <?php } if (!in_array($off15, $runningData) && !in_array($off15, $waitingData) && !in_array($off15, $stoppedData)) { ?>
														    	<option <?php if (in_array($off15,$offData)) { echo "selected";	} ?> value='<?php echo $off15; ?>'><?php echo displayColorName($off15); ?></option>
														    <?php } if (!in_array($off16, $runningData) && !in_array($off16, $waitingData) && !in_array($off16, $stoppedData)) { ?>
														    	<option <?php if (in_array($off16,$offData)) { echo "selected";	} ?> value='<?php echo $off16; ?>'><?php echo displayColorName($off16); ?></option>
														    <?php } if (!in_array($off17, $runningData) && !in_array($off17, $waitingData) && !in_array($off17, $stoppedData)) { ?>
														    	<option <?php if (in_array($off17,$offData)) { echo "selected";	} ?> value='<?php echo $off17; ?>'><?php echo displayColorName($off17); ?></option>
														    <?php } if (!in_array($off18, $runningData) && !in_array($off18, $waitingData) && !in_array($off18, $stoppedData)) { ?>
														    	<option <?php if (in_array($off18,$offData)) { echo "selected";	} ?> value='<?php echo $off18; ?>'><?php echo displayColorName($off18); ?></option>
														    <?php } if (!in_array($off19, $runningData) && !in_array($off19, $waitingData) && !in_array($off19, $stoppedData)) { ?>
														    	<option <?php if (in_array($off19,$offData)) { echo "selected";	} ?> value='<?php echo $off19; ?>'><?php echo displayColorName($off19); ?></option>
														    <?php } if (!in_array($off20, $runningData) && !in_array($off20, $waitingData) && !in_array($off20, $stoppedData)) { ?>
														    	<option <?php if (in_array($off20,$offData)) { echo "selected";	} ?> value='<?php echo $off20; ?>'><?php echo displayColorName($off20); ?></option>
														    <?php } if (!in_array($off21, $runningData) && !in_array($off21, $waitingData) && !in_array($off21, $stoppedData)) { ?>
														    	<option <?php if (in_array($off21,$offData)) { echo "selected";	} ?> value='<?php echo $off21; ?>'><?php echo displayColorName($off21); ?></option>
														    <?php } if (!in_array($off22, $runningData) && !in_array($off22, $waitingData) && !in_array($off22, $stoppedData)) { ?>
														    	<option <?php if (in_array($off22,$offData)) { echo "selected";	} ?> value='<?php echo $off22; ?>'><?php echo displayColorName($off22); ?></option>
														    <?php } if (!in_array($off23, $runningData) && !in_array($off23, $waitingData) && !in_array($off23, $stoppedData)) { ?>
														    	<option <?php if (in_array($off23,$offData)) { echo "selected";	} ?> value='<?php echo $off23; ?>'><?php echo displayColorName($off23); ?></option>
														    <?php } if (!in_array($off24, $runningData) && !in_array($off24, $waitingData) && !in_array($off24, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off24,$offData)) { echo "selected";	} ?> value='<?php echo $off24; ?>'><?php echo displayColorName($off24); ?></option>
														    <?php } if (!in_array($off25, $runningData) && !in_array($off25, $waitingData) && !in_array($off25, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off25,$offData)) { echo "selected";	} ?> value='<?php echo $off25; ?>'><?php echo displayColorName($off25); ?></option>
														    <?php } if (!in_array($off26, $runningData) && !in_array($off26, $waitingData) && !in_array($off26, $stoppedData)) { ?> 
														    	<option <?php if (in_array($off26,$offData)) { echo "selected";	} ?> value='<?php echo $off26; ?>'><?php echo displayColorName($off26); ?></option>
														    <?php }   if (!in_array($off27, $runningData) && !in_array($off27, $waitingData) && !in_array($off27, $stoppedData)) { ?>
														    	<option <?php if (in_array($off27,$offData)) { echo "selected";	} ?> value='<?php echo $off27; ?>'><?php echo displayColorName($off27); ?></option>
														    <?php }   if (!in_array($off28, $runningData) && !in_array($off28, $waitingData) && !in_array($off28, $stoppedData)) { ?>
														    	<option <?php if (in_array($off28,$offData)) { echo "selected";	} ?> value='<?php echo $off28; ?>'><?php echo displayColorName($off28); ?></option>
														    <?php }   if (!in_array($off29, $runningData) && !in_array($off29, $waitingData) && !in_array($off29, $stoppedData)) { ?>
														    	<option <?php if (in_array($off29,$offData)) { echo "selected";	} ?> value='<?php echo $off29; ?>'><?php echo displayColorName($off29); ?></option>
														    <?php }   if (!in_array($off30, $runningData) && !in_array($off30, $waitingData) && !in_array($off30, $stoppedData)) { ?>
														    	<option <?php if (in_array($off30,$offData)) { echo "selected";	} ?> value='<?php echo $off30; ?>'><?php echo displayColorName($off30); ?></option>
														    <?php }   if (!in_array($off31, $runningData) && !in_array($off31, $waitingData) && !in_array($off31, $stoppedData)) { ?>
														    	<option <?php if (in_array($off31,$offData)) { echo "selected";	} ?> value='<?php echo $off31; ?>'><?php echo displayColorName($off31); ?></option>
														    <?php } ?>
															
													    	  <?php } 

													    	  unset($runningData);
													    	  unset($stoppedData);
													    	  unset($offData);
													    	  unset($waitingData);

													    	  ?>	
															</select>
														</div>
													</div>
												</div>
											</div>
											
											<center><button type="submit" class="btn btn-sm btn-primary m-r-5 add_stacklight_configuration_submit" ><?php echo save; ?></button></center>
										</form>
									</div>
									
									<script type="text/javascript">

								
									</script>
								</div>
							</div>
					    </div>

					    
					   	
<?php 	unset($machine_color_name); ?>
		<?php if($accessPointMachineView == true) { ?>
			<style>
				@media only screen and (min-device-width: 1201px) and (max-device-width: 1310px) 
				{
	  			.smalllaptopsizeRes
	  			{
	  				min-width: 50%!important;
	  			}
				}
							</style>
		<div class="col-lg-3 col-md-12 col-sm-12 smalllaptopsizeRes" style="min-width: 240px!important;">
			<div class="card boxShadow" style="min-height: 300px;"> 
					
				<div class="card-img-overlay">

					<h4 class="card-title">
						<span  id="machineName1">
							<a class="machineNameText"style="color: #FF8000;"href="#modal-edit-machine<?php echo $value['machineId']; ?>"data-toggle="modal">
							<span data-toggle="tooltip" data-title="<?php echo $value['machineName']; ?>"> <?php echo $value['machineName']; ?> </span>
						</a>
						</span>

						<?php if ($value['noStacklight'] == "0") { ?>

						<a href="javascript:void(0);" style="right: 82px;position: absolute;color: #FF8000;" onclick="restartSetApp(<?php echo $value['machineId']; ?>)"><i class="fa fa-retweet" aria-hidden="true"></i></a>
						
						<a href="javascript:;" class="card-top-right bettery_per" id="battery_percentage<?php echo $value['machineId']; ?>" data-toggle="tooltip" data-title="<?php echo Battery?> <?php echo ($value['phoneDetail']->phoneId == 0) ? 0 : $value['batteryLevel'];  ?>%">
							

							<div>
							<i style="margin-left: -42px;margin-top: 1px;color: #FF8000;" id="chargeFlag<?php echo $value['machineId']; ?>" class="fa fa-bolt ChargeFlag <?php if ($value['ChargeFlag'] != "1") { echo "hide"; } ?>" ></i>
							  <div class="batteryContainer">
								<div class="batteryOuter" id="batteryOuter<?php echo $value['machineId']; ?>" style="border-color: <?php echo ($value['batteryLevel'] >= 50) ? "#76BA1B" : "#F60100"; ?>">
									<div class="batteryLevel" id="batteryLevelO<?php echo $value['machineId']; ?>" style="width: <?php echo ($value['phoneDetail']->phoneId == 0) ? 0 : $value['batteryLevel'];  ?>%;background-color: <?php echo ($value['batteryLevel'] >= 50) ? "#76BA1B" : "#F60100"; ?>  ;">
									</div>
								</div>
								<div class="batteryBump" id="batteryBump<?php echo $value['machineId']; ?>" style="background-color: <?php echo ($value['batteryLevel'] >= 50) ? "#76BA1B" : "#F60100"; ?>"></div>
							  </div>
							</div>
							<small class="ChargeFlag" style="color: <?php echo ($value['batteryLevel'] >= 50) ? "#76BA1B" : "#F60100"; ?>" id="textBatteryLevel<?php echo $value['machineId']; ?>"><?php echo ($value['phoneDetail']->phoneId == 0) ? 0 : $value['batteryLevel'];  ?>%
							</small>


							
						</a>
						<?php }else{ ?>	
						<?php } ?>
						<hr style="height: 2px !important;margin-top: 0.1rem !important;margin-bottom: -0.9rem !important;">
					</h4>

					<input type="hidden" id="liveDetectionState<?php echo $value['machineId']; ?>" value="<?php echo $value['machine_light_status']['statusText']; ?>">
					<input type="hidden" id="machineStatusDisconnect<?php echo $value['machineId']; ?>" value="<?php echo $value['machineStatus']; ?>">
					<?php if ($value['noStacklight'] == "0") { ?>
						
					<small><?php echo Code; ?> : <?php echo base64_decode($value['machineCode']); ?></small>
					<input type="hidden" id="isMachineNoStacklight<?php echo $value['machineId']; ?>" value="<?php echo $value['noStacklight']; ?>">
					<?php }else{ ?>
					<small>&nbsp;</small>
						<input type="hidden" id="isMachineNoStacklight<?php echo $value['machineId']; ?>" value="<?php echo $value['noStacklight']; ?>">
					<?php } ?>
					<div class="row card-content" style="margin-top: 20px;">
						<?php $colorCount = count($value['machine_light_colors_status_names']); ?>
						<div class="col-md-3" <?php if ($colorCount <= 3) { ?>
							style="max-width:25%;top: 37px;"
						<?php }else{ ?>
						style="max-width:25%;" <?php } ?> >

							<?php  
								if ($value['machineStatus'] == "0" && $value['noStacklight'] == "0") 
								{
									$cardLight = "block";
								}else
								{
									$cardLight = "none";
								}
							?>
							<div style="display: <?php echo $cardLight; ?>;" class="card-light" id="card-light<?php echo $value['machineId']; ?>"> 

								<?php

								for($p = 0; $p < $colorCount;$p++) {
								$status = $value['machine_light_colors_status_names'][$p];
								$firstLetter = strtoupper(substr($value['machine_light_colors_status_names'][$p], 0, 1));
								 ?>
									<div id="show_color<?php echo $firstLetter.'O'.$value['machineId']; ?>" 
										class="widget-img widget-img-sm <?php if($value['machine_light_status'][$status] == '1' && $value['machineSelected'] == '1' && $value['isSetAppOn'] == '1'){ 
										echo 'rounded' .' '.$value['machine_light_colors_status_names'][$p]; } else { echo $value['machine_light_colors_status_names'][$p].'Round'; } ?>  m-r-5 m-b-5 m-l-5 m-t-5" >
									</div>
								<?php } ?>
								 
							</div>
						</div>
						<div class="col-md-9" style="max-width:75%;">

							<?php if($value['machineSelected'] == '0') { ?>

								    <?php	if ($value['machineStatus'] == "0") { ?>

								    	<?php if($value['phoneDetail']->phoneId == 0) { ?>
								    	<div class="detectionImage<?php echo $value['machineId']; ?>">
											<img style="width:77px!important;margin-left: 20px!important" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?>" style="color: #FF8000;">
											<div style="padding-bottom: 23px;">
											<?php echo Nosetappconnectedto; ?> <span class="machineNameText"> <?php echo $value['machineName']; ?> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
											</div>
										</div>
								    	<?php } else { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img id="playImage<?php echo $value['machineId']; ?>" onclick="startSetApp(<?php echo $value['machineId']; ?>,<?php echo $value['machineStatus']; ?>)" style="width: 100px;cursor: pointer;margin-left: 18px;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" style="min-height: 40px!important;color: #FF8000;">
											<?php echo SetAppturnedoffCheckyourphoneorcontactinfonytttechcom; ?>
										</div>
									<?php } ?>
									<?php }else if ($value['machineStatus'] == "2") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/noproductionSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">
											<?php } ?>
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php }else { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/setupSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">
											<?php } ?>
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php } ?>


									<input type="hidden" id="detectionMachineName<?php echo $value['machineId']; ?>" value="<?php echo $value['machineName']; ?>">
									<div class="m-t-10 m-r-10 m-l-25" style="">

								<?php $user_online = $value['user_online'];

								if (is_array($user_online) && !empty($user_online)) 
								{ 
									$user_onlineText = implode(", ", $user_online); 
									$user_onlineText = (count($user_online) == 1) ? $user_onlineText." ".isonline."." :  $user_onlineText." ".areonline.".";
									$color = "#76BA1B";
							    }
							    else
							    {
							    	$user_onlineText = Nousersonline;
							    	$color = "#F60100";
							    } 
								?>
								
								<a style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;" href="<?php echo base_url('Operator/operator_checkin_checkout'); ?>" class="btn btn-primary" data-toggle="tooltip" data-title="<?php echo $user_onlineText; ?>">
									<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/user.svg'); ?>">
									<span class="badge badge-light" style="background: <?php echo $color; ?>">&nbsp;</span>
								</a>

								<form method="post" action="<?php echo base_url('Analytics/dashboard');?>" style="display:inline;" class="productions" > 

								<input type="hidden" name="machineId" value="<?php echo $value['machineId']; ?>" />
									<button type="submit" style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;margin-left: 5px !important;margin-right: 5px !important;" class="btn btn-primary" data-toggle="tooltip" data-title="<?php echo Machineworkingat; ?> <?php echo  number_format($value['working_per'],2); ?>% <?php echo Today; ?>">
										<img class="dropdownimagestyle" style="width:22px!important;height:22px!important;" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
									</button>
								</form>

								<a style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;" href="<?php echo base_url('Tasks/taskMaintenance'); ?>" class="btn btn-primary"  data-toggle="tooltip" data-title="<?php echo !empty($value['maintenanceUsers']) ? $value['maintenanceUsers'] : ''; ?>">
									<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/maintenace.svg'); ?>">
								</a>
								
								<?php if ($accessPointMachineEdit == true) { ?>

								
								<span id="filterTypeSelected" class="dropdown footerThreedot" style="right: 5px;bottom: 3px;position: absolute;">
										<span class="dropdown-toggle " data-toggle="dropdown" id="filterTypeSelectedValue"> 
											<img class="smalllaptopsizethreedot" style="width: 21px;height: 21px;margin-top:-14px;cursor: pointer;" src="<?php echo base_url('assets/nav_bar/more.svg'); ?>"></span>
										<div class="dropdown-menu" >
											
											<div class="dropdown-item">
												<a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" href="#modal-edit-machine<?php echo $value['machineId']; ?>" data-toggle="modal">
													<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>"> &nbsp;&nbsp; <?php echo Edimachine; ?>
												</a>
											</div>


											<div class="dropdown-item">
											    <a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" onclick="ckeck_function(<?php echo $value['machineId']; ?>);" href="#modal-update-stacklight-configured<?php echo $value['machineId']; ?>" data-toggle="modal" >
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/list.svg'); ?>"> &nbsp;&nbsp; <?php echo Stacklightconfiguration; ?>
											    </a>
											</div>

											<?php if($value['machineSelected'] == '1') { ?> 
											<div class="dropdown-item">
											    <a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" href="#modal-unlock-machine<?php echo $value['machineId']; ?>" data-toggle="modal" >
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/lock.svg'); ?>"> &nbsp;&nbsp; <?php echo unlockmachine; ?>
											    </a>
											</div>
											<?php } ?>

											<div class="dropdown-item">
												<a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" onclick="addParts(<?php echo $value['machineId']; ?>,'<?php echo $value['machineName']; ?>')" href="javascript:void(0);">
													<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/list.svg'); ?>"> &nbsp;&nbsp; <?php echo Partslist; ?>
												</a>
											</div>




											<div class="dropdown-item">
											    <a class="text-primary" href="#modal-delete-machine<?php echo $value['machineId']; ?>" data-toggle="modal" style="color: #F60100 !important;">
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/delete.svg'); ?>"> &nbsp;&nbsp; <?php echo Deletemachine; ?>
											    </a>
											</div>

										</div>
									</span>

									<?php } ?>
							</div>

							<?php }else{ ?> 

							<?php if($value['phoneDetail']->phoneId == 0) { ?>
								<?php if ($value['noStacklight'] == "1") { ?>

									<?php	if ($value['machineStatus'] == "0") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/producationSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/production.png'); ?>">
											<?php } ?>
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?> 
										</div>
									<?php }else if ($value['machineStatus'] == "2") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/noproductionSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?> 
										</div>
									<?php }else { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/setupSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?> 
										</div>
									<?php } ?>
								
								<?php }else{ ?>
									<div class="detectionImage<?php echo $value['machineId']; ?>">
										<img style="width:77px!important;margin-left: 20px!important" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">
									</div>

									<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?>" style="color: #FF8000;">
										<div style="padding-bottom: 23px;">
										No setapp connected to <span class="machineNameText"> <?php echo $value['machineName']; ?> </span>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
										</div>
									</div>
								<?php } ?>
							<?php } else if($value['setAppAlert'] == "<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?>" && $value['machineStatus'] == "0") {  ?>

								<div class="detectionImage<?php echo $value['machineId']; ?>">
									<img class="NodetectionImageStyle" style="" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>"></div>



								<div class="statusMessage<?php echo $value['machineId']; ?>" style="color: #FF8000;">
									<?php echo $value['setAppAlert']; ?>
									<br>
									
									<a onclick="checkLiveStatus(<?php echo $value['machineId']; ?>,'<?php echo str_replace("'", "+", $value['machineName']); ?>')" style="width: auto;margin-left:21px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">
										<?php echo Checklivestatus; ?>
									</a>
								</div>
							

							<?php }else if ($value['setAppAlert'] == "No internet or phone turned off. Check your phone or contact : info@nytt-tech.com" && $value['machineStatus'] == "0") { ?>
									<div class="detectionImage<?php echo $value['machineId']; ?>"><img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>"></div>

								<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?>" style="color: #FF8000;">
									<div style="margin-top: 10px;">
									<?php echo $value['setAppAlert']; ?>
									</div>
								</div>
							<?php }

							 else if($value['isSetAppOn'] == 0 ) {  ?>
								<?php	if ($value['machineStatus'] == "0") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img id="playImage<?php echo $value['machineId']; ?>" onclick="startSetApp(<?php echo $value['machineId']; ?>,<?php echo $value['machineStatus']; ?>)" style="width: 100px;cursor: pointer;margin-left: 18px;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" style="min-height: 40px!important;color: #FF8000;">
											<?php echo $value['setAppAlert']; ?>
										</div>
									<?php }else if ($value['machineStatus'] == "2") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/noproductionSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php }else { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/setupSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php } ?>
							<?php }else{ ?>
									<?php	if ($value['machineStatus'] == "1") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/setupSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php }else if ($value['machineStatus'] == "2") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<?php if($siteLang == "2"){ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/noproductionSw.png'); ?>">
											<?php }else{ ?>
												<img style="height: 82px; " src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">
											<?php } ?>
											
										</div>

										<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>" id="machineInSetupImageStyle">
											<?php echo MachineisinnoproductionstateYoucantstartsetapprightnow; ?>
										</div>
									<?php } else { ?>
								<?php if ($value['machine_light_status']['statusText'] == "Running") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.runningImage); ?>">
											
										</div>
									
								<?php }elseif ($value['machine_light_status']['statusText'] == "Waiting") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.waitingImage); ?>">
											
										</div>
								<?php }elseif ($value['machine_light_status']['statusText'] == "Stopped") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.stopImage); ?>">
											
										</div>
									
								<?php }elseif ($value['machine_light_status']['statusText'] == "Off") { ?>
										<div class="detectionImage<?php echo $value['machineId']; ?>">
											
												<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.offImage); ?>">
											
										</div>
								<?php }else { ?>
									<div class="detectionImage<?php echo $value['machineId']; ?>" style="min-height: 46px!important;">
										<img class="NodetectionImageStyle" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>"></div>
									<div style="color: #FF8000;" class="statusMessage<?php echo $value['machineId']; ?> statusmessagesfontsize<?php echo $value['machineId']; ?>">
										<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?> <br> 
										<a onclick="checkLiveStatus(<?php echo $value['machineId']; ?>,'<?php echo str_replace("'", "+", $value['machineName']); ?>')" style="margin-left: 20px;min-width: 75%;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">
											<?php echo Checklivestatus; ?>
										</a>
									</div>
								<?php }  if ($value['machine_light_status']['statusText'] != "nodet"){ ?>

								<div class="m-t-10 statusMessage<?php echo $value['machineId']; ?>" style="min-height: 46px!important;">
									<a onclick="checkLiveStatus(<?php echo $value['machineId']; ?>,'<?php echo str_replace("'", "+", $value['machineName']); ?>')" 
											style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">
											<?php echo Checklivestatus; ?>
										</a>
									
								</div>

							<?php } } } ?>

							<input type="hidden" id="detectionMachineName<?php echo $value['machineId']; ?>" value="<?php echo $value['machineName']; ?>">

							<div class="m-t-10 m-r-10 m-l-25">

								<?php $user_online = $value['user_online'];

								if (is_array($user_online) && !empty($user_online)) 
								{ 
									$user_onlineText = implode(", ", $user_online); 
									$user_onlineText = (count($user_online) == 1) ? $user_onlineText." ".isonline."." :  $user_onlineText." ".areonline.".";
									$color = "#76BA1B";
							    }
							    else
							    {
							    	$user_onlineText = Nousersonline;
							    	$color = "#F60100";
							    } 
								?>
								
								<a style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;" href="<?php echo base_url('Operator/operator_checkin_checkout'); ?>" class="btn btn-primary" data-toggle="tooltip" data-title="<?php echo $user_onlineText; ?>">
									<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/user.svg'); ?>">
									<span class="badge badge-light" style="background: <?php echo $color; ?>">&nbsp;</span>
								</a>

								<form method="post" action="<?php echo base_url('Analytics/dashboard');?>" style="display:inline;" class="productions" > 

								<input type="hidden" name="machineId" value="<?php echo $value['machineId']; ?>" />
									<button type="submit" style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;margin-left: 5px !important;margin-right: 5px !important;" class="btn btn-primary" data-toggle="tooltip" data-title="<?php echo Machineworkingat; ?> <?php echo  number_format($value['working_per'],2); ?>% <?php echo Today; ?>">
										<img class="dropdownimagestyle" style="width:22px!important;height:22px!important;" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
									</button>
								</form>

								<a style="background-color: #002060;border-color: #002060; padding: 3px 3px !important;" href="<?php echo base_url('Tasks/taskMaintenance'); ?>" class="btn btn-primary"  data-toggle="tooltip" data-title="<?php echo !empty($value['maintenanceUsers']) ? $value['maintenanceUsers'] : ''; ?>">
									<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/maintenace.svg'); ?>">
								</a>

								<?php if ($accessPointMachineEdit == true) { ?>

								
								
								<span id="filterTypeSelected" class="dropdown footerThreedot" style="right: 5px;bottom: 3px;position: absolute;">
										<span class="dropdown-toggle " data-toggle="dropdown" id="filterTypeSelectedValue"> 
											<img class="smalllaptopsizethreedot" style="width: 21px;height: 21px;margin-top:-14px;cursor: pointer;" src="<?php echo base_url('assets/nav_bar/more.svg'); ?>"></span>
										<div class="dropdown-menu" >
											
											<div class="dropdown-item">
												<a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" href="#modal-edit-machine<?php echo $value['machineId']; ?>" data-toggle="modal">
													<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>"> &nbsp;&nbsp; <?php echo Edimachine; ?>
												</a>
											</div>

											<?php if ($value['noStacklight'] == "0") { ?>
												
											<div class="dropdown-item">
											    <a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" onclick="ckeck_function(<?php echo $value['machineId']; ?>);" href="#modal-update-stacklight-configured<?php echo $value['machineId']; ?>" data-toggle="modal" >
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/list.svg'); ?>"> &nbsp;&nbsp; <?php echo Stacklightconfiguration; ?>
											    </a>
											</div>

											<?php if($value['machineSelected'] == '1') { ?> 
											<div class="dropdown-item">
											    <a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" href="#modal-unlock-machine<?php echo $value['machineId']; ?>" data-toggle="modal" >
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/lock.svg'); ?>"> &nbsp;&nbsp; <?php echo unlockmachine; ?>
											    </a>
											</div>
											<?php } ?>
											<?php } ?>

											<div class="dropdown-item">
												<a style="display: block;padding: 3px 0px;clear: both;line-height: 1.42857143;color: #002060 !important;white-space: nowrap;text-decoration: none;" class="text-primary" onclick="addParts(<?php echo $value['machineId']; ?>,'<?php echo $value['machineName']; ?>')" href="javascript:void(0);">
													<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/list.svg'); ?>"> &nbsp;&nbsp; <?php echo Partslist; ?>
												</a>
											</div>

											<div class="dropdown-item">
											    <a class="text-primary" href="#modal-delete-machine<?php echo $value['machineId']; ?>" data-toggle="modal" style="color: #F60100 !important;">
											    	<img class="dropdownimagestyle" src="<?php echo base_url('assets/nav_bar/delete.svg'); ?>"> &nbsp;&nbsp; <?php echo Deletemachine; ?>
											    </a>
											</div>

										</div>
									</span>

									<?php } ?>
								</div>

							<?php } ?>
						</div>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php } ?>
		<div class="col-lg-3 col-md-12 col-sm-12 smalllaptopsizeRes">
			<?php if ($accessPointMachineEdit == true) { ?>
				<div class="card text-center addMachinebackgroundwhite" style="">
				 	<a href="#modal-add-machine" data-toggle="modal" style="" class="btn btn-primary addMachinefafaplusstyle">
				 		<i style="font-size: 27px;" class="fa fa-plus"></i>
				 	</a>	
				</div>
			<?php } ?>
		</div>
	</div>
	<?php if ($accessPointVirtualMachineView == true) { ?>
		
	
	<h1 class="page-header machineStatusPageHeaderfont"><?php echo Virtualmachine; ?></h1>
	<div class="row">
		<?php foreach ($virtualMachine as $key => $value) { ?>
		<div class="col-lg-3 col-md-12 col-sm-12 smalllaptopsizeRes" style="min-width: 250px;">
			<div class="card boxShadow" style="min-height: 300px;"> 
				<div class="card-img-overlay">
					<h4 class="card-title">
						<div class="row">
							<div class="col-md-6">
								<span>
									<a style="color: #FF8000;">
										<span data-toggle="tooltip" data-title="<?php echo $value['IOName']; ?>" data-original-title="" title=""> <?php echo $value['IOName']; ?> </span>
									</a>
								</span>
							</div>
							<div class="col-md-3">
								
                                <form class="form-inline" action="<?php echo base_url('VirtualMachine/virtualMachineLogList') ?>" method="POST">
                                    <input type="hidden" name="virtualMachineId" value="<?php echo $value['virtualMachineId']; ?>">
                                    <input type="hidden" name="IOName" value="<?php echo $value['IOName']; ?>">
                                    <button type="submit" class="btn btn-primary">
                                        <img style="height: 18px;" src="<?php echo base_url('assets/nav_bar/report.svg'); ?>">
                                    </button>
                                </form>
                            </div>
                            <div class="col-md-3">

                                <form class="form-inline" action="<?php echo base_url('VirtualMachine/virtualMachineGraph') ?>" method="POST">
                                    <input type="hidden" name="IOName" value="<?php echo $value['IOName']; ?>">
                                    <button type="submit" class="btn btn-primary">
                                        <img style="height: 18px;" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
                                    </button>
                                </form>
							</div>
							<div class="col-md-12">
								<hr style="height: 2px !important;margin-top: 0.1rem !important;margin-bottom: -0.9rem !important;">
							</div>
						</div>
					</h4>

					<small>&nbsp;</small>
					<div class="row card-content" style="margin-top: 20px;">
						
								<?php
									if ($value['IOName'] == "GPIO14") {  ?>

						<div class="col-md-12" >
							<div>

									<div class="card11" >
					                    <center>
					                      <svg style="width: 160px;height: 160px;">
					                        <circle style="stroke:#FFFFFF"cx="75" cy="75" r="75"></circle>
					                        <circle cx="75" cy="75" r="75"></circle>
					                        <circle  style="stroke:#CCD2DF;" cx="75" cy="75" r="65"></circle>
					                        <div class="overallStatisticscircle" style="padding-top: 49px;padding-left: 21px;">
					                          <h3 style="margin-bottom:0.0rem;color:#002060;" id="partCount">0</h3>
					                          <h4 style="color:#002060;font-size: 15px;" class="textChange"><?php echo Partsproduced; ?></h4>
					                        </div>
					                      </svg>
					                    </center>
					                  </div>
							    
								<?php  }else{ ?>

									<div class="col-md-3" style="max-width:25%;top: 37px;"></div>
						<div class="col-md-9" style="max-width:75%;">
							<div>

									<?php if ($value['signalType'] == "0") { ?>
									<?php if ($siteLang == "2") { ?>
										<img id="detectionImageVM<?php echo $value['IOName']; ?>" style="height: 82px; " src="<?php echo base_url("assets/overview_gifs/inactiveSw.png"); ?>">
									<?php }else{ ?>
										<img id="detectionImageVM<?php echo $value['IOName']; ?>" style="height: 82px; " src="<?php echo base_url("assets/overview_gifs/not_active.png"); ?>">
									<?php } ?>
								<?php }else{ ?>
									<?php if ($siteLang == "2") { ?>
										<img id="detectionImageVM<?php echo $value['IOName']; ?>" style="height: 82px; " src="<?php echo base_url("assets/overview_gifs/activeSw.png"); ?>">
									<?php }else{ ?>
										<img id="detectionImageVM<?php echo $value['IOName']; ?>" style="height: 82px; " src="<?php echo base_url("assets/overview_gifs/active.png"); ?>">
									<?php } ?>
										
								<?php } ?> 
								<div class="m-t-10" style="color: #FF8000;">
								<?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?>
							</div>
						<?php } ?>

								
							</div>

							
							<input type="hidden" id="detectionMachineName13" value="Test 7" wtx-context="F92CF2A8-D6FC-414E-B896-683BFDC3CBA8">
								<div class="m-t-10 m-r-10 m-l-25">
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
	</div>

	<?php } ?>
	<hr style="background: gray;">
	<p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

	
<div id="liveDetectonImage" class="modal fade" role="dialog">
  <div class="modal-dialog" >

    <div class="modal-content">
      <div class="modal-header" style="background-color: #002060;">
        <h4 style="color: #FF8000;" class="modal-title" id="liveMachineName"></h4>
        <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
      </div>
      <div class="modal-body">
      	<div class="row">
      		<div class="col-md-6" style="text-align: center;font-size: 20px;font-weight: 500;" id="detectionName"></div>
      		<div class="col-md-6" style="text-align: center;font-size: 20px;font-weight: 500;" id="detectionState"></div>
      	</div>
      	<div class="row" style="margin-top: 10px;margin-bottom: 10px;">
      		<div class="col-md-12" style="text-align: center;" id="currentTime"></div>
      	</div>
      	<div class="row">
      		<div class="col-md-12">
      			<center>
		        	<img class="chechkLiveStatusStyle" style="width: 76%;border: 5px solid #F2F2F2;" id="ItemPreview" src="">
		        </center>
		        <br>
		        <br>
		        <form action="dashboardCaptureCamera" method="POST" id="dashboardCaptureCamera_form">
			        <input type="hidden" name="detectionMachineId" id="detectionMachineId">
			        <input type="hidden" name="imageName" id="imageName">
			        <input type="hidden" name="timestamp" id="timestamp">
			        <input type="hidden" name="color" id="color">
			        <input type="hidden" name="state" id="state">
			        <center><button style="font-weight: 100 !important;" type="submit" id="dashboardCaptureCamera_submit" class="btn btn-primary"><?php echo Reportwrongdetection; ?></button></center>
		    	</form>
		    </div>
        </div>
      </div>
     
    </div>

  </div>
</div>

<?php foreach ($listMachines as $key => $value) { ?>
	<div class="modal fade" id="modal-delete-machine<?php echo $value['machineId']; ?>" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header" style="background-color: #002060;">
					<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Delete; ?>  (<?php echo $value['machineName']; ?>)</h4>
					<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
				</div>
				<div class="row">
					<div class="col-md-12" style="margin-top: -20px;">
						<center>
							<img style="margin-top:10px;width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
						</center>
					</div>
					<div id="delete_machine_success<?php echo $value['machineId']; ?>" class="m-b-10 alert alert-success fade hide" ></div>
					<p></p>
					<!-- <div class="col-md-12" style="margin-top: -8px;">
						<center> Are you sure you want to delete the Machine ?</center>
					</div> -->

					<div class="col-md-12" style="">
                        <center> <?php echo AreyousureyouwanttodeletetheMachine; ?></center>
                    </div>

                   
				</div>
				<form class="p-b-20" action="delete_machine" method="POST" id="delete_machine_form<?php echo $value['machineId']; ?>" class="edit_machine_form" >
					<input type="hidden" name="deleteMachineId" id="deleteMachineId<?php echo $value['machineId']; ?>" value="<?php echo $value['machineId']; ?>" />

					 <div class="col-md-12" style="">
                        <center>
                            <label><?php echo PleasetypeDELETEinbelowtextboxtoconfirmdeletionofmachine; ?>.</label><br>
                            <input type="text" placeholder="<?php echo PleasetypeDelete; ?>" id="checkKeywordMachine<?php echo $value['machineId']; ?>" required>
                        <center>
                    </div>

					<div class="col-md-12" style="margin-top: 10px;">

			 			<center>
			 				<button type="submit" class="btn btn-danger" id="delete_machine_submit<?php echo $value['machineId']; ?>"><?php echo Delete; ?></a>
			 			</center>
					</div>
				</form>
			</div>
		</div>
	</div> 
	<div class="modal fade" id="modal-unlock-machine<?php echo $value['machineId']; ?>" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #002060;">
					<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo $value['machineName']; ?></h4>
					<button type="button" class="close" style="padding: 10px 14px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
				</div>
				<div class="row">
					<div class="col-md-12" style="margin-top: -20px;">
						<center>
							<img style="margin-top:25px;width: 17%;" src="<?php echo base_url().'assets/nav_bar/lock.svg"'?>" style="">
						</center>
					</div>
					<div id="unlock_machine_success<?php echo $value['machineId']; ?>" class="m-b-10 alert alert-success fade hide" ></div>
					<p></p>
					<div class="col-md-12" style="margin-top: 10px;">
						<center>
							<?php echo Areyousuretounlockmachine; ?><?php echo Areyousuretounlockmachine; ?> "<?php echo $value['machineName']; ?>" ?
						</center>
					</div>
				</div>
				<form class="p-b-20" action="unlock_machine" method="POST" id="unlock_machine_form<?php echo $value['machineId']; ?>" class="edit_machine_form" >
					<input type="hidden" name="unlockMachineId" id="unlockMachineId<?php echo $value['machineId']; ?>" value="<?php echo $value['machineId']; ?>" /> 
					<div class="col-md-12" style="margin-top: 10px;">
						<center>
							<button type="submit" class="btn btn-primary" id="unlock_machine_submit<?php echo $value['machineId']; ?>"><?php echo Unlock; ?></a>
						</center>
					</div>
				</form>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-edit-machine<?php echo $value['machineId']; ?>" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">

				<div class="modal-header" style="background-color: #002060;">
			        <h4 style="color: #FF8000;" class="modal-title machineNameTextEdit"><?php echo Edit; ?> (<span class="machine_name_<?php echo $value['machineId']; ?>"> <?php echo $value['machineName']; ?></span>)</h4>
			        <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
			    </div>
				
				<div class="modal-body">
					<div id="edit_machine_error<?php echo $value['machineId']; ?>" class="m-b-10 alert alert-danger fade hide"></div>
					<div id="edit_machine_success<?php echo $value['machineId']; ?>" class="m-b-10 alert alert-success fade hide" ></div>
					<form class="p-b-20" action="edit_machine" method="POST" id="edit_machine_form<?php echo $value['machineId']; ?>" class="edit_machine_form" >
						<input type="hidden" name="machineId" id="machineId<?php echo $value['machineId']; ?>" value="<?php echo $value['machineId']; ?>" /> 
					
						<div class="row">
							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Type of CNC" >
								<select  style="min-width:100%;width:100%;" class="form-control select2" name="machineType" placeholder="<?php echo MachineType; ?>" required >
										<option selected="" disabled=""><?php echo Selectmachinetype; ?></option>
										<option <?php if($value['machineType'] == "Milling"){ echo "selected"; } ?> value="Milling" ><?php echo Milling; ?></option>
										<option <?php if($value['machineType'] == "Turning"){ echo "selected"; } ?> value="Turning" ><?php echo Turning; ?></option>
										<option <?php if($value['machineType'] == "Grinding"){ echo "selected"; } ?> value="Grinding" ><?php echo Grinding; ?></option>
										<option <?php if($value['machineType'] == "Other"){ echo "selected"; } ?> value="Other" ><?php echo Other; 	?></option>
								</select>
							</div>

							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Make" >
								<input type="text" class="form-control col-md-12" name="machineMake" placeholder="<?php echo Machinemake; ?>" value="<?php echo $value['machineMake']; ?>" >
							</div>

							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Year of manufacture" >
								<input type="text" pattern="^(19|20)\d\d$" class="form-control col-md-12" name="machinePurchaseDate" value="<?php echo $value['machinePurchaseDate']; ?>" placeholder="<?php echo Yearofmanufacture; ?>" >
							</div>

							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Make" >
								<input type="text" class="form-control col-md-12" name="machineModal" placeholder="<?php echo Machinemodel; ?>" value="<?php echo $value['machineModal']; ?>" >
							</div>


							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Name"  >
								<input type="text" class="form-control col-md-12" onkeyup="get_machine_name_edit(this.value,<?php echo $value['machineId']; ?>);" value="<?php echo $value['machineName']; ?>" name="machineName" placeholder="<?php echo Entermachinename; ?>" required > 
							</div>

							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Name"  >
								<input type="text" class="form-control col-md-12" value="<?php echo $value['machineControlUnit']; ?>"  name="machineControlUnit" placeholder="<?php echo Controlsystem; ?>" required > 
							</div>
							<div style="padding: 0 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Machine Automation"  >
								<select style="min-width:100%;width:100%;" class="form-control select2"  name="machineAutomation" placeholder="<?php echo SelectMachineAutomation; ?>" required onchange="machineLoadingChangeMachine(this.value,<?php echo $value['machineId']; ?>);" >
										<option selected="" disabled="" ><?php echo Selectmachineautomation; ?></option>
										<option <?php if($value['machineAutomation'] == "Automatic"){ echo "selected"; } ?> value="Automatic" ><?php echo Automatic; ?></option>
										<option <?php if($value['machineAutomation'] == "Manual"){ echo "selected"; } ?> value="Manual" ><?php echo Manual; ?></option>
										<option <?php if($value['machineAutomation'] == "Both"){ echo "selected"; } ?> value="Both" ><?php echo Both; ?></option>
								</select>
							</div>

							<div style="padding: 0 5px;display: <?php if($value['machineAutomation'] == "Manual"){ echo "none"; } ?>;" class="form-group col-md-6" data-toggle="tooltip" data-title="Loading" id="loading_type_<?php echo $value['machineId']; ?>">
								<select style="min-width:100%;width:100%;" class="form-control select2" name="machineLoading" placeholder="<?php echo SelectMachineloadingtype; ?>" required >
										<option <?php if ($value['machineLoading'] == "Robot") {
											echo "selected";
										} ?> value="Robot" ><?php echo Robot; ?></option>
										<option <?php if ($value['machineLoading'] == "Bar feed") {
											echo "selected";
										} ?> value="Bar feed" ><?php echo barfeed; ?></option>
										<option <?php if ($value['machineLoading'] == "Other") {
											echo "selected";
										} ?> value="Other" ><?php echo Other; ?></option>
								</select>
							</div>
						</div>
						
						<div class="row">
							
						</div>
						<center>
							<button type="submit" style="margin-top: 5px;" class="btn btn-sm btn-primary m-r-5" id="edit_machine_submit<?php echo $value['machineId']; ?>"><?php echo save; ?></button>
						</center>
					</form>
				</div> 
				
			</div>
		</div>
	</div>

<?php } ?>


<div class="modal fade" id="modal-add-machine-parts" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="width: 715px;">

				<div class="modal-header" style="background-color: #002060;">
			        <h4 style="color: #FF8000;" class="modal-title"><?php echo Partslist; ?></h4>
			        <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
			    </div>
				
				<div class="modal-body">
					
					<form class="p-b-20" action="add_parts" method="POST" id="add_parts_form">

						<h5 class="fileDesign" style="margin-top: 20px;margin-bottom: 20px;">
							<center><?php echo Addthefirtspartfor; ?> <span id="machineNameTextParts"  style="color: #FF8000;"></span></center>
						</h5>
						<input type="hidden" name="machineId" id="machineIdParts" value="" /> 

						<style type="text/css">
							.border-none
							{
								border: none;
							}
						</style>
					
						<div class="row fileDesign finalData">
							

							<div class="col-md-2">
								<div class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
									<label><?php echo Partsnumber; ?></label>
	                <input type="text" class="form-control border-left-right-top-hide" id="partsNumber" name="" placeholder="<?php echo Enterpartsnumber; ?>" > 
	              </div>
	            </div> 

              <div class="col-md-3">
	              <div class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
									<label><?php echo Partsname; ?></label>
	                <input type="text" class="form-control border-left-right-top-hide" id="partsName" name="" placeholder="<?php echo Enterpartsname; ?>" >
	              </div>
	            </div> 


              <div class="col-md-3">
	              <div class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="">
									<label><?php echo Operation; ?></label>
	                <input type="text" class="form-control border-left-right-top-hide" id="partsOperation" name="" placeholder="<?php echo Enterpartsoperation; ?>" > 
	              </div>
	            </div> 


	            <style type="text/css">
	            	
	            	.placeholder1 {
								    position: relative;
								    display: inline-block;
								}

								.placeholder1::after {
								    position: absolute;
								    right: 5px;
								    top: 11px;
								    content: attr(data-placeholder);
								    pointer-events: none;
								    opacity: 0.6;
								}
	            </style>
              <div class="col-md-2">
	              <div class="form-group" data-toggle="tooltip" data-title="Name" data-original-title="" title="" >
									<label><?php echo Cycletime; ?></label>
									<div class="placeholder1" data-placeholder="sec">
	                	<input type="text" class="form-control border-left-right-top-hide" id="patrsCycletime" value="0" name="" placeholder="<?php echo time; ?>" >
	                </div>
	              </div>
	            </div> 



              <div class="col-md-2">
              	  <input type="hidden" id="incrementVal" value="1">
              		<button style="background: white;margin-top: 26px;" type="button" onclick="addPatrsData();"  class="btn btn-sm"><i class="fa fa-plus"> </i> <?php echo Addparts; ?></button>
              </div>


				

						</div>
						
						<div class="row fileDesign">
							<div class="col-md-12">
								<div style="width: 45%;float: left;"><hr style="background: gray;"></div>
								<div style="width: 5%;margin-top: 8px;float: left;"><center><?php echo ucfirst(ortext); ?></center></div>
								<div style="width: 45%;float: left;"><hr style="background: gray;"></div>
							</div>
						</div>

						<h5 class="fileDesign" style="margin-top: 20px;margin-bottom: 20px;"><center><?php echo Uploadanspreadsheetfilewiththistemplate; ?></center></h5>


						<style type="text/css">
							.upload-btn-wrapper {
							    position: relative;
							    overflow: hidden;
							    display: inline-block;
							    margin-top: 8%;
							}

							.btn1 {
							    border: 2px solid #ff8000;
							    color: #ff8000;
							    background-color: #ff8000;
							    border-radius: 50%;
							    font-size: 20px;
							    font-weight: 700;
							}

							.upload-btn-wrapper input[type=file] {
							    font-size: 100px;
							    position: absolute;
							    left: 0;
							    top: 0;
							    opacity: 0;
							}

							.pointer input {
							    cursor: pointer;
							}
						</style>

						
						<div class="row fileDesign" style="margin-left: 10px !important;margin-right: 10px !important;">
							<div class="col-md-12">
								<center><img style="width: 67%;" src="<?php echo base_url('assets/img/sheetImage.png'); ?>"></center>
							</div>
							<div class="col-md-12" style="background: #f1f3f5;height: 120px;border-radius: 5px;text-align: center;margin-top: 15px;">
								<div class="upload-btn-wrapper">
									<button class="btn1 pointer" type="button">
										<img style="margin-top: -8px;height: 13px;" width="18" src="<?php echo base_url("assets/nav_bar/upload.svg"); ?>">
										<input id="i_fileOv" type="file" name="image" />
									</button>
									<span id="disp_tmp_pathOv" style="font-weight: 600;color: #002060;"><?php echo ChooseanXLSXfile; ?></span>
		  							
		  					</div>
							</div>
						</div>

						<div class="row tableList" style="display:none;">

							<div class="col-md-12" style="margin-top: 20px;margin-bottom: 10px;">
								<h5>
									<center> 
										<img width="25" height="25" src="<?php echo base_url('assets/img/tick.svg'); ?>"> <span style="color: green;"><?php echo Fileuploaded; ?></span>
									</center>
								</h5>
							</div>

							<div class="col-md-12">
								<center> 
										<div style="background-color: #f1f3f5;height: 42px;width: 50%;border-radius: 5px;padding-top: 9px;;">
											<img style="float: left;margin-left: 10px;" style="float: left;" width="25" height="25" src="<?php echo base_url('assets/nav_bar/meeting_notes_blue.svg'); ?>"> 
											<span style="float: left;margin-left: 10px;margin-top: 6px;" id="uploadedFileName"></span>
											<img style="float: right;margin-right: 13px;" style="float: right;" width="25" height="25" onclick="addPatrsDisplay();" src="<?php echo base_url('assets/nav_bar/closebutton.svg'); ?>"> 
										</div>
								</center>
							</div>

							<div class="col-md-12" style="margin-top: 20px;">
								<center>
									<table border="2" style="text-align: center;">
										<tbody id="tableData">
											
										</tbody>
									</table>
								</center>
							</div>

							<div class="col-md-12" style="margin-top: 15px;">
								<center>
									<button onclick="showFinalPartsData()" type="button" style="margin-top: 5px;" class="btn btn-sm btn-primary m-r-5"><?php echo Addpartstolist; ?></button>
								</center>
							</div>

						</div>


						<div class="row fileError" style="display: none;">

							<div class="col-md-12" style="margin-top: 100px;margin-bottom: 10px;">
								<h5>
									<center> 
										<img width="25" height="25" src="<?php echo base_url('assets/img/cross.svg'); ?>"> <span style="color: #F60100;"><?php echo Fileuploadingfailed; ?></span>
									</center>
								</h5>

								<h6><center><?php echo WrongtemplateTryagainwithanotherXLSXfile; ?></center></h6>
							</div>

							<div class="col-md-12">
								<center> 
										<div style="background-color: #f1f3f5;height: 42px;width: 50%;border-radius: 5px;padding-top: 9px;margin-top : 55px;">
											<img style="float: left;margin-left: 10px;" style="float: left;" width="25" height="25" src="<?php echo base_url('assets/nav_bar/meeting_notes_red.svg'); ?>"> 
											<span style="float: left;margin-left: 10px;margin-top: 6px;" id="errorFileName"></span>
											<img style="float: right;margin-right: 13px;" style="float: right;" width="25" height="25" onclick="addPatrsDisplay();" src="<?php echo base_url('assets/nav_bar/closebutton.svg'); ?>"> 
										</div>
								</center>
							</div>

							

							<div class="col-md-12" style="margin-top: 100px;">
								<center>
									<button disabled="" type="button" style="margin-top: 5px;" class="btn btn-sm btn-primary m-r-5"><?php echo Addpartstolist; ?></button>
								</center>
							</div>

						</div>

						<div class="row finalData" style="display: none;margin-left: -1px;margin-right: -3px;">
								<div class="col-md-12" style="background-color: #f1f3f5;border-radius: 5px;">
										<table class="table">
											<tbody id="finalTableData">
													
											</tbody>
										</table>
								</div>
						</div>


						<div class="row fileDesign finalData" style="margin-top: 20px;">
							<div class="col-md-6">
								<center>
									<button type="submit" style="margin-top: 5px;" class="btn btn-sm btn-primary m-r-5" id="add_parts_submit"><?php echo confirm; ?></button>
								</center>
							</div>
							<div class="col-md-6">
								<center>
									<button type="button" style="margin-top: 5px;background: white;" data-dismiss="modal" class="btn btn-sm m-r-5"><?php echo Cancel; ?></button>
								</center>
							</div>
						</div>
						
					</form>
				</div> 
				
			</div>
		</div>
	</div>

<div class="modal fade" id="modal-add-machine" style="display: none;" aria-hidden="true">
	<div class="modal-dialog" style="max-width : 100% !important;width: 750px;">
		<div class="modal-content">

			<div class="modal-header" style="background-color: #002060;">
		        <h4 style="color: #FF8000;" class="modal-title machine_name" id="liveMachineName"><?php echo Addnewmachine; ?></h4>
		        <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
		    </div>
		    <style type="text/css">
		    	
		    </style>
			
			<div class="modal-body">
				<div id="add_machine_error" class="m-b-10 alert alert-danger fade hide"></div>
				<div id="add_machine_success" class="m-b-10 alert alert-success fade hide" ></div>

				<form class="p-b-20" action="add_machine" method="POST" id="add_machine_form" >
					<input type="hidden" name="nodet" id="nodet">
					<div class="row">

						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Name"  >
							<input type="text" style="padding: 0px 4px 13px !important;" class="form-control col-md-12 border-left-right-top-hide"  onkeyup="get_machine_name(this.value);" id="machineName" name="machineName" 
							placeholder="<?php echo Entermachinename;?>" required > 
						</div>
						
						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Machine Type" >
							<select style="min-width:100%;width:100%;" class="form-control select2" id="machineType" name="machineType" placeholder="<?php echo MachineType; ?>" required >
									<option selected="" disabled=""><?php echo Selectmachinetype; ?></option>
									<option value="Milling" ><?php echo Milling; ?></option>
									<option value="Turning" ><?php echo Turning; ?></option>
									<option value="Grinding" ><?php echo Grinding; ?></option>
									<option value="Other" ><?php echo Other; ?></option>
							</select>
						</div>

						<div style="padding: 0 5px;" class="form-group col-md-4" data-toggle="tooltip" data-title="Machine Automation"  >
							<select style="min-width:100%;width:100%;" class="form-control select2" id="machineAutomation" name="machineAutomation" placeholder="<?php echo SelectMachineAutomation; ?>" required onchange="machineLoadingChange(this.value);" >
									<option selected="" disabled="" ><?php echo SelectmachineAutomations; ?></option>
									<option value="Automatic" ><?php echo Automatic; ?></option>
									<option value="Manual" ><?php echo Manual; ?></option>
									<option value="Both" ><?php echo Both; ?></option>
							</select>
						</div>

						<div style="padding: 0 5px;display: none;" class="form-group col-md-2" data-toggle="tooltip" data-title="Loading" id="loading_type">
							<select style="min-width:100%;width:100%;" class="form-control select2" id="machineLoading" name="machineLoading" placeholder="<?php echo SelectMachineloadingtype; ?>" required >
									<option value="Robot" ><?php echo Robot; ?></option>
									<option value="Bar feed" ><?php echo Barfeed; ?></option>
									<option value="Other" ><?php echo Other; ?></option>
							</select>
						</div>
					</div>
					<div class="row">
						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Year of manufacture" >
							<input type="text" style="padding: 5px 3px 11px !important;" pattern="^(19|20)\d\d$" class="form-control col-md-12 border-left-right-top-hide" id="machinePurchaseDate" name="machinePurchaseDate" placeholder="&#9679;   <?php echo Enteryearofmanufacture; ?>" >
						</div>

						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Maintenance done bypraveen1,Marcus 2019-11-19"  >
							<input type="text" style="padding: 5px 3px 11px !important;" class="form-control col-md-12 border-left-right-top-hide" id="machineMake" name="machineMake" placeholder="&#9679;   <?php echo Entermachinemake; ?>" >
						</div>
						
						
						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Make" >
							<input type="text" style="padding: 5px 3px 11px !important;" class="form-control col-md-12 border-left-right-top-hide" id="machineModal" name="machineModal" placeholder="&#9679;   <?php echo Entermachinemodel; ?>" >
						</div>
						
						<div style="padding: 0 5px;" class="form-group col-md-3" data-toggle="tooltip" data-title="Name"  >
							<input type="text" style="padding: 5px 3px 11px !important;" class="form-control col-md-12 border-left-right-top-hide" id="controlSystem" name="controlSystem" placeholder="&#9679;   <?php echo Entercontrolsystem; ?>" required > 
						</div>
					</div>
					
					<input type="hidden" name="machineMTId" value="1">
					
					<div class="row">
						<div class="col-md-3" style="margin-top: 40px;">
							<div class="form-check" style="text-align: center;">
							  <input class="form-check-input" type="checkbox" value="1" name="noStacklight" id="noStacklight" style="width: 20px;height: 20px;" onclick="noStacklightFunc();">
							  <label class="form-check-label" for="noStacklight" style="font-size: 17px;margin-left: 8px;" onclick="noStacklightFunc();">
							    <?php echo Nostacklight; ?>
							  </label>
							</div>
						</div>
						<div class="col-md-9">
							<br>
							<div class="noStacklight">
								<center>
									<fieldset>
										<legend><b><?php echo Selectmachinestacklightcolors; ?></b></legend>
											<center>

												<?php for($z=0;$z<count($listColors);$z++) 
												{ 
													$imgUrl = base_url().'assets/img/new_color/'.$listColors[$z]->colorCode.'.png'; 
													if ($z != 0) 
													{
													 	$margin = "10px";
													}
													else
													{
													 	$margin = "0px";
													}
												?>
													<img style="width: 35px;height: 35px;margin-top: 10px;margin-bottom: 10px;margin-left:<?php echo $margin; ?>;" src="<?php echo $imgUrl; ?>" alt="..." class="img-check" id="data_image_<?php echo $listColors[$z]->colorId; ?>" data-color-name="<?php echo $listColors[$z]->colorName; ?>" data-color_id="<?php echo $listColors[$z]->colorId; ?>" data-img-src="<?php echo $imgUrl; ?>">
												<?php } ?>
											</center>
									</fieldset>
								</center>
							</div>
							
							<div class="noStacklightShow" style="display: none;margin-top: 16px;">
								<center><span> 
						<?php echo NosetappinstallationrequiredifnostacklightisavailableDatafrommachinemodescanbeobtainedfromopapp; ?>.</span></center>
							</div>
						</div>

						<?php for($z=0;$z<count($listColors);$z++) { 
								 $imgUrl = base_url().'assets/img/new_color/'.$listColors[$z]->colorCode.'.png'; 
							?>
									<input   style="display: none; margin-top: 50px;" type="checkbox" id="machineLight_<?php echo  $listColors[$z]->colorId; ?>" name="machineLight[]" value="<?php echo $listColors[$z]->colorId; ?>" class="hidden machineLightClass" autocomplete="off">
							<?php } ?>

						<div class="col-md-12 noStacklight" id="colorLightSelect">
						<br>
						</div>
					</div>
					<div class="row noStacklight">
						<div class="col-md-3">
							<div style="padding: 0 5px;" class="form-group" data-toggle="tooltip" data-title="Loading"  >
								<label style="margin-bottom: 0.1rem !important;"><b><?php echo Running; ?></b></label>
								<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="running" name="running[]" placeholder="<?php echo SelectRunning; ?>" onchange="disabled_color_value('running')" >
										
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div style="padding: 0 5px;" class="form-group" data-toggle="tooltip" data-title="Loading" >
								<label style="margin-bottom: 0.1rem !important;"><b><?php echo Waiting; ?></b></label>
								<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="waiting" onchange="disabled_color_value('waiting')" name="waiting[]" placeholder="<?php echo SelectWaiting; ?>" >
									
								</select>
							</div>
						</div>
						<div class="col-md-3">
							<div style="padding: 0 5px;" class="form-group" data-toggle="tooltip" data-title="Loading" >
								<label style="margin-bottom: 0.1rem !important;"><b><?php echo Stopped; ?></b></label>
								<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="stopped" onchange="disabled_color_value('stopped')" name="stopped[]" placeholder="<?php echo SelectStopped; ?>" >
									
								</select>
							</div>
						</div>
						<div class="col-md-3">

							<div style="padding: 0 5px;" class="form-group" data-toggle="tooltip" data-title="Loading" >
								<label style="margin-bottom: 0.1rem !important;"><b><?php echo Off; ?></b></label>
								<select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="off" onchange="disabled_color_value('off')" name="off[]" placeholder="<?php echo SelectOff; ?>" >
										
								</select>
							</div>
						</div>
					</div>
					<div class="row">
						<div class="col-md-12">
							<br>
							<center>
								<button type="submit" style="border-radius: 2.7rem !important;padding: 13px 30px !important;font-size: 20px;" class="btn btn-lg btn-primary m-r-5" id="add_machine_submit" ><i class="fa fa-plus" style="font-size: 20px;"></i> <?php echo Addmachine; ?></button>
							</center>
						</div>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
</body>