<link href="<?php echo base_url('assets/css/reportProblem.css');?>" rel="stylesheet" />
<style>
.displayfilterrowstyle
  {
    display:contents!important; 
  }
  .tableDatacontentStyle
  {
  	display: inline-block;
	width: 50%;
	white-space: nowrap;
	overflow: hidden !important;
	text-overflow: ellipsis;
	max-width: 50%;
  }
  #empTable_paginate {
    border-radius: 15px!important;
    padding-left: 0!important;
    box-shadow: grey 0 0 8px -2px!important;
    width: 30%;
    margin: AUTO;}
    input[type=checkbox] {
    -webkit-appearance: none;
    width: 13px;
    height: 13px;
    border: 1px solid #002060;
    border-radius: 50%;
    outline: 0;
}

.rowBLue
{
	border-left: 3px solid #002060 !important;
  background: #d3dfec !important;
  border-bottom: 5px solid white !important;
  color: #002060!important;
}

.rowOrange
{
	border-left: 3px solid #FF8000 !important;
  background: #F2F2F2 !important;
  border-bottom: 5px solid white !important;
}
.rowWhite
{
	border-left: 3px solid #FFFFFF !important;
  background: #FFFFFF !important;
  border-bottom: 5px solid white !important;
}
.table>tbody>tr
{
	color: black;
}

</style>
<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Reportednotes; ?></h1>
	<input type="hidden" name="problemId" value="0">
	<div class="row">
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;">
		  	<div class="row" style="float:left;margin: 10px;">
			  	<div class="displayfilterrowstyle" id="showUserFilter"></div>
			  	<div class="displayfilterrowstyle" id="showDate"></div>
			  	<div class="displayfilterrowstyle" id="showType"></div>
			  	<div class="displayfilterrowstyle" id="showResolved"></div>
			  	<div class="displayfilterrowstyle" id="showStatus"></div>
			</div>
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr role="row tableHeadTR">
								<th class="tableHeadTH">
									<div id="filterUserNameSelected" class="dropdown filterUserNameSelectesStyle">
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> 
											<?php echo Operator; ?>&nbsp;</span>
										<div class="dropdown-menu .hdhdhhd">
											<?php foreach ($users as $key => $value) { ?>
												<div class="dropdown-item">
													<div class="form-check form-checkWidth">
													  <input onclick="filterUserName(<?php echo $value['userId'] ?>,'<?php echo $value['userName'] ?>')" class="filterUserName form-check-input" type="checkbox" value="<?php echo $value['userId']; ?>" id="filterUserName<?php echo $value['userId'] ?>" >
													  <label class="form-check-label" for="filterUserName<?php echo $value['userId'] ?>">
													    <?php echo $value['userName']; ?>
													  </label>
													</div>
												</div>
											<?php } ?>
										</div>
									</div>

								<small class="smallstylee">&nbsp;</small>
								</th>

								<th class="tableHeaderDateStyle">
									<input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-29 day")); ?>" />  
									<input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />   
									<div id="advance-daterange" name="advance-daterange" style="margin-bottom : 20px;width: 64px;">
										<span>
										<?php echo Date; ?>&nbsp;&nbsp;
										</span> 
										<i class="fa fa-caret-down m-t-2"></i>
									</div>
								</th>

								<th class="tableHeaderTypeStyle" style="">
									<div id="filterTypeSelected" class="dropdown filtertypeSelectesStyle">
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterTypeSelectedValue"> <?php echo type; ?>&nbsp;</span>
										<div class="dropdown-menu" >
											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterType()" value="'1'" class="filterType form-check-input" type="checkbox" id="accident1" 
												   >
												  <label class="form-check-label" for="accident1">
												    <?php echo Accident; ?>
												  </label>
												</div>
											</div>
											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterType()" value="'0'" class="filterType form-check-input" type="checkbox" id="incident1" 
												   >
												  <label class="form-check-label" for="incident1">
												    <?php echo Incident; ?>
												  </label>
												</div>
											</div>
											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterType()" value="'2'" class="filterType form-check-input" type="checkbox" id="suggestion1" 
												   >
												  <label class="form-check-label" for="suggestion1">
												    <?php echo Suggestion; ?>
												  </label>
												</div>
											</div>
										</div>
									</div>
									
								<small style="color: #FF8000;"  >&nbsp;</small>
								</th>
								
								<th class="tableheadreport">
									<div class="tableheadreportdescription"><?php echo Description; ?><div>
								</th>
								<th class="tableheadreport">
									<div class="tableheadreportdescription"><?php echo Cause; ?><div>
								</th>
								<th class="tableheadreport">
									<div class="tableheadreportdescription"><?php echo Improvement; ?><div>
								</th>
								<th id="tableheadreportresolved"> 
									<div class="dropdown" id="filterResolvedSelected tableheadreportresolvedselected">
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterStatusSelectedValueR" style="width: 50px;"> <?php echo Resolved; ?>&nbsp;
										</span>
										<div class="dropdown-menu">
											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterResolved()" class="filterResolved form-check-input" type="checkbox" id="active1" value="'0'">
												  <label class="form-check-label" for="active1">
												    <?php echo Active; ?>
												  </label>
												</div>
											</div>
											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterResolved()" class="filterResolved form-check-input" type="checkbox" id="resolved1" value="'1'">
												  <label class="form-check-label" for="resolved1">
												    <?php echo Resolved; ?>
												  </label>
												</div>
											</div>
										</div>
									</div>
								<small style="color: #FF8000;text-align : center;"  >&nbsp;</small>
								</th>

								<th id="tableheadreportresolved"> 
									<div class="dropdown" id="filterStatusSelected tableheadreportresolvedselected">
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterStatusSelectedValue" style="width: 50px;">
										 <?php echo confidential; ?>&nbsp;
										</span>

										<div class="dropdown-menu">

											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="confidential1" value="'1'">
												  <label class="form-check-label" for="confidential1">
												    <?php echo confidential; ?>
												  </label>
												</div>	
											</div>

											<div class="dropdown-item">
												<div class="form-check form-checkWidth">
												  <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="not_confidential1" value="'0'">
												  <label class="form-check-label" for="not_confidential1">
												    <?php echo Notconfidential; ?>
												  </label>
												</div>
											</div>

										</div>
									</div>
									<small class="RPSPConfidentialstyle">&nbsp;</small>
								</th>
								
								<th id="tableheadreportresolved">
									<div style="margin-bottom: 17px;"><?php echo Picture; ?><div>
								</th>

							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div> 

		<div class="col-md-3 detail" style="min-width: 280px;overflow: hidden;">
			 <div class="panel panel-inverse panel-primary" >
				<div class="panel-body" style="padding: 0;">
						<div class="row leftpopuppanelstyle">
							<div class="col-md-12">
								<div style="padding: 14px 0 0 0;">
									<a href="javascript:void(0);" onclick="closeDetails();" style="opacity: 1.0!important;">
										<img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
								</div>
							</div>
							<div class="col-md-4 colmd4style">
								<img id="userImage" class="UserImageStyle" src="">  
							</div>
							<div class="col-md-8" class="colmd8Style">
								<span id="userName" class="RPSPUserNameStyle"></span> <br>
								<small style="color: #FFFFFF;font-size: 80%;" id="userId"></small>
							</div>
						</div>
						<form class="p-b-20" action="update_reportProblem_form" method="POST" id="update_reportProblem_form" style="overflow: auto;display: block;height: 500px;">
							<div class="row" style="">
								<div class="col-md-6 RPSPTypeStyle">
									<div class="form-group" style="padding-left: 15px;">
										<select class="form-control" id="type" name="type" style="width: 0px !important;min-width: 108px !important;">
											<option value="1"><?php echo Accident; ?></option>
											<option value="0"><?php echo Incident; ?></option>
											<option value="2"><?php echo Suggestion; ?></option>
										</select>
									</div>
								</div>
								<div class="col-md-6 RPSPColmd6DateStyle">
									<small class="RPSPColmd6SmallStyle" id="date"></small>
								</div>
							</div>
							<div class="row RPSPResolveddStyle">
								<div class="col-md-6">
									<div class="form-check form-check-inline">
										<input  class="form-check-input" type="checkbox" name="isActive" id="resolved" value="1">
										<label 	class="form-check-label" for="resolved" style="font-size: 80% !important;"><?php echo Resolved; ?></label>
									</div>
								</div>
							</div>
							<input type="hidden" id="problemId" name="problemId">
								<div class="row">
									<div class="col-md-12 colmd12Style">
										<b><?php echo Description; ?></b><br>
										<textarea class="textareaheader" name="description" style="color:#002060;" id="description"></textarea>
										<hr class="RPSPhrStyle">
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 colmd12Style">
										<b><?php echo Cause; ?></b><br>
										<textarea  class="textareaheader" name="cause" id="cause"></textarea>
										<hr class="RPSPhrStyle">
									</div>
								</div>

								<div class="row">
									<div class="col-md-12 colmd12Style">
										<b><?php echo Improvement; ?></b><br>
										<textarea  class="textareaheader" name="improvement" id="improvement"></textarea>
										<hr class="RPSPhrStyle">
									</div>
								</div>

								<div class="row" id="pictureRow">
									<div class="col-md-12 colmd12Style">
										<img id="picture" style="width: 200px;" src="">
									</div>
								</div>

								<div class="row" style="margin-top: 10px;">
									<div class="col-md-12 colmd12Style">
										<div id="not_confidentials" style="display: none;">
											<img  width="16" style="" src="<?php echo base_url('assets/img/cross.svg');?>"> 
											<span style="color: red"> &nbsp;&nbsp;<b><?php echo Notconfidential; ?></b></span>
										</div>
										<div id="confidentials" style="display: none;">
											<img width="16" style="" src="<?php echo base_url('assets/img/tick.svg');?>"> 
											<span style="color: green"> &nbsp;&nbsp;<b><?php echo confidential; ?></b></span>
										</div>
										<?php if ($accessPointEdit == true) { ?>
											<center>
												<button id="update_reportProblem_submit" style="width: 35%;" type="submit" class="btn btn-sm btn-primary savebuttonreportpanel"><?php echo save; ?></button>
											</center>
										<?php } ?>
									</div>
								</div>
							</form>
						</div>
					</div>
				</div>
			</div>
		<hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
	</div>
	<a href="javascript:;"class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
		<i class="fa fa-angle-up"></i>
	</a>
</div>
	

