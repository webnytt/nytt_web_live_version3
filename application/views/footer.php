	<script> COLOR_BLACK = 'rgba(255,255,255,0.75)'; </script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/chart-js/Chart.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script> 
	<script src="<?php echo base_url(); ?>assets/plugins/isotope/jquery.isotope.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/lightbox/js/lightbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/input.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/image-picker/image-picker.min.js"></script> 


	<script src="<?php echo base_url('assets/slick/slick.js'); ?>" type="text/javascript" charset="utf-8"></script>

	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/xlsx.full.min.js"></script>
	<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.13.5/jszip.js"></script>
</body>
</html>
<script>

		$('#i_fileOv').change( function(event) 
		{
		    var name = event.target.files[0].name;
		    //$("#disp_tmp_pathOv").html(name);
		    $("#uploadedFileName").html(name);
		    $("#errorFileName").html(name);


		    var fileUpload = document.getElementById("i_fileOv");
 
	        //Validate whether File is valid Excel file.
	        var regex = /^([a-zA-Z0-9\s_\\.\-:])+(.xls|.xlsx)$/;
	        if (regex.test(fileUpload.value.toLowerCase())) {
	            if (typeof (FileReader) != "undefined") {
	                var reader = new FileReader();
	 
	                //For Browsers other than IE.
	                if (reader.readAsBinaryString) {
	                    reader.onload = function (e) {
	                        GetTableFromExcel(e.target.result);
	                    };
	                    reader.readAsBinaryString(fileUpload.files[0]);
	                } else {
	                    //For IE Browser.
	                    reader.onload = function (e) {
	                        var data = "";
	                        var bytes = new Uint8Array(e.target.result);
	                        for (var i = 0; i < bytes.byteLength; i++) {
	                            data += String.fromCharCode(bytes[i]);
	                        }
	                        GetTableFromExcel(data);
	                    };
	                    reader.readAsArrayBuffer(fileUpload.files[0]);
	                }
	            } else {
	                alert("This browser does not support HTML5.");
	            }
	        } else {
	            $(".finalData").hide();
	            $(".fileDesign").hide();
	            $(".apiData").hide();
	            $(".fileError").show();
	        }
		});

		$('#modal-add-machine-parts').on('hidden.bs.modal', function () 
	    {
	    	$("#add_parts_submit").attr('disabled',false);
    		$("#edit_parts_submit").attr('disabled',false);
	    	$("#i_fileOv").val("");
	        $("#finalTableData").html("");
			$("#tableData").html("");
            $(".finalData").hide();	
            $(".tableList").hide();	
            $(".fileError").hide();	
            $(".fileDesign").show();

            $("#partsNumber").val("");
    		$("#partsName").val("");
    		$("#partsOperation").val("");
    		$("#patrsCycletime").val("0");
	    })

		function isDataGetFun(machineId,value)
		{
			if (value == "1") 
			{
				$(".isDataGetRow"+machineId).show();
			}else
			{
				$(".isDataGetRow"+machineId).hide();
			}
		}

		function isDataGetFunAdd(value)
		{
			$("#colorLightSelect").html("");
			$("#isDataGetValue").val(value);
			$("#checkOldValue").val(value);
			data = "<br>";
			running = "";
			waiting = "";
			stopped = "";

			$("#data_image_1").removeClass("check");
			$("#data_image_2").removeClass("check");
			$("#data_image_3").removeClass("check");
			$("#data_image_4").removeClass("check");
			$("#data_image_5").removeClass("check");

			$("#machineLight_1").attr("checked",false);
			$("#machineLight_2").attr("checked",false);
			$("#machineLight_3").attr("checked",false);
			$("#machineLight_4").attr("checked",false);
			$("#machineLight_5").attr("checked",false);
			if (value == "3") 
			{
				$(".noStacklight").fadeOut( "slow" ); 
				$(".noStacklightShow").fadeIn( "slow" );

				$("#running").html(running);
				$("#waiting").html(waiting);
				$("#stopped").html(stopped);
				$("#off").html("");

				$("#colorLightSelect").append(data);

				$("#data_image_1").css("pointer-events","auto");
				$("#data_image_2").css("pointer-events","auto");
				$("#data_image_3").css("pointer-events","auto");
				$("#data_image_4").css("pointer-events","auto");
				$("#data_image_5").css("pointer-events","auto");
			}else if(value == "2")
			{
				

				$(".noStacklightShow").fadeOut( "slow" );
				$(".noStacklight").fadeIn( "slow" );
				$("#data_image_1").addClass("check");
				$("#data_image_2").addClass("check");
				$("#data_image_3").addClass("check");

				$("#machineLight_1").attr("checked",true);
				$("#machineLight_2").attr("checked",true);
				$("#machineLight_3").attr("checked",true);

				data += '<div style="text-align: center;" id="data_image_remove_1"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="<?php echo base_url('assets/img/new_color/FF0000.png') ?>" alt="..." class="img-check" ></div>';
				data += '<div style="text-align: center;" id="data_image_remove_2"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="<?php echo base_url('assets/img/new_color/FFFF00.png') ?>" alt="..." class="img-check" ></div>';
				data += '<div style="text-align: center;" id="data_image_remove_3"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="<?php echo base_url('assets/img/new_color/00FF00.png') ?>" alt="..." class="img-check" ></div>';

				running = "<option  locked='locked' selected value='Green'>"+ displayColorNameFooter('Green') +"</option>";
		    	waiting = "<option  locked='locked' selected value='Yellow'>"+ displayColorNameFooter('Yellow') +"</option>";
		    	stopped = "<option  locked='locked' selected value='Red'>"+ displayColorNameFooter('Red') +"</option>";
		    	$('#running [value="Green"]').attr('locked', 'locked');
		    	$('#waiting [value="Yellow"]').attr('locked', 'locked');
		    	$('#stopped [value="Red"]').attr('locked', 'locked');

		    	$("#running").html(running);
				$("#waiting").html(waiting);
				$("#stopped").html(stopped);
				$("#off").html("");

				$("#colorLightSelect").append(data);

		    	$("#data_image_1").css("pointer-events","none");
				$("#data_image_2").css("pointer-events","none");
				$("#data_image_3").css("pointer-events","none");
				$("#data_image_4").css("pointer-events","none");
				$("#data_image_5").css("pointer-events","none");

				
			}else if(value == "4")
			{
				$(".noStacklightShow").fadeOut( "slow" );
				$(".noStacklight").fadeIn( "slow" );
				$("#data_image_1").addClass("check");
				$("#data_image_3").addClass("check");

				$("#machineLight_1").attr("checked",true);
				$("#machineLight_3").attr("checked",true);

				data += '<div style="text-align: center;" id="data_image_remove_1"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="<?php echo base_url('assets/img/new_color/FF0000.png') ?>" alt="..." class="img-check" ></div>';
				data += '<div style="text-align: center;" id="data_image_remove_3"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="<?php echo base_url('assets/img/new_color/00FF00.png') ?>" alt="..." class="img-check" ></div>';

				running = "<option  locked='locked' selected value='Green'>"+ displayColorNameFooter('Green') +"</option>";
		    	stopped = "<option  locked='locked' selected value='Red'>"+ displayColorNameFooter('Red') +"</option>";

		    	$("#running").html(running);
				$("#waiting").html(waiting);
				$("#stopped").html(stopped);
				$("#off").html("");

				$("#colorLightSelect").append(data);

		    	$("#data_image_1").css("pointer-events","none");
				$("#data_image_2").css("pointer-events","none");
				$("#data_image_3").css("pointer-events","none");
				$("#data_image_4").css("pointer-events","none");
				$("#data_image_5").css("pointer-events","none");
			}
			else
			{

				$(".noStacklightShow").fadeOut( "slow" );
				$(".noStacklight").fadeIn( "slow" );

				$("#running").html(running);
				$("#waiting").html(waiting);
				$("#stopped").html(stopped);
				$("#off").html("");

				$("#colorLightSelect").append(data);

				$("#data_image_1").css("pointer-events","auto");
				$("#data_image_2").css("pointer-events","auto");
				$("#data_image_3").css("pointer-events","auto");
				$("#data_image_4").css("pointer-events","auto");
				$("#data_image_5").css("pointer-events","auto");
			}

			
		}

		function addPatrsDisplay()
		{
			$("#i_fileOv").val("")
			$("#finalTableData").html("");
			$("#tableData").html("");
            $(".tableList").hide();	
            $(".fileError").hide();	
            var workcenterId = $("#workcenterId").val();
		    var employeeId = $("#employeeId").val();

		    if (workcenterId != "" && employeeId != "") 
        	{
        		$(".apiData").show();
        	}else
        	{
        		$(".apiData").hide();
        	}
            $(".fileDesign").show();
		}

		function GetTableFromExcel(data) 
		{
	        var workbook = XLSX.read(data, {
	            type: 'binary'
	        });
	 
	        var Sheet = workbook.SheetNames[0];
	 
	        var excelRows = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[Sheet]);

	        var PartsnumberKey = excelRows.some(obj => obj.hasOwnProperty('Partsnumber'));
			var PartsnameKey = excelRows.some(obj => obj.hasOwnProperty('Partsname'));
			var OperationKey = excelRows.some(obj => obj.hasOwnProperty('Operation'));
			var CycletimeKey = excelRows.some(obj => obj.hasOwnProperty('Cycletime'));

			if (PartsnumberKey == true && PartsnameKey == true && OperationKey == true && CycletimeKey == true) 
	        {
		        var tableData = '<tr><th style="width: 8%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</th><th style="width: 25%;color: black;"><?php echo Partsnumber; ?></th><th style="width: 25%;color: black;"><?php echo Partsname; ?></th><th style="width: 18%;color: black;"><?php echo Operation; ?></th><th style="color: black;"><?php echo Cycletime; ?>(<?php echo seconds; ?>)</th></tr>';
		        var finalTableData = "";

		        for (var i = 0; i < excelRows.length; i++) 
		        {
		           var partsNumber = (excelRows[i].Partsnumber == undefined) ? "" : excelRows[i].Partsnumber;
		           var partsName = (excelRows[i].Partsname == undefined) ? "" : excelRows[i].Partsname;
		           var patrsOperation = (excelRows[i].Operation == undefined) ? "" : excelRows[i].Operation;
		           var partsCycleTime = (excelRows[i].Cycletime == undefined) ? "" : excelRows[i].Cycletime;
		           if (partsNumber != "" && partsName != "" && patrsOperation != "" && partsCycleTime != "") 
		           {
		           	   $("#add_parts_submit").attr('disabled',false);
			           tableData += '<tr><td style="border-top: none;">'+Number(i + 1)+'</td><td style="border-top: none;">'+partsNumber+'</td><td style="border-top: none;">'+partsName+'</td><td style="border-top: none;">'+patrsOperation+'</td><td style="border-top: none;">'+partsCycleTime+'</td></tr>';
			           finalTableData += '<tr id="rowPartsId'+Number(i + 1)+'"><input id="partsNumberOld'+Number(i + 1)+'" type="hidden" value="'+partsNumber+'"><input id="partsNameOld'+Number(i + 1)+'" type="hidden" value="'+partsName+'"><input id="patrsOperationOld'+Number(i + 1)+'" type="hidden" value="'+patrsOperation+'"><input id="partsCycleTimeOld'+Number(i + 1)+'" type="hidden" value="'+partsCycleTime+'"><td style="border-top: none;padding:10px 5px;"><input id="partsNumber'+Number(i + 1)+'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsNumber[]" value="'+partsNumber+'"></td><td style="border-top: none;padding:10px 5px;"><input id="partsName'+Number(i + 1)+'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;color: #002060;font-weight: 700;" readonly name="partsName[]" value="'+partsName+'"></td><td style="border-top: none;padding:10px 5px;"><input id="patrsOperation'+Number(i + 1)+'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsOperation[]" value="'+patrsOperation+'"></td><td style="border-top: none;padding:10px 5px;"><div class="placeholder2" data-placeholder="<?php echo sec; ?>"><input id="partsCycleTime'+Number(i + 1)+'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="patrsCycletime[]" value="'+partsCycleTime+'"></td><td style="text-align: center;border-top: none;width: 20%;padding:10px 5px;"></div><a style="background-color: white;border-color: white; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="editPartsRow('+Number(i + 1)+');" href="javascript:void(0);" class="btn btn-primary editPartsButton editDeleteButton'+Number(i + 1)+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/create.svg") ?>"></a><a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 5px;" onclick="removePartsRow('+Number(i + 1)+');" href="javascript:void(0);" class="btn btn-primary editDeleteButton'+Number(i + 1)+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/delete_white.svg") ?>"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="confirmPartsRow('+Number(i + 1)+');" href="javascript:void(0);" class="confirmDeleteButton'+Number(i + 1)+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/confirmbutton.svg") ?>"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;" onclick="setOriginalValuePartsRow('+Number(i + 1)+');" href="javascript:void(0);" class="confirmDeleteButton'+Number(i + 1)+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/closebutton.svg") ?>"></a></td></tr>';
					}	           
		        }

		        $("#incrementVal").val(Number(i + 1));
		        $("#tableData").html(tableData);
		        $("#finalTableData").html(finalTableData);

		        $(".apiData").hide();
		        $(".fileDesign").hide();
		        $(".tableList").show();
		    }else
		    {
		    	$(".finalData").hide();
	            $(".fileDesign").hide();
	            $(".fileError").show();
		    }
    	}

    	

    	function editPartsRow(rowId)
    	{
    		$("#add_parts_submit").attr('disabled',true);
    		$("#edit_parts_submit").attr('disabled',true);
    		$(".editDeleteButton"+rowId).hide();
    		$(".confirmDeleteButton"+rowId).show();
    		$("#partsNumber"+rowId).removeClass('border-none');
    		$("#partsName"+rowId).removeClass('border-none');
    		$("#patrsOperation"+rowId).removeClass('border-none');
    		$("#partsCycleTime"+rowId).removeClass('border-none');

    		$("#partsNumber"+rowId).addClass('border-left-right-top-hide');
    		$("#partsName"+rowId).addClass('border-left-right-top-hide');
    		$("#patrsOperation"+rowId).addClass('border-left-right-top-hide');
    		$("#partsCycleTime"+rowId).addClass('border-left-right-top-hide');


    		$("#partsNumber"+rowId).attr('readonly',false);
    		$("#partsName"+rowId).attr('readonly',false);
    		$("#patrsOperation"+rowId).attr('readonly',false);
    		$("#partsCycleTime"+rowId).attr('readonly',false);
    	}

    	function confirmPartsRow(rowId)
    	{
    		var partsNumber = $("#partsNumber"+rowId).val();
			var partsName = $("#partsName"+rowId).val();
			var patrsOperation = $("#patrsOperation"+rowId).val();
			var partsCycleTime = $("#partsCycleTime"+rowId).val();

			if (partsNumber != "" && partsName != "" && patrsOperation != "" && partsCycleTime != "") 
			{
				$("#add_parts_submit").attr('disabled',false);
    			$("#edit_parts_submit").attr('disabled',false);
				$(".confirmDeleteButton"+rowId).hide();
	    		$(".editDeleteButton"+rowId).show();
	    		$("#partsNumber"+rowId).removeClass('border-left-right-top-hide');
	    		$("#partsName"+rowId).removeClass('border-left-right-top-hide');
	    		$("#patrsOperation"+rowId).removeClass('border-left-right-top-hide');
	    		$("#partsCycleTime"+rowId).removeClass('border-left-right-top-hide');

	    		$("#partsNumber"+rowId).addClass('border-none');
	    		$("#partsName"+rowId).addClass('border-none');
	    		$("#patrsOperation"+rowId).addClass('border-none');
	    		$("#partsCycleTime"+rowId).addClass('border-none');


	    		$("#partsNumber"+rowId).attr('readonly',true);
	    		$("#partsName"+rowId).attr('readonly',true);
	    		$("#patrsOperation"+rowId).attr('readonly',true);
	    		$("#partsCycleTime"+rowId).attr('readonly',true);

	    		$("#partsNumberOld"+rowId).val($("#partsNumber"+rowId).val());
	    		$("#partsNameOld"+rowId).val($("#partsName"+rowId).val());
	    		$("#patrsOperationOld"+rowId).val($("#patrsOperation"+rowId).val());
	    		$("#partsCycleTimeOld"+rowId).val($("#partsCycleTime"+rowId).val());
			}else
			{
				alert("<?php echo Pleasefillvaliddetails; ?>");
			}

    	
    	}

    	function setOriginalValuePartsRow(rowId)
    	{
    		$("#add_parts_submit").attr('disabled',false);
    		$("#edit_parts_submit").attr('disabled',false);
    		$(".confirmDeleteButton"+rowId).hide();
    		$(".editDeleteButton"+rowId).show();
    		$("#partsNumber"+rowId).removeClass('border-left-right-top-hide');
    		$("#partsName"+rowId).removeClass('border-left-right-top-hide');
    		$("#patrsOperation"+rowId).removeClass('border-left-right-top-hide');
    		$("#partsCycleTime"+rowId).removeClass('border-left-right-top-hide');

    		$("#partsNumber"+rowId).addClass('border-none');
    		$("#partsName"+rowId).addClass('border-none');
    		$("#patrsOperation"+rowId).addClass('border-none');
    		$("#partsCycleTime"+rowId).addClass('border-none');


    		$("#partsNumber"+rowId).attr('readonly',true);
    		$("#partsName"+rowId).attr('readonly',true);
    		$("#patrsOperation"+rowId).attr('readonly',true);
    		$("#partsCycleTime"+rowId).attr('readonly',true);

    		$("#partsNumber"+rowId).val($("#partsNumberOld"+rowId).val());
    		$("#partsName"+rowId).val($("#partsNameOld"+rowId).val());
    		$("#patrsOperation"+rowId).val($("#patrsOperationOld"+rowId).val());
    		$("#partsCycleTime"+rowId).val($("#partsCycleTimeOld"+rowId).val());
    	}

    	function addPatrsData()
    	{
    		var incrementVal = $("#incrementVal").val();
    		var partsNumber = $("#partsNumber").val();
    		var partsName = $("#partsName").val();
    		var patrsOperation = $("#partsOperation").val();
    		var partsCycleTime = $("#patrsCycletime").val();
    		if (partsNumber != "" && partsName != "" && patrsOperation != "" && partsCycleTime != "") 
    		{
    			$("#add_parts_submit").attr('disabled',false);
    			finalTableData = '<tr id="rowPartsId'+incrementVal+'"><input id="partsNumberOld'+incrementVal+'" type="hidden" value="'+partsNumber+'"><input id="partsNameOld'+incrementVal+'" type="hidden" value="'+partsName+'"><input id="patrsOperationOld'+incrementVal+'" type="hidden" value="'+patrsOperation+'"><input id="partsCycleTimeOld'+incrementVal+'" type="hidden" value="'+partsCycleTime+'"><td style="border-top: none;padding:10px 5px;"><input id="partsNumber'+incrementVal+'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsNumber[]" value="'+partsNumber+'"></td><td style="border-top: none;padding:10px 5px;"><input id="partsName'+incrementVal+'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;color: #002060;font-weight: 700;" readonly name="partsName[]" value="'+partsName+'"></td><td style="border-top: none; padding:10px 5px;"><input id="patrsOperation'+incrementVal+'" type="text" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="partsOperation[]" value="'+patrsOperation+'"></td><td style="border-top: none; padding:10px 5px;"><div class="placeholder2" data-placeholder="<?php echo sec; ?>"><input id="partsCycleTime'+incrementVal+'" type="text" onkeypress="return (event.charCode !=8 && event.charCode ==0 || (event.charCode >= 48 && event.charCode <= 57))" class="border-none" style="width: 100%;background-color: #f1f3f5;" readonly name="patrsCycletime[]" value="'+partsCycleTime+'"></div></td><td style="text-align: center;border-top: none;width: 20%;padding:10px 5px;"><a style="background-color: white;border-color: white; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="editPartsRow('+incrementVal+');" href="javascript:void(0);" class="btn btn-primary editPartsButton editDeleteButton'+incrementVal+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/create.svg") ?>"></a><a style="background-color: #F60100;border-color: #F60100; padding: 0px 0px !important;border-radius: 5px;" onclick="removePartsRow('+incrementVal+');" href="javascript:void(0);" class="btn btn-primary editDeleteButton'+incrementVal+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/delete_white.svg") ?>"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;margin-right: 5px;" onclick="confirmPartsRow('+incrementVal+');" href="javascript:void(0);" class="confirmDeleteButton'+incrementVal+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/confirmbutton.svg") ?>"></a><a style="display:none; padding: 0px 0px !important;border-radius: 5px;" onclick="setOriginalValuePartsRow('+incrementVal+');" href="javascript:void(0);" class="confirmDeleteButton'+incrementVal+'"><img style="width: 23px;height: 23px;" src="<?php echo base_url("assets/nav_bar/closebutton.svg") ?>"></a></td></tr>';
	    		$("#finalTableData").append(finalTableData);

	    		$(".fileDesign").hide();
		        $(".tableList").hide();
		        $(".apiData").hide();
	    		$(".finalData").show();
	    		$("#incrementVal").val(Number(incrementVal) + 1);
	    		$("#partsNumber").val("");
	    		$("#partsName").val("");
	    		$("#partsOperation").val("");
	    		$("#patrsCycletime").val("0");
    		}else
    		{
    			alert('<?php echo Pleasefillvaliddetails;  ?>');
    		}
    		
    	}

    	function removePartsRow(rowId)
    	{
    		var r = confirm("<?php echo Areyousureyouwanttodeletethispart; ?> ?");
			if (r == true) {
			  	$("#rowPartsId"+rowId).remove();
			  	$("#edit_parts_submit").attr('disabled',false);
			} 
    	}

    	function showFinalPartsData() 
    	{
    		$(".tableList").hide();
    		$(".finalData").show();
    	}

    	function addNewParts()
    	{
    		$("#partsDataTableData").html("");
    		$(".partsListData").hide();
    		$(".fileDesign").show();

    		var workcenterId = $("#workcenterId").val();
		    var employeeId = $("#employeeId").val();

		    if (workcenterId != "" && employeeId != "") 
        	{
        		$(".apiData").show();
        	}else
        	{
        		$(".apiData").hide();
        	}
    	}


		function addParts(machineId,machineName)
		{
			$.ajax({
		        url: "<?php echo base_url("admin/getPartsList") ?>",
		        type: "post",
		        data: {machineId:machineId} ,
		        success: function (data) 
		        {
		        	var obj = $.parseJSON(data);
		        	var machineObj = obj.machine;
		        	var workcenterId = machineObj.workcenterId;
		        	var employeeId = machineObj.employeeId;
		        	var isMonitor = "<?php echo $this->session->userdata('isMonitor'); ?>";
		        	
		        	if (obj.status == "1") 
		        	{
		        		$("#partsDataTableData").html(obj.htmlData);
		        		$(".fileDesign").hide();
		        		$(".partsListData").show();
		        		
		        		$(".apiData").hide();

		        		if (workcenterId != null && employeeId != null && workcenterId != "" && employeeId != "") 
			        	{
			        		$("#workcenterId").val(workcenterId);
		        			$("#employeeId").val(employeeId);
			        	}else
			        	{
			        		$("#workcenterId").val("");
		        			$("#employeeId").val("");
			        	}

			        	if (isMonitor == "1") 
			        	{
			        		if (workcenterId != null && employeeId != null && workcenterId != "" && employeeId != "") 
				        	{
			        			$("#checkMachinePart").text('<?php echo Addpartfor; ?>');
			        			$("#machineNameTextParts").text(machineName);
				        	}else
				        	{
			        			$("#checkMachinePart").text('<?php echo Addemployeeidfirsttoconnectthismachinetomonitor; ?>.');
			        			$("#machineNameTextParts").text("");
			        			$(".checkForMonitor").hide();
				        	}

			        	}else
			        	{
			        		$("#checkMachinePart").text('<?php echo Addpartfor; ?>');
			        		$("#machineNameTextParts").text(machineName);
			        	}

			        	//$(".addPartsButton").attr("disabled",true);
			        	//$(".upload-btn-wrapper").css("pointer-events","none");
			        	//$(".upload-btn-wrapper").css("opacity","0.4");

		        	}else
		        	{
		        		$(".partsListData").hide();
		        		$(".fileDesign").show();
		        		if (workcenterId != null && employeeId != null && workcenterId != "" && employeeId != "") 
			        	{
			        		$(".apiData").show();
			        		$("#workcenterId").val(workcenterId);
		        			$("#employeeId").val(employeeId);
			        	}else
			        	{
			        		$(".apiData").hide();
			        		$("#workcenterId").val("");
		        			$("#employeeId").val("");
			        	}

			        	if (isMonitor == "1") 
			        	{
			        		if (workcenterId != null && employeeId != null && workcenterId != "" && employeeId != "") 
				        	{
			        			$("#checkMachinePart").text('<?php echo Addthefirtspartfor; ?>');
			        			$("#machineNameTextParts").text(machineName);
			        			$(".checkForMonitor").show();
				        	}else
				        	{
			        			$("#checkMachinePart").text('<?php echo Addemployeeidfirsttoconnectthismachinetomonitor; ?>.');
			        			$("#machineNameTextParts").text("");
			        			$(".checkForMonitor").hide();
				        	}

			        	}else
			        	{
			        		$("#checkMachinePart").text('<?php echo Addthefirtspartfor; ?>');
			        		$("#machineNameTextParts").text(machineName);
			        		$(".checkForMonitor").show();
			        	}

			        	//$(".addPartsButton").attr("disabled",true);
			        	//$(".upload-btn-wrapper").css("pointer-events","none");
			        	//$(".upload-btn-wrapper").css("opacity","0.4");
		        	}	



		        	/*$("#partsNumber").attr('disabled',true);
		    		$("#partsName").attr('disabled',true);
		    		$("#partsOperation").attr('disabled',true);
		    		$("#patrsCycletime").attr('disabled',true);*/

		        	$("#add_parts_submit").attr('disabled',true);
		        	$("#edit_parts_submit").attr('disabled',true);
		        	$("#machineIdParts").val(machineId);
		        	$("#machineIdPartsEdit").val(machineId);
					
					$("#modal-add-machine-parts").modal({
					    backdrop: 'static',
					    keyboard: false
					});
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		}

		function getPartsListWithApi()
		{
			var workcenterId = $("#workcenterId").val();
			$.ajax({
		        url: "<?php echo base_url("admin/getPartsListWithApi") ?>",
		        type: "post",
		        data: {workcenterId:workcenterId},
		        success: function (data) 
		        {
		        	var obj = $.parseJSON(data);
		        	
		        	
		        	if (obj.status == "1") 
		        	{
		        		$("#incrementVal").val(Number(obj.incrementVal + 1));
				        $("#finalTableData").html(obj.htmlData);
				        $("#add_parts_submit").attr("disabled",false);
				        $(".apiData").hide();
				        $(".fileDesign").hide();
				        $(".tableList").hide();
    					$(".finalData").show();
				        $(".apiAddData").hide();
		        	}else
		        	{
		        		alert("No orders available");
		        	}	

		        	
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		}
		
		function logStatusUpdate(machineId,logStatus)
		{
			if (logStatus == "0") 
			{
				message = "Are you sure you want to disable machine log";
			}else
			{
				message = "Are you sure you want to enable machine log";
			}
			var r = confirm(message);
			if (r == true) 
			{
				$.ajax({
			        url: "<?php echo base_url("Admin/updateLogStatus") ?>",
			        type: "post",
			        data: {machineId:machineId,logStatus:logStatus},
			        success: function (data) 
			        {
						dataInput = {"machineId": machineId,"logStatus" : logStatus,"factoryId" : <?php echo $this->factoryId; ?>};
						var dataInput = JSON.stringify(dataInput);
						socket.emit('Update log status', dataInput, (data) => {
						 
						});

						location.reload();
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		}

		function noStacklightFunc()
		{
			if($("#noStacklight").is(':checked'))
			{
			    $(".noStacklight").fadeOut( "slow" ); 
			    $(".noStacklightShow").fadeIn( "slow" ); 
			}
			else
			{
			    $(".noStacklightShow").fadeOut( "slow" );
			    $(".noStacklight").fadeIn( "slow" );
			}
		}


		var getUrl = window.location;
		function ckeck_function(machineId)
		{
			stacklight_disabled_color_value('running',machineId);
			stacklight_disabled_color_value('waiting',machineId);
			stacklight_disabled_color_value('stopped',machineId);
			stacklight_disabled_color_value('off',machineId);
		}

		function get_machine_name(name)
		{
			if (name != "") 
			{
				$(".machine_name").text(name);
			}else
			{
				$(".machine_name").text("<?php echo Yourmachinename; ?>");
			}
		}

		function get_machine_name_edit(name,machineId)
		{
			if (name != "") 
			{
				$(".machine_name_"+machineId).text(name);
			}
			else
			{
				$(".machine_name_"+machineId).text("<?php echo Yourmachinename; ?>");
			}
		}

		function machineLoadingChange(value)
		{
			if (value == "Manual") 
			{
				$("#loading_type").hide();
			}else
			{
				$("#loading_type").show();
			}
		}

		function changeHourStatus(status)
		{
			if (status == "0") 
			{
				message = "Are you sure reduce an hour from clock!";
			}else
			{
				message = "Are you sure increase an hour from clock!";
			}
			var r = confirm(message);
			if (r == true) 
			{
				$.ajax({
			        url: "<?php echo base_url("admin/updateHourStatus") ?>",
			        type: "post",
			        data: {status:status} ,
			        success: function (data) 
			        {
			        	location.reload();
			        	
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			} 
		}

		function machineLoadingChangeMachine(value,machineId)
		{
			if (value == "Manual") 
			{
				$("#loading_type_"+machineId).hide();
			}else
			{
				$("#loading_type_"+machineId).show();
			}
		}

		function add_color_sequence(title)
		{
   			var title_value1 = displayColorNameInEnglish(title);
			title_value1 = title_value1.split(" - ");

			title_value = title_value1.join(",");
			console.log(title_value);


			$('#running  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#waiting  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#stopped  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#off  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);

			window.setTimeout(function(){
				disabled_color_value("running");
				disabled_color_value("waiting");
				disabled_color_value("stopped");
				disabled_color_value("off");
			},100);



		}


		function add_stacklight_color_sequence(title,type,machineId)
		{
		    var title_value1 = displayColorNameInEnglish(title);
			title_value1 = title_value1.split(" - ");
			title_value = title_value1.join(",");
			html = "<option value='"+title_value+"'>"+title+"</option>";
			$('#running'+machineId+' option[value="' + title_value + '"]').remove();
			$('#waiting'+machineId+' option[value="' + title_value + '"]').remove();
	    	$('#stopped'+machineId+' option[value="' + title_value + '"]').remove();
		    $('#off'+machineId+' option[value="' + title_value + '"]').remove();
			$("#running"+machineId).append(html);
			$("#waiting"+machineId).append(html);
			$("#stopped"+machineId).append(html);
			$("#off"+machineId).append(html);
		}

		function disabled_color_value(type)
		{	
			disabled_value = [];
			$("#"+type+" :selected").map(function(i, el) {
	       		 disabled_value.push($(el).val());
			}).get();

			$.each(disabled_value, function(k, v) 
	        {
	        	if (type == "running") 
	        	{
	        		
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
	        	}
	        	if (type == "waiting") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
			    }
	        	if (type == "stopped") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
			    }
	        	if (type == "off") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				}


				$("#running").select2("destroy");
				$("#waiting").select2("destroy");
				$("#stopped").select2("destroy");
				$("#off").select2("destroy");

				$("#running").select2({
					placeholder: "<?php echo Selectanoption; ?>"
				});
				$("#waiting").select2({
					placeholder: "<?php echo Selectanoption; ?>"
				});
				$("#stopped").select2({
					placeholder: "<?php echo Selectanoption; ?>"
				});
				$("#off").select2({
					placeholder: "<?php echo Selectanoption; ?>"
				});
				$(".select2-selection__choice").attr("onclick","add_color_sequence(this.title)");
			});	
				
		}
		function stacklight_disabled_color_value(type,machineId)
		{	
			disabled_value = [];

	        $("#"+type+machineId+" :selected").map(function(i, el) {
	       		 disabled_value.push($(el).val());
			}).get();
			
			$.each(disabled_value, function(k, v) 
	        {
	        	if (type == "running" && v != "off") 
	        	{
	        		
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
	        	}
	        	if (type == "waiting" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
			    }
	        	if (type == "stopped" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
			    }
	        	if (type == "off" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				}


				$(".select2-selection__choice").attr("onclick","add_stacklight_color_sequence(this.title,'"+ type +"',"+ machineId +")");
			});	
				
		}

		function displayColorNameFooter(colorArr)
		{
			var colorArrExport = colorArr.split(" - ");

			var color = "";

			for (i = 0; i < colorArrExport.length; i++) 
			{
				//console.log(colorArrExport[i]);
				if (colorArrExport[i] == "Red") 
				{
					colorArrExport[i] = "<?php echo red ?>";	
				}else if(colorArrExport[i] == "Green")
				{
					colorArrExport[i] = "<?php echo green ?>";
				}else if(colorArrExport[i] == "Yellow")
				{
					colorArrExport[i] = "<?php echo yellow ?>";
				}else if(colorArrExport[i] == "Blue")
				{
					colorArrExport[i] = "<?php echo blue ?>";
				}else if(colorArrExport[i] == "White")
				{
					colorArrExport[i] = "<?php echo white ?>";
				}else if(colorArrExport[i] == "off")
				{
					//console.log( "<?php echo Off ?>")
					colorArrExport[i] = "<?php echo Off ?>";
				}

				color += (i == 0) ? colorArrExport[i] : " - "+colorArrExport[i];
			}
		
			return color;
		}

		function displayColorNameCheckLiveStatus(colorArr)
		{
			var colorArrExport = colorArr.split(" ");


			var color = "";

			for (i = 0; i < colorArrExport.length; i++) 
			{
				if (colorArrExport[i] == "red") 
				{
					colorArrExport[i] = "<?php echo red ?>";	
				}else if(colorArrExport[i] == "green")
				{
					colorArrExport[i] = "<?php echo green ?>";
				}else if(colorArrExport[i] == "yellow")
				{
					colorArrExport[i] = "<?php echo yellow ?>";
				}else if(colorArrExport[i] == "blue")
				{
					colorArrExport[i] = "<?php echo blue ?>";
				}else if(colorArrExport[i] == "white")
				{
					colorArrExport[i] = "<?php echo white ?>";
				}else if(colorArrExport[i] == "off")
				{
					colorArrExport[i] = "<?php echo Off ?>";
				}else if(colorArrExport[i] == "NoDataStacklight")
				{
					colorArrExport[i] = "<?php echo NoDataStacklight ?>";
				}

				color += (i == 0) ? colorArrExport[i] : " "+colorArrExport[i];
			}
		
			return color;
		}

		function displayStatusName(argument) 
		{
			if(argument == "Running")
			{
				var argument = "<?php echo Running; ?>";
			}else if(argument == "Waiting")
			{
				var argument = "<?php echo Waiting; ?>";
			}else if(argument == "Stopped")
			{
				var argument = "<?php echo Stopped; ?>";
			}else if(argument == "Off")
			{
				var argument = "<?php echo Off; ?>";
			}else if(argument == "Nodet")
			{
				var argument = "<?php echo NoData; ?>";
			}

			return argument;
		}

		function displayColorNameInEnglish(colorArr)
		{
			var colorArrExport = colorArr.split(" - ");

			var color = "";

			for (i = 0; i < colorArrExport.length; i++) 
			{
				if (colorArrExport[i] == "Red" || colorArrExport[i] == "Röd" || colorArrExport[i] == "red" || colorArrExport[i] == "röd") 
				{
					colorArrExport[i] = "Red";	
				}else if(colorArrExport[i] == "Green" || colorArrExport[i] == "Grön" || colorArrExport[i] == "green" || colorArrExport[i] == "grön")
				{
					colorArrExport[i] = "Green";
				}else if(colorArrExport[i] == "Yellow" || colorArrExport[i] == "Gul" || colorArrExport[i] == "yellow" || colorArrExport[i] == "gul")
				{
					colorArrExport[i] = "Yellow";
				}else if(colorArrExport[i] == "Blue" || colorArrExport[i] == "Blå" || colorArrExport[i] == "blue" || colorArrExport[i] == "blå")
				{
					colorArrExport[i] = "Blue";
				}else if(colorArrExport[i] == "White" || colorArrExport[i] == "Vit" || colorArrExport[i] == "white" || colorArrExport[i] == "vit")
				{
					colorArrExport[i] = "White";
				}else if(colorArrExport[i] == "Off" || colorArrExport[i] == "Av" || colorArrExport[i] == "off" || colorArrExport[i] == "av")
				{
					colorArrExport[i] = "Off";
				}

				color += (i == 0) ? colorArrExport[i] : " - "+colorArrExport[i];
			}
		
			return color;
		}

		$(function() {
	       $('.overview-multiple').select2({
	         tags: true,
	         placeholder: 'Select an option',
	         templateSelection : function (tag, container){
	                // here we are finding option element of tag and
	            // if it has property 'locked' we will add class 'locked-tag' 
	            // to be able to style element in select
	            var $option = $('.overview-multiple option[value="'+tag.id+'"]');
	            if ($option.attr('locked')){
	               $(container).addClass('locked-tag');
	               tag.locked = true; 
	               $(container).find( "span" ).text("");
	            }

	            return tag.text;
	         },
	       })
	       .on('select2:unselecting', function(e){
	            // before removing tag we check option element of tag and 
	          // if it has property 'locked' we will create error to prevent all select2 functionality
	           if ($(e.params.args.data.element).attr('locked')) {
	               e.preventDefault();
	              $(container).find( "span" ).text("");
	            }
	         });
	    });


		$(document).ready(function() 
		{

			


			<?php if($this->uri->segment(2) == 'overview2') { ?>
			setInterval(function()
			{
			  	location.reload();
			}, 60 * 10000);

			<?php } ?>


			setInterval(function()
			{
			  	location.href("<?php echo base_url("admin/logout"); ?>");
			}, 7000 * 1000);
		
		$(".js-example-basic-multiple")
		.select2({
			placeholder: "<?php echo Selectanoption; ?>",
			"language": {
		       "noResults": function(){
		           return "<?php echo NoResultsFound; ?>";
		       }
		   },
		    escapeMarkup: function (markup) {
		        return markup;
		    }
		});

		var selected = new Array();
		var colors_ids = new Array();
		$(".img-check").click(function(){

				var checkOldValue = $("#checkOldValue").val();
				if (checkOldValue == "0") 
				{
					selected = jQuery.grep(selected, function(value) {
						return value != 'Red';
					});
					selected = jQuery.grep(selected, function(value) {
						return value != 'Yellow';
					});
					selected = jQuery.grep(selected, function(value) {
						return value != 'Green';
					});
					selected = jQuery.grep(selected, function(value) {
						return value != 'Blue';
					});
					selected = jQuery.grep(selected, function(value) {
						return value != 'White';
					});
					$("#checkOldValue").val("");
					$("#nodet").val("");
				}
				ids = $(this).attr('id');
				image = $(this).attr('data-img-src');
				color_name = $(this).attr('data-color-name');
				i = $(this).attr('data-color_id');
				spilit_idds = ids.split("_");
				check_uncheck = 0;
				if ($("#machineLight_"+i).prop('checked')==true)
			    { 
			    	$("#data_image_remove_"+i).remove();
			    	selected = jQuery.grep(selected, function(value) {
					  return value != color_name;
					});

					$(this).removeClass("check");
					$("#machineLight_"+i).attr("checked",false);
					check_uncheck = 1;
			    }else if($("#machineLight_"+i).prop('checked')==false)
			    {
			    	 data = '<div style="text-align: center;" id="data_image_remove_'+i+'"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="'+image+'" alt="..." class="img-check" ></div>';

			        $("#colorLightSelect").append(data);

			        $("#machineLight_"+i).attr("checked",true);

			        selected.push(color_name);
					$(this).addClass("check");
					check_uncheck = 1;
			    }


				if(check_uncheck = 1){
					html = "";
					if (selected.length == 1) 
				    {
						var nodet_array = ["off"];
				    	nodet_array.push(selected[0]);
				    	html += "<option value='off'><?php echo Off; ?></option>";
				    	html += "<option value='"+ selected[0] +"'>"+ displayColorNameFooter(selected[0]) +"</option>";
				    }
				    else if (selected.length == 2) 
				    {
						var nodet_array = ["off"];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[0]+','+selected[1]);
				    	html += "<option value='off'><?php echo Off; ?></option>";
				    	html += "<option value='"+selected[0]+"'>"+ displayColorNameFooter(selected[0]) +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ displayColorNameFooter(selected[1]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1]) +"</option>";
				    } else if (selected.length == 3) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	html += "<option value='off'><?php echo Off; ?></option>";
				    	html += "<option value='"+selected[0]+"'>"+ displayColorNameFooter(selected[0]) +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ displayColorNameFooter(selected[1]) +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ displayColorNameFooter(selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[1] +"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1]) +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[2] +"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[2] +"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1] +','+selected[2] +"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2]) +"</option>";
				    }else if (selected.length == 4) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]);



				    	html += "<option value='off'><?php echo Off; ?></option>";
				    	html += "<option value='"+selected[0]+"'>"+ displayColorNameFooter(selected[0]) +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ displayColorNameFooter(selected[1]) +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ displayColorNameFooter(selected[2]) +"</option>";
				    	html += "<option value='"+selected[3]+"'>"+ displayColorNameFooter(selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[1]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1]) +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[2]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[2]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[3]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[2]+','+ selected[3]+"'>"+ displayColorNameFooter(selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+ selected[0] +','+ selected[1] +','+ selected[2] +','+ selected[3] +"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
				    }else if (selected.length == 5) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[3]);
				    	nodet_array.push(selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[4]);
				    	nodet_array.push(selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);


				    	html += "<option value='off'><?php echo Off; ?></option>";
				    	html += "<option value='"+selected[0]+"'>"+ displayColorNameFooter(selected[0]) +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ displayColorNameFooter(selected[1]) +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ displayColorNameFooter(selected[2]) +"</option>";
				    	html += "<option value='"+selected[3]+"'>"+ displayColorNameFooter(selected[3]) +"</option>";
				    	html += "<option value='"+selected[4]+"'>"+ displayColorNameFooter(selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[2]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[3]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[4]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[2]+','+selected[4]+"'>"+ displayColorNameFooter(selected[2] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[3] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
						
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[4]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2] +' - '+ selected[4]) +"</option>";
						html += "<option value='"+selected[1]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
						html += "<option value='"+selected[2]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[2] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
						
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[3]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
						
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[1] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
						
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ displayColorNameFooter(selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4]) +"</option>";
				    }
				}  
				$("#running").html(html);
				$("#waiting").html(html);
				$("#stopped").html(html);
				$("#off").html(html);
				$("#nodet").val(nodet_array);
			});
			
			App.init();
			
			$("select#factoryId").select2({  
			  tags: false,
			  minimumResultsForSearch: 4 
			});
			
			$("select#factoryId").on("select2:select", function (evt) { 
				var factoryId = $("select#factoryId").val();
				$.post('<?php echo base_url();?>admin/changeFactory',{factoryId:factoryId},function getattribute(data) {  
					var obj = jQuery.parseJSON( data );
					//console.log(obj); 
					factoryType = obj.factoryType;
					previousFactoryType =  obj.previousFactoryType;

					if(factoryType == previousFactoryType)
					{
						location.reload();
					}else
					{
						location.href = "<?php echo base_url("admin/overview2"); ?>";
					}
				});
			});
		});
	(function(jQuery){
            jQuery.fn.MytoJson = function(options) 
            {
        
            options = jQuery.extend({}, options);
        	var self = this,
                json = {},
                push_counters = {},
                patterns = {
                    "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                    "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                    "push":     /^$/,
                    "fixed":    /^\d+$/,
                    "named":    /^[a-zA-Z0-9_]+$/
                };
        		this.build = function(base, key, value){
                base[key] = value;
                return base;
            };
        		this.push_counter = function(key){
                if(push_counters[key] === undefined){
                    push_counters[key] = 0;
                }
                return push_counters[key]++;
            };
        
            jQuery.each(jQuery(this).serializeArray(), function()
            {
        		if(!patterns.validate.test(this.name))
        		{
                    return;
                }
        		var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;
        			while((k = keys.pop()) !== undefined){
        			reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
        			if(k.match(patterns.push))
        			{
                        merge = self.build([], self.push_counter(reverse_key), merge);
                    }
                    else if(k.match(patterns.fixed)){
                        merge = self.build([], k, merge);
                    }
                    else if(k.match(patterns.named)){
                        merge = self.build({}, k, merge);
                    }
                }
        		json = jQuery.extend(true, json, merge);
            });
        	return json;
        }
        })(jQuery);

	</script>
		<script src='<?php echo base_url('assets/socket/socket.io.js') ?>'></script> 
	<script>

	


		var factoryId = "<?php echo $this->session->userdata('factoryId'); ?>"; 
		var socket = io.connect('https://nyttcloud.host:3006'); 

		var randomNumber;
		function checkLiveStatus(machineId,machineName)
		{
			var randomNumber = $("#randomNumber").val();
			dataInput = {"userId": randomNumber,"machineId" : machineId};
			//socket.emit('Screen Share Request', dataInput);

			socket.emit('Screen Share Request', dataInput, (data) => {
			 // console.log(data); // data will be 'woot'
			});
			$("#machineIdCheckIn").val(machineId);
			$("#liveMachineName").text(machineName.replace("+","'"));
			$("#compareRandomNumber").val(randomNumber);
		}


		function AddBreakdownReason()
		{
			dataInput = {"factoryId":"5","originalTime":"2021-09-16 07:57:26","IOName":"GPIO16","signal":"0"};
			//var dataInput = JSON.stringify(dataInput);

			//socket.emit('AddVirtualMachineLog', dataInput);

		}



		
		

		socket.on('remove_machine_parts_in_order_response',function(obj) 
		{	 
			console.log(obj);
		});


		


		socket.on('setapp_not_start_response',function(obj) 
		{	 
			console.log(obj);
			var factoryIdObj = obj.factoryId;
			var userId = obj.userId;
			var machineId = obj.machineId;
			var message = obj.message;
			var machineSetAppNotStart = $("#machineSetAppNotStart").val();
			var sessionUserId = "<?php echo $this->session->userdata('userId'); ?>";

			if (factoryId == factoryIdObj && userId == sessionUserId && machineSetAppNotStart == 1) 
			{
				//console.log(obj);
				$(".statusMessage"+machineId).css("color", "#FF8000") 
				$(".statusMessage"+machineId).addClass("m-t-10");
				$("#card-light"+machineId).show();
				$("#machineStatusDisconnect"+machineId).val("0");
				$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+',0)" style="width: 100px;margin-left: 35px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
				$(".statusMessage"+machineId).html('<?php echo SetAppisonhomescreenCheckyourphoneorcontactinfonytttechcom; ?>');

				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: message
				});
				$("#machineSetAppNotStart").val(0);
			}
		});

		socket.on('virtual_machine_log_response',function(obj) 
		{	 
			var machineId = obj['machineId'];
			var IOName = obj['IOName'];
			var signal = obj['signal'];
		    var siteLang = "<?php echo $siteLang ?>";

		    if(factoryId == obj['factoryId']) 
			{
			    if (signal == "0") 
				{ 
					if (siteLang == "2") 
					{
						$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/inactiveSw.png") ?>")
					}else
					{
						$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/not_active.png") ?>")
					}
					$(".detectionImage"+machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.stopImage); ?>">');
					$(".statusMessage"+machineId).html('<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.IOconnectlink; ?>');
				}else if (signal == "1") 
				{
					if (siteLang == "2") 
					{
						$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/activeSw.png") ?>")
					}else
					{
						$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/active.png") ?>")
					}
					$(".detectionImage"+machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.runningImage); ?>">');
					$(".statusMessage"+machineId).html('<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.IOconnectlink; ?>');
				}
				if(signal == "1") 
				{
					$("#show_colorGO"+machineId).addClass("rounded");
					$("#show_colorGO"+machineId).css("background-color", "#76BA1B");
					$("#show_colorGO"+machineId).css("opacity", "1 !important");
					$("#show_colorGO"+machineId).css("box-shadow", "none");
					$("#show_colorG"+machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorGO"+machineId).removeClass("rounded");
					$("#show_colorGO"+machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+machineId).css("box-shadow", "none"); 
					$("#show_colorG"+machineId).css("opacity", "0.4"); 
				}

				if(signal == "0") {
					$("#show_colorRO"+machineId).addClass("rounded");
					$("#show_colorRO"+machineId).css("background-color", "#F60100");
					$("#show_colorRO"+machineId).css("opacity", "1 !important");
					$("#show_colorRO"+machineId).css("box-shadow", "none");
					$("#show_colorR"+machineId).css("opacity", "1");
				} else 
				{
					$("#show_colorRO"+machineId).removeClass("rounded");
					$("#show_colorRO"+machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+machineId).css("opacity", "!important"); 
					$("#show_colorRO"+machineId).css("box-shadow", "none"); 
					$("#show_colorR"+machineId).css("opacity", "0.4"); 
				}
			}
		});

		

		socket.on('change_machine_status_response',function(response) 
		{	 
			var machineStatus = response.machineStatus;
			var machineId = response.machineId;
			
			var isMachineNoStacklight = $("#isMachineNoStacklight"+machineId).val();
			var machineIsDataGet = $("#machineIsDataGet"+machineId).val();
			if(factoryId == response.factoryId) 
			{
				if (machineIsDataGet == 4 && machineStatus == "0") 
				{
					$(".detectionImage"+machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	
					$(".statusMessage"+machineId).html('<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.IOconnectlink; ?>');
				}
				else if (machineIsDataGet == 2 && machineStatus == "0") 
				{
					$(".detectionImage"+machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	
					$(".statusMessage"+machineId).html('<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.MTconnectlink; ?>');
				}else
				{
					$(".statusMessage"+machineId).css("color", "#FF8000") 
					$(".statusMessage"+machineId).addClass("m-t-10");
					if (machineStatus == "0") 
					{ 
						$("#machineStatusDisconnect"+machineId).val("0");
						if (isMachineNoStacklight == "0") 
						{
							$("#card-light"+machineId).show();
							$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+','+machineStatus+')" style="width: 100px;margin-left: 35px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
							$(".statusMessage"+machineId).html('<?php echo SetAppstartedsuccessfullyCamerawillstartinnext30secondsforselectedmachine; ?>');
						}else
						{
							var siteLang = "<?php echo $siteLang ?>";
							if(siteLang == "2")
							{
								$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/producationSw.png'); ?>">');
							}else
							{
								$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/production.png'); ?>">');
							}
							$(".statusMessage"+machineId).html(' <?php echo Nosetappinstallationrequiredasnostacklightisavailable?>.');
						}
					}else if (machineStatus == "1") 
					{
						$("#card-light"+machineId).hide();
						$(".statusMessage"+machineId).css("min-height", "35px") 
						$("#machineStatusDisconnect"+machineId).val("1");
						

						var siteLang = "<?php echo $siteLang ?>";
						if(siteLang == "2")
						{
							$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setupSw.png'); ?>">');
						}else
						{
							$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">');
						}

						if (isMachineNoStacklight == "0") 
						{
							$(".statusMessage"+machineId).html("<?php echo MachineisinsetupstateYoucantstartsetapprightnow; ?>.");
						}else
						{
							$(".statusMessage"+machineId).html(' <?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?>.');
						}
					}else
					{
						$("#card-light"+machineId).hide();
						$("#machineStatusDisconnect"+machineId).val("2");
						var siteLang = "<?php echo $siteLang ?>";
						if(siteLang == "2")
						{
							$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/noproductionSw.png'); ?>">');
						}else
						{
							$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">');
						}


						if (isMachineNoStacklight == "0") 
						{
							$(".statusMessage"+machineId).html("<?php echo machine_is_in_no_producation_state_you_can_t_start_setapp_right_now; ?>");
						}else
						{
							$(".statusMessage"+machineId).html(' <?php echo Nosetappinstallationrequiredasnostacklightisavailable; ?>.');
						}
					}
				}
				
			}
		});	

		socket.on('add_breakdown_reason_response',function(obj) 
		{	 
			machineId = $("#machineIdStop").val();
			analytics = $("#analytics").val();
			if(factoryId == obj.factoryId && machineId == obj.machineId) 
			{
				if (analytics == "stopAnalytics") 
				{
					$("#errorNotification").text((obj.receiveErrorNotification == "0") ? "None" : "After "+ obj.receiveErrorNotification +" min");
					$("#errorReasonNotification").text((obj.provideReasonForErrors == "0") ? "None" : "After "+ obj.provideReasonForErrors +" min");
				}else
				{
					$("#errorNotification").text((obj.receiveWaitingNotification == "0") ? "None" : "After "+ obj.receiveWaitingNotification +" min");
					$("#errorReasonNotification").text((obj.provideReasonForWaiting == "0") ? "None" : "After "+ obj.provideReasonForWaiting +" min");
				}
			}
		});	

		
		var iJK = 1;
		socket.on('change_setting_data_response',function(userId) 
		{	 
			$.ajax({
		        url: "<?php echo base_url("Operator/getOperatorDetailById") ?>",
		        type: "post",
		        data: {userId:userId} ,
		        success: function (response) 
		        { 
			        	var obj = $.parseJSON(response);
			        	$("#userNameText"+userId).text(obj.userName);
			        	$("#userImageText"+userId).attr("src","<?php echo base_url('assets/img/user/') ?>"+obj.userImage+'?'+iJK);

			        	$("#userName"+userId).val(obj.userName);
			        	$(".file-upload-image-edit"+userId).attr("src","<?php echo base_url('assets/img/user/') ?>"+obj.userImage+'?'+iJK);
			        	iJK++;
			       
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$('#liveDetectonImage').on('hidden.bs.modal', function () {
		   $("#machinePopupOpen").val(1)
		})

		socket.on('screen_share_update_response',function(response) 
		{	 
			//console.log(response);
			var obj = $.parseJSON(response);
			var machineIdCheckIn = $("#machineIdCheckIn").val();
			var factoryIdObj = obj.factoryId;
			var machinePopupOpen = $("#machinePopupOpen").val();
			if (machineIdCheckIn == obj.machineId && machinePopupOpen == 1 && factoryId == factoryIdObj) 
			{
				var detectionName = obj.detectionName;
				detectionState = obj.stateVal;
				var currentTime = moment(parseInt(obj.currentTime)).format("DD/MM/YYYY HH:mm:ss");
				var compareFacotryId = obj.factoryId;
				var userId = "<?php echo $this->session->userdata('userId'); ?>";
				var sessionUserId = obj.userId;
				var binary = '';
			    var bytes = new Uint8Array( obj.imageBytes );
			    var len = bytes.byteLength;
			    for (var i = 0; i < len; i++) {
			        binary += String.fromCharCode( bytes[ i ] );
			    }
			    imageBytes = window.btoa( binary );
				document.getElementById("ItemPreview").src = "data:image/png;base64," + imageBytes;
				var randomNumber = $("#randomNumber").val();
				var compareRandomNumber = $("#compareRandomNumber").val();

				$("#detectionMachineId").val(obj.machineId);
				$("#imageName").val(imageBytes);
				$("#currentTime").text(currentTime);
				$("#detectionName").text(displayColorNameCheckLiveStatus(detectionName));
				$("#detectionState").text(displayStatusName(detectionState));

				$("#timestamp").val(currentTime);
				$("#color").val(detectionName);
				$("#state").val(detectionState);
				
				if (sessionUserId == randomNumber) 
				{	
					$("#liveDetectonImage").modal();
					$("#machinePopupOpen").val(0)
				}
			}
		});	

			

		socket.on('Battery Update',function(factoryIdSocket, machineId, battery, chargeFlag) 
		{
			 if(factoryId == factoryIdSocket) {
				 $("#battery_percentage"+machineId).attr('data-original-title', 'Battery '+battery+'%'); 

				 $("#batteryLevel"+machineId).css("width",battery+"%");
				 $("#batteryLevelO"+machineId).css("width",battery+"%");
				 $("#textBatteryLevel"+machineId).text(battery+"%");
				if(battery >= 50) { 
					
					$("#textBatteryLevel"+machineId).css("color","#76BA1B"); 
					$("#batteryOuter"+machineId).css("border-color","#76BA1B"); 
					$("#batteryLevelO"+machineId).css("border-color","#76BA1B"); 
					$("#batteryLevelO"+machineId).css("background-color","#76BA1B"); 
					$("#batteryBump"+machineId).css("background-color","#76BA1B"); 
					$("#batteryLevel"+machineId).css("background-color","#ff8000"); 
				} else {
					$("#textBatteryLevel"+machineId).css("color","#F60100"); 
					$("#batteryOuter"+machineId).css("border-color","#F60100"); 
					$("#batteryLevelO"+machineId).css("border-color","#F60100"); 
					$("#batteryLevelO"+machineId).css("background-color","#F60100"); 
					$("#batteryBump"+machineId).css("background-color","#F60100"); 
					$("#batteryLevel"+machineId).css("background-color","#F60100");
				}
				 if(chargeFlag == '1') {
					 $("#chargeFlag"+machineId).removeClass('hide').addClass('show')
				 } else if(chargeFlag == '0') {
					 $("#chargeFlag"+machineId).removeClass('show').addClass('hide')
				 } 
			 }
		  });
		socket.on('Connect SetApp', function(id, factoryIdSocket, machineId) 
		{ 
			if(factoryId == factoryIdSocket) {
				
				if($("#cardContent"+machineId).hasClass('card-inactive') == true) {
					$("#cardContent"+machineId).removeClass('card-inactive');
				}
				$("#inactiveText"+machineId).html(''); 
			}
		});


		socket.on('mt_connect_log_response', function(data) 
		{ 
			var machineIsDataGet = $("#machineIsDataGet"+data.machineId).val();
			if(factoryId == data.factoryId && machineIsDataGet == "2") 
			{ 
			
				//console.log(data)
			
				$("#machinePartsMtConnect"+data.machineId).text(data.PartcountNew);
				if (data.stateVal == "Running") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.runningImage); ?>">');
				}else if (data.stateVal == "Waiting") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.waitingImage); ?>">');
				}else if (data.stateVal == "Stopped") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.stopImage); ?>">');
				}else if (data.stateVal == "nodet") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	
				}

				if(data.stateVal == "Stopped") {
					$("#show_colorRO"+data.machineId).addClass("rounded");
					$("#show_colorRO"+data.machineId).css("background-color", "#F60100");
					$("#show_colorRO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorRO"+data.machineId).css("box-shadow", "none");
					$("#show_colorR"+data.machineId).css("opacity", "1");
				} else 
				{
					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorR"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.stateVal == "Waiting") 
				{
					$("#show_colorYO"+data.machineId).addClass("rounded");
					$("#show_colorYO"+data.machineId).css("background-color", "#FFCF00");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");
					$("#show_colorY"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorY"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.stateVal == "Running") 
				{
					$("#show_colorGO"+data.machineId).addClass("rounded");
					$("#show_colorGO"+data.machineId).css("background-color", "#76BA1B");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorGO"+data.machineId).css("box-shadow", "none");
					$("#show_colorG"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorG"+data.machineId).css("opacity", "0.4"); 
				}
				$(".statusMessage"+data.machineId).html('<?php echo '&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;'.MTconnectlink; ?>');
			}	
		});

		
		socket.on('Disconnect SetApp', function(reason, factoryIdSocket, machineId)
		{ 
			//console.log("factoryIdSocket"+factoryIdSocket+" machineId - "+machineId+",reason - "+reason);	
			if(factoryId == factoryIdSocket) {
				if($("#cardContent"+machineId).hasClass('card-inactive') == false) {
					$("#cardContent"+machineId).addClass('card-inactive');
				}
				var reasonText;
				if(reason == 'client namespace disconnect' || reason == 'transport close' || reason == 'transport error') { 
					reasonText = '<?php echo SetAppturnedoff; ?>.';
				} else if(reason == 'detection disconnect') {
					reasonText = '<?php echo SetAppisonhomescreen; ?>.';
				} else if(reason == 'ping timeout') {
					reasonText = '<?php echo Nointernetorphoneturnedoff; ?>.';
				} else if(reason == 'app close force fully') {
					reasonText = '<?php echo app_close_force_fully; ?>.';
				} else if(reason == 'app stop due to error') {
					reasonText = '<?php echo SetAppcrashedandstoppedduetoexception; ?>.';
				} else if(reason == 'setApp restart') {
					reasonText = '<?php echo SetApprestartedduetosomeexception; ?>.';
				} 
				$("#inactiveText"+machineId).html(reasonText+' <?php echo Checkyourphoneorcontactinfonytttechcom; ?>'); 

				$("#show_colorRO"+machineId).removeClass("rounded");
				$("#show_colorRO"+machineId).css("border-radius", "13px"); 
				$("#show_colorRO"+machineId).css("border", "3px solid #F60100"); 
				$("#show_colorRO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorRO"+machineId).css("opacity", "!important"); 
				$("#show_colorRO"+machineId).css("box-shadow", "none"); 

				$("#show_colorYO"+machineId).removeClass("rounded");
				$("#show_colorYO"+machineId).css("border-radius", "13px"); 
				$("#show_colorYO"+machineId).css("border", "3px solid #FFCF00"); 
				$("#show_colorYO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorYO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorYO"+machineId).css("box-shadow", "none");

				$("#show_colorGO"+machineId).removeClass("rounded");
				$("#show_colorGO"+machineId).css("border-radius", "13px"); 
				$("#show_colorGO"+machineId).css("border", "3px solid #76BA1B"); 
				$("#show_colorGO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorGO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorGO"+machineId).css("box-shadow", "none"); 

				$("#show_colorWO"+machineId).removeClass("rounded");
				$("#show_colorWO"+machineId).css("border-radius", "13px"); 
				$("#show_colorWO"+machineId).css("border", "3px solid black"); 
				$("#show_colorWO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorWO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorWO"+machineId).css("box-shadow", "none"); 

				$("#show_colorBO"+machineId).removeClass("rounded");
				$("#show_colorBO"+machineId).css("border-radius", "13px"); 
				$("#show_colorBO"+machineId).css("border", "3px solid #124D8D"); 
				$("#show_colorBO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorBO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorBO"+machineId).css("box-shadow", "none");

				$(".statusMessage"+machineId).css("color", "#FF8000") 
				$(".statusMessage"+machineId).addClass("m-t-10");
				var machineStatusDisconnect = $("#machineStatusDisconnect"+machineId).val();
				if (machineStatusDisconnect == "0") 
				{
					$(".statusMessage"+machineId).html(reasonText+' <?php echo Checkyourphoneorcontactinfonytttechcom; ?>');
					if(reason == 'client namespace disconnect' || reason == 'transport close' || reason == 'transport error' || reason == 'detection disconnect' || reason == 'app close force fully' || reason == 'app stop due to error' || reason == 'setApp restart') 
					{ 
						$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+',0)" style="width: 100px;margin-left: 18px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
					} else if(reason == 'ping timeout') 
					{
						$(".detectionImage"+machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	
					} 
				}
			}
		});
		
		
		var method = 'refresh_color'; 
		<?php if($this->uri->segment(2) == 'overview' || $this->uri->segment(2) == 'overview2' || $this->uri->segment(2) == 'overview3') { ?>
			method = 'beta_color_upadate';  
		<?php } ?>
		
		var refreshLog = 0;	
		var text = '<p class="card-text" ></p>';
		

		socket.on(method,function(data){  

			//console.log(data);

			if(factoryId == data.factoryId ) { 
				refreshLog = 1; 

				var detectionMachineName = "'"+$("#detectionMachineName"+data.machineId).val().replace("'","+")+"'";
				$("#card-light"+data.machineId).show();
				if (data.stateVal == "Running") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.runningImage); ?>">');
				}else if (data.stateVal == "Waiting") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.waitingImage); ?>">');
				}else if (data.stateVal == "Stopped") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.stopImage); ?>">');
				}else if (data.stateVal == "Off") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.offImage); ?>">');
				}else if (data.stateVal == "nodet") 
				{

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 

					$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	

					$(".statusMessage"+data.machineId).removeClass("m-t-10");
					$(".statusMessage"+data.machineId).html('<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?> <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
				}
				
				if(data.color == 'NoDataStacklight') {
					if($("#cardContent"+data.machineId).hasClass('card-inactive') == false) {
						$("#cardContent"+data.machineId).addClass('card-inactive');
					}
					var reasonText = '<?php echo Nodetection; ?>.';
					$("#inactiveText"+data.machineId).html(reasonText+' <?php echo Checkyourphoneorcontactinfonytttechcom; ?>'); 

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 

					$(".statusMessage"+data.machineId).css("color", "#FF8000");
					$(".statusMessage"+data.machineId).removeClass("m-t-10");
					$(".statusMessage"+data.machineId).html('<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?> <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
					$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
				} else {
					if($("#cardContent"+data.machineId).hasClass('card-inactive') == true) {
						$("#cardContent"+data.machineId).removeClass('card-inactive');
					}
					$("#inactiveText"+data.machineId).html(''); 
				}
				if(data.redStatus == '1') {
					$("#show_colorRO"+data.machineId).addClass("rounded");
					$("#show_colorRO"+data.machineId).css("background-color", "#F60100");
					$("#show_colorRO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorRO"+data.machineId).css("box-shadow", "none");
					$("#show_colorR"+data.machineId).css("opacity", "1");
				} else 
				{
					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorR"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.yellowStatus == '1') {
					refreshLog = 0;
					$("#show_colorYO"+data.machineId).addClass("rounded");
					$("#show_colorYO"+data.machineId).css("background-color", "#FFCF00");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");
					$("#show_colorY"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorY"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.greenStatus == '1') 
				{
					$("#show_colorGO"+data.machineId).addClass("rounded");
					$("#show_colorGO"+data.machineId).css("background-color", "#76BA1B");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorGO"+data.machineId).css("box-shadow", "none");
					$("#show_colorG"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorG"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.whiteStatus == '1') 
				{
					$("#show_colorWO"+data.machineId).addClass("rounded");
					$("#show_colorWO"+data.machineId).css("background-color", "#fff");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorWO"+data.machineId).css("box-shadow", "none");
					$("#show_colorW"+data.machineId).css("opacity", "1"); 
					
				} else 
				{
					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorW"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.blueStatus == '1') 
				{
					$("#show_colorBO"+data.machineId).addClass("rounded");
					$("#show_colorBO"+data.machineId).css("background-color", "#124D8D");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorBO"+data.machineId).css("box-shadow", "none");
					$("#show_colorB"+data.machineId).css("opacity", "1"); 
					
				} else 
				{
					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorB"+data.machineId).css("opacity", "0.4"); 
				}

				if(data.redStatus == '0' && data.yellowStatus == '0' && data.greenStatus == '0' && data.whiteStatus == '0' && data.blueStatus == '0' ) 
				{
					$("#show_colorR"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorY"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorG"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorW"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorB"+data.machineId).css("opacity", "0.4"); 

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 
					$(".statusMessage"+data.machineId).css("color", "#FF8000");

					if (data.stateVal == "Off" && data.color != 'NoDataStacklight') 
					{
						$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.offImage); ?>">');

						$(".statusMessage"+data.machineId).css("color", "#FF8000") 
						$(".statusMessage"+data.machineId).addClass("m-t-10");
						$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
					}else
					{
						if (data.stateVal == "Running" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.runningImage); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
						}else if (data.stateVal == "Waiting" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.waitingImage); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');

						}else if (data.stateVal == "Stopped" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/'.stopImage); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');

						}else
						{
							$(".statusMessage"+data.machineId).removeClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?> <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
							$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
						}
					}
						
				} 
				else
				{	
					if (data.stateVal == "nodet") 
					{
						$(".statusMessage"+data.machineId).removeClass("m-t-10");
						$(".statusMessage"+data.machineId).html('<?php echo NodetectionCheckyourphoneorcontactinfonytttechcom; ?> <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
						$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
					}else
					{
						$(".statusMessage"+data.machineId).css("color", "#FF8000") 
						$(".statusMessage"+data.machineId).addClass("m-t-10");
						$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title=""><?php echo Checklivestatus; ?></a>');
					}
				}
				
				text = '<p class="card-text" >'+data.stateVal+'</p>'; 

				$("#liveDetectionState"+data.machineId).val(data.stateVal);
				
				$("#notifyText"+data.machineId).html(text);
				
				$(".graphBlockOuter"+data.machineId).first().remove();  
				$("#card-text-graph"+data.machineId).append("<div class='graphBlockOuter graphBlockOuter"+data.machineId+"' ><span class='graphBlock' style='background-color:#"+data.color1+"' ></span><span class='graphBlock' style='background-color:#"+data.color2+"' ></span></div>")
			} 
        });

	

	

		socket.on('Disconnect SetAppAgv', function(reason, factoryIdSocket, machineId){ 
			
			if(factoryId == factoryIdSocket) {
				
				$("#AgvColor"+machineId).removeClass("NoAgvFind");
				$("#AgvColor"+machineId).removeClass("AgvFind");
				$("#AgvColor"+machineId).addClass("NoAgvDetection");

				var reasonText;
				if(reason == 'client namespace disconnect' || reason == 'transport close') { 
					reasonText = '<?php echo SetAppturnedoff; ?>.';
				} else if(reason == 'ping timeout') {
					reasonText = '<?php echo Nointernetorphoneturnedoff; ?>.';
				} 

				/*if (factoryId == 6) 
				{

					$.ajax({
				        url: "<?php echo base_url("admin/update_overview_data") ?>",
				        type: "post",
				        data: {machineId:machineId} ,
				        success: function (response) {
				        	

				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				           console.log(textStatus, errorThrown);
				        }
				    });
				}*/

				$("#agv"+machineId).html("<p style='color: black;font-size: 18px;'>"+reasonText+" <?php echo
					Checkyourphoneorcontactinfonytttechcom; ?></p>"); 

			}
		});

        socket.on("agv_color_upadate",function(data){  
				
			if(factoryId == data.factoryId ) { 
				refreshLog = 1; 
				var color = data.color;
				var phoneId = data.machineId;
				var lable = data.lable;

				if (color == "NoData") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvFind");
					$("#AgvColor"+phoneId).removeClass("AgvFind");
					$("#AgvColor"+phoneId).addClass("NoAgvDetection");
					$("#agv"+phoneId).html("<p style='color: black;font-size: 18px;'><?php echo NodetectionPleasecheckyourphoneorcontactinfonytttechcom; ?></p>");
				}

				if (color == "NoAGV") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvDetection");
					$("#AgvColor"+phoneId).removeClass("AgvFind");
					$("#AgvColor"+phoneId).addClass("NoAgvFind");
					$("#agv"+phoneId).html("<h2 style='color: white'>No AGVs</h2>");
				}

				if (color != "NoData" && color != "NoAGV") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvDetection");
					$("#AgvColor"+phoneId).removeClass("NoAgvFind");
					$("#AgvColor"+phoneId).addClass("AgvFind");
					$("#agv"+phoneId).html("<h2 style='color: white'>"+lable+"</h2>");
				}
			
			} 
        });
		
		
		
		$(document).ready(function() 
		{

			$('form#add_parts_form').submit(function(e) {
				
				e.preventDefault();
				
				
 				var form = $(this);

 				var inputData = form.MytoJson();
 				$("#add_parts_submit").attr('disabled',true);
				$("#add_parts_submit").html('<?php echo Saving; ?>...')
				
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/add_save_parts'); ?>",
					data: inputData,  
					dataType: "html",
					success: function(data)
					{	  
						var obj = $.parseJSON(data);
			        	if (obj.status == 1) 
			        	{
							$.gritter.add({
								title: '<?php echo Success; ?>',
								text: obj.message
							});

							$("#dataAddedText").html(obj.message);	
							$(".finalData").hide();	
				            $(".tableList").hide();	
				            $(".fileError").hide();	
				            $(".fileDesign").hide();
							$(".dataAdded").show()

							window.setTimeout(function(){location.reload()},3000)
						}else
						{
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: obj.message
							});
							$("#add_parts_submit").attr('disabled',false);
							$("#add_parts_submit").html('<?php echo confirm; ?>')
						} 
						
					},
					error: function() { 
						$("#add_parts_submit").attr('disabled',false);
						$("#add_parts_submit").html('<?php echo confirm; ?>')
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding data.'
						});
						 
					}
			   });
				
			});

			$('form#update_parts_form').submit(function(e) {
				
				e.preventDefault();
				
				
 				var form = $(this);

 				var inputData = form.MytoJson();
 				$("#edit_parts_submit").attr('disabled',true);
				$("#edit_parts_submit").html('<?php echo Saving; ?>...')
				
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/edit_save_parts'); ?>",
					data: inputData,  
					dataType: "html",
					success: function(data)
					{	  
						var obj = $.parseJSON(data);
			        	if (obj.status == 1) 
			        	{
							$.gritter.add({
								title: '<?php echo Success; ?>',
								text: obj.message
							});

							window.setTimeout(function(){location.reload()}) 
						}else
						{
							$("#edit_parts_submit").attr('disabled',false);
							$("#edit_parts_submit").html('<?php echo confirm; ?>')
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: obj.message
							});
						} 
						
					},
					error: function() { 
						$("#edit_parts_submit").attr('disabled',false);
						$("#edit_parts_submit").html('<?php echo confirm; ?>')
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding data.'
						});
						 
					}
			   });
				
			});
			
			
			
			
			var minimum = 2; 
			
			$('form#add_machine_form').submit(function(e) {
				
				e.preventDefault();
				var submit = 0;
				var running = $.trim($('#running').val());
				var waiting = $.trim($('#waiting').val());
				var stopped = $.trim($('#stopped').val());
				var off = $.trim($('#off').val());

				

				var isDataGet = $('#isDataGetValue').val();
				console.log(isDataGet);
				if(isDataGet != "0")
				{
				    var noStacklight = true;
				}else
				{
					var noStacklight = false;
				}


				if ((running  === '' || waiting  === '' || stopped  === '' || off  === '') && noStacklight == false) {
				 	var emptyArr = []; 
				 	if(running  === '') { 
				 		emptyArr.push('running') 
				 	}
				 	if(waiting  === '') { 
				 		emptyArr.push('waiting')
				 	}
				 	if(stopped  === '') { 
				 		emptyArr.push('stopped')
				 	}
				 	if(off  === '') { 
				 		emptyArr.push('off') 
				 	}
					emptyArr[0] = emptyArr[0].charAt(0).toUpperCase() + emptyArr[0].slice(1); 
					
				 	var emptyText = emptyArr.join();
				 	
				 	if(emptyText.length > 0 ) { 
					 	if(emptyArr.length == 1) {
					 		emptyText += ' is ';
					 	} else {
							emptyText += ' are ';
					 	}
					 	emptyText += 'not assigned, are you sure you want to continue?';
					} 
					if(confirm(emptyText)){
						submit = 1;
					}
					
				} else {
					submit = 1;
				}
				
				if(submit == 1) {
 					var form = $(this);
						$("#add_machine_submit").html('<?php echo Saving; ?>...')
						var machineLightNewArr = [];
						$('#colorLightSelect [id^="data_image_remove_"]').each(function(){
							var getId = $(this).attr('id').replace('data_image_remove_', '')
							machineLightNewArr.push(getId); 
						});
						var machineLightNew = machineLightNewArr.join();
						
						var inputData = form.MytoJson();
						inputData['machineLightNew'] = machineLightNew; 
						
						$.ajax({
							type: "POST",
							url: "<?php echo site_url('admin/add_machine'); ?>",
							data: inputData,  
							dataType: "html",
							success: function(data){
								  
									if($.trim(data) == "1") {  
										$.gritter.add({
											title: '<?php echo Success; ?>',
											text: '<?php echo Machineaddedsuccessfully; ?>'
										});
										$("#add_machine_submit").html('<?php echo Saving; ?>...')
										
										$("#refreshDiv").load(location.href + " #refreshDiv");
										form.each(function(){
											this.reset();
										}); 

										$('select').trigger('change');
										
										$("#modal-add-machine").modal('hide').fadeOut(1500); 
										$("#add_machine_submit").html('<i class="fa fa-plus" style="font-size: 20px;"></i> <?php echo Addmachine; ?>')

										window.setTimeout(function(){location.reload()}) 
										
									} else { 
										
										$.gritter.add({
											title: '<?php echo Error; ?>',
											text: data
										});
										$("#add_machine_submit").html('<?php echo Save; ?>')
									}
								
								
							},
							error: function() { 
								alert("Error while adding machine.");
								$.gritter.add({
									title: '<?php echo Error; ?>',
									text: 'Error while adding machine.'
								});
								 
							}
					   });
				}
			});

			$("form[id^='update_stacklight_configuration']").submit(function(e) {
			
				var form = $(this);
				e.preventDefault();
				var submit = 0;
				var machineId = form.find('input[name="machineId"]').val();
				var running = $.trim($('#running'+machineId).val());
				var waiting = $.trim($('#waiting'+machineId).val());
				var stopped = $.trim($('#stopped'+machineId).val());
				var off = $.trim($('#off'+machineId).val());
				if (running  === '' || waiting  === '' || stopped  === '' || off  === '') {
				 	var emptyArr = []; 
				 	if(running  === '') { 
				 		emptyArr.push('running') 
				 	}
				 	if(waiting  === '') { 
				 		emptyArr.push('waiting')
				 	}
				 	if(stopped  === '') { 
				 		emptyArr.push('stopped')
				 	}
				 	if(off  === '') { 
				 		emptyArr.push('off') 
				 	}
					emptyArr[0] = emptyArr[0].charAt(0).toUpperCase() + emptyArr[0].slice(1); 
					
				 	var emptyText = emptyArr.join();
				 	
				 	if(emptyText.length > 0 ) { 
					 	if(emptyArr.length == 1) {
					 		emptyText += ' is ';
					 	} else {
							emptyText += ' are ';
					 	}
					 	emptyText += 'not assigned, are you sure you want to continue?';
					} 
					if(confirm(emptyText)){
						submit = 1;
					}
				} else {
					submit = 1;
				}
				if(submit == 1) { 
					$(".add_stacklight_configuration_submit").html('<?php echo Saving; ?>...')
					
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('admin/update_stacklight_configuration'); ?>",
						data: form.MytoJson(),  
						dataType: "html",
						success: function(data){
							
								if($.trim(data) == "1") {  
									$.gritter.add({
										title: '<?php echo Success; ?>',
										text: '<?php echo Stacklightconfigurationupdatedsuccessfully; ?>'
									});
									
									$(".add_stacklight_configuration_submit").html('<?php echo Saved; ?>...')
								
									location.reload(1000);
									
								} else { 
									
									$.gritter.add({
										title: '<?php echo Error; ?>',
										text: data
									});
									$(".add_stacklight_configuration_submit").html('<?php echo Save; ?>')
								}
							
							
						},
						error: function() { 
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: 'Error while updating stacklight configuration.'
							});
						}
				   });
				}
				
			});
			
			$("form[id^='edit_machine_form']").submit(function(e) {  
				
				var form = $(this);
				e.preventDefault();
				var formId = form.attr('id');
				var machineId = formId.replace('edit_machine_form',''); 
				
				$("[id^='edit_machine_submit"+machineId+"']").html('<?php echo Saving; ?>...')
				
				var err_div = "[id^='edit_machine_error"+machineId+"']";
				var suc_div = "[id^='edit_machine_success"+machineId+"']";
				var modal_div = "[id^='modal-edit-machine"+machineId+"']";
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/edit_machine'); ?>", 
					data: form.serialize(), 
					dataType: "html",
					success: function(data){
						
							if($.trim(data) == "Machine updated successfully") { 
								$.gritter.add({
									title: '<?php echo Success; ?>',
									text: '<?php echo Machineupdatedsuccessfully; ?>'
								});
								$("[id^='edit_machine_submit"+machineId+"']").html('<?php echo Saved; ?>...')
								
								$(modal_div).modal('hide').fadeOut(1500);
								$("[id^='edit_machine_submit"+machineId+"']").html('<?php echo Save; ?>')
								window.setTimeout(function(){location.reload()},3000)
							} else { 
								$.gritter.add({
									title: '<?php echo Error; ?>',
									text: data
								});
								$("[id^='edit_machine_submit"+machineId+"']").html('<?php echo Save; ?>')
							}
						
						
					},
					error: function() { 
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: '<?php echo Errorwhileupdatingmachine; ?>.'
						});
						location.reload();
					}
			   });
			
			});
			
			$("form[id^='delete_machine_form']").submit(function(e) {

			var form = $(this);
			e.preventDefault();
			var formId = form.attr('id');
			var machineId = formId.replace('delete_machine_form',''); 
			var answer = $("#checkKeywordMachine"+machineId).val();


			 console.log(answer);
            if (answer != 'DELETE') 
            {
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Machineisnotdeleted; ?>"
                })
                return false;
            } 
            else 
            {  
				
					
					
					$("[id^='delete_machine_submit"+machineId+"']").html('<?php echo deleteing; ?>...')
					
					var suc_div = "[id^='delete_machine_success"+machineId+"']";
					var modal_div = "[id^='modal-delete-machine"+machineId+"']";
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('admin/delete_machine'); ?>", 
						data: form.serialize(), 
						dataType: "html",
						success: function(data){
							
								if($.trim(data) == "Machine deleted successfully") { 
								    $.gritter.add({
										title: '<?php echo Success; ?>',
										text: '<?php echo Machinedeletedsuccessfully; ?>'
									});
									$("[id^='delete_machine_submit"+machineId+"']").html('<?php echo Deleted; ?>...')
									 
									$(modal_div).modal('hide').fadeOut(1500);
									$("[id^='delete_machine_submit"+machineId+"']").html('<?php echo Delete; ?>')
									window.setTimeout(function(){location.reload()},3000)
								} 
							
						},
						error: function() { 
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: '<?php echo Errorwhiledeletingmachine; ?>.'
							});
							location.reload();
						}
				   });
				}
			});
			
			$("form[id^='unlock_machine_form']").submit(function(e) {  
				
				var form = $(this);
				e.preventDefault();
				var formId = form.attr('id');
				var machineId = formId.replace('unlock_machine_form',''); 
				
				var modal_div = "[id^='modal-unlock-machine"+machineId+"']";
				
				$("[id^='unlock_machine_submit"+machineId+"']").html('<?php echo Unlocking; ?>...')
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/unlock_machine'); ?>", 
					data: form.serialize(), 
					dataType: "html",
					success: function(data){
						
							$.gritter.add({
								title: '<?php echo Success; ?>',
								text: '<?php echo Machineunlockedsuccessfully; ?>'
							});
							$("[id^='unlock_machine_submit"+machineId+"']").html('<?php echo Unlocked; ?>...')
							
							$(modal_div).modal('hide').fadeOut(1500);
							$("[id^='unlock_machine_submit"+machineId+"']").html('<?php echo Unlock; ?>')
							window.setTimeout(function(){location.reload()},3000)
						
					},
					error: function() { 
						alert("Error while unlocking machine.");
						location.reload();
					}
			   });
			});
        
			
			$("#machineAutomation").change(function(){
				var selVal = $( "#machineAutomation option:selected" ).text();
				if ( selVal == "Automatic" || selVal == "Both" ) {
					$("#machineLoading").prop('required',true);
				} else {
					$("#machineLoading").prop('required',false);
				}
			}) 
			
			
			$("[id^='machineAutomationEdit']").change(function(){
				var machine_id = $(this).attr('id').replace('machineAutomationEdit',''); 
				var selVal = $( '#machineAutomationEdit'+machine_id+' option:selected' ).text(); 
				if ( selVal == "Automatic" || selVal == "Both" ) { 
					$("#machineLoading"+machine_id).prop('required',true);
					} else {
					$("#machineLoading"+machine_id).prop('required',false);
					}
			}) 
			
			$("select.machineStateValue, select[name^='machineMTId'], select[name^='machineType'], select[name^='machineAutomation'], select[name^='machineLoading'], select[name^='repeat'], select[name^='machineId'], select[name^='weekStartDate'], select[name^='monthStartDate'], select[name^='yearStartDate'], select[name^='weekEndDate'], select[name^='yearEndDate'],select[name^='userRole'],select[name^='Weekly'],select[name^='showcalender'],select[name^='showcalenderyear'],select[name^='showtaskRepeat'],select[name^='SetAppIoData'],select[name^='Operators']").select2({ 
			  tags: false
			});

			$(".select2Select").select2({ 
			  tags: false
			});


			$("select.machineStateValue, select[name^='machineMTId']").on("select2:select", function (evt) {
			  var element = evt.params.data.element;
			  var $element = $(element);
			  
			  $element.detach();
			  $(this).append($element);
			  $(this).trigger("change");
			}); 
		
		$( "select[name^='machineLight']").imagepicker({
		  hide_select : true,
		  show_label  : false 
		}); 
		
		$( "select[name^='stackLightTypeId']").imagepicker({
		  hide_select : false,
		  show_label  : false 
		}); 
		
		}); 
		
</script>


	<script type="text/javascript">
		
		function restartSetApp(machineId)
        {
        	
        	$.ajax({
		        url: "<?php echo base_url("admin/setAppReStart") ?>",
		        type: "post",
		        data: {machineId:machineId} ,
		        success: function (response) 
		        {
		        	var obj = $.parseJSON(response);
		        	if (obj.status == 1) 
		        	{	

		        		dataInput ={"reason":"setApp restart","machineId": machineId,"color":"NoDataRestart","factoryId": "<?php echo $this->session->userdata('factoryId'); ?>"};
						var dataInput = JSON.stringify(dataInput);

						socket.emit('setapp end detection with reason', dataInput, (data) => {
						  //console.log(data);
						});

						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});	
		        	}
		        	else
		        	{
		        		$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
		        	}
			    },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
        	
        	
        }

        function startSetApp(machineId,machineStatus)
        {
        	if (machineStatus == "0") 
        	{
        		$("#machineSetAppNotStart").val(1);
        		$("#playImage"+machineId).attr("onclick","");
        		$("#playImage"+machineId).attr("src","<?php echo base_url('assets/overview_gifs/loader_gif.gif'); ?>");
	        	$.ajax({
			        url: "<?php echo base_url("admin/setAppStart") ?>",
			        type: "post",
			        data: {machineId:machineId} ,
			        success: function (response) 
			        {
			        	//	console.log(response);
			        	var obj = $.parseJSON(response);
			        	if (obj.status == 1) 
			        	{
			        		$("#playImage"+machineId).css("width","110px");
			        		$("#playImage"+machineId).css("margin-left","20px");
			        		$(".statusMessage"+machineId).html(obj.message);
			        		
							/*$.gritter.add({
								title: 'Success',
								text: obj.message
							});	*/
			        	}
			        	else
			        	{
			        		$("#playImage"+machineId).attr("onclick","startSetApp("+machineId+",0)");
			        		$("#playImage"+machineId).attr("src","<?php echo base_url('assets/overview_gifs/play.png'); ?>");
			        		$.gritter.add({
								title: '<?php echo Error; ?>',
								text: obj.message
							});
			        	}
				    },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
        	}else
        	{
        		if (machineStatus == "2") 
        		{
        			$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo machine_is_in_no_producation_state_you_can_t_start_setapp_right_now; ?>"
					});	
        		}else
        		{
        			$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo MachineisinSetupstateYoucantstartsetapprightnow; ?>."
					});
        		}
        		
        	}
        	
        }

        $('form#dashboardCaptureCamera_form').submit(function(e) {  
            var form = $(this);
			e.preventDefault();
			$("#dashboardCaptureCamera_submit").html('<?php echo Reportwrongdetection; ?>...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('admin/dashboardCaptureCamera'); ?>",
				data: form.MytoJson(),  
				dataType: "html",
				success: function(data){
				
						if($.trim(data) == "1") {  
							$("#dashboardCaptureCamera_submit").html('<?php echo Reportwrongdetection; ?>')
							$.gritter.add({
								title: '<?php echo Success; ?>',
								text: "<?php echo Detectionissuemailsendsuccessfully; ?>."
							});
							$("#liveDetectonImage").modal('toggle');;
						} 
						else 
						{ 
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: data
							});
							$("#dashboardCaptureCamera_submit").html('<?php echo Reportwrongdetection; ?>')
						}
					
				},
				error: function() { 
					$.gritter.add({
							title: '<?php echo Error; ?>',
							text: '<?php echo Errorwhilesavingconfiguration; ?>..'
						});
					$("#dashboardCaptureCamera_submit").html('<?php echo Reportwrongdetection; ?>')
				}
		   });
        }); 

        



	</script>
