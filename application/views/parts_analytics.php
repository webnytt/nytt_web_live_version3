<link rel="stylesheet" href="<?php echo base_url('assets/scroller_timepicker/picker.css'); ?>">
<link href="<?php echo base_url('assets/css/dashboard5.css');?>" rel="stylesheet" /> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  

<style>
    .btn-primary.active, .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .btn-primary:focus, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .open > .dropdown-toggle.btn-primary, .open > .dropdown-toggle.btn-primary:focus, .open > .dropdown-toggle.btn-primary:hover, .show > .btn-primary.dropdown-toggle
{
    background-color: red !important;
    border-color: red !important;
}
@media screen and (min-device-width: 320px) and (max-width: 768px) 
{
    .margintopSmall
    {
        margin-top: 0px!important;
    }
}
@media screen and (min-device-width: 768px) and (max-width: 999px) 
{
    .minwidthcolmdStyle
    {
        min-width: 50%!important;   
    }
    .minwidthcolmdStylee
    {
        min-width: 30%!important;   
    }
    .minwidthcolmdStyleeshowby
    {
        min-width: 15%!important;
        margin-top: 10px;   
    }
    .minwidthCOLMDTODAY
    {
        min-width: 45%!important;
        margin-top: 10px;
    }
    .minwidthshiftsStyle
    {
        min-width: 12%!important;
        margin-top: 10px;
    }
    .machineviewShiftsStyle
    {
        min-width: 30%!important;
        margin-top: 10px;
    }
    .machineWeeklyStylees
    {
        min-width: 30%!important;
        margin-top: 10px;
    } 

    .machineYearlyStylees
    {
        min-width: 30%!important;
        margin-top: 10px;
    }
}
       @media screen and (min-device-width: 768px) and (max-width: 999px) 
        {
            .colmd4chartsstyle
            {
                min-width: 100%!important;
            }
        }
        @media screen and (min-device-width: 1000px) and (max-width: 1320px) 
        {
            .colmd4chartsstyle
            {
                min-width: 50%!important;
            }
        }

[data-tooltip] {
  position: relative;
  z-index: 2;
  cursor: pointer;
}

/* Hide the tooltip content by default */
[data-tooltip]:before,
[data-tooltip]:after {
  visibility: hidden;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=0)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=0);
  opacity: 0;
  pointer-events: none;
}

/* Position tooltip above the element */
[data-tooltip]:before {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-bottom: 5px;
  margin-left: -80px;
  padding: 7px;
  width: 160px;
  -webkit-border-radius: 3px;
  -moz-border-radius: 3px;
  border-radius: 3px;
  background-color: #000;
  background-color: hsla(0, 0%, 20%, 0.9);
  color: #fff;
  content: attr(data-tooltip);
  text-align: center;
  font-size: 14px;
  line-height: 1.2;
}

/* Triangle hack to make tooltip look like a speech bubble */
[data-tooltip]:after {
  position: absolute;
  bottom: 150%;
  left: 50%;
  margin-left: -5px;
  width: 0;
  border-top: 5px solid #000;
  border-top: 5px solid hsla(0, 0%, 20%, 0.9);
  border-right: 5px solid transparent;
  border-left: 5px solid transparent;
  content: " ";
  font-size: 0;
  line-height: 0;
}

/* Show tooltip content on hover */
[data-tooltip]:hover:before,
[data-tooltip]:hover:after {
  visibility: visible;
  -ms-filter: "progid:DXImageTransform.Microsoft.Alpha(Opacity=100)";
  filter: progid: DXImageTransform.Microsoft.Alpha(Opacity=100);
  opacity: 1;
}
.table td, .table th
{
    border-top: 0px!important;
}
.slick-prev
{
    box-shadow: grey 0 0 8px -2px!important;
}
.slick-next
{
    box-shadow: grey 0 0 8px -2px!important;
}

.select2-container
{
    /*display: block!important;*/
    height: 34px;
}
.select2-container--default .select2-selection--single
{
    border: 0px;
    border: none;
    box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;
    border-radius: 5px;
}
@media screen and (min-device-width: 320px) and (max-width: 374px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
} 
@media screen and (min-device-width: 375px) and (max-width: 424px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
}
@media screen and (min-device-width: 425px) and (max-width: 767px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 100%;
    }  
}
@media screen and (min-device-width: 768px) and (max-width: 1023px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container
    {
        min-width: 175px!important;
    }
    .oppgapstyle
    {
        min-width: 130px!important;
    }

}
@media screen and (min-device-width: 1320px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
    }  
    /*.select2-container
    {
        min-width: 175px!important;
    }*/
} 
table.scroll tbody {
    height: 100px;
    overflow-y: auto;
    overflow-x: hidden;
}

.SmallStyle {
    font-size: 13px!important;
    padding-left: 2px;
}

</style>

<div id="content" class="content">
        
        <h1 class="page-header">
        <form action="<?php echo base_url('Analytics/dashboard'); ?> "method="post">  

                        <input type="hidden" name="mainChoose1" id="mainChoose1"> 
                        <input type="hidden" name="mainChoose2" id="mainChoose2"> 
                        <input type="hidden" name="machineId" id="mainMachineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $listMachinesStop[0]->machineId; } ?>"> 
                        <button style="border: none;background: none;padding-right: 3px;margin-top: -1px;cursor: pointer;font-size: 22px !important;color: #002060!important;line-height: 0 !important;margin-left: -5px;font-weight: 600;" type="submit"><?php echo Analytics ;?>   <span class="SmallStyle" style="pointer-events: none !important;color:black;"> &gt; <?php echo Partsanalytics; ?></span></button>
                    </form>
                    
        </h1>
                      
       <!--  <h1 style="" class="page-header h1headerpagestyle"><?php echo Analytics; ?> <span style="font-weight: 700;font-size: 14px;"> > </span><small style="color:black;"><?php echo Partsanalytics; ?></small></h1> -->
    <br />

    <div class="row">
        <div class="col-md-3 minwidthcolmdStyle" style="display:flex;font-size: 16px;padding-left: 14px;margin-top: -20px;">
           <small class="filterSelected" style="font-weight: 600;color:black;">06/05/2021</small>
        </div>

        <div class="col-md-1 show_by minwidthcolmdStyleeshowby" style="padding-top:10px;">
            <label style="color:black;"><?php echo Show; ?>: </label>
        </div>
        <div class="col-md-2 minwidthcolmdStylee" style="padding-left: 5px;padding-right: 0px;">
            <select name="showcalender" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                <option class="new_test" <?php echo ($_POST['partsChoose1'] == "day") ? "selected" : ""; ?> value="day"><?php echo Day; ?></option>
                <option class="new_test" <?php echo ($_POST['partsChoose1'] == "weekly") ? "selected" : ""; ?> value="weekly"><?php echo week; ?></option>
                <option class="new_test" <?php echo ($_POST['partsChoose1'] == "monthly") ? "selected" : ""; ?> value="monthly"><span> <?php echo MONTH; ?> </span></option>
                <option class="new_test" <?php echo ($_POST['partsChoose1'] == "yearly") ? "selected" : ""; ?> value="yearly"><span> <?php echo year; ?> </span></option>
            </select>
        </div>

        <div class="col-md-2 day colmd2day minwidthCOLMDTODAY yearwidthstyle" style="padding-left: 10px!important;max-width: 23% !important;flex: 0 0 23% !important;">
            <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" value="<?php if($_POST['partsChoose1'] == "day") { echo $_POST['partsChoose2']; }else{  echo date('m/d/Y'); } ?>" style="width:70%!important;margin-right:0%!important;height: 34px!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;float: left!important;">
        </div>

        <?php 

        if($_POST['partsChoose1'] == "weekly") {
            $partsChoose2  = explode("/", $_POST['partsChoose2']);
              $weekPost = $partsChoose2[0];
            }else
            {
                $weekPost = "";
            }
         ?>

         <div class="col-md-2 weekly machineWeeklyStylees yearwidthstyle" style="display: none;max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="Weekly" style="display: block!important;min-width:200px!important;" class="form-control margintopstyle" id="choose3" onchange="changeFilterValue()">
            <?php for ($iS=1; $iS < 53; $iS++) {  ?>
                <option  <?php if($weekPost == $iS){ echo "selected"; }else if(date('W') == $iS && empty($weekPost)) { echo "selected"; } ?> value="<?php echo $iS; ?>"><?php echo week.' '.$iS; ?></option>
            <?php } ?>
            </select>
        </div>

        <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
        
        <div class="col-md-2 monthly colmdmonthlyStyle" style="display: none;padding-left: 10px!important;">
            <input onchange="changeFilterValue()" type="text" style="height:34px!important;width:80%!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;float: left!important;" class="form-control datepicker1 yearwidthstyle" id="choose4" value="<?php if($_POST['partsChoose1'] == "monthly") { echo $_POST['partsChoose2']; }else{ echo date('M Y'); } ?>">
        </div>

        <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
        <div class="col-md-2 yearly machineYearlyStylees yearwidthstyle" style="display: none; max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="showcalenderyear" class="form-control" style="border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" id="choose5" onchange="changeFilterValue()">
                <?php for($i=0 ; $i < $years; $i++){ 
                    
                        $yearValue = 2018 + $i;
                   
                    ?>
                    <option <?php if($_POST['partsChoose2'] == $yearValue && $_POST['partsChoose1'] == "yearly") { echo "selected"; } else if ($yearValue == date('Y') && $_POST['partsChoose1'] != "yearly") {
                                echo "selected";
                            } ?>  value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                <?php } ?>
            </select>
        </div>
        
        <div class="machineView minwidthshiftsStyle"><span class="viewddLabelstyle" style="color:black;"><?php if ($accessPointViewView == true) { ?> <?php echo shifts; ?> : <?php } ?> </span></div>

        <div class="col-md-2 machineView machineviewShiftsStyle">
            <input type="hidden" id="viewId" value="none">
            <?php if ($accessPointViewView == true) { ?>
                <div class="dropdown viewdropdownstyle" id="viewFilter">
                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <span style="padding-left: 8px;color:black;" id="selectedView">24 <?php echo Hours; ?> </span>
                    </div>
                    <div class="dropdown-menu dropdownMenustyle">
                        <div class="dropdown-item selectedView" id="viewIdnone" onclick="selectView('none','24 <?php echo Hours; ?>')" style="font-weight: 600;">
                            24 <?php echo Hours; ?> 
                        <hr class="dropdownHRstyle">
                        </div>

                        <?php foreach ($machineView as $key => $value) { ?>
                            <div class="dropdown-item" id="viewId<?php echo $value['viewId']; ?>" style="font-weight: 600;">
                                <span class="machineNameText" style="width:80%;" onclick="selectView(<?php echo $value['viewId']; ?>,'<?php echo $value['viewName']; ?>')">
                                    <?php echo $value['viewName']; ?>
                                </span> 
                                <?php if ($accessPointViewsEdit == true) { ?>
                                    <span style="float: right;">
                                        <img onclick="editView(<?php echo $value['viewId']; ?>)" style="width: 20px;" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                    </span> 
                                <?php } ?>
                                <hr class="dropdownHRstyle">
                            </div>

                        <?php } ?>

                        <?php if ($accessPointViewsEdit == true) { ?>
                            <div class="dropdown-item dropdownItemstyle">
                                <a data-toggle="modal" data-target="#add-view-modal">+ <?php echo Addnewshift; ?></a> 
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php $machineNameList = $listMachines->result_array(); ?>

    <div class="row">
        <section class="regular slider">
            <?php foreach($machineNameList as $key => $machine) { 
                if($machine['machineName'] != "Parts_1035" && $machine['machineName'] != "Parts_1832" && $machine['machineName'] != "Parts_1821"){

                    if($machine['machineName'] == "Status_1035"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1035";
                    }else if ($machine['machineName'] == "Status_1832"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1832";
                    }else if ($machine['machineName'] == "Status_1821"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1821";
                    }else {
                        $ioMachineName = $machine['machineName'];
                    } ?>
            <?php if($machine['machineId'] == $_POST['machineId']){ ?>
            <div>
                <button type="button" class="btn btn-default btn-lg filter boxShadow machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }  if(!$_POST && $key == 0) { echo "active";} ?>" id="machine<?php echo $machine['machineId']; ?>"><?php echo $ioMachineName; ?> 
                </button>
            </div>
            <?php } } } ?>
            <?php foreach($machineNameList as $key => $machine) { 
                 if($machine['machineName'] != "Parts_1035" && $machine['machineName'] != "Parts_1832" && $machine['machineName'] != "Parts_1821"){

                     if($machine['machineName'] == "Status_1035"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1035";
                    }else if ($machine['machineName'] == "Status_1832"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1832";
                    }else if ($machine['machineName'] == "Status_1821"  && $this->factoryId == 2) {
                        $ioMachineName = "IO- Robot 1821";
                    }else {
                        $ioMachineName = $machine['machineName'];
                    } ?>
            <?php if($machine['machineId'] != $_POST['machineId']){ ?>
            <div>
                <button type="button" class="btn btn-default btn-lg filter boxShadow machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }  if(!$_POST && $key == 0) { echo "active";} ?>" id="machine<?php echo $machine['machineId']; ?>"><?php echo $ioMachineName; ?> 
                </button>
            </div>
            <?php } } } ?>
        </section>
    </div>

    <input type="hidden" name="machineId" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $machineNameList[0]['machineId']; } ?>">
    
    <div class="row" id="chartwrap" class="machineModerowMainStyle">
        
        <div class="col-md-7">
            <div class="row">
                 <div class="col-md-12 colmd4chartsstyle colomd4machineModeStyle">
                    <div class="tab-content boxShadow">
                        <!-- <div class="tab-pane fade active show" > -->
                            <div class="row">

                                <div class="col-md-12"> 
                                    <div class="row">
                                        <div class="col-md-4">
                                            <h4 class="h4machineHeadcolor" style="padding-top:12px;"><?php echo Orders; ?> </h4>
                                        </div>
                                        <div class="col-md-4">
                                            <span style="font-size: 12px;color:#002060;float:right;line-height: 12px;padding-top: 7px;"><small><?php echo Goodpartsproduced; ?></small>
                                                <br>
                                                <small style="font-weight: 700;" class="filterSelected">25-01-2021</small></span>
                                            <span style="float: right;color: #76BA1B;font-weight: 600;float:right;font-size: 29px;" id="goodPartsCount"></span>
                                        </div>
                                         <div class="col-md-4">
                                            <span style="font-size: 12px;color:#002060;float: right;line-height: 12px;padding-top: 7px;"><small><?php echo Scrappartsproduced; ?></small>
                                                <br><small style="font-weight: 700;" class="filterSelected">25-01-2021</small></span>
                                            <span style="float: right;color: red; font-weight: 600;font-size: 29px;" id="scrapPartsCount"></span>
                                        </div>
                                    </div>
                                    <div class="panel-body" style="padding: 0px 0px 0px 5px !important;height: 156px;overflow: auto;width: 100%;">
                                        <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0"> 
                                            <thead>
                                                <tr>
                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:65px">
                                                    <span>&nbsp;<?php echo Partnr; ?> &nbsp;</span>
                                                  </th>
                                                  <?php  if ($this->session->userdata('isMonitor') == "1") { ?>
                                                      <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:65px">
                                                        <span style=" border-left: 2px solid #f1f3f5;"></span> 
                                                        <span>&nbsp;<?php echo Ordernr; ?> &nbsp;</span>
                                                      </th>
                                                  <?php } ?>
                                                      <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:55px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Name; ?></span>
                                                  </th>
                                                  
                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:70px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Operation; ?> </span>
                                                   
                                                  </th>

                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:75px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Starttime; ?> &nbsp;</span>
                                                    
                                                  </th>

                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:75px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Endtime; ?> &nbsp;</span>
                                                  </th>

                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:100px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Partsproduced; ?>  &nbsp;</span>
                                                   
                                                  </th>

                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:85px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span>&nbsp;<?php echo Scrapparts; ?>  &nbsp;</span>
                                                    
                                                  </th> 

                                                  <th style="padding: 0px 0px 5px 0px !important;border-bottom: none!important;min-width:65px">
                                                    <span style=" border-left: 2px solid #f1f3f5;"></span>
                                                    <span> &nbsp;<?php echo Status; ?>  &nbsp;</span>
                                                  </th>

                                                </tr>

                                            </thead>
                                            <tbody id="ordersData" style="background-color: #f2f2f2;height: 130px;">
                                                
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-md-6 colmd4chartsstyle colomd4machineModeStyle">
                    <div class="tab-content boxShadow" style="min-height: 309px !important;">
                        <div class="tab-pane fade active show" >
                            <div class="row m-l-0 m-r-0">

                                <div class="col-md-12">
                                    <center>
                                        <div id="DailyGraph6" style="height:235px;width: 100%;">  </div>
                                        <div class="col" style="font-weight: 700;margin-top: -60px;">
                                            <span id="dayThrouputActualText" style="font-size: 27px;">0 </span> 
                                            <span style="color:black;"><?php echo partshr; ?>,</span>
                                            <br>
                                            <small style="color:black;"><?php echo outof; ?> </small> 
                                            <small> 
                                                <span style="color:black;" id="dayThrouputMaxText"> 0 </span> 
                                                <span style="color:black;"><?php echo partshr; ?></span>
                                            </small>
                                        </div>
                                    </center>
                                </div>
                            </div>
                            <div id="gaugeText" style="text-align: center;color:black;margin-top: 15px;"></div>
                        </div>
                    </div>
                </div>

                <style type="text/css">



   svg {
    position: absolute;
    width: 300px;
    height: 300px;
    z-index: 1000px;
}
svg circle {
    width: 100%;
    height: 100%;
    fill: none;
    stroke: #d01c1c;
    stroke-width: 8;
    stroke-linecap: 8;
    transform: translate(5px, 5px);
    -webkit-transform: translate(5px, 5px);
}
svg circle:nth-child(2) {
    stroke-dasharray: 440;
    stroke-dashoffset: 440;
}
.overallStatisticscircle {
    width: 211px;
    display: inline-block;
    padding-top: 72px;
}
                    
                    .card1:nth-child(1) svg circle:nth-child(2)
                      {
                        stroke-dashoffset:<?php echo 440 - (630 * 75) / 100; ?>;
                          stroke-linecap: round;
                          stroke: #FF8000;
                      }

                      .overallStatisticscircle{width:211px;display:inline-block;padding-top:72px}
                </style>


                <div class="col-md-6 colmd4chartsstyle colomd4machineModeStyle">
                    <div class="tab-content boxShadow" style="min-height: 309px !important;">
                        <!-- <div class="tab-pane fade active show" > -->
                            <div class="row" id="productivityGraph">
                                <div id="DailyGraph8" style="height:270px;width: 100%;" >  </div>


                               
                            </div>
                            <div class="row" id="partsProducedGraph" style="display: none;">
                                    <div class="col" style="margin-top: 33px;">
                                      <div class="card1" >
                                        <center>
                                          <svg>
                                            <circle style="stroke:#FFFFFF"cx="100" cy="100" r="100"></circle>
                                            <circle cx="100" cy="100" r="100"></circle>
                                            <circle  style="stroke:#CCD2DF;" cx="100" cy="100" r="91"></circle>
                                            <div class="overallStatisticscircle">
                                              <h3 style="margin-bottom:0.0rem;color:#002060;" id="partCount">0</h3>
                                              <h4 style="color:#002060;" class="textChange"><?php echo Partsproduced; ?></h4>
                                            </div>
                                          </svg>
                                        </center>
                                      </div>
                                    </div>
                            </div>
                        <!-- </div> -->
                    </div>
                </div>

            </div>
        </div>

       <div class="col-md-5">
            <div class="tab-content boxShadow">
                <div class="tab-pane fade active show" >
                    <div class="row m-l-0 m-r-0">
                        <div class="col-md-4 oppgapstyle" style="padding-right: 0px;">
                            <h6><?php echo Opportunitygap; ?>
                                <i style="color:#ff8000;"class="fa fa-circle"></i>
                            </h6>
                        </div> 
                        <div class="col-md-4 oppgapstyle">
                            <h6>
                                <i style="color: #002060;" class="fa fa-circle"></i>  
                                <?php echo Partsestimated; ?>
                            </h6>
                        </div>
                        <div class="col-md-4 oppgapstyle">
                            <h6 style="float:right;color:black;font-weight: 600;">
                                <?php echo Last30Days; ?>
                            </h6>
                        </div>
                        <div class="col-md-12">
                           <div id="DailyGraph7" style="height:505px;width: 100%;" >  </div>
                           <div style="float: right!important;font-weight: 700!important;margin-top: -21px;"><?php echo Parts; ?></div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>


    <div class="graph-loader hide" ></div>
     
    <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<div class="modal fade" id="add-view-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Addnewshift; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                
                <form class="p-b-20 m-l-10 m-r-10" action="add_view" method="POST" id="add_view_form">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group p-5">
                                <input type="text" style="padding: 0px 12px 16px !important;" class="form-control border-left-right-top-hide h-29" id="viewName" name="viewName" placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-stop"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                          <hr style="height: 2px;margin-top: 1rem;">
                        </div>
                    </div>

                    <div class="addBreakData" style="margin-bottom: 10px;display: none;"></div>

                    <div class="row addBreakButton">
                        <div class="col-md-12">
                          <span onclick="addBreak()" style="color: #FF8000;cursor: pointer;">
                            <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="display: none;">
                        <div class="col-md-12">
                          <span onclick="removeBreak()" style="color: red;cursor: pointer;">
                            <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="margin-top: 25px;display: none;">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-break-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-break-stop"></div>
                        </div>
                    </div>
                    
                    <div class="row removeBreak m-l-0 m-r-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                            <center>
                                <a onclick="saveBreaks()" class="btn btn-primary m-r-5 savebreaksstyle" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                            </center>
                        </div>
                    </div>

                    <div class="row submitButton m-l-0 m-r-0">
                        <div class="col-md-12">
                            <br>
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5 AddbreakButtonstyle" id="add_view_submit" >
                                    <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Add; ?></button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
      </div>
    </div>
</div>

<?php foreach ($machineView as $key => $value) { ?>
<div class="modal fade" id="edit-view-modal-dynamic<?php echo $value['viewId']; ?>" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Editview; ?></h4>
                <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
            </div>

            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="edit_view_form" method="POST" id="edit_view_form<?php echo $value['viewId']; ?>">
                    <input type="hidden" name="viewId" id="viewIdEdit-dynamic<?php echo $value['viewId']; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group">
                              <input type="text" class="form-control border-left-right-top-hide" id="viewNameEdit-dynamic<?php echo $value['viewId']; ?>" name="viewName" 
                              placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-edit-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-edit-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr class="h-2 m-t-10"></div>
                    </div>

                    <div class="addEditBreakData-dynamic<?php echo $value['viewId']; ?> m-b-10" style="display: none;"></div>

                    <div class="row addEditBreakButton-dynamic<?php echo $value['viewId']; ?>">
                        <div class="col-md-12">
                            <span onclick="addEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksbuttonStyle">
                                <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?>" style="display: none;">
                        <div class="col-md-12">
                            <span onclick="removeEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksRenoStyle">
                                <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div style="display: none" class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-t-25">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-r-0 m-l-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <a onclick="saveEditBreaks(<?php echo $value['viewId']; ?>)" class="btn btn-primary m-r-5 updatesavebreakbutton" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                          </center>
                        </div>
                    </div>
                    
                    <div class="row submitEditButton-dynamic<?php echo $value['viewId']; ?> m-l-0 m-r-0">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <button type="submit"class="btn btn-primary m-r-5 savebuttonstylee" id="edit_view_submit<?php echo $value['viewId']; ?>" ><?php echo save; ?></button>
                            <a class="btn btn-primary m-r-5 deleteviewbuttonstylee" id="deleteView" >
                                <img width="38px"height="34px" src="<?php echo base_url('assets/nav_bar/delete_white.svg'); ?>"></a>
                          </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>



<div class="modal fade" id="delete-view-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content deleteModalContentStyle">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title h4machineHeadcolor p-5"><?php echo Delete; ?> </h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div> 
            <div class="modal-body">
                <form class="p-b-20" action="delete_view_form" method="POST" id="delete_view_form" >
                    <input type="hidden" name="deleteViewId" id="deleteViewId" /> 
                    <div class="modal-footer border-top-0 p-t-0">
                        <div class="row">
                            <div class="col-md-12 m-t-30">
                                <center>
                                    <img class="deleteGrayButtonStyle"src="<?php echo base_url().'assets/img/delete_gray.svg'?>">
                                </center>
                            </div>
                            <br>
                            <div id="" class="m-b-10 alert alert-success fade hide" ></div>
                            <div class="col-md-12" style="margin-top: -5px;">
                                <center><?php echo Areyousureyouwanttodeleteview; ?></center>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <br>
                                    <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_view_submit"><?php echo Delete; ?></button>
                                    <br>
                                </center>
                            </div>
                        </div>          
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="orderDetails" role="dialog">
    <div class="modal-dialog" style="min-width: 40%;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Orderdetails; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
            
                <div class="col-md-12">
                    <div class="row" style="padding-top:12px;">
                        <div class="col-md-1">
                            <img class="file-upload-image profileImage" id="orderUserImage" style="border-radius: 50%;border: 2px solid #002060;width: 40px;" src="https://nyttdev.com/testing_env/version4/assets/img/user/34.jpg" alt="administrator">
                        </div>
                        <div class="col-md-3" style="padding-left: 23px;line-height: 15px;">
                            <span style="font-size: 10px;color:grey;"><?php echo Addedby; ?></span><br>
                            <small style="font-weight: 700;font-size: 12px;color: black;" id="orderUserName">Eborny barotite</small>
                        </div>
                        <div class="col-md-2" style="min-width: 85px;line-height: 15px;">
                            <span style="font-size: 10px;color:grey;"><center><?php echo Createdon; ?></center></span>
                            <center><span style="font-weight: 700;color: black;" id="orderCreateDate">22-02-2021</span></center>

                        </div>

                        <div class="col-md-3" style="line-height: 15px;">
                            <span style="font-size: 10px;color:grey;"><center style="min-width:45px;"><?php echo Starttime; ?></center></span>
                            <center>
                            <span style="font-weight: 700;color: black;" id="orderStartDate">22-02-2021</span></center>

                        </div> 

                        <div class="col-md-3" style="line-height: 15px;">
                            <span style="font-size: 10px;color:grey;"><center><?php echo Endtime; ?></center></span>
                            <center><span style="font-weight: 700;color: black;" id="orderEndDate">22-02-2021</span></center>

                        </div>
                    </div>
                    <hr>

                    <div class="row" style="padding-top:12px;padding-bottom:15px;">
                        <div class="col-md-4" style="padding-left: 10px;line-height: 15px;">
                            <span style="font-size: 20px;font-weight: 700;" id="orderPartsName">Sleeve</span><br>
                            <small style="font-size: 12px;color:grey;" id="orderpartsNumber">100000</small> | <small style="font-size: 12px;color:grey;" id="orderManufacturingOrderId">100000</small> | <small style="font-size: 12px;color:grey;" id="orderpartsOperation">Milling</small>
                        </div>
                       
                        <div class="col-md-3" style="min-width: 82px;line-height: 16px;padding:0px;text-align: center;">
                            <span style="color: #76BA1B;font-size: 20px;font-weight: 700;" id="ordercorrectParts">218</span> <sapn style="font-size: 16px;">/</sapn> <span style="font-size: 16px;font-weight: 400;" id="orderTotalParts">226</span><br>
                            <span style="font-size:9px;color: grey;"><?php echo Goodpartsproduced; ?></span>

                        </div>
                       
                        <div class="col-md-2" style="line-height: 10px;">
                            <span style="font-size: 20px;font-weight: 700;color:red;"><center id="orderscrapParts">8</center></span><br>
                            <span style="font-size:9px;color: grey;"><center style="min-width: 45px;"><?php echo Scrapparts; ?></center></span>
                        </div>

                        <div class="col-md-3" style="min-width: 82px;line-height: 16px;padding-left:5px;text-align: center;">
                            <span style="color:#0056b3;font-size: 20px;font-weight: 700;" id="orderTimeTook">00:11:22</span><br>
                            <span style="font-size:9px;color: grey;"><?php echo Setapptimetaken;?></span>
                        </div>
                    </div>

                </div>    
            
            </div>
      </div>
    </div>
</div>