<link href="<?php echo base_url('assets/css/help_report.css');?>" rel="stylesheet" />
<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060!important;" class="page-header"><?php echo ContactNytt; ?></h1>
	<input type="hidden" name="problemId" value="0">
	<div class="row">
		<div class="col-md-12">
		  <div class="panel panel-inverse panel-primary boxShadow" >
				<div class="panel-body">
					<form class="p-b-20" action="add_helpReport_form" method="POST" id="add_helpReport_form" >
						<h6 style="color:#002060;"><?php echo Forquestionsrequestorothercommentspleasefillinyourconcernsintheboxbelow; ?></h6>
						<br>
						<div class="row">
							<div class="col-md-6 col-sm-12" style="flex: 0 0 47% !important;max-width: 47% !important;">
								<textarea placeholder="<?php echo Whatwentwrong; ?>" required maxlength="1000" onkeyup="checkwordcount();" style="border-left: none;border-right: none;border-top: none;border-bottom: 2px solid #F2F2F2; width: 100%;height: 80px;color: #124D8D;font-size: 90% !important;" name="problemText" id="description"></textarea>
								<small style="float: right;color: #d8cfcf;"><span id="descriptionLength"> 0 </span>/1000</small>
							</div>
						</div>

						<div class="row" style="margin-top: 25px;">
							<div class="col-md-12">
								<div class="upload-btn-wrapper">
									<button class="btn1 pointer" type="button">
										<img style="margin-top: -8px;height: 13px;" width="18" src="<?php echo base_url("assets/nav_bar/upload.svg"); ?>">
										<input id="i_file" type="file" name="image" />
									</button>
									<span id="disp_tmp_path" style="font-weight: 600;color: #002060;"><?php echo Uploadfileorimage; ?></span>
		  							
		  						</div>
							</div>
						</div>

						<div class="row" style="margin-top: 25px;margin-bottom: 30px;">
							<div class="col-md-12">
								<button id="add_helpReport_submit" style="padding-left: 47px;padding-right: 47px;padding-top: 7px;padding-bottom: 7px;border-radius: 7px;" type="submit" class="btn btn-sm btn-primary"><?php echo Submit; ?></button>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div> 
	</div>

	<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
			
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

