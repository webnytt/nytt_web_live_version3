<!DOCTYPE html>
<html lang="en">
<head>
  <title>Dashboard Instruction</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Dashboard Instruction</h2>
    <div class="form-group">
      <input type="text" class="form-control" onkeyup="searchInstruction(this.value)" id="searchKey" placeholder="Enter search keyword" name="search">
    </div>

    <div id="searchInstructionData">
    <?php foreach ($instruction as $key => $value) { ?>
      <div class="row" style="margin-top: 15px;">
        <div class="col-md-4"></div>
        <div class="col-md-4"><?php echo $value['main_title']; ?> <?php if(!empty($value['main_title_gif'])){ ?> <br> <img style="width: 420px;" src="<?php echo base_url('assets/instruction/').$value['main_title_gif']; ?>"> <?php } ?>
            <?php 
            $subInstruction = $this->AM->getWhereDB(array("instructionId" => $value['instructionId']),"subInstruction");
            foreach ($subInstruction as $keySub => $valueSub) { ?>
            <div class="row" style="margin-left: 10px;margin-top: 10px;">
                <div class="col-md-12">
                  <?php echo $valueSub['subTitle']; ?> <br> <img style="width: 420px;" src="<?php echo base_url('assets/instruction/').$valueSub['subTitleGif']; ?>">
                </div>
            </div>
          <?php } ?>
        </div>
        <div class="col-md-4"></div>
      </div>
    <?php } ?>
    </div>
</div>

</body>
</html>



<script type="text/javascript">

  function searchInstruction(value)
  {
     $.ajax({
        url: "<?php echo base_url("InstructionDetail/searchInstruction") ?>",
        type: "post",
        data: {searchKey:value,instruction_type:'1'} ,
        success: function (response) {
          $("#searchInstructionData").html(response);
        },
        error: function(jqXHR, textStatus, errorThrown) {
           console.log(textStatus, errorThrown);
        }
    });
  }
</script>
