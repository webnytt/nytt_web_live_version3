<link href="<?php echo base_url('assets/css/meeting_note_details.css');?>" rel="stylesheet" />
<div id="content" class="content">
  
  <h1 style="font-size: 22px;color: #002060;line-height: 22px;" class="page-header"><?php echo Meetingnotes; ?> - 
    <?php    if ($meetingNoteDetails->filterType == "1") 
      {
        echo Daily;
      }

      elseif ($meetingNoteDetails->filterType == "2") 

      {
        echo Weekly;
      }

      elseif ($meetingNoteDetails->filterType == "3") 

      {
        echo Monthly;
      

      }elseif ($meetingNoteDetails->filterType == "4") 

      {
        echo Yearly;
      } 
      ?>
  </h1>


  <small class="smallstyle">
    <a style="color: #002060 !important;" href="javascript:void(0)"> <?php echo Meetingnotes; ?> </a> &gt; <?php echo $meetingNoteDetails->noteName; ?>
  </small>
  <a class="btn btn-primary Downloadbuttonstyle" id="downloadbuttonstyle" href="#modal-download-meeting-notes" data-toggle="modal">
        <img class="DownloadbuttonimageStyle" src="<?php echo base_url('assets/nav_bar/download.png'); ?>"> <?php echo Download; ?></a>
  
  <div id="pdfContent">
    <input type="hidden" name="filterType" id="filterType" value="<?php echo $meetingNoteDetails->filterType; ?>">
      <div class="col-md-12" style="margin-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <?php foreach ($machines as $key => $value) { ?>
                  <button type="button" class="btn btn-primary machineNameDetailsstyle"><?php echo $value['machineName']; ?></button>
                <?php } ?>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
          <div class="col-md-6">
            <div class="maindivstyle">
              <h4 style="color:white;"><center> <?php echo Noofstoppages; ?></center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle startdatestyles">
                    <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                    ?>
                    </h3>
                  </center>
                </div>

                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle startdatestyles">
                      <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?>
                    </h3>
                  </center>
                </div>

                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary Noofstoppagesstartstyle"><h3  style="color:#002060;"><?php echo $NoOfStoppagesStart; ?></h3>
                    </div>
                  </center>
                </div>
                <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                  <?php if ($NoOfStoppagesStart > $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow NoofstoppagesMinusstyle">
                        <h3 class="NoofstoppagesH3style">
                          <center class="Noofstoppagesplusminusstyle">-<?php echo $NoOfStoppagesStart - $NoOfStoppagesEnd; ?></center>
                        </h3>
                   </div>
                   <div class="panel panel-inverse panel-primary boxShadow NoofstoppagesDecreasedstyle">
                        <h3 class="NoofstoppagesH3style">
                          <center class="NoofstoppagesincDecStabstyle"><img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                  <?php }else if ($NoOfStoppagesStart < $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow NoofstoppagesPlusstyle">
                        <h3 class="NoofstoppagesH3style">
                          <center  class="Noofstoppagesplusminusstyle">+<?php echo $NoOfStoppagesEnd - $NoOfStoppagesStart; ?></center>
                        </h3>
                   </div>
                   <div class="panel panel-inverse panel-primary boxShadow NoofstoppagesIncreasedstyle">
                        <h3 class="NoofstoppagesH3style">
                          <center class="NoofstoppagesincDecStabstyle"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increaseImageStopped.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                  <?php }else{ ?>
                   <div class="panel panel-inverse panel-primary boxShadow NoofstoppagesStablestyle">
                        <h3 class="NoofstoppagesH3style">
                          <center class="NoofstoppagesincDecStabstyle"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php  } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo NoofstoppagesEndstyle">
                    	<h3 style="color:#002060;"><?php echo  $NoOfStoppagesEnd; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="NoofreportsMainDiv">
              <h4 style="color:white;"><center> <?php echo Noofreports; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle NoofreportsStartDateStyle">
                    	<?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?>
                   	</h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle NoofreportsStartDateStyle">
                    	<?php    if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?>
                    </h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary Noofreportsstartstyle">
                      <h3 style="color:#002060;"><?php echo $NoOfReportsStart; ?></h3>
                    </div>
                  </center>
                </div>
                <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                   <?php if ($NoOfReportsEnd > $NoOfReportsStart) { ?>
                     <div class="panel panel-inverse panel-primary boxShadow NoofreportsPlusstyle">
                        <h3 class="NoofreportsH3style">
                          <center style="padding-top: 3px;color:white">+<?php echo $NoOfReportsEnd - $NoOfReportsStart; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow NoofreportsIncreasedstyle">
                        <h3 class="NoofreportsH3style">
                          <center style="padding-top: 3px;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconreports.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>

                   <?php }else if ($NoOfReportsEnd < $NoOfReportsStart) { ?>
                     <div class="panel panel-inverse panel-primary boxShadow NoofreportsMinusstyle">
                        <h3 class="NoofreportsH3style">
                          <center style="color:white">-<?php echo $NoOfReportsStart - $NoOfReportsEnd; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow NoofreportsDecresedstyle">
                        <h3 class="NoofreportsH3style">
                          <center><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreasediconreports.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>

                   <?php }else{ ?>

                    <div class="panel panel-inverse panel-primary boxShadow NoofreportsStablestyle">
                        <h3 class="NoofreportsH3style">
                          <center style="padding-top: 3px;">&nbsp;<?php echo Stable; ?></center>
                        </h3>
                    </div>

                   <?php  } ?>
				</div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo NoofreportsEndsstyle">
                    	<h3 style="color:#002060;"><?php echo $NoOfReportsEnd; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
         </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="RunningTimeMaindiv">
              <h4 style="color:white;"><center> <?php echo Runningtime; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle RunningStartdateStyless">
                    	<?php
	                      if ($meetingNoteDetails->filterType == "1") 
	                      {
	                        echo $meetingNoteDetails->startDate; 
	                      }
	                      elseif ($meetingNoteDetails->filterType == "2") 
	                      {
	                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
	                      }
	                      elseif ($meetingNoteDetails->filterType == "3") 
	                      {
	                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
	                      }
	                      elseif ($meetingNoteDetails->filterType == "4") 
	                      {
	                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
	                      } 
	                    ?>
	                </h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle RunningStartdateStyless">
                      <?php    if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?>
                        
                      </h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone RunningDatahourPanelsstyle">
                    	<h3 class="RunningDataH3style"><?php echo $runningStartDataHours; ?><br><?php echo Hours;?></h3>
                    </div>
                  </center>
                </div>

                <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  
                   <?php if ($runningEndDataHours > $runningStartDataHours) { ?>
                   <div class="panel panel-inverse panel-primary boxShadow RunningDataPlusstyle">
                        <h3 class="RunningDataH3style">
                          <center class="RunningDataplusminusstyle">+<?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                    </div>
                   <div class="panel panel-inverse panel-primary boxShadow RunningDataIncreasedstyle">
                        <h3 class="RunningDataH3style">
                          <center class="RunningDatavaluestyle"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasedicon.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                   <?php }else if ($runningEndDataHours < $runningStartDataHours) { ?>
                    <div class="panel panel-inverse panel-primary boxShadow RunningDataMinusstyle">
                        <h3 class="RunningDataH3style">
                          <center class="RunningDataplusminusstyle"><?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow RunningDataDecreasedstyle">
                        <h3 class="RunningDataH3style">
                          <center class="RunningDatavaluestyle"><img style="height:9px;" src="<?php echo base_url();?>assets/img/runningDecreased.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                   <?php }else{ ?>
                    <div class="panel panel-inverse panel-primary boxShadow RunningDataStablestyle">
                        <h3 class="RunningDataH3style">
                          <center class="RunningDatavaluestyle"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php  } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo RunningDatacirclPanelTwostyle">
                    	<h3 style="font-size:12px;color:#002060;"><?php echo $runningEndDataHours; ?><br><?php echo Hours;?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
         </div>
         <div class="col-md-6">
            <div class="NoProductionTimeMainDivstyle">
              <h4 style="color:white;"><center> <?php echo Noproductiontime; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle NoProductionstartdatestyless">
                    	<?php
	                      if ($meetingNoteDetails->filterType == "1") 
	                      {
	                        echo $meetingNoteDetails->startDate; 
	                      }
	                      elseif ($meetingNoteDetails->filterType == "2") 
	                      {
	                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
	                      }
	                      elseif ($meetingNoteDetails->filterType == "3") 
	                      {
	                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
	                      }
	                      elseif ($meetingNoteDetails->filterType == "4") 
	                      {
	                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
	                      } 
	                      ?>
	                </h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle NoProductionstartdatestyless">
                    	<?php    
                    		if ($meetingNoteDetails->filterType == "1") 
		                    {
		                       echo $meetingNoteDetails->endDate; 
		                    }
		                    elseif ($meetingNoteDetails->filterType == "2") 
		                    {
		                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
		                    }
		                    elseif ($meetingNoteDetails->filterType == "3") 
		                    {
		                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
		                    }
		                    elseif ($meetingNoteDetails->filterType == "4") 
		                    {
		                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
		                    } 
	                    ?>
	                </h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone NoproductionCirclepanelOneStyle">
                    	<h3 style="font-size:12px;color:#002060;"><?php echo $noProductionStartDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>
                <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  <?php if ($noProductionStartDataHours > $noProductionEndDataHours) { ?>
                    
                  <div class="panel panel-inverse panel-primary boxShadow NoproductionMinusStyle">
                        <h3 class="NoproductionH3Style">
                          <center class="NoproductionPlusMinusStyle">-<?php echo $noProductionStartDataHours - $noProductionEndDataHours; ?></center>
                        </h3>
                  </div>
                   <div class="panel panel-inverse panel-primary boxShadow NoproductionDecreasedStyle">
                        <h3 class="NoproductionH3Style">
                          <center class="NoproductionValuesStyle">
                            <img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconNoproduction.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                  <?php }else if ($noProductionStartDataHours < $noProductionEndDataHours) {  ?>
                    <div class="panel panel-inverse panel-primary boxShadow NoproductionPLusStyle">
                        <h3 class="NoproductionH3Style">
                          <center class="NoproductionPlusMinusStyle">+<?php echo $noProductionEndDataHours - $noProductionStartDataHours; ?></center>
                        </h3>
                  </div>
                   <div class="panel panel-inverse panel-primary boxShadow NoproductionIncreasedStyle">
                        <h3 class="NoproductionH3Style">
                          <center class="NoproductionValuesStyle">
                            <img style="height:9px;" src="<?php echo base_url();?>assets/img/decreaseiconNoporduction.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                  <?php }else{ ?>
                   <div class="panel panel-inverse panel-primary boxShadow NoproductionStableStyle">
                        <h3 class="NoproductionH3Style">
                          <center class="NoproductionValuesStyle"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo NoproductionCirclepanelTwoStyle">
                      <h3 style="font-size:12px;color:#002060;"><?php echo $noProductionEndDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
         </div>
        </div>
      </div>
    </div>

        <div class="col-md-12">
          <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;">
            <h3 style="color:#002060;margin-top: 25px;margin-left: 20px;"><?php echo Reports; ?></h3>
            <div class="panel-body">

          <table id="" class="display table m-b-0 reportAProblemTable"  width="100%" cellspacing="0">
            <thead>
              <tr role="row" style="border-left: none;background:rgb(255,255,255); ">
                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterUserNameSelected" class="dropdown" style="padding: 4px;border-radius: 5px;min-width: 90px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> <?php echo Operator; ?>&nbsp;</span>
                    <div class="dropdown-menu .hdhdhhd">
                  
                      <?php foreach ($users as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterUserName()" class="filterUserName form-check-input" type="checkbox" value="<?php echo $value['userId']; ?>" id="filterUserName<?php echo $key; ?>" >
                            <label class="form-check-label" for="filterUserName<?php echo $key; ?>">
                              <?php echo $value['userName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>

                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
                <th id="tableheadreportresolved" >
                  <div style="margin-bottom: 17px;"><?php echo Date; ?><div>
                  <input type="hidden" name="dateValS" id="dateValS" value="<?php echo $meetingNoteDetails->startDate; ?>" />  
                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo $meetingNoteDetails->endDate; ?>" />   
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterTypeSelected" class="dropdown" style="padding: 4px;border-radius: 5px;text-transform: capitalize;min-width: 55px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterTypeSelectedValue"> <?php echo type; ?>&nbsp;</span>
                    <div class="dropdown-menu" >
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'1'" class="filterType form-check-input" type="checkbox" id="accident1" 
                           >
                          <label class="form-check-label" for="accident1">
                            <?php echo Accident; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'0'" class="filterType form-check-input" type="checkbox" id="incident1" 
                           >
                          <label class="form-check-label" for="incident1">
                            <?php echo Incident; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'2'" class="filterType form-check-input" type="checkbox" id="suggestion1" 
                           >
                          <label class="form-check-label" for="suggestion1">
                            <?php echo Suggestion; ?>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                <small style="color: #FF8000;"  >&nbsp;</small>
              </th>
              <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Description; ?><div></th>
              <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Cause; ?><div></th>
              <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Improvement; ?><div></th>
              
              <th id="tableheadreportresolved"> 
                <div class="dropdown" id="filterStatusSelected tableheadreportresolvedselected">
                  <span class="dropdown-toggle" data-toggle="dropdown" id="filterStatusSelectedValue" style="width: 50px;"> <?php echo confidential; ?>&nbsp;
                  </span>
                  <div class="dropdown-menu">
                    <div class="dropdown-item">
                      <div class="form-check" style="width: max-content;">
                        <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="confidential1" value="'1'">
                        <label class="form-check-label" for="confidential1">
                          <?php echo confidential; ?>
                        </label>
                      </div>
                    </div>
                    <div class="dropdown-item">
                      <div class="form-check" style="width: max-content;">
                        <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="not_confidential1" value="'0'">
                        <label class="form-check-label" for="not_confidential1">
                          <?php echo Notconfidential; ?>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <small style="color: #FF8000;text-align : center;"  >&nbsp;</small>
              </th>
            </tr>
          </thead>
        </table>
      </div>
    </div>
  </div>

  <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 120px;margin-left: 12px;margin-right: 16px;">
    <h3 style="color:#002060;margin-left: 20px;    padding-top: 20px;"><?php echo Note; ?></h3>
      <input type="hidden" id="noteId" value="<?php echo $meetingNoteDetails->noteId; ?>">
      <div class="row" style="margin-left: 10px;">
        <div class="col-md-12" style="flex: 0 0 47% !important;max-width: 47% !important;">
          <textarea placeholder="<?php echo EnteranynoteshereOptional; ?>" required maxlength="1000" onkeyup="checkwordcount();"class="NotesStyle" name="problemText" id="description"><?php echo $meetingNoteDetails->notes; ?></textarea>
          <small style="float: right;color: #d8cfcf;"><span id="descriptionLength"> <?php echo strlen($meetingNoteDetails->notes); ?> </span>/1000</small>
        </div>
        
        <div class="col-md-12" style="margin-bottom: 10px">
         <center> <a href="<?php echo base_url('MeetingNotes/meeting_notes'); ?>" class="btn btn-primary"><?php echo save; ?></a></center>
        </div>
      </div>
    </div>
  <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
  <i class="fa fa-angle-up"></i>
</a>
</div>

<div class="modal fade" id="modal-update-report-meeting-notes" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content" style="width: 550px;">
      <div class="modal-header" style="background-color: #002060;">
        <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title machine_name" id="liveMachineName"><?php echo Report; ?></h4>
        <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
      </div>
      <div class="modal-body">
        <form class="p-b-20" action="update_reportProblem_form" method="POST" id="update_reportProblem_form" style="margin-left: 10px;margin-right: 10px;">
            <input type="hidden" id="problemId" name="problemId">
              <div class="row">
                <div class="col-md-4 modalbodycolmd4">
                  <img id="userImage modalbodyUserImageStyle">  
                </div>
                <div class="col-md-8 modalbodycolmd8">
                  <span id="userName modalbodyUserNamr"><?php echo testnytt; ?></span> <br>
                  <small style="font-size: 80%;" id="userId">UserID : 56</small>
                </div>
              </div>

              <div class="row">
                <div class="col-md-6" style="" tyle="padding-top: 32px;padding-bottom: 33px;">
                  <h3 id="typeText" style="color: #002060;margin-left: 10px;"></h3>
                  <input type="hidden" name="type" id="type">
                </div>
                <div class="col-md-6" style="padding-top: 32px;padding-bottom: 33px;">
                  <small style="float: right;margin-top: 10px;padding-right: 10px;" id="date">2020-12-14</small>
                </div>
            </div>
            <div class="row" style="padding-left: 15px;margin-bottom: 15px;">
              <div class="col-md-6">
                <div class="form-check form-check-inline">
                  <input class="form-check-input" type="checkbox" name="isActive" id="resolved" value="1" wtx-context="C5B7D81C-A843-4F32-A1D6-857A34C5C253">
                  <label class="form-check-label" for="resolved" style="font-size: 80% !important;"><?php echo Resolved; ?></label>
                </div>
              </div>
            </div>

            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Description; ?></b></small><br>
                <textarea class="textareaheader" name="description" id="descriptionText" maxlength="1000" onkeyup="checkReportWordCount(this.id)"></textarea>
                <small style="float: right;color: #d8cfcf;"><span id="descriptionTextLength"> 0 </span>/1000</small>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Cause; ?></b></small><br>
                <textarea class="textareaheader" name="cause" id="cause" maxlength="1000" onkeyup="checkReportWordCount(this.id)"></textarea>
                <small style="float: right;color: #d8cfcf;"><span id="causeLength"> 0 </span>/1000</small>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>

            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Improvement; ?></b></small><br>
                <textarea class="textareaheader" name="improvement" id="improvement" maxlength="1000" onkeyup="checkReportWordCount(this.id)"></textarea>
                <small style="float: right;color: #d8cfcf;"><span id="improvementLength"> 0 </span>/1000</small>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>


            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12" style="padding-left: 26px;">
                <div id="not_confidentials" style="display: none;">
                  <img  width="16" style="" src="<?php echo base_url('assets/img/cross.svg'); ?>"> 
                  <span style="color: red"> &nbsp;&nbsp;<b><?php echo Notconfidential; ?></b></span>
                </div>

                <div id="confidentials" style="display: none;">
                  <img width="16" style="" src="<?php echo base_url('assets/img/tick.svg'); ?>"> 
                  <span style="color: green"> &nbsp;&nbsp;<b><?php echo confidential; ?></b></span>
                </div>

                <center>
                  <button id="update_reportProblem_submit" style="" type="submit" class="btn btn-sm btn-primary savebuttonreportpanel">
                    <?php echo save; ?>
                  </button>
                </center>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>
  
<div class="modal fade" id="modal-download-meeting-notes" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content downloadpreviewbuttonstyle" style="width: 192%;left: -48%;" >
      <div class="modal-header" style="background-color: #002060;">
        <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title"><?php echo Downloadpreview; ?></h4>
        <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal">
          <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
        </button>
      </div>

      <div class="modal-body" style="background: #f2f2f2;">
      <div class="downloadpreviewinnerstyle" style="background-color: white;margin-left: 68px;margin-right: 68px;padding: 20px;">
      <h3 style="color:#002060;margin-left:18px;">
        <?php echo ucfirst($meetingNoteDetails->noteName); ?> - <?php    if ($meetingNoteDetails->filterType == "1") 
        {
          echo Daily;
        }
        elseif ($meetingNoteDetails->filterType == "2") 
        {
          echo Weekly;
        }
        elseif ($meetingNoteDetails->filterType == "3") 
        {
          echo Monthly;
        }
        elseif ($meetingNoteDetails->filterType == "4") 
        {
          echo Yearly;
        } 
        ?>
      </h3>
      <div class="row" style="margin-left: 10px;">
        <div class="col-md-12">
            <?php foreach ($machines as $key => $value) { ?>
              <button type="button" class="btn btn-primary" style="background: #002060 !important;border-color: #002060!important;"><?php echo $value['machineName']; ?></button>
            <?php } ?>
        </div>
    </div>


    <div class="col-md-12">
      <div class="row" style="margin-top: 20px;">
        <div class="col-md-6">
          <div class="NoofstoppagesMaindiv">
            <h4 style="color:white;"><center> <?php echo Noofstoppages; ?> </center></h4>
          </div>
          <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
            <div class="row">
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="startdateStyle startdateStyless">
                    <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3")
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?>
                    </h3>
                </center>
              </div>
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="enddateStyle startdateStyless">
                    <?php    if ($meetingNoteDetails->filterType == "1") 
                    {
                      echo $meetingNoteDetails->endDate; 
                    }
                    elseif ($meetingNoteDetails->filterType == "2") 
                    {
                      echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "3") 
                    {
                      echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "4") 
                    {
                      echo  date('Y',strtotime($meetingNoteDetails->endDate));
                    } 
                    ?>
                  </h3>
                </center>
              </div>
              <span class="divdotlinestyle" style="margin-left:20%;">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
              </span>
            </div>
            <div class="row">
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepanelone modalNoofstoppagescirclepanelOne">
                    <h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfStoppagesStart; ?></h3>
                  </div>
                </center>
              </div>
              <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                <?php if ($NoOfStoppagesStart > $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalNoofstoppagesMinusPanelstyle">
                    <h3 style="">
                      <center style="padding-top: 3px;color:white">-<?php echo $NoOfStoppagesStart - $NoOfStoppagesEnd; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow modalNoofstoppagesDecreasedstyle">
                    <h3 class="modalNoofstoppagesH3style">
                      <center style="padding-top: 3px;color:#f60100">
                        <img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png">&nbsp;<?php echo Decreased; ?></center>
                    </h3>
                  </div>
                <?php }else if ($NoOfStoppagesStart < $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalNoofstoppagesplusPanelstyle">
                    <h3 class="modalNoofstoppagesH3style">
                      <center style="padding-top: 3px;color:white">+<?php echo $NoOfStoppagesEnd - $NoOfStoppagesStart; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow modalNoofstoppagesIncreasedstyle">
                    <h3 class="modalNoofstoppagesH3style">
                      <center style="padding-top: 3px;color:#f60100">
                        <img style="height:9px;" src="<?php echo base_url();?>assets/img/increaseImageStopped.png">&nbsp;<?php echo Increased; ?></center>
                    </h3>
                  </div>
                <?php }else{ ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalNoofstoppagesStablestyle">
                    <h3 class="modalNoofstoppagesH3style">
                      <center style="padding-top: 3px;color:#f60100">
                        <img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png">&nbsp;<?php echo Stable; ?></center>
                    </h3>
                  </div>
                <?php  } ?>
              </div>
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo modalNoofstoppagescirclepanelTwo">
                    <h3 style="color:#002060;font-size: 18px;"><?php echo  $NoOfStoppagesEnd; ?></h3>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-6">
          <div class="ModalNoofReportsMaindiv">
            <h4 style="color:white;"><center> <?php echo Noofreports; ?> </center></h4>
          </div>
          <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
            <div class="row">
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="startdateStyle ModalstartdateStyleee">
                    <?php
                    if ($meetingNoteDetails->filterType == "1") 
                    {
                      echo $meetingNoteDetails->startDate; 
                    }
                    elseif ($meetingNoteDetails->filterType == "2") 
                    {
                      echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "3") 
                    {
                      echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "4") 
                    {
                      echo  date('Y',strtotime($meetingNoteDetails->startDate));
                    } 
                    ?>
                  </h3>
                </center>
              </div>
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="enddateStyle" style="color:#002060;margin-top: 15px;font-size:12px;">
                    <?php    if ($meetingNoteDetails->filterType == "1") 
                    {
                      echo $meetingNoteDetails->endDate; 
                    }
                    elseif ($meetingNoteDetails->filterType == "2") 
                    {
                      echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "3") 
                    {
                      echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "4") 
                    {
                      echo  date('Y',strtotime($meetingNoteDetails->endDate));
                    } 
                    ?>
                  </h3>
                </center>
              </div>
              <span class="divdotlinestyle" style="margin-left:20%;">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
              </span>
            </div>
            <div class="row">
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepanelone" style="border-radius: 50%;width: 50px;height: 50px;padding:10px;"><h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfReportsStart; ?></h3>
                  </div>
                </center>
              </div>
              <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                <?php if ($NoOfReportsEnd > $NoOfReportsStart) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofreportsPlusStyle">
                    <h3 class="ModalNoofreportH3style">
                      <center style="padding-top: 3px;color:white">+<?php echo $NoOfReportsEnd - $NoOfReportsStart; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofreportIncreasedStyle">
                    <h3 class="ModalNoofreportH3style" style="margin-top:-15px;">
                      <center style="padding-top: 3px;color: black;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconreports.png">&nbsp;<?php echo Increased; ?></center>
                     
                    </h3>
                  </div>

                <?php }else if ($NoOfReportsEnd < $NoOfReportsStart) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofreportMinustyle">
                    <h3 class="ModalNoofreportH3style">
                      <center style="padding-top: 3px;color:white">-<?php echo $NoOfReportsStart - $NoOfReportsEnd; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofreportDecreasedtyle">
                    <h3 class="ModalNoofreportH3style">
                     <center style="padding-top: 3px;color: black;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreasediconreports.png">&nbsp;<?php echo Decreased; ?></center>
                    </h3>
                  </div>

                <?php }else{ ?>

                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofreportStablestyle">
                    <h3 class="ModalNoofreportH3style">
                      <center style="padding-top: 3px;color: black;"><?php echo Stable; ?></center>
                    </h3>
                  </div>

                <?php  } ?>


              </div>
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo ModalNoofreportcirclePanelTwostyle">
                    <h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfReportsEnd; ?></h3>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-6">
          <div class="ModalRunningTimeMaindiv">
            <h4 style="color:white;"><center> <?php echo Runningtime; ?> </center></h4>
          </div>
          <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
            <div class="row">
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="startdateStyle ModalRunningstartdateStylee">
                    <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?>
                    </h3>
                </center>
              </div>
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="enddateStyle ModalRunningstartdateStylee">
                    <?php    if ($meetingNoteDetails->filterType == "1") 
                  {
                    echo $meetingNoteDetails->endDate; 
                  }
                  elseif ($meetingNoteDetails->filterType == "2") 

                  {
                    echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                  }
                  elseif ($meetingNoteDetails->filterType == "3") 

                  {
                    echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                  }
                  elseif ($meetingNoteDetails->filterType == "4") 

                  {
                    echo  date('Y',strtotime($meetingNoteDetails->endDate));
                  } 
                  ?></h3>
                </center>
              </div>
              <span class="divdotlinestyle" style="margin-left:20%;">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
              </span>
            </div>

            <div class="row">
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepanelone modalRunningCirclePanelOne">
                    <h3 style="font-size:12px;color:#002060;"><?php echo $runningStartDataHours; ?><br><?php echo Hours; ?></h3>
                  </div>
                </center>
              </div>
              <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                <?php if ($runningEndDataHours > $runningStartDataHours) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalRunningPlusstyle">
                    <h3 class="modalRunningH3Style">
                      <center style="padding-top: 3px;color:white">+<?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                    </h3>
                  </div>
                  
                  <div class="panel panel-inverse panel-primary boxShadow modalRunningIncreasedstyle">
                    <h3 class="modalRunningH3Style">
                    	<center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasedicon.png">&nbsp;<?php echo Increased; ?></center>
                    </h3>
                  </div>
                <?php }else if ($runningEndDataHours < $runningStartDataHours) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalRunningMinusstyle">
                    <h3 class="modalRunningH3Style">
                      <center style="padding-top: 3px;color:white"><?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow modalRunningDecreasedstyle">
                    <h3 class="modalRunningH3Style">
                       <center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/runningDecreased.png">&nbsp;<?php echo Decreased; ?></center>
                    </h3>
                  </div>
                <?php }else{ ?>
                  <div class="panel panel-inverse panel-primary boxShadow modalRunningStablestyle">
                    <h3 class="modalRunningH3Style">
                      <center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasedicon.png">&nbsp;<?php echo Stable; ?></center>
                    </h3>
                  </div>
                <?php  } ?>
              </div>
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo modalRunningCirclpanelTwo">
                    <h3 style="font-size:12px;color:#002060;"><?php echo $runningEndDataHours; ?><br><?php echo Hours; ?></h3>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>

        <div class="col-md-6">
          <div style="color: white;background-color: #ff8000;border-radius: 10px;padding-top: 15px;padding-bottom: 10px;">
            <h4 style="color:white;"><center> <?php echo Noproductiontime; ?> </center></h4>
          </div>
          <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
            <div class="row">
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="startdateStyle" style="color:#002060;margin-top: 15px;font-size:12px;"> 
                    <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?>
                    </h3>
                </center>
              </div>
              <div class="col-md-3" style="min-width: 50%;">
                <center>
                  <h3 class="enddateStyle" style="color:#002060;margin-top: 15px;font-size:12px;">
                    <?php    
                    if ($meetingNoteDetails->filterType == "1") 
                    {
                      echo $meetingNoteDetails->endDate; 
                    }
                    elseif ($meetingNoteDetails->filterType == "2") 
                    {
                      echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "3") 
                    {
                      echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                    }
                    elseif ($meetingNoteDetails->filterType == "4") 
                    {
                      echo  date('Y',strtotime($meetingNoteDetails->endDate));
                    } 
                    ?>
                  </h3>
                </center>
              </div>
              <span class="divdotlinestyle" style="margin-left:20%;">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
              </span>
            </div>
            <div class="row">
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepanelone ModalNoofproductionCirclePanel">
                    <h3 style="font-size:12px;color:#002060;"><?php echo $noProductionStartDataHours; ?><br><?php echo Hours; ?></h3>
                  </div>
                </center>
              </div>

              <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                <?php if ($noProductionStartDataHours > $noProductionEndDataHours) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofproductionMinusPanel">
                    <h3 class="ModalNoofproductionH3Style">
                      <center class="ModalNoofproductionPlusMinuStyle">-<?php echo $noProductionStartDataHours - $noProductionEndDataHours; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofproductionDecreased">
                    <h3 class="ModalNoofproductionH3Style">
                       <center class="ModalNoofproductionValuesStyle">
                        <img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconNoproduction.png">&nbsp;<?php echo Decreased; ?></center>
                    </h3>
                  </div>
                <?php }else if ($noProductionStartDataHours < $noProductionEndDataHours) {  ?>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofproductionPlusPanel">
                    <h3 class="ModalNoofproductionH3Style">
                      <center class="ModalNoofproductionPlusMinuStyle">+<?php echo $noProductionEndDataHours - $noProductionStartDataHours; ?></center>
                    </h3>
                  </div>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofproductionIncreased">
                    <h3 class="ModalNoofproductionH3Style">
                       <center class="ModalNoofproductionValuesStyle">
                        <img style="height:9px;" src="<?php echo base_url();?>assets/img/decreaseiconNoporduction.png">&nbsp;<?php echo Increased; ?></center>
                    </h3>
                  </div>
                <?php }else{ ?>
                  <div class="panel panel-inverse panel-primary boxShadow ModalNoofproductionStable">
                    <h3 class="ModalNoofproductionH3Style">
                      <center class="ModalNoofproductionValuesStyle"><?php echo Stable; ?></center>
                    </h3>
                  </div>
                <?php } ?>
              </div>
              <div class="col-md-2" style="min-width: 45%;">
                <center>
                  <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo ModalNoofproductionCirclePanelTwo">
                    <h3 style="font-size:12px;color:#002060;"><?php echo $noProductionEndDataHours; ?><br><?php echo Hours; ?></h3>
                  </div>
                </center>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>

    <div class="col-md-12 " style="padding-right: 0px;padding-left: 0px;">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;margin-left: 12px;margin-right: 20px;">
        <h3 style="color:#002060;margin-top: 25px;margin-left: 20px;"><?php echo Reports; ?></h3>
        <div class="panel-body">
          <table id="" class="display table m-b-0 reportAProblemTableModel"  width="50%" cellspacing="0">
            <thead>
              <tr role="row" style="border-left: none;background:rgb(255,255,255); ">
                <input type="hidden" name="dateValS" id="dateValS" value="<?php echo $meetingNoteDetails->startDate; ?>" />  
                <input type="hidden" name="dateValE" id="dateValE" value="<?php echo $meetingNoteDetails->endDate; ?>" />  
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Operator; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Date; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo type; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Description; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Cause; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Improvement; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo confidential; ?><div></th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 120px;margin-left: 12px;margin-right: 24px;">
      <h3 style="color:#002060;margin-left: 20px;padding-top: 19px;"><?php echo Note; ?></h3>

      <div class="row" style="margin-left: 10px;">
        <div class="col-md-12" style="flex: 0 0 47% !important;max-width: 47% !important;">
          <textarea id="notesText" required maxlength="1000" readonly style="border-left: none;border-right: none;border-top: none;border-bottom: none; width: 100%;height: 80px;color: #124D8D;font-size: 90% !important;"><?php echo $meetingNoteDetails->notes; ?></textarea>
        </div>
      </div>
    </div>
  <hr style="height: 2px;background: #e5e7e8;">
</div>
</div>
<div class="modal-footer">
  <center style="margin-bottom: 25px;">
    <a class="btn btn-primary modadownloadbuttonstyle" href="<?php echo base_url('MeetingNotes/downloadPdf/').$meetingNoteDetails->noteId; ?>" class="btn btn-primary">
      <img style="width: 18px;height: 15px;margin-top: -5px;" src="<?php echo base_url('assets/nav_bar/download.png'); ?>"> <?php echo Download; ?>
    </a>
  </center>
  </div>
</div>
</div>


