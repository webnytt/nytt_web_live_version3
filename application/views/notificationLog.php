<link href="<?php echo base_url('assets/css/notification_log.css');?>" rel="stylesheet" />

<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Notificationlog; ?></h1>
	<input type="hidden" name="notificationId" value="0">
	<div class="row">
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  <div class="panel panel-inverse panel-primary boxShadow" >
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;width: 100px !important">
									<?php echo Machine; ?>
								</th>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<?php echo Notificationtext; ?>
								</th>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<?php echo Createddate; ?>
								</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div> 
	</div>

	<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

