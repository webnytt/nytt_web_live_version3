<link href="<?php echo base_url('assets/css/taskMaintenance.css'); ?>" rel="stylesheet" />
  <style>
    <?php $allPer =   ($totalCompletedTask / $totalTask) * 100; 
    if (!empty($allPer)) { ?>
      
    .cardall:nth-child(1) svg circle:nth-child(2)
    {
      stroke-dashoffset:<?php echo 440 - (310 * $allPer) / 100; ?>;
        stroke-linecap: round;
        stroke: #FF8000;
    }
    <?php } ?>
    <?php foreach ($machines as $key => $value) { 
      $machinePer =   ($value['totalCompletedTask'] / $value['totalTask']) * 100;
      if (!empty($machinePer)) {
    ?>
    .card<?php echo $value['machineId']; ?>:nth-child(1) svg circle:nth-child(2)
    {
      stroke-dashoffset:<?php echo 440 - (310 * $machinePer) / 100; ?>;
        stroke-linecap: round;
        stroke: #FF8000;
    }
    <?php } } ?>
    .displayfilterrowstyle
    {
      display:contents!important; 
    }

  </style>

  <div id="content" class="content">
    
    <h1 class="page-header TaskAndMainTenanceHeaderStyle"><?php echo Tasksandmaintenance; ?></h1>

    <div class="row">
    <input type="hidden" name="taskId"  value="0">
      <div class="col-md-12 table_data">
        <div class="panel panel-inverse panel-primary boxShadow" style="overflow:auto;">
            <div class="row" style="float:left;margin: 10px;">
              <span class="displayfilterrowstyle" id="showStatus"></span>
              <span class="displayfilterrowstyle" id="showCreatedDate"></span>
              <span class="displayfilterrowstyle" id="showDueDate"></span>
              <span class="displayfilterrowstyle" id="showMachinefilter"></span>
              <span class="displayfilterrowstyle" id="showRepeat"></span>
              <span class="displayfilterrowstyle" id="showUserFilter"></span>
            </div>
          <div class="panel-body">

            <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
              <thead>
                <tr>
                  <th class="TableHeaderTHstyle">
                    <div id="filterStatusSelected" class="dropdown FilterStatusSelectedStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown"> <?php echo Status; ?>&nbsp;</span>
                      <div class="dropdown-menu">
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterStatus()" value="'completed'" class="form-check-input filterStatus" type="checkbox" id="filterStatusCompleted">
                            <label class="form-check-label" for="filterStatusCompleted">
                              <?php echo Completed; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterStatus()" value="'uncompleted'" class="form-check-input filterStatus" type="checkbox" id="filterStatusUncompleted">
                            <label class="form-check-label" for="filterStatusUncompleted">
                              <?php echo Uncompleted; ?>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>

                  <th  class="TableHeaderTHstyle">
                    <input type="hidden" name="dateValS" id="dateValS" />  
                      <input type="hidden" name="dateValE" id="dateValE" />   
                        <div id="advance-daterange" name="advance-daterange" class="daterangeandduedatestyle">
                          <span>
                              <?php echo Datecreated; ?>&nbsp;&nbsp;
                          </span> 
                         <i class="fa fa-caret-down m-t-2"></i>
                    </div>
                    <small  class="tableColumnsmallStyle">&nbsp;</small>
                  </th>

                  <?php 
                    if ($_GET && isset($_GET['dueDateValS'])) 
                    {
                      $dueDateValE = date("F d, Y", strtotime($_GET['dueDateValE']));
                      $dueDateValS = date("F d, Y", strtotime($_GET['dueDateValS'])); 
                    } 
                 ?>

                  <th class="TableHeaderTHstyle">
                    <input type="hidden" name="dueDateValS" id="dueDateValS" value="<?php echo ($_GET && isset($_GET['dueDateValS'])) ? $_GET['dueDateValS'] : ''; ?>"/>  
                    <input type="hidden" name="dueDateValE" id="dueDateValE" value="<?php echo ($_GET && isset($_GET['dueDateValE'])) ? $_GET['dueDateValS'] : ''; ?>"/>   
                    <div id="due-daterange" name="due-daterange" class="daterangeandduedatestyle">
                      <span>
                        <?php echo Duedate; ?>&nbsp;&nbsp;
                      </span> 
                      <i class="fa fa-caret-down m-t-2"></i>
                    </div>
                    <small  class="tableColumnsmallStyle">&nbsp;</small>
                  </th>
                  
                  <th class="TableHeaderTHstyle">
                    <div id="filterMachineIdSelected" class="dropdown DropDownFilterStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown" id="filterMachineIdSelectedValue"> <?php echo Machine; ?>&nbsp;</span>
                      <div class="dropdown-menu">
                        <?php foreach ($machines as $key => $value) { ?>
                          <div class="dropdown-item">
                            <div class="form-check formCheckWidth" >
                              <input style="cursor: pointer;" onclick="filterMachineId(<?php echo $value['machineId'] ?>,'<?php echo $value['machineName'] ?>')" class="form-check-input filterMachineId" type="checkbox" id="filterMachineId<?php echo $value['machineId'] ?>" name="filterMachineId" value="<?php echo $value['machineId']; ?>">
                              <label style="cursor: pointer;" class="form-check-label" for="filterMachineId<?php echo $value['machineId'] ?>">
                                <?php echo $value['machineName']; ?>
                              </label>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                    <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>
                  
                  <th class="TableHeaderTHstyle">
                    <div id="filterRepeatSelected" class="dropdown DropDownFilterStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown"> <?php echo Repeat; ?>&nbsp;</span>
                      <div class="dropdown-menu">
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterRepeat()" value="'never'" class="form-check-input filterRepeat" type="checkbox" id="filterRepeatNever">
                            <label class="form-check-label" for="filterRepeatNever">
                              <?php echo Never; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterRepeat()" value="'everyDay'" class="form-check-input filterRepeat" type="checkbox" id="filterRepeatEveryDay">
                            <label class="form-check-label" for="filterRepeatEveryDay">
                              <?php echo Everydays; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterRepeat()" value="'everyWeek'" class="form-check-input filterRepeat" type="checkbox" id="filterRepeatEveryWeek">
                            <label class="form-check-label" for="filterRepeatEveryWeek">
                              <?php echo Everyweek; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterRepeat()" value="'everyMonth'" class="form-check-input filterRepeat" type="checkbox" id="filterRepeatEveryMonth">
                            <label class="form-check-label" for="filterRepeatEveryMonth">
                              <?php echo Everymonth; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input onclick="filterRepeat()" value="'everyYear'" class="form-check-input filterRepeat" type="checkbox" id="filterRepeatEveryYear">
                            <label class="form-check-label" for="filterRepeatEveryYear">
                              <?php echo Everyyear; ?>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>

                  <th class="TableHeaderTHstyle">
                    <div id="filterUserNameSelected" class="dropdown DropDownFilterOpeStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> <?php echo Operator; ?>&nbsp;</span>
                      <div class="dropdown-menu">
                       <?php foreach ($users as $key => $value) { ?>
                          <div class="dropdown-item">
                            <div class="form-check formCheckWidth" >
                              <input onclick="filterUserName(<?php echo $value['userId'] ?>,'<?php echo $value['userName'] ?>')" class="form-check-input filterUserName" type="checkbox" id="filterUserName<?php echo $value['userId'] ?>" name="filterUserName" value="<?php echo $value['userId']; ?>">
                              <label class="form-check-label" for="filterUserName<?php echo $value['userId'] ?>">
                                <?php echo $value['userName']; ?>
                              </label>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                    <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>

                  <th class="TableHeaderTH2style"><?php echo Task; ?><br>
                    <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>
                  <?php if ($accessPointEdit == true) { ?>
                  <th class="TableHeaderTH2style"><?php echo Delete; ?><br>
                    <small class="tableColumnsmallStyle">&nbsp;</small>
                  </th>
                  <?php } ?>
                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div> 

      <div class="col-md-3 detail colmd3sidepanelstyle">
         <div class="panel panel-inverse panel-primary" >
          <div class="panel-body" style="padding: 0;">
              <div class="row sidepanelrowstyle">
                <div class="col-md-12" style="height: 47px !important;">
                  <div style="padding: 14px 0 0 0;">
                    <a href="javascript:void(0);" onclick="closeDetails();" style="opacity: 1.0!important;">
                      <img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>">
                    </a>

                    <?php if ($accessPointEdit == true) { ?>
                      <a style="float: right;" href="javascript:void(0);" onclick="editTask();">
                        <img width="25" src="<?php echo base_url("assets/nav_bar/orange_edit.svg"); ?>">
                      </a>
                    <?php } ?>
                  </div>
                </div>
                <div class="col-md-12" >
                  <h3 style="color: #FF8000;padding-bottom:10px;">
                    <center id="machineName" style="width: 100%;font-size: 20px;max-width: 100%;white-space: nowrap;overflow: hidden;text-overflow: ellipsis;"></center>
                </h3>
                  <center>
                    <img id="userImage" style="border-radius: 50%;width:32px;height: 32px;" src="<?php echo base_url('assets/img/user/2.jpg'); ?>">
                  </center> 
                  <center>
                    <span style="color: #9c9aab;"> <?php echo Createdby; ?>:</span> 
                    <span style="color: white;" id="userName"></span> 
                  </center>
                  <center>
                    <small style="color: #FF8000;" id="createdDate"></small>
                  </center>
                </div>
              </div>


                <div class="row sidepanelrowheaderstyle"><div class="col-md-12 colmd12mb5Style" style="font-weight: 500;"><?php echo Status; ?></div>
                  <div id="statusHtml" style="max-height: 100px;overflow: auto;"></div>
                  <div class="col-md-12"><hr class="Colmd3spHrStyle"></div>
                </div>
                
                <div class="row sidepanelrowheaderstyle">
                  <div class="col-md-12" style="font-weight: 500;"><?php echo Task; ?></div>
                  <div class="col-md-12">
                    <span class="sidepaneltextareastyle" style="color:#002060!important;font-weight:400!important;" name="task" id="task"></span></div>
                  <div class="col-md-12"> <hr class="Colmd3spHrStyle"></div>
                    
                  </div>
                </div>
                
                <div class="row sidepanelrowheaderstyle"><div class="col-md-12 colmd12mb5Style" style="font-weight: 500;"><?php echo Repetition; ?>(<span id="repeatName"></span>)</div>

                  <div class="dayT"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Everydays; ?>(<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="neverT"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Never; ?> (<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="week"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Dueon; ?> <span style="text-transform: lowercase;font-weight: bold" id="dueWeekDayText"></span> (<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="month month1" style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Onday; ?><span  style="text-transform: lowercase;font-weight: bold" id="dueMonthMonthText"></span> month (<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="month month2"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Onthe; ?> <span id="dueMonthDayText" style="text-transform: lowercase;font-weight: bold;"></span> of <span id="dueMonthWeekText" style="text-transform: lowercase;font-weight: bold;"></span> <?php echo week; ?> (<span class="dueTaskDate"></span>)</div>
                  </div>

                  <div class="month month3" style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Everymonth; ?> (<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="year year1"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo On; ?> <span style="text-transform: lowercase;font-weight: bold" id="dueYearMonthDayText"></span> <span style="text-transform: lowercase;font-weight: bold" id="dueYearMonthText"></span> (<span class="dueTaskDate"></span>)</div>
                  </div>


                  <div class="year year2"  style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Onthe; ?>  <span id="dueYearDayText" style="text-transform: lowercase;font-weight: bold;"></span> <?php echo of; ?> <span id="dueYearWeekText" style="text-transform: lowercase;font-weight: bold;"></span> <?php echo week; ?> <span id="dueYearMonthOnTheText" style="text-transform: lowercase;font-weight: bold;"></span> (<span class="dueTaskDate"></span>)</div>
                  </div>

                  <div class="year year3" style="display: none;margin-left: -5px;" >
                    <div class="col-md-12 colmd12mb5Style"><?php echo Everyyear; ?> (<span class="dueTaskDate"></span>) </div>
                  </div>

                  <div class="col-md-12"><hr class="Colmd3spHrStyle"></div>

                </div>

                 <div class="row sidepanelrowheaderstyle">
                  <div class="col-md-6 colmd12mb5Style" style="font-weight: 500;"><?php echo Repetitionendon; ?></div>
                  <div class="col-md-6 colmd12mb5Style" style="font-weight: 500;"><?php echo Repetitionendoccurances; ?></div>
                  <div class="col-md-6 colmd12mb5Style" id="endTaskDate"></div>
                  <div class="col-md-6 colmd12mb5Style" id="EndAfteroccurancesText"></div>
                  <div class="col-md-12"><hr class="Colmd3spHrStyle"></div>
                </div>

                <div class="row sidepanelrowheaderstyle EndAfteroccurancesText">
                  
                </div>
               

                <div class="row sidepanelrowheaderstyle"><div class="col-md-12 colmd12mb5Style" style="font-weight: 500;"><?php echo Operators; ?></div>
                  <div id="operatorsHtml"></div>
                </div>
              
            </div>
          </div>
        </div>
      
    <hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
  </div>

  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
  </div>


   <div class="modal fade" id="modal-edit-task" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
            <h4 class="modal-title machine_name addtaskmodalh4style" id="liveMachineName"><?php echo Edittask; ?></h4>
            <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
              <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
            </button>
        </div>
        <div class="modal-body">
          
          <form class="p-b-20" action="edit_task" method="POST" id="edit_task_form" style="margin-left: 10px;margin-right: 10px;">



            <input type="hidden" id="taskIdEdit" name="taskIdEdit">
            <input type="hidden" id="lastSelectedMachine" value="0">
            <div class="row" style="margin-left: 0px;margin-right: 0px;">
              <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
                <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="taskEdit" name="taskEdit" 
                placeholder="<?php echo Enternewtask; ?>" required oninvalid="this.setCustomValidity('<?php echo pleasefilloutthisfield; ?>')"> 
              </div>

              <div style="padding: 15px 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Machine Type" >
                <label><?php echo Repeatthetask; ?></label>
                <div class="dropdown colmd3SpRepeatTaskStyle">

                  <div class="dropdown-toggle" data-toggle="dropdown" id="selectedRepeatEdit"> <?php echo Selectanoption; ?> </div>
                  <div class="dropdown-menu" style="width: 100%;margin-left: -8px;margin-top: 17px;">
                    <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeatEdit('<?php echo never; ?>');">
                      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Never; ?>
                      <hr class="sidepanelrepeattaskstyle">
                    </div>

                    <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeatEdit('everyDay');">
                      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everydays; ?>
                      <hr class="sidepanelrepeattaskstyle">
                    </div>

                    <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeatEdit('everyWeek');">
                      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everyweek; ?>
                      <hr class="sidepanelrepeattaskstyle">
                    </div>

                    <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeatEdit('everyMonth');">
                      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everymonth; ?>
                      <hr class="sidepanelrepeattaskstyle">
                    </div>

                    <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeatEdit('everyYear');">
                      &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everyyear; ?>
                    </div>
                    <input type="hidden" id="repeatEdit" name="repeatEdit">
                    <input type="hidden" id="repeatOldEdit" name="repeatOldEdit">
                  </div>
                </div>
              </div>

              <div style="padding: 15px 5px;" class="form-group col-md-6">
                <label><?php echo Selectmachine; ?></label>
                <select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="machineIdEdit" name="machineIdEdit[]" >
                  <?php foreach ($machines as $key => $value) { ?>
                    <option value="<?php echo $value['machineId']; ?>"><?php echo $value['machineName']; ?></option>
                  <?php } ?>
                </select>
              </div>
            </div>

              
              <div class="col-md-12" style="margin-left: -20px;">
                <div>
                  <label style="font-weight: 600;padding-left: 15px;"><?php echo Dueon; ?></label><br>
                  <div class="row col-md-12">
                    <div class="col-md-2 monthSelectEdit" style="display: none;">
                      <div class="form-check">
                        <input class="form-check-input" type="radio" value="1" onclick="checkmonthDueOnEdit(this.value)" name="monthDueOnEdit" id="monthDueOnEdit1">
                        <label class="form-check-label" for="monthDueOnEdit1">
                          <!-- <?php echo Onday; ?>    -->
                        </label>
                      </div>
                    </div>
                    <div class="col-md-8 monthSelectEdit" style="display: none;">
                      <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthMonthEdit" id="dueMonthMonthEdit">
                        <option selected="" disabled=""><?php echo Selectmonthday; ?></option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                        <option value="21">21</option>
                        <option value="22">22</option>
                        <option value="23">23</option>
                        <option value="24">24</option>
                        <option value="25">25</option>
                        <option value="26">26</option>
                        <option value="27">27</option>
                        <option value="28">28</option>
                        <option value="third_last_day"><?php echo Thirdlastday; ?></option>
                        <option value="second_last_day"><?php echo Secondlastday; ?></option>
                        <option value="last_day"><?php echo Lastday; ?></option>
                      </select> 
                    </div>
                  </div>

                  <div class="col-md-12">
                    <div class="row">
                      <div class="col-md-2 yearSelectEdit" style="display: none;">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" value="1" onclick="checkyearDueOnEdit(this.value)" name="yearDueOnEdit" id="yearDueOnEdit1">
                          <label class="form-check-label" for="yearDueOnEdit1">
                            <!-- <?php echo On; ?>     -->
                          </label>
                        </div>
                      </div>
                      
                    
                      <div class="col-md-5 yearSelectEdit m-t-10" style="display: none;">
                        <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonthEdit" id="dueYearMonthEdit">
                          <option selected="" disabled=""><?php echo Selectmonth; ?></option>
                          <option value="January"><?php echo January; ?></option>
                          <option value="February"><?php echo February; ?></option>
                          <option value="March"><?php echo March; ?></option>
                          <option value="April"><?php echo April; ?></option>
                          <option value="May"><?php echo May; ?></option>
                          <option value="June"><?php echo June; ?></option>
                          <option value="July"><?php echo July; ?></option>
                          <option value="August"><?php echo August; ?></option>
                          <option value="September"><?php echo September; ?></option>
                          <option value="October"><?php echo October; ?></option>
                          <option value="November"><?php echo November; ?></option>
                          <option value="December"><?php echo December; ?></option>
                        </select> 
                      </div>

                      <div class="col-md-5 yearSelectEdit m-t-10" style="display: none;">
                        <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonthDayEdit" id="dueYearMonthDayEdit">
                          <option selected="" disabled=""><?php echo Selectmonthday; ?></option>
                          <option value="1">1</option>
                          <option value="2">2</option>
                          <option value="3">3</option>
                          <option value="4">4</option>
                          <option value="5">5</option>
                          <option value="6">6</option>
                          <option value="7">7</option>
                          <option value="8">8</option>
                          <option value="9">9</option>
                          <option value="10">10</option>
                          <option value="11">11</option>
                          <option value="12">12</option>
                          <option value="13">13</option>
                          <option value="14">14</option>
                          <option value="15">15</option>
                          <option value="16">16</option>
                          <option value="17">17</option>
                          <option value="18">18</option>
                          <option value="19">19</option>
                          <option value="20">20</option>
                          <option value="21">21</option>
                          <option value="22">22</option>
                          <option value="23">23</option>
                          <option value="24">24</option>
                          <option value="25">25</option>
                          <option value="26">26</option>
                          <option value="27">27</option>
                          <option value="28">28</option>
                          <option value="third_last_day"><?php echo Thirdlastday; ?></option>
                          <option value="second_last_day"><?php echo Secondlastday; ?></option>
                          <option value="last_day"><?php echo Lastday; ?></option>
                        </select> 
                      </div>
                      <!--   </div>
                      </div> -->
                    </div>
                  </div>

                  <div class="col-md-6 dueSelectEdit" style="padding: 0 15px;">
                    <div class="form-group sidepanelduedatestyle" data-toggle="tooltip" data-title="Name">
                      <input style="border: none;color: #b3acac;width: 100%;" type="text"  class="datepicker58" id="dueDateEdit" name="dueDateEdit" value="<?php echo date('m/d/Y'); ?>" > 
                    </div>
                  </div>

                  <div class="col-md-6 weekSelectEdit" style="padding: 0 15px;display: none;">
                    <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" id="dueWeekDayEdit" name="dueWeekDayEdit">
                      <option selected="" disabled=""><?php echo Selectday; ?></option>
                      <option value="Monday"><?php echo Monday; ?></option>
                      <option value="Tuesday"><?php echo Tuesday; ?></option>
                      <option value="Wednesday"><?php echo Wednesday; ?></option>
                      <option value="Thursday"><?php echo Thursday; ?></option>
                      <option value="Friday"><?php echo Friday; ?></option>
                      <option value="Saturday"><?php echo Saturday; ?></option>
                      <option value="Sundays"><?php echo Sunday; ?></option>
                    </select>
                  </div>

                </div>

                <div class="row monthSelectEdit" style="margin-top: 10px;display: none;">


                  <div class="col-md-12 p-b-10" style="padding-left: 22px;">
                    <div class="form-check">
                      <input class="form-check-input" type="radio" value="2" onclick="checkmonthDueOnEdit(this.value)" name="monthDueOnEdit" id="monthDueOnEdit2">
                      <label class="form-check-label" for="monthDueOnEdit2">
                        <!-- <?php echo Onthe; ?> -->
                      </label>
                    </div>

                  </div>

                  <div class="col-md-6 m-t-10" style="padding-left: 22px;">
                    <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthWeekEdit" id="dueMonthWeekEdit">
                      <option selected="" disabled=""><?php echo Selectweek; ?></option>
                      <option value="1"><?php echo First; ?></option>
                      <option value="2"><?php echo Second; ?></option>
                      <option value="3"><?php echo Third; ?></option>
                      <option value="4"><?php echo Fourth; ?></option>
                      <option value="5"><?php echo Last; ?></option>
                    </select> 
                  </div>

                  <div class="col-md-6 m-t-10" style="padding-left: 22px;">
                    <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthDayEdit" id="dueMonthDayEdit">
                      <option selected="" disabled=""><?php echo Selectday; ?></option>
                      <option value="Monday"><?php echo Monday; ?></option>
                      <option value="Tuesday"><?php echo Tuesday; ?></option>
                      <option value="Wednesday"><?php echo Wednesday; ?></option>
                      <option value="Thursday"><?php echo Thursday; ?></option>
                      <option value="Friday"><?php echo Friday; ?></option>
                      <option value="Saturday"><?php echo Saturday; ?></option>
                      <option value="Sundays"><?php echo Sunday; ?></option>
                    </select> 
                  </div>
                </div>

                <div class="row yearSelectEdit" style="margin-top: 10px;display: none;">

                  <div class="col-md-12 m-t-10 m-l-10">
                    
                  </div>

                  <div class="col-md-12 m-t-10 m-l-10">
                    <div class="row">
                      <div class="col-md-1">
                        <div class="form-check">
                          <input class="form-check-input" type="radio" value="2" onclick="checkyearDueOnEdit(this.value)" name="yearDueOnEdit" id="yearDueOnEdit2">
                        </div>
                      </div>
                      <div class="col-md-5 m-t-20">
                        <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearWeekEdit" id="dueYearWeekEdit">
                          <option selected="" disabled=""><?php echo Selectweek; ?></option>
                          <option value="1"><?php echo First; ?></option>
                          <option value="2"><?php echo Second; ?></option>
                          <option value="3"><?php echo Third; ?></option>
                          <option value="4"><?php echo Fourth; ?></option>
                          <option value="5"><?php echo Last; ?></option>
                        </select> 
                      </div>

                      <div class="col-md-5 m-t-20" style="display: contents;">
                        <div class="daysStyle col-md-5 m-t-20">
                          <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearDayEdit" id="dueYearDayEdit">
                            <option selected="" disabled=""><?php echo Selectday; ?></option>
                            <option value="Monday"><?php echo Monday; ?></option>
                            <option value="Tuesday"><?php echo Tuesday; ?></option>
                            <option value="Wednesday"><?php echo Wednesday; ?></option>
                            <option value="Thursday"><?php echo Thursday; ?></option>
                            <option value="Friday"><?php echo Friday; ?></option>
                            <option value="Saturday"><?php echo Saturday; ?></option>
                            <option value="Sundays"><?php echo Sunday; ?></option>
                          </select> 
                        </div>
                      </div>

                      <div class="col-md-6 m-t-20">
                        <select disabled style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonthOnTheEdit" id="dueYearMonthOnTheEdit">
                          <option selected="" disabled=""><?php echo Selectmonth; ?></option>
                          <option value="January"><?php echo January; ?></option>
                          <option value="February"><?php echo February; ?></option>
                          <option value="March"><?php echo March; ?></option>
                          <option value="April"><?php echo April; ?></option>
                          <option value="May"><?php echo May; ?></option>
                          <option value="June"><?php echo June; ?></option>
                          <option value="July"><?php echo July; ?></option>
                          <option value="August"><?php echo August; ?></option>
                          <option value="September"><?php echo September; ?></option>
                          <option value="October"><?php echo October; ?></option>
                          <option value="November"><?php echo November; ?></option>
                          <option value="December"><?php echo December; ?></option>
                        </select> 
                      </div>
                    </div>
                  </div>
                </div>


                <label class="m-t-10" style="font-weight: 600;margin-left: 15px;"><?php echo Endtask; ?></label>
                <div class="col-md-12">
                  <div class="row">
                    <div class="col-md-6">
                      <div class="form-group sidepanelduedatestyle" data-toggle="tooltip" data-title="Name"  >
                        <input style="border: none;color: #b3acac;width: 95%;" type="text"  class="datepicker59" onchange="selectEndTaskEdit(this.value)" id="endTaskDateEdit" name="endTaskDateEdit" placeholder="<?php echo Endtask; ?>"> 
                        <span onclick="removeEndTaskEdit()" id="removeEndTaskTextEdit" style="display: none;cursor: pointer;float: right;margin-top: -18px;">
                          <i style="color: red;" class="fa fa-times"></i>
                        </span> 
                      </div>
                    </div>

                    <div style="font-weight: 600;" class="col-md-1 m-t-10">
                      <center><?php echo ortext;  ?></center> 
                    </div> 

                    <div class="col-md-5" style="max-width: 180px;">
                      <div class="form-group" data-toggle="tooltip" data-title="Name">
                        <input type="text" class="form-control col-md-12 border-left-right-top-hide" onkeyup="selectEndAfteroccurancesEdit(this.value)" id="EndAfteroccurancesEdit" name="EndAfteroccurancesEdit" 
                        placeholder="<?php echo Enterendtaskafteroccurances; ?>" > 
                      </div>
                    </div>
                  </div>
                </div>


                <div class="row modalSPassignOpeRow">
                  <div class="col-md-12">
                    <label><b><?php echo Assignoperator; ?></b></label>
                  </div>
                </div>


                <input type="hidden" name="userIdsEdit" id="userIdsEdit">
                <input type="hidden" name="oldUserIdsEdit" id="oldUserIdsEdit">


                <div class="row" style="margin-left: 0px;margin-right: 0px;">
                  <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false" style="width: 100%;">
                    <div class="carousel-inner no-padding"  style="display: flex!important;">
                      <input type="hidden" name="" id="machineIds">
                      <div class="carousel-item active">
                        <?php $countQuery = 1;$i = 1; foreach ($users as $key => $value) { if($countQuery % 4 == 0){ ?>
                        </div>
                        <div class="carousel-item">
                        <?php } ?>
                        <div class="col-md-3 colmdCarouselmarginstyle modalCarouselOpeStyle" id="userIdBlockEdit<?php echo $value['userId']; ?>" onclick="selectUsersEdit(<?php echo $value['userId']; ?>)">
                          <img class="modalCarouselOpeImageStyle" style="width:55px!important;height: 55px!important;" src="<?php echo base_url('assets/img/user/').$value['userImage']; ?>">
                          <h6 style="margin-top: 10px;"><?php echo $value['userName']; ?></h6>
                        </div>
                        <?php  $i++; if ($i != 4) { $countQuery++; } } ?>
                      </div>
                    </div>
                    <a class="carousel-control-prev carouselbuttonprevaddtaskstyle"  href="#demo" role="button" data-slide="prev">
                      <span class="carousel-control-prev-icon carouselbuttonprevaddtaskspanstyle">
                        <i class="fa fa-angle-left carouselbuttonimgstyle" aria-hidden="true"></i>
                      </span>
                    </a>
                    <a class="carousel-control-next carouselnextmachinestyle" href="#demo" role="button" data-slide="next">
                      <span class="carousel-control-next-icon carouselnextspanstyle">
                        <i class="fa fa-angle-right carouselbuttonimgnextstyle" aria-hidden="true"></i>
                      </span>
                    </a>
                  </div>
                </div>



                <div class="row" style="margin-left: 0px;margin-right: 0px;">
                  <div class="col-md-12">
                    <br>
                    <center>
                      <button type="submit" class="btn btn-primary m-r-5 addtaskbuttonstyle" id="edit_task_submit" >
                        <?php echo save; ?>
                      </button>
                    </center>
                  </div>
                </div>
              </div>
          </form>

        </div>
      </div>
    </div>
  </div>   



  <input type="hidden" id="isDelete" value="0">
  <div class="modal fade" id="modal-delete-task<?php echo $taskMaintenace->taskId; ?>" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content deletemodaltask modaldeleteStyle">
        <div class="modal-header modalHeaderdeleteStyle">
           <input type="hidden" name="taskId" id="taskId<?php echo $taskMaintenace->taskId; ?>" value="<?php echo $taskMaintenace->taskId; ?>" /> 
            <h4 class="modal-title modalHeaderStyle"><?php echo Delete; ?> </h4>
            <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
              <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
            </button>
          </div> 
          <div class="modal-body">
            <form class="p-b-20" action="delete_task_form" method="POST" id="delete_task_form" >
              <input type="hidden" name="deleteTaskId" id="deleteTaskId" /> 
              <div class="modal-footer modalFooterStyle">
                <div class="row">
                    <div class="col-md-12 modalcolmd12Style">
                      <center>
                        <img class="modalDeleteImgStyle" src="<?php echo base_url().'assets/img/delete_gray.svg'?>">
                      </center>
                    </div>
                    <br>
                    <div class="m-b-10 alert alert-success fade hide" ></div>
                  <div class="col-md-12" style="margin-top: -5px;">
                    <center>
                      <?php echo AreyousureyouwanttodeleteTask; ?> 
                    </center>
                  </div>
                    <div class="col-md-12">
                      <center>
                        <br>
                          <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_task_submit"><?php echo Delete; ?></button>
                        <br>
                      </center>
                    </div>
                </div>          
            </div>
            </form>
        </div>
      </div>
    </div>
  </div>
  <style>
    .addtaskmodalh4style
    {
      color: #FF8000!important;padding-left: 15px!important;
    }

    .dropdown-toggle:after {
        vertical-align: 1px;
        border-width: 4px;
        float: right;
        margin-top: 6px;
    }
  </style>


   <style>
  .colmdCarouselmarginstyle
  {
    margin-top:10px!important;
  }
  </style>
    
  <div class="modal fade" id="modal-add-task" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
            <h4 class="modal-title machine_name addtaskmodalh4style" id="liveMachineName"><?php echo Addnewtask; ?></h4>
            <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
              <img width="16"height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
            </button>
        </div>
        <div class="modal-body">
          <form class="p-b-20" action="add_task" method="POST" id="add_task_form" style="margin-left: 10px;margin-right: 10px;">
            <div class="row" style="margin-left: 0px;margin-right: 0px;">
              <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name"  >
                <input type="text" style="padding: 0px 12px 16px !important;height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="task" name="task" 
                placeholder="<?php echo Enternewtask; ?>" required> 
              </div>
              
              <div style="padding: 15px 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Machine Type" >
                <label><?php echo Repeatthetask; ?></label>
                <div class="dropdown colmd3SpRepeatTaskStyle">
                      
                      <div class="dropdown-toggle" data-toggle="dropdown" id="selectedRepeat"> <?php echo Selectanoption; ?> </div>
                      <div class="dropdown-menu" style="width: 100%;margin-left: -8px;margin-top: 17px;">
                        <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeat('never');">
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Never; ?>
                          <hr class="sidepanelrepeattaskstyle">
                        </div>

                        <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeat('everyDay');">
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everyday; ?>
                          <hr class="sidepanelrepeattaskstyle">
                        </div>

                        <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeat('everyWeek');">
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everyweek; ?>
                          <hr class="sidepanelrepeattaskstyle">
                        </div>

                        <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeat('everyMonth');">
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everymonth; ?>
                          <hr class="sidepanelrepeattaskstyle">
                        </div>
                        
                        <div class="dropdown-item dropdownrepeatitemstyle" onclick="selectRepeat('everyYear');">
                            &nbsp;&nbsp;&nbsp;&nbsp;<?php echo Everyyear; ?>
                        </div>
                        <input type="hidden" id="repeat" name="repeat">
                      </div>
                    </div>
              </div>

              <div style="padding: 15px 5px;" class="form-group col-md-6" data-toggle="tooltip" data-title="Machine Automation">
                <label><?php echo Selectmachine; ?></label>
                <select style="min-width:100%;width:100%;" class="form-control js-example-basic-multiple" multiple="multiple" id="machineId" name="machineId[]" >
                    <?php foreach ($machines as $key => $value) { ?>
                    <option value="<?php echo $value['machineId']; ?>"><?php echo $value['machineName']; ?></option>
                    <?php } ?>
                </select>
              </div>
            </div>

 
            <div class="col-md-12" style="margin-left: -20px;">
              <div>
              <label style="font-weight: 600;padding-left: 15px;"><?php echo Dueon; ?></label><br>
              <div class="row col-md-12">
                <div class="col-md-2 monthSelect" style="display: none;">
                  <div class="form-check">
                    <input class="form-check-input" type="radio" value="1" onclick="checkmonthDueOn(this.value)" name="monthDueOn" id="monthDueOn1" checked>
                    <label class="form-check-label" for="monthDueOn1">
                      <!-- <?php echo Onday; ?>  -->
                    </label>
                  </div>
                </div>
                <div class="col-md-10 monthSelect" style="display: none;">
                  <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthMonth" id="dueMonthMonth">
                    <option selected="" disabled=""><?php echo Selectmonthday; ?></option>
                    <option value="1">1</option>
                    <option value="2">2</option>
                    <option value="3">3</option>
                    <option value="4">4</option>
                    <option value="5">5</option>
                    <option value="6">6</option>
                    <option value="7">7</option>
                    <option value="8">8</option>
                    <option value="9">9</option>
                    <option value="10">10</option>
                    <option value="11">11</option>
                    <option value="12">12</option>
                    <option value="13">13</option>
                    <option value="14">14</option>
                    <option value="15">15</option>
                    <option value="16">16</option>
                    <option value="17">17</option>
                    <option value="18">18</option>
                    <option value="19">19</option>
                    <option value="20">20</option>
                    <option value="21">21</option>
                    <option value="22">22</option>
                    <option value="23">23</option>
                    <option value="24">24</option>
                    <option value="25">25</option>
                    <option value="26">26</option>
                    <option value="27">27</option>
                    <option value="28">28</option>
                    <option value="third_last_day"><?php echo Thirdlastday; ?></option>
                    <option value="second_last_day"><?php echo Secondlastday; ?></option>
                    <option value="last_day"><?php echo Lastday;?></option>
                  </select> 
                </div>
              </div>

               <div class="col-md-6 yearSelect" style="display: none;">
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="1" onclick="checkyearDueOn(this.value)" name="yearDueOn" id="yearDueOn1" checked>
                  <label class="form-check-label" for="yearDueOn1">
                    <!-- <?php echo On; ?>  -->
                  </label>
                </div>
              </div>
              <div class="col-md-12">
                <div class="row">
                  <div class="col-md-6 yearSelect" style="display: none;">
                    <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonth" id="dueYearMonth">
                      <option selected="" disabled=""><?php echo Selectmonth; ?></option>
                      <option value="January"><?php echo January; ?></option>
                      <option value="February"><?php echo February; ?></option>
                      <option value="March"><?php echo March; ?></option>
                      <option value="April"><?php echo April; ?></option>
                      <option value="May"><?php echo May; ?></option>
                      <option value="June"><?php echo June; ?></option>
                      <option value="July"><?php echo July; ?></option>
                      <option value="August"><?php echo August; ?></option>
                      <option value="September"><?php echo September; ?></option>
                      <option value="October"><?php echo October; ?></option>
                      <option value="November"><?php echo November; ?></option>
                      <option value="December"><?php echo December; ?></option>
                    </select> 
                  </div>

                  <div class="col-md-6 yearSelect" style="display: none;">
                    <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonthDay" id="dueYearMonthDay">
                      <option selected="" disabled=""><?php echo Selectmonthday; ?></option>
                      <option value="1">1</option>
                      <option value="2">2</option>
                      <option value="3">3</option>
                      <option value="4">4</option>
                      <option value="5">5</option>
                      <option value="6">6</option>
                      <option value="7">7</option>
                      <option value="8">8</option>
                      <option value="9">9</option>
                      <option value="10">10</option>
                      <option value="11">11</option>
                      <option value="12">12</option>
                      <option value="13">13</option>
                      <option value="14">14</option>
                      <option value="15">15</option>
                      <option value="16">16</option>
                      <option value="17">17</option>
                      <option value="18">18</option>
                      <option value="19">19</option>
                      <option value="20">20</option>
                      <option value="21">21</option>
                      <option value="22">22</option>
                      <option value="23">23</option>
                      <option value="24">24</option>
                      <option value="25">25</option>
                      <option value="26">26</option>
                      <option value="27">27</option>
                      <option value="28">28</option>
                      <option value="third_last_day"><?php echo Thirdlastday; ?></option>
                      <option value="second_last_day"><?php echo Secondlastday; ?></option>
                      <option value="last_day"><?php echo Lastday; ?></option>
                    </select> 
                  </div>
                </div>
              </div>

              <div class="col-md-6 dueSelect" style="padding: 0 15px;">
                <div class="form-group sidepanelduedatestyle" data-toggle="tooltip" data-title="Name">
                 <input style="border: none;color: #b3acac;width: 100%;" type="text"  class="datepicker58" id="dueDate" name="dueDate" value="<?php echo date('m/d/Y'); ?>" > 
                </div>
              </div>

              <div class="col-md-6 weekSelect" style="padding: 0 15px;display: none;">
                <select  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueWeekDay">
                    <option selected="" disabled=""><?php echo Selectday; ?></option>
                    <option value="Monday"><?php echo Monday; ?></option>
                    <option value="Tuesday"><?php echo Tuesday; ?></option>
                    <option value="Wednesday"><?php echo Wednesday; ?></option>
                    <option value="Thursday"><?php echo Thursday; ?></option>
                    <option value="Friday"><?php echo Friday; ?></option>
                    <option value="Saturday"><?php echo Saturday; ?></option>
                    <option value="Sundays"><?php echo Sunday; ?></option>
                </select>
              </div>

            </div>

            <div class="row monthSelect" style="margin-top: 10px;display: none;">
              <div class="col-md-12"></div>

              <div class="p-b-10" style="padding-left: 22px;">
                <div class="form-check">
                  <input class="form-check-input" type="radio" value="2" onclick="checkmonthDueOn(this.value)" name="monthDueOn" id="monthDueOn2">
                  <label class="form-check-label" for="monthDueOn2">
                    <!-- <?php echo Onthe; ?> -->
                  </label>
                </div>
              </div>

              <div class="col-md-5 m-t-10" style="padding-left: 22px;">
                  <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthWeek" id="dueMonthWeek">
                    <option selected="" disabled=""><?php echo Selectweek; ?></option>
                    <option value="1"><?php echo First; ?></option>
                    <option value="2"><?php echo Second; ?></option>
                    <option value="3"><?php echo Third; ?></option>
                    <option value="4"><?php echo Fourth; ?></option>
                    <option value="5"><?php echo Last; ?></option>
                  </select> 
              </div>

              <div class="col-md-5 m-t-10" style="padding-left: 22px;">
                  <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueMonthDay" id="dueMonthDay">
                    <option selected="" disabled=""><?php echo Selectday; ?></option>
                    <option value="Monday"><?php echo Monday;?></option>
                    <option value="Tuesday"><?php echo Tuesday; ?></option>
                    <option value="Wednesday"><?php echo Wednesday; ;?></option>
                    <option value="Thursday"><?php echo Thursday; ?></option>
                    <option value="Friday"><?php echo Friday; ?></option>
                    <option value="Saturday"><?php echo Saturday; ?></option>
                    <option value="Sundays"><?php echo Sunday; ?></option>
                  </select> 
              </div>
            </div>

            <div class="row yearSelect" style="margin-top: 10px;display: none;">
              <div class="col-md-12"></div>

              <div class="row">

                <div class="col-md-12 m-l-10">
                 
                </div>

                <div class="col-md-12 m-l-10">
                  <div class="row col-md-12">
                    <div class="col-md-1 m-t-10">
                      <div class="form-check">
                        <input class="form-check-input" type="radio" value="2" onclick="checkyearDueOn(this.value)" name="yearDueOn" id="yearDueOn2">
                        <label class="form-check-label" for="yearDueOn2">
                          <!-- <?php echo Onthe; ?> -->
                        </label>
                      </div>
                    </div>
                    <div class="col-md-5 m-t-10">
                        <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearWeek" id="dueYearWeek">
                          <option selected="" disabled=""><?php echo Selectweek; ?></option>
                          <option value="1"><?php echo First; ?></option>
                          <option value="2"><?php echo Second; ?></option>
                          <option value="3"><?php echo Third; ?></option>
                          <option value="4"><?php echo Fourth; ?></option>
                          <option value="5"><?php echo Last; ?></option>
                        </select> 
                    </div>
                    
                    <div class="col-md-6 m-t-10" style="display: contents;">
                      <div class="col-md-6 daysStyle m-t-10">
                        <select disabled  style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearDay" id="dueYearDay">
                          <option selected="" disabled=""><?php echo Selectday; ?></option>
                          <option value="Monday"><?php echo Monday; ?></option>
                          <option value="Tuesday"><?php echo Tuesday; ?></option>
                          <option value="Wednesday"><?php echo Wednesday; ?></option>
                          <option value="Thursday"><?php echo Thursday; ?></option>
                          <option value="Friday"><?php echo Friday; ?></option>
                          <option value="Saturday"><?php echo Saturday; ?></option>
                          <option value="Sundays"><?php echo Sunday; ?></option>
                        </select> 
                      </div>
                    </div>
                    
                    <div class="col-md-6 m-t-10">
                      <select disabled style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="dueYearMonthOnThe" id="dueYearMonthOnThe">
                        <option selected="" disabled=""><?php echo Selectmonth; ?></option>
                        <option value="January"><?php echo January; ?></option>
                        <option value="February"><?php echo February; ?></option>
                        <option value="March"><?php echo March; ?></option>
                        <option value="April"><?php echo April; ?></option>
                        <option value="May"><?php echo May; ?></option>
                        <option value="June"><?php echo June; ?></option>
                        <option value="July"><?php echo July; ?></option>
                        <option value="August"><?php echo August; ?></option>
                        <option value="September"><?php echo September; ?></option>
                        <option value="October"><?php echo October; ?></option>
                        <option value="November"><?php echo November; ?></option>
                        <option value="December"><?php echo December; ?></option>
                      </select> 
                    </div>
                  </div>
                </div>
              </div>
            </div>

          <style type="text/css">
            input:disabled {
                background-color: #eee !important;
                border-color: #eee !important;
            }
          </style>
            
            <label class="m-t-10" style="font-weight: 600;margin-left: 15px;mar"><?php echo Endtask; ?></label>
              <div class="col-md-12">
                <div class="row">
                 <div class="col-md-6">
                  <div class="form-group sidepanelduedatestyle" data-toggle="tooltip" data-title="Name"  >
                   <input style="border: none;color: #b3acac;width: 95%;" type="text"  class="datepicker59" onchange="selectEndTask(this.value)" id="endTaskDate" name="endTaskDate" placeholder="<?php echo Endtask; ?>"> 
                   <span onclick="removeEndTask()" id="removeEndTaskText" style="display: none;cursor: pointer;float: right;margin-top: -18px;">
                    <i style="color: red;" class="fa fa-times"></i></span> 
                  </div>
                </div>

                <div style="font-weight: 600;" class="col-md-1 m-t-10">
                  <center><?php echo ortext;  ?></center> 
                </div> 

                <div class="col-md-5  " style="max-width: 180px;">
                  <div class="form-group" data-toggle="tooltip" data-title="Name"  >
                    <input type="text" class="form-control col-md-12 border-left-right-top-hide" onkeyup="selectEndAfteroccurances(this.value)" id="EndAfteroccurances" name="EndAfteroccurances" 
                    placeholder="<?php echo Enterendtaskafteroccurances; ?>" > 
                  </div>
                </div>
              </div>
            </div>
            <div class="row modalSPassignOpeRow">
              <div class="col-md-12">
                <label><b><?php echo Assignoperator; ?></b></label>
              </div>
            </div>
            <input type="hidden" name="userIds" id="userIds">
            <div class="row" style="margin-left: 0px;margin-right: 0px;">
              <div id="demo1" class="carousel slide" data-ride="carousel" data-interval="false" style="width: 100%;">
                <div class="carousel-inner no-padding"  style="display: flex!important;">
                  <input type="hidden" name="" id="machineIds">
                  <div class="carousel-item active">
                    <?php
                    $countQuery = 1;
                    $i = 1;
                    foreach ($users as $key => $value) { ?>
                    <?php if($countQuery % 4 == 0){ ?>
                  </div>
                  <div class="carousel-item">
                  <?php } ?>
                  <div class="col-md-3 colmdCarouselmarginstyle modalCarouselOpeStyle <?php echo $comparValue; ?>" id="userIdBlock<?php echo $value['userId']; ?>" onclick="selectUsers(<?php echo $value['userId']; ?>)">
                    <img class="modalCarouselOpeImageStyle" style="width:55px!important;height: 55px!important;" src="<?php echo base_url('assets/img/user/').$value['userImage']; ?>">
                    <h6 style="margin-top: 10px;"><?php echo $value['userName']; ?></h6>
                  </div>
                  <?php  $i++; if ($i != 4) { $countQuery++; } } ?>
                </div>
              </div>
              <a class="carousel-control-prev carouselbuttonprevaddtaskstyle"  href="#demo1" role="button" data-slide="prev">
                  <span class="carousel-control-prev-icon carouselbuttonprevaddtaskspanstyle">
                    <i class="fa fa-angle-left carouselbuttonimgstyle" aria-hidden="true"></i></span>
              </a>
              <a class="carousel-control-next carouselnextmachinestyle" href="#demo1" role="button" data-slide="next">
                  <span class="carousel-control-next-icon carouselnextspanstyle">
                    <i class="fa fa-angle-right carouselbuttonimgnextstyle" aria-hidden="true"></i></span>
                </a>
            </div>
          </div>
          <div class="row" style="margin-left: 0px;margin-right: 0px;">
            <div class="col-md-12">
              <br>
              <center>
                <button type="submit" class="btn btn-primary m-r-5 addtaskbuttonstyle" id="add_task_submit" >
                  <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addtask; ?></button>
              </center>
            </div>
          </div>
        </form>
      </div>
      </div>
    </div>
  </div>





  


