<link href="<?php echo base_url('assets/css/app_versions.css');?>" rel="stylesheet" />	
<style>
	/*a{
	color: black!important;
	}*/
</style>
		<div id="content" class="content">
			
			
			<h1 class="page-header" style="color: #002060!important;"><?php echo Appversions; ?></h1>
			<?php ?>
			<div class="row">
			<?php if ($accessPointEdit == true) { ?>
				<a href="#modal-add-appVersion" class="m-t-15 m-b-15 m-l-5 btn btn-white boxShadow" data-toggle="modal" >
					<i class="fa fa-fw fa-plus"></i> <?php echo Addappversions; ?>
				</a>
			<?php } ?>



			<div class="modal fade" id="modal-add-appVersion" style="display: none;" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color: #002060;">
							<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Addappversions; ?></h4>
							<button type="button" class="close" style="padding: 12px 32px !important;" data-dismiss="modal">
								<img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
							</button>
						</div>

						<div class="modal-body">
							<div id="add_appVersion_error" class="m-b-10 m-r-10 alert alert-danger fade hide"></div>
							<div id="add_appVersion_success" class="m-b-10 m-r-10 alert alert-success fade hide" ></div>
							<form class="p-b-20" action="" method="POST" id="add_appVersion_form" enctype="multipart/form-data" > 
								<div class="row">
									<div class="form-group col-md-12" data-toggle="tooltip" data-title="Version name" >
										<label><?php echo Versionname; ?></label>
										<input type="text" class="form-control" id="versionName" name="versionName" placeholder=" 
										<?php echo Enterversionname; ?>" required > 
									</div>
									<div class="form-group col-md-12" data-toggle="tooltip" data-title="Version code" >
										<label><?php echo Versioncode; ?></label>
										<input type="text" class="form-control" id="versionCode" name="versionCode" placeholder="<?php echo Enterversioncode; ?>" required > 
									</div>
									<div class="form-group col-md-12">
										<label><?php echo Versionfile; ?></label>
										<input type="file" class="form-control"  name="versionFile" required>
									</div>
								</div>
								<center><button type="submit" class="btn btn-sm btn-primary m-r-5" id="add_appVersion_submit"><?php echo save; ?></button></center>
							</form>
						</div>
					</div>
				</div>
			</div>

			<div class="modal fade" id="modal-update-appVersion" style="display: none;" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h4 class="modal-title m-b-10"><?php echo Updateappversionsfile; ?></h4>
							<div id="update_appVersion_error" class="m-b-10 m-r-10 alert alert-danger fade hide"></div>
							<div id="update_appVersion_success" class="m-b-10 m-r-10 alert alert-success fade hide" ></div>
							<form class="p-b-20" action="" method="POST" id="update_appVersion_form" enctype="multipart/form-data" > 
								<div class="row">
									<input type="hidden" name="app_version_id" id="app_version_id">
									<div class="form-group col-md-12">
										<label><?php echo Versionfile; ?></label>
										<input type="file" class="form-control"  name="versionFile" required>
									</div>
								</div>
								<center><button type="submit" class="btn btn-sm btn-primary m-r-5" id="update_appVersion_submit"><?php echo Save; ?></button></center>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<div class="col-md-12" >
				  <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;" >
						<div class="panel-body">
							<table id="empTable" class="display table table-bordered m-b-0" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th><?php echo Downloadversionfile; ?></th>
										<th><?php echo Versionname; ?></th>
										<th><?php echo Versioncode; ?></th>
										<th><?php echo Uploadeddate; ?></th>
										<?php if ($accessPointEdit == true) { ?>
										<th><?php echo Updatefile; ?></th>
										<?php } ?>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div> 
			</div>
			<hr style="background: gray;">
	<p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
			<i class="fa fa-angle-up"></i></a>
	</div>
	

