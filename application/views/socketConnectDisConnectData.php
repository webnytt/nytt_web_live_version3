<link href="<?php echo base_url('assets/css/taskMaintenance.css');?>" rel="stylesheet" />
<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Socketlogdata; ?></h1>
	<input type="hidden" name="notificationId" value="0">
	<div class="row">
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  <div class="panel panel-inverse panel-primary boxShadow" >
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="TableHeaderTHstyle">
				                  <div id="filterMachineIdSelected" class="dropdown DropDownFilterStyle">
				                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterMachineIdSelectedValue"> <?php echo Machine; ?>&nbsp;</span>
				                    <div class="dropdown-menu">
				                      <?php foreach ($machines as $key => $value) { ?>
				                        <div class="dropdown-item">
				                          <div class="form-check formCheckWidth" >
				                            <input onclick="filterMachineId()" class="form-check-input filterMachineId" type="checkbox" id="filterMachineId<?php echo $key; ?>" 
				                             name="filterMachineId" value="<?php echo $value['machineId']; ?>">
				                            <label class="form-check-label" for="filterMachineId<?php echo $key; ?>">
				                              <?php echo $value['machineName']; ?>
				                            </label>
				                          </div>
				                        </div>
				                      <?php } ?>
				                    </div>
				                  </div>
				                  <small class="tableColumnsmallStyle">&nbsp;</small>
				                </th>
								<th  class="TableHeaderTHstyle">
				                  <input type="hidden" name="dateValS" id="dateValS" />  
				                    <input type="hidden" name="dateValE" id="dateValE" />   
				                      <div id="advance-daterange" name="advance-daterange" class="daterangeandduedatestyle">
				                        <span>
				                            <?php echo Starttime; ?>&nbsp;&nbsp;
				                        </span> 
				                       <i class="fa fa-caret-down m-t-2"></i>
				                  </div>
				                  <small  class="tableColumnsmallStyle">&nbsp;</small>
				                </th>
								 <th class="TableHeaderTHstyle">
				                  <input type="hidden" name="dueDateValS" id="dueDateValS" />  
				                  <input type="hidden" name="dueDateValE" id="dueDateValE" />   
				                  <div id="due-daterange" name="due-daterange" class="daterangeandduedatestyle"><span><?php echo Endtime; ?>&nbsp;&nbsp;</span> 
				                    <i class="fa fa-caret-down m-t-2"></i>
				                  </div>
				                  <small  class="tableColumnsmallStyle">&nbsp;</small>
				                </th>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<div style="padding-bottom: 21px;"><?php echo Reason; ?></div>
								</th>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<div style="padding-bottom: 21px;"><?php echo Reasontext; ?></div>
								</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div> 
	</div>

	<hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

