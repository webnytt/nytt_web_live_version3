<link href="<?php echo base_url('assets/css/operator_new_design.css');?>" rel="stylesheet" />
<style>
	.searchIcon
	{
		background-image: url('https://nyttdev.com/testing_env/version4/assets/img/search.png');
	}
	.gritter-item
	{
		color:white!important;
	}
	body #gritter-notice-wrapper {
    	z-index: 2000!important;
	}
</style>
	<div id="content" class="content" style="">
		
			<h1 class="page-header" style="color: #002060;"><?php echo Operators; ?></h1>
			<a class="btn btn-primary" href="<?php echo base_url('Operator/operator_checkin_checkout');?>"><img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/report.svg'); ?>"> <?php echo Operatorreports; ?></a>
			<br><br>
			<div class="row">
				<?php if($this->session->flashdata('error_message') != '' ) { ?>
				<div class="col-md-12">
					<div class="alert alert-danger fade show m-b-10">
						<span class="close" data-dismiss="alert">×</span>
						<?php echo $this->session->flashdata('error_message'); ?>
					</div>
				</div>
				<?php } ?> 
				
				<?php if($this->session->flashdata('success_message') != '' ) { ?>
				<div class="col-md-12">
					<div class="alert alert-success fade show m-b-10">
						<span class="close" data-dismiss="alert">×</span>
						<?php echo $this->session->flashdata('success_message'); ?>
					</div>
				</div>
				<?php } ?>
			</div>
			
			<?php ?>
			
			<div class=" row ">
				<?php foreach($listOperators as $user) { ?>
					<?php if($user->userImage == '') {
						$imgUrl = base_url().'assets/img/user/default.jpg';  
					} else {
						$imgUrl = base_url().'assets/img/user/'.$user->userImage;   
					}?>
					<div class="col-lg-4">
						<div class="card" style="border-radius: 8px;">
							<div class="card-img-overlay boxShadow" style="border-radius: 5px;">
								<div class="card-content">
										
									<div class="row">
										<div class="col-md-12" style="width: 100%;">
										
											<?php if($user->isOnline == "1")
											{ 
												$onlineColor = "#76BA1B";
											}else
											{ 
												$onlineColor = "#F60100";
											} ?>
										
											<div class='icon-container firstDiv' style="float: left;margin-right: 22px;">
											  <div><img style="height: 60px;width: 60px;" id="userImageText<?php echo $user->userId; ?>" class="rounded-corner" src="<?php echo $imgUrl; ?>" /></div>
											  <div class='status-circle' style="background-color: <?php echo $onlineColor; ?>">
											  </div>
											</div>
											<div class="secondDiv" style="float: left;">
												<h4 id="userNameText<?php echo $user->userId; ?>" class="userNameTextstyle"><?php echo $user->userName; ?></h4>
												<input type="password" disabled class="passwordIcon passWordfielsStyle" id="currentPassword<?php echo $user->userId; ?>" name="currentPassword" value="<?php echo base64_decode($user->userPassword);?>"  />
												
											    <span id="iconcurrentPassword<?php echo $user->userId; ?>" style="background-color: white !important;">
											    	<i onclick="showPassword('currentPassword<?php echo $user->userId; ?>','text');" style="margin-top: -5px;cursor:pointer;" class="fa fa-eye-slash"></i>
											    </span>
											</div>

											<div class="thirdDiv mobilesizeeditbutton" style="float: left;">
												<div class="thirdDivButton" style="margin-left: 26px;margin-top: 12px;">
													<a href="#modal-edit-user<?php echo $user->userId; ?>" data-toggle="modal"> 
														<img class="mobilesizeeditbutton ImageCreatestylee" src="<?php echo base_url('assets/nav_bar/create.svg');?>">
													</a>
													<a style ="margin-left: 10px;"class="text-primary" href="#modal-delete-user<?php echo $user->userId; ?>" data-toggle="modal" >
														<img class="mobiledeletebutton ImageDeletestylee" src="<?php echo base_url('assets/img/DeleteIcon.png'); ?>">
													</a>
												</div>
											</div>
										</div>
									</div>

									<div class="row" style="margin-top: 20px;">
										<div class="col-md-12" style="">
											<label style="font-weight: 700;"><?php echo Assingnmachines; ?>:</label>	
										</div>
									</div>
									<div class="row" style="margin-left: 0px;margin-right: 0px;height: 66px;overflow: auto;">
										
										<a href="#modal-selectmachine<?php echo $user->userId; ?>" data-toggle="modal" style="" class="btn btn-primary blueButton AssignMachineModalStyle"><?php echo Addmachine; ?> <i style="font-size: 9px;" class="fa fa-plus"></i></a>

										<?php $machines = explode(",", $user->machines); ?>
										<?php foreach($listMachines->result() as $machine) { 
										 if( is_array($machines) && in_array($machine->machineId, $machines) ) {  ?>
											
												<button id="bluebuttonMachineNamestyle" class="blueButton btn btn-primary "><?php echo $machine->machineName; ?></button>
											
										<?php } } ?>
									</div>
									
									<div class="row" style="margin-top: 25px;">
										<div class="col-md-12" style="">
											<label style="font-weight: 700;"><?php echo Machinesworkingon ;?>:</label>	
										</div>
									</div>

									<div class="row" style="margin-left: 0px;margin-right: 0px;height: 68px;overflow: auto;">
										<?php if ($user->isOnline == "1" && !empty($user->workingMachine)) { ?>
										<?php foreach($user->workingMachine as $workingMachineValue) {  ?>
											<button id="bluebuttonMachineNamestyle" class="blueButton btn btn-primary"><?php echo $workingMachineValue['machineName']; ?></button>
										<?php  } ?>
										<?php }  ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				<?php } ?> 
				<div class="col-lg-4 col-md-4">
					<div class="card text-center addNewModalOperatorCardStyle">
						<center>
							<a href="#modal_addnewOperator" data-toggle="modal" class="btn btn-primary AddnewModalOperatorStyle">
								<i style="font-size: 27px;" class="fa fa-plus"></i></a>	
						</center>
					</div>
				</div>
			</div>
		</div>

		<?php foreach($listOperators as $user) { ?>
			<div class="modal fade " id="modal-selectmachine<?php echo $user->userId   ?>" style="display: none;" aria-hidden="true">
				<div class="modal-dialog listOperatorModalDialogStyle">
					<div class="modal-content " >
						<div class="modal-header" style="background-color: #002060;">
							<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Assignmachines; ?></h4>
							<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
						</div>
						<div class="modal-body">
							<div class="col-md-12" style="max-width:100%;padding: 0;margin-bottom: 20px;">
								<form method="post" action="assign_form"  id="assign_form<?php echo $user->userId; ?>" >
									<input type="hidden" name="userId" value="<?php echo $user->userId; ?>">
									<?php $machines = explode(",", $user->machines); ?>
									<?php $workingMachine = !empty($user->workingMachine) ? array_column($user->workingMachine,"machineId") : array(0); ?>

									<?php if ($user->isOnline == "1" && !empty($user->workingMachine)) { ?>

									<label>Machines working on:</label><br>

											<span style="padding-left: 15px;">
										<?php foreach($user->workingMachine as $workingMachineValue) {  ?>
												<button type="button" style="margin-bottom: 5px;border-radius: 18px;background: #002060 !important;    border-color: #002060;padding: 3px 12px;height: 28px;margin-right: 5px;" class="blueButton btn btn-primary"><?php echo $workingMachineValue['machineName']; ?></button>
											<input type="hidden" name="userMachine[]" value="<?php echo $workingMachineValue['machineId']; ?>">
										<?php  } ?>
											</span>
										<br>
									<?php }  ?>

									<br>
									<div class="row">
										<div class="col-md-6">
											<label><?php echo Otherassignmachines; ?>:</label>
										</div>
									</div>
									<br>

									<select class="userMachineSel form-control col-md-12" style="border:5px solid black;margin-top: 5px;" multiple="multiple"  name="userMachine[]" >
										<?php foreach($listMachines->result() as $machine) { 
											if ($user->isOnline == "1" && !empty($user->workingMachine)) { 
											if (!in_array($machine->machineId, $workingMachine)) { ?>

											<option <?php if( is_array($machines) && in_array($machine->machineId, $machines) ) { echo "selected"; } ?> value="<?php echo $machine->machineId; ?>" ><?php echo $machine->machineName; ?></option>
										<?php } } else {  ?>
											<option <?php if( is_array($machines) && in_array($machine->machineId, $machines) ) { echo "selected"; } ?> value="<?php echo $machine->machineId; ?>" ><?php echo $machine->machineName; ?></option>

										<?php } } ?>
									</select>
									<center>
										<button type="submit" class="btn btn-sm btn-primary" style="margin-top: 20px;" id="assign_form_submit<?php echo $user->userId;?>"><?php echo Update; ?></button>
									</center>
								</form> 
							</div>
						</div> 
					</div>
				</div>
			</div>
		<div class="modal fade" id="modal-edit-user<?php echo $user->userId; ?>" style="display: none;" aria-hidden="true">
			<div class="modal-dialog EditModalDialogStylee">
				<div class="modal-content" >
					<div class="modal-header" style="background-color: #002060;">
						<h4 class="modal-title EditModaltitleStylee"><?php echo EditOperator; ?>  (<?php echo $user->userName; ?>)</h4>
						<button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
					</div>
						
					<div class="modal-body" >
						<form class="p-b-20" action="update_userOperator_form" method="POST" id="update_userOperator_form<?php  echo $user->userId; ?>">
							<input type="hidden" name="userId" value="<?php  echo $user->userId; ?>">
							<div class="row">
								<div class="col-md-6" >
									<ceter>
										<div class="form-group" data-toggle="tooltip" data-title="Picture" style="padding-left: 25px;padding-top: 20px;" >
											<?php if($user->userImage == '') {
												$imgUrl = base_url().'assets/img/user/default.jpg';  
											} else {
												$imgUrl = base_url().'assets/img/user/'.$user->userImage;   
											}?>
											<img class="file-upload-image-edit<?php echo $user->userId; ?>"	style="border-radius: 50%;border: 3px solid #002060;width: 130px;margin-left: 0px;height: 130px;"  src="<?php echo $imgUrl; ?>" alt="<?php echo $user->userName; ?>"> 
										
											<div class="upload-btn-wrapper editImage">
												<button class="btn1 pointer" type="button">
													<img style="width: 32px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
			  										<input name="userImage" title="Please select profile image" onchange="readURLEdit(this,<?php echo $user->userId; ?>);" type="file" accept="image/*" >
												</button>
			  								</div>	
		  								</div>
	  								<center>
								</div>

								<div class="col-md-6" style="padding-top: 50px;margin-top: -12px;">
									<h1 class="EditOperatotModalHeaderuserNameStyle"><?php echo Changeusername ;?></h1>
									<br>
									<center>
										<input type="text" style="" class="form-control col-md-12 border-left-right-top-hide userName EditOperatortextboxuserName" id="userName<?php echo $user->userId; ?>" name="userName" value="<?php echo set_value('userName', $user->userName);?>" placeholder="<?php echo Enterusername; ?>"  required > 
										<input type="hidden" name="olduserName" value="<?php echo $user->userName;?>"required > 
									</center>
								</div>
							</div>
							
							<div class="row">
								<div class="col-md-6">
									<h1 class="EditOperatotModalHeaderPassWordStyle"><?php echo Changepassword; ?></h1><br>
									<center>
										<input type="text" style="" class="form-control col-md-12 border-left-right-top-hide passwordIcon EditOperatortextboxChangePass" id="operatorNewPassword<?php echo $user->userId; ?>" name="operatorNewPassword" value="" placeholder="<?php echo Newpassword; ?>">
									</center>
								</div> 

								<div class="col-md-6">
									<br>
									<center>
										<input type="text" style="" class="form-control col-md-12 border-left-right-top-hide passwordIcon EditOperatortextboxConfirmPass" id="operatorConfirmPassword<?php echo $user->userId; ?>" name="operatorConfirmPassword" value="" placeholder="<?php echo Confirmpassword; ?>"> 
									</center>
								</div> 
							</div>
							<br>
							<br>
							<div class="text-center" style="width: 85%;	margin-bottom: 15px;">
									<button type="submit" class="btn btn-primary UpdateOperatorbuttonstyle" id="update_user_submit<?php echo $user->userId; ?>"><?php echo Save; ?></button>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
					

<div class="modal fade" id="modal-delete-user<?php echo $user->userId; ?>" style="display: none;" aria-hidden="true">
	<div class="modal-dialog">
		<div class="modal-content" style="">
			<div class="modal-header" style="background-color: #002060;">
				<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo delete; ?>  <?php echo $user->userName; ?></h4>
				<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
			</div>

			<div class="modal-body">
				<form class="p-b-20" action="delete_user" method="POST" id="delete_user_form<?php echo $user->userId; ?>" class="edit_user_form" >
					<input type="hidden" name="deleteuserId" id="deleteuserId<?php echo $user->userId; ?>" value="<?php echo $user->userId; ?>" /> 
					<div class="modal-footer" style="border-top: none;padding-top: 0;" >
						<div class="row">
							<div class="col-md-12" style="margin-top: -20px;">
								<center>
									<img style="width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
								</center>
							</div>
							<div class="col-md-12" style="">
								<center><?php echo Areyousureyouwanttodeletetheuser; ?></center>
							</div>

							<div class="col-md-12" style="">
		                        <center>
		                            <label><?php echo PleasetypeDELETEinbelowtextboxtoconfirmdeletionofOperator; ?></label><br>
		                            <input type="text" placeholder="<?php echo PleasetypeDELETE; ?>" id="checkKeywordUser<?php echo $user->userId; ?>" required>
		                        <center>
		                    </div>
							<div class="col-md-12" style="margin-top: 5px;">
								<center>
									<button type="submit" style="width:25%;" class="btn btn-danger" id="delete_user_submit<?php echo $user->userId; ?>"><?php echo Delete; ?></button>
								</center>
							</div>

						</div>					
					</div>
				</form>
			</div>
		</div>
	</div>
</div>
<?php } ?> 
</div>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

	<div class="modal fade" id="modal_addnewOperator" style="display: none;" aria-hidden="true">
		<div class="modal-dialog" style="min-width: 300px!important;border-top-left-radius: 25px;">
			<div class="modal-content" >
				<div class="modal-header" style="background-color: #002060;">
					<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Addnewoperator; ?></h4>
					<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
				</div>
				<div class="modal-body">
					<form class="p-b-20" action="" method="POST" id="add_operator_form" enctype="multipart/form-data" >
						<div class="row" style="margin-top: -10px;">
							<div class="col-md-6" >
								<div class="form-group" data-toggle="tooltip" data-title="Picture" style="padding-left: 25px;padding-top: 20px;" >
									<?php if($user->userImage == '') {
										$imgUrl = base_url().'assets/img/user/default.jpg';  
									} else {
										$imgUrl = base_url().'assets/img/user/'.$user->userImage;   
									}?>
									<div class="addOperatorModaluploadImage">
										<img style="margin-top: 35%;" class="file-upload-image"	style=""  src="<?php echo base_url().'assets/img/user-icon.png';?>" alt="<?php echo $user->userName; ?>"> 
									</div>

									<div class="upload-btn-wrapper editImage">
										<button class="btn1 pointer">
											<img style="width: 32px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
										<input name="userImage" title="Please select profile image" id="i_file" onchange="readURL(this);" type="file" accept="image/*" >
										</button>
									</div>
								</div>
							</div>

							<div class="col-md-6" style="">
								<h1 class="addoperatortextboxsize addOperatorUserNameLabel"><?php echo Enterusername; ?></h1>
								<br>
								<center>
									<input type="text" class="form-control col-md-12 border-left-right-top-hide userName addnewOperatornametextboxStyle" id="userName" name="userName" value="" placeholder="<?php echo Entername; ?>"  required > 
								</center>
							</div>
						</div>

						<div class="row">
							<div class="col-md-6">
								<br>
								<h1 class="addoperatortextboxsize" style="margin-left: 32px;"><?php echo Addpassword; ?></h1><br>
								<center>
									<input type="text" style="padding-bottom:12px!important; " class="form-control col-md-12 border-left-right-top-hide passwordIcon OperatorpasswordIconStyle" id="userPassword" name="userPassword" value="" placeholder="<?php echo Enterpassword; ?>" required >
								</center>
							</div> 

							<div class="col-md-6">
								<br>
								<center>
									<input type="text" style="padding-bottom:12px!important;" class="form-control col-md-12 border-left-right-top-hide passwordIcon OperatorpasswordConfirmIconStyle" id="userConfirmpassword" name="userConfirmpassword" value="" placeholder="<?php echo Confirmpassword; ?>" required > 
								</center>
							</div> 
						</div>
						<div class="row">

							<div class="col-md-12 addOperatorSelectMachine addoperatorSelectMachhinestyle">
								<input type="hidden" name="userId" value="" />
									<label for="userMachine[]" class="addoperatortextboxsize" style="margin-left: 28px;"><br><?php echo Assignmachines;?></label>
									<br >
									<center>
										<select style="margin-left:25px !important;" class="userMachineSel form-control col-md-12" multiple="multiple" id="userMachine<?php echo $user->userId; ?>" name="userMachine[]" >
											<?php foreach($listMachines->result() as $machine) { ?>

												<option value="<?php echo $machine->machineId; ?>" style="border:5px solid black !important;" ><?php echo $machine->machineName; ?></option>
											<?php } ?>
										</center>
									</select>
									<br>
							</div>
						</div>
						<br>
						<br>
						<center>
							<div class="text-center ">
								<button type="submit" id="submit_new_Operator" class="btn btn-sm btn-primary m-r-5 Addoperatorbuttonstyle" >
									<i style="font-size: 14px;" class="fa fa-plus"></i>  <?php echo Addoperator; ?></button>
							</div>
						</center>
						<br>
					</form>
				</div>
			</div>
		</div>
	</div>