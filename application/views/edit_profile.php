<link href="<?php echo base_url('assets/css/edit_profile.css');?>" rel="stylesheet" />
<style>
	input#userName 
		{
		    background-image: url("<?php echo base_url('../img/user.png'); ?>");
		    background-repeat: no-repeat;
		    text-indent: 35px;
		}
		input.passwordIcon 
		{
		    background-image: url("<?php echo base_url('../img/lock.png'); ?>");
		    background-repeat: no-repeat;
		}
</style>
<div id="content" class="content">
	<?php if(!$this->session->userdata('userImage')) 
	{ 
		    $imgUrl = base_url().'assets/img/user/default.jpg';  
	} else {
	    $imgUrl = base_url().'assets/img/user/'.$users->userImage;   
	}?>
	<h1 style="font-size: 22px;color: #002060;" class="page-header"><?php echo Profile; ?></h1>
	<div class="row" style="margin-top: 35px;">
		<div class="col-md-7 mainBox boxShadow" >
			<div class="panel panel-inverse panel-primary" >
				<div class="panel-body" style="padding: 0;">
					<form class="p-b-20" action="update_profile_form" method="POST" id="update_profile_form" >
					<div class="row blueCard">
						<div class="col-md-12" style="float: left">
							<img class="file-upload-image profileImage" style="border-radius: 50%;border: 5px solid #002060;width: 150px;margin-top: 38px;margin-left: 16px;height: 150px;" src="<?php echo $imgUrl; ?>" alt="administrator"> 
									<div class="imageText" style="font-size: 17px;color: #FF8000;font-weight: 700;display: inline-grid;padding-left: 9px;">
										<span style="font-size:17px;color: #FF8000;font-weight: 700;">
											<br><?php echo $users->userName; ?><br></span>
										<span style="color: #bec6ce;font-size: 10px;font-weight: 400;">
										<?php echo ($users->userRole == "2") ? 'Admin' : 'Factory manager'; ?></span>
									</div>
									<div class="upload-btn-wrapper editImage" style="width: 50px;">
										<button class="btn1 pointer" type="button">
											<img style="width: 38px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
			  								<input name="userImage" title="Please select profile image" id="i_file" onchange="readURL(this);" type="file" accept="image/*" >
										</button>
			  						</div>
						</div>
					</div>
					<div class="row" style="padding-top: 77px;">
						<input type="hidden" name="userId" id="userId" value="<?php echo $this->session->userdata('userId'); ?>">
						<input type="hidden" name="olduserName" id="olduserName" value="<?php echo $users->userName; ?>">
							<div class="col-md-6" style="padding-left: 45px;">
								<span style="font-weight: 600;"><?php echo Changeusername; ?></span>
								<div class="form-group" style="margin-top: 35px;" >
					              <input type="text" style="width: 67%;padding: 0px 12px 16px !important;height: 29px !important;" class="form-control garyColor col-md-12 border-left-right-top-hide" id="userName" name="userName" placeholder="<?php echo Enterusername; ?>" value="<?php echo $users->userName; ?>" required=""> 
					            </div>
							</div>

							<div class="col-md-6" style="padding-left: 45px;">
								<span style="font-weight: 600;"><?php echo Changepassword; ?></span>
								<div class="form-group input-group formgrpinputgrpstyle " style="">
					              <input maxlength="16" type="password" class="form-control garyColor col-md-12 passwordIcon border-left-right-top-hide passwordField" id="currentPassword" name="currentPassword" placeholder="<?php echo Currentpassword; ?>" >
					              <div class="input-group-append">
								    <span class="input-group-text iconeyeslashpassword" id="iconcurrentPassword" style="">
								    	<i onclick="showPassword('currentPassword','text');" style="margin-top: -5px;" class="fa fa-eye-slash"></i>
								    </span>
								  </div>
					            </div>

					            <div class="form-group input-group formgrpinputgrpstyle" style="">
					              <input maxlength="16" type="password" class="form-control garyColor col-md-12 passwordIcon border-left-right-top-hide passwordField" id="userNewPassword" name="userNewPassword" placeholder="<?php echo Newpassword; ?>" >
					              <div class="input-group-append">
								    <span class="input-group-text iconeyeslashpassword" id="iconuserNewPassword" style="">
								    	<i onclick="showPassword('userNewPassword','text');" style="margin-top: -5px;" class="fa fa-eye-slash"></i>
								    </span>
								  </div>
					            </div>

					            <div class="form-group input-group formgrpinputgrpstyle" style="">
					              <input maxlength="16" type="password" class="form-control garyColor col-md-12 passwordIcon border-left-right-top-hide passwordField" id="userConfirmPassword" name="userConfirmPassword" placeholder="<?php echo Confirmpassword; ?>" >
					              <div class="input-group-append">
								    <span class="input-group-text iconeyeslashpassword" id="iconuserConfirmPassword" style="">
								    	<i onclick="showPassword('userConfirmPassword','text');" style="margin-top: -5px;" class="fa fa-eye-slash"></i>
								    </span>
								  </div>
					            </div>
							</div>
						</div>

						<div class="row" style="margin-top: 10%;">
							<div class="col-md-6">
								<button id="update_profile_submit" type="submit" class="btn btn-primary updateButton"><?php echo Update; ?></button>
							</div>
							<div class="col-md-6">
								<center>
									<button style="background-color: white !important;border-color: white !important;color: black!important;" type="reset" class="btn btn-primary"><?php echo Cancel; ?></button>
								</center>
							</div>
						</div>
					</form>
				</div>
			</div>
		</div>
	</div>
	<hr style="background: gray;">
            <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

