<link href="<?php echo base_url('assets/css/taskMaintenance.css');?>" rel="stylesheet" />
<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Emailstatus;?></h1>
	<input type="hidden" name="notificationId" value="0">
	<div class="row">
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  <div class="panel panel-inverse panel-primary boxShadow" >
		  	<div class="row" style="float:left;margin: 10px;">
	          <span class="displayfilterrowstyle" id="showRepeat"></span>
	          <span class="displayfilterrowstyle" id="showAddeddDate"></span>
	        </div>
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr>
								<th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<div style="padding-bottom: 21px;"><?php echo Message; ?></div>
								</th>

								
								<th class="TableHeaderTHstyle">
				                    <div id="filterStatusSelected" class="dropdown DropDownFilterStyle">
				                      <span class="dropdown-toggle" data-toggle="dropdown"> <?php echo Status; ?>&nbsp;</span>
				                      <div class="dropdown-menu">
				                        <div class="dropdown-item">
				                          <div class="form-check formCheckWidth" >
				                            <input onclick="filterStatus()" value="'0'" class="form-check-input filterStatus" type="checkbox" id="filterStatusProcessed">
				                            <label class="form-check-label" for="filterStatusProcessed">
				                              <?php echo Processed; ?>
				                            </label>
				                          </div>
				                        </div>

				                        <div class="dropdown-item">
				                          <div class="form-check formCheckWidth" >
				                            <input onclick="filterStatus()" value="'1'" class="form-check-input filterStatus" type="checkbox" id="filterStatusDelivered">
				                            <label class="form-check-label" for="filterStatusDelivered">
				                              <?php echo Delivered; ?>
				                            </label>
				                          </div>
				                        </div>

				                        <div class="dropdown-item">
				                          <div class="form-check formCheckWidth" >
				                            <input onclick="filterStatus()" value="'2'" class="form-check-input filterStatus" type="checkbox" id="filterStatusDropped">
				                            <label class="form-check-label" for="filterStatusDropped">
				                              	<?php echo Dropped; ?>
				                            </label>
				                          </div>
				                        </div>
				                        
				                      </div>
				                    </div>
				                  <small class="tableColumnsmallStyle">&nbsp;</small>
				                </th>

				                <th style="color: #b8b0b0;padding-bottom: 0px !important;">
									<div style="padding-bottom: 21px;"><?php echo Failurereason; ?></div>
								</th>
								
								<th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;" >
				                  <input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-3000 day")); ?>" />  
				                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />   
				                    <div id="advance-daterange" name="advance-daterange" style="width: 87px;padding-bottom: 5px!important">
				                      <span>
				                          <?php echo Sentdate; ?>&nbsp;&nbsp;
				                      </span> 
				                     <i class="fa fa-caret-down m-t-2"></i>
				                    </div>
				                    <small  style="color: #FF8000;text-align : center;" >&nbsp;</small>
				                </th>

							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div> 
	</div>

	<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

