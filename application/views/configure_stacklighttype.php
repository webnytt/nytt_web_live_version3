<link href="<?php echo base_url('assets/css/configure_stacklighttype.css');?>" rel="stylesheet" />
<style>
.upload-btn-wrapper{position:relative;overflow:hidden;top:-39px;left:92px}.btn1{border:none;border-radius:50%;font-size:20px;font-weight:700;background:0 0}.upload-btn-wrapper input[type=file]{font-size:100px;position:absolute;left:0;top:0;opacity:0}

</style>
<script type="text/javascript">
	$(function () {
  		$('[data-toggle="tooltip"]').tooltip()
	})
</script>

	<div id="content" class="content">
			
			<h1 class="page-header" style="color: #002060!important;"><?php echo Stacklighttype; ?>
            	<br>
			<?php if($VersionList == true){ ?>
				<a class="btn btn-primary" style="margin-top: 10px;" href="<?php echo base_url('admin/configure_versions');?>">
					<img class="stakclightVerLisButton" src="<?php echo base_url('assets/nav_bar/report.svg'); ?>"> <?php echo Stacklightversionlist; ?></a>
			<?php } ?>
            	<br>
			</h1>
			
			<div class="row">
				<?php if($this->session->flashdata('error_message') != '' ) { ?>
				<div class="col-md-12">
					<div class="alert alert-danger fade show m-b-10">
						<span class="close" data-dismiss="alert">×</span>
						<?php echo $this->session->flashdata('error_message'); ?>
					</div>
				</div>
				<?php } ?> 
				<?php if($this->session->flashdata('success_message') != '' ) { ?>
				<div class="col-md-12">
					<div class="alert alert-success fade show m-b-10">
						<span class="close" data-dismiss="alert">×</span>
						<?php echo $this->session->flashdata('success_message'); ?>
					</div>
				</div>
				<?php } ?>
			</div>
			<div class="modal fade" id="modal-add-stackLightType" style="display: none;" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-header" style="background-color: #002060;">
							<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Addstacklighttype; ?></h4>
							<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
						</div>
						<div class="modal-body">
							<h4 class="modal-title m-b-10"></h4>
							<div id="add_stackLightType_error" class="m-b-10 m-r-10 alert alert-danger fade hide"></div>
							<div id="add_stackLightType_success" class="m-b-10 m-r-10 alert alert-success fade hide" ></div>
							<form class="p-b-20" action="" method="POST" id="add_stackLightType_form" enctype="multipart/form-data" > 
								<div class="row">
									<div class="form-group col-md-6" data-toggle="tooltip" data-title="Stack light type name" >
										<label><?php echo Name; ?></label>
										<input type="text" class="form-control" id="stackLightTypeName" name="stackLightTypeName" placeholder="<?php echo EnterstackLightTypename; ?>" required > 
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Image; ?></label>
										<input type="file" class="form-control" id="stackLightTypeImage" name="stackLightTypeImage" required>
									</div>
								
									<div class="form-group col-md-6">
										<label><?php echo Uploadfileclassificationcfg; ?></label>
										<input type="file" class="form-control col-md-12" id="classification_cfg" name="classification_cfg" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfileclassificationweights; ?></label>
										<input type="file" class="form-control col-md-12" id="classification_weights" name="classification_weights" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfiledetectioncfg;?></label>
										<input type="file" class="form-control col-md-12" id="detection_cfg" name="detection_cfg" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfiledetectionweights;?></label>
										<input type="file" class="form-control col-md-12" id="detection_weights" name="detection_weights" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfilelabeltxt; ?></label>
										<input type="file" class="form-control col-md-12" id="label_txt" name="label_txt" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfilelabelstxt; ?></label>
										<input type="file" class="form-control col-md-12" id="labels_txt" name="labels_txt" required >
									</div>
								</div>
								<center>
									<button type="submit" class="btn btn-sm btn-primary m-r-5" id="add_stackLightType_submit"><?php echo save; ?></button>
								</center>
							</form>
						</div>
					</div>
				</div>
			</div>
			
			<div id="gallery" class="gallery row ">
			<?php if(count($listTypes) <= 0 ) { ?> 
				<div class="col-md-12">
					<br /><br /><br /><br />
					<div class="note note-secondary note-with-left-icon">
					  <div class="note-icon"><i class="fa fa-exclamation-triangle"></i></div>
					  <div class="note-content">
						<h4><b><?php echo Nostacklighttypes; ?></b></h4>
						<p><?php echo NoStacklighttypeavailable; ?>. </p>
					  </div> 
					</div>
				</div>
			<?php } else {
				if ($stacklighttypeList == true) {

			 ?>
				
				<?php foreach($listTypes as $stackLightType) {  ?>
					<?php $log_url = '../assets/img/stackLightType/'.$stackLightType->stackLightTypeImage; ?> 
					<div class="col-lg-4 ">
					<div class="card boxShadow" >
						<div class="card-img-overlay" >
							<h4 class="card-title">
								<a href="<?php echo $log_url; ?>" data-lightbox="gallery-group-1">
									<img class="widget-img widget-img-sm rounded-corner" style="width: 50px!important;height:50px!important;" src="<?php echo $log_url; ?>" alt="..." />
								</a>
								
								<span class="text-primary configurestacklightnamewrap machineNameText" data-title="<?php echo $stackLightType->stackLightTypeName; ?>" style="cursor: pointer;" data-toggle="tooltip" >
									<?php echo $stackLightType->stackLightTypeName; ?></span>
								<div class="thirdDiv mobilesizeeditbutton" style="float: right;">

									<div class="thirdDivButton" style="">

										<?php if ($stacklighttypeListEdit == true) { ?>
										<a href="#modal-edit-stackLightType<?php echo $stackLightType->stackLightTypeId;?>" data-toggle="modal"> 
											<img class="mobilesizeeditbutton" style="width:22px;height: 22px;margin-left: 18px;" src="<?php echo base_url('assets/nav_bar/create.svg');?>">
										</a>
										<a style ="margin-left: 10px;"class="text-primary" href="#modal-delete-stackLightType<?php echo $stackLightType->stackLightTypeId; ?>" data-toggle="modal" >
											<img class="mobiledeletebutton" style="width:38px;height: 36px;" src="<?php echo base_url('assets/img/DeleteIcon.png'); ?>">
										</a>
									<?php } ?>
									</div>
								</div>
							</h4>
							<div class="card-content">
								<div class="clearfix"> 
									<div class="col-md-12" style="max-width:100%;padding: 0;">
										<form class="" id="update_stacklight_machine<?php echo $stackLightType->stackLightTypeId; ?>" >
										   <?php $machines = explode(",", $stackLightType->machines); ?>
												<input type="hidden" name="stackLightTypeId" value="<?php echo $stackLightType->stackLightTypeId; ?>" />
												<label for="stackLightTypeMachine[]" ><?php echo Machineshaving ?> <?php echo $stackLightType->stackLightTypeName; ?></label>
												<select class="stackLightTypeMachineSel form-control col-md-12" multiple="multiple" id="stackLightTypeMachine<?php echo $stackLightType->stackLightTypeId; ?>" name="stackLightTypeMachine[]" >
													<?php foreach($listMachines->result() as $machine) { ?>
														<option <?php if( is_array($machines) && in_array($machine->machineId, $machines) ) { echo "selected"; } ?> value="<?php echo $machine->machineId; ?>" ><?php echo $machine->machineName; ?></option>
													<?php } ?>
												</select>
												<button type="submit" class="btn btn-sm btn-primary offset-md-4 col-md-4 m-t-20" id="update_stacklight_machine_submit<?php echo $stackLightType->stackLightTypeId; ?>" >
													<?php echo Update; ?></button>
										</form> 
									</div> 
									
								</div>
							</div>
						</div>
					</div>
					
	<div class="modal fade" id="modal-edit-stackLightType<?php echo $stackLightType->stackLightTypeId; ?>" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header" style="background-color: #002060;">
					<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Edit; ?> (<?php echo $stackLightType->stackLightTypeName; ?>)</h4>
					<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
				</div>	
				<div class="modal-body">
					<h4 class="modal-title m-b-10"></h4> 
					<div id="edit_stackLightType_error<?php echo $stackLightType->stackLightTypeId; ?>" class="m-b-10 alert alert-danger fade hide"></div>
					<div id="edit_stackLightType_success<?php echo $stackLightType->stackLightTypeId; ?>" class="m-b-10 alert alert-success fade hide" ></div>
					<form class="p-b-20" action="edit_stackLightType" method="POST" id="edit_stackLightType_form<?php echo $stackLightType->stackLightTypeId; ?>" class="edit_stackLightType_form" enctype="multipart/form-data" >

					<div class="row">
						<div class="col-md-5">
							<div class="form-group  m-r-10" data-toggle="tooltip" data-title="Picture" >
								<?php if($stackLightType->stackLightTypeImage == '') {
									$imgUrl = base_url().'assets/img/stackLightType/default.jpg';  
								} else {
									$imgUrl = base_url().'assets/img/stackLightType/'.$stackLightType->stackLightTypeImage;   
								} ?> 
								<img class="file-upload-image" style="border-radius: 50%;border: 3px solid #002060;width: 130px;margin-left: 0px;height: 130px;" src="<?php echo $imgUrl; ?>" id="stackLightTypeImage<?php echo $stackLightType->stackLightTypeId; ?>" alt="<?php echo $stackLightType->stackLightTypeName; ?>">
								
								<div class="upload-btn-wrapper editImage">
									<button class="btn1 pointer" type="button">
										<img style="width: 32px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
											<input name="stackLightTypeImage" title="Please select Stacklight image" id="stackLightTypeImage<?php echo $stackLightType->stackLightTypeId; ?>"  onchange="" type="file" accept="image/*" >
									</button>
								</div>
							</div>
						</div>
						<div class="col-md-7">
							<input type="hidden" name="stackLightTypeId" id="stackLightTypeId<?php echo $stackLightType->stackLightTypeId; ?>" value="<?php echo $stackLightType->stackLightTypeId; ?>" /> 
							<h1 style="font-size: 12px;font-weight: 700;"><?php echo ChangeStacklighttypeName; ?></h1>
							<div class="form-group  m-r-10" data-toggle="tooltip" data-title="Stack light type Name" >
								<input type="text" class="form-control col-md-12" id="stackLightTypeName<?php echo $stackLightType->stackLightTypeId; ?>" name="stackLightTypeName" value="<?php echo set_value('stackLightTypeName', $stackLightType->stackLightTypeName);?>" placeholder="<?php echo EnterstackLightTypename; ?>" required > 
							</div>
						</div>
					</div>
						

						<div class="form-group m-r-10" data-toggle="tooltip" data-title="Select detection file version"  >
							<select style="min-width:100%;width:100%;" class="form-control select2 stackLightTypeVersion" id="stackLightTypeVersion<?php echo $stackLightType->stackLightTypeId; ?>" name="stackLightTypeVersion" placeholder="<?php echo Stacklighttypeversion; ?>" required >
									<option selected="" disabled=""><?php echo Selectversion; ?></option>
									<?php foreach ($stackLightType->versions as $key => $value) { ?>
										<option <?php if($stackLightType->stackLightTypeVersion == $value['versionName']){ echo "selected"; } ?> value="<?php echo $value['versionName'] ?>" >Version <?php echo $value['versionName'] ?></option>
									<?php } ?>
							</select>
						</div>
						<center>
							<button type="submit" class="btn btn-sm btn-primary m-r-5" id="edit_stackLightType_submit<?php echo $stackLightType->stackLightTypeId; ?>"><?php echo save; ?></button>
						</center>
					</form>
				</div>
			</div>
		</div>
	</div>

	<div class="modal fade" id="modal-delete-stackLightType<?php echo $stackLightType->stackLightTypeId; ?>" style="display: none;" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content" style="max-height: 240px;max-width: 100%;">
				<div class="modal-header" style="background-color: #002060;">
					<h4 style="color: #FF8000;padding: 4px;" class="modal-title"><?php echo Delete; ?> (<?php echo $stackLightType->stackLightTypeName; ?>)</h4>
					<button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
				</div>
				<div class="modal-body" style="">
					<form class="p-b-20" action="delete_stackLightType" method="POST" id="delete_stackLightType_form<?php echo $stackLightType->stackLightTypeId; ?>" class="edit_stackLightType_form" >
					<input type="hidden" name="deleteStackLightTypeId" id="deletestackLightTypeId<?php echo $stackLightType->stackLightTypeId; ?>" value="<?php echo $stackLightType->stackLightTypeId; ?>" /> 
						<div class="modal-footer" style="border-top: none;padding-top: 0;" >
							<div class="row">
						
								<div class="col-md-12" style="margin-top: 10px;" >
									<center>
										<img style="margin-top:-22px;width: 22%;" src="<?php echo base_url().'assets/img/delete_gray.svg'?>" style="">
									</center>
								</div>
						
							<div id="delete_stackLightType_success<?php echo $stackLightType->stackLightTypeId; ?>" class="m-b-10 alert alert-success fade hide" ></div>
							<div class="col-md-12">
								<center>
									<?php echo Areyousuretodeletestacklighttype; ?> "<?php echo $stackLightType->stackLightTypeName; ?>"?
								</center>
							</div>
							<div class="col-md-12" style="margin-top: 10px;">
								<center>
									<button type="submit" class="btn btn-danger" id="delete_stackLightType_submit<?php echo $stackLightType->stackLightTypeId; ?>"><?php echo Delete; ?></a>
								</center>
							</div>
						</div>
				</form>
			</div>
		</div>
			</div>
		</div>
	</div>
</div>
<?php } } ?> 
<?php } ?> 

<?php if ($stacklighttypeListEdit == true) { ?>

		<div class="col-lg-4 col-md-4">
			<div class="card text-center" style="min-height: 300px;background: none !important;">
				<center>
					<a href="#modal-add-stackLightType" data-toggle="modal"class="btn btn-primary addstakclightButton">
						<i style="font-size: 27px;" class="fa fa-plus"></i>
					</a>	
				</center>
			</div>
		</div>
<?php } ?>
		</div>
	

	<hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
	</div>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
