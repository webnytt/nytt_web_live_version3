	
		<div id="content" class="content">
			
			<h1 class="page-header" style="color:#002060;"><?php echo Configureversions; ?></h1>
			<small style="font-size: 12px !important;font-weight: 600 !important;padding-left: 2px;color: #002060;">
			      <a style="color: #002060 !important;" href="<?php echo base_url('admin/configure_stacklighttype');?>"> 
			      	<?php echo Stacklighttype; ?> </a> > <?php echo Stacklighttypeversion; ?>
			    </small>
			    <br>
			<?php if ($accessPointEdit == true) { ?>
				<a href="#modal-add-stackLightTypeVersion" class="m-t-15 m-b-15 m-l-5 btn btn-white boxShadow " data-toggle="modal" ><i class="fa fa-fw fa-plus"></i> <?php echo Addstacklightversion; ?></a>
			<?php } ?>
			<div class="modal fade" id="modal-add-stackLightTypeVersion" style="display: none;" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						<div class="modal-body">
							<h4 class="modal-title m-b-10"><?php echo Addstacklightversion; ?></h4>
							<div id="add_stackLightTypeVersion_error" class="m-b-10 m-r-10 alert alert-danger fade hide"></div>
							<div id="add_stackLightTypeVersion_success" class="m-b-10 m-r-10 alert alert-success fade hide" ></div>
							<form class="p-b-20" action="" method="POST" id="add_stackLightTypeVersion_form" enctype="multipart/form-data" > 
								<div class="row">
									<div class="form-group col-md-6" data-toggle="tooltip" data-title="<?php echo SelectstackLightType; ?>"  >
										<label><?php echo Stacklighttype; ?></label>
										<select style="min-width:100%;width:100%;display: block !important;" class="form-control select2 stackLightTypeVersion" id="stackLightTypeVersionId" name="stackLightTypeId" placeholder="Stacklight type version" required >
											<option selected="" disabled=""><?php echo Selectstacklighttype; ?></option>
											<?php foreach ($listStackLightTypes->result() as $key => $value) { ?>
												<option value="<?php echo $value->stackLightTypeId ?>" ><?php echo $value->stackLightTypeName ?></option>
											<?php } ?>
										</select>
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Version; ?></label>
										<input type="text" class="form-control col-md-12" id="StackversionName" name="versionName" required readonly>
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfileclassificationcfg; ?></label>
										<input type="file" class="form-control col-md-12" id="classification_cfg" name="classification_cfg" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfileclassificationweights; ?></label>
										<input type="file" class="form-control col-md-12" id="classification_weights" name="classification_weights" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfiledetectioncfg; ?></label>
										<input type="file" class="form-control col-md-12" id="detection_cfg" name="detection_cfg" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfiledetectionweights; ?></label>
										<input type="file" class="form-control col-md-12" id="detection_weights" name="detection_weights" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfilelabeltxt; ?></label>
										<input type="file" class="form-control col-md-12" id="label_txt" name="label_txt" required >
									</div>
									<div class="form-group col-md-6">
										<label><?php echo Uploadfilelabelstxt;?></label>
										<input type="file" class="form-control col-md-12" id="labels_txt" name="labels_txt" required >
									</div>
								</div>
								<center><button type="submit" class="btn btn-sm btn-primary m-r-5" id="add_stackLightTypeVersion_submit"><?php echo save; ?></button></center>
							</form>
						</div>
					</div>
				</div>
			</div>
			<?php  ?>
				<input type="hidden" name="stackLightTypeId" id="stackLightTypeId" value="<?php if($_POST) { echo $_POST['stackLightTypeId']; } else { echo 0; } ?>" /> 
				<div id="options" class="m-b-10">
					<span class="gallery-option-set text-center " id="filter" data-option-key="filter">
					<a href="javascript:;" class="btn btn-primary btn-sm <?php if(!$_POST) { echo "active";} ?>" id="stackLightType0" ><?php echo Showall; ?></a>
					<?php foreach($listStackLightTypes->result() as $key1234 =>  $stackLightType) { ?>
						<a href="javascript:;" id="stackLightType<?php echo $stackLightType->stackLightTypeId; ?>" class="btn btn-primary btn-sm  <?php if($_POST) { if($stackLightType->stackLightTypeId == $_POST['stackLightTypeId']) echo "active"; } ?>" ><?php echo $stackLightType->stackLightTypeName; ?></a>
					<?php } ?>
					</span>
				</div>
			<div class="row">
				<div class="col-md-12">
				  <div class="panel panel-inverse panel-primary boxShadow" style="overflow-x: scroll!important"; >
						<div class="panel-body">
							<table id="empTable" class="display table table-bordered m-b-0" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th><?php echo Id; ?></th>
										<th><?php echo Stacklighttypename; ?></th>
										<th><?php echo Versionname; ?></th>
										<th><?php echo Files; ?></th>
										<th><?php echo Addeddate; ?></th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div> 
			</div>
			<hr style="background: gray;">
            <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	</div>
	

