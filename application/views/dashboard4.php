<style type="text/css">
    .btn-default.btn-on.active
    {
        background-color: #002060 !important;
        color: white;
    }
    .btn-default.btn-off.active
    {
        background-color: #002060 !important;
        color: white;
    }
    .btn-default.btn-on
    {
        background-color: #d8dbe0;
        color: white;
    }
    .btn-default.btn-off
    {
        background-color: #d8dbe0;
        color: white;
    }
</style>

<style type="text/css">
    .col-md-3
    {
      display: inline-block;
      margin-left:-4px;
    }
    .col-md-3 img
    {
      width:100%;
      height:auto;
    }
@media (min-width: 768px) and (max-width: 991px) {
    .carousel-inner .active.col-md-4.carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: absolute;
        top: 0;
        right: -33.3333%;  /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }
}
@media (min-width: 576px) and (max-width: 768px) {
    /* Show 3rd slide on sm if col-sm-6*/
    .carousel-inner .active.col-sm-6.carousel-item + .carousel-item + .carousel-item {
        position: absolute;
        top: 0;
        right: -50%;  /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }
}
@media (min-width: 576px) {
    .carousel-item {
        margin-right: 0;
    }
    .carousel-inner .active + .carousel-item {
        display: block;
    }
    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left),
    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item {
        transition: none;
    }
    .carousel-inner .carousel-item-next {
        position: relative;
        transform: translate3d(0, 0, 0);
    }
    .active.carousel-item-left + .carousel-item-next.carousel-item-left,
    .carousel-item-next.carousel-item-left + .carousel-item,
    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }
    .carousel-inner .carousel-item-prev.carousel-item-right 
    {
        position: absolute;
        top: 0;
        left: 0;
        z-index: -1;
        display: block;
        visibility: visible;
    }
    .active.carousel-item-right + .carousel-item-prev.carousel-item-right,
    .carousel-item-prev.carousel-item-right + .carousel-item,
    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
@media (min-width: 768px) 
{
    .carousel-inner .active + .carousel-item + .carousel-item 
    {
        display: block;
    }
    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item 
    {
        transition: none;
    }
    .carousel-inner .carousel-item-next 
    {
        position: relative;
        transform: translate3d(0, 0, 0);
    }
    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }
    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
@media (min-width: 991px) {
    .carousel-inner .active + .carousel-item + .carousel-item + .carousel-item 
    {
        display: block;
    }
    .carousel-inner .carousel-item.active:not(.carousel-item-right):not(.carousel-item-left) + .carousel-item + .carousel-item + .carousel-item {
        transition: none;
    }
    .carousel-inner .active.col-lg-3.carousel-item + .carousel-item + .carousel-item + .carousel-item + .carousel-item {
        position: absolute;
        top: 0;
        right: -25%;  /*change this with javascript in the future*/
        z-index: -1;
        display: block;
        visibility: visible;
    }
    .carousel-item-next.carousel-item-left + .carousel-item + .carousel-item + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(-100%, 0, 0);
        visibility: visible;
    }
    .carousel-item-prev.carousel-item-right + .carousel-item + .carousel-item + .carousel-item + .carousel-item 
    {
        position: relative;
        transform: translate3d(100%, 0, 0);
        visibility: visible;
        display: block;
        visibility: visible;
    }
}
body 
.no-padding
    {
          padding-left: 0;
          padding-right: 0;
   }

.carousel-item-next, .carousel-item-prev, .carousel-item.active 
{
    display: flex !important;
}
.carousel-control-prev 
{
    left: 4px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 2px;
}
.carousel-control-next 
{
    right: 27px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 2px;
}
.filter.active
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: orange !important;
}
.filter
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: gray !important;
}
.filter1.active
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: orange !important;
}
.filter1
{
    width: 100% !important; 
    background: #FFFFFF !important;
    color: gray !important;
}
.datepicker.dropdown-menu 
{
    min-width: 200px !important;
}
.datepicker.datepicker-dropdown 
{
    width: 207px !important;
}
.datepicker 
{
    min-width: 207px!important;
}
.datepicker,
.table-condensed 
{
}
.datepicker table tr td, 
.datepicker table tr th 
{
    padding-left: 0px !important;
    padding-right: 0px !important;
}
.dow 
{
    font-weight: 500 !important;
}
.table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th 
{
    padding: 0px 0px !important;
}
.datepicker table tr td, .datepicker table tr th 
{
    width: 0px !important;
    height: 19px !important;
    border-radius: 15px !important;
}
.datepicker table tr td span.active.active, .datepicker table tr td.active.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active:hover:hover, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover, .open .dropdown-toggle.datepicker table tr td.active:hover 
{
    background: #FF8000!important;
}
.datepicker .prev 
{
    box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .prev:before 
{
    color: #FF8000 !important;
}
.datepicker .next 
{
    box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .next:before 
{
    color: #FF8000 !important;
}
.datepicker table tr td.old, .datepicker table tr td.new 
{
    color: #ddd2d2 !important;
}
@media screen and (min-device-width:768px)and (max-width:999px) 
    {
       .laptopsize
                    {
                       /* width: 20px;*/
                        height: 20px;
                        padding: 2px;
                        float: left;
                        margin-right: 10px;
                        font-size: 10px!important;
                    }
    }
</style>

   <style type="text/css">
        .machineNameText
        {
            display: inline-block;
            width: 60%;
            white-space: nowrap;
            overflow: hidden !important;
            text-overflow: ellipsis;
        }
   </style>

<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
<div id="content" class="content">
            
            <h1 style="font-size: 22px;color: #002060!important;" class="page-header">Analytics</h1>
            <br />
            <div class="row" style="margin-top: -23px;">
                <div class="col-md-3" style="padding-left: 0px;">
                    <!-- <a href="<?php echo base_url('admin/breakdown3'); ?>" class="btn btn-primary" style="background: #F60100;padding-left: 18px !important;padding-right: 18px !important;margin-left: 10px;" >Stop analytics</a> -->
                </div>
                <div class="col-md-1 show_by">
                    <label>Show by :</label>
                </div>
                <div class="col-md-2" style="padding-left: 5px;padding-right: 0px;">
                    <select  class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                        <option class="new_test" value="day">Day</option>
                        <option class="new_test" value="weekly">Week</option>
                        <option class="new_test" value="monthly"><span> Month </span></option>
                        <option class="new_test" value="yearly"><span> Year </span></option>
                    </select>
                </div>
                <div class="col-md-1 day show_by" style="width:100%;flex: 0 0 2% !important;padding-left:0px;padding-right: 0px !important;padding-top: 5px;">
                    <label style="padding-left:8px;padding-right: 5px;"> For:&nbsp;</label>
                </div> 
                <div class="col-md-2 day" style="width:100%;padding-left: 5px;padding-right: 12px;">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%;" value="<?php echo date('m/d/Y'); ?>">
                </div>
                <div class="col-md-2 weekly" style="display: none;">
                    <select class="form-control" id="choose3" onchange="changeFilterValue()">
                        <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1">Week 1</option>
                        <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2">Week 2</option>
                        <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3">Week 3</option>
                        <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4">Week 4</option>
                        <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5">Week 5</option>
                        <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6">Week 6</option>
                        <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7">Week 7</option>
                        <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8">Week 8</option>
                        <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9">Week 9</option>
                        <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10">Week 10</option>
                        <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11">Week 11</option>
                        <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12">Week 12</option>
                        <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13">Week 13</option>
                        <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14">Week 14</option>
                        <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15">Week 15</option>
                        <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16">Week 16</option>
                        <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17">Week 17</option>
                        <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18">Week 18</option>
                        <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19">Week 19</option>
                        <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20">Week 20</option>
                        <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21">Week 21</option>
                        <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22">Week 22</option>
                        <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23">Week 23</option>
                        <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24">Week 24</option>
                        <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25">Week 25</option>
                        <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26">Week 26</option>
                        <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27">Week 27</option>
                        <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28">Week 28</option>
                        <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29">Week 29</option>
                        <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30">Week 30</option>
                        <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31">Week 31</option>
                        <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32">Week 32</option>
                        <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33">Week 33</option>
                        <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34">Week 34</option>
                        <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35">Week 35</option>
                        <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36">Week 36</option>
                        <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37">Week 37</option>
                        <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38">Week 38</option>
                        <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39">Week 39</option>
                        <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40">Week 40</option>
                        <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41">Week 41</option>
                        <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42">Week 42</option>
                        <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43">Week 43</option>
                        <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44">Week 44</option>
                        <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45">Week 45</option>
                        <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46">Week 46</option>
                        <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47">Week 47</option>
                        <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48">Week 48</option>
                        <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49">Week 49</option>
                        <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50">Week 50</option>
                        <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51">Week 51</option>
                        <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52">Week 52</option>
                    </select>
                </div>
                <div class="col-md-1 weekly" style="display: none;">
                </div>
                <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
                <div class="col-md-1 monthly show_by" style="max-width: 2% !important;flex: 0 0 2% !important;padding-left: 0px !important;padding-right: 0px !important;display: none;">
                    <label> for </label>
                </div>
                <div class="col-md-2 monthly" style="max-width: 23% !important;flex: 0 0 23% !important;padding-left: 3px;padding-right: 10px;display: none;">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker1" id="choose4">
                </div>
                <div class="col-md-2 yearly" style="display: none;">
                    <select class="form-control"  id="choose5" onchange="changeFilterValue()">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option selected="" value="2020">2020</option>
                    </select>
                </div>
                <div class="col-md-1 yearly" style="display: none;"></div>
            </div>
            <div class="row" style="margin-top: 13px;">
                <div class="col-md-12" style="padding-left: 6px !important;padding-right: 2px!important;">
                        <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>">
                        <div id="carousel-example" class="carousel slide" data-ride="carousel" data-interval="false">
                        <div class="carousel-inner row w-100 mx-auto" role="listbox">
                            <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active showAll" style="padding-left: 0px !important;">
                                    <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> Show all  </a>
                                </div>
                              <?php 
                            foreach($listMachines->result() as $machine) { ?>
                                <?php if($machine->machineId == $_POST['machineId']){  ?>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3" style="padding-left: 0px !important;">
                                    <a href="javascript:;" class="btn btn-default btn-lg filter active machineNameText" id="machine<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                                </div>
                            <?php } ?>
                            <?php } ?>
                            <?php 
                            $i = 1;
                            foreach($listMachines->result() as $machine) { ?>
                                <?php if($machine->machineId != $_POST['machineId']){ ?>
                                <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3" style="padding-left: 0px !important;">
                                    <a href="javascript:;" class="btn btn-default btn-lg filter machineNameText" id="machine<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                                </div>
                            <?php }  ?>
                            <?php $i++; } ?>
                        </div>
                        <?php $count = count($listMachines->result());
                        if ($count > 3) { ?>
                        <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                            <span style="background-color: #FFFFFF !important;color: #FF8000 !important;" class="carousel-control-prev-icon"><i style="font-size: 20px;" class="fa fa-angle-left" aria-hidden="true"></i></span>
                        </a>
                        <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                            <span style="background-color: #FFFFFF !important;color: #FF8000 !important;" class="carousel-control-next-icon"><i style="font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></span>
                        </a>
                        <?php } ?>
                    </div>
                </div> 
            </div>
            <style>
               #chartwrap
               {
                position: relative!important;
               /* padding-bottom: 100%!important;*/
                height:0%!important;
                overflow: hidden!important;
               }

         @media screen and (min-device-width:320px)and (max-width: 375px)
        {
           .DailyGraphs3
            {
               
                margin-left:5px!important;
            }
        }
            </style>

            <div class="row" id="chartwrap" style="margin-top: 13px;display: flex;flex-wrap: wrap;justify-content:space-between;margin-left: -20px;">
                <div class="col-md-5" style="max-height: 390px !important;">
                    <div class="tab-content boxShadow" style="min-height: 335px!important;" >
                        <div class="tab-pane fade active show">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-md-12" >
                                    <h4 style="color: #002060;">Machine mode</h4>
                                    <center style="margin-bottom: -30px;margin-top: -24px;">
                                        <div id="DailyGraph3" class="DailyGraphs3" style="padding-right:20px;height:330px;"></div>
                                    </center>
                                </div>
                                <div id="DailyGraph3 " class="col-md-12 DailyGraphs3" style="height:273px;display:none;" >
                                    <h4 style="color: #002060;">Actual production time</h4>
                                    <center>
                                        <div class="laptopsizegraph" style="height: 180px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 180px;" >
                                            <div class="laptopsizegraph2" style="height: 150px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 150px;" ></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-md-4" style="max-height: 390px !important;">
                   <div class="tab-content boxShadow" style="min-height: 335px!important;">
                        <div class="tab-pane fade active show" >
                            <div class="row m-l-0 m-r-0">
                                <div id="dataDailyGraph4" class="col-md-12">
                                    <h4 style="color: #002060;">Production time</h4>
                                    <center>
                                        <div id="DailyGraph4" style="height:270px;" ></div>
                                    </center>
                                </div>
                                <div id="blankDailyGraph4" class="col-md-12 graphmargin" style="height:273px;display:none;" >
                                    <h4 style="color: #002060;">Production time</h4>
                                    <center>
                                        <div class="laptopsizegraph" style="height: 160px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 160px;" >
                                            <div class="laptopsizegraph2" style="height: 130px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 130px;" ></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <style type="text/css">
                    .bagerClass
                    {
                        width: 20px;
                        height: 20px;
                        padding: 2px;
                        float: left;
                        margin-right: 10px;
                    }
                    @media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .graphmargin
                        {
                            margin-left:-19px!important;
                        }
                    }@media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .laptopsizegraph
                        {
                            height: 146px!important;
                            border: 1px solid #d8cfcf!important;
                            border-radius: 50%!important;
                            margin-top: 55px!important;
                            width: 138px!important;
                        }
                    }
                    @media screen and (min-device-width:768px)and (max-width:999px) 
                    {
                        .laptopsizegraph2
                        {
                            height: 114px!important;
                            border: 1px solid #d8cfcf!important;
                            border-radius: 50%!important;
                            margin-top: 14px!important;
                            width: 82%!important;
                            margin-left: 0px!important;
                        }
                    }
                </style>

                <div class="col-md-3">
                    <div class="tab-content laptopsize boxShadow" style="height: 335px;">
                        <div class="tab-pane fade active show">
                            
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #76BA1B;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> RUNNING</div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FFCF00;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> WAITING</div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #F60100;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> STOPPED</div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #CCD2DF;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> OFF</div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #000000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> NODET</div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #002060;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> PRODUCTION TIME </div>
                            </div>
                        
                            <div style="padding-bottom: 20px;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #124D8D;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;">  SETUP TIME </div>
                            </div>
                        
                            <div style="padding-bottom: 6%;">
                                <div class="bagerClass bagerClasss laptopsize" style="background: #FF8000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> NO PRODUCTION </div>
                            </div>
                            
                        </div>
                    </div>
                </div>
               <!--  <div class="col">
                    <h4 style="color: #FF8000;background-color: #124D8D;padding: 14px;border-radius: 5px;color: white;">Actual setup time</h4>
                    <div class="tab-content boxShadow">
                        <div class="tab-pane fade active show">
                            <div class="row m-l-0 m-r-0">
                                <div id="dataDailyGraph5" class="col-md-12">
                                    <div id="DailyGraph5" style="height:273px;" ></div>
                                </div>
                                <div id="blankDailyGraph5" class="col-md-12" style="height:273px;display:none;">
                                    <center>
                                        <div style="height: 155px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 55px;width: 155px;" >
                                            <div style="height: 126px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 82%;"></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col">
                    <h4 style="color: #FF8000;background-color: #FF8000;padding: 14px;border-radius: 5px;color: white;">No production</h4>
                    <div class="tab-content boxShadow">
                        <div class="tab-pane fade active show">
                            <div class="row m-l-0 m-r-0">
                                <div id="dataDailyGraph6" class="col-md-12">
                                    <div id="DailyGraph6" style="height:273px;" ></div>
                                </div>
                                <div id="blankDailyGraph6" class="col-md-12" style="height:273px;display:none;">
                                    <center>
                                        <div style="height: 155px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 55px;width: 155px;" >
                                            <div style="height: 126px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 82%;" ></div>
                                        </div>
                                    </center>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> -->
            </div>
            <div class="graph-loader hide" ></div>
                <!-- <div class="row" style="margin-top: 13px;margin-left: -20px;">
                <div class="col-md-12">
                    <div class="tab-content boxShadow">
                        <div class="row">
                            <div style="min-width: 10%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #76BA1B;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> RUNNING</center>
                            </div>
                           <div style="min-width: 10%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #FFCF00;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> WAITING</center>
                            </div>
                            <div style="min-width: 10%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #F60100;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> STOPPED</center>
                            </div>
                            <div style="min-width: 10%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #CCD2DF;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> OFF</center>
                            </div>
                            <div style="min-width: 10%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #000000;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> NODET</center>
                            </div>
                            <div style="min-width: 16%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #002060;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> ACTUAL PRODUCTION TIME </center>
                            </div>
                            <div style="min-width: 16%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #124D8D;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span>  ACTUAL SETUP TIME </center>
                            </div>
                            <div style="min-width: 16%; display:flex;flex-wrap:wrap;padding-top:7px;padding-left:8px;">
                                <center> <span style="background: #FF8000;width: 20px;height: 20px;padding: 2px;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</span> NO PRODUCTION </center>
                            </div>
                        </div>
                    </div>
                </div>
            </div> -->
            <div class="row"  style="margin-left: -20px;">
                <div class="col-md-12" >
                    <div class="tab-content boxShadow" style="">
                        <div class="tab-pane fade active show" style="overflow-x: scroll!important;">
                            <div class="row m-l-0 m-r-0">
                                <div class="col-md-12">
                                    <h3 id="headerChange" style="color: #002060;">Hourly machine analysis</h3>
                                    <div id="DailyGraph1" style="height:350px;width:100%;" >                        
                                    </div> 
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <hr style="background: gray;">
                <p>@2021 nytt | All Rights Reserved</p>
        </div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
    </div>


    