<?php  defined('BASEPATH') OR exit ('No direct script access allowed'); ?>
<html>
<head>
	<title></title>
	<style>
		#form{
			width:100px;
		}

		#fileExplorer
		{
			float:left;
			width:100px;
		}
		
		.thumbnail
		{
			float:left;
			margin:3px;
			padding:3px;
			border:1px solid black;
		}
		ul
		{
			list-style-type: none;
			margin:0;
			padding:0; 
		}
		li
		{
			padding:0;
		}
	</style>

	<script src="<?php echo base_url('assets/ckeditor/jquery.min.js');?>" type="text/javascript"></script>
	<script src="<?php echo base_url('assets/ckeditor/ckeditor.js');?>" type="text/javascript"></script>
	
	<script>
		$(document).ready(function(){
			var funcNum = <?php echo $_GET['CKEditorFuncNum'].';'; ?>
			$('#fileExplorer').on('click', 'gif', function() {
				var fileUrl = $(this).attr('title');
				window.opener.CKEDITOR.tools.callFunction(funcNum, fileUrl);
				window.close();	
			}).hover(function(){
				$(this).css('cursor', 'pointer');
			});
		});
	</script>
	</head>

	<body>
		<div id="fileExplorer">
			<?php foreach ($filelist as $fileName) { ?>
				<div class="thumbnail">
					<img src="<?php echo base_url().$fileName; ?>" alt="thumb" title="<?php echo base_url().$fileName; ?>" width="120px;">
					<br>
					<?php echo base_url().$fileName; ?>
				</div>
			<?php } ?>
		</div>

	</body>
</html>