<link href="<?php echo base_url('assets/css/virtual_machine_log_list.css');?>" rel="stylesheet" />

  <div id="content" class="content">
    
    <h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Virtualmachine; ?></h1>
    <div class="row">
      <div class="col-md-3">
        <button class="btn btn-primary exportToCsvButtonStyle" onclick="exportCsv();"><?php echo Exporttocsv; ?></button>
      </div>
      <div class="col-md-9">
          <?php 
          if($_POST && isset($_POST['dateValS'])) { 
            $dateValE = date("F d, Y", strtotime($_POST['dateValE']));
            $dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
          } else { 
            $dateValE =  date("F d, Y", strtotime("-29"));
            $dateValS = date("F d, Y"); 
          } 
          ?>
          <div class="form-group row">
            <div class="col-md-10">
              <input type="hidden" name="endDateValS" id="endDateValS" value="<?php echo date("Y-m-d", strtotime("-29 day"));?>" />  
              <input type="hidden" name="endDateValE" id="endDateValE" value="<?php echo date("Y-m-d");?>" />   
              <div id="checkout-daterange" name="checkout-daterange" class="btn btn-primary btn-block text-left f-s-12 exportToCsvButtonStyle">
                <i class="fa fa-caret-down pull-right m-t-2"></i>
                <span>
                <?php echo Last30Days; ?>
                </span> 
              </div>
            </div>
          </div>
      </div>
      <div class="col-md-12 table_data colmd12TableDataStyle">
        <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >
            <div class="row" style="float:left;margin: 10px;">
              <span class="displayfilterrowstyle" id="showIOname"></span>
              <span class="displayfilterrowstyle" id="showCreatedDate"></span>
              <span class="displayfilterrowstyle" id="showSignal"></span>
              <span class="displayfilterrowstyle" id="showCheckOut"></span>
            </div>
          <div class="panel-body">
            <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
              <thead>
                <tr>
                 
                  <th class="tableHeaderLogIdStyle">
                    <div style="width:45px!important;"><?php echo LogID; ?></div>
                    <br>
                    <small>&nbsp;</small>
                  </th>

                  <th class="tableHeaderIONameStyle">
                    <div id="filterIONameSelected" class="dropdown IONameSelectedStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown" id="filterIONameSelectedValue"> <?php echo IOName; ?>&nbsp;</span>
                      <div class="dropdown-menu">


                       
                        <?php foreach ($virtualMachine as $key => $value) { ?>
                          <div class="dropdown-item">
                            <div class="form-check" style="width: max-content;">
                              <input onclick="filterIOName(<?php echo $value['virtualMachineId'] ?>,'<?php echo $value['IOName'] ?>')" 
                              class="form-check-input filterIOName" type="checkbox" id="filterIOName<?php echo $value['virtualMachineId'] ?>" name="filterIOName" value="'<?php echo $value['IOName']; ?>'" <?php if ($value['IOName'] == $_POST['IOName']) {
                                echo "checked";
                              } ?> >
                              <label class="form-check-label" for="filterIOName<?php echo $key; ?>">
                                <?php
                                if ($value['IOName'] == "GPIO15") {
                                  echo "Parts produced";
                                }else if ($value['IOName'] == "GPIO14") {
                                  echo "Robot status";
                                }else{
                                  echo $value['IOName']; 
                                } ?>
                              </label>
                            </div>
                          </div>
                        <?php } ?>
                      </div>
                    </div>
                    <small class="TableSmallIOnamestyle" >&nbsp;</small>
                  </th>

              <!--     <th class="tableHeaderAddeddDateStyle" >
                    <input type="hidden" name="dateValS" id="dateValS" value="" />  
                    <input type="hidden" name="dateValE" id="dateValE" value="" />   
                    <div id="advance-daterange" name="advance-daterange" style="width: 86px;padding-bottom: 5px!important">
                      <span>
                      <?php echo AddedTime; ?>&nbsp;&nbsp;
                      </span> 
                      <i class="fa fa-caret-down m-t-2"></i>
                    </div>
                    <small class="TableSmallIOnamestyle">
                      &nbsp;
                    </small>
                  </th> -->

                  <th class="tableHeaderAddeddDateStyle" style="padding-bottom: 18px;">  
                    <div style="width: 100px;">
                      <span>
                        <?php echo AddedTime; ?>&nbsp;&nbsp;
                      </span> 
                    </div>
                    <small  style="color: #FF8000;text-align : center;" >
                      &nbsp;
                    </small>
                  </th>

                   <th class="tableHeaderSignalStyle">
                    <div id="filterSignalSelected" class="dropdown SignalSelectedFilterStyle">
                      <span class="dropdown-toggle" data-toggle="dropdown"> <?php echo Signal; ?>&nbsp;</span>
                      <div class="dropdown-menu">
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterSignal()" value="'1'" class="form-check-input filterSignal" type="checkbox" id="filterSignalHigh">
                            <label class="form-check-label" for="filterSignalHigh">
                              <?php echo Active; ?>
                            </label>
                          </div>
                        </div>

                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterSignal()" value="'0'" class="form-check-input filterSignal" type="checkbox" id="filterSignalEveryLow">
                            <label class="form-check-label" for="filterSignalEveryLow">
                              <?php echo NotActive; ?>
                            </label>
                          </div>
                        </div>
                      </div>
                    </div>
                  <small class="TableSmallIOnamestyle">&nbsp;</small>
                  </th>

                  <th class="tableHeaderAddeddDateStyle" style="padding-bottom: 18px;">
                  
                    <div style="width: 100px;">
                      <span>
                        <?php echo OriginalTime; ?>&nbsp;&nbsp;
                      </span> 
                    </div>
                    <small  style="color: #FF8000;text-align : center;" >
                      &nbsp;
                    </small>
                  </th>

                </tr>
              </thead>
            </table>
          </div>
        </div>
      </div> 
    </div>
    <hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
    </div>
  <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
    <i class="fa fa-angle-up">
    </i>
  </a>
</div>
  





