<link rel="stylesheet" href="<?php echo base_url('assets/css/RolesandTeams.css');?>">
<style>
    input#userName 
    {
        background-image: url('<?php echo base_url("assets/img/user.png"); ?>')
        background-repeat: no-repeat;
        text-indent: 35px
    }
    input[type=radio]:checked:before {
        width: 15px !important;
        height: 15px !important;
        margin-top: -2px !important;
        margin-left: -2px !important;
        background-image: url('<?php echo base_url("assets/img/tick_orange.svg"); ?>')
    }
    input[type=checkbox]:checked:before {
        width: 15px !important;
        height: 15px !important;
        margin-top: -2px !important;
        margin-left: -2px !important;
        background-image: url('<?php echo base_url("assets/img/tick_orange.svg"); ?>')
    }
     input#userName {
        background-image: url(<?php echo base_url("assets/img/user.png");?>);
        background-repeat: no-repeat;
        text-indent: 25px
    }

    input.passwordIcon {
        background-image: url(<?php echo base_url('assets/img/lock.png');?>);
        background-repeat: no-repeat;
        text-indent: 25px
    }
</style>
<div id="content" class="content">
    
    <h1 class="page-header headerStyle"><?php echo $this->lang->line('OverallStatistics'); ?></h1>
    </head>

    <body>

        <div class="col-md-12">
            <div class="row">
          <div class="graph-loader hide"></div>
                <div class="col-md-6">
                    <div class="row">
                        <div class="col-md-3 m-t-10 m-l-10">
                            <button class="tablink GrouplistButtonStyle" style="width: 100%;border-radius: 10px;height: 32px;padding-top: 8px;background-color: rgb(0, 32, 96);font-size: 13px;" onclick="openCity('groupList', this, '#002060')" id="defaultOpen">Group List</button>
                        </div>
                        <div class="col-md-3 m-t-10 m-l-10">
                            <button class="tablink userlistButtonStyle" style="width: 100%;border-radius: 10px;height: 32px;padding-top: 8px;background-color: rgb(0, 32, 96);font-size: 13px;" onclick="openCity('userList', this, '#002060')">User List </button>
                        </div>
                    </div>
                </div>

                <div class="col-md-6">
                    <div class="col-md-12">
                        <div class="row">
                            <div class="col-md-5"></div>
                            <div class="col-md-4 m-t-10">
                                <button class="addnewRoleStyle AddnewRolebuttonStyle" id="defaultOpen" data-toggle="modal" data-target="#modalAddNewRole">
                                    &nbsp;&nbsp;&nbsp;<img class="buttonIconstyle" src="<?php echo base_url('assets/img/addnewrole.png'); ?>">ADD NEW ROLE</button>
                            </div>
                            <div class="col-md-3 m-t-10">
                                <button class="addUserButtonStyle AddnewUserButtonStyle" style="" data-toggle="modal" data-target="#AddNewuser">
                                    &nbsp;<img class="buttonIconstyle" src="<?php echo base_url('assets/img/addnewuser.png'); ?>">ADD NEW USER</button>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div id="groupList" class="tabcontent">
            <div class="col-md-12">
                <div class="row">
                    <?php foreach ($roles as $key => $value) { ?>
                        <div class="col-md-6">
                            <div class="card boxShadow">
                                <div class="card-img-overlay">
                                    <h4 class="card-title">
                                        <span>
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <a style="color: #FF8000;" href="" data-toggle="modal">
                                                        <span data-toggle="tooltip" data-title=""><?php echo $value['rolesName']; ?></span>
                                                    </a>
                                                </div>
                                                <div class="col-md-4">
                                                    <div class="row">
                                                        <div class="">
                                                            <img class="btn btn-primary editpencilfinal" onclick="editRoles(<?php echo $value['rolesId']; ?>)" style="height: 35px;margin-left: 0%;" src="<?php echo base_url('assets/nav_bar/createwhite.svg'); ?>">
                                                        </div>

                                                            <?php
                                                            $rolesType = $value['rolesType'];
                                                            $accessReportPoint = $this->AM->accessReportPointEdit("22","$rolesType");
                                                            if ($accessReportPoint == true) { ?>
                                                        <div class="" style="margin-left: 2%;">

                                                            <form class="form-inline editPencilMon" action="<?php echo base_url('Operator/operator_checkin_checkout'); ?>" method="POST">
                                                                <input type="hidden" name="userRole" value="<?php echo $value['rolesType']; ?>">
                                                                <button type="submit" class="btn btn-primary">
                                                                    <img style="height: 18px;"  src="<?php echo base_url('assets/nav_bar/report.svg'); ?>">
                                                                </button>
                                                            </form>
                                                        </div>
                                                            <?php } ?>

                                                        <?php if ($value['rolesType'] > 2) { ?>
                                                            <div class="userListmargins" style="margin-left: 2%;">
                                                                <img class="btn btn-danger deletebuttonstyle" onclick="deleteRoles(<?php echo $value['rolesId']; ?>,'<?php echo $value['rolesName']; ?>')" style="height: 35px;cursor:pointer;padding: 3px;" src="<?php echo base_url('assets/nav_bar/delete_white.svg'); ?>">
                                                            </div>
                                                        <?php } ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </span>

                                        
                                    <hr class="m-t-10" style="height: 2px !important;">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <div class="col-md-8">
                                                    <span style="font-size: 12px;font-weight: 700;">Users&nbsp;&nbsp;&nbsp;&nbsp;</span>
                                                    <span style="font-size: 12px;color:#ff8000;"><?php echo $value['totalUsers']; ?> Members</span>
                                                </div>
                                                <div class="col-md-4">
                                                    <button class="addUserButtonStyle AdduserbuttonmainStyle" onclick="assignOperatorRole(<?php echo $value['rolesType'] ?>)" >Add users
                                                    </button>
                                                </div>
                                            </div>
                                        </div>
                                    </h4>

                                    <div class="row rolesheight" style="overflow: auto;flex-wrap: wrap;display: flex;height: 80px;">
                                        <?php foreach ($value['users'] as $keyUser => $valueUser) { ?>
                                            <div class="m-10 userListmargin ">
                                                <img style="width: 20px;height: 20px;border-radius: 50%;" src="<?php echo base_url('assets/img/user/' . $valueUser['userImage']); ?>">
                                                <span class="OperatorNameSpanStyle"><?php echo $valueUser['userName']; ?></span>
                                            </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                    <?php } ?>

                    <div class="modal fade" id="AddUsers" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <div class="modal-header" style="background-color: #002060;">
                                    <h4 class="modal-title" id="liveMachineName" style="color:#ff8000;">Add users - Operators</h4>
                                    <button type="button" class="close" data-dismiss="modal">
                                        <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                                    </button>
                                </div>
                                <div class="modal-body" style="display:block;">
                                    <form action="assignOperatorToRole" method="POST" class="m-r-10 m-l-10" id="assignOperatorToRole_form">
                                        <div class="col-md-12">
                                            <div class="row">
                                                <input type="hidden" name="assignOperator" id="assignOperator">
                                                <input type="hidden" name="oldAssignOperator" id="oldAssignOperator">
                                                <input type="hidden" name="rolesType" id="rolesType">
                                                <div class="col-md-6">
                                                    <span style="color:black;font-weight:700;">Selected users</span>
                                                    <input type="text" class="selectedUsersearch">
                                                    <br>
                                                    <div class="panel panel-inverse panel-primary boxShadow m-t-10 adduserSelectPanel">
                                                        <div id="show_menu" class="m-10 p-t-10">

                                                        </div>
                                                        <span class="m-10"></span>
                                                    </div>
                                                </div>
                                                <span class="vertical"></span>
                                                <div class="col-md-6">
                                                    <span style="color:black;font-weight:700;">All users</span>
                                                    <br>
                                                    <input type="text" class="AllUsersSearch">
                                                    <div class="panel panel-inverse panel-primary boxShadow m-t-10 AllSelectPanel">
                                                        <div id="hide_menu" class="m-10 p-t-10 list_container">

                                                        </div>

                                                    </div>
                                                </div>
                                            </div>
                                            <div class="">
                                                <center>
                                                    <button style="width:20%;" id="assignOperatorToRole_submit" type="submit" class="btn btn-primary">Save</button>
                                                </center>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modal-delete-roles" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content" style="max-height: 230px;">
                                <div class="modal-header" style="background-color: #002060;">
                                    <h4 class="modal-title deletemodalTitle" id="role_title"></h4>
                                    <button type="button" class="close" style="padding: 12px 32px !important;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="p-b-20" action="deleteRoles" method="POST" id="deleteRoles_form" class="">
                                        <input type="hidden" id="deleteRolesId" name="rolesId">
                                        <div class="modal-footer deleteModalFooter">
                                            <div class="row">
                                                <div class="col-md-12" style="margin-top: -20px;">
                                                    <center>
                                                        <img style="width: 22%;" src="<?php echo base_url() . 'assets/img/delete_gray.svg' ?>" style="">
                                                    </center>
                                                </div>
                                                <div class="col-md-12" style="">
                                                    <center> Are you sure you want to delete</center>
                                                </div>
                                                <div class="col-md-12" style="margin-top: 5px;">
                                                    <center>
                                                        <button type="submit" style="width:25%;" class="btn btn-danger">Delete</button>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="editGroupList" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header editmodalHeader">
                                    <h4 class="modal-title editmodalTitle">Edit Group</h4>
                                    <button type="button" class="close" style="padding: 12px 32px !important;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                                </div>
                                <div class="modal-body">
                                    <!-- <div class="column col-md-8"> -->
                                    <div class="col-md-12">
                                        <div class="row">
                                            <div class="col-md-6">
                                                <div style="padding: 15px 5px;" class="form-group">
                                                    <label style="font-weight: 650;color:black;">Group Name</label>
                                                    <input type="text" style="padding: 0px;border-top: 0px;border-left: 0px;border-right: 0px;border-radius: 0px;padding-bottom: 12px;margin-top: 10%;" class="form-control col-md-12 border-left-right-top-hide" id="userName" name="" placeholder="Enter name" required>
                                                </div>
                                            </div>

                                            <div class="col-md-6">
                                                <div style="padding: 15px 5px;" class="form-group">
                                                    <label style="font-weight: 650;color:black;">Role</label>
                                                    <div class="dropdown colmd3SpRepeatTaskStyle" style="box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;border: none !important;padding: 8px !important;border-radius: 5px !important;cursor: pointer !important;margin-top: 10%;">
                                                        <div class="dropdown-toggle" data-toggle="dropdown"> Select an option </div>
                                                        <div class="dropdown-menu" style="width: 100%;margin-left: -8px;margin-top: 17px;line-height: 0.5;">
                                                            <div class="dropdown-item" style="line-height: 0.5;">
                                                                Administrator
                                                                <hr class="sidepanelrepeattaskstyle">
                                                            </div>
                                                            <div class="dropdown-item" style="line-height: 0.5;">
                                                                Manager
                                                                <hr class="sidepanelrepeattaskstyle">
                                                            </div>
                                                            <div class="dropdown-item" style="line-height: 0.5;">
                                                                Operator
                                                                <hr class="sidepanelrepeattaskstyle">
                                                            </div>
                                                            <div data-toggle="modal" data-target="#modalAddNewRole" class="dropdown-item" style="font-weight: 600 !important;background-color: #ff8000 !important;color: #fff !important;padding: 13px !important;text-align: center !important;border-bottom-left-radius: 10px !important;border-bottom-right-radius: 10px !important;margin-bottom: -12px !important;margin-top: -5px !important;line-height: 0.5;">
                                                                <center><i class="fa fa-plus" aria-hidden="true"></i>&nbsp;&nbsp;&nbsp;Add new role</center>
                                                            </div>

                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <label style="font-weight: 650;color:black;">User</label>
                                            
                                            <br><br>
                                            <div class="col-md-12">
                                                <center><button class="btn btn-primary">Update</button></center>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div id="userList" class="tabcontent">
            <div class="row" style="padding-left: 15px!important;">
                <div class="col-md-12">
                    <div class="panel panel-inverse panel-primary boxShadow" style="overflow:auto;">
                        <div class="row">
                            <span class="displayfilterrowstyle" id="showUserFilter"></span>
                            <span class="displayfilterrowstyle" id="showAddeddDate"></span>
                            <span class="displayfilterrowstyle" id="showRole"></span>
                        </div>
                        <div class="panel-body">
                            <table id="empTable" class="display table m-b-0" style="width:100%;" cellspacing="0">
                                <thead>
                                    <tr>
                                        <th class="TableHeaderTHstyle" style="min-width:100px;">UserId<br>
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>

                                        <th class="TableHeaderTHstyle">
                                            <div id="filterUserNameSelected" class="dropdown DropDownFilterOpeStyle">
                                                <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> Users&nbsp;</span>
                                                <div class="dropdown-menu" style="overflow: auto;height: 200px;">
                                                    <?php foreach ($users as $key => $value) { ?>
                                                        <div class="dropdown-item">
                                                            <div class="form-check formCheckWidth" style="justify-content: start;">
                                                                <input onclick="filterUserName(<?php echo $value['userId'] ?>,'<?php echo $value['userName'] ?>')" class="form-check-input filterUserName" type="checkbox" id="filterUserName<?php echo $value['userId'] ?>" name="filterUserName" value="<?php echo $value['userId']; ?>">
                                                                <label class="form-check-label" for="filterUserName<?php echo $value['userId'] ?>">
                                                                    <?php echo $value['userName']; ?>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>

                                        <th class="TableHeaderTHstyle" style="min-width:200px;">
                                            <input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-3000 day")); ?>" />
                                            <input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />
                                            <div id="advance-daterange" name="advance-daterange" class="daterangeandduedatestyle">
                                                <span>
                                                    Added Date&nbsp;&nbsp;
                                                </span>
                                                <i class="fa fa-caret-down m-t-2"></i>
                                            </div>
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>

                                        <!-- <th class="TableHeaderTHstyle" style="min-width:200px;">
                                            <div id="filterRolesAssignedSelected" class="dropdown DropDownFilterStyle">
                                                <span class="dropdown-toggle" data-toggle="dropdown"> Role assigned&nbsp;</span>
                                                <div class="dropdown-menu">
                                                    <?php foreach ($roles as $key => $value) { ?>
                                                        
                                                    <div class="dropdown-item">
                                                        <div class="form-check formCheckWidth" style="justify-content: start!important;">
                                                            <input onclick="filterRolesAssigned()" value="'<?php echo $value['rolesType']; ?>'" class="form-check-input filterRolesAssigned" type="checkbox" id="filterRoleOperator">
                                                            <label class="form-check-label" for="filterRoleOperator">
                                                                <?php echo $value['rolesName']; ?>
                                                            </label>
                                                        </div>
                                                    </div>
                                                    <?php } ?>

                                                </div>
                                            </div>
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th> -->

                                        <th class="TableHeaderTHstyle">
                                            <div id="filterRolesAssignedSelected" class="dropdown DropDownFilterOpeStyle">
                                                <span class="dropdown-toggle" data-toggle="dropdown" id="filterRolesAssignedSelectedValue"> Roles assigned&nbsp;</span>
                                                <div class="dropdown-menu" style="overflow: auto;height: 200px;">
                                                    <?php foreach ($roles as $key => $value) { ?>
                                                        <div class="dropdown-item">
                                                            <div class="form-check formCheckWidth" style="justify-content: start;">
                                                                <input onclick="filterRoleAss(<?php echo $value['rolesType'] ?>,'<?php echo $value['rolesName'] ?>')" class="form-check-input filterRoleAss" type="checkbox" id="filterRoleAss<?php echo $value['rolesType'] ?>" name="filterRoleAss" value="<?php echo $value['rolesType']; ?>">
                                                                <label class="form-check-label" for="filterRoleAss<?php echo $value['rolesType'] ?>">
                                                                    <?php echo $value['rolesName']; ?>
                                                                </label>
                                                            </div>
                                                        </div>
                                                    <?php } ?>
                                                </div>
                                            </div>
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>

                                        <th class="TableHeaderTH2style tableHeaderEditStyle">
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>

                                        <th class="TableHeaderTH2style tableHeaderEditStyle">
                                            <small class="tableColumnsmallStyle">&nbsp;</small>
                                        </th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>

                    <div class="modal fade" id="modal-delete-users" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content ">
                                <div class="modal-header deletemodalTitleBackground">
                                    <h4 class="delteTitleStyle" class="modal-title">Delete user</h4>
                                    <button type="button" class="close" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="p-b-20" action="delete_operator" id="delete_operator_form" method="POST" class="">
                                        <input type="hidden" name="deleteUserId" id="deleteUserId">
                                        <div class="modal-footer deleteModalUserFooter">
                                            <div class="row">
                                                <div class="col-md-12 widthModalDeleteIcon">
                                                    <center>
                                                        <img style="width: 22%;" src="<?php echo base_url() . 'assets/img/delete_gray.svg' ?>" style="">
                                                    </center>
                                                </div>
                                                <div class="col-md-12" style="">
                                                    <center> Are you sure you want to delete</center>
                                                </div>

                                                <div class="col-md-12" style="">
                                                    <center>
                                                        <label>Please type DELETE in below text box to confirm deletion of operator.</label><br>
                                                        <input type="text" placeholder="Please type DELETE" id="Deleted" class="textbox" required>
                                                    <center>
                                                </div>
                                                
                                                <div class="col-md-12" style="margin-top: 5px;">
                                                    <center>
                                                        <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_operator_submit">Delete</button>
                                                    </center>
                                                </div>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="modal fade" id="modalEditUserList" style="display: none;" aria-hidden="true">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header deletemodalTitleBackground">
                                    <h4 class="delteTitleStyle" class="modal-title">Edit User</h4>
                                    <button type="button" class="close" style="padding: 12px 32px !important;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
                                </div>
                                <div class="modal-body">
                                    <form class="p-b-20" action="edit_user" id="edit_user_form" method="POST" class="">

                                        <input type="hidden" name="userId" id="editUserId">
                                        <div class="row">
                                            <div class="column col-md-4">
                                                <div class="row">
                                                    <div class="col-md-4 m-t-40">
                                                        <div class="addOperatorModaluploadImage">
                                                            <img class="file-upload-image imgEditUserListStyle"  id="editUserImsge" src="<?php echo base_url() . 'assets/img/user-icon.png'; ?>">
                                                        </div>
                                                        <div class="upload-btn-wrapper editImage" style="width: 50px;">
                                                            <button class="btn1 pointer" type="button">
                                                                <img style="width: 38px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
                                                                <input name="userImage" title="Please select profile image" id="i_file" onchange="readURL(this);" type="file" accept="image/*">
                                                            </button>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="column col-md-8">
                                                <div class="col-md-12">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <div style="padding: 15px 5px;" class="form-group">
                                                                <label class="InputtypeHeaderText">Enter username</label>
                                                                <input type="text" class="form-control col-md-12 border-left-right-top-hide UserNameEdituserListStyle" id="editUserName" name="userName" placeholder="Enter name" required>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div style="padding: 15px 5px;" class="form-group">
                                                                <label class="InputtypeHeaderText">Select role</label>
                                                                <select name="userRole" id="editUserRole boxShadow" class="form-control UserEditStyleUserRole">
                                                                    <?php foreach ($roles as $key => $value) { ?>
                                                                    <option class="dropdown-item" value="<?php echo $value['rolesType']; ?>"><?php echo $value['rolesName']; ?></option>
                                                                    <?php } ?>
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-12">
                                                            <div style="" class="form-group">
                                                                <label class="InputtypeHeaderText">Current password</label>
                                                                <input disabled type="password" style="" class="form-control col-md-12 passwordIcon UserNameEdituserListStyle" id="currentPassword" >
                                                                <span id="iconcurrentPassword" style="background-color: white !important;float: right;margin-top: -29px;margin-right: -16px"><i onclick="showPassword('currentPassword','text');" style="margin-top: -5px;" class="fa fa-eye-slash"></i></span>
                                                            </div>
                                                        </div>

                                                        <div class="col-md-6">
                                                            <div style="" class="form-group">
                                                                <label class="InputtypeHeaderText">Change password</label>
                                                                <input type="password" class="form-control col-md-12 passwordIcon chnagePasswordInputType" name="userPassword" placeholder="Enter password" >
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <div style="" class="form-group">
                                                                <label></label>
                                                                <input type="password" style="margin-top:12px;" class="form-control col-md-12 passwordIcon chnagePasswordInputType" name="userConfirmpassword" placeholder="Confirm password" >
                                                            </div>
                                                        </div>

                                                        

                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-md-12" id="workingMachineHtml">
                                               
                                            </div>

                                            <div class="col-md-12" id="assignMachineHtml">
                                               
                                            </div>
                                        </div>
                                        <div>
                                            <center><button type="submit" class="btn btn-primary" id="edit_user_submit">Update</button></center>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>

<div class="modal fade" id="AddNewuser" tabindex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
    <div class="modal-dialog" role="document" style="min-width: 40%;">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title" id="liveMachineName" style="color:#ff8000;">Add new user</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                <form class="p-b-20" action="" method="POST" id="add_operator_form" enctype="multipart/form-data">
                    <div class="row">
                        <div class="column col-md-4">
                            <div class="row">
                                <div class="col-md-4 m-t-40">
                                    <?php if ($user->userImage == '') {
                                        $imgUrl = base_url() . 'assets/img/user/default.jpg';
                                    } else {
                                        $imgUrl = base_url() . 'assets/img/user/' . $user->userImage;
                                    } ?>
                                    <div class="addOperatorModaluploadImage" style="border-radius: 50%;border: 5px solid #002060;width: 150px;height: 150px;">
                                        <img name="userImage" style="margin: 35%;" class="file-upload-image" style="" src="<?php echo base_url() . 'assets/img/user-icon.png'; ?>" alt="<?php echo $user->userName; ?>">
                                    </div>
                                    <div class="upload-btn-wrapper editImage">
                                        <button class="btn1 pointer">
                                            <img style="width: 26px;" src="<?php echo base_url('assets/img/user/edit-photo.png'); ?>">
                                            <input name="userImage" style="margin:0px!important;" title="Please select profile image" id="i_file" onchange="readURL(this);" type="file" accept="image/*">
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="column col-md-8">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div style="padding: 15px 5px;" class="form-group">
                                            <label style="font-weight: 650;color:black;">Enter username</label>
                                            <input type="text" name="userName" style="padding: 0px;border-top: 0px;border-left: 0px;border-right: 0px;border-radius: 0px;padding-bottom: 12px;margin-top: 10%;" class="form-control col-md-12 border-left-right-top-hide" id="userName" name="userName" placeholder="Enter name" required>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="padding: 15px 5px;" class="form-group">
                                            <label style="font-weight: 650;color:black;">Select Role</label>
                                                    <select name="userRole" id="userRole" class="form-control">
                                                        <?php foreach ($roles as $key => $value) { ?>
                                                        <option class="dropdown-item" value="<?php echo $value['rolesType']; ?>"><?php echo $value['rolesName']; ?></option>
                                                        <?php } ?>
                                                    </select>
                                        </div>
                                    </div>

                                    <div class="col-md-6">
                                        <div style="padding: 15px 5px;" class="form-group">
                                            <label style="font-weight: 650;color:black;">Add password</label>
                                            <input type="password" style="padding: 0px;border-top: 0px;border-left: 0px;border-right: 0px;border-radius: 0px;padding-bottom: 12px;margin-top: 10%;" class="form-control col-md-12 passwordIcon" name="userPassword" id="userPassword" placeholder="Enter password" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div style="padding: 40px 5px;" class="form-group">
                                            <input type="password" style="padding: 0px;border-top: 0px;border-left: 0px;border-right: 0px;border-radius: 0px;padding-bottom: 12px;margin-top: 10%;" class="form-control col-md-12 passwordIcon" name="userConfirmpassword" id="userConfirmpassword" placeholder="Confirm password" required>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <button style="margin-left:10%;" type="submit" id="submit_new_Operator" class="text-center btn btn-sm btn-primary m-r-5 Addoperatorbuttonstyle">Save</button>
                        </div>
                        <div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modalAddNewRole" style="display: none;" aria-hidden="true">
    <div class="modal-dialog addnewRoleModal">
        <div class="modal-content">
            <div class="modal-header deletemodalTitleBackground">
                <h4 class="modal-title" id="liveMachineName" style="color:#ff8000;">Add new role</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body addnewroleModalRestyle">
                <form class="p-b-20 m-r-10 m-l-10" action="add_rolesResponsibilities" method="POST" id="add_rolesResponsibilities_form">
                    <div class="col-md-12">
                        <span class="AddNewRoleModalTitle">Role's name</span>
                        <div class="row m-l-0 m-r-0">
                            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name">
                                <input type="text" style="" class="form-control col-md-12 border-left-right-top-hide AddNewRoleInputType" name="rolesName" required>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: flex;">
                            <div class="col-md-6 addnewRoleModalbuttonStyle">
                                <span class="AssignModulesStyle">Assign modules</span>
                            </div>
                            <div class="col-md-3" style="display: flex;">
                                <i class="fa fa-eye p-t-4" style="color:#ff8000;" aria-hidden="true"></i>&nbsp;&nbsp;
                                <span style="color:#ff8000;">View</span>
                            </div>
                            <div class="col-md-3" style="display: flex;">
                                <img class="EditPencileStylee" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">&nbsp;&nbsp;
                                <span>Edit</span>
                            </div>
                        </div>

                        <?php foreach ($responsibilities as $key => $value) { ?>

                            <div class="col-md-12 m-t-15" style="display: flex;">
                                <div class="col-md-6 addnewRoleModalbuttonStyle">
                                    <span class="ResponsibilitiesNameStyles"><?php echo $value['responsibilitiesName']; ?>
                                        
                                    </span>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-6" style="float: left;">
                                        <div class="form-check-inline">
                                            <input class="form-check-input" name="isView[]" id="isViewClass<?php echo $value['responsibilitiesId']; ?>" onclick="checkSubResponsibilities(this.value,'view')" type="checkbox" value="<?php echo $value['responsibilitiesId']; ?>">
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="col-md-6" style="float: right;">
                                        <div class="form-check-inline">
                                            <input class="form-check-input" name="isEdit[]" id="isEditClass<?php echo $value['responsibilitiesId']; ?>" onclick="checkSubResponsibilities(this.value,'edit')" type="checkbox" value="<?php echo $value['responsibilitiesId']; ?>">
                                        </div>
                                    </div>
                                </div>
                            </div>


                            <?php foreach ($value['subResponsibilities'] as $keySub => $valueSub) { ?>

                                <div class="col-md-12 m-t-15" style="display: flex;">
                                    <div class="col-md-6 addnewRoleModalbuttonStyle">
                                        <span style="color: #b8b0b0 !important;margin-left: -10px;"><?php echo $valueSub['responsibilitiesName']; ?></span>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-6" style="float: left;">
                                            <div class="form-check-inline">
                                                <input disabled onclick="checkIsEdit(<?php echo $valueSub['responsibilitiesId']; ?>,<?php echo $value['responsibilitiesId']; ?>)" class="form-check-input isViewClass<?php echo $value['responsibilitiesId']; ?>" id="isViewId<?php echo $valueSub['responsibilitiesId']; ?>" name="isView[]" type="checkbox" value="<?php echo $valueSub['responsibilitiesId']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-3">
                                        <div class="col-md-6" style="float: right;">
                                            <div class="form-check-inline">
                                                <input disabled onclick="checkIsView(<?php echo $valueSub['responsibilitiesId']; ?>,<?php echo $value['responsibilitiesId']; ?>)" class="form-check-input isEditClass<?php echo $value['responsibilitiesId']; ?>" name="isEdit[]" type="checkbox" value="<?php echo $valueSub['responsibilitiesId']; ?>" id="isEditId<?php echo $valueSub['responsibilitiesId']; ?>">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php } ?>

                            <hr class="m-t-10" style="height: 2px !important;">
                        <?php } ?>
                        <center>
                            <button id="add_rolesResponsibilities_submit" type="submit" class="btn btn-primary m-r-5 m-t-15">Save</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="modal fade" id="modalEditNewRole" style="display: none;" aria-hidden="true">
    <div class="modal-dialog addnewRoleModal">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title" id="liveMachineName" style="color:#ff8000;">Edit roles</h4>
                <button type="button" class="close" data-dismiss="modal">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body addnewroleModalRestyle">
                <form class="p-b-20 m-r-10 m-l-10" action="edit_rolesResponsibilities" method="POST" id="edit_rolesResponsibilities_form">
                    <input type="hidden" id="editRolesId" name="rolesId" required>
                    <div class="col-md-12">
                        <span class="EditNewRoleStyleee">Role's name</span>
                        <div class="row editRoleStyleeRow">
                            <div style="padding: 0 5px;" class="form-group col-md-7" data-toggle="tooltip" data-title="Name">
                                <input type="text" class="form-control col-md-12 border-left-right-top-hide editRolesInputTypeText" id="editRolesName" name="rolesName" required>
                            </div>
                        </div>
                        <div class="col-md-12" style="display: flex;">
                            <div class="col-md-6  addnewRoleModalbuttonStyle">
                                <span class="AssigModulestextStyle">Assign modules</span>
                            </div>
                            <div class="col-md-3 addnewRoleModalbuttonStyle" style="display: flex;">
                                <i class="fa fa-eye p-t-4" style="color:#ff8000;" aria-hidden="true"></i>&nbsp;&nbsp;
                                <span style="color:#ff8000;">View</span>
                            </div>
                            <div class="col-md-3 addnewRoleModalbuttonStyle" style="display: flex;">
                                <img class="EditRolePencilStyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">&nbsp;&nbsp;
                                <span>Edit</span>
                            </div>
                        </div>

                        <div class="addnewRoleModalbuttonStyle" id="rolesResponsibilitiesHtml"></div>

                        <center>
                            <button id="edit_rolesResponsibilities_submit" type="submit" class="btn btn-primary m-r-5 m-t-15">Save</button>
                        </center>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>