<link href="<?php echo base_url('assets/css/taskMaintenance.css'); ?>" rel="stylesheet" />
<style>

.displayfilterrowstyle
{
  display:contents!important; 
}

.table>tbody>tr>td, .table>tbody>tr>th, .table>tfoot>tr>td, .table>tfoot>tr>th, .table>thead>tr>td, .table>thead>tr>th
{
  width: 150px;
    max-width: 200px;
    /*display: inline-block;*/
    white-space: normal;
    /*overflow: hidden;*/
    word-wrap: break-word;
}

</style>

<div id="content" class="content">

  <h1 class="page-header TaskAndMainTenanceHeaderStyle">SetApp error log</h1>

  <div class="row">
   
    <div class="col-md-12">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow:auto;">
        <div class="row" style="float:left;margin: 10px;">
          <span class="displayfilterrowstyle" id="showDueDate"></span>
          <span class="displayfilterrowstyle" id="showMachinefilter"></span>
          <span class="displayfilterrowstyle" id="showdeviceName"></span>
        </div>
        <div class="panel-body">



          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>

               <!--  <th class="TableHeaderTHstyle" style="min-width: 100px!important;">
                  <div id="filterMachineIdSelected" class="dropdown DropDownFilterStyle">
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterPhoneIdSelectedValue"> <?php echo deviceName; ?>&nbsp;</span>
                    <div class="dropdown-menu">
                      <?php foreach ($deviceNames as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input style="cursor: pointer;" onclick="filterPhoneId(<?php echo $value['phoneId'] ?>,'<?php echo $value['machineName'] ?>')" class="form-check-input filterPhoneId" type="checkbox" id="filterPhoneId<?php echo $value['phoneId'] ?>" name="filterPhoneId" value="<?php echo $value['phoneId']; ?>">
                            <label style="cursor: pointer;" class="form-check-label" for="filterPhoneId<?php echo $value['phoneId'] ?>">
                              <?php echo $value['model']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>
                -->

                <th class="TableHeaderTHstyle" style="min-width: 100px!important;">
                  <div id="filterMachineIdSelected" class="dropdown DropDownFilterStyle">
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterMachineIdSelectedValue"> <?php echo Machine; ?>&nbsp;</span>
                    <div class="dropdown-menu">
                      <?php foreach ($machines as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input style="cursor: pointer;" onclick="filterMachineId(<?php echo $value['machineId'] ?>,'<?php echo $value['machineName'] ?>')" class="form-check-input filterMachineId" type="checkbox" id="filterMachineId<?php echo $value['machineId'] ?>" name="filterMachineId" value="<?php echo $value['machineId']; ?>">
                            <label style="cursor: pointer;" class="form-check-label" for="filterMachineId<?php echo $value['machineId'] ?>">
                              <?php echo $value['machineName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <?php 
                if ($_GET && isset($_GET['dueDateValS'])) 
                {
                  $dueDateValE = date("F d, Y", strtotime($_GET['dueDateValE']));
                  $dueDateValS = date("F d, Y", strtotime($_GET['dueDateValS'])); 
                } 
                ?>

                <th class="TableHeaderTHstyle" style="min-width: 100px!important;">
                  <input type="hidden" name="dueDateValS" id="dueDateValS" value="<?php echo ($_GET && isset($_GET['dueDateValS'])) ? $_GET['dueDateValS'] : ''; ?>"/>  
                  <input type="hidden" name="dueDateValE" id="dueDateValE" value="<?php echo ($_GET && isset($_GET['dueDateValE'])) ? $_GET['dueDateValS'] : ''; ?>"/>   
                  <div id="due-daterange" name="due-daterange" class="daterangeandduedatestyle">
                    <span>
                      <?php echo Addeddate; ?>&nbsp;&nbsp;
                    </span> 
                    <i class="fa fa-caret-down m-t-2"></i>
                  </div>
                  <small  class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                  <th class="TableHeaderTH2style" style="min-width: 100px!important;"><?php echo tag; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th> 

                <th class="TableHeaderTH2style" style="min-width: 600px!important;"><?php echo message; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

               <!--  <th class="TableHeaderTH2style" style="min-width: 100px!important;"><?php echo deviceName; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th> -->

                <th class="TableHeaderTHstyle" style="min-width: 100px!important;">
                  <div id="filterPhoneIdSelected" class="dropdown DropDownFilterStyle">
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterPhoneIdSelectedValue"> <?php echo deviceName; ?>&nbsp;</span>
                    <div class="dropdown-menu">
                      <?php foreach ($deviceNames as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" >
                            <input style="cursor: pointer;" onclick="filterPhoneId(<?php echo $value['machineId'] ?>,'<?php echo $value['model'] ?>')" class="form-check-input filterPhoneId" type="checkbox" id="filterPhoneId<?php echo $value['machineId'] ?>" name="filterPhoneId" value="<?php echo $value['machineId']; ?>">
                            <label style="cursor: pointer;" class="form-check-label" for="filterPhoneId<?php echo $value['machineId'] ?>">
                              <?php echo $value['model']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <th class="TableHeaderTH2style" style="min-width: 100px!important;"><?php echo appVersion; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <th class="TableHeaderTH2style" style="min-width: 100px!important;"><?php echo appId; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>
                
                <th class="TableHeaderTH2style"><?php echo level; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <th class="TableHeaderTH2style"><?php echo stacktrace; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <th class="TableHeaderTH2style" style="min-width: 100px!important;"><?php echo SDKversion; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>

                <th class="TableHeaderTH2style"><?php echo syncId; ?><br>
                  <small class="tableColumnsmallStyle">&nbsp;</small>
                </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div> 
  </div>



  <hr style="background: gray;">
  <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>




  


