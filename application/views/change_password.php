		<div id="content" class="content">
			
			<h1 class="page-header">Profile</h1>
			<div class="row">
				<div class="col-md-12">
				  <div class="panel " data-id="widget">
						<div class="panel-heading" > 
							<h4 class="panel-title">Change password</h4>
						</div>
						<div class="panel-body widget-list-item">
						<form id="change_password" class="row" method="post" >	
							<input type="hidden" name="userId" id="userId" value="<?php echo $this->session->userdata('userId'); ?>" />
							<div class="col-md-2" >
    							<div class="profile-header-img m-b-15">
    								<?php if(!$this->session->userdata('userImage')) { 
            						    $imgUrl = base_url().'assets/img/user/default.jpg';  
            						} else {
            						    $imgUrl = base_url().'assets/img/user/'.$this->session->userdata('userImage');   
            						}?>
        							<img src="<?php echo $imgUrl; ?>" alt="<?php echo $this->session->userdata('userName'); ?>" style="width:100%" id="userProfile" />
    							</div>
    							<h4 class="pull-left " >
    								<?php echo $roleName;  ?> 
    							</h4>
    						</div>
							<div class="table-responsive col-md-9"> 
								<?php if($this->session->flashdata('success_message') != '' ) { ?>
            					<div>
            						<div class="alert alert-success fade show m-b-10">
            							<span class="close" data-dismiss="alert">×</span>
            							<?php echo $this->session->flashdata('success_message'); ?>
            						</div>
            					</div>
            					<?php } ?> 
								<table class="table table-profile">
						            <tbody>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr classs="form-group" >
										    <td class="field">Current password</td>
										    <td><input type="password" name="userCurrentPassword" id="userCurrentPassword" class="form-control input" required /></td>
										</tr>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr classs="form-group" >
										    <td class="field">New password</td>
										    <td><input type="password" name="userNewPassword" id="userNewPassword" class="form-control input" required minlength=5 /></td>
										</tr>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr classs="form-group" >
										    <td class="field">Confirm new password</td>
										    <td><input type="password" name="userConfirmPassword" id="userConfirmPassword" class="form-control input" required minlength=5 /></td>
										</tr>
										<tr class="divider">
										    <td colspan="2"></td>
										</tr>
										<tr class="highlight form-group">
    										<td class="field">&nbsp;</td>
    										<td class="p-t-10 p-b-10">
    											<button type="submit" class="btn btn-primary " id="change_password_submit" > Change password </button>
    										</td>
    									</tr>
									</tbody>
					            </table>
					        </div>
						</form>	
						</div>
					</div>
				</div> 
			</div>
			<hr style="background: gray;">
            <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
		</div>
	

