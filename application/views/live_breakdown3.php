<style type="text/css">
    .card2:nth-child(1) svg circle:nth-child(2)
    {
        stroke-dashoffset:<?php echo 440 - (440 * 100) / 100; ?>;
        stroke-linecap: round;
        stroke: #002060;
    }
</style>

     <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script> 
        <link href="<?php echo base_url('assets/css/breakdown3.css');?>" rel="stylesheet" /> 
        <div id="content" class="content">
            
            <h1 class="page-header H1pageTitle"><?php echo Analytics; ?></h1>

            <br />
            <div class="row">
                <input type="hidden" id="analytics" value="stopAnalytics">
                <div class="col-md-3" style="padding-left: 0px;">
                   <small class="SmallStyle">
                      <a class="Alinkstyle" href="<?php echo base_url('TestToLive/live_dashboard3'); ?>"> <?php echo Analytics; ?> </a> &gt; <?php echo Stopanalytics; ?>
                    </small>
                </div>
                <div class="col-md-1 show_by Colmd1ShowBystyle">
                    <label><?php echo showby; ?> :</label>
                </div>
                <div class="col-md-2 Colmd2style">
                    <select disabled="" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                        <option class="new_test" value="day"><?php echo Day; ?></option>
                    </select>
                </div>
                <div class="col-md-1 day show_by">
                    <label class="LabelForStyle"> <?php echo Fors; ?>:&nbsp;</label>
                </div> 
                <div class="col-md-2 day colmd2Stylee">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%;" value="<?php echo date('m/d/Y'); ?>">
                </div>
                <div class="col-md-2 weekly" style="display: none;">
                    <select class="form-control" id="choose3" onchange="changeFilterValue()">
                        <option value="1"><?php echo week.' 1'; ?> </option>
                        <option value="2"><?php echo week.' 2'; ?> </option>
                        <option value="3"><?php echo week.' 3'; ?> </option>
                        <option value="4"><?php echo week.' 4'; ?> </option>
                        <option value="5"><?php echo week.' 5'; ?> </option>
                        <option value="6"><?php echo week.' 6'; ?> </option>
                        <option value="7"><?php echo week.' 7'; ?> </option>
                        <option value="8"><?php echo week.' 8'; ?> </option>
                        <option value="9"><?php echo week.' 9'; ?> </option>
                        <option value="10"><?php echo week.' 10'; ?> </option>
                        <option value="11"><?php echo week.' 11'; ?> </option>
                        <option value="12"><?php echo week.' 12'; ?> </option>
                        <option value="13"><?php echo week.' 13'; ?> </option>
                        <option value="14"><?php echo week.' 14'; ?> </option>
                        <option value="15"><?php echo week.' 15'; ?> </option>
                        <option value="16"><?php echo week.' 16'; ?> </option>
                        <option value="17"><?php echo week.' 17'; ?> </option>
                        <option value="18"><?php echo week.' 18'; ?> </option>
                        <option value="19"><?php echo week.' 19'; ?> </option>
                        <option value="20" selected=""><?php echo week.' 20'; ?> </option>
                        <option value="21"><?php echo week.' 21'; ?> </option>
                        <option value="22"><?php echo week.' 22'; ?> </option>
                        <option value="23"><?php echo week.' 23'; ?> </option>
                        <option value="24"><?php echo week.' 24'; ?> </option>
                        <option value="25"><?php echo week.' 25'; ?> </option>
                        <option value="26"><?php echo week.' 26'; ?> </option>
                        <option value="27"><?php echo week.' 27'; ?> </option>
                        <option value="28"><?php echo week.' 28'; ?> </option>
                        <option value="29"><?php echo week.' 29'; ?> </option>
                        <option value="30"><?php echo week.' 30'; ?> </option>
                        <option value="31"><?php echo week.' 31'; ?> </option>
                        <option value="32"><?php echo week.' 32'; ?> </option>
                        <option value="33"><?php echo week.' 33'; ?> </option>
                        <option value="34"><?php echo week.' 34'; ?> </option>
                        <option value="35"><?php echo week.' 35'; ?> </option>
                        <option value="36"><?php echo week.' 36'; ?> </option>
                        <option value="37"><?php echo week.' 37'; ?> </option>
                        <option value="38"><?php echo week.' 38'; ?> </option>
                        <option value="39"><?php echo week.' 39'; ?> </option>
                        <option value="40"><?php echo week.' 40'; ?> </option>
                        <option value="41"><?php echo week.' 41'; ?> </option>
                        <option value="42"><?php echo week.' 42'; ?> </option>
                        <option value="43"><?php echo week.' 43'; ?> </option>
                        <option value="44"><?php echo week.' 44'; ?> </option>
                        <option value="45"><?php echo week.' 45'; ?> </option>
                        <option value="46"><?php echo week.' 46'; ?> </option>
                        <option value="47"><?php echo week.' 47'; ?> </option>
                        <option value="48"><?php echo week.' 48'; ?> </option>
                        <option value="49"><?php echo week.' 49'; ?> </option>
                        <option value="50"><?php echo week.' 50'; ?> </option>
                        <option value="51"><?php echo week.' 51'; ?> </option>
                        <option value="52"><?php echo week.' 52'; ?> </option>
                    </select>
                </div>
                <div class="col-md-1 weekly" style="display: none;">
                </div>
                <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
                <div class="col-md-1 monthly show_by colmd1showbystyle">
                    <label> for </label>
                </div>
                <div class="col-md-2 monthly colmd2Monthly">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker1" id="choose4">
                </div>
                <div class="col-md-2 yearly" style="display: none;">
                    <select class="form-control"  id="choose5" onchange="changeFilterValue()">
                        <option value="2018">2018</option>
                        <option value="2019">2019</option>
                        <option selected="" value="2020">2020</option>
                    </select>
                </div>
                <div class="col-md-1 yearly" style="display: none;"></div>
            </div>
            
            <div class="row" style="margin-top: 13px;">
                    <input type="hidden" name="" id="machineIdStop" value="<?php echo $listMachines[0]->machineId; ?>">
                        <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false" style="width: 100%;">
                        <div class="carousel-inner no-padding">
                            <div class="carousel-item active">
                            <?php 
                                $countQuery = 1;
                                $i = 1;
                                foreach($listMachines as $machine) { ?>
                                    <?php if($countQuery % 4 == 0){ 
                                               ?>
                                        </div>
                                        <div class="carousel-item">
                                      <?php   } ?>
                                    <div style="margin-left: -7px;margin-right: 3px;" class="col-xs-3 col-sm-3 col-md-3 <?php if($i == 1){ echo "active"; } ?>">
                                        <a href="javascript:;" class="btn btn-default btn-lg filter1 <?php if($i == 1){ echo "active"; } ?> machineNameText" id="machineStop<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                                    </div>
                                <?php $i++; if ($i != 4) {
                                   $countQuery++;
                                }   } ?>
                            </div>
                        </div>
                        <?php if ($i > 4) { ?>
                        <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
                            <span class="carousel-control-prev-icon carouselcontrollerstyle">
                                <i class="fa fa-angle-left carouselIconStyle" aria-hidden="true"></i>
                            </span>
                        </a>
                        <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
                            <span class="carousel-control-next-icon carouselcontrollerstyle">
                                <i class="fa fa-angle-right carouselIconStyle" aria-hidden="true"></i>
                            </span>
                        </a>
                    <?php } ?>
                    </div>
            </div>

        <div class="graph-loader hide" ></div>

        <div class="row graphloaderrowstyle">
        <input type="hidden" min="0" class="inputText" id="duration1" name="" value="0">
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <div class="card-body">
                <h4 class="cardcard2h4style">0 <?php echo mins; ?> - <input type="text" min="0" class="inputText" id="duration2" name="" value="5">  <?php echo mins; ?></h4>
                <div class="widget widget-stats widget1" id="filter1widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70">
                                            
                            </circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter1count">0 <?php echo stops; ?></h3>
                                <small class="secondText" id="filter1">0 <?php echo mins; ?></small><br>
                                <small class="thirdText"><?php echo Totaltime; ?></small>
                            </div>
                        </svg>
                    </center>
                </div>
              </div>
            </div>
        </div>
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <div class="card-body">
                    <h4 class="cardcard2h4style">
                        <span class="duration2Text">5 <?php echo mins; ?></span> - <input type="text" min="0" class="inputText" id="duration3" name="" value="10">  <?php echo mins; ?></h4>
                    <div class="widget widget-stats widget1" id="filter2widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70">
                            </circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter2count">0 <?php echo stops; ?></h3>
                                <small class="secondText" id="filter2">0 <?php echo mins; ?></small><br>
                                <small class="thirdText"><?php echo Totaltime; ?></small>
                            </div>
                        </svg>
                    </center>
                  </div>
                </div>
            </div>
        </div>

        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <div class="card-body">
                    <h4 class="cardcard2h4style"><span class=
                        "duration3Text">10 <?php echo mins; ?></span> - <input type="text" min="0" class="inputText" id="duration4" name="" value="15">  <?php echo mins; ?></h4>
                    <div class="widget widget-stats widget1" id="filter3widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter3count">0 <?php echo stops; ?></h3>
                                    <small class="secondText" id="filter3">0 <?php echo mins; ?></small><br>
                                    <small class="thirdText"><?php echo Totaltime; ?></small>
                                </div>
                            </svg>
                        </center>
                  </div>
                </div>
            </div>
        </div>
        
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <div class="card-body">
                   <h4 class="cardcard2h4style">
                    <span class="duration4Text">15 <?php echo mins; ?></span> - <img style="margin-top: -5px;" src="<?php echo base_url('assets/img/infiniti.png'); ?>"></h4>
                    <div class="widget widget-stats widget1" id="filter4widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter4count">0 <?php echo stops; ?></h3>
                                    <small class="secondText" id="filter4">0 <?php echo mins; ?></small><br>
                                    <small class="thirdText"><?php echo Totaltime; ?></small>
                                </div>
                            </svg>
                        </center>
                  </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content boxShadow row tabcontentstyle">
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle"><?php echo Receiveerrornotification; ?>: <span id="errorNotification"></span></h4>
            </div>
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle"><?php echo Providereasonforerrors; ?> : <span id="errorReasonNotification"></span></h4>
            </div>
        </div>
        <div class="row m-r-10">
            <div class="col-md-6">
                <div class="tab-content boxShadow">
                <div class="tabcontentMaindivStyle">
                    <button id="filter1widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">0 <?php echo mins; ?> - 
                        <span class="duration2Text">5 <?php echo mins; ?></span>
                    </button>
                    <button id="filter2widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration2Text">5 <?php echo mins; ?></span> - <span class="duration3Text">10 <?php echo mins; ?></span>
                    </button>
                    <button id="filter3widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration3Text">10 <?php echo mins; ?></span> - <span class="duration4Text">15 <?php echo mins; ?></span>
                    </button>
                    <button id="filter4widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration4Text">15 <?php echo mins; ?></span> - <img src="<?php echo base_url('assets/img/white_infinity.svg'); ?>">
                    </button>
                </div>

                <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime1" style="height:350px;" ></div> 
                                <div class="TimeStampGraphStyle"><?php echo TIMESTAMPHHMMSS; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tab-content boxShadow" >
                    <div class="tabcontentMaindivStyle">
                        &nbsp;
                    </div>
                    <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime2" style="height:368px;" ></div> 
                                <div class="ReasonDurationGraphStyle"><?php echo REASONDURATIONMIN; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            <hr style="background: gray;">
            <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
        </div>
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>


   