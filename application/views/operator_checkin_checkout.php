<link href="<?php echo base_url('assets/css/operator_checkin_checkout.css');?>" rel="stylesheet" />
<style>
  .displayfilterrowstyle
  {
    display:contents!important; 
  }
  #empTable_paginate {
    border-radius: 15px!important;
    padding-left: 0!important;
    box-shadow: grey 0 0 8px -2px!important;
    width: 30%!important;
    margin: AUTO!important;
</style>
<div id="content" class="content">
 
    <h1 style="font-size: 22px;color: #002060;line-height: 17px !important;" class="page-header"><?php echo Operatorreports; ?></h1>
    <small style="font-size: 12px !important;font-weight: 600 !important;padding-left: 2px;color: #002060;">
      <a style="color: #002060 !important;" href="<?php echo base_url('Operator/operator_details');?>"> <?php echo Operators; ?> </a> > <?php echo Operatorreports; ?>
    </small>
  
  <div class="row" style="margin-top: 55px;">
  <input type="hidden" id="activeId"  value="0">
    <div class="col-md-12 table_data" style="padding-left:12px;">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >
        <div class="panel-body">
          <div class="row" style="float:left;margin:10px;">
            <div class="displayfilterrowstyle" id="showUserFilter"></div>
            <div class="displayfilterrowstyle" id="showMachinefilter"></div>
            <div class="displayfilterrowstyle" id="showCheckedIn"></div>
            <div class="displayfilterrowstyle" id="showCheckOut"></div>
          </div>
          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>

                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterUserNameSelected" class="dropdown" style="padding: 4px;border-radius: 5px;min-width: 71px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> <?php echo Operator; ?>&nbsp;</span>
                    <div class="dropdown-menu">
                     <?php foreach ($users as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterUserName(<?php echo $value['userId'] ?>,'<?php echo $value['userName'] ?>')" 
                            class="form-check-input filterUserName" type="checkbox" id="filterUserName<?php echo $value['userId'] ?>" 
                            name="filterUserName" value="<?php echo $value['userId']; ?>">
                            <label class="form-check-label" for="filterUserName<?php echo $value['userId'] ?>">
                              <?php echo $value['userName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
              <!--    <label style="background-color: #FF8000;min-width: 40px;width: max-content;border-radius: 10px;color: white;" id="filterUserNamee"></label> -->
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;">
                  <div id="filterMachineNameSelected" class="dropdown" style="padding: 4px;border-radius: 5px;min-width: 90px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> <?php echo Workingmachine; ?>&nbsp;</span>
                    <div class="dropdown-menu">
                      <?php foreach ($machines as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterMachineName(<?php echo $value['machineId'] ?>,'<?php echo $value['machineName'] ?>')"
                            class="form-check-input filterMachineName" type="checkbox" id="filterMachineName<?php echo $value['machineId'] ?>"
                             name="filterMachineName" value="<?php echo $value['machineId']; ?>">
                            <label class="form-check-label" for="filterMachineName<?php echo $value['machineId'] ?>">
                              <?php echo $value['machineName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>


                <?php 
                  if ($_GET && isset($_GET['dateValS'])) 
                  {
                    $dateValE = date("F d, Y", strtotime($_GET['dateValE']));
                    $dateValS = date("F d, Y", strtotime($_GET['dateValS'])); 
                  } 
                 ?>

                <th  style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;" >
                  <input type="hidden" name="startDateValS" id="startDateValS" value="<?php echo date("Y-m-d", strtotime("-29 day")); ?>"/>  
                  <input type="hidden" name="startDateValE" id="startDateValE" value="<?php echo date("Y-m-d", strtotime("29 day")); ?>"/>   
                  <div id="checkin-daterange" name="checkin-daterange" style="width: 100px;">
                    <span>
                    <?php echo Checkedin; ?>&nbsp;&nbsp;
                    </span> 
                    <i class="fa fa-caret-down m-t-2"></i>
                  </div>
                  <small  style="color: #FF8000;text-align : center;" >
                    &nbsp;
                  </small>
                </th>

                <th  style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;" >
                  <input type="hidden" name="endDateValS" id="endDateValS" value="<?php echo ($_GET && isset($_GET['dateValS'])) ? $_GET['dateValS'] : ''; ?>"/>  
                  <input type="hidden" name="endDateValE" id="endDateValE" value="<?php echo ($_GET && isset($_GET['dateValE'])) ? $_GET['dateValS'] : ''; ?>"/>   
                  <div id="checkout-daterange" name="checkout-daterange" style="width: 100px;">
                    <span>
                    <?php echo Checkedout; ?>&nbsp;&nbsp;
                    </span> 
                    <i class="fa fa-caret-down m-t-2"></i>
                  </div>
                  <small  style="color: #FF8000;text-align : center;" >
                    &nbsp;
                  </small>
                </th>
                
                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;"><?php echo Reports; ?><br><small style="color: #FF8000;text-align : center;" >&nbsp;</small></th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div> 
            
    <div class="col-md-3 detail" style="margin-top: -133px;display: none;margin-bottom: -63px;background: #FFFFFF !important;border-bottom-left-radius: 5px;    border-bottom-right-radius: 5px;box-shadow: gray -10px 10px 10px -10px;height: 100%;position: fixed;right: 0%;width: 23%;overflow: auto;">
       <div class="panel panel-inverse panel-primary" >
        <div class="panel-body" style="padding: 0;">
          <div class="row" style="padding: 8px;padding-top: 0 !important;background: #002060;height: 190px; border-top-left-radius: 5px;    border-top-right-radius: 5px;">
              <div class="col-md-12">
                <div style="padding: 14px 0 0 0;">
                  <a href="javascript:void(0);" onclick="closeDetails();"><img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
                </div>
              </div>
              <div class="col-md-4" style="float: left">
                <img id="userImage" style="border-radius: 50%;border: 3px solid #FFFFFF" height="85" width="80" src="">  
              </div>
              <div class="col-md-8" style="float: left;margin-top: 27px;">
                <span id="userName" style="font-size: 17px;color: #FF8000;font-weight: 700;"></span> <br>
                <small style="color: #FFFFFF;font-size: 80%;" id="userId"></small>
              </div>
            </div>

            <form class="p-b-20" >
              <div class="row" style="margin-top: 25px;">
                <div class="col-md-12" style="padding-left: 26px;">

                  <div style="font-weight: 600;font-size: 80%;"><?php echo Reports; ?></div>
                  <h3 style="color: #FF8000;" id="reportCount"></h3>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>
        
              <div class="row">
                <div class="col-md-12" style="padding-left: 26px;">
                  <div style="font-weight: 600;font-size: 80%;margin-bottom: 7px;"><?php echo Checkedin; ?></div>
                  <div style="font-weight: 600;font-size: 12px;margin-bottom: 7px;"><?php echo Date; ?>:&nbsp; <span style="color: #124D8D;" id="startDate"></span></div>
                  <div style="font-weight: 600;font-size: 12px;"><?php echo Time; ?>:&nbsp; <span style="color: #124D8D;" id="startTime"></span></div>
                  <br>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>

              <div class="row">
                <div class="col-md-12" style="padding-left: 26px;">
                  <div style="font-weight: 600;font-size: 80%;margin-bottom: 7px;"><?php echo Checkedout; ?></div>
                  <div style="font-weight: 600;font-size: 12px;margin-bottom: 7px;"><?php echo Date; ?>:&nbsp; <span style="color: #124D8D;" id="endDate"></span></div>
                  <div style="font-weight: 600;font-size: 12px;"><?php echo Time; ?>:&nbsp; <span style="color: #124D8D;" id="endTime"></span></div>
                   <br>
                  
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>

              <div class="row">
                <div class="col-md-12" style="padding-left: 26px;">
                  <div style="font-weight: 600;font-size: 80%;margin-bottom: 7px;"><?php echo Workingmachine; ?></div>
                  <div style="font-weight: 600;font-size: 12px;margin-bottom: 7px;"><span style="color: #124D8D;" id="workingMachineName"></span></div>
                </div>
              </div>
          </form>
                </div>
              </div>
            </div>
        </div>
        <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>
<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
  

