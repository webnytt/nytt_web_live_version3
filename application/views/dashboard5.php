<link rel="stylesheet" href="<?php echo base_url('assets/scroller_timepicker/picker.css'); ?>">
<link href="<?php echo base_url('assets/css/dashboard5.css');?>" rel="stylesheet" /> 
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  

<style>
    .btn-primary.active, .btn-primary.active.focus, .btn-primary.active:focus, .btn-primary.active:hover, .btn-primary:active, .btn-primary:active.focus, .btn-primary:active:focus, .btn-primary:active:hover, .btn-primary:focus, .btn-primary:hover, .btn-primary:not(:disabled):not(.disabled).active, .btn-primary:not(:disabled):not(.disabled):active, .open > .dropdown-toggle.btn-primary, .open > .dropdown-toggle.btn-primary:focus, .open > .dropdown-toggle.btn-primary:hover, .show > .btn-primary.dropdown-toggle
{
    background-color: red !important;
    border-color: red !important;
}
@media screen and (min-device-width: 768px) and (max-width: 999px) 
{
    .minwidthcolmdStyle
    {
    	min-width: 50%!important;   
    }
    .minwidthcolmdStylee
    {
    	min-width: 30%!important;   
    }
    .minwidthcolmdStyleeshowby
    {
    	min-width: 15%!important;
    	margin-top: 10px;   
    }
    .minwidthCOLMDTODAY
    {
    	min-width: 45%!important;
    	margin-top: 10px;
    }
    .minwidthshiftsStyle
    {
    	min-width: 12%!important;
    	margin-top: 10px;
    }
    .machineviewShiftsStyle
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    }
    .machineWeeklyStylees
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    } 

    .machineYearlyStylees
    {
    	min-width: 30%!important;
    	margin-top: 10px;
    }
}
.slick-prev
{
    box-shadow: grey 0 0 8px -2px!important;
}
.slick-next
{
    box-shadow: grey 0 0 8px -2px!important;
}

.select2-container
{
    /*display: block!important;*/
    height: 34px;
}
.select2-container--default .select2-selection--single
{
    border: 0px;
    border: none;
    box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;
    border-radius: 5px;
}
@media screen and (min-device-width: 320px) and (max-width: 374px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
} 
@media screen and (min-device-width: 375px) and (max-width: 424px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
}
@media screen and (min-device-width: 425px) and (max-width: 767px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 100%;
    }  
}
@media screen and (min-device-width: 768px) and (max-width: 1023px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container
    {
        min-width: 175px!important;
    }
}
@media screen and (min-device-width: 1320px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
    }  
    .select2-container
    {
        min-width: 175px!important;
    }

} 

</style>

<div id="content" class="content">
    <h1 style="" class="page-header h1headerpagestyle"><?php echo Analytics; ?></h1>
    <br />

    <div class="row">
        <div class="col-md-3 minwidthcolmdStyle" style="display:flex;padding-left: 0px;">
            <?php if ($accessPointStopView == true) { ?>
            <a href="<?php echo base_url('Analytics/breakdown'); ?>"class="btn btn-primary stopAnalyticsButtonStyle"><?php echo Stopanalytics ;?></a>
            <?php } ?>
            <?php if ($accessPointWaitView == true) { ?>
            <a style="background-color: #FFCF00 !important;border-color: #FFCF00 !important;" href="<?php echo base_url('Analytics/waitBreakdown'); ?>"class="btn btn-primary stopAnalyticsButtonStyle"><?php echo Waitanalytics; ?></a>
            <?php } ?>
        </div>

        <div class="col-md-1 show_by minwidthcolmdStyleeshowby">
            <label><?php echo Show; ?>: </label></div>
        <div class="col-md-2 minwidthcolmdStylee" style="padding-left: 5px;padding-right: 0px;">
            <select name="showcalender" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                <option class="new_test" value="day"><?php echo Day; ?></option>
                <option class="new_test" value="weekly"><?php echo week; ?></option>
                <option class="new_test" value="monthly"><span> <?php echo MONTH; ?> </span></option>
                <option class="new_test" value="yearly"><span> <?php echo year; ?> </span></option>
            </select>
        </div>
       <!--  <div class="col-md-1 day show_by colmd1dayshow_by">
            <label class="colmd1dayshow_byLabel"> </label>
        </div> -->

        <div class="col-md-2 day colmd2day minwidthCOLMDTODAY yearwidthstyle" style="padding-left: 10px!important;max-width: 23% !important;
    flex: 0 0 23% !important;">
            <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%!important;margin-right:0%!important;height: 34px!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" value="<?php echo date('m/d/Y'); ?>">
        </div>

        
        <div class="col-md-2 weekly machineWeeklyStylees yearwidthstyle" style="display: none;max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="Weekly" style="display: block!important;min-width:200px!important;" class="form-control margintopstyle" id="choose3" onchange="changeFilterValue()">
                <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1"><?php echo week.' 1'; ?></option>
                <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2"><?php echo week.' 2'; ?></option>
                <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3"><?php echo week.' 3'; ?></option>
                <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4"><?php echo week.' 4'; ?></option>
                <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5"><?php echo week.' 5'; ?></option>
                <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6"><?php echo week.' 6'; ?></option>
                <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7"><?php echo week.' 7'; ?></option>
                <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8"><?php echo week.' 8'; ?> </option>
                <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9"><?php echo week.' 9'; ?> </option>
                <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10"><?php echo week.' 10'; ?></option>
                <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11"><?php echo week.' 11'; ?></option>
                <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12"><?php echo week.' 12'; ?></option>
                <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13"><?php echo week.' 13'; ?></option>
                <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14"><?php echo week.' 14'; ?></option>
                <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15"><?php echo week.' 15'; ?></option>
                <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16"><?php echo week.' 16'; ?></option>
                <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17"><?php echo week.' 17'; ?></option>
                <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18"><?php echo week.' 18'; ?></option>
                <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19"><?php echo week.' 19'; ?></option>
                <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20"><?php echo week.' 20'; ?></option>
                <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21"><?php echo week.' 21'; ?></option>
                <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22"><?php echo week.' 22'; ?></option>
                <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23"><?php echo week.' 23'; ?></option>
                <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24"><?php echo week.' 24'; ?></option>
                <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25"><?php echo week.' 25'; ?></option>
                <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26"><?php echo week.' 26'; ?></option>
                <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27"><?php echo week.' 27'; ?></option>
                <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28"><?php echo week.' 28'; ?></option>
                <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29"><?php echo week.' 29'; ?></option>
                <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30"><?php echo week.' 30'; ?></option>
                <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31"><?php echo week.' 31'; ?></option>
                <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32"><?php echo week.' 32'; ?></option>
                <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33"><?php echo week.' 33'; ?></option>
                <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34"><?php echo week.' 34'; ?></option>
                <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35"><?php echo week.' 35'; ?></option>
                <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36"><?php echo week.' 36'; ?></option>
                <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37"><?php echo week.' 37'; ?></option>
                <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38"><?php echo week.' 38'; ?></option>
                <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39"><?php echo week.' 39'; ?></option>
                <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40"><?php echo week.' 40'; ?></option>
                <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41"><?php echo week.' 41'; ?></option>
                <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42"><?php echo week.' 42'; ?></option>
                <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43"><?php echo week.' 43'; ?></option>
                <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44"><?php echo week.' 44'; ?></option>
                <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45"><?php echo week.' 45'; ?></option>
                <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46"><?php echo week.' 46'; ?></option>
                <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47"><?php echo week.' 47'; ?></option>
                <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48"><?php echo week.' 48'; ?></option>
                <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49"><?php echo week.' 49'; ?></option>
                <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50"><?php echo week.' 50'; ?></option>
                <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51"><?php echo week.' 51'; ?></option>
                <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52"><?php echo week.' 52'; ?></option>
            </select>
        </div>

        <!-- <div class="col-md-1 weekly" style="display: none;"></div> -->
        <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
        <!-- <div class="col-md-1 monthly show_by colmdmonthlyshowbyStyle"></div> -->

        <div class="col-md-2 monthly colmdmonthlyStyle" style="display: none;padding-left: 10px!important;">
            <input onchange="changeFilterValue()" type="text" style="height:34px!important;margin-right:0px!important;width:100%!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" class="form-control datepicker1 yearwidthstyle" id="choose4">
        </div>

        <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
        <div class="col-md-2 yearly machineYearlyStylees yearwidthstyle" style="display: none; max-width: 23% !important;flex: 0 0 23% !important;">
            <select name="showcalenderyear" class="form-control" style="border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" id="choose5" onchange="changeFilterValue()">
                <?php for($i=0 ; $i < $years; $i++){ 
                    $yearValue = 2018 + $i;
                    ?>
                    <option <?php if ($yearValue == date('Y')) {
                        echo "selected";
                    } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                <?php } ?>
            </select>
        </div>
        <!-- <div class="col-md-1 yearly" style="display: none;"></div> -->

        <div class="col-md-1 machineView minwidthshiftsStyle"><span class="viewddLabelstyle"><?php if ($accessPointViewView == true) { ?> <?php echo shifts; ?> : <?php } ?> </span></div>

        <div class="col-md-2 machineView machineviewShiftsStyle">
            <input type="hidden" id="viewId" value="none">
            <?php if ($accessPointViewView == true) { ?>
                <div class="dropdown viewdropdownstyle">
                    <div class="dropdown-toggle" data-toggle="dropdown">
                        <span style="padding-left: 8px;width: 90%!important;" class="machineNameText" id="selectedView">24 <?php echo Hours; ?> </span>
                    </div>
                    <div class="dropdown-menu dropdownMenustyle">
                        <div class="dropdown-item selectedView" id="viewIdnone" onclick="selectView('none','24 <?php echo Hours; ?>')" style="font-weight: 600;">
                            24 <?php echo Hours; ?> 
                        <hr class="dropdownHRstyle">
                        </div>

                        <?php foreach ($machineView as $key => $value) { ?>
                            <div class="dropdown-item" id="viewId<?php echo $value['viewId']; ?>" style="font-weight: 600;">
                                <span class="machineNameText" style="width:84%;" onclick="selectView(<?php echo $value['viewId']; ?>,'<?php echo $value['viewName']; ?>')">
                                    <?php echo $value['viewName']; ?>
                                </span> 
                                <?php if ($accessPointViewsEdit == true) { ?>
                                    <span style="float: right;">
                                        <img onclick="editView(<?php echo $value['viewId']; ?>)" style="width: 20px;" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                    </span> 
                                <?php } ?>
                                <hr class="dropdownHRstyle">
                            </div>

                        <?php } ?>

                        <?php if ($accessPointViewsEdit == true) { ?>
                            <div class="dropdown-item dropdownItemstyle">
                                <a data-toggle="modal" data-target="#add-view-modal">+ <?php echo Addnewshift; ?></a> 
                            </div>
                        <?php } ?>
                    </div>
                </div>
            <?php } ?>
        </div>
    </div>

    <?php $machineNameList = $listMachines->result_array(); ?>

    <div class="row">
        <!-- <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>"> -->

        <section class="regular slider">
            <div>
                <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> 
                <?php echo Showall; ?> </a>
            </div>

            <?php foreach($machineNameList as $key => $machine) { 
                 if($machine['machineName'] != "GPIO15"){ ?>
            <div>
                <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }?>" id="machine<?php echo $machine['machineId']; ?>"> <?php echo ($machine['machineName'] == "GPIO14") ? "Robot 1035" : $machine['machineName']; ?>
                </a>
            </div>
            <?php } } ?>
        </section>
    </div>

    
    <div class="row" id="chartwrap" class="machineModerowMainStyle">
        <div class="col-md-5 colomd5machineModeStyle" style="display: none;">
            <div class="tab-content boxShadow machineModeStyle">
                <div class="tab-pane fade active show">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-md-12" >
                            <h4 class="h4machineHeadcolor"><?php echo Machinemode; ?></h4>
                            <center class="machineModeCenterStyle">
                                <div id="DailyGraph3" class="DailyGraphs3 machineDailygStyle"></div>
                            </center>
                        </div>
                        <div id="DailyGraph3 " class="col-md-12 DailyGraphs3" style="height:273px;display:none;" >
                            <h4 class="h4machineHeadcolor"><?php echo Actualproductiontime; ?></h4>
                            <center>
                                <div class="laptopsizegraph ActualproductDivclass">
                                    <div class="laptopsizegraph2 ActualproductDivMainclass"></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 colomd5machineModeStyle">
            <div class="tab-content boxShadow machineModeStyle">
                <div class="tab-pane fade active show" >
                    <div class="row m-l-0 m-r-0">


                        <div class="col-md-12">
                            <h4 class="h4machineHeadcolor"><?php echo Productiontime; ?> <span  id="ActualProductionText" style="color: #002060 !important;float: right;font-size: 14px;"></span></h4>
                            <br>
                            

                            <center>
                                <div id="DailyGraph4" style="height:270px;" ></div>
                            </center>
                        </div>

                        <div id="blankDailyGraph4" class="col-md-12 graphmargin dailygraphDailyGraph4">
                            <center>
                                <div class="laptopsizegraph laptopsizeDivgraph">
                                    <div class="laptopsizegraph2 laptopsizeDivgraph2Style"></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-5 colomd5machineModeStyle">
            <div class="tab-content boxShadow">
                <div class="tab-pane fade active show" >
                    <h4 class="h4machineHeadcolor"><?php echo Noproductiontime; ?> <span id="NoProductionText" style="color: #002060 !important;float: right;font-size: 14px;"></span></h4>
                    
                </div>
            </div>

            <div class="tab-content boxShadow">
                <div class="tab-pane fade active show" >
                    <h4 class="h4machineHeadcolor"><?php echo Setuptime; ?> <span id="SetupText" style="color: #002060 !important;float: right;font-size: 14px;"></span></h4>
                </div>
            </div>
        </div>
        <div class="col-md-2">
            <div class="tab-content laptopsize boxShadow" style="height: 345px;">
                <div class="tab-pane fade active show">
                    
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #76BA1B;"> </div>
                        <div class="p-l-10"> <?php echo Running; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #FFCF00;"> </div>
                        <div class="p-l-10"> <?php echo Waiting; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #F60100;"> </div>
                        <div class="p-l-10"> <?php echo Stopped; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #CCD2DF;"> </div>
                        <div class="p-l-10"> <?php echo Off; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #000000;"> </div>
                        <div class="p-l-10"> <?php echo Nodet; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #002060;"> </div>
                        <div class="p-l-10"> <?php echo Productiontime; ?></div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #124D8D;"> </div>
                        <div class="p-l-10">  <?php echo Setuptime; ?> </div>
                    </div>
                
                    <div class="p-b-20">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #FF8000;"> </div>
                        <div class="p-l-10"> <?php echo Noproduction; ?> </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="graph-loader hide" ></div>
                
    <div class="row">
        <div class="col-md-12" >
            <div class="tab-content boxShadow" style="">
                <div class="tab-pane fade active show" style="overflow-x: scroll!important;">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-md-12">
                            <h3 id="headerChange" style="color: #002060;"><?php echo Hourlymachineanalysis; ?> </h3>
                            <form style="float: right;margin-top: -34px;" action="<?php echo base_url('Analytics/beta_GraphLog'); ?>" method="post"> <input type="hidden" name="machineId" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>"> <button class="btn btn-primary"><?php echo Log; ?></button> </form>
                            <div id="DailyGraph1" style="height:350px;width:100%;" >                        
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<div class="modal fade" id="add-view-modal" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Addnewshift; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                
                <form class="p-b-20 m-l-10 m-r-10" action="add_view" method="POST" id="add_view_form">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group p-5">
                                <input type="text" style="padding: 0px 12px 16px !important;" class="form-control border-left-right-top-hide h-29" id="viewName" name="viewName" placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-stop"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12">
                          <hr style="height: 2px;margin-top: 1rem;">
                        </div>
                    </div>

                    <div class="addBreakData" style="margin-bottom: 10px;display: none;"></div>

                    <div class="row addBreakButton">
                        <div class="col-md-12">
                          <span onclick="addBreak()" style="color: #FF8000;cursor: pointer;">
                            <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="display: none;">
                        <div class="col-md-12">
                          <span onclick="removeBreak()" style="color: red;cursor: pointer;">
                            <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?></span>
                        </div>
                    </div>

                    <div class="row removeBreak" style="margin-top: 25px;display: none;">
                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-break-start"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 style="color: #002060"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-break-stop"></div>
                        </div>
                    </div>
                    
                    <div class="row removeBreak m-l-0 m-r-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                            <center>
                                <a onclick="saveBreaks()" class="btn btn-primary m-r-5 savebreaksstyle" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                            </center>
                        </div>
                    </div>

                    <div class="row submitButton m-l-0 m-r-0">
                        <div class="col-md-12">
                            <br>
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5 AddbreakButtonstyle" id="add_view_submit" >
                                    <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Add; ?></button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
      </div>
    </div>
</div>

<?php foreach ($machineView as $key => $value) { ?>
<div class="modal fade" id="edit-view-modal-dynamic<?php echo $value['viewId']; ?>" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 style="color: #FF8000;" class="modal-title p-l-15"><?php echo Editview; ?></h4>
                <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
            </div>

            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="edit_view_form" method="POST" id="edit_view_form<?php echo $value['viewId']; ?>">
                    <input type="hidden" name="viewId" id="viewIdEdit-dynamic<?php echo $value['viewId']; ?>">
                    <div class="row">
                        <div class="col-md-12">
                            <div style="padding: 0 5px;" class="form-group">
                              <input type="text" class="form-control border-left-right-top-hide" id="viewNameEdit-dynamic<?php echo $value['viewId']; ?>" name="viewName" 
                              placeholder="<?php echo Enterview; ?>" required > 
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionstarts; ?></center></h5>
                            <div class="js-inline-picker-edit-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Productionends; ?></center></h5>
                            <div class="js-inline-picker-edit-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-md-12"><hr class="h-2 m-t-10"></div>
                    </div>

                    <div class="addEditBreakData-dynamic<?php echo $value['viewId']; ?> m-b-10" style="display: none;"></div>

                    <div class="row addEditBreakButton-dynamic<?php echo $value['viewId']; ?>">
                        <div class="col-md-12">
                            <span onclick="addEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksbuttonStyle">
                                <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addbreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?>" style="display: none;">
                        <div class="col-md-12">
                            <span onclick="removeEditBreak(<?php echo $value['viewId']; ?>)" class="addbreaksRenoStyle">
                                <i class="fa fa-times" style="font-size: 15px;"></i> <?php echo Removebreaks; ?>
                            </span>
                        </div>
                    </div>

                    <div style="display: none" class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-t-25">
                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakstartsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-start-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>

                        <div class="col-md-6">
                            <h5 class="h4machineHeadcolor"><center><?php echo Breakendsat; ?></center></h5>
                            <div class="js-inline-picker-edit-break-stop-dynamic<?php echo $value['viewId']; ?>"></div>
                        </div>
                    </div>

                    <div class="row removeEditBreak-dynamic<?php echo $value['viewId']; ?> m-r-0 m-l-0" style="display: none;">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <a onclick="saveEditBreaks(<?php echo $value['viewId']; ?>)" class="btn btn-primary m-r-5 updatesavebreakbutton" id="add_view_break_submit" ><?php echo Savebreak; ?></a>
                          </center>
                        </div>
                    </div>
                    
                    <div class="row submitEditButton-dynamic<?php echo $value['viewId']; ?> m-l-0 m-r-0">
                        <div class="col-md-12">
                          <br>
                          <center>
                            <button type="submit"class="btn btn-primary m-r-5 savebuttonstylee" id="edit_view_submit<?php echo $value['viewId']; ?>" ><?php echo save; ?></button>
                            <a class="btn btn-primary m-r-5 deleteviewbuttonstylee" id="deleteView" >
                                <img width="38px"height="34px" src="<?php echo base_url('assets/nav_bar/delete_white.svg'); ?>"></a>
                          </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<?php } ?>



<div class="modal fade" id="delete-view-modal" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content deleteModalContentStyle">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title h4machineHeadcolor p-5"><?php echo Delete; ?> </h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div> 
            <div class="modal-body">
                <form class="p-b-20" action="delete_view_form" method="POST" id="delete_view_form" >
                    <input type="hidden" name="deleteViewId" id="deleteViewId" /> 
                    <div class="modal-footer border-top-0 p-t-0">
                        <div class="row">
                            <div class="col-md-12 m-t-30">
                                <center>
                                    <img class="deleteGrayButtonStyle"src="<?php echo base_url().'assets/img/delete_gray.svg'?>">
                                </center>
                            </div>
                            <br>
                            <div id="" class="m-b-10 alert alert-success fade hide" ></div>
                            <div class="col-md-12" style="margin-top: -5px;">
                                <center><?php echo Areyousureyouwanttodeleteview; ?></center>
                            </div>
                            <div class="col-md-12">
                                <center>
                                    <br>
                                    <button type="submit" style="width:25%;" class="btn btn-danger" id="delete_view_submit"><?php echo Delete; ?></button>
                                    <br>
                                </center>
                            </div>
                        </div>          
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>