<style type="text/css">
  .table>tbody>tr>td, 
  .table>tbody>tr>th, 
  .table>tfoot>tr>td, 
  .table>tfoot>tr>th, 
  .table>thead>tr>td, 
  .table>thead>tr>th 
  {
     border: none;
  }

  .table>tbody>tr
  {
    color: #002060 !important;
    font-weight: 600 !important;
    cursor: pointer;
  } 

  .table thead th, .table>thead>tr>th 
  {
      color: #1f2225;
      font-weight: 400;
      border: none !important;
  }
  
  .last-column
  {
    
  }

  .paginate_button, .paginate_input
  {
    border: none !important;
    width: 7%;
  }

  #empTable_wrapper .col-sm-5 
  {
    flex: 0 0 27.666667% !important;
      max-width: 27.666667% !important;
  }

  #empTable_wrapper .col-sm-7 
  {
    flex: 0 0 28.333333% !important;
      max-width: 28.333333% !important;
      text-align: center !important;
      margin-top: 10px;
  }

  table.dataTable thead .sorting_asc:after 
  {
    display: none !important;
  }


.datepicker.dropdown-menu 
{
    min-width: 100px !important;
}

.dropdown-menu.show
{
    max-height: 250px;
    overflow: auto;
}
.datepicker.datepicker-dropdown 
{
    width: 207px !important;
}

.datepicker 
{
    min-width: 207px!important;
}

.datepicker,
.table-condensed {
  width: 200px !important;
  height:200px !important;
}

.datepicker table tr td, 
.datepicker table tr th 
{
  padding-left: 0px !important;
    padding-right: 0px !important;
}


.table-condensed>tbody>tr>td, .table-condensed>tbody>tr>th, .table-condensed>tfoot>tr>td, .table-condensed>tfoot>tr>th, .table-condensed>thead>tr>td, .table-condensed>thead>tr>th {
    padding: 0px 0px !important;
}

.datepicker table tr td, .datepicker table tr th 
{
  width: 0px !important;
    height: 19px !important;
    border-radius: 15px !important;
}


.datepicker table tr td span.active.active, .datepicker table tr td.active.active, .datepicker table tr td.active.disabled.active, .datepicker table tr td.active.disabled:active, .datepicker table tr td.active.disabled:focus, .datepicker table tr td.active.disabled:hover, .datepicker table tr td.active.disabled:hover.active, .datepicker table tr td.active.disabled:hover:active, .datepicker table tr td.active.disabled:hover:focus, .datepicker table tr td.active.disabled:hover:hover, .datepicker table tr td.active:active, .datepicker table tr td.active:focus, .datepicker table tr td.active:hover, .datepicker table tr td.active:hover.active, .datepicker table tr td.active:hover:active, .datepicker table tr td.active:hover:focus, .datepicker table tr td.active:hover:hover, .open .dropdown-toggle.datepicker table tr td.active, .open .dropdown-toggle.datepicker table tr td.active.disabled, .open .dropdown-toggle.datepicker table tr td.active.disabled:hover, .open .dropdown-toggle.datepicker table tr td.active:hover 
{
  background: #FF8000!important;
}

.datepicker .prev {
  box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .prev:before {
  color: #FF8000 !important;
}
.datepicker .next {
  box-shadow: grey 0px 0px 3px 1px !important;
}
.datepicker .next:before {
  color: #FF8000 !important;
}

#empTable_paginate
{
  border-radius: 15px !important;
  padding-left: 0px !important;
  box-shadow: grey 0px 0px 8px -2px !important;
}





input[type='radio'] {
  -webkit-appearance:none;
  width:13px;
  height:13px;
  border:1px solid black;
  border-radius:50%;
  outline:none;
}

input[type='radio']:hover {
  box-shadow:0 0 5px 0px #FF8000 inset;
}

input[type='radio']:before {
  content:'';
  display:block;
  width:60%;
  height:60%;
  margin: 20% auto;    
  border-radius:50%;    
}
input[type='radio']:checked:before {
    width: 15px !important;
    height: 15px !important;
    margin-top: -2px !important;
    margin-left: -2px !important;
    background-image: url(<?php echo base_url("assets/img/tick_orange.svg"); ?>);
}


input[type='checkbox'] {
  -webkit-appearance:none;
  width:13px;
  height:13px;
  border:1px solid #002060;
  border-radius:50%;
  outline:none;
}



input[type='checkbox']:before {
  content:'';
  display:block;
  width:60%;
  height:60%;
  margin: 20% auto;    
  border-radius:50%;    
}
input[type='checkbox']:checked:before {
    width: 15px !important;
    height: 15px !important;
    margin-top: -2px !important;
    margin-left: -2px !important;
    background-image: url(<?php echo base_url("assets/img/tick_orange.svg"); ?>);
}


#testetst
{
  color: #FF8000 !important;
}

.garyColor::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #b8b0b0;
  opacity: 1; /* Firefox */
}

.garyColor:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #b8b0b0;
}

.garyColor::-ms-input-placeholder { /* Microsoft Edge */
  color: #b8b0b0;
}

.orangColor::placeholder { /* Chrome, Firefox, Opera, Safari 10.1+ */
  color: #FF8000;
  opacity: 1; /* Firefox */
}

.orangColor:-ms-input-placeholder { /* Internet Explorer 10-11 */
  color: #FF8000;
}

.orangColor::-ms-input-placeholder { /* Microsoft Edge */
  color: #FF8000;
}

.form-check-inline ~ label {
  color: blue !important;
}


svg
  {
    position: absolute;
    width:150px;
    height:150px;
    z-index: 1000px;
  }
  svg circle
  {
    width:100%;
    height:100%;
    fill:none;
    stroke:#d01c1c;
    stroke-width:8;
    stroke-linecap:8;
    transform: translate(5px,5px);

    -webkit-transform: translate(5px,5px);
  }
  svg circle:nth-child(2)
  {
    stroke-dasharray: 440;
    stroke-dashoffset:440;
  }


  .border-left-right-top-hide
  {
    border-left: none;
    border-top: none;
    border-right: none;
    border-radius: 0;
  }

  input:focus,
  select:focus,
  textarea:focus,
  .form-control:focus,
  button:focus {
      outline: none;
  }

  .form-control.focus, .form-control.input-white.focus, .form-control.input-white:focus, .form-control:focus
  {
    box-shadow : none;
    border-color: #bec6ce;
  }
</style>

<style type="text/css">
  . .col-md-3{
  display: inline-block;
  margin-left:-4px;
}
/*.col-md-3 img{
  width:100%;
  height:auto;
}*/
body .carousel-indicators li{
  background-color:red;
}
body .carousel-indicators{
  bottom: 0;
}
body .carousel-control-prev-icon,
body .carousel-control-next-icon{
  background-color:red;
}
body .no-padding{
  padding-left: 15px;
  padding-right: 0;
   }


.carousel-item-next, .carousel-item-prev, .carousel-item.active {
    display: block !important;
}

.carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.users-carousel-control-prev {
    left: 18px !important;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}

.users-carousel-control-next {
  right: 18px;
    box-shadow: grey 0px 0px 3px 1px !important;
    border-radius: 50px;
    width: 43px;
    height: 43px;
    top: 71px;
}
.boxShadow
{
  box-shadow: grey 0px 0px 8px -2px !important;
}
@media (max-width:400px)
 {
  .deletemodaltask
  {
    width: 300px;
    height: 235px;
  }
 
 }

 .form-check-input:checked + .form-check-label 
 {
  color: #FF8000;
}
@media screen and (min-device-width:320px)and (max-width:375px) 
    {
      .col-md-3 
      {
        margin-top:-332px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
    @media screen and (min-device-width:376px)and (max-width:424px) 
    {
      .col-md-3 
      {
        margin-top:-332px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
    @media screen and (min-device-width:425px)and (max-width:575px) 
    {
      .col-md-3 
      {
        margin-top:-300px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }
     @media screen and (min-device-width:576px)and (max-width:767px) 
    {
      .col-md-3 
      {
        margin-top:-494px!important;
        width: 100%!important;
        padding-left: 20px!important;
        padding-right: 20px!important;
      }
    }

    @media screen and (min-device-width:768px)and (max-width:1023px) 
    {
      .col-md-3 
      {
        margin-top:-637px!important;
        width: 100%!important;
      }
    }
    .displayfilterrowstyle
    {
      display:contents!important; 
    }
</style>

<div id="content" class="content">
  
  <h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo LanguageListing; ?></h1>

  <div class="row">
    <div class="col-md-12 table_data" style="padding-left:14px;">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >
          <div class="row" style="float:left;margin: 10px;">
            <span class="displayfilterrowstyle" id="showAppVersionName"></span>
            <span class="displayfilterrowstyle" id="showAddeddDate"></span>
            <span class="displayfilterrowstyle" id="showType"></span>
          </div>
        <div class="panel-body">
          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>  
                
                <!-- <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">languageId<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>  -->


                  <th class="TableHeaderTHstyle" style="padding-bottom: 23px !important;">
                    <div id="filterStatusSelected" class="dropdown FilterStatusSelectedStyle">
                      <span class="dropdown-toggle" style="color: rgb(184, 176, 176);" data-toggle="dropdown"> <?php echo ucfirst(type); ?>&nbsp;</span>
                      <div class="dropdown-menu">
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" style="display: flex;align-items: left;justify-content: left;">
                            <input onclick="laguageType()" value="'opapp'" class="form-check-input type" type="checkbox" id="laguageTypeopapp">
                            <label class="form-check-label" for="laguageTypeCompleted">
                              <?php echo 'Opapp'; ?>
                            </label>
                          </div>
                        </div>
                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" style="display: flex;align-items: left;justify-content: left;">
                            <input onclick="laguageType()" value="'setappapi'" class="form-check-input type" type="checkbox" id="laguageTypesetappapi">
                            <label class="form-check-label" for="filterStatusUncompleted">
                              <?php echo 'Setappapi'; ?>
                            </label>
                          </div>
                        </div>

                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" style="display: flex;align-items: left;justify-content: left;">
                            <input onclick="laguageType()" value="'opappapi'" class="form-check-input type" type="checkbox" id="laguageTypeopappapi">
                            <label class="form-check-label" for="filterStatusUncompleted">
                              <?php echo 'Opappapi'; ?>
                            </label>
                          </div>
                        </div>

                        <div class="dropdown-item">
                          <div class="form-check formCheckWidth" style="display: flex;align-items: left;justify-content: left;">
                            <input onclick="laguageType()" value="'dashboard'" class="form-check-input type" type="checkbox" id="laguageTypedashboard">
                            <label class="form-check-label" for="filterStatusUncompleted">
                              <?php echo 'Dashboard'; ?>
                            </label>
                          </div>
                        </div>


                      </div>
                    </div>
                  </th>
  

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;"><?php echo Englishtext; ?><br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>  

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;"><?php echo Swedishtext; ?><br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th> 

               
                 <!-- <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">textIdentify<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th> -->

              
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div> 

   

     

    <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
</div>

<div class="modal fade" id="editLanguage" style="display: none;" aria-hidden="true">
      <div class="modal-dialog">
          <div class="modal-content">
              <div class="modal-header editmodalHeader">
                  <h4 class="modal-title editmodalTitle"><?php echo Updatelanguage; ?></h4>
                  <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
              </div>
              <div class="modal-body">
                  <!-- <div class="column col-md-8"> -->
                  <div class="col-md-12">
                      <div class="row">

                          <input type="hidden" name="languageId" id="languageId">

                          <div class="col-md-12">
                              <div style="padding: 15px 5px;" class="form-group">
                                  <label style="font-weight: 650;color:black;"><?php echo Englishtext; ?></label><br>
                                  <span id="englishText"></span>
                              </div>
                          </div>

                          <div class="col-md-12">
                              <div style="padding: 15px 5px;" class="form-group">
                                  <label style="font-weight: 650;color:black;"><?php echo Swedishtext; ?></label>
                                  <input type="text" style="padding: 0px;border-top: 0px;border-left: 0px;border-right: 0px;border-radius: 0px;padding-bottom: 12px;margin-top: 10%;" class="form-control col-md-12 border-left-right-top-hide" id="swedishText" name="swedishText" placeholder="" required>
                              </div>
                          </div>
                          
                          <br>
                          <br>

                          <div class="col-md-12">
                              <center><button type="button" onclick="updateSaveLanguage()" class="btn btn-primary"><?php echo Update; ?></button></center>
                          </div>
                      </div>
                  </div>
              </div>
          </div>
      </div>
  </div>