<link href="<?php echo base_url('assets/css/operatorDetailsList.css');?>" rel="stylesheet" />
<div id="content" class="content">
	
	<h1 style="font-size: 22px;color: #002060" class="page-header">Report</h1>
	<input type="hidden" name="problemId" value="0">
	<div class="row">
		<div class="col-md-12 table_data" style="padding-right: 0px;">
		  	<div class="panel panel-inverse panel-primary" >
				<div class="panel-body">
					<table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
						<thead>
							<tr>
								<th class="width_10" style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;">
									<div class="dropdown" id="filterUserIdSelected" style="text-align: center;border: 1px solid #F2F2F2;padding: 4px;border-radius: 5px;text-transform: capitalize;" >
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserIdSelectedValue" > UserID&nbsp;</span>
										<div class="dropdown-menu">

												<div class="dropdown-item">
													<div class="form-check" style="width: max-content;">
													  <input onclick="filterUserId('all')" class="form-check-input" type="radio" id="filterUserIdAll" 
													   name="filterUserId">
													  <label onclick="filterUserId('all')" class="form-check-label radioClick1" for="filterUserIdAll">
													    All
													  </label>
													</div>
												</div>
											<?php foreach ($users as $key => $value) { ?>
												<div class="dropdown-item">
													
													<div class="form-check" style="width: max-content;">
													  <input onclick="filterUserId(<?php echo $value['userId']; ?>)" class="form-check-input" type="radio" id="filterUserId<?php echo $key; ?>" 
													   name="filterUserId">
													  <label onclick="filterUserId(<?php echo $value['userId']; ?>)" class="form-check-label radioClick1" for="filterUserId<?php echo $key; ?>">
													    <?php echo $value['userId']; ?>
													  </label>
													</div>
												</div>
											<?php } ?>
											<input type="hidden" id="filterUserId">

										</div><br>
									</div>
								<small style="color: #FF8000;text-align : center;" >&nbsp;</small>
								</th>
								<th class="width_11" style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;">
									<div id="filterUserNameSelected" class="dropdown" style="text-align: center;border: 1px solid #F2F2F2;padding: 4px;border-radius: 5px;" >
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> Operator&nbsp;</span>
										<div class="dropdown-menu">
											<div class="dropdown-item">
												<div class="form-check" style="width: max-content;">
												  <input onclick="filterUserName('all','All')" class="form-check-input" type="radio" id="filterUserNameAll" 
												   name="filterUserName">
												  <label onclick="filterUserName('all','All')" class="form-check-label radioClick2" for="filterUserNameAll">
												    All
												  </label>
												</div>
											</div>

											<?php foreach ($users as $key => $value) { ?>
												<div class="dropdown-item">
													<div class="form-check" style="width: max-content;">
													  <input onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $value['userName']; ?>')" class="form-check-input" type="radio" id="filterUserName<?php echo $key; ?>" 
													   name="filterUserName">
													  <label onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $value['userName']; ?>')" class="form-check-label radioClick2" for="filterUserName<?php echo $key; ?>">
													    <?php echo $value['userName']; ?>
													  </label>
													</div>
												</div>
											<?php } ?>
											<input type="hidden" id="filterUserName">
										</div>
									</div>
								<small style="color: #FF8000;text-align : center;" >&nbsp;</small>
								</th>
								<th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; width: 100px !important;" >
									<input id="filterDateSelected" type="text" class="datepicker56 garyColor" name="" placeholder="Checked in" onchange="filterDate(this.value)" style="height: 26px !important;border: 1px solid #F2F2F2;font-weight: 400;color: #b8b0b0;width: 85px;padding-left: 10px;border-radius: 5px;" >

									<span id="dateArrow"><i style="margin-left: -17px;" class="fa fa-caret-down" aria-hidden="true"></i></span>

									<input type="hidden" id="filterDate"><br>
								<small  style="color: #FF8000;text-align : center;" >
									&nbsp;
								</small>
								</th>
								
								<th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; width: 100px !important;" >
									<input id="filterDateSelected" type="text" class="datepicker56 garyColor" name="" placeholder="Checked out" onchange="filterDate(this.value)" style="height: 26px !important;border: 1px solid #F2F2F2;font-weight: 400;color: #b8b0b0;width: 85px;padding-left: 10px;border-radius: 5px;" >

									<span id="dateArrow"><i style="margin-left: -17px;" class="fa fa-caret-down" aria-hidden="true"></i></span>

									<input type="hidden" id="filterDate"><br>
								<small  style="color: #FF8000;text-align : center;" >
									&nbsp;
								</small>
								</th>

								<th class="width_11" style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
									<div id="filterUserNameSelected" class="dropdown" style="text-align: center;border: 1px solid #F2F2F2;padding: 4px;border-radius: 5px;" >
										<span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> Reports&nbsp;</span>
										<div class="dropdown-menu">
											<div class="dropdown-item">
												<div class="form-check" style="width: max-content;">
												  <input onclick="filterUserName('all','All')" class="form-check-input" type="radio" id="filterUserNameAll" 
												   name="filterUserName">
												  <label onclick="filterUserName('all','All')" class="form-check-label radioClick2" for="filterUserNameAll">
												    All
												  </label>
												</div>
											</div>

											<?php foreach ($users as $key => $value) { ?>
												<div class="dropdown-item">
													<div class="form-check" style="width: max-content;">
													  <input onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $value['userName']; ?>')" class="form-check-input" type="radio" id="filterUserName<?php echo $key; ?>" 
													   name="filterUserName">
													  <label onclick="filterUserName(<?php echo $value['userId']; ?>,'<?php echo $value['userName']; ?>')" class="form-check-label radioClick2" for="filterUserName<?php echo $key; ?>">
													    <?php echo $value['userName']; ?>
													  </label>
													</div>
												</div>
											<?php } ?>
											<input type="hidden" id="filterUserName">
										</div>
									</div>
								<small style="color: #FF8000;text-align : center;" >&nbsp;</small>
								</th>
							</tr>
						</thead>
					</table>
				</div>
			</div>
		</div> 
		<div class="col-md-3 detail" style="margin-top: -75px;display: none;margin-bottom: -63px;background: #FFFFFF !important;border-bottom-left-radius: 5px;    border-bottom-right-radius: 5px;box-shadow: gray -10px 10px 10px -10px;">
			 <div class="panel panel-inverse panel-primary" >
				<div class="panel-body" style="padding: 0;">
						
						<div class="row" style="padding: 8px;padding-top: 0 !important;background: #002060;height: 150px; border-top-left-radius: 5px;    border-top-right-radius: 5px;">
							<div class="col-md-12">
								<div style="padding: 14px 0 0 0;">
									<a href="javascript:void(0);" onclick="closeDetails();" style="opacity: 1.0!important;"><img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
								</div>
							</div>
							<div class="col-md-4" style="float: left">
								<img id="userImage" style="border-radius: 50%;border: 3px solid #FFFFFF" height="85" width="80" src="">  
							</div>
							<div class="col-md-8" style="float: left;margin-top: 27px;">
								<span id="userName" style="font-size: 17px;color: #FF8000;font-weight: 700;"></span> <br>
								<small style="color: #FFFFFF;font-size: 80%;" id="userId"></small>
							</div>
						</div>

						<div class="row" style="">

								<div class="col-md-6" style="padding-top: 32px;padding-bottom: 33px;">
									<h3 style="padding-left: 15px;color: #002060" id="type"></h3>
								</div>
								<div class="col-md-6" style="padding-top: 32px;padding-bottom: 33px;">
									<small style="float: right;margin-top: 10px;padding-right: 10px;" id="date"></small>
								</div>
						</div>
						
						<form class="p-b-20" action="update_reportProblem_form" method="POST" id="update_reportProblem_form" >

							<input type="hidden" id="problemId" name="problemId">
							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<small><b>Description</b></small><br>
									<textarea style="border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="description" id="description"></textarea>
									<hr style="height: 2px;margin-top: 0rem;">
								</div>
							</div>

							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<small><b>Cause</b></small><br>
									<textarea style="border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="cause" id="cause"></textarea>
									<hr style="height: 2px;margin-top: 0rem;">
								</div>
							</div>

							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<small><b>Improvement</b></small><br>
									<textarea style="border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="improvement" id="improvement"></textarea>
									<hr style="height: 2px;margin-top: 0rem;">
								</div>
							</div>

							<div class="row">
								<div class="col-md-12" style="padding-left: 26px;">
									<div id="not_confidentials" style="display: none;">
										<img  width="16" style="" src="http://nyttdev.com/testing_env/version2/assets/img/cross.svg"> 
										<span style="color: red"> &nbsp;&nbsp;<b>Not confidential</b></span>
									</div>
									<div id="confidentials" style="display: none;">
										<img width="16" style="" src="http://nyttdev.com/testing_env/version2/assets/img/tick.svg"> 
										<span style="color: green"> &nbsp;&nbsp;<b>Confidential</b></span>
									</div>

									<center>
										<button id="update_reportProblem_submit" style="margin-top: 40px;margin-bottom: 40px;padding-left: 35px;padding-right: 35px;padding-top: 7px;padding-bottom: 7px;border-radius: 7px;" type="submit" class="btn btn-sm btn-primary">Save</button>
									</center>
								</div>
							</div>
						</form>
					</div>
				</div>
			</div>
		</div>
		<hr style="background: gray;">
    <p>&copy; &nbsp; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
	</div>
	<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	

