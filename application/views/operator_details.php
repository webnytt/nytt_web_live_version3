<link href="<?php echo base_url('assets/css/operator_details.css');?>" rel="stylesheet" />	
	<div id="content" class="content">
		
		
		
		<h1 class="page-header" style="color: #002060;">Operators</h1>
		<a class="btn btn-primary" href="<?php echo base_url('Operator/operator_checkin_checkout');?>"><img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/report.svg'); ?>"> Operator reports</a>
		<br><br>
		<div class="row">
			<?php if($this->session->flashdata('error_message') != '' ) { ?>
			<div class="col-md-12">
				<div class="alert alert-danger fade show m-b-10">
					<span class="close" data-dismiss="alert">×</span>
					<?php echo $this->session->flashdata('error_message'); ?>
				</div>
			</div>
			<?php } ?> 
			
			<?php if($this->session->flashdata('success_message') != '' ) { ?>
			<div class="col-md-12">
				<div class="alert alert-success fade show m-b-10">
					<span class="close" data-dismiss="alert">×</span>
					<?php echo $this->session->flashdata('success_message'); ?>
				</div>
			</div>
			<?php } ?>
		</div>
		
		<?php  ?>
		
		<div class=" row ">
			<?php foreach($listOperators->result() as $user) { ?>
				<?php if($user->userImage == '') {
					$imgUrl = base_url().'assets/img/user/default.jpg';  
				} else {
					$imgUrl = base_url().'assets/img/user/'.$user->userImage;   
				}?>
				<div class="col-lg-3 col-md-6">
				<div class="card" >
					<div class="card-img-overlay" >
						<h4 class="card-title"><a class="text-primary" href="#modal-edit-user<?php echo $user->userId; ?>" data-toggle="modal" ><img class="widget-img widget-img-sm rounded-corner" src="<?php echo $imgUrl; ?>" alt="..." />
								<?php echo $user->userName; ?></a></h4>
						<div class="card-content">
							<div class="clearfix"> 
								<div class="col-md-12" style="max-width:100%;padding: 0;">
									<form class="" id="assign_form<?php echo $user->userId; ?>" >
									   <?php $machines = explode(",", $user->machines); ?>
											<input type="hidden" name="userId" value="<?php echo $user->userId; ?>" />
											<label for="userMachine[]" >Machines assigned</label>
											<select class="userMachineSel form-control col-md-12" multiple="multiple" id="userMachine<?php echo $user->userId; ?>" name="userMachine[]" >
												<?php foreach($listMachines->result() as $machine) { ?>
													<option <?php if( is_array($machines) && in_array($machine->machineId, $machines) ) { echo "selected"; } ?> value="<?php echo $machine->machineId; ?>" ><?php echo $machine->machineName; ?></option>
												<?php } ?>
											</select>
											<button type="submit" class="btn btn-sm btn-primary offset-md-4 col-md-4 m-t-20" id="assign_form_submit<?php echo $user->userId; ?>" >Update</button>
									</form> 
								</div> 
								<div class="card-action" style="bottom:15px;" >
									<a class="text-primary" href="#modal-edit-user<?php echo $user->userId; ?>" data-toggle="modal" ><i class="btn btn-primary btn-sm far fa-edit fa-2x"></i></a>  
									<a class="text-primary" href="#modal-delete-user<?php echo $user->userId; ?>" data-toggle="modal" ><i class="btn btn-primary btn-sm far fa-trash-alt fa-2x"></i></a>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
					
			<?php } ?> 
			
			<?php foreach($listOperators->result() as $user) { ?>
				<div class="modal fade" id="modal-edit-user<?php echo $user->userId; ?>" style="display: none;" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-body">
								<h4 class="modal-title m-b-10" >Edit (<?php echo $user->userName; ?>)</h4> 
								<div id="edit_user_error<?php echo $user->userId; ?>" class="m-b-10 alert alert-danger fade hide"></div>
								<div id="edit_user_success<?php echo $user->userId; ?>" class="m-b-10 alert alert-success fade hide" ></div>
								<form class="p-b-20" action="edit_user" method="POST" id="edit_user_form<?php echo $user->userId; ?>" class="edit_user_form" enctype="multipart/form-data" >
									<input type="hidden" name="userId" id="userId<?php echo $user->userId; ?>" value="<?php echo $user->userId; ?>" /> 
									<div class="form-group" data-toggle="tooltip" data-title="Username" >
										<input type="text" class="form-control col-md-12" id="userName<?php echo $user->userId; ?>" name="userName" value="<?php echo set_value('userName', $user->userName);?>" placeholder="Enter user name" required > 
									</div>
									<div class="form-group" data-toggle="tooltip" data-title="Password" >
										<input type="text" class="form-control col-md-12" id="userPassword<?php echo $user->userId; ?>" name="userPassword" value="<?php echo set_value('userPassword', $user->userPassword);?>" placeholder="Enter password" required > 
									</div>
									<div class="form-group" data-toggle="tooltip" data-title="Age" >
										<input type="number" min="0" max="100" class="form-control col-md-12" id="userAge<?php echo $user->userId; ?>" name="userAge" value="<?php echo set_value('userAge', $user->userAge);?>" placeholder="Enter operator age" > 
									</div>
									<div class="form-group" data-toggle="tooltip" data-title="Experience" >
										<input type="number" step="0.01" min="0" max="99" class="form-control col-md-12" id="userExperience<?php echo $user->userId; ?>" name="userExperience" value="<?php echo set_value('userExperience', $user->userExperience);?>" placeholder="Enter operator experience(in year)" > 
									</div>
									<div class="" data-toggle="tooltip" data-title="Gender" >
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="userGender" id="1userGender<?php echo $user->userId; ?>" value="0" <?php if($user->userGender == '0') { ?>checked="" <?php } ?> >
											<label class="form-check-label" for="1userGender<?php echo $user->userId; ?>">Male</label>
										</div>
										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="userGender" id="2userGender<?php echo $user->userId; ?>" value="1" <?php if($user->userGender == '1') { ?>checked="" <?php } ?> >
											<label class="form-check-label" for="2userGender<?php echo $user->userId; ?>">Female</label>
										</div>

										<div class="form-check form-check-inline">
											<input class="form-check-input" type="radio" name="userGender" id="3userGender<?php echo $user->userId; ?>" value="2" <?php if($user->userGender == '2') { ?>checked="" <?php } ?> >
											<label class="form-check-label" for="3userGender<?php echo $user->userId; ?>">Unspecified</label>
										</div>
									</div>
									<div class="form-group" data-toggle="tooltip" data-title="Picture" >
										<?php if($user->userImage == '') {
											$imgUrl = base_url().'assets/img/user/default.jpg';  
										} else {
											$imgUrl = base_url().'assets/img/user/'.$user->userImage;   
										}?>
										<img class="widget-img widget-img-sm rounded-corner " src="<?php echo $imgUrl; ?>" alt="<?php echo $user->userName; ?>"><input type="file" class="" id="userImage<?php echo $user->userId; ?>" name="userImage"  >
									</div>
									<button type="submit" class="btn btn-sm btn-primary m-r-5" id="edit_user_submit<?php echo $user->userId; ?>">Save</button>
								</form>
							</div>
							
						</div>
					</div>
				</div>
				
				<div class="modal fade" id="modal-delete-user<?php echo $user->userId; ?>" style="display: none;" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content" >
							<div class="modal-body">
								<div id="delete_user_success<?php echo $user->userId; ?>" class="m-b-10 alert alert-success fade hide" ></div>
								<p></p>
								<p class="modal-title">
									Are you sure to delete user "<?php echo $user->userName; ?>"?
								</p>
							</div>
							<form class="p-b-20" action="delete_user" method="POST" id="delete_user_form<?php echo $user->userId; ?>" class="edit_user_form" >
								<input type="hidden" name="deleteuserId" id="deleteuserId<?php echo $user->userId; ?>" value="<?php echo $user->userId; ?>" /> 
								<div class="modal-footer" style="border-top: none;padding-top: 0;" >
									<a href="javascript:;" class="btn btn-white" data-dismiss="modal">Cancel</a>
									<button type="submit" class="btn btn-danger" id="delete_user_submit<?php echo $user->userId; ?>">Delete</a>
								</div>
							</form>
						</div>
					</div>
				</div>
			<?php } ?> 
			<div class="col-lg-3">
				<div class="card text-center">
					<i class="fa fa-plus add-new-icon" ></i>
					<a class="text-primary add-new-text" id="add-new-text" href="#modal-add-user" data-toggle="modal" >Add operator</a>
				</div>
			</div>
			
			<div class="modal fade" id="modal-add-user" style="display: none;" aria-hidden="true">
				<div class="modal-dialog">
					<div class="modal-content">
						
						<div class="modal-body">
							<h4 class="modal-title m-b-10" >Add operator</h4>
							<div id="add_user_error" class="m-b-10 alert alert-danger fade hide"></div>
							<div id="add_user_success" class="m-b-10 alert alert-success fade hide" ></div>
							
							<form class="p-b-20" action="" method="POST" id="add_user_form" enctype="multipart/form-data" > 
								<div class="form-group m-r-10" data-toggle="tooltip" data-title="Username" >
									<input type="text" class="form-control col-md-12" id="userName" name="userName" placeholder="Enter operator name" required > 
								</div>
								<div class="form-group m-r-10" data-toggle="tooltip" data-title="Password" >
									<input type="text" class="form-control col-md-12" id="userPassword" name="userPassword" placeholder="Enter password" required >
								</div>
								<div class="form-group m-r-10" data-toggle="tooltip" data-title="Age" >
									<input type="number" min="0" max="100" class="form-control col-md-12" id="userAge" name="userAge" placeholder="Enter operator age" > 
								</div>
								<div class="form-group m-r-10" data-toggle="tooltip" data-title="Experience" >
									<input type="number" step="0.01" min="0" max="99" class="form-control col-md-12" id="userExperience" name="userExperience" placeholder="Enter operator experience(in year)" > 
								</div>
								<div class=" m-b-10" data-toggle="tooltip" data-title="Gender" >
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="userGender" id="1userGender" value="0" checked="">
										<label class="form-check-label" for="1userGender">Male</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="userGender" id="2userGender" value="1">
										<label class="form-check-label" for="2userGender">Female</label>
									</div>
									<div class="form-check form-check-inline">
										<input class="form-check-input" type="radio" name="userGender" id="3userGender" value="2">
										<label class="form-check-label" for="3userGender">Unspecified</label>
									</div>
								</div>
								<div class="form-group">
									<input type="file" class="" id="userImage" name="userImage" >
								</div>
								<button type="submit" class="btn btn-sm btn-primary m-r-5" id="add_user_submit">Save</button>
									
									<a href="javascript:;" class="btn btn-sm btn-primary m-b-20 m-r-10 pull-right " onclick="generateUser(<?php echo $newUserId; ?>)" >Generate</a>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>
	