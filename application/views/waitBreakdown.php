<style type="text/css">
    .card2:nth-child(1) svg circle:nth-child(2)
    {
        stroke-dashoffset:<?php echo 440 - (440 * 100) / 100; ?>;
        stroke-linecap: round;
        stroke: #002060;
    }

        .select2-container
{
    /*display: block!important;*/
    height: 34px;
}
.select2-container--default .select2-selection--single
{
    border: 0px;
    border: none;
    box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;
    border-radius: 5px;
}
@media screen and (min-device-width: 320px) and (max-width: 374px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
} 
@media screen and (min-device-width: 375px) and (max-width: 424px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    }   
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 270px;
    }
}
@media screen and (min-device-width: 425px) and (max-width: 767px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .select2-container--default .select2-selection--single .select2-selection__rendered
    {
        width: 100%;
    }  
}
@media screen and (min-device-width: 768px) and (max-width: 1023px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
        margin-top: 10px!important;
    } 
    .headingblockstyle
    {
        width: 100%!Important;
    }
    .select2-container
    {
        min-width: 175px!important;
    }
}
@media screen and (min-device-width: 1320px)
{
    .yearwidthstyle
    {
        min-width:270px!important;
    }  
    .select2-container
    {
        min-width: 175px!important;
    }
}
</style>

    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
        <script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script> 
        <link href="<?php echo base_url('assets/css/breakdown3.css');?>" rel="stylesheet" /> 
        <div id="content" class="content">
            <?php $listMachinesStop =  $listMachines->result(); ?>
            <h1 class="page-header H1pageTitle"><?php echo Analytics; ?></h1>

            
           
         <div class="row">
                <input type="hidden" id="analytics" value="waitAnalytics">
                <div class="col-md-4" style="padding-left: 0px;">

                   <form action="<?php echo base_url('Analytics/dashboard'); ?> "method="post" style="float: left;">  

                        <input type="hidden" name="mainChoose1" id="mainChoose1"> 
                        <input type="hidden" name="mainChoose2" id="mainChoose2"> 
                        <input type="hidden" name="machineId" id="mainMachineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo $listMachinesStop[0]->machineId; } ?>"> 
                        <button style="border: none;background: none;padding-right: 3px;margin-top: -1px;cursor: pointer;" type="submit"><small class="SmallStyle"><?php echo Analytics ;?> </small></button>
                    </form>
                   <small class="SmallStyle">
                      &gt; <?php echo Waitanalytics; ?>
                    </small>
                </div>
                <div class="col-md-2 show_by Colmd1ShowBystyle">
                    <label><?php echo Show; ?> </label>
                </div>
                <div class="col-md-3">
                    <select name="showcalender" class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                        <option class="new_test" <?php echo ($_POST['partsChoose1'] == "day") ? "selected" : ""; ?> value="day"><?php echo Day; ?></option>
                        <option class="new_test" <?php echo ($_POST['partsChoose1'] == "weekly") ? "selected" : ""; ?> value="weekly"><?php echo week; ?></option>
                        <option class="new_test" <?php echo ($_POST['partsChoose1'] == "monthly") ? "selected" : ""; ?> value="monthly"><?php echo MONTH; ?></option>
                        <option class="new_test" <?php echo ($_POST['partsChoose1'] == "yearly") ? "selected" : ""; ?> value="yearly"><?php echo year; ?></option>
                    </select>
                </div>
        
                <div class="col-md-2 day colmd2day yearwidthstyle">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="height: 34px!important;margin-right: 0px!important;width: 100%!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" value="<?php if($_POST['partsChoose1'] == "day") { echo $_POST['partsChoose2']; }else{  echo date('m/d/Y'); } ?>">
                </div>

                <?php 
                    if($_POST['partsChoose1'] == "weekly") {
                    $partsChoose2  = explode("/", $_POST['partsChoose2']);
                      $weekPost = $partsChoose2[0];
                    }else
                    {
                        $weekPost = "";
                    }
                 ?>
                <div class="col-md-2 weekly yearwidthstyle" style="display: none;">
                    <select name="Weekly" class="form-control" id="choose3" onchange="changeFilterValue()">
                        <?php for ($iS=1; $iS < 53; $iS++) {  ?>
                            <option  <?php if($weekPost == $iS){ echo "selected"; }else if(date('W') == $iS && empty($weekPost)) { echo "selected"; } ?> value="<?php echo $iS; ?>"><?php echo week.' '.$iS; ?></option>
                        <?php } ?>
                    </select>
                </div>
                
                <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
                
                <div class="col-md-2 monthly " style="display: none;">
                    <input onchange="changeFilterValue()" type="text" class="form-control datepicker1 yearwidthstyle" id="choose4" style="height: 34px!important;margin-right: 0px!important;width: 100%!important;border: none;box-shadow: 2px 2px 6px rgb(133 159 172 / 41%) !important;" value="<?php if($_POST['partsChoose1'] == "monthly") { echo $_POST['partsChoose2']; }else{ echo date('M Y'); } ?>">
                </div>

                <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
                <div class="col-md-2 yearly yearwidthstyle" style="display: none;">
                    <select name="showcalenderyear" class="form-control"  id="choose5" onchange="changeFilterValue()">
                        <?php for($i=0 ; $i < $years; $i++){ 
                            $yearValue = 2018 + $i;
                            ?>
                            <option <?php if($_POST['partsChoose2'] == $yearValue && $_POST['partsChoose1'] == "yearly") { echo "selected"; } else if ($yearValue == date('Y') && empty($_POST['partsChoose1'])) {
                                echo "selected";
                            } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                        <?php } ?>
                    </select>
                </div>

                <div class="col-md-1 yearly" style="display: none;"></div>
            </div>

            <div class="row" style="margin-top: 13px;">
                
                <input type="hidden" name="" id="machineIdStop" value="<?php echo (!empty($_POST['machineId'])) ? $_POST['machineId'] : $listMachinesStop[0]->machineId; ?>">
                    <div id="demo" class="carousel slide" data-ride="carousel" data-interval="false" style="width: 100%;">
                    <div class="carousel-inner no-padding">
                        <div class="carousel-item active">
                        <?php 
                            $countQuery = 1;
                            $i = 1;
                                foreach($listMachinesStop as $machine) 
                                { 
                                    if($machine->machineName != "Parts_1035" && $machine->machineName != "Parts_1832" && $machine->machineName != "Parts_1821"){
                                        if ($_POST['machineId'] == $machine->machineId) 
                                        {

                                            
                                            if($countQuery % 4 == 0){ ?>
                                                    </div>
                                                    <div class="carousel-item">
                                                  <?php   } ?>
                                                <div style="margin-left: -7px;margin-right: 3px;" class="col-xs-3 col-sm-3 col-md-3 <?php if($i == 1){ echo "active"; } ?>">
                                                    <button type="button" class="btn btn-default btn-lg filter1 <?php if($i == 1){ echo "active"; } ?> machineNameText" id="machineStop<?php echo $machine->machineId; ?>">
                                                        <?php 
                                                            if($machine->machineName == "Status_1035"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1035";
                                                            }else if ($machine->machineName == "Status_1832"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1832";
                                                            }else if ($machine->machineName == "Status_1821"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1821";
                                                            }else {
                                                                echo $machine->machineName;
                                                        } ?> 
                                                    </button>
                                                </div>
                                            <?php $i++; if ($i != 4) { $countQuery++; }  
                                        } 
                                    }
                                } 

                                foreach($listMachinesStop as $machine) 
                                { 
                                    if($machine->machineName != "Parts_1035" && $machine->machineName != "Parts_1832" && $machine->machineName != "Parts_1821"){
                                        if ($_POST['machineId'] != $machine->machineId) 
                                        {
                                            if($machine->machineName == "Status_1035"  && $this->factoryId == 2) {
                                                $ioMachineName = "IO- Robot 1035";
                                            }else if ($machine->machineName == "Status_1832"  && $this->factoryId == 2) {
                                                $ioMachineName = "IO- Robot 1832";
                                            }else if ($machine->machineName == "Status_1821"  && $this->factoryId == 2) {
                                                $ioMachineName = "IO- Robot 1821";
                                            }else {
                                                $ioMachineName = $machine->machineName;
                                            }
                                            if($countQuery % 4 == 0){ ?>
                                                    </div>
                                                    <div class="carousel-item">
                                                  <?php   } ?>
                                                <div style="margin-left: -7px;margin-right: 3px;" class="col-xs-3 col-sm-3 col-md-3 <?php if($i == 1){ echo "active"; } ?>">
                                                    <button type="button" class="btn btn-default btn-lg filter1 <?php if($i == 1){ echo "active"; } ?> machineNameText" id="machineStop<?php echo $machine->machineId; ?>"> 
                                                        <?php 
                                                            if($machine->machineName == "Status_1035"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1035";
                                                            }else if ($machine->machineName == "Status_1832"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1832";
                                                            }else if ($machine->machineName == "Status_1821"  && $this->factoryId == 2) {
                                                                echo "IO- Robot 1821";
                                                            }else {
                                                                echo $machine->machineName;
                                                        } ?> 
                                                    </button>
                                                </div>
                                            <?php $i++; if ($i != 4) { $countQuery++; }  
                                        } 
                                    }
                                } 
                             ?>
                        </div>
                    </div>

                    <?php if ($i > 4) { ?>
                    <a class="carousel-control-prev" href="#demo" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon carouselcontrollerstyle">
                            <i class="fa fa-angle-left carouselIconStyle" aria-hidden="true"></i>
                        </span>
                    </a>
                    <a class="carousel-control-next" href="#demo" role="button" data-slide="next">
                        <span class="carousel-control-next-icon carouselcontrollerstyle">
                            <i class="fa fa-angle-right carouselIconStyle" aria-hidden="true"></i>
                        </span>
                    </a>
                <?php } ?>
            </div>
        </div>
        <div class="graph-loader hide" ></div>
        <div class="row graphloaderrowstyle" id="stopBlock">
        <input type="hidden" min="0" class="inputText" id="duration1" name="" value="0">
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <i id="upperLimitFirst" class="fa fa-bell" aria-hidden="true" style="float: right;display: none;font-size: 14px;padding-top: 5px;padding-right: 5px;"></i>
              <div class="card-body">
                <h4 class="cardcard2h4style">0 <?php echo mins; ?> - <input type="text" min="0" class="inputText" id="duration2" name="" value="5">  <?php echo mins; ?></h4>
                <div class="widget widget-stats widget1" id="filter1widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70"></circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter1count">0 <?php echo waits; ?></h3>
                                <small class="secondText" id="filter1">0 <?php echo mins; ?></small><br>
                                <small class="thirdText"><?php echo Totaltime; ?></small>
                            </div>
                        </svg>
                    </center>
                </div>
              </div>
            </div>
        </div>
        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <i id="upperLimitSecond" class="fa fa-bell" aria-hidden="true" style="float: right;display: none;font-size: 14px;padding-top: 5px;padding-right: 5px;"></i>
                <div class="card-body">
                    <h4 class="cardcard2h4style">
                        <span class="duration2Text">5 <?php echo mins; ?></span> - <input type="text" min="0" class="inputText" id="duration3" name="" value="10">  <?php echo mins; ?></h4>
                    <div class="widget widget-stats widget1" id="filter2widget">
                    <center>
                        <svg>
                            <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                            <circle cx="70" cy="70" r="70">
                            </circle>
                            <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                            <div class="roundText">
                                <h3 class="firstText" id="filter2count">0 <?php echo waits; ?></h3>
                                <small class="secondText" id="filter2">0 <?php echo mins; ?></small><br>
                                <small class="thirdText"><?php echo Totaltime; ?></small>
                            </div>
                        </svg>
                    </center>
                  </div>
                </div>
            </div>
        </div>

        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
              <i id="upperLimitThird" class="fa fa-bell" aria-hidden="true" style="float: right;display: none;font-size: 14px;padding-top: 5px;padding-right: 5px;"></i>
              <div class="card-body">
                    <h4 class="cardcard2h4style"><span class=
                        "duration3Text">10 <?php echo mins; ?></span> - <input type="text" min="0" class="inputText" id="duration4" name="" value="15">  <?php echo mins; ?></h4>
                    <div class="widget widget-stats widget1" id="filter3widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter3count">0 <?php echo waits; ?></h3>
                                    <small class="secondText" id="filter3">0 <?php echo mins; ?></small><br>
                                    <small class="thirdText"><?php echo Totaltime; ?></small>
                                </div>
                            </svg>
                        </center>
                    </div>
                </div>
            </div>
        </div>

        <div class="col colNew">
            <div class="card card2 boxShadow cardcard2style">
                <i id="upperLimitFour" class="fa fa-bell" aria-hidden="true" style="float: right;display: none;font-size: 14px;padding-top: 5px;padding-right: 5px;"></i>
                <div class="card-body">
                   <h4 class="cardcard2h4style"><span class=
                        "duration4Text">15 <?php echo mins; ?></span> - <img style="margin-top: -5px;" src="<?php echo base_url('assets/img/infiniti.png'); ?>"></h4>
                    <div class="widget widget-stats widget1" id="filter4widget">
                        <center>
                            <svg>
                                <circle style="stroke:#FFFFFF"cx="70" cy="70" r="70"></circle>
                                <circle cx="70" cy="70" r="70">
                                </circle>
                                <circle  style="stroke:#CCD2DF;" cx="70" cy="70" r="63"></circle>
                                <div class="roundText">
                                    <h3 class="firstText" id="filter4count">0 <?php echo waits; ?></h3>
                                    <small class="secondText" id="filter4">0 <?php echo mins; ?></small><br>
                                    <small class="thirdText"><?php echo Totaltime; ?></small>
                                </div>
                            </svg>
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="tab-content boxShadow row tabcontentstyle">
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle"><?php echo receive_waiting_notification; ?>: <span id="errorNotification"></span></h4>
            </div>
            <div class="col-md-6 col-sm-12" style="float: left;">
                <h4 class="h4ErrorNotificationStyle"><?php echo Reasonsforwaits; ?> : <span id="errorReasonNotification"></span></h4>
            </div>
        </div>

        <style type="text/css">
            .bagerClass {
                width: 20px;
                height: 20px;
                padding: 2px;
                float: left;
                margin-right: 10px;
            }
        </style>

        <div class="tab-content boxShadow row" style="margin-left: -4px!important;margin-right: 18px!important;">
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #FFCF00;"> </span><?php echo Machine; ?></div>
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #Ff8000;"> </span> <?php echo Tool; ?></div>
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #124D8D;"> </span> <?php echo Operator; ?></div>
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #002060;"> </span> <?php echo Material; ?></div>
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #CCD2DF;"> </span> <?php echo Other; ?></div>
            <div class="col-md-2 m-t-10"> <span class="bagerClass laptopsize" style="display:flex;background: #d32f2f;"> </span> <?php echo Unanswered; ?> </div>
        </div>
        <div class="row m-r-10">
            <div class="col-md-6">
                <div class="tab-content boxShadow">
                <div class="tabcontentMaindivStyle">
                    <h4 style="color: #002060 !important;"><?php echo Distributionofwaits; ?></h4>
                    <button id="filter1widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">0 <?php echo mins; ?> - 
                        <span class="duration2Text">5 <?php echo mins; ?></span>
                    

                    <button id="filter2widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration2Text">5 <?php echo mins; ?></span> - <span class="duration3Text">10 <?php echo mins; ?></span>
                    

                    <button id="filter3widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration3Text">10 <?php echo mins; ?></span> - <span class="duration4Text">15 <?php echo mins; ?></span>
                    

                    <button id="filter4widgetbutton" class="btn btn-primary filter1widgetbuttonBoxStyle">
                        <span class="duration4Text">15 <?php echo mins; ?></span> - <img src="<?php echo base_url('assets/img/white_infinity.svg'); ?>">
                    

                </div>

                <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime1" style="height:350px;" ></div> 
                                <div class="TimeStampGraphStyle"><?php echo TIMESTAMPHHMMSS; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="tab-content boxShadow" >
                    <div class="tabcontentMaindivStyle">
                        <h4 style="color: #002060 !important;"><?php echo Reasonsforwaits; ?></h4>
                    </div>
                    <div class="tab-pane fade active show tabpaneGraphpanelStyle">
                        <div class="row m-l-0 m-r-0">
                            <div class="col-md-12" id="graphdynamicsize">
                                <div id="DailyDowntime2" style="height:368px;" ></div> 
                                <div class="ReasonDurationGraphStyle"><?php echo REASONDURATIONMIN; ?></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div> 
            <hr style="background: gray;">
    <p>&copy;<?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
        </div>
        <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
            <i class="fa fa-angle-up"></i>
        </a>
    </div>