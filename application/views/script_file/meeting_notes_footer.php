 <script>

 	function delete_meeting_notes(taskId)
    {
    	$("#isDelete").val("1");
    	$("#deleteNoteId").val(taskId);
    	$("#modal-delete-meeting-notes").modal();
    }

	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterType.length > 0) 
		{
			$("#filterTypeSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'1'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='DailyButton' class='btn btn-sm btn-primary'><?php echo ucfirst(type); ?> - <?php echo Daily; ?>  &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}

			if (in_array("'2'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='WeeklyButton' class='btn btn-sm btn-primary'><?php echo ucfirst(type); ?> - <?php echo Weekly; ?>  &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'3'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='MonthlyButton' class='btn btn-sm btn-primary'><?php echo ucfirst(type); ?> - <?php echo Monthly; ?>  &nbsp;&nbsp;<i onclick='uncheckType(3)' class='fa fa-times'></i></button>";
			}

			if (in_array("'4'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='YearlyButton' class='btn btn-sm btn-primary'><?php echo ucfirst(type); ?> - <?php echo Yearly; ?>  &nbsp;&nbsp;<i onclick='uncheckType(4)' class='fa fa-times'></i></button>";
			}
			$("#showType").html(html);
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
			$("#showType").html("");
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			// html +="";
			$("#Daily").prop('checked',false);
			$("#DailyButton").remove();
		}
		
		else if(id == "2") 
		{
			// html +="";
			$("#Weekly").prop('checked',false);
			$("#WeeklyButton").remove();
		}

		else if(id == "3") 
		{
			// html +="";
			$("#Monthly").prop('checked',false);
			$("#MonthlyButton").remove();
		}
		
		else if(id == "4") 
		{
			// html +="";
			$("#Yearly").prop('checked',false);
			$("#YearlyButton").remove();
		}

		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
		
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}


    $('form#delete_meeting_notes_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#delete_meeting_notes_submit").html('<?php echo Deleting; ?>...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('MeetingNotes/delete_meeting_notes'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                	var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#delete_meeting_notes_submit").html('<?php echo Delete; ?>')
					}
                },
                error: function() { 
                    alert("<?php echo Errorwhileupdatemeetingnotes; ?>");
                }
           });
    });


    $('#modal-add-meeting-notes').on('hidden.bs.modal', function () 
    {
	  $("#addMeetingNotesButton").fadeIn();
	})

	$('#modal-add-meeting-notes').on('shown.bs.modal', function () 
	{
	  $("#addMeetingNotesButton").fadeOut();
	})


	function selectMachine(machineId)
	{
		var machineIds = $("#machineIds").val().split(",");
		if (in_array(machineId,machineIds)!= -1) 
		{
			$("#machineId"+machineId).removeClass("bluebuttonSelected");
			$("#machineId"+machineId).addClass("bluebuttonUnSelected");
			$("#removeMachine"+machineId).hide();
			for(var i = machineIds.length - 1; i >= 0; i--) 
			{
				if(machineIds[i] == machineId) 
				{
			        machineIds.splice(i, 1);
			    }
			}
			
			var selectedMachineId = machineIds.join(",");
			$("#machineIds").val(selectedMachineId);
		}
		else
		{   
			var checkMachineIds = $("#machineIds").val();
			if (checkMachineIds != "") 
			{
				var selectedMachineId = $("#machineIds").val()+','+machineId;
				$("#machineIds").val(selectedMachineId);
			}
			else
			{
				$("#machineIds").val(machineId);
			}

			$("#machineId"+machineId).removeClass("bluebuttonUnSelected");
			$("#machineId"+machineId).addClass("bluebuttonSelected");
			$("#removeMachine"+machineId).show();
		}
	}

	function selectFilterType(filterType)
	{
		if (filterType == "1") 
		{
			$(".monthlyDate").hide();
			$(".weeklyDate").hide();
			$(".yearlyDate").hide();
			$(".dailyDate").show();
		}
		else if (filterType == "2") 
		{
			$(".dailyDate").hide();
			$(".monthlyDate").hide();
			$(".yearlyDate").hide();
			$(".weeklyDate").show();
		}
		else if (filterType == "3") 
		{
			$(".dailyDate").hide();
			$(".weeklyDate").hide();
			$(".yearlyDate").hide();
			$(".monthlyDate").show();
		}
		else if (filterType == "4") 
		{
			$(".dailyDate").hide();	
			$(".monthlyDate").hide();
			$(".weeklyDate").hide();
			$(".yearlyDate").show();
		}
		$(".filterType").addClass("bluebuttonUnSelected");
		$("#filterType"+filterType).removeClass("bluebuttonUnSelected");
		$("#filterType"+filterType).addClass("bluebuttonSelected");
		$("#filterType").val(filterType);
	}

	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}
	
	$('form#add_meeting_notes_form').submit(function(e) 
	{ 
	    var form = $(this);
        e.preventDefault();
			$("#add_meeting_notes_submit").attr('disabled',true); 
			$("#add_meeting_notes_submit").html('<?php echo Saving; ?>...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('MeetingNotes/add_meeting_notes'); ?>",
				data: form.MytoJson(),  
				dataType: "html",
				success: function(data){
					//console.log(data);
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{
						$('#modal-add-meeting-notes').modal('toggle');  
						$("#add_meeting_notes_submit").html('<?php echo Saved; ?>...')
						
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						location.href = "<?php echo base_url('MeetingNotes/meeting_note_details/') ?>"+obj.noteId;
					}
					else 
					{ 
						$('#modal-add-meeting-notes').modal('toggle');	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#add_meeting_notes_submit").attr('disabled',false); 
						$("#add_meeting_notes_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo ADDNOTE; ?>')
					}
				},
				error: function() 
				{ 
					$('#modal-add-meeting-notes').modal('toggle');
					$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding task'
						});
					$("#add_meeting_notes_submit").attr('disabled',false);
					$("#add_meeting_notes_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo ADDNOTE; ?>')
				}
		   });
	});

	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
			language: "<?php echo datepickerLanguage; ?>"
		});

		$('.datepicker59').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
			language: "<?php echo datepickerLanguage; ?>"
		});

		$('#monthStartDate').datepicker({
			format: "MM, yyyy",
		    viewMode: "months", 
		    minViewMode: "months",
		    language: "<?php echo datepickerLanguage; ?>"
		});

		$('#monthEndDate').datepicker({
			format: "MM, yyyy",
		    viewMode: "months", 
		    minViewMode: "months",
		    language: "<?php echo datepickerLanguage; ?>"
		}).datepicker('setDate',new Date());

		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
            language: "<?php echo datepickerLanguage; ?>"
		});

		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>

		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: new Date(),
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			 ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 
		
		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Createddate; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});


		<?php if ($accessPointEdit == true) 
		{
			$tableData = "[
			 { data: 'noteId' },
			 { data: 'noteName' },
			 { data: 'notes' },
			 { data: 'createdDate' },
			 { data: 'filterType' },
			 { data: 'download' },
			 { data: 'delete' },
		  ]";
			$columnDefs = "[
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    },
			    {
			      targets: 6,
			      orderable: false
			    }
			]";
		}else
		{
			$tableData = "[
			 { data: 'noteId' },
			 { data: 'noteName' },
			 { data: 'notes' },
			 { data: 'createdDate' },
			 { data: 'filterType' },
			 { data: 'download' },
		  ]";
			$columnDefs = "[
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    }
			]";
		}
		?>

		$('#empTable').removeAttr('width').DataTable({
		   "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
		      "first": "<?php echo First; ?>",
		      "last": "<?php echo Last; ?>",
		      "next": "<?php echo Next; ?>",
		      "previous": "<?php echo Previous; ?>",
		      "page": "<?php echo Page; ?>",
		      "of": "<?php echo of; ?>"
		    }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'taskId',
		  'ajax': {
			  'url':'meeting_notes_pagination',
			  "type": "POST",
				"data":function(data) {
					data.filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();

					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  },
		  'columns': <?php echo $tableData; ?>,
		  "columnDefs": <?php echo $columnDefs; ?>, 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				$(".paginate_page").html('<?php echo Page; ?>');
				var paginate_of = $(".paginate_of").text();
				var paginate_of = paginate_of.split(" ");
				$(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
				
			},
	   });

		<?php if ($accessPointEdit == true) { ?>
			$(".col-sm-5").html('<a id="addMeetingNotesButton" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-meeting-notes" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo ADDNOTE; ?></a>');
		<?php } ?>

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
	});

	function uncheckCreatedDate()
 	{
 		$("#showCreatedDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 3000 days'));
			$dateValESET = date("Y-m-d", strtotime('today'));
			$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': 		[moment(), moment()],
				'<?php echo Yesterday; ?>': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': 	[moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': 	[moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			 locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 

		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Createddate; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$("#empTable").DataTable().ajax.reload();
 	}
</script>

