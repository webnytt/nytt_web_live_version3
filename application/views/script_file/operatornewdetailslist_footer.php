<script>  
    function showPassword(id,type)
        {
            if (type == "text") 
            {
                type1 = "'password'";
                class1 = "fa fa-eye"; 
            }
            else
            {
                type1 = "'text'";
                class1 = "fa fa-eye-slash"; 
            }
            $("#"+id).attr("type",type);
            id1 = "'"+id+"'";
            $("#icon"+id).html('<i onclick="showPassword('+id1+','+type1+');" style="margin-top: -5px;" class="'+class1+'"></i>');
        }  

        function readURL(input) 
        {
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $(".file-upload-image").css("width","134px");
                $(".file-upload-image").css("height","134px");
                $(".file-upload-image").css("margin-top","0px");
                $(".file-upload-image").css("border-radius","50%");
                $('.file-upload-image').attr('src', e.target.result);
                $('.image-title').html(input.files[0].name);
            };
           reader.readAsDataURL(input.files[0]);
        }

        function readURLEdit(input,userId) 
        {
            var reader = new FileReader();
            reader.onload = function (e) 
            {

                $(".file-upload-image-edit"+userId).css("width","134px");
                $(".file-upload-image-edit"+userId).css("height","134px");
                $(".file-upload-image-edit"+userId).css("margin-top","0px");
                $(".file-upload-image-edit"+userId).css("border-radius","50%");
                $('.file-upload-image-edit'+userId).attr('src', e.target.result);
                $('.image-title').html(input.files[0].name);
            };
            reader.readAsDataURL(input.files[0]);
        }

        $(document).ready(function() 
        {
            $('form#add_operator_form').submit(function(e) 
            { 
            var form = $(this);
            e.preventDefault();
            $("#submit_new_Operator").html('Saving...')
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Operator/add_new_operator'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    if($.trim(data) == "1") { 
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: '<?php echo Operatoraddedsuccessfully; ?>.',
                            time: 300

                        })
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#submit_new_Operator").html('<?php echo Saved; ?>...')
                        $("#modal_addnewOperator").modal('hide').fadeOut(1500); 
                        $("#submit_new_Operator").html('Save')
                        window.setTimeout(function(){location.reload()})
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: 'Error',
                            text: data,
                            time: 10000
                        })
                        $("#submit_new_Operator").html('Save')
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: '<?php echo Error; ?>',
                        text: '<?php echo Errorwhileaddinguser; ?>.',
                        time: 10000
                    })
                    location.reload();
                }
            });
        });

        $("form[id^='delete_user_form']").submit(function(e) 
        {  
                var form = $(this);
                e.preventDefault();
                var formId = form.attr('id');
                var userId = formId.replace('delete_user_form',''); 

                var answer = $("#checkKeywordUser"+userId).val();
                console.log(answer);
                if (answer != 'DELETE') 
                {
                    $.gritter.add({
                        title: '<?php echo Error; ?>',
                        text: "<?php echo Operatorisnotdeleted; ?>"
                    })
                    return false;
                } 
                else 
                {  

                    $("[id^='delete_user_submit"+userId+"']").html('<?php echo Deleting; ?>...')
                    var suc_div = "[id^='delete_user_success"+userId+"']";
                    var modal_div = "[id^='modal-delete-user"+userId+"']";
                    $.ajax({
                        type: "POST",
                        url: "<?php echo site_url('Operator/delete_user'); ?>", 
                        data: form.serialize(), 
                        dataType: "html",
                        success: function(data)
                        {
                            $.gritter.add({
                                title: '<?php echo Success; ?>',
                                text: '<?php echo Operatordeletedsuccessfully; ?>.'
                            })
                    
                            $("[id^='delete_user_submit"+userId+"']").html('<?php echo Deleted; ?>...')
                            
                            $(modal_div).modal('hide').fadeOut(1500);
                            
                            $("[id^='delete_user_submit"+userId+"']").html('Delete')
                            window.setTimeout(function(){location.reload()})
                        },
                        error: function() { 
                            $.gritter.add({
                                title: '<?php echo Error; ?>',
                                text: '<?php echo Errorwhiledeletingoperator; ?>.'
                            })
                        }
                    });
                }
            });
        $(".userMachineSel").select2({ placeholder: "<?php echo Selectmachines; ?>" }); 
        $("form[id^='assign_form']").submit(function(e) 
        {  
            var form = $(this);
            e.preventDefault();
            var formId = form.attr('id');
            var userId = formId.replace('assign_form',''); 
            $("[id^='assign_form_submit"+userId+"']").html('<?php echo Updating; ?>...')
            var err_div = "[id^='assign_form_error"+userId+"']";
            var suc_div = "[id^='assign_form_success"+userId+"']";
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Operator/assign_machine'); ?>",
                data: form.MytoJson(),  
                dataType: "html",
                success: function(data){
                    
                    if($.trim(data) == "<?php echo Machinesassignedsuccessfully; ?>") {   
                        
                        $.gritter.add({
                            title: 'Success',
                            text: data
                        })
                        
                        $("[id^='assign_form_submit"+userId+"']").html('<?php echo Updated; ?>'); 

                        socket.emit('Assign machine', userId); 
                        window.setTimeout(function(){location.reload()},3000);
                    } else { 
                        $("[id^='assign_form_submit"+userId+"']").html('Update')
                        $.gritter.add({
                            title: 'Error',
                            text: data
                        }); 
                    }
                    
                },
                error: function() 
                { 
                    alert("<?php echo Errorwhileassinguser; ?>");
                }
            });
        }); 
    }); 

    $(document).ready(function() 
    {
        $("form[id^='update_userOperator_form']").submit(function(e) 
        {  
            var form = $(this);
            e.preventDefault();
            var formData = new FormData(this);
            var formId = form.attr('id');
            var userId = formId.replace('update_userOperator_form',''); 

            $("#update_user_submit"+userId).html('<?php echo Updating; ?>...');
            
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Operator/update_operator_details'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    
                    if($.trim(data) == "1")
                    { 
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: '<?php echo Profileupdatedsuccessfully; ?>.'
                        });
                        
                        $("#update_user_submit"+userId).html('<?php echo Updated; ?>...');
                        location.reload(2000);
                    }
                    else if($.trim(data) == "2") 
                    { 
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: '<?php echo Noupdatetosave; ?>.'
                        });
                        
                        $("#update_user_submit"+userId).html('Save');
                    }
                    else 
                    {  
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: data
                        })
                        $("#update_user_submit"+userId).html('Save');
                    }
                    
                },
                error: function() { 
                    $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: '<?php echo Errorwhileupdatingprofile; ?>.'
                        });
                    location.reload();
                }
           });
        
        });
    }); 
    
    function generateUser(id) 
    {  
        $("#userName").val("username"+id)
        $("#userPassword").val("password"+id)
    }
</script>