<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
	<script>
		function changeFilterValue()
		{
			machineIdStop = $('#machineIdStop').val();
			reloadGraph1(machineIdStop);
		}
		function changeAnalyticsType(type)
		{
			if (type == "waitAnalytics") 
			{
				$(".stopAnalytics").css("opacity","0.5");
				$(".waitAnalytics").css("opacity","1");
			}
			else
			{
				$(".waitAnalytics").css("opacity","0.5");
				$(".stopAnalytics").css("opacity","1");
			}
			$('#analytics').val(type);
			machineIdStop = $('#machineIdStop').val();
			reloadGraph1(machineIdStop);
		}

		$(document).ready(function() 
		{ 
			$('.datepicker').datepicker({
					format: "mm/dd/yyyy"
				});
		});

		$(".regular").slick({
	        dots: true,
	        infinite: false,
	        slidesToShow: 4,
	        slidesToScroll: 4,
	          responsive: [
		        {
			        breakpoint: 1024,
			        settings: 
			        {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 3,
			            slidesToScroll: 3
			        }
		        },
		        {
		          	breakpoint: 600,
		          	settings: 
		          	{
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 2,
			            slidesToScroll: 2
		          	}
		        },
		        {
		          	breakpoint: 480,
		          	settings: 
		          	{
		          		dots: false,
	        			infinite: false,
		            	slidesToShow: 1,
		            	slidesToScroll: 1
		          	}
		        }
		    ],
		});

		$('#carousel-example1').on('slide.bs.carousel', function (e) 
		{   
		    var $e = $(e.relatedTarget);
		    var idx = $e.index();
		    var itemsPerSlide = 5;
		    var totalItems = $('.carousel-item').length;
		 
		    if (idx >= totalItems-(itemsPerSlide-1)) {
		        var it = itemsPerSlide - (totalItems - idx);
		        for (var i=0; i<it; i++) {
		            // append slides to end
		            if (e.direction=="left") {
		                $('.carousel-item').eq(i).appendTo('.carousel-inner');
		            }
		            else {
		                $('.carousel-item').eq(0).appendTo('.carousel-inner');
		            }
		        }
		    }
		});

		(function($) 
		{
		  	$.fn.inputFilter = function(inputFilter) 
		  	{
		    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		      if (inputFilter(this.value)) {
		        this.oldValue = this.value;
		        this.oldSelectionStart = this.selectionStart;
		        this.oldSelectionEnd = this.selectionEnd;
		      } else if (this.hasOwnProperty("oldValue")) {
		        this.value = this.oldValue;
		        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		      } else {
		        this.value = "";
		      }
		    });
		  };
		}(jQuery));


		$(document).ready(function() { 

			$(".inputText").inputFilter(function(value) 
			{
			    return /^\d*$/.test(value);

			});
			$("#duration2").keyup(function(){
			  	var duration2 = parseInt($(this).val());
			  	var duration3 = parseInt($("#duration3").val());
			  	var duration4 = parseInt($("#duration4").val());
			  	if (duration2 >= duration3) 
			  	{
			  		$.gritter.add({
						title: 'Error',
						text: "Please enter valid increasing values for blocks."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
			  		console.log(duration2);
			  		if (duration2 != NaN) 
			  		{
						$(".duration2Text").text(duration2+" mins");
			  		}
			  	}

			  	if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			$("#duration3").keyup(function()
			{
			  	var duration2 = parseInt($("#duration2").val());
			  	var duration3 = parseInt($(this).val());
			  	var duration4 = parseInt($("#duration4").val());
			  	if (duration3 >= duration4) 
			  	{
			  		$.gritter.add({
						title: 'Error',
						text: "Please enter valid increasing values for blocks."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else if(duration3 < duration2) 
			  	{
			  		$.gritter.add({
						title: 'Error',
						text: "Please enter valid decreasing values for blocks."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
			  		$(".duration3Text").text(duration3+" mins");
			  	}
				if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			$("#duration4").keyup(function()
			{
			  	var duration4 = parseInt($(this).val());
			  	var duration3 = parseInt($("#duration3").val());
			  	var duration2 = parseInt($("#duration2").val());

			  	if (duration4 < duration3) 
			  	{
			  		$.gritter.add({
						title: 'Error',
						text: "Please enter valid decreasing values for blocks."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
					$(".duration4Text").text(duration4+" mins");
			  	}
			  	if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			var machineId = $("#machineIdStop").val();
			if (machineId != 0) 
			{
				reloadGraph1(machineId);
			}
			
			$(".filter1").click(function() 
			{
				var clickedId = $(this).attr('id');
				var machineId = clickedId.replace('machineStop',''); 
				$('#machineIdStop').val(machineId);
				$(".filter1").removeClass('active');
				$(this).addClass('active');
				var machineId = $("#machineIdStop").val();
				reloadGraph1(machineId);
			});
			
			$(document).on('click', '.widget1', function() 
			{
				if($(this).hasClass("widget_bordered")) 
				{
					$(this).removeClass("widget_bordered");
				} 
				else 
				{
					$(this).addClass("widget_bordered");
				}
				var machineId = $("#machineIdStop").val();
				reloadGraph1(machineId);
			});
		});
		
		function formatSeconds(value)
		{
			var sec_num = parseInt(value, 10); 
			var minutes = Math.floor(sec_num / 60);
			var seconds = sec_num - (minutes * 60);
			if (minutes < 10) {minutes = "0"+minutes;}
			if (seconds < 10) {seconds = "0"+seconds;}
			return minutes+':'+seconds;
		}
		
		function reloadGraph1(machineId) 
		{ 
			var theme_color = '#ff8000'; 
			choose = $("#choose2").val().split("/");
			var choose1 = choose[2];  
			var choose2 = choose[0];  
			var choose3 = $("#choose3").val();  
			var choose4 = choose[1];
			var duration1Min = parseInt($("#duration1").val());
			var duration2Min = parseInt($("#duration2").val());
			var duration3Min = parseInt($("#duration3").val());
			var duration4Min = parseInt($("#duration4").val());
			var analytics = $("#analytics").val();

			if($("#filter1widget").hasClass("widget_bordered")) 
			{
				$("#filter1widget").css('opacity',"1");
				$("#filter1widgetbutton").show();
				var filter1Val = 1;
			} 
			else 
			{
				$("#filter1widget").css('opacity',"0.5");
				$("#filter1widgetbutton").hide();
				var filter1Val = 0;
			}
			if($("#filter2widget").hasClass("widget_bordered")) 
			{
				$("#filter2widget").css('opacity',"1");
				$("#filter2widgetbutton").show();
				var filter2Val = 1;
			} 
			else 
			{
				$("#filter2widget").css('opacity',"0.5");
				$("#filter2widgetbutton").hide();
				var filter2Val = 0;
			}
			if($("#filter3widget").hasClass("widget_bordered")) 
			{
				$("#filter3widget").css('opacity',"1");
				$("#filter3widgetbutton").show();
				var filter3Val = 1;
			} 
			else 
			{
				$("#filter3widget").css('opacity',"0.5");
				$("#filter3widgetbutton").hide();
				var filter3Val = 0;
			} 
			if($("#filter4widget").hasClass("widget_bordered")) 
			{
				$("#filter4widget").css('opacity',"1");
				$("#filter4widgetbutton").show();
				var filter4Val = 1;
			} 
			else 
			{
				$("#filter4widget").css('opacity',"0.5");
				$("#filter4widgetbutton").hide();
				var filter4Val = 0;
			} 
			
			var factoryId = "<?php  echo $this->session->userdata('factoryId'); ?>";
			$('.graph-loader').addClass("show").removeClass("hide"); 
			if (duration1Min < duration2Min && duration2Min < duration3Min && duration3Min < duration4Min) 
		  	{
				$.post('<?php echo base_url();?>Analytics/breakdown_pagination',{machineId:machineId,choose1:choose1,choose2:choose2,choose3:choose3,choose4:choose4,filter1Val:filter1Val,filter2Val:filter2Val,filter3Val:filter3Val,filter4Val:filter4Val,factoryId:factoryId,duration1:duration1Min,duration2:duration2Min,duration3:duration3Min,duration4:duration4Min,analytics:analytics}, function getattribute(returnData)
				{   
					$('.graph-loader').addClass("hide").removeClass("show");
					
					$("#filter1").html(returnData.daily.filter1+" mins");
					$("#filter1count").html(returnData.daily.filter1count+" stops");
					
					$("#filter2").html(returnData.daily.filter2+" mins");
					$("#filter2count").html(returnData.daily.filter2count+" stops");
					
					$("#filter3").html(returnData.daily.filter3+" mins");
					$("#filter3count").html(returnData.daily.filter3count+" stops");
					
					$("#filter4").html(returnData.daily.filter4+" mins");
					$("#filter4count").html(returnData.daily.filter4count+" stops");

					$("#errorNotification").text(returnData.machineNotifyTime.errorNotification);
					$("#errorReasonNotification").text(returnData.machineNotifyTime.errorReasonNotification);
					
					var duration1 = parseInt($("#duration1").val())*60;
					var duration2 = parseInt($("#duration2").val())*60;
					var duration3 = parseInt($("#duration3").val())*60;
					var duration4 = parseInt($("#duration4").val())*60;
						var seriesText111, seriesText11, seriesText12, seriesText13, seriesText14, seriesText15, seriesText16, seriesText17, seriesText18, seriesText19, seriesText20, seriesText21;
						var seriesText222, seriesText21, seriesText22, seriesText23, seriesText24, seriesText25, seriesText26, seriesText27, seriesText28, seriesText29, seriesText30, seriesText31;
						var seriesText333, seriesText31, seriesText32, seriesText33, seriesText34, seriesText35, seriesText36, seriesText37, seriesText38, seriesText39, seriesText40, seriesText41;
						var seriesText444, seriesText41, seriesText42, seriesText43, seriesText44, seriesText45, seriesText46, seriesText47, seriesText48, seriesText49, seriesText50, seriesText51;
						
						var f1w = 0;  
					if($("#filter1widget").hasClass('widget_bordered') === true) 
					{
						f1w = 1;

						seriesText111 =	{
							type: 'line',
							lineStyle:{
								width:0
							},
							symbolSize: 0,
							data: [[0,duration2], [86400,duration2]],
							itemStyle: {
				                color: '#FF8000'
				            },
						    areaStyle: {
				                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
				                    offset: 0,
				                    color: '#FF8000'
				                }, {
				                    offset: 1,
				                    color: '#ffffff'
				                }])
				            },
						} 

						seriesText1 = {
							symbolSize: 6,
							data: returnData.daily.stopfilter1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#5d4de2',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
						
						seriesText11 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li1,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi1.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
						
						seriesText12 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li2,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi2.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText13 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li3,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi3.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
						
						seriesText14 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li4,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi4.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText15 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li5,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi5.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText16 = {
							    symbolSize: 6,
								data: returnData.daily.reasonfilter1.li6,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi6.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}
						seriesText17 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li7,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi7.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}
						seriesText18 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li8,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi8.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText19 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li9,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi9.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText20 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li10,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi10.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText21 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li11,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: '#d32f2f',
						            color: '#ffffff'
						        },
								type: 'scatter',
							}


					} else { 
						
						seriesText111 =	{
							symbolSize: 6,
							data: '',
							itemStyle: {
								normal: {
									color:'#F67280',
								 }
							},
							type: 'scatter',
						} 

						seriesText1 = {
							symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#F8B195',
								 }
							},
							type: 'scatter',
						}
						
						seriesText11 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#CC6600',
									 }
								},
								type: 'scatter',
							} 
						
						seriesText12 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#9933CC',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText13 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#0000FF',
									 }
								},
								type: 'scatter',
							} 
						
						seriesText14 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#FF00CC',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText15 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#999900',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText16 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#FF6600',
									 }
								},
								type: 'scatter',
							}
						seriesText17 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#00796b',
									 }
								},
								type: 'scatter',
							}
						seriesText18 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText19 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText20 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText21 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
						
					}
				
				var f2w = 0;
				if($("#filter2widget").hasClass('widget_bordered') === true) {
					f2w = 1;

					seriesText222 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration3], [86400,duration3]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 


					seriesText2 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter2,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#8b82d2',
				            color: '#ffffff'
				        },
						type: 'scatter', 
					} 
					
					seriesText21 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText22 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText23 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText24 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText25 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText26 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText27 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText28 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText29 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText30 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText31 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {
					
					seriesText222 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#F67280',
							 }
						},
						type: 'scatter',
					} 

					seriesText2 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#F67280',
							 }
						},
						type: 'scatter',
					} 
					
					seriesText21 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText22 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText23 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText24 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText25 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText26 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText27 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText28 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText29 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText30 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText31 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var f3w = 0; 
				if($("#filter3widget").hasClass('widget_bordered') === true) {
					f3w = 1;

					seriesText333 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration4], [86400,duration4]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 

					seriesText3 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter3,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#c57421',
				            color: '#ffffff'
					    },
						type: 'scatter',
					}
					
					seriesText31 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText32 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText33 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText34 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText35 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText36 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText37 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText38 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText39 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText40 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText41 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {

					seriesText333 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#C06C84',
							 }
						},
						type: 'scatter',
					} 

					seriesText3 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#C06C84',
							 }
						},
						type: 'scatter',
					}
					
					seriesText31 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText32 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText33 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText34 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText35 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText36 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText37 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText38 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText39 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText40 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText41 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var f4w = 0;

				
				if($("#filter4widget").hasClass('widget_bordered') === true) {
					f4w = 1;
					duration5 = returnData.daily.maxSecondsValue;

					seriesText444 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration5], [86400,duration5]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 

					seriesText4 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter4,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#1d0c96',
				            color: '#ffffff'
					    },
						type: 'scatter',
					}
					
					seriesText41 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText42 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText43 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText44 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText45 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText46 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText47 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText48 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText49 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText50 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText51 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {

					seriesText444 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#6C5B7B',
							 }
						},
						type: 'scatter',
					} 

					
					seriesText4 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#6C5B7B',
							 }
						},
						type: 'scatter',
					}
					
					seriesText41 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText42 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText43 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText44 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText45 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText46 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText47 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText48 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText49 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText50 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
					
					seriesText51 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var Daily5 = document.getElementById('DailyDowntime1');
					var DailyDowntime1 = echarts.init(Daily5);  
					var DailyDowntime1Opt = {
						tooltip : {
							axisPointer : {            
								type : 'shadow'
							},
						
						formatter:  
							function (params, ticket, callback) {
								
								var seriesName = (params.value[2] == undefined) ? "Short red" : params.value[2];
								var sec_num1 = parseInt(params.value[0], 10);  
								var hours1  = Math.floor(sec_num1 / 3600);
								var minutes1 = Math.floor((sec_num1 - (hours1 * 3600)) / 60);
								var seconds1 = sec_num1 - (hours1 * 3600) - (minutes1 * 60);

								if (hours1   < 10) {hours1   = "0"+hours1;}
								if (minutes1 < 10) {minutes1 = "0"+minutes1;}
								if (seconds1 < 10) {seconds1 = "0"+seconds1;}
								var value1 = hours1+':'+minutes1+':'+seconds1;
								
								var sec_num2 = parseInt(params.value[1], 10); 
								var minutes2 = Math.floor(sec_num2 / 60);
								var seconds2 = sec_num2 - (minutes2 * 60);
								
								if (minutes2 < 10) {minutes2 = "0"+minutes2;}
								if (seconds2 < 10) {seconds2 = "0"+seconds2;}
								var value2 = minutes2+':'+seconds2;
								
								return seriesName+" at "+value1+" <br/> for "+value2+" minutes";
							}
						}, 
						legend: {
							data: ['Stopped'],
						},
						xAxis:  {
							type: 'value',
							splitLine: false,
							axisLabel: {
								color:"#000000",
							
								formatter:  
									function (value, index) {
										
										var sec_num = parseInt(value, 10); 
										var hours   = Math.floor(sec_num / 3600);
										var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
										var seconds = sec_num - (hours * 3600) - (minutes * 60);

										if (hours   < 10) {hours   = "0"+hours;}
										if (minutes < 10) {minutes = "0"+minutes;}
										if (seconds < 10) {seconds = "0"+seconds;}

										return hours+':'+minutes+':'+seconds;
									},  
							},
							axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false,
					            
					        },  
					        interval: 14400, 
					        max:86400
						},
						grid: {
							left: '10%',
							right: '5%',
						}, 
						yAxis: {
							type : 'value',
							name:"DURATION(SEC)" ,
							nameTextStyle: {
								color:"#000000",
								fontSize:12, 
								right:15,
							},
							scale: true,
					        splitLine: {
					            lineStyle: {
					                type: 'dot'
					            }
					        },
					        axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false
					        }
							
						},
						 dataZoom: [
						    {
					            type: 'inside'
					        }, 
					        {
					            type: 'slider',
					            showDataShadow: false,
					            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
					            handleSize: '80%',
					            handleStyle: {
					                color: '#999',
					                shadowBlur: 3,
					                shadowColor: 'rgba(0, 0, 0, 0.6)',
					                shadowOffsetX: 2,
					                shadowOffsetY: 2
					            },
					            top:320,
					        }, 
					        {
					            type: 'inside',
					            orient: 'vertical'
					        }, 
					        {
					            type: 'slider',
					            orient: 'vertical',
					            showDataShadow: false,
					            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
					            handleSize: '80%',
					            handleStyle: {
					                color: '#999',
					                shadowBlur: 3,
					                shadowColor: 'rgba(0, 0, 0, 0.6)',
					                shadowOffsetX: 2,
					                shadowOffsetY: 2
					            },
					            right:0,
					        }
					        ], 
						series: [
							{
						        type: 'scatter',
						        symbolSize: 0,
						        data: [[0,0], [30,30]],   
						        itemStyle: {
						            color: "blue"
						        }
						    }, 
							
						],
						  
					};  
				
						DailyDowntime1Opt.series.push(seriesText1, seriesText11, seriesText12, seriesText13, seriesText14, seriesText15, seriesText16, seriesText17, seriesText18, seriesText19, seriesText20, seriesText21, seriesText111);
					
					
						DailyDowntime1Opt.series.push(seriesText2, seriesText21, seriesText22, seriesText23, seriesText24, seriesText25, seriesText26, seriesText27, seriesText28, seriesText29, seriesText30, seriesText31, seriesText222);
					
					
						DailyDowntime1Opt.series.push(seriesText3, seriesText31, seriesText32, seriesText33, seriesText34, seriesText35, seriesText36, seriesText37, seriesText38, seriesText39, seriesText40, seriesText41, seriesText333);
					
					
						DailyDowntime1Opt.series.push(seriesText4, seriesText41, seriesText42, seriesText43, seriesText44, seriesText45, seriesText46, seriesText47, seriesText48, seriesText49, seriesText50, seriesText51, seriesText444);
					
					
					
					
					
					DailyDowntime1.setOption(DailyDowntime1Opt); 
					
					
					var i;
					var RD1 = returnData.daily.reasonCountArrDuration1.reverse();
					var FRD1 = new Array(RD1.length);
					for (i = 0; i < RD1.length; i++) {
						if(typeof RD1[i].value == 'undefined') { RD1[i].value = 0; }
						FRD1[i] = formatSeconds(RD1[i].value);
					} 
					
					var RD2 = returnData.daily.reasonCountArrDuration2.reverse();
					var FRD2 = new Array(RD2.length);
					for (i = 0; i < RD2.length; i++) {
						if(typeof RD2[i].value == 'undefined') { RD2[i].value = 0; }
						FRD2[i] = formatSeconds(RD2[i].value);
					} 
					var RD3 = returnData.daily.reasonCountArrDuration3.reverse();
					var FRD3 = new Array(RD3.length);
					for (i = 0; i < RD3.length; i++) {
						if(typeof RD3[i].value == 'undefined') { RD3[i].value = 0; }
						FRD3[i] = formatSeconds(RD3[i].value);
					} 
					var RD4 = returnData.daily.reasonCountArrDuration4.reverse();
					var FRD4 = new Array(RD4.length);
					for (i = 0; i < RD4.length; i++) {
						if(typeof RD4[i].value == 'undefined') { RD4[i].value = 0; }
						FRD4[i] = formatSeconds(RD4[i].value);
					} 
					
					
					var RC1 = returnData.daily.reasonCountArr1.reverse();
					var FRC1 = new Array(RC1.length);
					for (i = 0; i < RC1.length; i++) {
						if(typeof RC1[i].value == 'undefined') { RC1[i].value = 0; }
						FRC1[i] = RC1[i].value;
					}  
					
					var RC2 = returnData.daily.reasonCountArr2.reverse();
					var FRC2 = new Array(RC2.length);
					for (i = 0; i < RC2.length; i++) {
						if(typeof RC2[i].value == 'undefined') { RC2[i].value = 0; }
						FRC2[i] = RC2[i].value;
					}  
					
					var RC3 = returnData.daily.reasonCountArr3.reverse();
					var FRC3 = new Array(RC3.length);
					for (i = 0; i < RC3.length; i++) {
						if(typeof RC3[i].value == 'undefined') { RC3[i].value = 0; }
						FRC3[i] = RC3[i].value;
					}  
					
					var RC4 = returnData.daily.reasonCountArr4.reverse();
					var FRC4 = new Array(RC4.length);
					for (i = 0; i < RC4.length; i++) {
						if(typeof RC4[i].value == 'undefined') { RC4[i].value = 0; }
						FRC4[i] = RC4[i].value;
					}  
					
					maxSecondsValue = 600;
					if(f1w == 1) 
					{
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr1Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr1,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F8B195" 
								}
							}
						};
						var reasonCountArrDuration1Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration1, 
							type: 'bar',
							stack: 1, 
						};
					} else {
						var reasonCountArr1Text = {
							name:'Reason Count', 
							data:[{"value":60,"itemStyle":{"color":"#CC6600"}},{"value":0,"itemStyle":{"color":"#9933CC"}},{"value":0,"itemStyle":{"color":"#0000FF"}},{"value":0,"itemStyle":{"color":"#FF00CC"}},{"value":0,"itemStyle":{"color":"#999900"}},{"value":0,"itemStyle":{"color":"#FF6600"}},{"value":0,"itemStyle":{"color":"#00796b"}},{"value":0,"itemStyle":{"color":"#d32f2f"}}], 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F8B195" 
								}
							}
						};
						var reasonCountArrDuration1Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 
					
					if(f2w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr2Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr2,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F67280" 
								}
							}
						};
						var reasonCountArrDuration2Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration2, 
							type: 'bar',
							stack: 1, 
						};
					} else {
						var reasonCountArr2Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F67280" 
								}
							}
						};
						var reasonCountArrDuration2Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 
					if(f3w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr3Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr3,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#C06C84" 
								}
							}
						};
						var reasonCountArrDuration3Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration3, 
							type: 'bar',
							stack: 1,
						};
					} else {
						var reasonCountArr3Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#C06C84" 
								}
							}
						};
						var reasonCountArrDuration3Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 	
					if(f4w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr4Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr4,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#6C5B7B" 
								}
							}
						};
						var reasonCountArrDuration4Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration4, 
							type: 'bar',
							stack: 1,
						};
					} else {
						var reasonCountArr4Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#6C5B7B" 
								}
							}
						};
						var reasonCountArrDuration4Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					}

					/*var seriesText1 = {
								name: returnData.daily.machine.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue1,
								color: '#FFCF00',
							};
						var seriesText2 = {
								name: returnData.daily.machine.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue2,
								color: '#FCD219',
							};
						var seriesText3 = {
								name: returnData.daily.machine.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue3,
								color: '#FFD933',
							};
						var seriesText4 = {
								name: returnData.daily.machine.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue4,
								color: '#FFDD4D',
							};
						var seriesText5 = {
								name: returnData.daily.machine.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue5,
								color: '#FFE266',
							};
						var seriesText6 = {
								name: returnData.daily.machine.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue6,
								color: '#FFE780',
							};
						var seriesText7 = {
								name: returnData.daily.machine.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue7,
								color: '#FFEC99',
							};
						var seriesText8 = {
								name: returnData.daily.machine.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue8,
								color: '#FFF1B3',
							};
						var seriesText9 = {
								name: returnData.daily.machine.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue9,
								color: '#FFF5CC',
							};
						var seriesText10 = {
								name: returnData.daily.machine.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue10,
								color: '#FFFAE6',
							};*/

					

					//console.log(returnData.daily.maxValueArr);
					var intervalValue = returnData.daily.maxValueArr / 8;

					//console.log(intervalValue);
					
					var DailyDowntime2 = echarts.init(document.getElementById('DailyDowntime2'));  
					var DailyDowntime2Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						formatter: 
							function (params) {
							
							var outStrng = "";	
							for (i = 0; i < params.length; i++) {
							  if (params[i].value != 0) 
							  {
							  	var outStrng = outStrng.concat(params[i].seriesName+": "+params[i].value+" min<br/>");
							  }
							}
							return outStrng; 
								
							}, 
						
						},
						title:{
							text:"Downtime split analysis",
							show:false,
							x:"right",
							y:"top", 
							textStyle:{
								color: theme_color,  
								fontSize:16, 
							},
						}, 
						legend: {
							show:false, 
						},
						grid: {
							left: '30%',
							right: '4%',
						}, 
						yAxis: {
							
							data:returnData.daily.reasonNameArr.reverse(), 
							name:"ERROR TYPE" ,
							nameTextStyle: {
								color:"#000000",
								fontSize:12, 
							},
							axisLabel: { 
								rotate: 0,
								verticalAlign: 'middle',
								color:"#124D8D",
							},
							axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false
					        },
						},
						xAxis: 
							{
								type: 'value',
								min: 0,
								showSymbol: false,
								hoverAnimation: true,
								axisLine: {
						            show: false
						        },
								splitLine: {
						            lineStyle: {
						                type: 'dot'
						            }
						        },
						        axisTick: {
						            show: false
						        },
						        interval: intervalValue, 
						        max: returnData.daily.maxValueArr, 
								axisLabel: {
									
									formatter:  
									function (value, index) {
										
										var sec_num = parseInt(value, 10); 
										var minutes = Math.floor(sec_num / 60);
										var seconds = sec_num - (minutes * 60);
										if (minutes < 10) {minutes = "0"+minutes;}
										if (seconds < 10) {seconds = "0"+seconds;}
										return minutes+':'+seconds;
									},  
									color:"black"
								},
								nameTextStyle: {
									color:"black",
								},

							}
						, 
						series: [
							/*seriesText1,
							seriesText2,
							seriesText3,
							seriesText4,
							seriesText5,
							seriesText6,
							seriesText7*/
							{
								name: returnData.daily.machine.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue1,
								color: '#FFCF00',
							},
							{
								name: returnData.daily.machine.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue2,
								color: '#FCD219',
							},
							{
								name: returnData.daily.machine.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue3,
								color: '#FFD933',
							},
							{
								name: returnData.daily.machine.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue4,
								color: '#FFDD4D',
							},
							{
								name: returnData.daily.machine.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue5,
								color: '#FFE266',
							},
							{
								name: returnData.daily.machine.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue6,
								color: '#FFE780',
							},
							{
								name: returnData.daily.machine.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue7,
								color: '#FFEC99',
							},
							{
								name: returnData.daily.machine.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue8,
								color: '#FFF1B3',
							},
							{
								name: returnData.daily.machine.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue9,
								color: '#FFF5CC',
							},
							{
								name: returnData.daily.machine.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue10,
								color: '#FFFAE6',
							},
							{
								name: returnData.daily.tool.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue1,
								color: '#Ff8000',
							},
							{
								name: returnData.daily.tool.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue2,
								color: '#FF8D1A',
							},
							{
								name: returnData.daily.tool.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue3,
								color: '#Ff9933',
							},
							{
								name: returnData.daily.tool.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue4,
								color: '#FFA64D',
							},
							{
								name: returnData.daily.tool.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue5,
								color: '#FFB366',
							},
							{
								name: returnData.daily.tool.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue6,
								color: '#FFC080',
							},
							{
								name: returnData.daily.tool.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue7,
								color: '#FFCC99',
							},
							{
								name: returnData.daily.tool.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue8,
								color: '#FFD9B3',
							},
							{
								name: returnData.daily.tool.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue9,
								color: '#FFE6CC',
							},
							{
								name: returnData.daily.tool.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.tool.seriesValue10,
								color: '#FFF2E6',
							},
							{
								name: returnData.daily.operator.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue1,
								color: '#124D8D',
							},
							{
								name: returnData.daily.operator.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue2,
								color: '#2A5F98',
							},
							{
								name: returnData.daily.operator.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue3,
								color: '#4171A4',
							},
							{
								name: returnData.daily.operator.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue4,
								color: '#5982AF',
							},
							{
								name: returnData.daily.operator.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue5,
								color: '#7194BB',
							},
							{
								name: returnData.daily.operator.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue6,
								color: '#89A6C6',
							},
							{
								name: returnData.daily.operator.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue7,
								color: '#A0B8D1',
							},
							{
								name: returnData.daily.operator.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue8,
								color: '#B8CADD',
							},
							{
								name: returnData.daily.operator.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue9,
								color: '#D0DBE8',
							},
							{
								name: returnData.daily.operator.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.operator.seriesValue10,
								color: '#E7EDF4',
							},
							{
								name: returnData.daily.material.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue1,
								color: '#002060',
							},
							{
								name: returnData.daily.material.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue2,
								color: '#1A3670',
							},
							{
								name: returnData.daily.material.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue3,
								color: '#334D80',
							},
							{
								name: returnData.daily.material.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue4,
								color: '#4D6390',
							},
							{
								name: returnData.daily.material.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue5,
								color: '#6679A0',
							},
							{
								name: returnData.daily.material.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue6,
								color: '#8090B0',
							},
							{
								name: returnData.daily.material.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue7,
								color: '#99A6BF',
							},
							{
								name: returnData.daily.material.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue8,
								color: '#B3BCCF',
							},
							{
								name: returnData.daily.material.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue9,
								color: '#CCD2DF',
							},
							{
								name: returnData.daily.material.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.material.seriesValue10,
								color: '#E6E9EF',
							},
							{
								name: returnData.daily.other.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue1,
								color: '#CCD2DF',
							},
							{
								name: returnData.daily.other.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue2,
								color: '#D1D7E2',
							},
							{
								name: returnData.daily.other.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue3,
								color: '#D6DBE5',
							},
							{
								name: returnData.daily.other.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue4,
								color: '#DBE0E9',
							},
							{
								name: returnData.daily.other.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue5,
								color: '#E0E4EC',
							},
							{
								name: returnData.daily.other.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue6,
								color: '#E6E9EF',
							},
							{
								name: returnData.daily.other.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue7,
								color: '#EBEDF2',
							},
							{
								name: returnData.daily.other.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue8,
								color: '#F0F2F5',
							},
							{
								name: returnData.daily.other.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue9,
								color: '#F5F6F9',
							},
							{
								name: returnData.daily.other.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.other.seriesValue10,
								color: '#FAFBFC',
							},
							{
								name: returnData.daily.unanswered.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.unanswered.seriesValue1,
								color: '#d32f2f',
							}

						],
					};  
					
						

						/*var seriesText1 = {
								name: returnData.daily.machine.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue1,
								color: '#FFCF00',
							};
						var seriesText2 = {
								name: returnData.daily.machine.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue2,
								color: '#FCD219',
							};
						var seriesText3 = {
								name: returnData.daily.machine.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue3,
								color: '#FFD933',
							};
						var seriesText4 = {
								name: returnData.daily.machine.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue4,
								color: '#FFDD4D',
							};
						var seriesText5 = {
								name: returnData.daily.machine.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue5,
								color: '#FFE266',
							};
						var seriesText6 = {
								name: returnData.daily.machine.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue6,
								color: '#FFE780',
							};
						var seriesText7 = {
								name: returnData.daily.machine.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue7,
								color: '#FFEC99',
							};
						var seriesText8 = {
								name: returnData.daily.machine.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue8,
								color: '#FFF1B3',
							};
						var seriesText9 = {
								name: returnData.daily.machine.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue9,
								color: '#FFF5CC',
							};
						var seriesText10 = {
								name: returnData.daily.machine.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue10,
								color: '#FFFAE6',
							};*/


					//DailyDowntime2.series.push(seriesText1,seriesText2,seriesText3,seriesText4,seriesText5,seriesText6,seriesText7,seriesText8,seriesText9,seriesText10);
					

					DailyDowntime2.setOption(DailyDowntime2Opt);


					
				},  'json');

			}



			
		}
		
	</script>


	<script type="text/javascript">

		$('#modal-changetimeadd').on('hidden.bs.modal', function () {
			$(".addBlock").css("opacity","0.5");
		})

		$('#modal-changetimeadd').on('shown.bs.modal', function () {
			$(".addBlock").css("opacity","1");
		})
		
		
	</script>

