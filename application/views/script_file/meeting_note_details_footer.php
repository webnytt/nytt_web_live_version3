<script src="https://cdnjs.cloudflare.com/ajax/libs/html2canvas/0.4.1/html2canvas.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jspdf/1.0.272/jspdf.debug.js"></script>
<script>
    function closeDetails()
    {
    	problemId = $("#problemId").val();
    	$("#"+problemId).css("background", "white");
		$("#"+problemId).css("border-left", "none");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    function downloadPdf()
    {
    	let doc = new jsPDF('p','pt','a4');
    	download = $("download");
		doc.addHTML($("#content"),function() {
		    doc.save('html.pdf');
		});
    }

    function checkwordcount()
	{
		var description = $("#description").val();
		var noteId = $("#noteId").val();
		var descriptionLength = description.length;
		$("#descriptionLength").text(descriptionLength);
		$("#notesText").val(description);

		$.ajax({ 
            type: 'POST',
            url: "<?php echo site_url('MeetingNotes/update_MeetingNotes'); ?>",
            data:{noteId:noteId,description:description},
            success: function(data)
            {
            },
            error: function() { 
                alert("<?php echo Errorwhileupdatereportproblem; ?>.");
            }
       });

	}


	function checkReportWordCount(id)
	{
		var description = $("#"+id).val();
		var descriptionLength = description.length;
		$("#"+id+"Length").text(descriptionLength);
	}


    $('form#update_reportProblem_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#update_reportProblem_submit").html('<?php echo Saving;?>...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('admin/update_reportProblem'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    $.gritter.add({
            			title: '<?php echo Success; ?>',
            			text: "<?php echo Reportproblemupdatedsuccessfully; ?>."
            		})
                    $("#update_reportProblem_submit").html('<?php echo Save; ?>')
                    window.setTimeout(function(){location.reload()},3000)
                },
                error: function() { 
                    alert("<?php echo Errorwhileupdatereportproblem; ?>.");
                }
           });
    });


	$(document).ready(function() 
	{
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today')); ?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
	
		$('table').on( 'click', 'tr', function () 
		{
		    problemId = $(this).attr('id');
		    if(problemId != "" && problemId != undefined)
		    {
				 $.ajax({
			        url: "<?php echo base_url('Admin/getReportProblemById') ?>",
			        type: "post",
			        data: {problemId:problemId} ,
			        success: function (response) 
			        {
			        	var obj = $.parseJSON(response);

			        	userId = obj.userId;
			        	userName = obj.userName;
			        	date = obj.date;
			        	type = obj.type;
			        	description = obj.description;
			        	cause = obj.cause;
			        	improvement = obj.improvement;
			        	status = obj.status;
			        	isActive = obj.isActive;
						userImage = "<?php echo base_url('assets/img/user/') ?>"+obj.userImage;
			        	picture = "<?php echo base_url('assets/img/reportProblem/') ?>"+obj.picture;
						$("#userImage").attr("src",userImage);

			        	if (status == "1") 
			        	{
			        		$("#not_confidentials").hide();
			        		$("#confidentials").show();
			        	}
			        	else
			        	{
			        		$("#confidentials").hide();
			        		$("#not_confidentials").show();
			        	}

			        	$("#userId").text("<?php echo UserId; ?> : "+userId);
			        	$("#userName").text(userName);
			        	$("#date").text(date);
			        	$("#type").val(type);
			        	if (type == "1") 
			        	{
			        		$("#typeText").text("<?php echo Accident; ?>");
			        	}
			        	else if (type == "0") 
			        	{
			        		$("#typeText").text("<?php echo Incident; ?>");
			        	}
			        	else 
			        	{
			        		$("#typeText").text("<?php echo Suggestion; ?>");
			        	}
			        	$("#descriptionText").val(description);
			        	$("#descriptionTextLength").text(description.length);
			        	$("#cause").val(cause);
			        	$("#causeLength").text(cause.length);
			        	$("#improvement").val(improvement);
			        	$("#improvementLength").text(improvement.length);
			        	$("#problemId").val(problemId);
						$("#resolved").attr("checked",false);
			        	if (isActive == "1") 
			        	{
			        		$("#resolved").attr("checked",true);
			        	}
						$('#modal-update-report-meeting-notes').modal();
			        },
			        error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		})

		
		$('.reportAProblemTable').removeAttr('width').DataTable({
			"language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'problemId',
		  'ajax': {
			  'url':'<?php echo base_url("MeetingNotes/reportProblemMeetingNotes_pagination") ?>',
			  "type": "POST",
				"data":function(data) {
					data.filterType = $("#filterType").val();
					data.isActive = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.userName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.type = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.status = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  },
		  'columns': [
			 { data: 'userName' },
			 { data: 'date' },
			 { data: 'type' },
			 { data: 'description' },
			 { data: 'cause' },
			 { data: 'improvement' },
			 { data: 'status' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    }, 
			    {
			      targets: 6,
			      className: 'last-column',
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });
		
		$('.reportAProblemTableModel').removeAttr('width').DataTable({
		  "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  'ajax': {
			  'url':'<?php echo base_url("MeetingNotes/reportProblemMeetingNotes_pagination") ?>',
			  "type": "POST",
				"data":function(data) {
					data.filterType = $("#filterType").val();
					data.isActive = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.userName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.type = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.status = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  },
		  'columns': [
			{ data: 'userName' },
			{ data: 'date' },
			{ data: 'type' },
			{ data: 'description' },
			{ data: 'cause' },
			{ data: 'improvement' },
			{ data: 'status' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    }, 
			    {
			      targets: 6,
			      className: 'last-column',
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
			},
	   	});
	}); 
	
	function filterStatus()
	{
		var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterStatus.length > 0) 
		{
			$("#filterStatusSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterStatusSelected").css('color','#b8b0b0');
		}
		$("#reportAProblemTable").DataTable().ajax.reload();
	}

	function filterUserName()
	{
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterUserName.length > 0) 
		{
			$("#filterUserNameSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#reportAProblemTable").DataTable().ajax.reload();
	}

	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
			$("#filterTypeSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterTypeSelected").css('color','#b8b0b0');
		}
		$("#reportAProblemTable").DataTable().ajax.reload();
	}

	function filterResolved()
	{
		var filterResolved = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterResolved.length > 0) 
		{
			$("#filterResolvedSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterResolvedSelected").css('color','#b8b0b0');
		}
		$("#reportAProblemTable").DataTable().ajax.reload();
	}
</script>