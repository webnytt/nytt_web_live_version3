 <script>
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function filtersversionName(app_version_id, app_version_name)
	{
		var filtersversionName = $('.filtersversionName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filtersversionName.length > 0) 
		{
			$("#filterappVersionsSelected").css('color','#FF8000');
			if (in_array(app_version_id,filtersversionName) != -1) 
			{
				html = "<button id='removeAppversiontButton"+app_version_id+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>App version name - "+app_version_name+" &nbsp;&nbsp;<i onclick='uncheckAppversionName("+app_version_id+")' class='fa fa-times'></i></button>";
				$("#showAppVersionName").append(html);
			}
			else
			{
				$("#removeAppversiontButton"+app_version_id).remove();
			}
		}
		else
		{
			$("#showAppVersionName").html("");
			$("#filterappVersionsSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckAppversionName(app_version_id)
	{
		$("#removeAppversiontButton"+app_version_id).remove();
		$("#filtersversionName"+app_version_id).prop('checked',false);
		var filtersversionName = $('.filtersversionName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filtersversionName.length > 0) 
		{
		}
		else
		{
			$("#filterappVersionsSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	

	function updateAppVersion(app_version_id)
    {
    	$("#modal-add-appVersion").val(app_version_id);
    	$("#modal-update-appVersion").modal();
    }
	
	
	$('#modal-add-task').on('hidden.bs.modal', function () 
	{
	  $("#modal-add-appVersion").show();
	})

	$('#modal-add-task').on('shown.bs.modal', function ()
	{
	  $("#addTaskButton").hide();
	})

	$('#modal-delete-task').on('hidden.bs.modal', function () 
	{
	  $("#isDelete").val("0");
	})

    function closeDetails()
    {
    	app_version_id = $("#app_version_id").val();
    	$("#"+app_version_id).css("background", "white");
		$("#"+app_version_id).css("border-left", "none");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    $('form#add_appVersion_form').submit(function(e) 
	 { 
        var form = $(this);
        e.preventDefault();
        $("#add_appVersion_submit").html('Saving...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Task_testing/add_appVersion'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                		{
                			alert(data);
                	       $.gritter.add({
                			title: 'Success',
                			text: "New version for app added successfully."
                		})
                        form.each(function()
                        {
                            this.reset();
                        }); 
                        $("#modal-add-appVersion").modal('hide').fadeOut(1500); 
                        $("#add_appVersion_submit").html('Save')
                        window.setTimeout(function(){location.reload()},3000)
                },
                error: function() 
                { 
                    alert("Error while adding the app version.");
                    location.reload();
                }
           });
    });

    $('form#update_appVersion_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#update_appVersion_submit").html('Saving...')
        var formData = new FormData(this);
        $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Task_testing/update_appVersion'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    $.gritter.add({
            			title: 'Success',
            			text: "New version file updated successfully."
            		})
                    form.each(function(){
                        this.reset();
                    }); 
                    $("#update_appVersion_submit").html('Save')
                    window.setTimeout(function(){location.reload()},3000)
                },
                error: function() { 
                    alert("Error while adding Version file.");
                    location.reload();
                }
           });
    });

    
	$(document).ready(function() 
	{
		
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		// function(start, end, label) {
		// 	$('#advance-daterange').css("color","#FF8000");
		// });

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Uploaded Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});


		

		$('table').on( 'click', 'tr', function () 
		{
		    app_version_id = $(this).attr('id');
		  
		    if(app_version_id != "" && app_version_id != undefined)
		    {
				$.ajax({
			        url: "<?php echo base_url('Task_testing/getappVersionById') ?>",
			        type: "post",
			        data: {app_version_id:app_version_id} ,
			        success: function (response) 
			        {
		        		var obj = $.parseJSON(response);

						app_version_id 		= obj.app_version_id;
						app_version_name 	= obj.app_version_name;
			        	app_version_file 	= obj.app_version_file;
			        	app_version_code 	= obj.app_version_code;
			        	created_date 		= obj.created_date;
			        	
			        	$("#app_version_id").html(app_version_id); 
			        	$("#app_version_id").html(app_version_id);
			        	$("#app_version_id").val(app_version_id);
			        	$("#app_version_id_hidden").val(app_version_id);
			        	$("#app_version_name").val(app_version_name);
			        	$("#app_version_file").val(app_version_file);
			        	$("#app_version_code").val(app_version_code);
			        	$("#created_date").val(created_date);
			        	
			        	$('tr').css("border-left", "none");
					 	$('tr').css("background", "#FFFFFF");
					 	$("#"+app_version_id).css("background", "#F2F2F2");
					 	$("#"+app_version_id).css("border-left", "7px solid #FF8000");

					 	$("#empTable").css("width", "100%");
					 	$(".table_data").removeClass("col-md-12");
					 	$(".table_data").addClass("col-md-9");
					    $(".detail").show();
					},

			        error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		})



		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 10,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'app_version_id',
		  'ajax': {
			  'url':'app_versions_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.app_version_name = $('.filtersversionName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
						 { data: 'app_version_id' },
						 { data: 'app_version_file' },
						 { data: 'app_version_name' },
						 { data: 'app_version_code' },
						 { data: 'created_date' }
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$(".col-sm-5").html('<a id="modaladdappVersion" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-appVersion" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;ADD APP VERSION</a>');

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

	}); 

 	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE =  date("Y-m-d");
		$dateValS =  date("Y-m-d", strtotime("-3000 day"));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}

  </script>


