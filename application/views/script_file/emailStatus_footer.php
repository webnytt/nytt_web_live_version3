<script>

	function filterStatus()
    {
        var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterStatus.length > 0) 
        {
            $("#filterStatusSelected").css('color','#FF8000');

            var html = "";
            if (in_array("'0'",filterStatus) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterStatusProcessed' class='btn btn-sm btn-primary'><?php echo Status; ?> - <?php echo Processed; ?> &nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
            }
            
            if (in_array("'1'",filterStatus) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterStatusDelivered' class='btn btn-sm btn-primary'><?php echo Status; ?> - <?php echo Delivered; ?> &nbsp;&nbsp;<i onclick='uncheckStatus(2)' class='fa fa-times'></i></button>";
            }

            if (in_array("'2'",filterStatus) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonfilterStatusDropped' class='btn btn-sm btn-primary'><?php echo Status; ?> - <?php echo Dropped; ?> &nbsp;&nbsp;<i onclick='uncheckStatus(3)' class='fa fa-times'></i></button>";
            }

           	$("#showRepeat").html(html);
        }
        else
        {
            $("#filterStatusSelected").css('color','#b8b0b0');
            $("#showRepeat").html("");
        }

        $("#empTable").DataTable().ajax.reload();
    }



    function uncheckStatus(id)
    {
        if (id == "1") 
        {
            $("#filterStatusProcessed").prop('checked',false);
            $("#buttonFilterStatusProcessed").remove();
        }
        else if (id == "2") 
        {
            $("#filterStatusDelivered").prop('checked',false);
            $("#buttonFilterStatusDelivered").remove();
        }
        else if (id == "3") 
        {
            $("#filterStatusDropped").prop('checked',false);
            $("#buttonfilterStatusDropped").remove();
        }
       

        var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterStatus.length > 0) 
        {

        }
        else
        {
            $("#filterStatusSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }
	

	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Sentdate; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});
		 
		$('#empTable').removeAttr('width').DataTable({
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'emailStatusId',
		  'ajax': {
			  'url':'EmailStatus_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					// data.isActive = $('.filterStatus:checkbox:checked').map(function(){
     //                                  return $(this).val();
     //                                }).get();

     				data.isActive = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();

				},  
		  },
		  'columns': [
			{ data: 'message' },
			{ data: 'isActive' },
			{ data: 'resone' },
			{ data: 'insertTime' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			  	{
			      targets: 2,
			      orderable: false,
			    },
			    {
			    	targets: 3,
			    	orderable: false,
			    }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   	});


		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		
	}); 

	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 3000 days'));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Sent date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$("#empTable").DataTable().ajax.reload();
 	}

 	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterType.length > 0) 
		{
			$("#filterTypeSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'1'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='accident1Button' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Accident; ?>  &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}

			if (in_array("'0'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='incident1Button' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Incident; ?>  &nbsp;&nbsp;<i onclick='uncheckType(0)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'2'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='suggestion1Button' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Suggestion; ?>  &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}

			$("#showType").html(html);
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
			$("#showType").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			$("#accident1").prop('checked',false);
			$("#accident1Button").remove();
		}
		
		else if(id == "0") 
		{
			$("#incident1").prop('checked',false);
			$("#incident1Button").remove();
		}

		else if(id == "2") 
		{
			$("#suggestion1").prop('checked',false);
			$("#suggestion1Button").remove();
		}

		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
		
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
</script>