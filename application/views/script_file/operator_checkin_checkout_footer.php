 <script>
    function closeDetails()
    {
    	activeId = $("#activeId").val();
    	$("#"+activeId).css("background", "white");
		$("#"+activeId).css("border-left", "none");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    $(document).ready(function() 
    {
    	<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		
		$('#checkin-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 
		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showCheckedIn").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Checkedin; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckInDate()' class='fa fa-times'></i></button>");

			$('#checkin-daterange').css("color","#FF8000");
		});

		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
	
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#checkout-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 
		
		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showCheckOut").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Checkedout; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckOutDate()' class='fa fa-times'></i></button>");
		
			$('#checkout-daterange').css("color","#FF8000");
		});

		$('table').on( 'click', 'tr', function () 
		{

		   activeId = $(this).attr('id');
		   if(activeId != "" && activeId != undefined)
		   {
		   	
			 $.ajax({
			        url: "<?php echo base_url('Operator/getOperatorById') ?>",
			        type: "post",
			        data: {activeId:activeId} ,
			        success: function (response) 
			        {
			        	var obj = $.parseJSON(response);
			        	console.log(obj);
			        	userId = obj.userId;
			        	userName = obj.userName;
			        	startDate = obj.startDate;
			        	startTime = obj.startTime;
			        	endDate = obj.endDate;
			        	endTime = obj.endTime;
			        	reportCount = obj.reportCount;
			        	workingMachineName = obj.workingMachineName;
			        	userImage = "<?php echo base_url('assets/img/user/') ?>"+obj.userImage;
			        	
			        	$("#activeId").html(activeId);
			        	$("#activeId").val(activeId);
			        	$("#userId").text('<?php echo UserId; ?>:-' + userId);
			        	$("#userName").text(userName);
			        	$("#startDate").text(startDate);
			        	$("#startTime").text(startTime);
			        	$("#endDate").text(endDate);
			        	$("#endTime").text(endTime);
			        	$("#reportCount").text(reportCount);
			        	$("#workingMachineName").html(workingMachineName);
			        	$("#userImage").attr("src",userImage);
			        	$('tr').css("border-left", "none");
					 	$('tr').css("background", "#FFFFFF");
						$("#"+activeId).css("background", "#F2F2F2");
					 	$("#"+activeId).css("border-left", "7px solid #FF8000");
					 	$("#empTable").css("width", "100%");
					 	$(".col-sm-7").css("padding-left", "28%");
					 	$(".table_data").removeClass("col-md-12");
					 	$(".table_data").addClass("col-md-9");
					    $(".detail").show();
			        },
			        error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown,jqXHR);
			        }
			    });
		   	}	
		})

		$('#empTable').removeAttr('width').DataTable({
		  "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "dom": '<"top"i>rt<"bottom"flp><"clear">',
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'activeId',
		  'ajax': {
			  'url':'operator_checkin_checkout_pagination',

			  "type": "POST",
				"data":function(data) {
					data.userName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.machineIds = $('.filterMachineName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.startDateValS = $('#startDateValS').val();
					data.startDateValE = $('#startDateValE').val();
					data.endDateValS = $('#endDateValS').val();
					data.endDateValE = $('#endDateValE').val();
					data.userRole = <?php echo $userRole; ?>;
				},  
		  	},
			'columns': [
				{ data: 'userName' },
				{ data: 'workingMachineName' },
				{ data: 'startTime' },
				{ data: 'endTime' },
				{ data: 'reportCount' },
			],
		  	"columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    }
			   ], 
			fixedColumns: true, 
			drawCallback: function( settings ) 
			{ 
				$(".paginate_page").html('<?php echo Page; ?>');
				var paginate_of = $(".paginate_of").text();
				var paginate_of = paginate_of.split(" ");
				$(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
			},
	   });

		$('#checkin-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#startDateValS').val(dateValS);
			$('#startDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#checkout-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#endDateValS').val(dateValS);
			$('#endDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
	}); 

	function uncheckInDate()
 	{
 		$("#showCheckedIn").html("");
 		$('#checkin-daterange').css("color","#b8b0b0");

 		<?php 
			$startDateValE = date("F d, Y", strtotime('today'));
			$startDateValS =  date("F d, Y", strtotime('today - 29 days'));

			$dateValESET = date("Y-m-d", strtotime('today'));
			$dateValSSET =  date("Y-m-d", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $startDateValE; ?>"
		var DdateValS = "<?php echo $startDateValS; ?>"

		$('#startDateValS').val("<?php echo $dateValSSET; ?>");
		$('#startDateValE').val("<?php echo $dateValESET; ?>");

		$('#checkin-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			 locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 

		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			
			$("#showCheckedIn").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Checkedin; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckInDate()' class='fa fa-times'></i></button>");
			$('#checkin-daterange').css("color","#FF8000");
		});

		$('#checkin-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#startDateValS').val(dateValS);
			$('#startDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}


	function uncheckOutDate()
 	{
 		$("#showCheckOut").html("");
 		$('#checkout-daterange').css("color","#b8b0b0");

 		<?php 
			$endDateValE = date("F d, Y", strtotime('today'));
			$endDateValS =  date("F d, Y", strtotime('today - 29 days'));
			$dateValESET = date("Y-m-d", strtotime('today'));
			$dateValSSET =  date("Y-m-d", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $endDateValE; ?>"
		var DdateValS = "<?php echo $endDateValS; ?>"

		$('#endDateValS').val("<?php echo $dateValSSET; ?>");
		$('#endDateValE').val("<?php echo $dateValESET; ?>");

		$('#checkout-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			 locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 

		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			
			$("#showCheckOut").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Checkedout; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckOutDate()' class='fa fa-times'></i></button>");
			$('#checkout-daterange').css("color","#FF8000");
		});

		$('#checkout-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#endDateValS').val(dateValS);
			$('#endDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$("#empTable").DataTable().ajax.reload();
 	}

		
	function filterUserName(userId, userName)
	{
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
			// alert("hello");exit;
			$("#filterUserNameSelected").css('color','#FF8000');
			if (in_array(userId,filterUserName) != -1) 
			{
				html = "<button id='removeUserButton"+userId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Operator; ?> - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
				$("#showUserFilter").append(html);
			}
			else
			{
				$("#removeUserButton"+userId).remove();
			}
		}
		else
		{
			$("#showUserFilter").html("");
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckUserName(userId)
	{
		$("#removeUserButton"+userId).remove();
		$("#filterUserName"+userId).prop('checked',false);
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
		}
		else
		{
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
	
	function filterMachineName(machineId, machineName)
	{
		var filterMachineName = $('.filterMachineName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterMachineName.length > 0) 
		{
			$("#filterMachineNameSelected").css('color','#FF8000');

			if (in_array(machineId,filterMachineName) != -1) 
			{
				html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Machine; ?> - "+machineName+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

				$("#showMachinefilter").append(html);
			}
			else
			{
				$("#removeMachineButton"+machineId).remove();
			}
		}
		else
		{
			$("#showMachinefilter").html("");
			$("#filterMachineNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckMachineName(machineId)
	{
		$("#removeMachineButton"+machineId).remove();

		$("#filterMachineName"+machineId).prop('checked',false);
		var filterMachineName = $('.filterMachineName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterMachineName.length > 0) 
		{
		}
		else
		{
			$("#filterMachineNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
	
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}
</script>