 <script>
	function filterstackLightTypeName(stackLightTypeId, stackLightTypeName)
	{
		var filterstackLightTypeName = $('.filterstackLightTypeName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterstackLightTypeName.length > 0) 
		{
			$("#filterstackLightTypeNameSelected").css('color','#FF8000');
			if (in_array(stackLightTypeId,filterstackLightTypeName) != -1) 
			{
				html = "<button id='removeStacklightButton"+stackLightTypeId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>stacklighttype - "+stackLightTypeName+" &nbsp;&nbsp;<i onclick='uncheckStacklightnameName("+stackLightTypeId+")' class='fa fa-times'></i></button>";
				$("#showStacklightFilterNamefilter").append(html);
			}
			else
			{
				$("#removeStacklightButton"+stackLightTypeId).remove();
			}
		}
		else
		{
			$("#showStacklightFilterNamefilter").html("");
			$("#filterstackLightTypeNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckStacklightnameName(stackLightTypeId)
	{
		$("#removeStacklightButton"+stackLightTypeId).remove();
		$("#filterstackLightTypeName"+stackLightTypeId).prop('checked',false);
		var filterstackLightTypeName = $('.filterstackLightTypeName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterstackLightTypeName.length > 0) 
		{
		}
		else
		{
			$("#filterstackLightTypeNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterversionName()
	{
		var filterversionName = $('.filterversionName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterversionName.length > 0) 
		{
			$("#filterversionNameSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterversionNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}


	$('#carousel-example').on('slide.bs.carousel', function (e) 
	{
		var $e = $(e.relatedTarget);
		    var idx = $e.index();
		    var itemsPerSlide = 5;
		    var totalItems = $('.carousel-item').length;
		    if (idx >= totalItems-(itemsPerSlide-1)) {
		        var it = itemsPerSlide - (totalItems - idx);
		        for (var i=0; i<it; i++) {
		            // append slides to end
		            if (e.direction=="left") 
		            {
		                $('.carousel-item').eq(i).appendTo('.carousel-inner');
		            }
		            else 
		            {
		                $('.carousel-item').eq(0).appendTo('.carousel-inner');
		            }
		        }
		    }
	});
	$('#modal-add-task').on('hidden.bs.modal', function () 
	{
	  $("#addTaskButton").show();
	})

	$('#modal-add-task').on('shown.bs.modal', function ()
	{
	  $("#addTaskButton").hide();
	})

	$('#modal-delete-task').on('hidden.bs.modal', function () 
	{
	  $("#isDelete").val("0");
	})

    function closeDetails()
    {
    	versionId = $("#versionId").val();
    	$("#"+versionId).css("background", "white");
		$("#"+versionId).css("border-left", "none");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    function delete_task(task_id)
    {
    	$("#isDelete").val("1");
    	$("#deleteTaskId").val(task_id);
    	$("#modal-delete-task").modal();
    }
     function plusMinusDate(type)
    {
    	dueDate = $("#dueDate").val();
    	$.ajax({
	        url: "<?php echo base_url("Tasks/get_due_date") ?>",
	        type: "post",
	        data: {type:type,dueDate:dueDate} ,
	        success: function (response) 
	        {
				$("#dueDate").val($.trim(response));	
	        },
	        error: function(jqXHR, textStatus, errorThrown) {
	           console.log(textStatus, errorThrown);
	        }
	    });
	}

   	$(document).ready(function() 
	{
		
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});


		$('table').on( 'click', 'tr', function () 
		{
		    versionId = $(this).attr('id');
		  	if(versionId != "" && versionId != undefined)
		    {
				$.ajax({
			        url: "<?php echo base_url('Task_testing/getconfigureVersionById') ?>",
			        type: "post",
			        data: {versionId:versionId} ,
			        success: function (response) 
			        {
		        		var obj = $.parseJSON(response);
						versionId = obj.versionId;
						stackLightTypeName = obj.stackLightTypeName;
			        	versionName = obj.versionName;
			        	fileName = obj.fileName;
			        	versionDate = obj.versionDate;
			        	stackLightTypeImage = "<?php echo base_url('assets/img/stackLightType/') ?>"+obj.stackLightTypeImage;
			        	
			        	$("#versionId").val(versionId);
			        	$("#stackLightTypeName").val(stackLightTypeName);
			        	$("#versionName").val(versionName);
			        	$("#fileName").text(fileName);
			        	$("#versionDate").text(versionDate);
			        	$("#stackLightTypeImage").attr("src",stackLightTypeImage);
						
						$('tr').css("border-left", "none");
					 	$('tr').css("background", "#FFFFFF");
					 	$("#"+versionId).css("background", "#F2F2F2");
					 	$("#"+versionId).css("border-left", "7px solid #FF8000");

					 	$("#empTable").css("width", "100%");
					 	$(".table_data").removeClass("col-md-12");
					 	$(".table_data").addClass("col-md-9");
					    $(".detail").show();
					},
					error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		})

		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 10,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'versionId',
		  'ajax': {
			  'url':'configure_version_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.stackLightTypeName = $('.filterstackLightTypeName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.versionName = $('.filterversionName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
			{ data: 'versionId' },
			{ data: 'stackLightTypeName' },
			{ data: 'versionName' },
			{ data: 'fileName' },
			{ data: 'versionDate' },
		],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$(".col-sm-5").html('<a id="addTaskButton" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-stackLightType" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;ADD CONFIGURE VERSION</a>');

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

	
        $('form#add_stackLightType_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#add_stackLightType_submit").html('Saving...')
            var formData = new FormData(this);
            var classification_cfg = $("#classification_cfg").val();
            var classification_cfg_extension = classification_cfg.replace(/^.*\./, '');
            var classification_weights = $("#classification_weights").val();
            var classification_weights_extension = classification_weights.replace(/^.*\./, '');
            var detection_cfg = $("#detection_cfg").val();
            var detection_cfg_extension = detection_cfg.replace(/^.*\./, '');
            var detection_weights = $("#detection_weights").val();
            var detection_weights_extension = detection_weights.replace(/^.*\./, '');
            var label_txt = $("#label_txt").val();
            var label_txt_extension = label_txt.replace(/^.*\./, '');
            var labels_txt = $("#labels_txt").val();
            var labels_txt_extension = labels_txt.replace(/^.*\./, '');
            if (classification_cfg_extension != "cfg") 
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'cfg'"
        		});
            }
            else if(classification_weights_extension != "weights")
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'weights'"
        		});
            }
            else if(detection_cfg_extension != "cfg")
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'cfg'"
        		});
            }
            else if(detection_weights_extension != "weights")
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'weights'"
        		});
            }
            else if(label_txt_extension != "txt")
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'txt'"
        		});
            }
            else if(labels_txt_extension != "txt")
            {
            	$("#add_stackLightType_submit").html('Save')
            	$.gritter.add({
        			title: 'Error',
        			text: "Please select a valid file with extension 'txt'"
        		});
            }
            else
            {
	            $.ajax({ 
	                type: 'POST',
	                url: "<?php echo site_url('admin/add_stacklighttype'); ?>",
	                data:formData,
	                cache:false,
	                contentType: false,
	                processData: false,
	                dataType: "html",
	                success: function(data){
	                    if($.trim(data) == "New stacklighttype added successfully.") 
                        { 
	                        $("#add_stackLightType_error").html('');
	                        $("#add_stackLightType_error").removeClass("show");
	                        $("#add_stackLightType_error").addClass("hide");
	                        
	                        $("#add_stackLightType_success").addClass("show");
	                        $("#add_stackLightType_success").removeClass("hide");
	                        $("#add_stackLightType_success").html(data);
	                        
	                        form.each(function(){
	                            this.reset();
	                        }); 
	                        $("#add_stackLightType_submit").html('Saved...')
	                        
	                        $("#add_stackLightType_success").fadeOut(1500); 
	                        $("#modal-add-stackLightType").modal('hide').fadeOut(1500); 
	                        
	                        $("#add_stackLightType_submit").html('Save')
	                        window.setTimeout(function(){location.reload()},3000)
	                        
	                    } 
                        else 
                        { 
	                        $("#add_stackLightType_error").addClass("show");
	                        $("#add_stackLightType_error").removeClass("hide"); 
	                        $("#add_stackLightType_error").html(data);
	                        //alert(data); 
	                        $("#add_stackLightType_submit").html('Save')
	                    }
	                    
	                },
	                error: function() { 
	                    alert("Error while adding stack light type.");
	                    location.reload();
	                }
	           });
        	}
        });
	}); 

 	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 3000 days'));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$("#empTable").DataTable().ajax.reload();
 	}


  </script>


       