 <script>
 	function exportCsv()
 	{
 		var IOName = $('.filterIOName:checkbox:checked').map(function(){
					      return $(this).val();
					    }).get();
		var dateValS = $('#dateValS').val();
		var dateValE = $('#dateValE').val();
		var signal = $('.filterSignal:checkbox:checked').map(function(){
					      return $(this).val();
					    }).get();
        var endDateValS = $('#endDateValS').val();
        var endDateValE = $('#endDateValE').val();
 		location.href = "<?php echo base_url('VirtualMachine/exportCsv?IOName=') ?>"+IOName+"&dateValS="+dateValS+"&dateValE="+dateValE+"&signal="+signal+"&endDateValS="+endDateValS+"&endDateValE="+endDateValE;
 	}

	$(document).ready(function() 
	{
		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			 ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 
		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'> <?php echo Addeddate; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");

			$('#advance-daterange').css("color","#FF8000");
		});

        <?php 
            $dateValE = date("F d, Y", strtotime('today'));
            $dateValS =  date("F d, Y", strtotime('today - 29 days'));
        ?>
    
        var DdateValE = "<?php echo $dateValE; ?>"
        var DdateValS = "<?php echo $dateValS; ?>"

        $('#checkout-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 
        
        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }

          //  $("#showCheckOut").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Original time - "+label+" &nbsp;&nbsp;<i onclick='uncheckOutDate()' class='fa fa-times'></i></button>");
            $('#checkout-daterange span').html(label);
           // $('#checkout-daterange').css("color","#FF8000");
        });


		
		$('#empTable').removeAttr('width').DataTable({
           "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'taskId',
		  'ajax': {
			  'url':'virtual_machine_log_pagination',
			  "type": "POST",
				"data":function(data) {
					data.IOName = $('.filterIOName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
                    data.endDateValS = $('#endDateValS').val();
                    data.endDateValE = $('#endDateValE').val();
					data.signal = $('.filterSignal:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
			 { data: 'logId' },
			 { data: 'IOName' },
			 { data: 'addedTime' },
			 { data: 'signalType' },
             { data: 'originalTime' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
                {
                  targets: 4,
                  orderable: false,
                }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) 
			{ 
				
			},
	   });

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

        $('#checkout-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#endDateValS').val(dateValS);
            $('#endDateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });


        <?php if (!empty($_POST['IOName'])) { ?>
            filterIOName(<?php echo $_POST['virtualMachineId']; ?>,'<?php echo $_POST['IOName']; ?>');
        <?php } ?>
	}); 


	function in_array(needle, haystack)
    {
        var found = 0;
        for (var i=0, len=haystack.length;i<len;i++) 
        {
            if (haystack[i] == needle) return i;
                found++;
        }
        return -1;
    }

	function filterIOName(virtualMachineId, IOName)
    {
        console.log(IOName);
        var filterIOName = $('.filterIOName:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterIOName.length > 0) 
        {
            $("#filterIONameSelected").css('color','#FF8000');

            // $("#showIOname").html("<p>hello</p>");
            var IONameName = "'"+IOName+"'";
            if (in_array(IONameName, filterIOName) != -1) 
            {
                 if (IOName == "GPIO15") {
                  var IOName = "Parts produced";
                }else if (IOName == "GPIO14") {
                  var IOName = "Robot status";
                }
                html = "<button id='removeIONAME"+virtualMachineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo IOName; ?> - "+IOName+" &nbsp;&nbsp;<i onclick='uncheckIOName("+virtualMachineId+")' class='fa fa-times'></i></button>";

                $("#showIOname").append(html);
            }
            else
            {
                $("#removeIONAME"+virtualMachineId).remove();
            }
        }
        else
        {
            $("#showIOname").html("");
            $("#filterIONameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckIOName(virtualMachineId)
    {
        $("#removeIONAME"+virtualMachineId).remove();
        
        $("#filterIOName"+virtualMachineId).prop('checked',false);
        var filterIOName = $('.filterIOName:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterIOName.length > 0) 
        {

        }
        else
        {
            $("#filterIONameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }



	function filterSignal()
    {
        var filterSignal = $('.filterSignal:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterSignal.length > 0) 
        {
            $("#filterSignalSelected").css('color','#FF8000');

            var html = "";
            if (in_array("'1'",filterSignal) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterActive' class='btn btn-sm btn-primary'><?php echo Signal; ?> - Active &nbsp;&nbsp;<i onclick='uncheckSignal(1)' class='fa fa-times'></i></button>";
            }
            
            if (in_array("'0'",filterSignal) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterNotActive' class='btn btn-sm btn-primary'><?php echo Signal; ?> - Not Active &nbsp;&nbsp;<i onclick='uncheckSignal(2)' class='fa fa-times'></i></button>";
            }

          	$("#showSignal").html(html);
        }
        else
        {
            $("#filterSignalSelected").css('color','#b8b0b0');
            $("#showSignal").html("");
        }

        $("#empTable").DataTable().ajax.reload();
    }



    function uncheckSignal(id)
    {
        if (id == "1") 
        {
            $("#filterSignalHigh").prop('checked',false);
            $("#buttonFilterActive").remove();
        }
        else if (id == "2") 
        {
            $("#filterSignalEveryLow").prop('checked',false);
            $("#buttonFilterNotActive").remove();
        }
       
        var filterSignal = $('.filterSignal:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterSignal.length > 0) 
        {

        }
        else
        {
            $("#filterSignalSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }



	function uncheckCreatedDate()
    {
        $("#showCreatedDate").html("");
        $('#advance-daterange').css("color","#b8b0b0");

        <?php 
            $dateValE = date("F d, Y", strtotime('today'));
            $dateValS =  date("F d, Y", strtotime('today - 29 days'));
            $dateValESET = date("Y-m-d", strtotime('today'));
            $dateValSSET =  date("Y-m-d", strtotime('today - 29 days'));
        ?>
        var DdateValE = "<?php echo $dateValE; ?>"
        var DdateValS = "<?php echo $dateValS; ?>"

        $('#dateValS').val("<?php echo $dateValSSET; ?>");
        $('#dateValE').val("<?php echo $dateValESET; ?>");
        $('#advance-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

                var label = label+"("+startDate+" - "+endDate+")";
            }
            $("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo AddedDate; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
            $('#advance-daterange').css("color","#FF8000");
        });

        $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#dateValS').val(dateValS);
            $('#dateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckOutDate()
    {
        $("#showCheckOut").html("");
        $('#checkout-daterange').css("color","#b8b0b0");

        <?php 
            $endDateValE = date("F d, Y", strtotime('today'));
            $endDateValS =  date("F d, Y", strtotime('today - 29 days'));
            $dateValSSET =  date("Y-m-d", strtotime('today - 29 days'));
            $dateValESET = date("Y-m-d", strtotime('today'));
        ?>
        var DdateValE = "<?php echo $endDateValE; ?>"
        var DdateValS = "<?php echo $endDateValS; ?>"


 //       $('#checkout-daterange span').html(DdateValS.format('MMMM D, YYYY') + ' - ' + DdateValE.format('MMMM D, YYYY'));
        $('#endDateValS').val("<?php echo $dateValSSET; ?>");
        $('#endDateValE').val("<?php echo $dateValESET; ?>");

        $('#checkout-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

                var label = label+"("+startDate+" - "+endDate+")";
            }
            
            $("#showCheckOut").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Original time - "+label+" &nbsp;&nbsp;<i onclick='uncheckOutDate()' class='fa fa-times'></i></button>");
            $('#checkout-daterange span').html(label);
           // $('#checkout-daterange').css("color","#FF8000");
        });

        $('#checkout-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#endDateValS').val(dateValS);
            $('#endDateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $("#empTable").DataTable().ajax.reload();
    }

</script>

