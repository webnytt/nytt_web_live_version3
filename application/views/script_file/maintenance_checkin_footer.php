 <script>
	$(document).ready(function() { 
		<?php 
			if($_POST && isset($_POST['dateValS'])) { 
				$dateValE = date("F d, Y", strtotime($_POST['dateValE']));
				$dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
			}
			elseif ($_GET && isset($_GET['dateValS']))
			{
				$dateValE = date("F d, Y", strtotime($_GET['dateValE']));
				$dateValS = date("F d, Y", strtotime($_GET['dateValS'])); 
			}
			else 
			{ 
				$dateValE = date("F d, Y", strtotime('today'));
				$dateValS =  date("F d, Y", strtotime('today - 29 days'));
			} 
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		
		$('#advance-daterange span').html(moment(DdateValS).format('MMMM D, YYYY') + ' - ' + moment(DdateValE).format('MMMM D, YYYY'));
		
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			$('#advance-daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		});
		
		$('#empTable').removeAttr('width').DataTable({ 
		  "pagingType": "input", 
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 50,
		  "order": [
			  [0, "desc" ]
			],  
		  'ajax': {
			  'url':'maintenance_checkin_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.userId = $('#userId').val(); 
				}, 
		  },
		  'columns': [
			 { data: 'activeId' },
			 { data: 'userName' },
			 { data: 'machineNames' }, 
			 { data: 'isActive' },
			 { data: 'startTime' },
			 { data: 'endTime' },
			 { data: 'status' },
			 { data: 'type' },
		  ], 
		  "columnDefs": [
				{
					"targets": [ 6 ],
					"searchable": false,
					"orderable": false,
				},
				{
					"targets": [ 7 ],
					"searchable": false,
					"orderable": false,
				},
			],  
			fixedColumns: true, 
			drawCallback: function( settings ) { 
			},
	   }); 
	   
	   $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
	   { 
		  	var userId = $('#userId').val();
			var dateValS = picker.startDate.format('YYYY-MM-DD');  
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		
		$("#filter a").click(function() 
		{
			var clickedId = $(this).attr('id');
            var userId = clickedId.replace('user',''); 
			$('#userId').val(userId);
			$("#filter a").removeClass('active');
			$(this).addClass('active');  
			$('#dateValS').val();
			$('#dateValE').val();
			$("#empTable").DataTable().ajax.reload();
		});
	}); 
    </script>