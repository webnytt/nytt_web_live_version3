	<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
	<script>
		
		myFunction();
		function myFunction() 
		{
		
		}
		analytics_type('wait_analytics');
		
		function analytics_type(type)
		{
			if (type == "wait_analytics") 
			{
				$(".stop_analytics").hide();
				$(".wait_analytics").show();
				$("#choose1").attr("disabled",false);
			}
			else
			{
				$(".wait_analytics").hide();
				$(".stop_analytics").show();
				$('#choose1 option[value=day]').attr('selected',false);
				$('#choose1 option[value=day]').attr('selected','selected');
				$("#choose1").attr("disabled",true);
				changeFilterType('day');
			}
		}
		
		machineId = $('#machineId').val();
		reloadGraph(machineId);

		function changeFilterType(value)
		{
			if (value == "day") 
			{
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").hide();
				$(".day").show();
				$("#headerChange").text("Hourly machine analysis");
			}
			else if (value == "weekly") 
			{
				$(".day").hide();	
				$(".monthly").hide();
				$(".yearly").hide();
				$(".weekly").show();
				$("#headerChange").text("Daily machine analysis");
			}
			else if (value == "monthly") 
			{
				$(".day").hide();	
				$(".yearly").hide();
				$(".weekly").hide();
				$(".monthly").show();
				$("#headerChange").text("Weekly machine analysis");
			}
			else if (value == "yearly") 
			{
				$(".day").hide();	
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").show();
				$("#headerChange").text("Monthly machine analysis");
			}
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		function changeFilterValue()
		{
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		$(".filter").click(function() {
			var clickedId = $(this).attr('id');
			var machineId = clickedId.replace('machine',''); 
			$('#machineId').val(machineId);
			$(".filter").removeClass('active');
			$(this).addClass('active');
			reloadGraph(machineId);
		});

		function reloadGraph(machineId) 
		{
			$('.graph-loader').addClass("show").removeClass("hide");
			var choose1 = $("#choose1").val(); 
			if (choose1 == "day") 
			{
				var choose2 = $("#choose2").val(); 
			} 
			else if (choose1 == "weekly") 
			{
				var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
			}
			else if (choose1 == "monthly") 
			{
				var choose2 = $("#choose4").val(); 
			}
			else if (choose1 == "yearly") 
			{
				var choose2 = $("#choose5").val(); 
			}

			$.post('<?php echo base_url();?>admin/dashboard3_pagination',{machineId:machineId,choose1:choose1,choose2:choose2}, function getattribute(returnData)
			{
				$('.graph-loader').addClass("hide").removeClass("show");
				returnData = jQuery.parseJSON(returnData);

				if (choose1 == "day") 
				{

					var screenWithCount = window.innerWidth;
					  
					if (screenWithCount > 1200 && screenWithCount < 1850) 
					{
					 	var paddingText = [0, -40, 0, 0];
					}
					else
					{
					  	var paddingText = [0, -60, 0, 0];
					}
				}
				else
				{
					var paddingText = [0, 0, 0, 0];
				}

				var DailyGraph3 = echarts.init(document.getElementById('DailyGraph3'));  
				DailyGraph3.setOption(

				option = {
				    tooltip: {
				        trigger: 'item',
				        formatter: '{b} <br/> {c} '+returnData.yAxisName+' ({d}%)'
				    },
				    series: [
				        {
				            name: 'Per',
				            type: 'pie',
				            radius: ['40%', '55%'],
				            label: {
				                formatter: '{per|{d}%}',
				                backgroundColor: '#eee',
				                borderColor: '#aaa',
				                borderWidth: 1,
				                borderRadius: 4,
				                rich: {
				                    a: {
				                        color: '#999',
				                        lineHeight: 22,
				                        align: 'center'
				                    },
				                    hr: {
				                        borderColor: '#aaa',
				                        width: '100%',
				                        borderWidth: 0.5,
				                        height: 0
				                    },
				                    b: {
				                        fontSize: 16,
				                        lineHeight: 33
				                    },
				                    per: {
				                        color: '#eee',
				                        backgroundColor: '#334455',
				                        padding: [2, 4],
				                        borderRadius: 2
				                    }
				                }
				            },
				            data: [
				                {
				                	value: returnData.ActualProduction,
				                	name: 'Production time',
				                	itemStyle: {
										normal: {
											color: "#002060" 
										},
										label: {
											color: '#124D8D' 
										}, 
									}
				                },
				                {
				                	value: returnData.Setup,
				                	name: 'Setup time',
				                	itemStyle: {
										normal: {
											color: "#124D8D" 
										},
										label: {
											color: '#124D8D' 
										}, 
									}
				                },
				                {
				                	value: returnData.NoProduction,
				                	name: 'No production',
				                	itemStyle: {
										normal: {
											color: "#FF8000" 
										},
										label: {
											color: '#124D8D',
										}, 
									}
				                }
				            ],
				             itemStyle: {
								emphasis: {
									shadowBlur: 0,
									shadowOffsetX: 0,
									shadowColor: 'rgba(0, 0, 0, 0.5)'
								}
							},
							hoverOffset: 1,
							
				        }
				    ]
				}

				, true);
				
				if (returnData.ActualProductionStopped != 0 ||  returnData.ActualProductionWaiting != 0 || returnData.ActualProductionRunning != 0 || returnData.ActualProductionOff != 0 || returnData.ActualProductionNodet != 0 || returnData.ActualProductionNoStacklight != 0) 
				{
					$("#blankDailyGraph4").hide();
					$("#dataDailyGraph4").show();

					if (returnData.machineId == 0) 
					{
						var DailyGraph4 = echarts.init(document.getElementById('DailyGraph4'));  
						DailyGraph4.setOption(option = {
						    tooltip: {
						        trigger: 'item',
						        formatter: '{a} {b} <br/> {c} '+returnData.yAxisName+' ({d}%)'
						    },
						    series: [
						    	{
										name: '',
										type: 'pie',
										radius : ['25%', '0%'],
										data: [
											   {
						                	value: returnData.ActualProduction,
						                	name: 'Production time',
						                	itemStyle: {
												normal: {
													color: "#002060" 
												},
												label: {
													color: '#124D8D' 
												}, 
											}
						                }
										], 
										itemStyle: {
											normal : {
												 labelLine : {
													show : true,
													length: returnData.ActualProduction,
													length2: 0, 
													lineStyle: {
														width: 0,
													}
												 },
												 label : {
												 	show:false,
													position:"outside",
												 },
											 },
											emphasis: {
												show: false,
												shadowBlur: 0,
												shadowOffsetX: 0,
												shadowColor: 'rgba(0, 0, 0, 0.5)'
											}
										},
									},
						        {
						            name: '',
						            type: 'pie',
						            radius: ['35%', '55%'],
						            label: {
						                formatter: '{per|{d}%}',
						                backgroundColor: '#eee',
						                borderColor: '#aaa',
						                borderWidth: 1,
						                borderRadius: 4,
						                rich: {
						                    a: {
						                        color: '#999',
						                        lineHeight: 22,
						                        align: 'center'
						                    },
						                    hr: {
						                        borderColor: '#aaa',
						                        width: '100%',
						                        borderWidth: 0.5,
						                        height: 0
						                    },
						                    b: {
						                        fontSize: 16,
						                        lineHeight: 33
						                    },
						                    per: {
						                        color: '#eee',
						                        backgroundColor: '#334455',
						                        padding: [2, 4],
						                        borderRadius: 2
						                    }
						                }
						            },
						            data: [
						            		{
												value:returnData.ActualProductionRunning, 
												name:'Running',
												itemStyle: {
													normal: {
														color: "#76BA1B" 
													},
													label: {
														color: '#76BA1B' 
													},

												}
											},
											{
												value:returnData.ActualProductionWaiting, 
												name:'Waiting',
												itemStyle: {
													normal: {
														color: "#FFCF00" 
													},
													label: {
														color: '#FFCF00' 
													},

												}
											},
											{
												value:returnData.ActualProductionStopped, 
												name:'Stopped',
												itemStyle: {
													normal: {
														color: "#F60100" 
													},
													label: {
														color: '#F60100' 
													},

												}
											},
											{
												value:returnData.ActualProductionOff, 
												name:'Off',
												itemStyle: {
													normal: {
														color: "#CCD2DF" 
													},
													label: {
														color: '#CCD2DF' 
													},

												}
											},
											{
												value:returnData.ActualProductionNodet, 
												name:'Nodet',
												itemStyle: {
													normal: {
														color: "#000000" 
													},
													label: {
														color: '#000000' 
													},

												}
											},
											{
												value:returnData.ActualProductionNoStacklight, 
												name:'No stacklight production time',
												itemStyle: {
													normal: {
														color: "#002060" 
													},
													label: {
														color: '#002060' 
													},

												}
											}
											
										], 
						             itemStyle: {
										emphasis: {
											shadowBlur: 0,
											shadowOffsetX: 0,
											shadowColor: 'rgba(0, 0, 0, 0.5)'
										}
									},
									hoverOffset: 1,
						        }
						    ]
						}, true);
					}
					else if(returnData.noStacklight == "1")
					{
						$("#dataDailyGraph4").hide();
						$("#blankDailyGraph4").show();
					}
					else
					{
						var DailyGraph4 = echarts.init(document.getElementById('DailyGraph4'));  
						DailyGraph4.setOption(option = {
						    tooltip: {
						        trigger: 'item',
						        formatter: '{a} {b} <br/> {c} '+returnData.yAxisName+' ({d}%)'
						    },
						    series: [
						    	{
										name: '',
										type: 'pie',
										radius : ['25%', '0%'],
										data: [
											   {
						                	value: returnData.ActualProduction,
						                	name: 'Production time',
						                	itemStyle: {
												normal: {
													color: "#002060" 
												},
												label: {
													color: '#124D8D' 
												}, 
											}
						                }
										], 
										itemStyle: {
											normal : {
												 labelLine : {
													show : true,
													length: returnData.ActualProduction,
													length2: 0, 
													lineStyle: {
														width: 0,
													}
												 },
												 label : {
												 	show:false,
													position:"outside",
												 },
											 },
											emphasis: {
												show: false,
												shadowBlur: 0,
												shadowOffsetX: 0,
												shadowColor: 'rgba(0, 0, 0, 0.5)'
											}
										},
									},
						        {
						            name: '',
						            type: 'pie',
						            radius: ['35%', '55%'],
						            label: {
						                formatter: '{per|{d}%}',
						                backgroundColor: '#eee',
						                borderColor: '#aaa',
						                borderWidth: 1,
						                borderRadius: 4,
						                rich: {
						                    a: {
						                        color: '#999',
						                        lineHeight: 22,
						                        align: 'center'
						                    },
						                    hr: {
						                        borderColor: '#aaa',
						                        width: '100%',
						                        borderWidth: 0.5,
						                        height: 0
						                    },
						                    b: {
						                        fontSize: 16,
						                        lineHeight: 33
						                    },
						                    per: {
						                        color: '#eee',
						                        backgroundColor: '#334455',
						                        padding: [2, 4],
						                        borderRadius: 2
						                    }
						                }
						            },
						            data: [
						            		{
												value:returnData.ActualProductionRunning, 
												name:'Running',
												itemStyle: {
													normal: {
														color: "#76BA1B" 
													},
													label: {
														color: '#76BA1B' 
													},

												}
											},
											{
												value:returnData.ActualProductionWaiting, 
												name:'Waiting',
												itemStyle: {
													normal: {
														color: "#FFCF00" 
													},
													label: {
														color: '#FFCF00' 
													},

												}
											},
											{
												value:returnData.ActualProductionStopped, 
												name:'Stopped',
												itemStyle: {
													normal: {
														color: "#F60100" 
													},
													label: {
														color: '#F60100' 
													},

												}
											},
											{
												value:returnData.ActualProductionOff, 
												name:'Off',
												itemStyle: {
													normal: {
														color: "#CCD2DF" 
													},
													label: {
														color: '#CCD2DF' 
													},

												}
											},
											{
												value:returnData.ActualProductionNodet, 
												name:'Nodet',
												itemStyle: {
													normal: {
														color: "#000000" 
													},
													label: {
														color: '#000000' 
													},

												}
											}
											
										], 
						             itemStyle: {
										emphasis: {
											shadowBlur: 0,
											shadowOffsetX: 0,
											shadowColor: 'rgba(0, 0, 0, 0.5)'
										}
									},
									hoverOffset: 1,
						        }
						    ]
						}, true);
					}
				}
				else
				{
					$("#dataDailyGraph4").hide();
					$("#blankDailyGraph4").show();
				}
				

				
				var maxValue = returnData.maxValue;
				if (maxValue == 31) 
				{
					maxValue = 32;

				}

				if (returnData.maxValue == 0) 
				{
					maxValue = 3600;
				}

				var intervalValue = maxValue / 8;

				if (returnData.machineId == 0) 
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						},
						grid: {
							left: '1%',
							right: '6%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
            				axisLabel: {
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
						},
						series: [
							{
								name: 'Running',
								type: 'bar',
								stack: 1,
								data: returnData.Running,
								color: '#76BA1B',
							}, 
							{
								name: 'Waiting',
								type: 'bar',
								stack: 1,
								data: returnData.Waiting,
								color: '#FFCF00',
							},
							{
								name: 'Stopped',
								type: 'bar',
								stack: 1,
								data: returnData.Stopped,
								color: '#F60100',
							},
							{
								name: 'Off',
								type: 'bar',
								stack: 1,
								data: returnData.Off,
								color: '#CCD2DF',
							},
							{
								name: 'Nodet',
								type: 'bar',
								stack: 1,
								data: returnData.Nodet,
								color: '#000000',
							},
							{
								name: 'Setup time',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: 'No production',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							},
							{
								name: 'Production time',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducation,
								color: '#002060',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
				}
				else if (returnData.noStacklight == "1") 
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						},
						grid: {
							left: '1%',
							right: '6%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
							axisLabel: {
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
							
						},
						series: [
							{
								name: 'Setup time',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: 'No production',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							},
							{
								name: 'Production time',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducation,
								color: '#002060',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
				}
				else
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						},
						grid: {
							left: '1%',
							right: '6%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
							axisLabel: {
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
							
						},
						series: [
							{
								name: 'Running',
								type: 'bar',
								stack: 1,
								data: returnData.Running,
								color: '#76BA1B',
							}, 
							{
								name: 'Waiting',
								type: 'bar',
								stack: 1,
								data: returnData.Waiting,
								color: '#FFCF00',
							},
							{
								name: 'Stopped',
								type: 'bar',
								stack: 1,
								data: returnData.Stopped,
								color: '#F60100',
							},
							{
								name: 'Off',
								type: 'bar',
								stack: 1,
								data: returnData.Off,
								color: '#CCD2DF',
							},
							{
								name: 'Nodet',
								type: 'bar',
								stack: 1,
								data: returnData.Nodet,
								color: '#000000',
							},
							{
								name: 'Setup time',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: 'No production',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
					}
					
				});

		}

		$(document).ready(function() { 
			$('.datepicker').datepicker({
					format: "mm/dd/yyyy",
					endDate: "today"
				});
			$('.datepicker1').datepicker({
					format: "M yyyy",
				    viewMode: "months", 
				    minViewMode: "months"
				}).datepicker('setDate',new Date());
		});

		function in_array(needle, haystack)
		{
		    var found = 0;
		    for (var i=0, len=haystack.length;i<len;i++) {
		        if (haystack[i] == needle) return i;
		            found++;
		    }
		    return -1;
		}

		function selectStopTime(selectedBox)
		{
			var stopTimes = $("#stopTimes").val().split(",");
			if (in_array(selectedBox,stopTimes)!= -1) 
			{
				$("#selectStopTime"+selectedBox).css("opacity","0.5");
				for(var i = stopTimes.length - 1; i >= 0; i--) {
					if(stopTimes[i] == selectedBox) {
				        stopTimes.splice(i, 1);
				    }
				}
				
				var selectedselectedBox = stopTimes.join(",");
				$("#stopTimes").val(selectedselectedBox);

			}
			else
			{   
				var checkstopTimes = $("#stopTimes").val();
				if (checkstopTimes != "") 
				{
					var selectedselectedBox = $("#stopTimes").val()+','+selectedBox;
					$("#stopTimes").val(selectedselectedBox);
				}else
				{
					$("#stopTimes").val(selectedBox);
				}
				$("#selectStopTime"+selectedBox).css("opacity","1");
			}
		
		}
		
	</script>
