    <script>

   
    
    $(document).ready(function() 
    {

        <?php 
            $dateValE = date("F d, Y", strtotime('today'));
            $dateValS =  date("F d, Y", strtotime('today - 29 days'));
        ?>

        var DdateValE = "<?php echo $dateValE; ?>"
        var DdateValS = "<?php echo $dateValS; ?>"

       


        $('.datepicker58').datepicker({
            format: "mm/dd/yyyy",
            autoclose : true,
            language: "<?php echo datepickerLanguage; ?>"
        });

        <?php 
            $dueDateValS = date("F d, Y", strtotime('today'));
            $dueDateValE =  date("F d, Y", strtotime('today - 29 days'));
        ?>

        var DdateValE = "<?php echo $dueDateValS; ?>"
        var DdateValS = "<?php echo $dueDateValE; ?>"
        
        $('#due-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Duedate ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
            $('#due-daterange').css("color","#FF8000");
        });

        



        <?php 
      
            $tableData = "[
                // { data: 'deviceName' },
                { data: 'machineId' },
                { data: 'logTime' },
                { data: 'tag' },
                { data: 'message' },
                { data: 'deviceName' },
                { data: 'appVersion' },
                { data: 'appId' },
                { data: 'level' },
                { data: 'stacktrace' },
                { data: 'osVersion' },
            ]";

          $columnDefs = "[
                {
                  targets: 0,
                  orderable: false,
                },
                {
                  targets: 1,
                  orderable: false,
                },
                {
                  targets: 2,
                  orderable: false,
                },
                {
                  targets: 3,
                  orderable: false,
                },
                {
                  targets: 4,
                  orderable: false,
                },
                {
                  targets: 5,
                  orderable: false,
                },
                {
                  targets: 6,
                  orderable: false
                },
                {
                  targets: 7,
                  orderable: false
                },
                {
                  targets: 8,
                  orderable: false
                },
                {
                  targets: 9,
                  orderable: false
                }

            ]";
       
         ?>

        $('#empTable').removeAttr('width').DataTable({
           "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
          'processing': true,
          "pagingType": "input", 
          'serverSide': true,
          'serverMethod': 'post',
          "pageLength" : 1000,
          "info" : false,
          "searching" : true,
          "lengthChange": false,
          "stateSave": true,
          "rowId" : 'taskId',
          'ajax': {
              'url':'setAppErrorLog_pagination',
              "type": "POST",
                "data":function(data) {
                    data.dueDateValS = $('#dueDateValS').val();
                    data.dueDateValE = $('#dueDateValE').val();
                    data.machineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                    }).get();
                    // data.deviceName = $('.filterPhoneId:checkbox:checked').map(function(){
                    //                   return $(this).val();
                    //                 }).get();
                    data.dueDate = $('#filterDueDate').val();
                },  
          },
          'columns': <?php echo $tableData; ?>,
          "columnDefs": <?php echo $columnDefs; ?>, 
            fixedColumns: true, 
            drawCallback: function( settings ) { 
                $(".paginate_page").html('<?php echo Page; ?>');
                var paginate_of = $(".paginate_of").text();
                var paginate_of = paginate_of.split(" ");
                $(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
                
            },
       });
       
        $('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#dueDateValS').val(dateValS);
            $('#dueDateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });
    }); 

  

    function uncheckDueDate()
    {
        $("#showDueDate").html("");
        $('#due-daterange').css("color","#b8b0b0");
        <?php 
            $dueDateValS = date("F d, Y", strtotime('today'));
            $dueDateValE = date("Y-m-d", strtotime("-29 day"));
            $dateValESET = date("Y-m-d", strtotime('today'));
            $dateValSSET =  date("Y-m-d", strtotime('today -29 days'));
        ?>

        var DdateValE = "<?php echo $dueDateValS; ?>"
        var DdateValS = "<?php echo $dueDateValE; ?>"

        $('#dueDateValS').val("");
        $('#dueDateValE').val("");

        $('#due-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonResetDueDate' class='btn btn-sm btn-primary'>Due Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
        
            $('#due-daterange').css("color","#FF8000");
        });

        $('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dueDateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dueDateValE = picker.endDate.format('YYYY-MM-DD');     
            $('#dueDateValS').val(dueDateValS);
            $('#dueDateValE').val(dueDateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $("#empTable").DataTable().ajax.reload();
    }

    function filterMachineId(machineId, machineName)
    {
        var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterMachineId.length > 0) 
        {
            $("#filterMachineIdSelected").css('color','#FF8000');

            if (in_array(machineId,filterMachineId) != -1) 
            {
                html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px; margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Machine; ?> - "+machineName+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

                $("#showMachinefilter").append(html);
            }
            else
            {
                $("#removeMachineButton"+machineId).remove();
            }
        }
        else
        {
            $("#showMachinefilter").html("");
            $("#filterMachineIdSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckMachineName(machineId)
    {
        $("#removeMachineButton"+machineId).remove();
        $("#filterMachineId"+machineId).prop('checked',false);
        var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterMachineId.length > 0) 
        {
        }
        else
        {
            $("#filterMachineIdSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    // function filterPhoneId(machineId, deviceName)
    // {
    //     var filterPhoneId = $('.filterPhoneId:checkbox:checked').map(function(){
    //                                   return $(this).val();
    //                             }).get();

    //     if (filterPhoneId.length > 0) 
    //     {
    //         $("#filterPhoneIdSelected").css('color','#FF8000');

    //         if (in_array(machineId,filterPhoneId) != -1) 
    //         {
    //             html = "<button id='removedeviceName"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px; margin-top:10px;' class='btn btn-sm btn-primary'><?php echo deviceName; ?> - "+deviceName+" &nbsp;&nbsp;<i onclick='uncheckdeviceName("+machineId+")' class='fa fa-times'></i></button>";

    //             $("#showdeviceName").append(html);
    //         }
    //         else
    //         {
    //             $("#removedeviceName"+machineId).remove();
    //         }
    //     }
    //     else
    //     {
    //         $("#showdeviceName").html("");
    //         $("#filterPhoneIdSelected").css('color','#b8b0b0');
    //     }
    //     $("#empTable").DataTable().ajax.reload();
    // }

    // function uncheckdeviceName(machineId)
    // {
    //     $("#removedeviceName"+machineId).remove();
    //     $("#filterPhoneId"+machineId).prop('checked',false);
    //     var filterPhoneId = $('.filterPhoneId:checkbox:checked').map(function(){
    //                                   return $(this).val();
    //                             }).get();
    //     if (filterPhoneId.length > 0) 
    //     {
    //     }
    //     else
    //     {
    //         $("#filterPhoneIdSelected").css('color','#b8b0b0');
    //     }
    //     $("#empTable").DataTable().ajax.reload();
    // }

   

    function in_array(needle, haystack)
    {
        var found = 0;
        for (var i=0, len=haystack.length;i<len;i++) 
        {
            if (haystack[i] == needle) return i;
                found++;
        }
        return -1;
    }
</script>



