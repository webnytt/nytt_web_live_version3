 <script>

 	function changeLanguage(languageId,swedishText)
 	{
		 $.ajax({
                url: '<?php echo base_url("ExternalListing/updateSaveLanguage") ?>',
                type: 'POST',
                data: {languageId: languageId,swedishText: swedishText},
                success: function (result) {
                    //location.reload();
                }
            });
 	}

 	function laguageType()
	{
		var type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (type.length > 0) 
		{
			$("#typeSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'dashboard'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='DailyButton' class='btn btn-sm btn-primary'>type - Dashboard  &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}

			if (in_array("'opapp'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='WeeklyButton' class='btn btn-sm btn-primary'>type - OpApp  &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'setappapi'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='MonthlyButton' class='btn btn-sm btn-primary'>type - SetApp  &nbsp;&nbsp;<i onclick='uncheckType(3)' class='fa fa-times'></i></button>";
			}

			
			$("#showType").html(html);
		}
		else
		{
			$("#TypeSelectedValue").css('color','#b8b0b0');
			$("#showtype").html("");
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			// html +="";
			$("#dashboard").prop('checked',false);
			$("#DailyButton").remove();
		}
		
		else if(id == "2") 
		{
			// html +="";
			$("#opapp").prop('checked',false);
			$("#WeeklyButton").remove();
		}

		else if(id == "3") 
		{
			// html +="";
			$("#setappapi").prop('checked',false);
			$("#MonthlyButton").remove();
		}
		var Type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (type.length > 0) 
		{
		
		}
		else
		{
			$("#typeSelectedValue").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

 		
	function editLanguage(languageId,englishText,swedishText)
	{
		$("#languageId").val(languageId);
		$("#englishText").text(englishText);
		$("#swedishText").val(swedishText);
		$("#editLanguage").modal();
	}



   

   	function updateSaveLanguage()
   	{
   		var languageId = $("#languageId").val();
		var swedishText = $("#swedishText").val();

		 $.ajax({
                url: '<?php echo base_url("ExternalListing/updateSaveLanguage") ?>',
                type: 'POST',
                data: {languageId: languageId,swedishText: swedishText},
                success: function (result) {
                    location.reload();
                }
            });
   	}

   	
	$(document).ready(function() 
	{
		$('#empTable').removeAttr('width').DataTable({
		  "language": {
            "search": "<?php echo Search; ?>:"
        	},
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : true,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : '',
		  'ajax': {
			  'url':'changelanguage_pagination',
			  "type": "POST",
				"data":function(data) {
					data.type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
						 { data: 'englishText' },
						 { data: 'swedishText' },
						 { data: 'type' },
						
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

	});



	
 	

  </script>


