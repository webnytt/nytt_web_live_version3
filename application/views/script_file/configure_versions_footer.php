<script>
	$(document).ready(function() {
		
		$('#empTable').removeAttr('width').DataTable({ 
		   "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
		      "first": "<?php echo First; ?>",
		      "last": "<?php echo Last; ?>",
		      "next": "<?php echo Next; ?>",
		      "previous": "<?php echo Previous; ?>",
		      "page": "<?php echo Page; ?>",
		      "of": "<?php echo of; ?>"
		    }
          },
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 50,
		  "order": [
			  [0, "desc" ] 
			],
		  'ajax': {
			  'url':'configure_versions_pagination',
			  "type": "POST",
				"data":function(data) {
					data.stackLightTypeId = $('#stackLightTypeId').val();
				},  
		  },
		  'columns': [
			 { data: 'versionId' },
			 { data: 'stackLightTypeName' },
			 { data: 'versionName' },
			 { data: 'fileName' },
			 { data: 'versionDate' },
		  ],
		  "columnDefs": [], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				$(".paginate_page").html('<?php echo Page; ?>');
				var paginate_of = $(".paginate_of").text();
				var paginate_of = paginate_of.split(" ");
				$(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
				
			},
	   }); 
	   
		
		$("#filter a").click(function() {
			var clickedId = $(this).attr('id');
			var stackLightTypeId = clickedId.replace('stackLightType',''); 
			$('#stackLightTypeId').val(stackLightTypeId);
			$("#filter a").removeClass('active');
			$(this).addClass('active');
			$("#empTable").DataTable().ajax.reload();
		});

		$("#stackLightTypeVersionId" ).change(function() {

        	stackLightTypeId = $(this).val();
		  	$.ajax({
                type: 'POST',
                url: "<?php echo site_url('admin/getstackLightTypeVersion'); ?>",
                data: {stackLightTypeId:stackLightTypeId},  
                success: function(data){
                    $("#StackversionName").val($.trim(data));
                },
                error: function() { 
                    alert("<?php echo Errorwhileselectversion; ?>.");
                }
           });

		});


		$('form#add_stackLightTypeVersion_form').submit(function(e) 
		{ 
        	var form = $(this);
        	e.preventDefault();
            $("#add_stackLightTypeVersion_submit").html('Saving...')
            var formData = new FormData(this);
			var classification_cfg = $("#classification_cfg").val();
            var classification_cfg_extension = classification_cfg.replace(/^.*\./, '');
			var classification_weights = $("#classification_weights").val();
            var classification_weights_extension = classification_weights.replace(/^.*\./, '');
			var detection_cfg = $("#detection_cfg").val();
            var detection_cfg_extension = detection_cfg.replace(/^.*\./, '');
			var detection_weights = $("#detection_weights").val();
            var detection_weights_extension = detection_weights.replace(/^.*\./, '');
			var label_txt = $("#label_txt").val();
            var label_txt_extension = label_txt.replace(/^.*\./, '');
			var labels_txt = $("#labels_txt").val();
            var labels_txt_extension = labels_txt.replace(/^.*\./, '');
            
            if (classification_cfg_extension != "cfg") 
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensioncfg; ?>"
        		}); 
            }
            else if(classification_weights_extension != "weights")
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensionweights; ?>"
        		});
            }
            else if(detection_cfg_extension != "cfg")
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensioncfg; ?>"
        		});
            }
            else if(detection_weights_extension != "weights")
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensionweights; ?>"
        		});
            }
            else if(label_txt_extension != "txt")
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensiontxt; ?>"
        		});
            }
            else if(labels_txt_extension != "txt")
            {
            	$("#add_stackLightTypeVersion_submit").html('Save')
            	$.gritter.add({
        			title: '<?php echo Error; ?>',
        			text: "<?php echo Pleaseselectavalidfilewithextensiontxt; ?>"
        		});
            }
            else
            {
	            $.ajax({ 
	                type: 'POST',
	                url: "<?php echo site_url('admin/add_stackLightTypeVersion'); ?>",
	                data:formData,
	                cache:false,
	                contentType: false,
	                processData: false,
	                dataType: "html",
	                success: function(data){
	                    if($.trim(data) == "<?php echo NewversionforstacklighttypeaddedsuccessfullyPleaseupdateitinyourstacklighttypesettingtoseethenewdetectionresults; ?>") { 

	                        $.gritter.add({
	                			title: '<?php echo Success; ?>',
	                			text: data
	                		})
	                        
	                        form.each(function(){
	                            this.reset();
	                        }); 
	                        
	                        $("#modal-add-stackLightTypeVersion").modal('hide').fadeOut(1500); 
	                        
	                        $("#add_stackLightTypeVersion_submit").html('Save')
	                        window.setTimeout(function(){location.reload()},3000)
	                        
	                    } else { 
	                        $.gritter.add({
	                			title: '<?php echo Error; ?>',
	                			text: data
	                		})
	                        $("#add_stackLightTypeVersion_submit").html('Save')
	                    }
	                    
	                },
	                error: function() { 
	                    alert("<?php echo Errorwhileaddingstacklightversion; ?>.");
	                    location.reload();
	                }
	           });
        	}
        });
	}); 
</script>