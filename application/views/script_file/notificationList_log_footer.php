 <script>
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function filterMachineId(machineId, machineName)
	{
		// alert("hello");
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterMachineId.length > 0) 
		{
			$("#filterMachineIdSelected").css('color','#FF8000');

			if (in_array(machineId,filterMachineId) != -1) 
			{
				html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px; margin-top:10px;' class='btn btn-sm btn-primary'>Machine - "+machineName+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

				$("#showMachinefilter").append(html);
			}
			else
			{
				$("#removeMachineButton"+machineId).remove();
			}
		}
		else
		{
			$("#showMachinefilter").html("");
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckMachineName(machineId)
	{
		$("#removeMachineButton"+machineId).remove();
		$("#filterMachineId"+machineId).prop('checked',false);
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterMachineId.length > 0) 
		{
		}
		else
		{
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterStatus()
	{
		var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterStatus.length > 0) 
		{
			$("#filterStatusSelected").css('color','#FF8000');

			var html = "";
			if (in_array("'0'",filterStatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusNotResponded' class='btn btn-sm btn-primary'>Status - Not Responded &nbsp;&nbsp;<i onclick='uncheckStatus(0)' class='fa fa-times'></i></button>";
				// $("#showStatus").append(html);
			}

			if (in_array("'1'",filterStatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusResponded' class='btn btn-sm btn-primary'>Status - Responded &nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
				
				// $("#showStatus").append(html);
			}

			$("#showStatus").html(html);
		}
		else
		{
			$("#filterStatusSelected").css('color','#b8b0b0');
			$("#showStatus").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckStatus(id)
	{
		if (id == "0") 
		{
			$("#filterNotResponded").prop('checked',false);
			$("#buttonFilterstatusNotResponded").remove();
		}
		else if (id == "1") 
		{
			$("#filterResponded").prop('checked',false);
			$("#buttonFilterstatusResponded").remove();
		}

		var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterStatus.length > 0) 
		{

		}
		else
		{
			$("#filterStatusSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
			$("#filtertypeselected").css('color','#FF8000');

			var html = "";
			if (in_array("'0'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonManually' class='btn btn-sm btn-primary'>Type - Red &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'1'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonSetApp' class='btn btn-sm btn-primary'>Type - Yellow &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}

			if (in_array("'2'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonOppApp' class='btn btn-sm btn-primary'>Type - Wait &nbsp;&nbsp;<i onclick='uncheckType(3)' class='fa fa-times'></i></button>";
			}
			$("#showType").html(html);
		}
		else
		{
			$("#filtertypeselected").css('color','#b8b0b0');
			$("#showType").html("");
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			$("#filterManually").prop('checked',false);
			$("#buttonManually").remove();
		}
		else if (id == "2") 
		{
			$("#filterSetApp").prop('checked',false);
			$("#buttonSetApp").remove();
		}
		else if (id == "3") 
		{
			$("#filterDashboard").prop('checked',false);
			$("#buttonOppApp").remove();
		}
		
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{

		}
		else
		{
			$("#filtertypeselected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterUserName(userId, userName)
	{
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
			$("#filterUserNameSelected").css('color','#FF8000');
			if (in_array(userId,filterUserName) != -1) 
			{
				html = "<button id='removeUserButton"+userId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>UserName - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
				$("#showUserFilter").append(html);
			}
			else
			{
				$("#removeUserButton"+userId).remove();
			}
		}
		else
		{
			$("#showUserFilter").html("");
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckUserName(userId)
	{
		$("#removeUserButton"+userId).remove();
		$("#filterUserName"+userId).prop('checked',false);
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{

		}
		else
		{
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
    
	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>

		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});




		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
		});

		<?php 
			$dueDateValS = date("F d, Y", strtotime('today'));
			$dueDateValE = date("F d, Y", strtotime('today - 29 days'));
		?>

		var DdateValE = "<?php echo $dueDateValS; ?>"
		var DdateValS = "<?php echo $dueDateValE; ?>"
		
		$('#due-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Responded Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
			$('#due-daterange').css("color","#FF8000");
		});


		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'notificationId',
		  'ajax': {
			  'url':'notificationListLog_pagination',
			  "type": "POST",
				"data":function(data) 
				{
					data.dateValS 	 = $('#dateValS').val();
					data.dateValE 	 = $('#dateValE').val();
					data.dueDateValS = $('#dueDateValS').val();
					data.dueDateValE = $('#dueDateValE').val();
					data.type 		 = $('.filterType:checkbox:checked').map(function()
										{
								      		return $(this).val();
							    		}).get();
					data.status 	 = $('.filterStatus:checkbox:checked').map(function()
										{
									      	return $(this).val();
								    	}).get();
					data.userId 	 = $('.filterUserName:checkbox:checked').map(function()
										{
								      		return $(this).val();
								    	}).get();
					data.machineId 	 = $('.filterMachineId:checkbox:checked').map(function()
										{
								      		return $(this).val();
								    	}).get();
				},  
		  },
		  'columns':[
						{ data: 'notificationId' },
						{ data: 'notificationText' },
						{ data: 'userName' },
						{ data: 'machineName' },
						{ data: 'addedDate' },
						{ data: 'flag' },
						{ data: 'logId' },
						{ data: 'status' },
					 	{ data: 'comment' },
						{ data: 'respondTime' },
						{ data: 'responderId' },
					 	{ data: 'type' }
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    }, 
			    {
			      targets: 3,
			      orderable: false,
			    }, 
			    {
			      targets: 4,
			      orderable: false,
			    }, 
			    {
			      targets: 5,
			      orderable: false,
			    }, 
			    {
			      targets: 6,
			      orderable: false,
			    }, 
			    {
			      targets: 7,
			      orderable: false,
			    }, 
			    {
			      targets: 8,
			      orderable: false,
			    }, 
			    {
			      targets: 9,
			      orderable: false,
			    }, 
			    {
			      targets: 10,
			      orderable: false,
			    }, 
			    {
			      targets: 11,
			      orderable: false,
			    }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

	}); 

 	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE =  date("Y-m-d");
		$dateValS =  date("Y-m-d", strtotime("-3000 day"));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}

 	function uncheckDueDate()
 	{
 		$("#showDueDate").html("");
 		$('#due-daterange').css("color","#b8b0b0");
 		<?php 
			$dueDateValS = date("F d, Y", strtotime('today'));
			$dueDateValE = date("Y-m-d", strtotime("-3000 day"));

			$dateValESET = date("Y-m-d", strtotime('today'));
			$dateValSSET =  date("Y-m-d", strtotime('today -3000 days'));
		?>

		var DdateValE = "<?php echo $dueDateValS; ?>"
		var DdateValS = "<?php echo $dueDateValE; ?>"

		$('#dueDateValS').val("<?php echo $dateValSSET; ?>");
		$('#dueDateValE').val("<?php echo $dateValESET; ?>");

		$('#due-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonResetDueDate' class='btn btn-sm btn-primary'>Responded Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
		
			$('#due-daterange').css("color","#FF8000");
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  	
			$('#dueDateValS').val(dueDateValS);
			$('#dueDateValE').val(dueDateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$("#empTable").DataTable().ajax.reload();
 	}

  </script>


