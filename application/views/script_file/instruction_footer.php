
<script>

    var i = 1;
    function add_sub_detail()
    {
        var html = '<div id="sub_detail_row'+j+'" class="row" style="margin-left: 0px;margin-right: 0px;margin-top: 10px;"><div style="padding: 0 5px;" class="col-md-12"><label style="font-weight: 600;"><?php echo Addsubtitledetail; ?> '+i+'</label></div><div style="padding: 0 5px;" class="form-group col-md-8"><input type="text" style="height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="sub_title'+i+'" name="sub_title[]" placeholder="<?php echo Entersubtitle; ?>" required ></div><div style="padding: 0 5px;" class="form-group col-md-4"><input type="text" style="height: 29px !important;display: none;" class="form-control col-md-12 border-left-right-top-hide" id="search_position'+i+'" name="search_position[]" placeholder="<?php echo Enterposition; ?>" value="1" > </div><div style="padding: 0 5px;" class="form-group col-md-8"><input type="file" style="height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="sub_title_gif'+i+'" name="sub_title_gif[]"> </div><div class="col-md-4"><span style="cursor: pointer;" onclick="remove_sub_detail('+i+')"> <img style="width: 33px;" src="<?php echo base_url("assets/nav_bar/delete.svg"); ?>"> </div><div class="col-md-12"><hr style="background: gray;"></div></div>';

        $("#sub_title_detail").append(html);
        i++;
    }

    function remove_sub_detail(value)
    {
        $("#sub_detail_row"+value).remove();
    }

    var j = 1;
    function edit_sub_detail()
    {
        var html = '<div id="edit_sub_detail_row'+j+'" class="row" style="margin-left: 0px;margin-right: 0px;margin-top: 10px;"><div style="padding: 0 5px;" class="col-md-12"><label style="font-weight: 600;"><?php echo Addsubtitledetail; ?> '+j+'</label></div><div style="padding: 0 5px;" class="form-group col-md-8"><input type="text" style="height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="sub_title'+j+'" name="sub_title[]" placeholder="<?php echo Entersubtitle; ?>" required ></div><div style="padding: 0 5px;" class="form-group col-md-4"><input type="text" style="height: 29px !important;display: none;" class="form-control col-md-12 border-left-right-top-hide" id="search_position'+j+'" name="search_position[]" placeholder="<?php echo Enterposition; ?>" value="1" > </div><div style="padding: 0 5px;" class="form-group col-md-8"><input type="file" style="height: 29px !important;" class="form-control col-md-12 border-left-right-top-hide" id="sub_title_gif'+j+'" name="sub_title_gif[]"> </div><div class="col-md-4"><span style="cursor: pointer;" onclick="remove_edit_sub_detail('+j+')"> <img style="width: 33px;" src="<?php echo base_url("assets/nav_bar/delete.svg"); ?>"> </div><div class="col-md-12"><hr style="background: gray;"></div></div>';

        $("#edit_sub_title_detail").append(html);
        j++;
    }

    function remove_edit_sub_detail(value)
    {
        $("#edit_sub_detail_row"+value).remove();
    }

    function deleteInstruction(instructionId)
    {
        if (confirm('Are sure you wan`t to delete')) 
        {
            $.ajax({
                url: "<?php echo base_url('Instruction/deleteInstruction') ?>",
                type: "post",
                data: {instructionId:instructionId} ,
                success: function (response) 
                {
                    location.reload();
                },
                error: function(jqXHR, textStatus, errorThrown) {
                   console.log(textStatus, errorThrown);
                }
            });   
        } 
    }

    function editInstruction(instructionId)
    {
       
        $.ajax({
            url: "<?php echo base_url('Instruction/getInstruction') ?>",
            type: "post",
            data: {instructionId:instructionId} ,
            success: function (response) 
            {
                var obj = JSON.parse(response);
                $("#instructionId").val(instructionId);
                $("#edit_main_title").val(obj.main_title);
                $("#edit_search_keyword").val(obj.search_keyword);
                $("#edit_search_position_main").val(obj.search_position);
                $("#old_edit_main_title_gif").val(obj.edit_main_title_gif);
                $("#modal-edit-task").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });  
    }

    function searchInstruction(search_keyword,instruction_type)
    {
       
        $.ajax({
            url: "<?php echo base_url('Instruction/searchInstruction') ?>",
            type: "post",
            data: {search_keyword:search_keyword,instruction_type:instruction_type} ,
            success: function (response) 
            {
                if (instruction_type == "1") 
                {
                    $("#dashboardSerachData").html(response);
                }else if(instruction_type == "2")
                {
                    $("#opAppSerachData").html(response);
                }else
                {
                    $("#setAppSerachData").html(response);
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });  
    }

    function editSubInstruction(subInstructionId)
    {
        $.ajax({
            url: "<?php echo base_url('Instruction/getSubInstruction') ?>",
            type: "post",
            data: {subInstructionId:subInstructionId} ,
            success: function (response) 
            {
                var obj = JSON.parse(response);
                $("#subInstructionId").val(subInstructionId);
                $("#edit_sub_title").val(obj.subTitle);
                $("#old_edit_sub_title_gif").val(obj.subTitleGif);
                $("#edit_sub_search_position").val(obj.searchPosition);
                $("#modal-edit-sub-task").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });    
    }

    $(document).ready(function() 
    {
        $('form#add_instruction_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#add_instruction_submit").html('Saving...')
            $("#add_instruction_submit").attr('disabled',true);
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Instruction/add_save_instruction'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    if($.trim(data) == "1") { 
                        $.gritter.add({
                            title: 'Success',
                            text: 'Instruction added successfully.'
                        })
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#add_instruction_submit").html('Saved...')
                        window.setTimeout(function(){location.reload()},3000)
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: 'Error',
                            text: data
                        })
                        $("#add_instruction_submit").attr('disabled',false);
                        $("#add_instruction_submit").html('Save')
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: 'Error',
                        text: 'Error while adding user.'
                    })
                    location.reload();
                }
            });
        });


        $('form#edit_sub_instruction_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#edit_sub_instruction_submit").html('Saving...')
            $("#edit_sub_instruction_submit").attr('disabled',true);
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Instruction/edit_sub_save_instruction'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    if($.trim(data) == "1") { 
                        $.gritter.add({
                            title: 'Success',
                            text: 'Sub instruction edit successfully.'
                        })
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#edit_sub_instruction_submit").html('Saved...')
                        window.setTimeout(function(){location.reload()},3000)
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: 'Error',
                            text: data
                        })
                        $("#edit_sub_instruction_submit").attr('disabled',false);
                        $("#edit_sub_instruction_submit").html('Save')
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: 'Error',
                        text: 'Error while adding user.'
                    })
                    location.reload();
                }
            });
        }); 


        $('form#edit_instruction_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#edit_instruction_submit").html('Saving...')
            $("#edit_instruction_submit").attr('disabled',true);
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Instruction/edit_save_instruction'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    if($.trim(data) == "1") { 
                        $.gritter.add({
                            title: 'Success',
                            text: 'Instruction edit successfully.'
                        })
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#edit_instruction_submit").html('Saved...')
                        window.setTimeout(function(){location.reload()},3000)
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: 'Error',
                            text: data
                        })
                        $("#edit_instruction_submit").attr('disabled',false);
                        $("#edit_instruction_submit").html('Save')
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: 'Error',
                        text: 'Error while adding user.'
                    })
                    location.reload();
                }
            });
        });
    });
</script>

