<script>
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function filterMachineId(machineId, machineName)
	{
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterMachineId.length > 0) 
		{
			$("#filterMachineIdSelected").css('color','#FF8000');

			if (in_array(machineId,filterMachineId) != -1) 
			{
				html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>Machine Name :- "+machineName+'&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Machine Id:- '+machineId+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

				$("#showMachinefilter").append(html);
			}
			else
			{
				$("#removeMachineButton"+machineId).remove();
			}
		}
		else
		{
			$("#showMachinefilter").html("");
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckMachineName(machineId)
	{
		$("#removeMachineButton"+machineId).remove();
		$("#filterMachineId"+machineId).prop('checked',false);
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterMachineId.length > 0) 
		{
		}
		else
		{
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterColor()
	{
		var filterColor = $('.filterColor:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterColor.length > 0) 
		{
			$("#filterColorSelected").css('color','#FF8000');

			var html = "";
			if (in_array("'NoData'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterNoData' class='btn btn-sm btn-primary'>Color - NoData &nbsp;&nbsp;<i onclick='uncheckColor(1)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'yellow'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilteryellow' class='btn btn-sm btn-primary'>Color - yellow &nbsp;&nbsp;<i onclick='uncheckColor(2)' class='fa fa-times'></i></button>";
			}

			if (in_array("'Off'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterOff' class='btn btn-sm btn-primary'>Color - Off &nbsp;&nbsp;<i onclick='uncheckColor(3)' class='fa fa-times'></i></button>";
			}

			if (in_array("'green'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFiltergreen' class='btn btn-sm btn-primary'>Color - green &nbsp;&nbsp;<i onclick='uncheckColor(4)' class='fa fa-times'></i></button>";
			}

			if (in_array("'green yellow'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFiltergreenyellow' class='btn btn-sm btn-primary'>Color - green yellow &nbsp;&nbsp;<i onclick='uncheckColor(5)' class='fa fa-times'></i></button>";
			}

			if (in_array("'red green'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterredgreen' class='btn btn-sm btn-primary'>Color - red green &nbsp;&nbsp;<i onclick='uncheckColor(6)' class='fa fa-times'></i></button>";
			}

			if (in_array("'red'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterred' class='btn btn-sm btn-primary'>Color - red &nbsp;&nbsp;<i onclick='uncheckColor(7)' class='fa fa-times'></i></button>";
			}

			if (in_array("'red yellow green'",filterColor) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterredyellowGreen' class='btn btn-sm btn-primary'>Color - red yellow green &nbsp;&nbsp;<i onclick='uncheckColor(8)' class='fa fa-times'></i></button>";
			}

			$("#showColor").html(html);
		}
		else
		{
			$("#filterColorSelected").css('color','#b8b0b0');
			$("#showColor").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}



	function uncheckColor(id)
	{
		if (id == "1") 
		{
			$("#filterColorNoData").prop('checked',false);
			$("#buttonFilterNoData").remove();
		}
		else if (id == "2") 
		{
			$("#filterColoryellow").prop('checked',false);
			$("#buttonFilteryellow").remove();
		}
		else if (id == "3") 
		{
			$("#filterColorOff").prop('checked',false);
			$("#buttonFilterOff").remove();
		}
		else if (id == "4") 
		{
			$("#filterColorgreen").prop('checked',false);
			$("#buttonFiltergreen").remove();
		}
		else if (id == "5") 
		{
			$("#filterColorgreenyellow").prop('checked',false);
			$("#buttonFiltergreenyellow").remove();
		}
		else if (id == "6") 
		{
			$("#filterColorredgreen").prop('checked',false);
			$("#buttonFilterredgreen").remove();
		}
		else if (id == "7") 
		{
			$("#filterColorred").prop('checked',false);
			$("#buttonFilterred").remove();
		}
		else if (id == "8") 
		{
			$("#filterColorredYellowGreen").prop('checked',false);
			$("#buttonFilterredyellowGreen").remove();
		}

		var filterColor = $('.filterColor:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterColor.length > 0) 
		{

		}
		else
		{
			$("#filterColorSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

    
	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Original Time - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});
		
		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 1000,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'logId',
		  'ajax': {
			  'url':'beta_detection_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.machineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.color = $('.filterColor:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
						{ data: 'logId' },
					 	{ data: 'machineId' },
					 	{ data: 'machineName' },
					 	{ data: 'color' },
					 	{ data: 'accuracy' },
					 	{ data: 'originalTime' }
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    },
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
	}); 
	

	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE =  date("Y-m-d");
		$dateValS =  date("Y-m-d", strtotime("-3000 day"));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': 		[moment(), moment()],
				'Yesterday': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': 	[moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': 	[moment().startOf('month'), moment().endOf('month')],
				'Last Month': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>originalTime - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}
</script>



 


