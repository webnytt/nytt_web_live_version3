<script>  
        function showPassword(id,type)
        {
            if (type == "text") 
            {
                type1 = "'password'";
                class1 = "fa fa-eye"; 
            }
            else
            {
                type1 = "'text'";
                class1 = "fa fa-eye-slash"; 
            }
            $("#"+id).attr("type",type);
            id1 = "'"+id+"'";
            $("#icon"+id).html('<i onclick="showPassword('+id1+','+type1+');" style="margin-top: -5px;" class="'+class1+'"></i>');
        } 

        function showPassword(id,type)
        {
            if (type == "text") 
            {
                type1 = "'password'";
                class1 = "fa fa-eye"; 
            }
            else
            {
                type1 = "'text'";
                class1 = "fa fa-eye-slash"; 
            }
            $("#"+id).attr("type",type);
            id1 = "'"+id+"'";
            $("#icon"+id).html('<i onclick="showPassword('+id1+','+type1+');" style="margin-top: -5px;" class="'+class1+'"></i>');
        } 
           
        function readURL(input) 
        {
            var reader = new FileReader();
		        reader.onload = function (e)
                {
		            $('.file-upload-image').attr('src', e.target.result);
		            $('.image-title').html(input.files[0].name);
		        };
		        reader.readAsDataURL(input.files[0]);
			}
		$(document).ready(function() {
            $("form#update_profile_form").submit(function(e) 
            {  
                var form = $(this);
                e.preventDefault();
                var formData = new FormData(this);
                $("#update_profile_submit").html('<?php echo Updating; ?>...');
                $.ajax({
                    type: 'POST',
                    url: "<?php echo site_url('admin/update_profile'); ?>",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    dataType: "html",
                    success: function(data){
                        
                        if($.trim(data) == "1") 
                        { 
                            $.gritter.add({
								title: '<?php echo Success; ?>',
								text: '<?php echo Profileupdatedsuccessfully; ?>.'
							});
							
                            $("#update_profile_submit").html('<?php echo Updated; ?>...');
                            location.reload(2000);
                            }else if($.trim(data) == "2") { 
                            $.gritter.add({
                                title: '<?php echo Error; ?>',
                                text: '<?php echo Noupdatetosave; ?>.'
                            });
                            
                            $("#update_profile_submit").html('<?php echo Update; ?>');
                        } 
                        else 
                        {  
							$.gritter.add({
								title: '<?php echo Error; ?>',
								text: data
							})
							$("#update_profile_submit").html('<?php echo Update; ?>');
                        }
                    },
                    error: function() 
                    { 
                    	$.gritter.add({
								title: '<?php echo Error; ?>',
								text: '<?php echo Errorwhileupdatingprofile; ?>.'
							});
                        location.reload();
                    }
                });
            });
        }); 
        $(".toggle-password").click(function() 
        {

            $(this).toggleClass("fa-eye fa-eye-slash");
            var input = $($(this).attr("toggle"));
            if (input.attr("type") == "password") 
            {
                input.attr("type", "text");
            } 
            else 
            {
                input.attr("type", "password");
            }
        });
</script>
	
	
