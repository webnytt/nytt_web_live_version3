<script>

	function exportCsvBeta()
	{
		var dateValS = $('#dateValS').val();
		var dateValE = $('#dateValE').val();
		var machineId = $('#machineId').val();

		var serchFiled = $("#empTable_filter input").val();
		location.href = "<?php echo base_url('admin/exportBetaDetectionCsv?machineId=') ?>"+machineId+"&dateValS="+dateValS+"&dateValE="+dateValE+"&serchFiled="+serchFiled;
	}
	$(document).ready(function() {

		
		
		<?php 
		
		if($_POST && isset($_POST['dateValS'])) { 
			$dateValE = date("F d, Y", strtotime($_POST['dateValE']));
			$dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
		} else { 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		} 
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		
		$('#advance-daterange span').html("<?php echo Last30Days; ?>");
		
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: '<?php echo Submit; ?>',
				cancelLabel: '<?php echo Cancel; ?>',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
				monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
				firstDay: 1
			}
		}, function(start, end, label) {

			if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }
			$('#advance-daterange span').html(label);
		});
		
		
		
		$('#empTable').removeAttr('width').DataTable({ 
			 "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
		      "first": "<?php echo First; ?>",
		      "last": "<?php echo Last; ?>",
		      "next": "<?php echo Next; ?>",
		      "previous": "<?php echo Previous; ?>",
		      "page": "<?php echo Page; ?>",
		      "of": "<?php echo of; ?>"
		    }
          },
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "order": [
			  [0, "asc" ] 
			],
		  'ajax': {
			  'url':'beta_detection_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.machineId = $('#machineId').val();
				},  
		  },
		  'columns': [
			 { data: 'logId' },
			 { data: 'machineId' },
			 { data: 'machineName' },
			 { data: 'color' },
			 { data: 'machineStatus' },
			 { data: 'originalTime' },
		  ],
		  "columnDefs": [], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				$(".paginate_page").html('<?php echo Page; ?>');
				var paginate_of = $(".paginate_of").text();
				var paginate_of = paginate_of.split(" ");
				$(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
			},
	   }); 

		
	   
	   $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) { 
		 var machineId = $('#machineId').val();
			var dateValS = picker.startDate.format('YYYY-MM-DD');  
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		
		
		$("#filter a").click(function() {
			var clickedId = $(this).attr('id');
            var machineId = clickedId.replace('machine',''); 
			$('#machineId').val(machineId);
			$("#filter a").removeClass('active');
			$(this).addClass('active');  
			$('#dateValS').val();
			$('#dateValE').val();
			$("#empTable").DataTable().ajax.reload();
		});
	
		
	}); 
    </script>