<script>
	$('#carousel-example').on('slide.bs.carousel', function (e) 
	{
		var $e = $(e.relatedTarget);
		    var idx = $e.index();
		    var itemsPerSlide = 5;
		    var totalItems = $('.carousel-item').length;
		    if (idx >= totalItems-(itemsPerSlide-1)) {
		        var it = itemsPerSlide - (totalItems - idx);
		        for (var i=0; i<it; i++) {
		            // append slides to end
		            if (e.direction=="left") 
		            {
		                $('.carousel-item').eq(i).appendTo('.carousel-inner');
		            }
		            else 
		            {
		                $('.carousel-item').eq(0).appendTo('.carousel-inner');
		            }
		        }
		    }
	});
	
	$('#modal-add-task').on('hidden.bs.modal', function () 
	{
	  $("#addTaskButton").show();
	})

	$('#modal-add-task').on('shown.bs.modal', function ()
	{
	  $("#addTaskButton").hide();
	})

	$('#modal-delete-task').on('hidden.bs.modal', function () 
	{
	  $("#isDelete").val("0");
	})

    function closeDetails()
    {
    	task_id = $("#task_id").val();
    	$("#"+task_id).css("background", "white");
		$("#"+task_id).css("border-left", "none");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    function delete_task(task_id)
    {
    	$("#isDelete").val("1");
    	$("#deleteTaskId").val(task_id);
    	$("#modal-delete-task").modal();
    }

    function selectRepeat(value)
    {
    	if (value == "never") 
    	{
    		$(".dueDate").show();
    		$("#selectedRepeat").css("padding-right","75%");
    		$("#selectedRepeat").html("Never");
    		$("#repeat").val(value);
    	}
    	else if (value == "everyDay") 
    	{
    	  	var today = new Date();
		  	var day = today.getDay();
			var days = ["Sunday","Monday","Tuesday","Wednesday","Thursday","Friday","Saturday"];
			var dayname = days[day];
			if (dayname == "Saturday" || dayname == "Sunday") 
			{
			  	var r = confirm("This daily task will be added for next monday as today is holiday.");
				if (r == false) 
				{
					$('#modal-add-task').modal('hide');
				}
				else
				{
					$(".dueDate").hide();
		    		$("#selectedRepeat").css("padding-right","64%");
		    		$("#selectedRepeat").html("Every day");
		    		$("#repeat").val(value);
				}
			}
			else
			{
				$(".dueDate").hide();
	    		$("#selectedRepeat").css("padding-right","64%");
	    		$("#selectedRepeat").html("Every day");
	    		$("#repeat").val(value);
			}
	    }
	    else if (value == "everyWeek") 
    	{
    		$(".dueDate").hide();
    		$("#selectedRepeat").css("padding-right","60%");
    		$("#selectedRepeat").html("Every week");
    		$("#repeat").val(value);
    	}
    	else if (value == "everyMonth") 
    	{
    		$(".dueDate").hide();
    		$("#selectedRepeat").css("padding-right","56%");
    		$("#selectedRepeat").html("Every month");
    		$("#repeat").val(value);
    	}
    	else if (value == "everyYear") 
    	{
    		$(".dueDate").hide();
    		$("#selectedRepeat").css("padding-right","62%");
    		$("#selectedRepeat").html("Every year");
    		$("#repeat").val(value);
    	}
    }

    function plusMinusDate(type)
    {
    	dueDate = $("#dueDate").val();
    	$.ajax({
	        url: "<?php echo base_url("Tasks/get_due_date") ?>",
	        type: "post",
	        data: {type:type,dueDate:dueDate} ,
	        success: function (response) 
	        {
				$("#dueDate").val($.trim(response));	
	        },
	        error: function(jqXHR, textStatus, errorThrown) 
	        {
	           console.log(textStatus, errorThrown);
	        }
	    });
	}

    function allOperators()
    {
    	if($("#usersAll").prop('checked') == true)
    	{
		    $(".operatorsCheckAll").prop('checked',true);
		    $(".operatorsLabelAll").css('color',"#FF8000");
		}
		else
		{
		    $(".operatorsLabelAll").css('color',"#1f2225");
			$(".operatorsCheckAll").prop('checked',false);
		}
    }

    function Operators(userId)
    {

    	if($("#users"+userId).prop('checked') == true){
		    $("#users"+userId).prop('checked',true);
		    $("#operatorsLabel"+userId).css('color',"#FF8000");
		}
		else
		{
			$("#users"+userId).prop('checked',false);
		    $("#operatorsLabel"+userId).css('color',"#1f2225");
		}
    }

    function checkRepeat(value)
    {
    	$(".repeatAll").css("color","#1f2225");
    	if($("#"+value).prop('checked') == true)
    	{
		    $("#"+value).prop('checked',true);
		    $("#"+value+"Label").css('color',"#FF8000");
		}
		else
		{
			$("#"+value).prop('checked',false);
		    $("#"+value+"Label").css('color',"#1f2225");
		}
    }

    function checkStatus(value)
    {
    	$(".statusAll").css("color","#1f2225");
    	if($("#"+value).prop('checked') == true){
		    $("#"+value).prop('checked',true);
		    $("#"+value+"Label").css('color',"#FF8000");
		}
		else
		{
			$("#"+value).prop('checked',false);
		    $("#"+value+"Label").css('color',"#1f2225");
		}
    }

	$('form#update_task_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#update_task_submit").html('Saving...')
        var formData = new FormData(this);
        $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Tasks/update_task'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
					var obj = $.parseJSON($.trim(data));
					//console.log(obj);
					if(obj.status == "1") 
					{
						socket.emit('Update task data', obj.taskData,'update'); 
						$("#update_task_submit").html('Saved...')
						$.gritter.add({
							title: 'Success',
							text: obj.message
						});
					}
					else 
					{ 	
						$.gritter.add({
							title: 'Error',
							text: obj.message
						});
						$("#update_task_submit").html('Save')
					}
                },
                error: function() { 
                    alert("Error while update task.");
                }
           });
    });

    $('form#delete_task_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#delete_task_submit").html('Deleting...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Tasks/delete_task'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                	var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{
						socket.emit('Update task data', obj.taskData,'delete');
						$("#delete_task_submit").html('Deleted...')
						$.gritter.add({
							title: 'Success',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}else 
					{ 	
						$.gritter.add({
							title: 'Error',
							text: obj.message
						});
						$("#delete_task_submit").html('Delete')
					}
                },
                error: function() { 
                    alert("Error while update task.");
                }
           });
    });

 	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#advance-daterange').css("color","#FF8000");
		});

	
		$('#due-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#due-daterange').css("color","#FF8000");
		});
		
		$('table').on( 'click', 'tr', function () 
		{
		    task_id = $(this).attr('id');
		    if(task_id != "" && task_id != undefined)
		    {
				$.ajax({
			        url: "<?php echo base_url('Task_testing/getTaskById') ?>",
			        type: "post",
			        data: {task_id:task_id} ,
			        success: function (response) 
			        {
			        	console.log(response)
			        	isDelete = $("#isDelete").val();

			        	if (isDelete == "0") 
			        	{
				        	var obj = $.parseJSON(response);
							task_id = obj.task_id;
							task_name = obj.task_name;
				        	task_description = obj.task_description;
				        	created_date = obj.created_date;
				        	type = obj.type;
				        	status = obj.status;
				        	
				        	$("#task_id").val(task_id);
				        	$("#task_nameDed").val(task_name);
				        	$("#task_descriptionDed").val(task_description);
				        	$("#created_date").text(created_date);

				        	if (type == "1") 
				        	{
				        		$("input[name=type][value=1]").attr('checked', 'checked');
				        	}
				        	else
				        	{
				        		$("input[name=type][value=0]").attr('checked', 'checked');
				        	}

							if (type == "1") 
				        	{
				        		$("#typeValue option[value=0]").attr('selected', false);
				        		$("#typeValue option[value=1]").attr('selected', true);
				        	}
				        	else
				        	{
				        		$("#typeValue option[value=1]").attr('selected', false);
				        		$("#typeValue option[value=0]").attr('selected', true);
				        	}

							if(status == "1")
				        	{
				        		$("input[name=status][value=1]").attr('checked', 'checked');
				        	}
				        	else
				        	{
				        		$("input[name=status][value=0]").attr('checked', 'checked');
				        	}
				        	
							if (status == "1") 
				        	{
				        		$("#statusValue option[value=0]").attr('selected', false);
				        		$("#statusValue option[value=1]").attr('selected', true);
				        	}
				        	else
				        	{
				        		$("#statusValue option[value=1]").attr('selected', false);
				        		$("#statusValue option[value=0]").attr('selected', true);
				        	}
				        	
				        	$('tr').css("border-left", "none");
						 	$('tr').css("background", "#FFFFFF");
						 	$("#"+task_id).css("background", "#F2F2F2");
						 	$("#"+task_id).css("border-left", "7px solid #FF8000");

						 	$("#empTable").css("width", "100%");
						 	$(".table_data").removeClass("col-md-12");
						 	$(".table_data").addClass("col-md-9");
						    $(".detail").show();
						}
			           
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		})

		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 10,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'task_id',
		  'ajax': {
			  'url':'task_maintenace_pagination',
			  "type": "POST",
				"data":function(data) {
					data.isDelete = $('.filterIsDelete:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

					data.type = $('.filtertype:checkbox:checked').map(function()
								{
								      return $(this).val();
							    }).get();

					data.status = $('.filterstatus:checkbox:checked').map(function()
								{
								      return $(this).val();
							    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  },
		  'columns': [
			{ data: 'task_id' },
			{ data: 'task_name' },
			{ data: 'task_description' },
			{ data: 'status' },
			{ data: 'id_deleted' },
			{ data: 'created_date' },
			{ data: 'type' },
		],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    },
			    {
			      targets: 6,
			      orderable: false,
			    }
			    
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$(".col-sm-5").html('<a id="addTaskButton" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-task" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;ADD TASK</a>');

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			if (dateValS.length > 0) 
			{
				$("#dateValSSelected").css('color','#FF8000');

				var html = "";
				if (in_array("'Today'",dateValS) != -1) 
				{
					html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Complete&nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
				}
				$("#showDate").html(html);
			}
			else
			{
				$("#showDate").html("");
			}
			$("#empTable").DataTable().ajax.reload();
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
	}); 

	function filterIsDelete()
	{
		var filterIsDelete = $('.filterIsDelete:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterIsDelete.length > 0) 
		{
			$("#filterIsDeleteSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterIsDeleteSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterstatus()
	{
		var filterstatus = $('.filterstatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterstatus.length > 0) 
		{
			$("#filterstatusSelected").css('color','#FF8000');
			var html = "";
			if (in_array("'1'",filterstatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Complete&nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
			}
			if (in_array("'0'",filterstatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusUncompleted' class='btn btn-sm btn-primary'>Uncomplete&nbsp;&nbsp;<i onclick='uncheckStatus(0)' class='fa fa-times'></i></button>";
			}

			$("#showStatus").html(html);
		}
		else
		{
			$("#filterstatusSelected").css('color','#b8b0b0');
			$("#showStatus").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckStatus(id)
	{
		if (id == "1") 
		{
			$("#filterstatusCompleted").prop('checked',false);
			$("#buttonFilterstatusCompleted").remove();
		}
		else if (id == "0") 
		{
			$("#filterstatusUncompleted").prop('checked',false);
			$("#buttonFilterstatusUncompleted").remove();
		}
		var filterstatus = $('.filterstatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterstatus.length > 0) 
		{

		}
		else
		{
			$("#filterstatusSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function filtertype()
	{
		var filtertype = $('.filtertype:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filtertype.length > 0) 
		{
			$("#filtertypeselected").css('color','#FF8000');
		}
		else
		{
			$("#filtertypeselected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	$('form#add_task_form').submit(function(e) 
	{ 
	    var form = $(this);
        e.preventDefault();
			$("#add_task_submit").attr('disabled',true); 
			$("#add_task_submit").html('Saving...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Task_testing/add_task'); ?>",
				data: form.MytoJson(),  
				dataType: "html",
				success: function(data)
				{
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{  
						$("#add_task_submit").html('Saved...')
						$.gritter.add({
							title: 'Success',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: 'Error',
							text: obj.message
						});
						$("#add_task_submit").attr('disabled',false); 
						$("#add_task_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add task')
					}
				},
				error: function() { 
					$.gritter.add({
							title: 'Error',
							text: 'Error while adding task'
						});
					$("#add_task_submit").attr('disabled',false);
					$("#add_task_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add task')
				}
		   });
	}); 
  
  	$( function() 
  	{
    	$( "#datepicker" ).datepicker();
  	});
</script>


       