<script>
	$(document).ready(function() 
	{
		var availableTags = [
			'Operator Interface',
			'Tool Breakage',
			'Lack of material',
			'Machine Breakdown',
			'Setup'
		];
		<?php 
			if($_POST && isset($_POST['dateValS'])) 
			{ 
				$dateValE = date("F d, Y", strtotime($_POST['dateValE']));
				$dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
			} 
			else 
			{ 
				$dateValE = date("F d, Y", strtotime('today'));
				$dateValS =  date("F d, Y", strtotime('today - 29 days'));
			} 
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		
		$('#advance-daterange span').html(moment(DdateValS).format('MMMM D, YYYY') + ' - ' + moment(DdateValE).format('MMMM D, YYYY'));
		
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['Su', 'Mo', 'Tu', 'We', 'Th', 'Fr','Sa'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			$('#advance-daterange span').html(start.format('MMMM D, YYYY') + ' - ' + end.format('MMMM D, YYYY'));
		});
		
		$('#empTable').removeAttr('width').DataTable({ 
		  "pagingType": "input", 
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 50,
		  "order": [
			  [0, "desc" ]
			],  
		  'ajax': {
			  'url':'notification_list_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.machineId = $('#machineId').val(); 
				},  
		  },
		  'columns': [
			 { data: 'logId' },
			 { data: 'commentVal' },
			 { data: 'responderName' },
			 { data: 'respondTime' },
			 { data: 'machineName' },
			 { data: 'startTime' }, 
			 { data: 'endTime' },
			 { data: 'duration' },
			 { data: 'addedDate' }, 
		  ],
		  "columnDefs": [
				{
					"targets": [ 7 ],
					"searchable": false,
					"orderable": false,
				},
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
				$('[id^=comment').autocomplete({
					source: availableTags
				});
				
				$("form[id^='edit_logs_form']").submit(function(e) {  
					
					var form = $(this);
					e.preventDefault();
					var formData = new FormData(this);
					var formId = form.attr('id');
					var logId = formId.replace('edit_logs_form',''); 
					$("[id^='edit_logs_form_submit"+logId+"']").html('Saving...')
					var modal_div = "[id^='modal-edit-comment"+logId+"']";
					$.ajax({
						type: 'POST',
						url: "<?php echo site_url('admin/notification_list_comment'); ?>",
						data:formData,
						cache:false,
						contentType: false,
						processData: false,
						dataType: "html",
						success: function(data){
								$.gritter.add({
									title: 'Success',
									text: 'Commented successfully'
								});
								
								$("[id^='edit_logs_form_submit"+logId+"']").html('Saved...')
								
								$(modal_div).modal('hide').fadeOut(1500);
								
								$("[id^='edit_logs_form_submit"+logId+"']").html('Save')
								$("[id^='loadComment"+logId+"']").html($('#comment'+logId).val()+'<br />')
						},
						error: function() { 
							aler.t("Error while commenting.");
							location.reload();
						}
				   });
				});
			},
	   	}); 
	   
	   $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
	   { 
		  	var userId = $('#userId').val();
			var dateValS = picker.startDate.format('YYYY-MM-DD');  
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		
		
		$("#filter a").click(function() 
		{
			var clickedId = $(this).attr('id');
            var machineId = clickedId.replace('machine',''); 
			$('#machineId').val(machineId);
			$("#filter a").removeClass('active');
			$(this).addClass('active');  
			$('#dateValS').val();
			$('#dateValE').val();
			$("#empTable").DataTable().ajax.reload();
		});
	}); 
</script>