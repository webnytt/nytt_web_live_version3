<script>
$(document).ready(function() 
{
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#due-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},	
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		
		function(start, end, label) 
		{
			$('#due-daterange').css("color","#FF8000");
		});

		$('#empTable').removeAttr('width').DataTable({
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  'ajax': {
			  'url':'socket_connect_disconnect_data_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.dueDateValS = $('#dueDateValS').val();
					data.dueDateValE = $('#dueDateValE').val();
					data.machineId =  $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
				},  
		  },
		  'columns': [
			 { data: 'machineName' },
			 { data: 'startTime' },
			 { data: 'endTime' },
			 { data: 'reason' },
			 { data: 'reasonText' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			  	{
			      targets: 2,
			      orderable: false,
			    },
			  	{
			      targets: 3,
			      orderable: false,
			    },
			  	{
			      targets: 4,
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   	});


		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		
	}); 

	function filterMachineId()
	{
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterMachineId.length > 0) 
		{
			$("#filterMachineIdSelected").css('color','#FF8000');
		}
		else
		{
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}

		$("#empTable").DataTable().ajax.reload();
	}
</script>