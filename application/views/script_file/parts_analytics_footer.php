	<script src="https://cdnjs.cloudflare.com/ajax/libs/echarts/5.1.2/echarts.min.js"></script>  
	<script src="<?php echo base_url('assets/scroller_timepicker/picker.js'); ?>"></script>  
	<script>
		
		function selectView(viewId,viewName)
		{
			$("#viewId").val(viewId);
			$("#selectedView").text(viewName);
			$(".dropdown-item").removeClass('selectedView');
			$("#viewId"+viewId).addClass("selectedView");
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		function orderDetails(machinePartsFinishLogId,partsNumberParame,partsNameParame,partsOperationParame,startDateParame,endDateParame,correctPartsParame,scrapPartsParame,timeTookParame,createDateParame,userImageParame,userNameParame,ManufacturingOrderIdParame)
		{
			$("#orderStartDate").text(startDateParame);
			$("#orderEndDate").text(endDateParame);
			$("#orderPartsName").text(partsNameParame);
			$("#orderpartsNumber").text(partsNumberParame);
			$("#orderpartsOperation").text(partsOperationParame);
			$("#ordercorrectParts").text(correctPartsParame);
			$("#orderscrapParts").text(scrapPartsParame);
			$("#orderTimeTook").text(timeTookParame);
			$("#orderCreateDate").text(createDateParame);
			$("#orderUserImage").attr("src","<?php echo base_url('assets/img/user/') ?>"+userImageParame);
			$("#orderUserName").text(userNameParame);
			$("#orderManufacturingOrderId").text(ManufacturingOrderIdParame);
			$("#orderTotalParts").text(Number(scrapPartsParame) + Number(correctPartsParame));

			$("#orderDetails").modal();
		}


				function removeFirstChar(sText){
				   return Math.abs(sText);
				}
		
		<?php if(!empty($_POST['partsChoose1'])) { ?>
			changeFilterType("<?php echo $_POST['partsChoose1'] ?>");
		<?php }else{ ?>
			changeFilterValue();
		<?php } ?>
		function changeFilterType(value)
		{
			if (value == "day") 
			{
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").hide();
				$(".day").show();
				$("#headerChange").text("<?php echo Hourlymachineanalysis; ?>");
			}
			else if (value == "weekly") 
			{
				$(".day").hide();	
				$(".monthly").hide();
				$(".yearly").hide();
				$(".weekly").show();
				$("#headerChange").text("<?php echo Dailymachineanalysis; ?>");
			}
			else if (value == "monthly") 
			{
				$(".day").hide();	
				$(".yearly").hide();
				$(".weekly").hide();
				$(".monthly").show();
				$("#headerChange").text("<?php echo Weeklymachineanalysis; ?>");
			}
			else if (value == "yearly") 
			{
				$(".day").hide();	
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").show();
				$("#headerChange").text("<?php echo Monthlymachineanalysis; ?>");
			}
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		function changeFilterValue()
		{
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		$(".slick-track").click(function() 
		{
			console.log('hello')
		});



		function reloadGraph(machineId) 
		{
			$('.graph-loader').addClass("show").removeClass("hide");
			var choose1 = $("#choose1").val(); 
			if (choose1 == "day") 
			{
				var choose2 = $("#choose2").val(); 
				var displayTooltip = "<?php echo seconds; ?>";
				$(".filterSelected").text(choose2);
			} 
			else if (choose1 == "weekly") 
			{
				var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
				var displayTooltip = "<?php echo hours; ?>";
				$(".filterSelected").text('<?php echo week; ?> '+$("#choose3").val());
			}
			else if (choose1 == "monthly") 
			{
				var choose2 = $("#choose4").val(); 
				var displayTooltip = "<?php echo hours; ?>";
				$(".filterSelected").text(choose2);
			}
			else if (choose1 == "yearly") 
			{
				var choose2 = $("#choose5").val(); 
				var displayTooltip = "<?php echo day; ?>";
				$(".filterSelected").text(choose2);
			}

			$("#mainChoose1").val(choose1);
			$("#mainChoose2").val(choose2);
			$("#mainMachineId").val(machineId);


			var viewId = $("#viewId").val(); 

			$("#choose1").prop("disabled",true);
			$("#choose2").prop("disabled",true);
			$("#choose3").prop("disabled",true);
			$("#choose4").prop("disabled",true);
			$("#choose5").prop("disabled",true);
			$(".machineNameText").prop("disabled",true);
			$("#viewFilter").css('pointer-events','none');
			$("#viewFilter").css('opacity','0.4');
			$.post('<?php echo base_url();?>Analytics/parts_analytics_pagination',{machineId:machineId,choose1:choose1,choose2:choose2,viewId:viewId}, function getattribute(returnData)
			{
				$("#choose1").prop("disabled",false);
				$("#choose2").prop("disabled",false);
				$("#choose3").prop("disabled",false);
				$("#choose4").prop("disabled",false);
				$("#choose5").prop("disabled",false);
				$(".machineNameText").prop("disabled",false);
				$("#viewFilter").css('pointer-events','fill');
				$("#viewFilter").css('opacity','1');
				$('.graph-loader').addClass("hide").removeClass("show");
				returnData = jQuery.parseJSON(returnData);

				var isDataGet = returnData.isDataGet;

				

				$("#partCount").text(returnData.partCount);

				$("#ordersData").html(returnData.ordersHtml);

				$("#goodPartsCount").html( (returnData.goodParts > 0) ? returnData.goodParts.toFixed(0) + '&nbsp' : 0 + '&nbsp');
				$("#scrapPartsCount").html( (returnData.scrapParts > 0) ? returnData.scrapParts + '&nbsp' : 0 + '&nbsp');

				var dayThrouputActualTextPer =  Math.floor(returnData.machinePartsData.dayThrouputActualText);
				var dayThrouputMaxTextPer =  Math.round(returnData.machinePartsData.dayThrouputMaxText);	
				$("#dayThrouputActualText").text(dayThrouputActualTextPer);
				$("#dayThrouputMaxText").text(dayThrouputMaxTextPer);

				if (dayThrouputActualTextPer != 0 && dayThrouputMaxTextPer != 0) 
				{
					var gaugePer = (dayThrouputActualTextPer * 100) / dayThrouputMaxTextPer;
				}else
				{
					var gaugePer = 0;
				}

				if (gaugePer >= 0 && gaugePer <= 33) 
				{
						var gaugeColor = "#f60100";
						var gaugeText = "<?php echo Productionispoorneedtospeedup; ?>.";
				}else if(gaugePer > 33 && gaugePer <= 66)
				{
						var gaugeColor = "#FFCF00";
						var gaugeText = "<?php echo Productionisgoodbutcanbeimproved; ?>.";
				}else
				{
						var gaugeColor = "#76BA1B";
						var gaugeText = "<?php echo Productionisexceptionalkeepitup; ?>.";
				}

				$("#dayThrouputActualText").css("color",gaugeColor);

				$("#gaugeText").text(gaugeText);

				var dayThrouputMaxText = Math.round(returnData.machinePartsData.dayThrouputMaxText);

				if (dayThrouputMaxText == 0) 
				{
					var dayThrouputMaxText = 8;
				}

				var DailyGraph6 = echarts.init(document.getElementById('DailyGraph6'));  
				DailyGraph6.setOption(

				option = {

					series: [{
				        type: 'gauge',
				        max: dayThrouputMaxText,
				        progress: {
				            show: true,
				            width: 10,
				            roundCap: true,
				            itemStyle:
				            {
				                color:gaugeColor,
				            }
				        },
				        splitNumber: 8,
				        axisLine: {
				        		roundCap: true,
				            lineStyle: {
				                width: 10
				            }
				        },
				        axisTick: {
				            show: false
				        },
				        splitLine: {
				        	distance: 0,
			            lineStyle: {
			                width: 2,
			                color: '#999'
			            }
				        },
				        axisLabel: {
				            distance: 15,
				            color: '#999',
				            fontSize: 10,
				            formatter: function (params) {
		                            return Math.ceil(params);
		                        }
				        },
				        anchor: {
				            show: true,
				            showAbove: true,
				            size: 10,
				            itemStyle: {
				            	borderColor: gaugeColor, 
				                borderWidth: 0
				            }
				        },
				        itemStyle: {
			                color:gaugeColor,
			                borderColor: gaugeColor
			            },
				        title: {
				            show: false
				        },
				        detail: {
				            valueAnimation: true,
				            fontSize: 0,
				            offsetCenter: [0, '85%'],
				            formatter: '',
				        },
				        data: [{
				            value: Math.floor(returnData.machinePartsData.dayThrouputActualText)
				        }]
				    }]

				}

				, true);



				var DailyGraph7 = echarts.init(document.getElementById('DailyGraph7'));  
				DailyGraph7.setOption(

				option = {
				    tooltip: {
				        trigger: 'axis',
				        axisPointer: {           
				            type: 'shadow'        
				        },
				        formatter:   
							function (params) {
								var partsProduced = params[0].value;
								var opportunityGap = removeFirstChar(params[1].value);
								return "<?php echo Partsestimated; ?> :  "+Math.round(partsProduced)+" <br/> <?php echo Opportunitygap; ?> : "+Math.round(opportunityGap);
							}
						},
				    grid: {
				        left: '2%',
				        right: '15%',
				        bottom: '1%',
				        top: '2%',
				        containLabel: true
				    },
				    xAxis: [
				        {
				            type: 'value',
				             axisLine: {
					            show: false
					        },
							splitLine: {
					            lineStyle: {
					                type: 'dashed'
					            }
					        },
					        axisTick: {
					            show: false
					        },
					        axisLabel: {
					        	formatter: function (params) {
		                            return removeFirstChar(params);
		                        }	
					        }
					       
				        }
				    ],
				    yAxis: [
				        {
				            type: 'category',
				            axisLine: {
					            show: false
					        },
							splitLine: {
					            lineStyle: {
					                type: 'dotted'
					            }
					        },
					        axisTick: {
					            show: false
					        },
					        axisLabel:
				            {
				                fontSize: 9,
				            },
				            data: returnData.dateDataArr.reverse()
				        }
				    ],
				    series: [
				        {
				            name: '<?php echo Partsestimated; ?>',
				            type: 'bar',
				            stack: 'Total',
				            emphasis: {
				                focus: 'series',
				            },
				            color: '#002060',
				            data: returnData.dayPartsProduceArr.reverse()
				        },
				        {
				            name: '<?php echo Opportunitygap; ?>',
				            type: 'bar',
				            stack: 'Total',
				            emphasis: {
				                focus: 'series',
				            },
				            color: '#ff8000',
				            data: returnData.dayOpprtunityGapArr.reverse()
				        }
				    ]
				}

				, true);


				var symbols = [
				    'path://M68.1,217.7H37.3A7.34,7.34,0,0,0,30,225H75.4A7.34,7.34,0,0,0,68.1,217.7Zm158.8,0H196.1a7.34,7.34,0,0,0-7.3,7.3h45.4a7.34,7.34,0,0,0-7.3-7.3Zm-48.1-47.9a2.9,2.9,0,0,0-2.9-2.9h-.2l-4.7-2,2.1,4.7v.2a2.9,2.9,0,0,0,2.9,2.9,2.82,2.82,0,0,0,2.8-2.9Zm27.2-2.9h-.2l-4.7-2.1,2,4.7v.2a2.9,2.9,0,1,0,2.9-2.8Zm58.2-36.2c0-9.6-8.3-17.3-18.5-17.3h-1.6v-56l-7.4-5.9V0H27.5V53.4l-7.4,5.9v54.1H18.5C8.3,113.3,0,121.1,0,130.7c0,8.6,6.7,15.7,15.4,17.1h-.1v66.3H248.8V147.8h-.1C257.5,146.3,264.2,139.2,264.2,130.7Zm-18.6,14H18.5c-8.4,0-15.2-6.3-15.2-14s6.8-14,15.2-14H245.6c8.4,0,15.2,6.3,15.2,14s-6.7,14-15.2,14ZM152.5,52.5V45.9h71.8V59.2H142V52.5ZM141.9,67.7h82.3v6.6H141.9Zm-19.7,6.6H39.9V67.5h82.3ZM39.9,82.8h82.3v7.3h7.1V98h-6.2v3.8l3.1,3.7h11.6l3.1-3.7V98h-6.1V90.1h7.1V82.8h82.3v30.5H39.9ZM224.2,18V37.1H152.4V30.5H111.6v.3H90.1a10.63,10.63,0,0,0-10.5,8.8,11,11,0,0,0-.2,1.8,11,11,0,0,0,.2,1.8A10.63,10.63,0,0,0,90.1,52h21.5v.5h10.6V59H39.9V18ZM111.6,43.2H91.9a1.86,1.86,0,1,1-.3-3.7h20Zm58.2,120.4a8.7,8.7,0,1,1,0,12.3A8.69,8.69,0,0,1,169.8,163.6Zm42.3,12.3a8.7,8.7,0,1,1,0-12.3,8.69,8.69,0,0,1,0,12.3ZM43.7,200.7H32.4V161.1H43.7Zm22.8,0H55.2V161.1H66.5Zm22.9,0H78.1V161.1H89.4ZM18.6,128.8a1.8,1.8,0,1,0,1.8,1.8h0A1.86,1.86,0,0,0,18.6,128.8Zm0-8a9.9,9.9,0,1,0,9.9,9.9,9.86,9.86,0,0,0-9.9-9.9Zm3.5,3.8a1.84,1.84,0,1,1,0,2.6A1.87,1.87,0,0,1,22.1,124.6Zm-3.5-2.5a1.8,1.8,0,1,1-1.8,1.8h0a1.63,1.63,0,0,1,1.8-1.8Zm-6.1,2.5a1.84,1.84,0,1,1,0,2.6h0A1.87,1.87,0,0,1,12.5,124.6ZM10,130.7a1.8,1.8,0,1,1,1.8,1.8h0a1.79,1.79,0,0,1-1.8-1.8Zm5.1,6.1a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm3.5,2.5a1.8,1.8,0,1,1,1.8-1.8,1.79,1.79,0,0,1-1.8,1.8Zm0-5a3.7,3.7,0,1,1,0-7.4,3.63,3.63,0,0,1,3.7,3.7h0a3.69,3.69,0,0,1-3.7,3.7Zm6.1,2.5a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm2.5-6.1a1.8,1.8,0,1,1-1.8-1.8,1.79,1.79,0,0,1,1.8,1.8Zm48.3-1.9a1.8,1.8,0,1,0,1.8,1.8h0A1.86,1.86,0,0,0,75.5,128.8Zm0-8a9.9,9.9,0,1,0,9.9,9.9,9.86,9.86,0,0,0-9.9-9.9Zm3.5,3.8a1.84,1.84,0,1,1,0,2.6,1.87,1.87,0,0,1,0-2.6Zm-3.5-2.5a1.8,1.8,0,1,1-1.8,1.8h0A1.79,1.79,0,0,1,75.5,122.1Zm-6.1,2.5a1.84,1.84,0,1,1,0,2.6A1.87,1.87,0,0,1,69.4,124.6Zm-2.5,6.1a1.8,1.8,0,1,1,1.8,1.8h0A1.79,1.79,0,0,1,66.9,130.7Zm5.1,6.1a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm3.5,2.5a1.8,1.8,0,1,1,1.8-1.8,1.79,1.79,0,0,1-1.8,1.8Zm0-5a3.69,3.69,0,0,1-3.7-3.7,3.63,3.63,0,0,1,3.7-3.7,3.69,3.69,0,0,1,3.7,3.7h0a3.69,3.69,0,0,1-3.7,3.7Zm6.1,2.5a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm2.5-6.1a1.8,1.8,0,1,1-1.8-1.8h0a1.68,1.68,0,0,1,1.8,1.8Zm48.4-9.9a9.9,9.9,0,1,0,9.9,9.9h0A9.92,9.92,0,0,0,132.5,120.8Zm3.5,3.8a1.84,1.84,0,1,1,0,2.6,1.87,1.87,0,0,1,0-2.6Zm-3.5-2.5a1.8,1.8,0,1,1-1.8,1.8h0a1.63,1.63,0,0,1,1.8-1.8Zm-6.1,2.5a1.84,1.84,0,1,1,0,2.6h0a1.71,1.71,0,0,1,0-2.6Zm-2.6,6.1a1.8,1.8,0,1,1,1.8,1.8h0a1.86,1.86,0,0,1-1.8-1.8Zm5.2,6.1a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm3.5,2.5a1.8,1.8,0,1,1,1.8-1.8,1.79,1.79,0,0,1-1.8,1.8Zm0-5a3.7,3.7,0,1,1,3.7-3.7h0a3.76,3.76,0,0,1-3.7,3.7Zm6.1,2.5a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm2.5-6.1a1.8,1.8,0,1,1-1.8-1.8,1.68,1.68,0,0,1,1.8,1.8Zm-8.6-1.9a1.8,1.8,0,1,0,1.8,1.8h0A1.86,1.86,0,0,0,132.5,128.8Zm56.9,0a1.8,1.8,0,1,0,1.8,1.8h0A1.86,1.86,0,0,0,189.4,128.8Zm0-8a9.9,9.9,0,1,0,9.9,9.9h0A9.86,9.86,0,0,0,189.4,120.8Zm3.5,3.8a1.84,1.84,0,1,1,0,2.6A1.87,1.87,0,0,1,192.9,124.6Zm-3.5-2.5a1.8,1.8,0,1,1-1.8,1.8h0a1.79,1.79,0,0,1,1.8-1.8Zm-6.1,2.5a1.84,1.84,0,1,1,0,2.6A1.87,1.87,0,0,1,183.3,124.6Zm-2.5,6.1a1.8,1.8,0,1,1,1.8,1.8h0A1.79,1.79,0,0,1,180.8,130.7Zm5.1,6.1a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm3.5,2.5a1.8,1.8,0,1,1,1.8-1.8,1.79,1.79,0,0,1-1.8,1.8Zm0-5a3.7,3.7,0,1,1,3.7-3.7h0A3.69,3.69,0,0,1,189.4,134.3Zm6.1,2.5a1.84,1.84,0,1,1,0-2.6,1.87,1.87,0,0,1,0,2.6Zm2.5-6.1a1.8,1.8,0,1,1-1.8-1.8h0a1.73,1.73,0,0,1,1.8,1.8Zm48.3-1.9a1.8,1.8,0,1,0,1.8,1.8h0A1.73,1.73,0,0,0,246.3,128.8Zm0-8a9.9,9.9,0,1,0,9.9,9.9h0A9.86,9.86,0,0,0,246.3,120.8Zm3.5,3.8a1.84,1.84,0,1,1,0,2.6h0A1.87,1.87,0,0,1,249.8,124.6Zm-3.5-2.5a1.8,1.8,0,1,1-1.8,1.8h0a1.79,1.79,0,0,1,1.8-1.8Zm-6.1,2.5a1.84,1.84,0,1,1,0,2.6A1.87,1.87,0,0,1,240.2,124.6Zm-2.5,6.1a1.8,1.8,0,1,1,1.8,1.8,1.79,1.79,0,0,1-1.8-1.8Zm5.1,6.1a1.84,1.84,0,1,1,0-2.6,1.71,1.71,0,0,1,0,2.6Zm3.5,2.5a1.8,1.8,0,1,1,1.8-1.8,1.68,1.68,0,0,1-1.8,1.8Zm0-5a3.7,3.7,0,1,1,3.7-3.7h0a3.63,3.63,0,0,1-3.7,3.7Zm6.1,2.5a1.84,1.84,0,1,1,0-2.6,1.71,1.71,0,0,1,0,2.6Zm2.6-6.1a1.8,1.8,0,1,1-1.8-1.8h0a1.73,1.73,0,0,1,1.8,1.8Z'
				];

				var bodyMax = 100;

				var markLineSetting = {
				    symbol: 'none',
				    lineStyle: {
				        opacity: 1
				    },
				    data: [{
				        type: 'max',
				        label: {
				        	color: '#124D8D',
				        	// fontStyle: "bold",
				            formatter: '{c}%\n<?php echo PRODUCTIVITY; ?>'
				        }
				    }]
				};

				var DailyGraph8 = echarts.init(document.getElementById('DailyGraph8'));  
				DailyGraph8.setOption(

					option = {
							    tooltip: {
							    	trigger: 'axis',
							        axisPointer: {           
							            type: 'shadow'        
							        },
							        formatter:   
										function (params) {
											return '<?php echo PRODUCTIVITY; ?> : '+returnData.machinePartsData.dayProductivityText+'%';
										}
							    },
							    legend: {
							        data: [],
							        selectedMode: 'single'
							    },
							    xAxis: {
							        data: [''],
							        axisTick: {show: false},
							        axisLine: {show: false},
							        axisLabel: {show: false}
							    },
							    yAxis: {
							        max: bodyMax,
							        offset: 20,
							        splitLine: {show: false},
							        axisTick: {show: false},
							        axisLine: {show: false},
							        axisLabel: {show: false}
							    },
							    grid: {
							        top: 'center',
							        // left:'center',
							        height: '60%',
							        width: '68%',
							        right: '3%',
							        left: '3%'
							    },
							    markLine: {
							        z: -100,
							    },
							    series: [{
							        name: '<?php echo PRODUCTIVITY; ?>',
							        type: 'pictorialBar',
							        symbolClip: true,
							        color: '#124D8D',
							        symbolBoundingData: bodyMax,
							        label: false,
							        data: [{
							            value: returnData.machinePartsData.dayProductivityText,
							            symbol: symbols[0]
							        }],
							        markLine: markLineSetting,
							        z: 10
							    },{
							        name: '<?php echo PRODUCTIVITY; ?>',
							        type: 'pictorialBar',
							        symbolBoundingData: bodyMax,
							        animationDuration: 0,
							        itemStyle: {
							            color: '#ccc'
							        },
							        data: [{
							            value: 0,
							            symbol: symbols[0]
							        }]
							    }]
							}

				, true);

				if (isDataGet != "4") 
				{
					$("#partsProducedGraph").hide();
					$("#productivityGraph").show();
				}else
				{
					$("#productivityGraph").hide();
					$("#partsProducedGraph").show();
				}

				
				});
			}




		$(document).ready(function() 
		{ 

			$('.datepicker').datepicker({
					format: "mm/dd/yyyy",
					endDate: "today",
					language: "<?php echo datepickerLanguage; ?>",
					autoclose: true
				});
			$('.datepicker1').datepicker({
					format: "M yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
				    language: "<?php echo datepickerLanguage; ?>",
				    autoclose: true
				});

			$(".picker-minutes").attr('data-type','');

			$(".regular").slick({
		        dots: false,
		        infinite: false,
		        slidesToShow: 4,
		        slidesToScroll: 4,
		          responsive: [
			        {
			          breakpoint: 1024,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 3,
			            slidesToScroll: 3
			          }
			        },
			        {
			          breakpoint: 600,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 2,
			            slidesToScroll: 2
			          }
			        },
			        {
			          breakpoint: 480,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 1,
			            slidesToScroll: 1
			          }
			        }
			      ],

		      });

			$(".filter").click(function(e) 
			{
				e.preventDefault();
				var clickedId = $(this).attr('id');
				var machineId = clickedId.replace('machine',''); 
				$('#machineId').val(machineId);
				$(".filter").removeClass('active');
				$(this).addClass('active');
				reloadGraph(machineId);
			});

			$(window).on('resize orientationChange', function(event) { //check window.width()... 
				$('.regular').slick('init');
				$('.regular').slick('reinit');
				$(".filter").click(function(e) {
					e.preventDefault();
					var clickedId = $(this).attr('id');
					var machineId = clickedId.replace('machine',''); 
					$('#machineId').val(machineId);
					$(".filter").removeClass('active');
					$(this).addClass('active');
					reloadGraph(machineId);
				});
			});
		});


		

		function in_array(needle, haystack)
		{
		    var found = 0;
		    for (var i=0, len=haystack.length;i<len;i++) 
		    {
		        if (haystack[i] == needle) return i;
		            found++;
		    }
		    return -1;
		}

		function selectStopTime(selectedBox)
		{
			var stopTimes = $("#stopTimes").val().split(",");
			if (in_array(selectedBox,stopTimes)!= -1) 
			{
				$("#selectStopTime"+selectedBox).css("opacity","0.5");
				for(var i = stopTimes.length - 1; i >= 0; i--) {
					if(stopTimes[i] == selectedBox) {
				        stopTimes.splice(i, 1);
				    }
				}
				
				var selectedselectedBox = stopTimes.join(",");
				$("#stopTimes").val(selectedselectedBox);

			}
			else
			{   
				var checkstopTimes = $("#stopTimes").val();
				if (checkstopTimes != "") 
				{
					var selectedselectedBox = $("#stopTimes").val()+','+selectedBox;
					$("#stopTimes").val(selectedselectedBox);
				}
				else
				{
					$("#stopTimes").val(selectedBox);
				}
				$("#selectStopTime"+selectedBox).css("opacity","1");
			}
		
		}
	</script>
  	<script type="text/javascript">
  	new Picker(document.querySelector('.js-inline-picker-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
      mindate: new Date(0, 0, 0, 1, 0),
      maxdate: new Date(0, 0, 0, 10, 0),
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});

  	new Picker(document.querySelector('.js-inline-picker-break-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-break-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});

 /* 	new Picker(document.querySelector('.js-inline-picker-edit-break-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-edit-break-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});*/


  	$('form#add_view_form').submit(function(e) 
	{ 
		var startTimeHour =  $(".js-inline-picker-start .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-start .picker-minutes .picker-picked").attr('data-value');

		var stopTimeHour =  $(".js-inline-picker-stop .picker-hours .picker-picked").attr('data-value');
		var stopTimeMinutes =  $(".js-inline-picker-stop .picker-minutes .picker-picked").attr('data-value');

		var form = $(this);
	    e.preventDefault();
		if (Number(startTimeHour) >= Number(stopTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else
		{	
	   		$("#add_view_submit").attr('disabled',true); 
			$("#add_view_submit").html('Saving...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/add_view'); ?>",
				data: {'formData': form.MytoJson(),'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': stopTimeHour+':'+stopTimeMinutes},  
				dataType: "html",
				success: function(data)
				{
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#add_view_submit").attr('disabled',false); 
						$("#add_view_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add')
					}
				},
				error: function() { 
					$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding task'
						});
					$("#add_view_submit").attr('disabled',false);
					$("#add_view_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add')
				}
		   });
		}
    }); 

    function addBreak()
    {
    	$(".submitButton").hide();
    	$(".addBreakButton").hide();
    	$(".removeBreak").fadeIn();
    }
	function removeBreak()
    {
    	$(".removeBreak").hide();
    	$(".submitButton").fadeIn();
    	$(".addBreakButton").fadeIn();
    }

    var i = 1;
    function saveBreaks()
    {
    	var startTimeHourAdd =  $(".js-inline-picker-start .picker-hours .picker-picked").attr('data-value');
		var stopTimeHourAdd =  $(".js-inline-picker-stop .picker-hours .picker-picked").attr('data-value');

    	var startTimeHour =  $(".js-inline-picker-break-start .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-break-start .picker-minutes .picker-picked").attr('data-value');

		var endTimeHour =  $(".js-inline-picker-break-stop .picker-hours .picker-picked").attr('data-value');
		var endTimeMinutes =  $(".js-inline-picker-break-stop .picker-minutes .picker-picked").attr('data-value');

		if (Number(startTimeHour) >= Number(endTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else if(Number(startTimeHourAdd) <= Number(startTimeHour) && Number(stopTimeHourAdd) >= Number(endTimeHour)) 
		{

			var html = '<div class="row" id="breakRow'+i+'"><div class="col-md-10"><span style="font-size: 16px;"><?php echo Breakstartsfrom; ?> </span><span style="color: #FF8000;font-size: 16px;">'+startTimeHour+':'+startTimeMinutes+'</span><span style="font-size: 16px;">&nbsp;&nbsp;to&nbsp;&nbsp;</span><span style="color: #FF8000;font-size: 16px;">'+endTimeHour+':'+endTimeMinutes+'</span></div><div class="col-md-2"><img onclick="removeBreakDataAdd('+i+')" style="width: 20px;" src="<?php echo base_url('assets/img/cross.svg'); ?>"></div><input type="hidden" name="breakStartTimes[]" value="'+startTimeHour+':'+startTimeMinutes+'"><input type="hidden" name="breakEndTimes[]" value="'+endTimeHour+':'+endTimeMinutes+'"></div>';

			$(".addBreakData").append(html);
			$(".removeBreak").hide();
	    	$(".addBreakData").fadeIn();
	    	$(".submitButton").fadeIn();
	    	$(".addBreakButton").fadeIn();
	    }
	    else
	    {
			alert("<?php echo Pleasechoosevalidduration; ?>");
	    }

    }


    function removeBreakDataAdd(breakViewId)
    {
    	$("#breakRow"+breakViewId).remove();
    }
    function removeBreakData(breakViewId)
    {
    	$('.graph-loader').addClass("show").removeClass("hide");
    	$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/removeBreakTime'); ?>", 
			data: {breakViewId:breakViewId}, 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$('.graph-loader').addClass("hide").removeClass("show");
					$("#breakRowEdit"+breakViewId).remove();
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
				}
			},
			error: function() { 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: '<?php echo Errorwhileupdatingmachine; ?>.'
				});
				location.reload();
			}
	   });
	}


    function editView(viewId)
    {
    	$('.graph-loader').addClass("show").removeClass("hide");
    	$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/getViewDataById'); ?>", 
			data: {viewId:viewId}, 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$('.graph-loader').addClass("hide").removeClass("show");
					new Picker(document.querySelector('.js-inline-picker-edit-start-dynamic'+viewId), 
					{
				      format: 'HH:mm',
				      date: new Date(0, 0, 0, obj.result.startTimeHour, 0),
					  controls: true,
					  inline: true,
					});

				    new Picker(document.querySelector('.js-inline-picker-edit-stop-dynamic'+viewId), 
				    {
				      format: 'AA:mm',
				      date: new Date(0, 0, 0, obj.result.stopTimeHour, 0),
					  controls: true,
					  inline: true,
				  	}); 

				  	new Picker(document.querySelector('.js-inline-picker-edit-break-start-dynamic'+viewId), 
				  	{
				      format: 'HH:mm',
				      date: new Date(0, 0, 0, 1, 0),
					  controls: true,
					  inline: true,
					});

				    new Picker(document.querySelector('.js-inline-picker-edit-break-stop-dynamic'+viewId), 
				    {
				      format: 'AA:mm',
				      date: new Date(0, 0, 0, 1, 0),
					  controls: true,
					  inline: true,
				  	});

				  	
				  	$(".picker-minutes").attr('data-type','');
					$("#deleteView").attr("onclick","deleteView("+viewId+")");
					$("#viewIdEdit-dynamic"+viewId).val(obj.result.viewId);
					$("#viewNameEdit-dynamic"+viewId).val(obj.result.viewName);
					$(".addEditBreakData-dynamic"+viewId).html(obj.result.breakData);
    				$("#edit-view-modal-dynamic"+viewId).modal();
    				$(".addEditBreakData-dynamic"+viewId).fadeIn();
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
				}
			},
			error: function() { 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: 'Error while updating machine.'
				});
				location.reload();
			}
	   });
    }


    function deleteView(viewId)
    {
    	$("#deleteViewId").val(viewId);
    	$("#delete-view-modal").modal();
    }


    $("form[id^='delete_view_form']").submit(function(e) 
    {  
		var form = $(this);
		e.preventDefault();
		
		$("#delete_view_submit").attr('disabled',true); 
		$("#delete_view_submit").html('Deleting...'); 
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/delete_view'); ?>", 
			data: form.serialize(), 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$.gritter.add({
						title: '<?php echo Success; ?>',
						text: obj.message
					});
					window.setTimeout(function(){location.reload()},3000) 
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
					$("#delete_view_submit").attr('disabled',false); 
					$("#delete_view_submit").html('Delete')
				}
			},
			error: function() 
			{ 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: 'Error while updating machine.'
				});
				location.reload();
			}
	   });
	}); 

	$("form[id^='edit_view_form']").submit(function(e) 
	{  

		var form = $(this);
		e.preventDefault();
		var formId = form.attr('id');
		var viewId = formId.replace('edit_view_form',''); 

		var startTimeHour =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');
		var stopTimeHour =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var stopTimeMinutes =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');

		if (Number(startTimeHour) >= Number(stopTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else
		{
			var viewId = $("#viewIdEdit-dynamic"+viewId).val(); 
			var viewName = $("#viewNameEdit-dynamic"+viewId).val(); 
			
			$("#edit_view_submit").attr('disabled',true); 
			$("#edit_view_submit").html('Saving...'); 
			
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/edit_view'); ?>", 
				data: {'viewId' : viewId,'viewName' : viewName,'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': stopTimeHour+':'+stopTimeMinutes}, 
				dataType: "html",
				success: function(data){
					
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#edit_view_submit").attr('disabled',false); 
						$("#edit_view_submit").html('Save')
					}
				},
				error: function() 
				{ 
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: 'Error while updating machine.'
					});
					location.reload();
				}
		   });
		}
	}); 

	function addEditBreak(viewId)
    {
    	$(".submitEditButton-dynamic"+viewId).hide();
    	$(".addEditBreakButton-dynamic"+viewId).hide();
    	$(".removeEditBreak-dynamic"+viewId).fadeIn();
    }

    function removeEditBreak(viewId)
    {
    	$(".removeEditBreak-dynamic"+viewId).hide();
    	$(".submitEditButton-dynamic"+viewId).fadeIn();
    	$(".addEditBreakButton-dynamic"+viewId).fadeIn();
    }

	var f = 1;
    function saveEditBreaks(viewId)
    {
		var startTimeHourEdit =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var stopTimeHourEdit =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeHour =  $(".js-inline-picker-edit-break-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-edit-break-start-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');
		var endTimeHour =  $(".js-inline-picker-edit-break-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var endTimeMinutes =  $(".js-inline-picker-edit-break-stop-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');

		//alert(startTimeHourEdit)

		if (Number(startTimeHour) >= Number(endTimeHour)) 
		{
			alert("<?php echo Pleasechoosevalidduration; ?>");
		}
		else if(Number(startTimeHourEdit) <= Number(startTimeHour) && Number(stopTimeHourEdit) >= Number(endTimeHour)) 
		{
			$('.graph-loader').addClass("show").removeClass("hide");
	    	$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/addBreakTime'); ?>", 
				data: {'viewId' : viewId,'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': endTimeHour+':'+endTimeMinutes}, 
				dataType: "html",
				success: function(data){
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$('.graph-loader').addClass("hide").removeClass("show");
						var html = '<div class="row" id="breakRowEdit'+obj.breakViewId+'"><div class="col-md-10"><span style="font-size: 16px;"><?php echo Breakstartsfrom; ?> </span><span style="color: #FF8000;font-size: 16px;">'+startTimeHour+':'+startTimeMinutes+'</span><span style="font-size: 16px;">&nbsp;&nbsp;to&nbsp;&nbsp;</span><span style="color: #FF8000;font-size: 16px;">'+endTimeHour+':'+endTimeMinutes+'</span></div><div class="col-md-2"><img onclick="removeBreakData('+obj.breakViewId+')" style="width: 20px;" src="<?php echo base_url('assets/img/cross.svg'); ?>"></div></div>';

						$(".addEditBreakData-dynamic"+viewId).append(html);
						$(".removeEditBreak-dynamic"+viewId).hide();
				    	$(".addEditBreakData-dynamic"+viewId).fadeIn();
				    	$(".submitEditButton-dynamic"+viewId).fadeIn();
				    	$(".addEditBreakButton-dynamic"+viewId).fadeIn();
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
					}
				},
				error: function() 
				{ 
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: 'Error while updating machine.'
					});
					location.reload();
				}
		   });
		}
		else
		{
			alert("Please choose valid duration");
	    }
		

    }
</script>

