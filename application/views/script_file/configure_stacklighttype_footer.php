<script>   
   
        $(document).ready(function() 
        {
            $(".stackLightTypeMachineSel").select2({ placeholder: "<?php echo Selectmachines; ?>" }); 
            $("select.stackLightTypeVersion").select2({ 
              tags: true
            }); 
        }); 

        
        $(document).ready(function() 
        {

        $("form[id^='update_stacklight_machine']").submit(function(e) 
        {
            var form = $(this);
            e.preventDefault();
            var formId = form.attr('id');
            var stackLightTypeId = formId.replace('update_stacklight_machine',''); 
            $("[id^='update_stacklight_machine_submit"+stackLightTypeId+"']").html('<?php echo Updating; ?>...')
            
            var err_div = "[id^='assign_form_error"+stackLightTypeId+"']";
            var suc_div = "[id^='assign_form_success"+stackLightTypeId+"']";
            
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('admin/update_stacklight_machine'); ?>",
                data: form.MytoJson(),  
                dataType: "html",
                success: function(data){
                    
                    if($.trim(data) == "<?php echo Stacklighttypedetailsupdatedsuccessfully; ?>") 
                    {   
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: data
                        })
                        location.reload();
                    } 
                    else 
                    { 
                        $("[id^='update_stacklight_machine_submit"+stackLightTypeId+"']").html('<?php echo Update;?>')
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: data
                        }); 
                    }
                },
                error: function() { 
                    alert("<?php echo Errorwhileupdatinguser; ?>.");
                }
           });
        }); 
        
        $('form#add_stackLightType_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#add_stackLightType_submit").html('<?php echo Saving; ?>...')
            var formData = new FormData(this);
            var classification_cfg = $("#classification_cfg").val();
            var classification_cfg_extension = classification_cfg.replace(/^.*\./, '');
            var classification_weights = $("#classification_weights").val();
            var classification_weights_extension = classification_weights.replace(/^.*\./, '');
            var detection_cfg = $("#detection_cfg").val();
            var detection_cfg_extension = detection_cfg.replace(/^.*\./, '');
            var detection_weights = $("#detection_weights").val();
            var detection_weights_extension = detection_weights.replace(/^.*\./, '');
            var label_txt = $("#label_txt").val();
            var label_txt_extension = label_txt.replace(/^.*\./, '');
            var labels_txt = $("#labels_txt").val();
            var labels_txt_extension = labels_txt.replace(/^.*\./, '');
            if (classification_cfg_extension != "cfg") 
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensioncfg; ?>"
                });
            }
            else if(classification_weights_extension != "weights")
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensionweights; ?>"
                });
            }
            else if(detection_cfg_extension != "cfg")
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensioncfg; ?>"
                });
            }
            else if(detection_weights_extension != "weights")
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensionweights; ?>"
                });
            }
            else if(label_txt_extension != "txt")
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensiontxt; ?>"
                });
            }
            else if(labels_txt_extension != "txt")
            {
                $("#add_stackLightType_submit").html('<?php echo save; ?>')
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Pleaseselectavalidfilewithextensiontxt; ?>"
                });
            }
            else{
                $.ajax({ 
                    type: 'POST',
                    url: "<?php echo site_url('admin/add_stacklighttype'); ?>",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    dataType: "html",
                    success: function(data){
                        if($.trim(data) == "<?php echo Newstacklighttypeaddedsuccessfully; ?>") 
                        { 
                            $("#add_stackLightType_error").html('');
                            $("#add_stackLightType_error").removeClass("show");
                            $("#add_stackLightType_error").addClass("hide");
                            $("#add_stackLightType_success").addClass("show");
                            $("#add_stackLightType_success").removeClass("hide");
                            $("#add_stackLightType_success").html(data);
                            form.each(function(){
                                this.reset();
                            }); 
                            $("#add_stackLightType_submit").html('<?php echo Saved; ?>...')
                            
                            $("#add_stackLightType_success").fadeOut(1500); 
                            $("#modal-add-stackLightType").modal('hide').fadeOut(1500); 
                            
                            $("#add_stackLightType_submit").html('Save')
                            window.setTimeout(function(){location.reload()},3000)
                            
                        } 
                        else 
                        { 
                            $("#add_stackLightType_error").addClass("show");
                            $("#add_stackLightType_error").removeClass("hide"); 
                            $("#add_stackLightType_error").html(data);
                            $("#add_stackLightType_submit").html('Save')
                        }
                        
                    },
                    error: function() { 
                        alert("<?php echo Errorwhileaddingstacklighttype; ?>.");
                        location.reload();
                    }
               });
            }
        });
            
            var handleDataTableColor = function() 
            {
                "use strict";
                if ($('#data-table-stackLightType').length !== 0) {
                     var table = $('#data-table-stackLightType').DataTable({
                        "pagingType": "input", 
                        responsive: true,
                    });
                }
            }; 
            
            var TableManageColor = function () 
            {
                "use strict";
                return {
                    init: function () {
                        handleDataTableColor();
                    }
                };
            }();
            
            TableManageColor.init();
            
            $("form[id^='edit_stackLightType_form']").submit(function(e) 
            {  
                var form = $(this);
                e.preventDefault();
                var formData = new FormData(this);
                var formId = form.attr('id');
                var stackLightTypeId = formId.replace('edit_stackLightType_form',''); 
                
                $("[id^='edit_stackLightType_submit"+stackLightTypeId+"']").html('<?php echo Saving; ?>...')
                
                var err_div = "[id^='edit_stackLightType_error"+stackLightTypeId+"']";
                var suc_div = "[id^='edit_stackLightType_success"+stackLightTypeId+"']";
                var modal_div = "[id^='modal-edit-stackLightType"+stackLightTypeId+"']";
                
                $.ajax({
                    type: 'POST',
                    url: "<?php echo site_url('admin/edit_stacklighttype'); ?>",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    dataType: "html",
                    success: function(data){
                        var obj = $.parseJSON($.trim(data));
                        //console.log(obj);
                        if(obj.status == 1) 
                        {
                            if (obj.updateStatus == 1) 
                            {
                                socket.emit('Update auto sync stacklight', obj); 
                            } 
                            $(err_div).html('');
                            $(err_div).removeClass("show");
                            $(err_div).addClass("hide");
                            
                            $(suc_div).addClass("show");
                            $(suc_div).removeClass("hide");
                            $(suc_div).html(obj.message);
                            
                            $("[id^='edit_stackLightType_submit"+stackLightTypeId+"']").html('Saved...')
                            
                            $(suc_div).fadeOut(1500);
                            $(modal_div).modal('hide').fadeOut(1500);
                            
                            $("[id^='edit_stackLightType_submit"+stackLightTypeId+"']").html('Save')
                            
                            window.setTimeout(function(){location.reload()},3000)
                        } 
                        else 
                        { 
                            $(err_div).addClass("show");
                            $(err_div).removeClass("hide"); 
                            $(err_div).html(obj.message);
                        }
                        
                    },
                    error: function() { 
                        alert("<?php echo Errorwhileupdatingstacklighttype; ?>.");
                        location.reload();
                    }
               });
            });
            
            $("form[id^='delete_stackLightType_form']").submit(function(e) 
            {  
                var form = $(this);
                e.preventDefault();
                var formId = form.attr('id');
                var stackLightTypeId = formId.replace('delete_stackLightType_form',''); 
                $("[id^='delete_stackLightType_submit"+stackLightTypeId+"']").html('Deleting...')
                var suc_div = "[id^='delete_stackLightType_success"+stackLightTypeId+"']";
                var modal_div = "[id^='modal-delete-stackLightType"+stackLightTypeId+"']";
                $.ajax({
                    type: "POST",
                    url: "<?php echo site_url('admin/delete_stacklighttype'); ?>",  
                    data: form.serialize(), 
                    dataType: "html",
                    success: function(data)
                    {
                        if($.trim(data) == "<?php echo StackLightTypedeletedsuccessfully; ?>") 
                        { 
                            $(suc_div).addClass("show");
                            $(suc_div).removeClass("hide");
                            $(suc_div).html(data);
                            $("[id^='delete_stackLightType_submit"+stackLightTypeId+"']").html('Deleted...')
                            $(suc_div).fadeOut(1500);
                            $(modal_div).modal('hide').fadeOut(1500);
                            $("[id^='delete_stackLightType_submit"+stackLightTypeId+"']").html('Delete')
                            window.setTimeout(function(){location.reload()},3000)
                        } 
                    },
                    error: function() { 
                        alert("<?php echo Errorwhiledeletingstacklighttype; ?>.");
                        location.reload();
                    }
               });
            
            });
        }); 
       function readURL(input) 
       {
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $('.file-upload-image').attr('src', e.target.result);
                $('.image-title').html(input.files[0].name);
            };
            reader.readAsDataURL(input.files[0]);
        }
    
    
    </script>