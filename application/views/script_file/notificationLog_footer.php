 <script>
    function closeDetails()
    {
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    $('.radioClick1').click(function(e) 
    {
    	$(".radioClick1").css("color","black");
    	$(this).css("color","#FF8000");
    });

    $('.radioClick2').click(function(e) 
    {
    	$(".radioClick2").css("color","black");
    	$(this).css("color","#FF8000");
    });

    $('.radioClick3').click(function(e) 
    {
    	$(".radioClick3").css("color","black");
    	$(this).css("color","#FF8000");
    });

    $('.radioClick4').click(function(e) 
    {
    	$(".radioClick4").css("color","black");
    	$(this).css("color","#FF8000");
    });

    $('form#update_reportProblem_form').submit(function(e) 
	{ 
        var form = $(this);
    	e.preventDefault();
        $("#update_reportProblem_submit").html('Saving...')
        var formData = new FormData(this);
	    $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('admin/update_reportProblem'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                	
                    $.gritter.add({
            			title: 'Success',
            			text: "Report problem updated successfully."
            		})
                    
                    $("#update_reportProblem_submit").html('Save')
                    window.setTimeout(function(){location.reload()},3000)
                },
                error: function() 
                { 
                    alert("Error while update report problem.");
                }
           });
    });

	$(document).ready(function() 
	{
		$('.datepicker56').datepicker({
			format: "yyyy-mm-dd",
			autoclose : true
		});
	
		$('table').on( 'click', 'tr', function () 
		{
		    problemId = $(this).attr('id');
			$.ajax({
			        url: "<?php echo base_url('Admin/getReportProblemById') ?>",
			        type: "post",
			        data: {problemId:problemId} ,
			        success: function (response) 
			        {
			        	var obj = $.parseJSON(response);
						userId = obj.userId;
			        	userName = obj.userName;
			        	date = obj.date;
			        	type = obj.type;
			        	description = obj.description;
			        	cause = obj.cause;
			        	improvement = obj.improvement;
			        	status = obj.status;
			        	userImage = "<?php echo base_url('assets/img/user/') ?>"+obj.userImage;

			        	$("#userId").text("UserID : "+userId);
			        	$("#userName").text(userName);
			        	$("#date").text(date);
			        	if (type == "1") 
			        	{
			        		type = "Accident";
			        	}
			        	else
			        	{
			        		type = "Incident";
			        	}
			        	$("#type").text(type);
			        	$("#description").val(description);
			        	$("#cause").val(cause);
			        	$("#improvement").val(improvement);
			        	$("#problemId").val(problemId);

			        	if (status == "1") 
			        	{
			        		$("#not_confidentials").hide();
			        		$("#confidentials").show();
			        	}
			        	else
			        	{
			        		$("#confidentials").hide();
			        		$("#not_confidentials").show();
			        	}
						$("#userImage").attr("src",userImage);
						$('tr').css("border-left", "none");
					 	$('tr').css("background", "#FFFFFF");

					 	$("#"+problemId).css("background", "#F2F2F2");
					 	$("#"+problemId).css("border-left", "7px solid #FF8000");
					 	$("#empTable").css("width", "100%");
					 	$(".col-sm-7").css("padding-left", "28%");
					 	$(".table_data").removeClass("col-md-12");
					 	$(".table_data").addClass("col-md-9");
					    $(".detail").show();
			        },
			        error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown);
			        }
			    });
		})

		$('#empTable').removeAttr('width').DataTable({
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  'ajax': {
			  'url':'notificationLog_pagination',
			  "type": "POST",
				"data":function(data) {
					data.notificationId = $('#notificationId').val();
				},  
		  },
		  'columns': [
			 { data: 'machineName' },
			 { data: 'notificationText' },
			 { data: 'createdDate' },
		  ],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			  	{
			      targets: 2,
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });
		
	}); 

	function filterStatus(value)
	{
		$("#filterStatus").val(value);
		$("#empTable").DataTable().ajax.reload();
		if (value == "confidential") 
		{
			value = "Yes";
		}
		else if(value == "not_confidential")
		{
			value = "No";
		}
		$("#filterStatusSelected").css('border','1px solid #FF8000');
		$("#filterStatusSelected").css('color','#FF8000');
	}

	function filterUserId(value)
	{
		$("#filterUserId").val(value);
		$("#empTable").DataTable().ajax.reload();
		$("#filterUserIdSelected").css('border','1px solid #FF8000');
		$("#filterUserIdSelected").css('color','#FF8000');
	}

	function filterUserName(value,name)
	{
		$("#filterUserName").val(value);
		$("#empTable").DataTable().ajax.reload();
		$("#filterUserNameSelected").css('border','1px solid #FF8000');
		$("#filterUserNameSelected").css('color','#FF8000');
	}

	function filterType(value)
	{
		$("#filterType").val(value);
		$("#empTable").DataTable().ajax.reload();
		if (value == "accident") 
		{
			value = "Accident";
		}
		else if(value == "incident")
		{
			value = "Incident";
		}
		$("#filterTypeSelected").css('border','1px solid #FF8000');
		$("#filterTypeSelected").css('color','#FF8000');
	}

	function filterDate(value) 
	{
		$("#filterDate").val(value);
		$(".datepicker56").val("");
		$(".datepicker56").removeClass("garyColor");
		$(".datepicker56").addClass("orangColor");
		$("#empTable").DataTable().ajax.reload();
		$("#filterDateSelected").css('border','1px solid #FF8000');
		$("#filterDateSelected").css('color','#FF8000');
		$("#dateArrow").html('<i style="margin-left: -17px;color:red;" onclick="reset_date();" class="fa fa-times" aria-hidden="true"></i>');
	}
	
	function reset_date()
	{
		$("#filterDateSelected").css('border','1px solid #F2F2F2');
		$("#filterDate").val("");
		$(".datepicker56").val("");
		$("#dateArrow").html('<i style="margin-left: -17px;" class="fa fa-caret-down" aria-hidden="true"></i>');
		$("#empTable").DataTable().ajax.reload();
		$(".datepicker56").removeClass("orangColor");
		$(".datepicker56").addClass("garyColor");
	}

	function reset_userId()
	{
		$("#filterUserId").val("");
		$("#filterUserIdSelectedValue").html("UserID&nbsp;");
		$("#empTable").DataTable().ajax.reload();
		$("#filterUserIdSelected").html('&nbsp;');
	}

	function reset_userName()
	{
		$("#filterUserName").val("");
		$("#filterUserNameSelectedValue").html("Operator&nbsp;");
		$("#empTable").DataTable().ajax.reload();
		$("#filterUserNameSelected").html('&nbsp;');
	}

	function reset_type()
	{
		$("#filterType").val("");
		$("#filterTypeSelectedValue").html("Type&nbsp;");
		$("#empTable").DataTable().ajax.reload();
		$("#filterTypeSelected").html('&nbsp;');
	}

	function reset_status()
	{
		$("#filterStatus").val("");
		$("#filterStatusSelectedValue").html("Confidential&nbsp;");
		$("#empTable").DataTable().ajax.reload();
		$("#filterStatusSelected").html('&nbsp;');
	}
    </script>