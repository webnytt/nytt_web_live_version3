 <script>
    function closeDetails()
    {
    	problemId = $("#problemId").val();
    	$("#"+problemId).removeClass("rowOrange");
    	$("#"+problemId).addClass("rowWhite");
    	$(".table_data").removeClass("col-md-9");
    	$(".table_data").addClass("col-md-12");
	    $(".detail").hide();
    }

    $('form#update_reportProblem_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#update_reportProblem_submit").html('<?php echo Saving; ?>...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('admin/update_reportProblem'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    $.gritter.add({
            			title: '<?php echo Success; ?>',
            			text: "<?php echo Reportproblemupdatedsuccessfully; ?>."
            		})
                    $("#update_reportProblem_submit").html('<?php echo Save; ?>')
                    window.setTimeout(function(){location.reload()},3000)
                },
                error: function() { 
                    alert("<?php echo Errorwhileupdatereportproblem; ?>.");
                }
           });
    });


	$(document).ready(function() 
	{
		$('.datepicker58').datepicker({
		format: "mm/dd/yyyy",
		autoclose : true,
        daysOfWeekDisabled: [0,6],
		});

		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			 ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 

		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Date ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckShowDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('table').on( 'click', 'tr', function () 
		{
		    problemId = $(this).attr('id');
		    if(problemId != "" && problemId != undefined)
		    {
				$.ajax({
			        url: "<?php echo base_url('Admin/getReportProblemById') ?>",
			        type: "post",
			        data: {problemId:problemId} ,
			        success: function (response) 
			        {
			        	var obj = $.parseJSON(response);
				    	reportedNotesCount = obj.reportedNotesCount;
				    	userId = obj.userId;
			        	userName = obj.userName;
			        	date = obj.date;
			        	type = obj.type;
			        	description = obj.description;
			        	cause = obj.cause;
			        	improvement = obj.improvement;
			        	status = obj.status;
			        	isActive = obj.isActive;

			        	if (reportedNotesCount == 0) 
			        	{
			        		$("#reportedNotesDots").hide();
			        	}

			        	userImage = "<?php echo base_url('assets/img/user/') ?>"+obj.userImage;
			        	picture = "<?php echo base_url('assets/img/reportProblem/') ?>"+obj.picture;

			        	$("#userId").text("<?php echo UserId; ?> : "+userId);
			        	$("#userName").text(userName);
			        	$("#date").text(date);
			        	$('#type [value=0]').attr('selected', false);
			        	$('#type [value=1]').attr('selected', false);
			        	$('#type [value=2]').attr('selected', false);
			        	$('#type [value='+type+']').attr('selected', true);
			        	$("#description").val(description);
			        	$("#cause").val(cause);
			        	$("#improvement").val(improvement);
			        	$("#problemId").val(problemId);
				    	$("#resolved").attr("checked",false);
			        	if (isActive == "1") 
			        	{
			        		$("#resolved").attr("checked",true);
			        	}

			        	if (status == "1") 
			        	{
			        		$("#not_confidentials").hide();
			        		$("#confidentials").show();
			        	}
			        	else
			        	{
			        		$("#confidentials").hide();
			        		$("#not_confidentials").show();
			        	}
						$("#userImage").attr("src",userImage);
						if (obj.picture != "") 
			        	{
			        		$("#pictureRow").show();
			        		$("#picture").attr("src",picture);
			        	}
			        	else
			        	{
			        		$("#pictureRow").hide();
			        	}

						$(".rowOrange").addClass("rowWhite");
					 	$(".rowOrange").removeClass("rowOrange");
					 	$("#"+problemId).removeClass("rowWhite");
					 	$("#"+problemId).removeClass("rowBLue");
						$("#"+problemId).addClass("rowOrange");
					 	$("#empTable").css("width", "100%");
					 	$(".col-sm-7").css("padding-left", "28%");
					 	$(".table_data").removeClass("col-md-12");
					 	$(".table_data").addClass("col-md-9");
					    $(".detail").show();
			           
			        },
			        error: function(jqXHR, textStatus, errorThrown) 
			        {
			           console.log(textStatus, errorThrown);
			        }
			    });
			}
		})

		$('#empTable').removeAttr('width').DataTable({
		  "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
          "first": "<?php echo First; ?>",
          "last": "<?php echo Last; ?>",
          "next": "<?php echo Next; ?>",
          "previous": "<?php echo Previous; ?>",
          "page": "<?php echo Page; ?>",
          "of": "<?php echo of; ?>"
        }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "dom": '<"top"i>rt<"bottom"flp><"clear">',
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'problemId',
		  'ajax': {
			  'url':'reportProblem_pagination',
			  "type": "POST",
				"data":function(data) {
					data.isActive = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.userName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.type = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.status = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  	},
		  'columns': [
			 { data: 'userName' },
			 { data: 'date' },
			 { data: 'type' },
			 { data: 'description' },
			 { data: 'cause' },
			 { data: 'improvement' },
			 { data: 'isActive' },
			 { data: 'status' },
			 { data: 'picture' },
		  ],
		  "createdRow": function(row, data, dataIndex ) {

		    if (data['isSeen'] == "0" ) {
		      $(row).addClass('rowBLue');
		    }
		  },
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    },
			    {
			      targets: 5,
			      orderable: false,
			    }, 
			    {
			      targets: 6,
			      className: 'last-column',
			      orderable: false,
			    },
			  	{
			      targets: 7,
			      className: 'last-column',
			      orderable: false,
			    },
			    {
			      targets: 8,
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				$(".paginate_page").html('<?php echo Page; ?>');
				var paginate_of = $(".paginate_of").text();
				var paginate_of = paginate_of.split(" ");
				$(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
			},
	   	});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
	}); 

	function uncheckShowDate()
 	{
		$("#showDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
			$dateValE = date("Y-m-d");
			$dateValS = date("Y-m-d", strtotime("-29 day"));
			$dateValESET = date("Y-m-d");
			$dateValSSET = date("Y-m-d", strtotime("-29 day"));
		?>

		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': 		[moment(), moment()],
				'<?php echo Yesterday; ?>': 	[moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': 	[moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': 	[moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': 	[moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			 locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
		}, 

		function(start, end, label) 
		{
			if (label == "<?php echo Custom; ?>") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Date ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckShowDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}

	
	function filterStatus()
	{
		var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterStatus.length > 0) 
		{
			$("#filterStatusSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'1'",filterStatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Buttonconfidential1' class='btn btn-sm btn-primary'><?php echo confidential ." - ". confidential ?>&nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
			}
			if (in_array("'0'",filterStatus) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Buttonnot_confidential1' class='btn btn-sm btn-primary'><?php echo confidential ." - ". Notconfidential ?>&nbsp;&nbsp;<i onclick='uncheckStatus(0)' class='fa fa-times'></i></button>";
			}
			$("#showStatus").html(html);
		}
		else
		{
			$("#filterStatusSelectedValue").css('color','#b8b0b0');
			$("#showStatus").html("");
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckStatus(id)
	{
		if (id == "1") 
		{
			$("#confidential1").prop('checked',false);
			$("#Buttonconfidential1").remove();
		}
		else if (id == "0") 
		{
			$("#not_confidential1").prop('checked',false);
			$("#Buttonnot_confidential1").remove();
		}
		var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterStatus.length > 0) 
		{
		
		}
		else
		{
			$("#filterStatusSelectedValue").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
	
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}
	
	function filterUserName(userId,userName)
	{
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
			$("#filterUserNameSelected").css('color','#FF8000');
			if (in_array(userId,filterUserName) != -1) 
			{
				html = "<button id='removeUserButton"+userId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Operator ?> - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
				$("#showUserFilter").append(html);
			}
			else
			{
				$("#removeUserButton"+userId).remove();
			}
		}
		else
		{
			$("#showUserFilter").html("");
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckUserName(userId)
	{
		$("#removeUserButton"+userId).remove();
		$("#filterUserName"+userId).prop('checked',false);
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
			
		}
		else
		{
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterType.length > 0) 
		{
			$("#filterTypeSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'1'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='accident1Button' class='btn btn-sm btn-primary'><?php echo Type ." - ". Accident ?>  &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}

			if (in_array("'0'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='incident1Button' class='btn btn-sm btn-primary'><?php echo Type ." - ". Incident ?>  &nbsp;&nbsp;<i onclick='uncheckType(0)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'2'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='suggestion1Button' class='btn btn-sm btn-primary'><?php echo Type ." - ". Suggestion ?>  &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}

			$("#showType").html(html);
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
			$("#showType").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			$("#accident1").prop('checked',false);
			$("#accident1Button").remove();
		}
		
		else if(id == "0") 
		{
			$("#incident1").prop('checked',false);
			$("#incident1Button").remove();
		}

		else if(id == "2") 
		{
			$("#suggestion1").prop('checked',false);
			$("#suggestion1Button").remove();
		}

		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
		
		}
		else
		{
			$("#filterTypeSelectedValue").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterResolved()
	{
		var filterResolved = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterResolved.length > 0) 
		{
			$("#filterStatusSelectedValueR").css('color','#FF8000');

			var html = "";
			
			if (in_array("'0'",filterResolved) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='active1Button' class='btn btn-sm btn-primary'><?php echo Resolved ." - ". Active ?>&nbsp;&nbsp;<i onclick='uncheckResolved(0)' class='fa fa-times'></i></button>";
			}

			if (in_array("'1'",filterResolved) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='resolved1Button' class='btn btn-sm btn-primary'><?php echo Resolved ." - ". Resolved ?>&nbsp;&nbsp;<i onclick='uncheckResolved(1)' class='fa fa-times'></i></button>";
			}
			$("#showResolved").html(html);
		}
		else
		{
			$("#filterStatusSelectedValueR").css('color','#b8b0b0');
			$("#showResolved").html("");
		}

		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckResolved(id)
	{
		if (id == "0") 
		{
			$("#active1").prop('checked',false);
			$("#active1Button").remove();
		}
		else if (id == "1") 
		{
			$("#resolved1").prop('checked',false);
			$("#resolved1Button").remove();
		}

		var filterResolved = $('.filterResolved:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterResolved.length > 0) 
		{
		
		}
		else
		{
			$("#filterStatusSelectedValueR").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
</script>