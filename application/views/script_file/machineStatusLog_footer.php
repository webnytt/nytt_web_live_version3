 <script>
	
	$(document).ready(function() 
	{
		
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#due-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'Today': [moment(), moment()],
				'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'Last 7 Days': [moment().subtract(6, 'days'), moment()],
				'Last 30 Days': [moment().subtract(29, 'days'), moment()],
				'This Month': [moment().startOf('month'), moment().endOf('month')],
				'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, function(start, end, label) {
			$('#due-daterange').css("color","#FF8000");
		});


		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : 'logId',
		  'ajax': {
			  'url':'machineStatusLog_pagination',
			  "type": "POST",
				"data":function(data) {
					data.machineStatus = $('.filterMachinestatus:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();

					data.machineName = $('.filtermachineName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
				},  
		  },
		  'columns': [
						 { data: 'machineName' },
						 { data: 'machineStatus' },
						 { data: 'insertTime' }
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    }
		    ], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });
});
		


		function filterMachinestatus()
		{
			var filterMachinestatus = $('.filterMachinestatus:checkbox:checked').map(function(){
									      return $(this).val();
								    }).get();

			if (filterMachinestatus.length > 0) 
			{
				$("#filterMachinestatusSelected").css('color','#FF8000');
			}
			else
			{
				$("#filterMachinestatusSelected").css('color','#b8b0b0');
			}
			$("#empTable").DataTable().ajax.reload();
		} 

		function filtermachineName()
		{
			var filtermachineName = $('.filtermachineName:checkbox:checked').map(function(){
									      return $(this).val();
								    }).get();

			if (filtermachineName.length > 0) 
			{
				$("#filtermachineNameSelected").css('color','#FF8000');
			}
			else
			{
				$("#filtermachineNameSelected").css('color','#b8b0b0');
			}
			$("#empTable").DataTable().ajax.reload();
		} 

		$(".col-sm-5").html('<a id="addTaskButton" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-task" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;ADD TASK</a>');

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

		$('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dueDateValS').val(dateValS);
			$('#dueDateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

  </script>


