<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
<script type="text/javascript">

	function changeFilterValue()
	{
		IOName = $('#IOName').val();
		reloadGraph(IOName);
	}

	function changeFilterType(value)
	{
		if (value == "day") 
		{
			$(".weekly").hide();
			$(".monthly").hide();
			$(".yearly").hide();
			$(".custom").hide();
			$(".day").show();
		}
		else if (value == "weekly") 
		{
			$(".day").hide();	
			$(".monthly").hide();
			$(".yearly").hide();
			$(".custom").hide();
			$(".weekly").show();
		}
		else if (value == "monthly") 
		{
			$(".day").hide();	
			$(".yearly").hide();
			$(".weekly").hide();
			$(".custom").hide();
			$(".monthly").show();
		}
		else if (value == "yearly") 
		{
			$(".day").hide();	
			$(".weekly").hide();
			$(".monthly").hide();
			$(".custom").hide();
			$(".yearly").show();
		}
		else if (value == "custom") 
		{
			$(".day").hide();	
			$(".weekly").hide();
			$(".monthly").hide();
			$(".yearly").hide();
			$(".custom").show();
		}
		IOName = $('#IOName').val();
		reloadGraph(IOName);
	}
	
	$(".filter").click(function() 
	{
		var clickedId = $(this).attr('id');
		var IOName = clickedId.replace('IOName',''); 
		$('#IOName').val(IOName);
		$(".filter").removeClass('active');
		$(this).addClass('active');
		reloadGraph(IOName);
	});

	function reloadGraph(IOName) 
	{
		$('.graph-loader').addClass("show").removeClass("hide");
		var choose1 = $("#choose1").val(); 
		if (choose1 == "day") 
		{
			var choose2 = $("#choose2").val(); 
		} 
		else if (choose1 == "weekly") 
		{
			var choose2 = $("#choose3").val()+"/"+$("#choose5").val(); 
		}
		else if (choose1 == "monthly") 
		{
			var choose2 = $("#choose4").val(); 
		}
		else if (choose1 == "yearly") 
		{
			var choose2 = $("#choose6").val(); 
		}
		else if (choose1 == "custom") 
		{
			var choose2 = $("#advance-daterange").val(); 
		}
		var choose7 = $("#choose7").val(); 
		var choose8 = $("#choose8").val(); 
		if (IOName == "GPIO14") 
		{
			$(".textChange").text("<?php echo Activecycles; ?>");
		}else
		{
			$(".textChange").text("<?php echo Partsproduced; ?>");
		}
		
		$.post('<?php echo base_url("VirtualMachine/virtual_machine_graph_pagination");?>',{IOName:IOName,choose1:choose1,choose2:choose2,choose7:choose7,choose8:choose8}, function getattribute(returnData)
		{
			$('.graph-loader').addClass("hide").removeClass("show");
			returnData = jQuery.parseJSON(returnData);

			$("#partCount").text(returnData.partCount);
			var DailyGraph4 = echarts.init(document.getElementById('DailyGraph4'));  
			DailyGraph4.setOption(

			option = {
			    tooltip: {
			        trigger: 'item',
			        formatter: '{b} <br/> {c} '+returnData.yAxisName+' ({d}%)'
			    },
			    series: [
			        {
			            name: 'Per',
			            type: 'pie',
			            radius: ['40%', '55%'],
			            label: {
			                formatter: '{per|{d}%}',
			                backgroundColor: '#eee',
			                borderColor: '#aaa',
			                borderWidth: 1,
			                borderRadius: 4,
			                rich: {
			                    a: {
			                        color: '#999',
			                        lineHeight: 22,
			                        align: 'center'
			                    },
			                    hr: {
			                        borderColor: '#aaa',
			                        width: '100%',
			                        borderWidth: 0.5,
			                        height: 0
			                    },
			                    b: {
			                        fontSize: 16,
			                        lineHeight: 33
			                    },
			                    per: {
			                        color: '#eee',
			                        backgroundColor: '#334455',
			                        padding: [2, 4],
			                        borderRadius: 2
			                    }
			                }
			            },
			            data: [
			                {
			                	value: returnData.producationTime,
			                	name: '<?php echo Active; ?>',
			                	itemStyle: {
									normal: {
										color: "#002060" 
									},
									label: {
										color: '#124D8D' 
									}, 
								}
			                },
			                {
			                	value: returnData.noProducationTime,
			                	name: '<?php echo Notactive; ?>',
			                	itemStyle: {
									normal: {
										color: "#FF8000" 
									},
									label: {
										color: '#124D8D',
									}, 
								}
			                }
			            ],
			             itemStyle: {
							emphasis: {
								shadowBlur: 0,
								shadowOffsetX: 0,
								shadowColor: 'rgba(0, 0, 0, 0.5)'
							}
						},
						hoverOffset: 1,
						
			        }
			    ]
			}
		, true);
		});
	}

	$(document).ready(function() 
	{
		<?php 
			$dateValE = date("F d, Y", strtotime('today'));
			$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 
		function(start, end, label) 
		{
			//$('#advance-daterange').css("color","#FF8000");
		});


		$('#choose2').datepicker({
			format: "mm/dd/yyyy",
			endDate: "today"
		});

		$('#choose7').timepicker({
			showMeridian: false,
			defaultTime: '00:00' 
		});

		$('#choose8').timepicker({
			showMeridian: false,
			defaultTime:  '23:59'
		});

		$('#choose4').datepicker({
			format: "M yyyy",
		    viewMode: "months", 
		    minViewMode: "months"
		}).datepicker('setDate',new Date());
	});
</script>