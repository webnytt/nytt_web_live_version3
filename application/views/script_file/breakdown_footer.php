<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
	<script>
		function changeFilterValue()
		{
			machineIdStop = $('#machineIdStop').val();
			reloadGraph1(machineIdStop);
		}
		function changeAnalyticsType(type)
		{
			if (type == "waitAnalytics") 
			{
				$(".stopAnalytics").css("opacity","0.5");
				$(".waitAnalytics").css("opacity","1");
			}
			else
			{
				$(".waitAnalytics").css("opacity","0.5");
				$(".stopAnalytics").css("opacity","1");
			}
			$('#analytics').val(type);
			machineIdStop = $('#machineIdStop').val();
			reloadGraph1(machineIdStop);
		}

		<?php if(!empty($_POST['partsChoose1'])) { ?>
			changeFilterType("<?php echo $_POST['partsChoose1'] ?>");
		<?php }else{ ?>
			changeFilterValue();
		<?php } ?>


		function changeFilterType(value)
		{
			if (value == "day") 
			{
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").hide();
				$(".day").show();
			}
			else if (value == "weekly") 
			{
				$(".day").hide();	
				$(".monthly").hide();
				$(".yearly").hide();
				$(".weekly").show();
			}
			else if (value == "monthly") 
			{
				$(".day").hide();	
				$(".yearly").hide();
				$(".weekly").hide();
				$(".monthly").show();
			}
			else if (value == "yearly") 
			{
				$(".day").hide();	
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").show();
			}
			machineId = $('#machineIdStop').val();
			reloadGraph1(machineId);
		}

		$(document).ready(function() 
		{ 
			$('.datepicker').datepicker({
					format: "mm/dd/yyyy",
					language: "<?php echo datepickerLanguage; ?>",
					autoclose: true
				});


			$('.datepicker1').datepicker({
					format: "M yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
				    language: "<?php echo datepickerLanguage; ?>",
				    autoclose: true
				});
		});

		$('#carousel-example1').on('slide.bs.carousel', function (e) 
		{   
		    var $e = $(e.relatedTarget);
		    var idx = $e.index();
		    var itemsPerSlide = 5;
		    var totalItems = $('.carousel-item').length;
		 
		    if (idx >= totalItems-(itemsPerSlide-1)) {
		        var it = itemsPerSlide - (totalItems - idx);
		        for (var i=0; i<it; i++) {
		            // append slides to end
		            if (e.direction=="left") {
		                $('.carousel-item').eq(i).appendTo('.carousel-inner');
		            }
		            else {
		                $('.carousel-item').eq(0).appendTo('.carousel-inner');
		            }
		        }
		    }
		});

		(function($) 
		{
		  	$.fn.inputFilter = function(inputFilter) 
		  	{
		    return this.on("input keydown keyup mousedown mouseup select contextmenu drop", function() {
		      if (inputFilter(this.value)) {
		        this.oldValue = this.value;
		        this.oldSelectionStart = this.selectionStart;
		        this.oldSelectionEnd = this.selectionEnd;
		      } else if (this.hasOwnProperty("oldValue")) {
		        this.value = this.oldValue;
		        this.setSelectionRange(this.oldSelectionStart, this.oldSelectionEnd);
		      } else {
		        this.value = "";
		      }
		    });
		  };
		}(jQuery));


		$(document).ready(function() { 

			$(".inputText").inputFilter(function(value) 
			{
			    return /^\d*$/.test(value);

			});
			$("#duration2").keyup(function(){
			  	var duration2 = parseInt($(this).val());
			  	var duration3 = parseInt($("#duration3").val());
			  	var duration4 = parseInt($("#duration4").val());
			  	if (duration2 >= duration3) 
			  	{
			  		$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo Pleaseentervalidincreasingvaluesforblocks; ?>."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
			  		console.log(duration2);
			  		if (duration2 != NaN) 
			  		{
						$(".duration2Text").text(duration2+" mins");
			  		}
			  	}

			  	if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			$("#duration3").keyup(function()
			{
			  	var duration2 = parseInt($("#duration2").val());
			  	var duration3 = parseInt($(this).val());
			  	var duration4 = parseInt($("#duration4").val());
			  	if (duration3 >= duration4) 
			  	{
			  		$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo Pleaseentervalidincreasingvaluesforblocks; ?>."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else if(duration3 < duration2) 
			  	{
			  		$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo Pleaseentervaliddecreasingvaluesforblocks; ?>."
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
			  		$(".duration3Text").text(duration3+" mins");
			  	}
				if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			$("#duration4").keyup(function()
			{
			  	var duration4 = parseInt($(this).val());
			  	var duration3 = parseInt($("#duration3").val());
			  	var duration2 = parseInt($("#duration2").val());

			  	if (duration4 < duration3) 
			  	{
			  		$.gritter.add({
						title: '<?php echo Error; ?>',
						text: "<?php echo Pleaseentervaliddecreasingvaluesforblocks; ?>"
					});
			  		
			  		$(this).css("background",'#ffe5e5');
			  	}
			  	else
			  	{
			  		$(this).css("color",'black');
			  		$(this).css("background",'#FFFFFF');
					$(".duration4Text").text(duration4+" mins");
			  	}
			  	if (duration2 < duration3 && duration3 < duration4) 
			  	{
			  		machineIdStop = $('#machineIdStop').val();
					reloadGraph1(machineIdStop);
			  	}
			});

			var machineId = $("#machineIdStop").val();
			if (machineId != 0) 
			{
				reloadGraph1(machineId);
			}
			
			$(".filter1").click(function() 
			{
				var clickedId = $(this).attr('id');
				var machineId = clickedId.replace('machineStop',''); 
				$('#machineIdStop').val(machineId);
				$(".filter1").removeClass('active');
				$(this).addClass('active');
				var machineId = $("#machineIdStop").val();
				reloadGraph1(machineId);
			});
			
			$(document).on('click', '.widget1', function() 
			{
				if($(this).hasClass("widget_bordered")) 
				{
					$(this).removeClass("widget_bordered");
				} 
				else 
				{
					$(this).addClass("widget_bordered");
				}
				var machineId = $("#machineIdStop").val();
				reloadGraph1(machineId);
			});
		});
		
		function formatSeconds(value)
		{
			var sec_num = parseInt(value, 10); 
			var minutes = Math.floor(sec_num / 60);
			var seconds = sec_num - (minutes * 60);
			if (minutes < 10) {minutes = "0"+minutes;}
			if (seconds < 10) {seconds = "0"+seconds;}
			return minutes+':'+seconds;
		}
		
		function reloadGraph1(machineId) 
		{ 
			var theme_color = '#ff8000'; 

			var choose1 = $("#choose1").val();
			if (choose1 == "day") 
			{
				var choose2 = $("#choose2").val(); 
			} 
			else if (choose1 == "weekly") 
			{
				var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
			}
			else if (choose1 == "monthly") 
			{
				var choose2 = $("#choose4").val(); 
			}
			else if (choose1 == "yearly") 
			{
				var choose2 = $("#choose5").val(); 
			}

			$("#mainChoose1").val(choose1);
			$("#mainChoose2").val(choose2);
			$("#mainMachineId").val(machineId);
			
			/*choose = $("#choose2").val().split("/");
			var choose1 = choose[2];  
			var choose2 = choose[0];  
			var choose3 = $("#choose3").val();  
			var choose4 = choose[1];*/
			var choose3 = "";
			var choose4 = "";
			var duration1Min = parseInt($("#duration1").val());
			var duration2Min = parseInt($("#duration2").val());
			var duration3Min = parseInt($("#duration3").val());
			var duration4Min = parseInt($("#duration4").val());
			var analytics = $("#analytics").val();

			if($("#filter1widget").hasClass("widget_bordered")) 
			{
				$("#filter1widget").css('opacity',"1");
				$("#filter1widgetbutton").show();
				var filter1Val = 1;
			} 
			else 
			{
				$("#filter1widget").css('opacity',"0.5");
				$("#filter1widgetbutton").hide();
				var filter1Val = 0;
			}
			if($("#filter2widget").hasClass("widget_bordered")) 
			{
				$("#filter2widget").css('opacity',"1");
				$("#filter2widgetbutton").show();
				var filter2Val = 1;
			} 
			else 
			{
				$("#filter2widget").css('opacity',"0.5");
				$("#filter2widgetbutton").hide();
				var filter2Val = 0;
			}
			if($("#filter3widget").hasClass("widget_bordered")) 
			{
				$("#filter3widget").css('opacity',"1");
				$("#filter3widgetbutton").show();
				var filter3Val = 1;
			} 
			else 
			{
				$("#filter3widget").css('opacity',"0.5");
				$("#filter3widgetbutton").hide();
				var filter3Val = 0;
			} 
			if($("#filter4widget").hasClass("widget_bordered")) 
			{
				$("#filter4widget").css('opacity',"1");
				$("#filter4widgetbutton").show();
				var filter4Val = 1;
			} 
			else 
			{
				$("#filter4widget").css('opacity',"0.5");
				$("#filter4widgetbutton").hide();
				var filter4Val = 0;
			} 
			
			var factoryId = "<?php  echo $this->session->userdata('factoryId'); ?>";
			$('.graph-loader').addClass("show").removeClass("hide"); 
			if (duration1Min < duration2Min && duration2Min < duration3Min && duration3Min < duration4Min) 
		  	{
			  	$("#choose1").prop("disabled",true);
				$("#choose2").prop("disabled",true);
				$("#choose3").prop("disabled",true);
				$("#choose4").prop("disabled",true);
				$("#choose5").prop("disabled",true);
				$("#duration2").prop("disabled",true);
				$("#duration3").prop("disabled",true);
				$("#duration4").prop("disabled",true);
				$(".machineNameText").prop("disabled",true);
				$("#stopBlock").css('pointer-events','none');
				$("#stopBlock").css('opacity','0.4');
				$.post('<?php echo base_url();?>Analytics/breakdown_pagination',{machineId:machineId,choose1:choose1,choose2:choose2,choose3:choose3,choose4:choose4,filter1Val:filter1Val,filter2Val:filter2Val,filter3Val:filter3Val,filter4Val:filter4Val,factoryId:factoryId,duration1:duration1Min,duration2:duration2Min,duration3:duration3Min,duration4:duration4Min,analytics:analytics}, function getattribute(returnData)
				{ 
					$("#stopBlock").css('pointer-events','fill');
					$("#stopBlock").css('opacity','1');  
					$("#choose1").prop("disabled",false);
					$("#choose2").prop("disabled",false);
					$("#choose3").prop("disabled",false);
					$("#choose4").prop("disabled",false);
					$("#choose5").prop("disabled",false);
					$("#duration2").prop("disabled",false);
					$("#duration3").prop("disabled",false);
					$("#duration4").prop("disabled",false);
					$(".machineNameText").prop("disabled",false);
					$('.graph-loader').addClass("hide").removeClass("show");
					
					var analyticsVar = (analytics == "waitAnalytics") ? "<?php echo waits;  ?>" : "<?php echo stops;  ?>"; 

					var upperLimitFirst = returnData.daily.upperLimitFirst; 
					if (upperLimitFirst == 1) 
					{
						$("#upperLimitFirst").show();
					}else
					{
						$("#upperLimitFirst").hide();
					}
					var upperLimitSecond = returnData.daily.upperLimitSecond; 
					if (upperLimitSecond == 1) 
					{
						$("#upperLimitSecond").show();
					}else
					{
						$("#upperLimitSecond").hide();
					}
					var upperLimitThird = returnData.daily.upperLimitThird; 
					if (upperLimitThird == 1) 
					{
						$("#upperLimitThird").show();
					}else
					{
						$("#upperLimitThird").hide();
					}
					var upperLimitFour = returnData.daily.upperLimitFour;
					if (upperLimitFour == 1) 
					{
						$("#upperLimitFour").show();
					}else
					{
						$("#upperLimitFour").hide();
					} 

					$("#filter1").html(returnData.daily.filter1+" <?php echo mins; ?>");
					$("#filter1count").html(returnData.daily.filter1count+" "+analyticsVar);
					
					$("#filter2").html(returnData.daily.filter2+" <?php echo mins; ?>");
					$("#filter2count").html(returnData.daily.filter2count+" "+analyticsVar);
					
					
					$("#filter3").html(returnData.daily.filter3+" <?php echo mins; ?>");
					$("#filter3count").html(returnData.daily.filter3count+" "+analyticsVar);
					
					$("#filter4").html(returnData.daily.filter4+" <?php echo mins; ?>");
					$("#filter4count").html(returnData.daily.filter4count+" "+analyticsVar);

					$("#errorNotification").text(returnData.machineNotifyTime.errorNotification);
					$("#errorReasonNotification").text(returnData.machineNotifyTime.errorReasonNotification);
					
					var duration1 = parseInt($("#duration1").val())*60;
					var duration2 = parseInt($("#duration2").val())*60;
					var duration3 = parseInt($("#duration3").val())*60;
					var duration4 = parseInt($("#duration4").val())*60;
						var seriesText111, seriesText11, seriesText12, seriesText13, seriesText14, seriesText15, seriesText16, seriesText17, seriesText18, seriesText19, seriesText20, seriesText121;
						var seriesText222, seriesText21, seriesText22, seriesText23, seriesText24, seriesText25, seriesText26, seriesText27, seriesText28, seriesText29, seriesText30, seriesText131;
						var seriesText333, seriesText31, seriesText32, seriesText33, seriesText34, seriesText35, seriesText36, seriesText37, seriesText38, seriesText39, seriesText40, seriesText141;
						var seriesText444, seriesText41, seriesText42, seriesText43, seriesText44, seriesText45, seriesText46, seriesText47, seriesText48, seriesText49, seriesText50, seriesText151;
						
						var f1w = 0;  
					if($("#filter1widget").hasClass('widget_bordered') === true) 
					{
						f1w = 1;

						seriesText111 =	{
							type: 'line',
							lineStyle:{
								width:0
							},
							symbolSize: 0,
							data: [[0,duration2], [86400,duration2]],
							itemStyle: {
				                color: '#FF8000'
				            },
						    areaStyle: {
				                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
				                    offset: 0,
				                    color: '#FF8000'
				                }, {
				                    offset: 1,
				                    color: '#ffffff'
				                }])
				            },
						} 

						seriesText1 = {
							symbolSize: 6,
							data: returnData.daily.stopfilter1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#5d4de2',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
						
						seriesText11 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li1,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi1.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
						
						seriesText12 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li2,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi2.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText13 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li3,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi3.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
						
						seriesText14 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li4,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi4.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText15 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li5,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi5.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							} 
							
						seriesText16 = {
							    symbolSize: 6,
								data: returnData.daily.reasonfilter1.li6,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi6.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}
						seriesText17 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li7,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi7.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}
						seriesText18 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li8,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi8.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText19 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li9,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi9.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText20 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li10,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: returnData.daily.reasonfilter1.colorLi10.color,
						            color: '#ffffff'
						        },
								type: 'scatter',
							}

						seriesText121 = {
								symbolSize: 6,
								data: returnData.daily.reasonfilter1.li11,
								itemStyle: {
						            borderWidth: 3,
						            borderColor: '#d32f2f',
						            color: '#ffffff'
						        },
								type: 'scatter',
							}


					} else { 
						
						seriesText111 =	{
							symbolSize: 6,
							data: '',
							itemStyle: {
								normal: {
									color:'#F67280',
								 }
							},
							type: 'scatter',
						} 

						seriesText1 = {
							symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#F8B195',
								 }
							},
							type: 'scatter',
						}
						
						seriesText11 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#CC6600',
									 }
								},
								type: 'scatter',
							} 
						
						seriesText12 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#9933CC',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText13 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#0000FF',
									 }
								},
								type: 'scatter',
							} 
						
						seriesText14 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#FF00CC',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText15 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#999900',
									 }
								},
								type: 'scatter',
							} 
							
						seriesText16 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#FF6600',
									 }
								},
								type: 'scatter',
							}
						seriesText17 = {
							symbolSize: 6,
								data: '',
								itemStyle: {
									 normal: {
										color:'#00796b',
									 }
								},
								type: 'scatter',
							}
						seriesText18 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText19 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText20 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

						seriesText121 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
						
					}
				
				var f2w = 0;
				if($("#filter2widget").hasClass('widget_bordered') === true) {
					f2w = 1;

					seriesText222 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration3], [86400,duration3]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 


					seriesText2 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter2,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#5d4de2',
				            color: '#ffffff'
				        },
						type: 'scatter', 
					} 
					
					seriesText21 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText22 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText23 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText24 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText25 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText26 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText27 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText28 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText29 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText30 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter2.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText131 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter2.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {
					
					seriesText222 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#F67280',
							 }
						},
						type: 'scatter',
					} 

					seriesText2 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#F67280',
							 }
						},
						type: 'scatter',
					} 
					
					seriesText21 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText22 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText23 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText24 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText25 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText26 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText27 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText28 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText29 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText30 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText131 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var f3w = 0; 
				if($("#filter3widget").hasClass('widget_bordered') === true) {
					f3w = 1;

					seriesText333 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration4], [86400,duration4]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 

					seriesText3 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter3,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#5d4de2',
				            color: '#ffffff'
					    },
						type: 'scatter',
					}
					
					seriesText31 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText32 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText33 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText34 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText35 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText36 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText37 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText38 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText39 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText40 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter3.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText141 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter3.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {

					seriesText333 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#C06C84',
							 }
						},
						type: 'scatter',
					} 

					seriesText3 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#C06C84',
							 }
						},
						type: 'scatter',
					}
					
					seriesText31 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText32 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText33 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText34 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText35 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText36 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText37 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText38 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText39 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText40 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText141 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var f4w = 0;

				
				if($("#filter4widget").hasClass('widget_bordered') === true) {
					f4w = 1;
					duration5 = returnData.daily.maxSecondsValue;

					seriesText444 =	{
						type: 'line',
						lineStyle:{
							width:0
						},
						symbolSize: 0,
						data: [[0,duration5], [86400,duration5]],
						itemStyle: {
			                color: '#FF8000'
			            },
					    areaStyle: {
			                color: new echarts.graphic.LinearGradient(0, 0, 0, 1, [{
			                    offset: 0,
			                    color: '#FF8000'
			                }, {
			                    offset: 1,
			                    color: '#ffffff'
			                }])
			            },
					} 

					seriesText4 =	{
						symbolSize: 6,
						data: returnData.daily.stopfilter4,
						itemStyle: {
				            borderWidth: 3,
				            borderColor: '#5d4de2',
				            color: '#ffffff'
					    },
						type: 'scatter',
					}
					
					seriesText41 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li1,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi1.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText42 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li2,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi2.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText43 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li3,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi3.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
					
					seriesText44 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li4,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi4.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText45 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li5,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi5.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						} 
						
					seriesText46 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li6,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi6.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText47 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li7,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi7.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
					seriesText48 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li8,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi8.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText49 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li9,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi9.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText50 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li10,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: returnData.daily.reasonfilter4.colorLi10.color,
					            color: '#ffffff'
					        },
							type: 'scatter',
						}

					seriesText151 = {
						symbolSize: 6,
							data: returnData.daily.reasonfilter4.li11,
							itemStyle: {
					            borderWidth: 3,
					            borderColor: '#d32f2f',
					            color: '#ffffff'
					        },
							type: 'scatter',
						}
				} else {

					seriesText444 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#6C5B7B',
							 }
						},
						type: 'scatter',
					} 

					
					seriesText4 =	{
						symbolSize: 6,
						data: '',
						itemStyle: {
							normal: {
								color:'#6C5B7B',
							 }
						},
						type: 'scatter',
					}
					
					seriesText41 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#CC6600',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText42 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#9933CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText43 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#0000FF',
								 }
							},
							type: 'scatter',
						} 
					
					seriesText44 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF00CC',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText45 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#999900',
								 }
							},
							type: 'scatter',
						} 
						
					seriesText46 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#FF6600',
								 }
							},
							type: 'scatter',
						}
					seriesText47 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#00796b',
								 }
							},
							type: 'scatter',
						}
					seriesText48 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText49 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}

					seriesText50 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
					
					seriesText151 = {
						symbolSize: 6,
							data: '',
							itemStyle: {
								 normal: {
									color:'#d32f2f',
								 }
							},
							type: 'scatter',
						}
				}
				
				var Daily5 = document.getElementById('DailyDowntime1');
					var DailyDowntime1 = echarts.init(Daily5);  
					var DailyDowntime1Opt = {
						tooltip : {
							axisPointer : {            
								type : 'shadow'
							},
						
						formatter:  
							function (params, ticket, callback) {
								var shortVar = (analytics == "waitAnalytics") ? "<?php echo Shortwait; ?>" : "<?php echo Shortstop; ?>"; 
								var seriesName = (params.value[2] == undefined || params.value[2] == "") ? shortVar : params.value[2];
								var sec_num1 = parseInt(params.value[0], 10);  
								var hours1  = Math.floor(sec_num1 / 3600);
								var minutes1 = Math.floor((sec_num1 - (hours1 * 3600)) / 60);
								var seconds1 = sec_num1 - (hours1 * 3600) - (minutes1 * 60);

								if (hours1   < 10) {hours1   = "0"+hours1;}
								if (minutes1 < 10) {minutes1 = "0"+minutes1;}
								if (seconds1 < 10) {seconds1 = "0"+seconds1;}
								var value1 = params.value[3]+' '+hours1+':'+minutes1+':'+seconds1;
								

								var sec_num2 = parseInt(params.value[1], 10); 
								var minutes2 = Math.floor(sec_num2 / 60);
								var seconds2 = sec_num2 - (minutes2 * 60);
								
								if (minutes2 < 10) {minutes2 = "0"+minutes2;}
								if (seconds2 < 10) {seconds2 = "0"+seconds2;}
								var value2 = minutes2+':'+seconds2;

								if (params.value[1] >  returnData.daily.upperLimit && returnData.daily.upperLimit != 0 ) 
								{
									var longStop = (analytics == "waitAnalytics") ? "<?php echo Longwait; ?> <br/>" : "<?php echo Longstop; ?> <br/>"; 
								}else
								{
									var longStop = "";
								}

								if (params.value[4]  == "1") 
								{
									return longStop+seriesName+" <br/> "+value1+" <br/> > "+value2+" <?php echo minuteslong; ?>";
								}else
								{
									return longStop+seriesName+" <br/> "+value1+" <br/>  "+value2+" <?php echo minuteslong; ?>";
								}
							}
						}, 
						legend: {
							data: ['Stopped'],
						},
						xAxis:  {
							type: 'value',
							splitLine: false,
							axisLabel: {
								color:"#000000",
							
								formatter:  
									function (value, index) {
										
										var sec_num = parseInt(value, 10); 
										var hours   = Math.floor(sec_num / 3600);
										var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
										var seconds = sec_num - (hours * 3600) - (minutes * 60);

										if (hours   < 10) {hours   = "0"+hours;}
										if (minutes < 10) {minutes = "0"+minutes;}
										if (seconds < 10) {seconds = "0"+seconds;}

										return hours+':'+minutes+':'+seconds;
									},  
							},
							axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false,
					            
					        },  
					        interval: 14400, 
					        max:86400
						},
						grid: {
							left: '10%',
							right: '7%',
						}, 
						yAxis: {
							type : 'value',
							name:"<?php echo   DURATIONSEC; ?>" ,
							nameTextStyle: {
								color:"#000000",
								fontSize:12, 
								right:15,
							},
							scale: true,
					        splitLine: {
					            lineStyle: {
					                type: 'dot'
					            }
					        },
					        axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false
					        }
							
						},
						 dataZoom: [
						    {
					            type: 'inside'
					        }, 
					        {
					            type: 'slider',
					            showDataShadow: false,
					            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
					            handleSize: '80%',
					            handleStyle: {
					                color: '#999',
					                shadowBlur: 3,
					                shadowColor: 'rgba(0, 0, 0, 0.6)',
					                shadowOffsetX: 2,
					                shadowOffsetY: 2
					            },
					            top:320,
					        }, 
					        {
					            type: 'inside',
					            orient: 'vertical'
					        }, 
					        {
					            type: 'slider',
					            orient: 'vertical',
					            showDataShadow: false,
					            handleIcon: 'M10.7,11.9v-1.3H9.3v1.3c-4.9,0.3-8.8,4.4-8.8,9.4c0,5,3.9,9.1,8.8,9.4v1.3h1.3v-1.3c4.9-0.3,8.8-4.4,8.8-9.4C19.5,16.3,15.6,12.2,10.7,11.9z M13.3,24.4H6.7V23h6.6V24.4z M13.3,19.6H6.7v-1.4h6.6V19.6z',
					            handleSize: '80%',
					            handleStyle: {
					                color: '#999',
					                shadowBlur: 3,
					                shadowColor: 'rgba(0, 0, 0, 0.6)',
					                shadowOffsetX: 2,
					                shadowOffsetY: 2
					            },
					            right:0,
					        }
					        ], 
						series: [
							{
						        type: 'scatter',
						        symbolSize: 0,
						        data: [[0,0], [30,30]],   
						        itemStyle: {
						            color: "blue"
						        }
						    }, 
							
						],
						  
					};  
				
						DailyDowntime1Opt.series.push(seriesText1, seriesText11, seriesText12, seriesText13, seriesText14, seriesText15, seriesText16, seriesText17, seriesText18, seriesText19, seriesText20, seriesText121, seriesText111);
					
					
						DailyDowntime1Opt.series.push(seriesText2, seriesText21, seriesText22, seriesText23, seriesText24, seriesText25, seriesText26, seriesText27, seriesText28, seriesText29, seriesText30, seriesText131, seriesText222);
					
					
						DailyDowntime1Opt.series.push(seriesText3, seriesText31, seriesText32, seriesText33, seriesText34, seriesText35, seriesText36, seriesText37, seriesText38, seriesText39, seriesText40, seriesText141, seriesText333);
					
					
						DailyDowntime1Opt.series.push(seriesText4, seriesText41, seriesText42, seriesText43, seriesText44, seriesText45, seriesText46, seriesText47, seriesText48, seriesText49, seriesText50, seriesText151, seriesText444);
					
					
					
					
					
					DailyDowntime1.setOption(DailyDowntime1Opt); 
					
					
					var i;
					var RD1 = returnData.daily.reasonCountArrDuration1.reverse();
					var FRD1 = new Array(RD1.length);
					for (i = 0; i < RD1.length; i++) {
						if(typeof RD1[i].value == 'undefined') { RD1[i].value = 0; }
						FRD1[i] = formatSeconds(RD1[i].value);
					} 
					
					var RD2 = returnData.daily.reasonCountArrDuration2.reverse();
					var FRD2 = new Array(RD2.length);
					for (i = 0; i < RD2.length; i++) {
						if(typeof RD2[i].value == 'undefined') { RD2[i].value = 0; }
						FRD2[i] = formatSeconds(RD2[i].value);
					} 
					var RD3 = returnData.daily.reasonCountArrDuration3.reverse();
					var FRD3 = new Array(RD3.length);
					for (i = 0; i < RD3.length; i++) {
						if(typeof RD3[i].value == 'undefined') { RD3[i].value = 0; }
						FRD3[i] = formatSeconds(RD3[i].value);
					} 
					var RD4 = returnData.daily.reasonCountArrDuration4.reverse();
					var FRD4 = new Array(RD4.length);
					for (i = 0; i < RD4.length; i++) {
						if(typeof RD4[i].value == 'undefined') { RD4[i].value = 0; }
						FRD4[i] = formatSeconds(RD4[i].value);
					} 
					
					
					var RC1 = returnData.daily.reasonCountArr1.reverse();
					var FRC1 = new Array(RC1.length);
					for (i = 0; i < RC1.length; i++) {
						if(typeof RC1[i].value == 'undefined') { RC1[i].value = 0; }
						FRC1[i] = RC1[i].value;
					}  
					
					var RC2 = returnData.daily.reasonCountArr2.reverse();
					var FRC2 = new Array(RC2.length);
					for (i = 0; i < RC2.length; i++) {
						if(typeof RC2[i].value == 'undefined') { RC2[i].value = 0; }
						FRC2[i] = RC2[i].value;
					}  
					
					var RC3 = returnData.daily.reasonCountArr3.reverse();
					var FRC3 = new Array(RC3.length);
					for (i = 0; i < RC3.length; i++) {
						if(typeof RC3[i].value == 'undefined') { RC3[i].value = 0; }
						FRC3[i] = RC3[i].value;
					}  
					
					var RC4 = returnData.daily.reasonCountArr4.reverse();
					var FRC4 = new Array(RC4.length);
					for (i = 0; i < RC4.length; i++) {
						if(typeof RC4[i].value == 'undefined') { RC4[i].value = 0; }
						FRC4[i] = RC4[i].value;
					}  
					
					maxSecondsValue = 600;
					if(f1w == 1) 
					{
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr1Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr1,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F8B195" 
								}
							}
						};
						var reasonCountArrDuration1Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration1, 
							type: 'bar',
							stack: 1, 
						};
					} else {
						var reasonCountArr1Text = {
							name:'Reason Count', 
							data:[{"value":60,"itemStyle":{"color":"#CC6600"}},{"value":0,"itemStyle":{"color":"#9933CC"}},{"value":0,"itemStyle":{"color":"#0000FF"}},{"value":0,"itemStyle":{"color":"#FF00CC"}},{"value":0,"itemStyle":{"color":"#999900"}},{"value":0,"itemStyle":{"color":"#FF6600"}},{"value":0,"itemStyle":{"color":"#00796b"}},{"value":0,"itemStyle":{"color":"#d32f2f"}}], 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F8B195" 
								}
							}
						};
						var reasonCountArrDuration1Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 
					
					if(f2w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr2Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr2,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F67280" 
								}
							}
						};
						var reasonCountArrDuration2Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration2, 
							type: 'bar',
							stack: 1, 
						};
					} else {
						var reasonCountArr2Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#F67280" 
								}
							}
						};
						var reasonCountArrDuration2Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 
					if(f3w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr3Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr3,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#C06C84" 
								}
							}
						};
						var reasonCountArrDuration3Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration3, 
							type: 'bar',
							stack: 1,
						};
					} else {
						var reasonCountArr3Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#C06C84" 
								}
							}
						};
						var reasonCountArrDuration3Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					} 	
					if(f4w == 1) {
						maxSecondsValue = returnData.daily.maxSecondsValue;
						var reasonCountArr4Text = {
							name:'Reason Count', 
							data:returnData.daily.reasonCountArr4,
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#6C5B7B" 
								}
							}
						};
						var reasonCountArrDuration4Text = {
							name:'Reason duration', 
							data:returnData.daily.reasonCountArrDuration4, 
							type: 'bar',
							stack: 1,
						};
					} else {
						var reasonCountArr4Text = {
							name:'Reason Count', 
							data:'', 
							type: 'scatter',
							symbolSize: 0,
							itemStyle: {
								normal: {
									color: "#6C5B7B" 
								}
							}
						};
						var reasonCountArrDuration4Text = {
							name:'Reason duration', 
							data:'', 
							type: 'bar',
							stack: 1,
						};
					}

					/*var seriesText1 = {
								name: returnData.daily.machine.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue1,
								color: '#FFCF00',
							};
						var seriesText2 = {
								name: returnData.daily.machine.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue2,
								color: '#FCD219',
							};
						var seriesText3 = {
								name: returnData.daily.machine.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue3,
								color: '#FFD933',
							};
						var seriesText4 = {
								name: returnData.daily.machine.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue4,
								color: '#FFDD4D',
							};
						var seriesText5 = {
								name: returnData.daily.machine.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue5,
								color: '#FFE266',
							};
						var seriesText6 = {
								name: returnData.daily.machine.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue6,
								color: '#FFE780',
							};
						var seriesText7 = {
								name: returnData.daily.machine.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue7,
								color: '#FFEC99',
							};
						var seriesText8 = {
								name: returnData.daily.machine.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue8,
								color: '#FFF1B3',
							};
						var seriesText9 = {
								name: returnData.daily.machine.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue9,
								color: '#FFF5CC',
							};
						var seriesText10 = {
								name: returnData.daily.machine.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue10,
								color: '#FFFAE6',
							};*/

					

					//console.log(returnData.daily.maxValueArr);
					var intervalValue = returnData.daily.maxValueArr / 8;

					//console.log(intervalValue);
					
					var DailyDowntime2 = echarts.init(document.getElementById('DailyDowntime2'));  
					var DailyDowntime2Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						formatter: 
							function (params) {
							
							var outStrng = "";	
							for (i = 0; i < params.length; i++) {
							  if (params[i].value != 0) 
							  {
							  	var outStrng = outStrng.concat(params[i].seriesName+": "+params[i].value+" min<br/>");
							  }
							}
							return outStrng; 
								
							}, 
						
						},
						title:{
							text:"Downtime split analysis",
							show:false,
							x:"right",
							y:"top", 
							textStyle:{
								color: theme_color,  
								fontSize:16, 
							},
						}, 
						legend: {
							show:false, 
						},
						grid: {
							left: '30%',
							right: '4%',
						}, 
						yAxis: {
							
							data:returnData.daily.reasonNameArr.reverse(), 
							name:"<?php echo ERRORTYPE; ?>" ,
							nameTextStyle: {
								color:"#000000",
								fontSize:12, 
							},
							axisLabel: { 
								rotate: 0,
								verticalAlign: 'middle',
								color:"#124D8D",
							},
							axisLine: {
					            show: false
					        },
					        axisTick: {
					            show: false
					        },
						},
						xAxis: 
							{
								type: 'value',
								min: 0,
								showSymbol: false,
								hoverAnimation: true,
								axisLine: {
						            show: false
						        },
								splitLine: {
						            lineStyle: {
						                type: 'dot'
						            }
						        },
						        axisTick: {
						            show: false
						        },
						        interval: intervalValue, 
						        max: returnData.daily.maxValueArr, 
								axisLabel: {
									
									formatter:  
									function (value, index) {
										
										var sec_num = parseInt(value, 10); 
										var minutes = Math.floor(sec_num / 60);
										var seconds = sec_num - (minutes * 60);
										if (minutes < 10) {minutes = "0"+minutes;}
										if (seconds < 10) {seconds = "0"+seconds;}
										return minutes+':'+seconds;
									},  
									color:"black"
								},
								nameTextStyle: {
									color:"black",
								},

							}
						, 
						series: [
							{
							  name: returnData.daily.machine.seriesName1,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue1,
							  color: '#FFCF00',
							},
							{
							  name: returnData.daily.machine.seriesName2,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue2,
							  color: '#FCD219',
							},
							{
							  name: returnData.daily.machine.seriesName3,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue3,
							  color: '#FFD933',
							},
							{
							  name: returnData.daily.machine.seriesName4,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue4,
							  color: '#FFDD4D',
							},
							{
							  name: returnData.daily.machine.seriesName5,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue5,
							  color: '#FFE266',
							},
							{
							  name: returnData.daily.machine.seriesName6,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue6,
							  color: '#FFE780',
							},
							{
							  name: returnData.daily.machine.seriesName7,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue7,
							  color: '#FFEC99',
							},
							{
							  name: returnData.daily.machine.seriesName8,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue8,
							  color: '#FFF1B3',
							},
							{
							  name: returnData.daily.machine.seriesName9,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue9,
							  color: '#FFF5CC',
							},
							{
							  name: returnData.daily.machine.seriesName10,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue10,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName11,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue11,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName12,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue12,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName13,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue13,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName14,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue14,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName15,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue15,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName16,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue16,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName17,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue17,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName18,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue18,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName19,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue19,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName20,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue20,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName21,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue21,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName22,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue22,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName23,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue23,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName24,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue24,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName25,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue25,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName26,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue26,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName27,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue27,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName28,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue28,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName29,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue29,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName30,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue30,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName31,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue31,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName32,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue32,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName33,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue33,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName34,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue34,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName35,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue35,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName36,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue36,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName37,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue37,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName38,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue38,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName39,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue39,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName40,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue40,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName41,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue41,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName42,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue42,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName43,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue43,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName44,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue44,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName45,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue45,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName46,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue46,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName47,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue47,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName48,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue48,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName49,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue49,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName50,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue50,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName51,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue51,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName52,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue52,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName53,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue53,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName54,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue54,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName55,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue55,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName56,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue56,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName57,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue57,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName58,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue58,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName59,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue59,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName60,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue60,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName61,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue61,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName62,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue62,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName63,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue63,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName64,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue64,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName65,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue65,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName66,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue66,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName67,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue67,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName68,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue68,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName69,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue69,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName70,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue70,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName71,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue71,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName72,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue72,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName73,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue73,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName74,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue74,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName75,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue75,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName76,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue76,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName77,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue77,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName78,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue78,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName79,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue79,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName80,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue80,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName81,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue81,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName82,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue82,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName83,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue83,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName84,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue84,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName85,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue85,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName86,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue86,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName87,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue87,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName88,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue88,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName89,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue89,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName90,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue90,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName91,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue91,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName92,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue92,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName93,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue93,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName94,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue94,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName95,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue95,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName96,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue96,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName97,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue97,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName98,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue98,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName99,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue99,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.machine.seriesName100,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.machine.seriesValue100,
							  color: '#FFFAE6',
							},
							{
							  name: returnData.daily.tool.seriesName1,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue1,
							  color: '#Ff8000',
							},
							{
							  name: returnData.daily.tool.seriesName2,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue2,
							  color: '#FF8D1A',
							},
							{
							  name: returnData.daily.tool.seriesName3,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue3,
							  color: '#Ff9933',
							},
							{
							  name: returnData.daily.tool.seriesName4,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue4,
							  color: '#FFA64D',
							},
							{
							  name: returnData.daily.tool.seriesName5,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue5,
							  color: '#FFB366',
							},
							{
							  name: returnData.daily.tool.seriesName6,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue6,
							  color: '#FFC080',
							},
							{
							  name: returnData.daily.tool.seriesName7,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue7,
							  color: '#FFCC99',
							},
							{
							  name: returnData.daily.tool.seriesName8,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue8,
							  color: '#FFD9B3',
							},
							{
							  name: returnData.daily.tool.seriesName9,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue9,
							  color: '#FFE6CC',
							},
							{
							  name: returnData.daily.tool.seriesName10,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue10,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName11,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue11,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName12,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue12,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName13,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue13,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName14,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue14,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName15,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue15,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName16,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue16,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName17,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue17,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName18,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue18,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName19,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue19,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName20,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue20,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName21,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue21,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName22,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue22,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName23,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue23,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName24,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue24,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName25,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue25,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName26,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue26,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName27,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue27,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName28,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue28,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName29,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue29,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName30,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue30,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName31,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue31,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName32,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue32,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName33,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue33,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName34,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue34,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName35,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue35,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName36,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue36,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName37,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue37,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName38,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue38,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName39,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue39,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName40,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue40,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName41,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue41,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName42,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue42,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName43,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue43,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName44,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue44,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName45,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue45,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName46,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue46,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName47,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue47,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName48,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue48,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName49,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue49,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName50,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue50,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName51,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue51,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName52,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue52,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName53,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue53,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName54,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue54,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName55,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue55,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName56,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue56,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName57,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue57,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName58,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue58,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName59,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue59,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName60,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue60,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName61,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue61,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName62,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue62,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName63,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue63,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName64,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue64,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName65,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue65,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName66,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue66,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName67,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue67,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName68,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue68,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName69,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue69,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName70,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue70,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName71,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue71,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName72,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue72,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName73,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue73,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName74,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue74,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName75,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue75,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName76,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue76,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName77,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue77,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName78,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue78,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName79,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue79,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName80,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue80,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName81,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue81,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName82,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue82,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName83,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue83,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName84,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue84,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName85,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue85,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName86,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue86,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName87,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue87,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName88,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue88,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName89,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue89,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName90,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue90,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName91,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue91,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName92,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue92,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName93,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue93,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName94,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue94,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName95,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue95,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName96,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue96,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName97,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue97,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName98,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue98,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName99,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue99,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.tool.seriesName100,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.tool.seriesValue100,
							  color: '#FFF2E6',
							},
							{
							  name: returnData.daily.operator.seriesName1,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue1,
							  color: '#124D8D',
							},
							{
							  name: returnData.daily.operator.seriesName2,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue2,
							  color: '#2A5F98',
							},
							{
							  name: returnData.daily.operator.seriesName3,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue3,
							  color: '#4171A4',
							},
							{
							  name: returnData.daily.operator.seriesName4,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue4,
							  color: '#5982AF',
							},
							{
							  name: returnData.daily.operator.seriesName5,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue5,
							  color: '#7194BB',
							},
							{
							  name: returnData.daily.operator.seriesName6,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue6,
							  color: '#89A6C6',
							},
							{
							  name: returnData.daily.operator.seriesName7,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue7,
							  color: '#A0B8D1',
							},
							{
							  name: returnData.daily.operator.seriesName8,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue8,
							  color: '#B8CADD',
							},
							{
							  name: returnData.daily.operator.seriesName9,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue9,
							  color: '#D0DBE8',
							},
							{
							  name: returnData.daily.operator.seriesName10,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue10,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName11,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue11,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName12,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue12,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName13,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue13,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName14,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue14,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName15,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue15,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName16,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue16,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName17,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue17,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName18,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue18,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName19,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue19,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName20,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue20,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName21,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue21,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName22,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue22,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName23,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue23,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName24,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue24,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName25,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue25,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName26,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue26,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName27,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue27,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName28,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue28,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName29,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue29,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName30,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue30,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName31,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue31,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName32,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue32,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName33,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue33,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName34,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue34,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName35,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue35,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName36,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue36,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName37,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue37,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName38,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue38,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName39,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue39,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName40,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue40,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName41,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue41,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName42,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue42,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName43,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue43,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName44,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue44,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName45,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue45,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName46,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue46,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName47,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue47,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName48,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue48,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName49,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue49,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName50,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue50,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName51,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue51,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName52,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue52,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName53,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue53,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName54,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue54,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName55,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue55,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName56,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue56,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName57,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue57,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName58,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue58,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName59,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue59,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName60,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue60,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName61,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue61,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName62,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue62,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName63,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue63,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName64,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue64,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName65,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue65,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName66,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue66,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName67,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue67,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName68,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue68,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName69,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue69,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName70,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue70,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName71,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue71,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName72,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue72,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName73,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue73,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName74,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue74,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName75,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue75,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName76,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue76,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName77,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue77,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName78,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue78,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName79,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue79,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName80,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue80,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName81,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue81,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName82,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue82,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName83,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue83,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName84,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue84,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName85,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue85,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName86,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue86,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName87,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue87,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName88,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue88,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName89,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue89,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName90,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue90,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName91,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue91,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName92,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue92,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName93,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue93,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName94,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue94,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName95,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue95,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName96,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue96,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName97,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue97,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName98,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue98,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName99,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue99,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.operator.seriesName100,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.operator.seriesValue100,
							  color: '#E7EDF4',
							},
							{
							  name: returnData.daily.material.seriesName1,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue1,
							  color: '#002060',
							},
							{
							  name: returnData.daily.material.seriesName2,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue2,
							  color: '#1A3670',
							},
							{
							  name: returnData.daily.material.seriesName3,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue3,
							  color: '#334D80',
							},
							{
							  name: returnData.daily.material.seriesName4,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue4,
							  color: '#4D6390',
							},
							{
							  name: returnData.daily.material.seriesName5,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue5,
							  color: '#6679A0',
							},
							{
							  name: returnData.daily.material.seriesName6,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue6,
							  color: '#8090B0',
							},
							{
							  name: returnData.daily.material.seriesName7,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue7,
							  color: '#99A6BF',
							},
							{
							  name: returnData.daily.material.seriesName8,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue8,
							  color: '#B3BCCF',
							},
							{
							  name: returnData.daily.material.seriesName9,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue9,
							  color: '#CCD2DF',
							},
							{
							  name: returnData.daily.material.seriesName10,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue10,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName11,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue11,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName12,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue12,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName13,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue13,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName14,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue14,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName15,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue15,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName16,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue16,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName17,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue17,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName18,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue18,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName19,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue19,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName20,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue20,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName21,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue21,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName22,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue22,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName23,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue23,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName24,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue24,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName25,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue25,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName26,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue26,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName27,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue27,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName28,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue28,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName29,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue29,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName30,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue30,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName31,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue31,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName32,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue32,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName33,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue33,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName34,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue34,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName35,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue35,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName36,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue36,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName37,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue37,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName38,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue38,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName39,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue39,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName40,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue40,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName41,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue41,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName42,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue42,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName43,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue43,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName44,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue44,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName45,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue45,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName46,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue46,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName47,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue47,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName48,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue48,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName49,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue49,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName50,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue50,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName51,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue51,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName52,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue52,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName53,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue53,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName54,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue54,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName55,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue55,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName56,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue56,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName57,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue57,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName58,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue58,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName59,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue59,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName60,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue60,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName61,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue61,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName62,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue62,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName63,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue63,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName64,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue64,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName65,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue65,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName66,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue66,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName67,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue67,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName68,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue68,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName69,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue69,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName70,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue70,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName71,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue71,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName72,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue72,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName73,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue73,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName74,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue74,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName75,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue75,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName76,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue76,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName77,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue77,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName78,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue78,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName79,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue79,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName80,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue80,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName81,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue81,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName82,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue82,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName83,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue83,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName84,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue84,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName85,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue85,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName86,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue86,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName87,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue87,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName88,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue88,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName89,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue89,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName90,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue90,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName91,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue91,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName92,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue92,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName93,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue93,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName94,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue94,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName95,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue95,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName96,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue96,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName97,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue97,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName98,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue98,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName99,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue99,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.material.seriesName100,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.material.seriesValue100,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.other.seriesName1,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue1,
							  color: '#CCD2DF',
							},
							{
							  name: returnData.daily.other.seriesName2,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue2,
							  color: '#D1D7E2',
							},
							{
							  name: returnData.daily.other.seriesName3,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue3,
							  color: '#D6DBE5',
							},
							{
							  name: returnData.daily.other.seriesName4,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue4,
							  color: '#DBE0E9',
							},
							{
							  name: returnData.daily.other.seriesName5,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue5,
							  color: '#E0E4EC',
							},
							{
							  name: returnData.daily.other.seriesName6,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue6,
							  color: '#E6E9EF',
							},
							{
							  name: returnData.daily.other.seriesName7,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue7,
							  color: '#EBEDF2',
							},
							{
							  name: returnData.daily.other.seriesName8,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue8,
							  color: '#F0F2F5',
							},
							{
							  name: returnData.daily.other.seriesName9,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue9,
							  color: '#F5F6F9',
							},
							{
							  name: returnData.daily.other.seriesName10,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue10,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName11,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue11,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName12,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue12,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName13,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue13,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName14,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue14,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName15,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue15,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName16,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue16,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName17,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue17,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName18,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue18,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName19,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue19,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName20,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue20,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName21,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue21,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName22,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue22,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName23,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue23,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName24,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue24,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName25,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue25,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName26,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue26,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName27,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue27,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName28,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue28,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName29,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue29,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName30,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue30,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName31,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue31,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName32,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue32,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName33,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue33,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName34,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue34,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName35,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue35,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName36,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue36,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName37,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue37,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName38,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue38,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName39,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue39,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName40,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue40,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName41,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue41,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName42,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue42,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName43,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue43,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName44,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue44,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName45,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue45,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName46,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue46,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName47,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue47,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName48,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue48,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName49,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue49,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName50,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue50,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName51,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue51,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName52,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue52,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName53,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue53,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName54,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue54,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName55,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue55,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName56,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue56,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName57,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue57,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName58,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue58,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName59,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue59,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName60,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue60,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName61,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue61,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName62,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue62,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName63,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue63,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName64,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue64,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName65,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue65,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName66,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue66,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName67,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue67,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName68,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue68,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName69,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue69,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName70,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue70,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName71,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue71,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName72,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue72,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName73,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue73,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName74,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue74,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName75,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue75,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName76,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue76,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName77,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue77,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName78,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue78,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName79,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue79,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName80,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue80,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName81,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue81,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName82,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue82,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName83,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue83,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName84,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue84,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName85,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue85,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName86,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue86,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName87,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue87,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName88,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue88,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName89,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue89,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName90,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue90,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName91,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue91,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName92,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue92,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName93,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue93,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName94,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue94,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName95,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue95,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName96,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue96,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName97,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue97,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName98,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue98,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName99,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue99,
							  color: '#FAFBFC',
							},
							{
							  name: returnData.daily.other.seriesName100,
							  type: 'bar',
							  stack: 1,
							  data: returnData.daily.other.seriesValue100,
							  color: '#FAFBFC',
							},
							{
								name: returnData.daily.unanswered.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.unanswered.seriesValue1,
								color: '#d32f2f',
							}

						],
					};  
					
						

						/*var seriesText1 = {
								name: returnData.daily.machine.seriesName1,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue1,
								color: '#FFCF00',
							};
						var seriesText2 = {
								name: returnData.daily.machine.seriesName2,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue2,
								color: '#FCD219',
							};
						var seriesText3 = {
								name: returnData.daily.machine.seriesName3,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue3,
								color: '#FFD933',
							};
						var seriesText4 = {
								name: returnData.daily.machine.seriesName4,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue4,
								color: '#FFDD4D',
							};
						var seriesText5 = {
								name: returnData.daily.machine.seriesName5,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue5,
								color: '#FFE266',
							};
						var seriesText6 = {
								name: returnData.daily.machine.seriesName6,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue6,
								color: '#FFE780',
							};
						var seriesText7 = {
								name: returnData.daily.machine.seriesName7,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue7,
								color: '#FFEC99',
							};
						var seriesText8 = {
								name: returnData.daily.machine.seriesName8,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue8,
								color: '#FFF1B3',
							};
						var seriesText9 = {
								name: returnData.daily.machine.seriesName9,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue9,
								color: '#FFF5CC',
							};
						var seriesText10 = {
								name: returnData.daily.machine.seriesName10,
								type: 'bar',
								stack: 1,
								data: returnData.daily.machine.seriesValue10,
								color: '#FFFAE6',
							};*/


					//DailyDowntime2.series.push(seriesText1,seriesText2,seriesText3,seriesText4,seriesText5,seriesText6,seriesText7,seriesText8,seriesText9,seriesText10);
					

					DailyDowntime2.setOption(DailyDowntime2Opt);


					
				},  'json');

			}



			
		}
		
	</script>


	<script type="text/javascript">

		$('#modal-changetimeadd').on('hidden.bs.modal', function () {
			$(".addBlock").css("opacity","0.5");
		})

		$('#modal-changetimeadd').on('shown.bs.modal', function () {
			$(".addBlock").css("opacity","1");
		})
		
		
	</script>

