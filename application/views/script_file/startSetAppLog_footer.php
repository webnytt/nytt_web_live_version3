 <script>
	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function filterMachineId(machineId, machineName)
	{
		// alert("hello");
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterMachineId.length > 0) 
		{
			$("#filterMachineIdSelected").css('color','#FF8000');

			if (in_array(machineId,filterMachineId) != -1) 
			{
				html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px; margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Machine; ?> - "+machineName+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

				$("#showMachinefilter").append(html);
			}
			else
			{
				$("#removeMachineButton"+machineId).remove();
			}
		}
		else
		{
			$("#showMachinefilter").html("");
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckMachineName(machineId)
	{
		$("#removeMachineButton"+machineId).remove();
		$("#filterMachineId"+machineId).prop('checked',false);
		var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterMachineId.length > 0) 
		{
		}
		else
		{
			$("#filterMachineIdSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterType()
	{
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{
			$("#filtertypeselected").css('color','#FF8000');

			var html = "";
			if (in_array("'0'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonManually' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Manually; ?> &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";
			}
			
			if (in_array("'1'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonSetApp' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo StartedfromOpApp; ?> &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";
			}

			if (in_array("'2'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonOppApp' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo StartedfromDashboard; ?> &nbsp;&nbsp;<i onclick='uncheckType(3)' class='fa fa-times'></i></button>";
			}

			if (in_array("'3'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Automaticallystartedafter30seconds' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo AutomaticallystartedafterTenseconds; ?> &nbsp;&nbsp;<i onclick='uncheckType(4)' class='fa fa-times'></i></button>";
			}

			if (in_array("'4'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='setupfterproductionstart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Setuptonoproduction; ?> &nbsp;&nbsp;<i onclick='uncheckType(5)' class='fa fa-times'></i></button>";
			}

			if (in_array("'5'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='noproductionafterproductionstart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Noproductiontoproduction; ?> &nbsp;&nbsp;<i onclick='uncheckType(6)' class='fa fa-times'></i></button>";
			}

			if (in_array("'6'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Batterytempafterstart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Batterytemperaturereachednormalafterhigh; ?> &nbsp;&nbsp;<i onclick='uncheckType(7)' class='fa fa-times'></i></button>";
			}

			if (in_array("'7'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='batterylowafterstart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Batterypercentagereachednormalafterlow; ?> &nbsp;&nbsp;<i onclick='uncheckType(8)' class='fa fa-times'></i></button>";
			}

			if (in_array("'8'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='ramhighafterstart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Ramreachedoperationvalueafterhigh; ?> &nbsp;&nbsp;<i onclick='uncheckType(9)' class='fa fa-times'></i></button>";
			}
			if (in_array("'9'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Duetolowmemory' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Duetolowmemory; ?> &nbsp;&nbsp;<i onclick='uncheckType(10)' class='fa fa-times'></i></button>";
			}
			if (in_array("'10'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='RestartedfromOpApp' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo RestartedfromOpApp; ?> &nbsp;&nbsp;<i onclick='uncheckType(11)' class='fa fa-times'></i></button>";
			}
			if (in_array("'11'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='RestartedfromDashboard' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo RestartedfromDashboard; ?> &nbsp;&nbsp;<i onclick='uncheckType(12)' class='fa fa-times'></i></button>";
			}
			if (in_array("'12'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='SetAppcrashedandrestartedautomatically' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo SetAppcrashedandrestartedautomatically; ?> &nbsp;&nbsp;<i onclick='uncheckType(13)' class='fa fa-times'></i></button>";
			}
			if (in_array("'13'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='Startappwhenphonerestart' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo Startappwhenphonerestart; ?> &nbsp;&nbsp;<i onclick='uncheckType(14)' class='fa fa-times'></i></button>";
			}
			if (in_array("'14'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='SetAppalwayson' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo SetAppautomaticallyrestartedinthirtysecondsafterunexpectedstop; ?> &nbsp;&nbsp;<i onclick='uncheckType(15)' class='fa fa-times'></i></button>";
			}
			if (in_array("'15'",filterType) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='UnexpectedReason' class='btn btn-sm btn-primary'><?php echo Type; ?> - <?php echo UnexpectedReason; ?> &nbsp;&nbsp;<i onclick='uncheckType(16)' class='fa fa-times'></i></button>";
			}
			$("#showRepeat").html(html);
		}
		else
		{
			$("#filtertypeselected").css('color','#b8b0b0');
			$("#showRepeat").html("");
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			$("#filterManually").prop('checked',false);
			$("#buttonManually").remove();
		}
		else if (id == "2") 
		{
			$("#filterSetApp").prop('checked',false);
			$("#buttonSetApp").remove();
		}
		else if (id == "3") 
		{
			$("#filterDashboard").prop('checked',false);
			$("#buttonOppApp").remove();
		}

		else if (id == "4") 
		{
			$("#filterAutomatically").prop('checked',false);
			$("#Automaticallystartedafter30seconds").remove();
		}

		else if (id == "5") 
		{
			$("#filtersetupafterproductionstart").prop('checked',false);
			$("#setupfterproductionstart").remove();
		}

		else if (id == "6") 
		{
			$("#filternoproductionafterproductionstart").prop('checked',false);
			$("#noproductionafterproductionstart").remove();
		}

		else if (id == "7") 
		{
			$("#filterBatterytempafterstart").prop('checked',false);
			$("#Batterytempafterstart").remove();
		}

		else if (id == "8") 
		{
			$("#filterbatterylowafterstart").prop('checked',false);
			$("#batterylowafterstart").remove();
		}

		else if (id == "9") 
		{
			$("#filterramhighafterstart").prop('checked',false);
			$("#ramhighafterstart").remove();
		}
		else if (id == "10") 
		{
			$("#filterDuetolowmemory").prop('checked',false);
			$("#Duetolowmemory").remove();
		}
		else if (id == "11") 
		{
			$("#filterRestartedfromOpApp").prop('checked',false);
			$("#RestartedfromOpApp").remove();
		}
		else if (id == "12") 
		{
			$("#filterRestartedfromDashboard").prop('checked',false);
			$("#RestartedfromDashboard").remove();
		}
		else if (id == "13") 
		{
			$("#filterSetAppcrashedandrestartedautomatically").prop('checked',false);
			$("#SetAppcrashedandrestartedautomatically").remove();
		}
		else if (id == "14") 
		{
			$("#filterStartappwhenphonerestart").prop('checked',false);
			$("#Startappwhenphonerestart").remove();
		}
		else if (id == "15") 
		{
			$("#filterSetAppalwayson").prop('checked',false);
			$("#SetAppalwayson").remove();
		}
		else if (id == "16") 
		{
			$("#filterUnexpectedReason").prop('checked',false);
			$("#UnexpectedReason").remove();
		}
		
		var filterType = $('.filterType:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();

		if (filterType.length > 0) 
		{

		}
		else
		{
			$("#filtertypeselected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function filterUserName(userId,userName)
	{
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{
			$("#filterUserNameSelected").css('color','#FF8000');
			if (in_array(userId,filterUserName) != -1) 
			{
				html = "<button id='removeUserButton"+userId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo UserName; ?> - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
				$("#showUserFilter").append(html);
			}
			else
			{
				$("#removeUserButton"+userId).remove();
			}
		}
		else
		{
			$("#showUserFilter").html("");
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}

	function uncheckUserName(userId)
	{
		$("#removeUserButton"+userId).remove();
		$("#filterUserName"+userId).prop('checked',false);
		var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (filterUserName.length > 0) 
		{

		}
		else
		{
			$("#filterUserNameSelected").css('color','#b8b0b0');
		}
		$("#empTable").DataTable().ajax.reload();
	}
    
	$(document).ready(function() 
	{
		
		$('.datepicker58').datepicker({
			format: "mm/dd/yyyy",
			autoclose : true,
            daysOfWeekDisabled: [0,6],
		});
		<?php 
		$dateValE = date("F d, Y", strtotime('today'));
		$dateValS =  date("F d, Y", strtotime('today - 29 days'));
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},
			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: '<?php echo Custom; ?>',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}

			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>App start time - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			
			$('#advance-daterange').css("color","#FF8000");
		});



		$('#empTable').removeAttr('width').DataTable({
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : false,
		  "lengthChange": false,
		  "stateSave": true,
		  'ajax': {
			  'url':'startSetAppLog_pagination',
			  "type": "POST",
				"data":function(data) {
					data.dateValS = $('#dateValS').val();
					data.dateValE = $('#dateValE').val();
					data.type = $('.filterType:checkbox:checked').map(function()
								{
								      return $(this).val();
							    }).get();
					data.userId = $('.filterUserName:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
					// data.machineId = $('.filtermachineName:checkbox:checked').map(function(){
					// 			      return $(this).val();
					// 			    }).get();

					data.machineId = $('.filterMachineId:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();

				},  
		  },
		  'columns': [
						 // { data: 'id' },
						 	{ data: 'machineName' },
						 	{ data: 'userName' },
						 	{ data: 'type' },
						 	{ data: 'startDate' }
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    },
			    {
			      targets: 3,
			      orderable: false,
			    }
			    // {
			    //   targets: 4,
			    //   orderable: false,
			    // }
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				
			},
	   });

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});

	}); 

 	function uncheckCreatedDate()
 	{
 		$("#showAddeddDate").html("");
 		$('#advance-daterange').css("color","#b8b0b0");

 		<?php 
		$dateValE =  date("Y-m-d");
		$dateValS =  date("Y-m-d", strtotime("-3000 day"));

		$dateValESET = date("Y-m-d", strtotime('today'));
		$dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
		
		?>
		var DdateValE = "<?php echo $dateValE; ?>"
		var DdateValS = "<?php echo $dateValS; ?>"

		$('#dateValS').val("<?php echo $dateValSSET; ?>");
		$('#dateValE').val("<?php echo $dateValESET; ?>");
		$('#advance-daterange').daterangepicker({
			format: 'MM/DD/YYYY',
			startDate: moment(DdateValS),
			endDate: moment(DdateValE), 
			minDate: '01/01/2018',
			maxDate: '12/31/2050',
			showDropdowns: true,
			showWeekNumbers: true,
			timePicker: false,
			timePickerIncrement: 1,
			timePicker12Hour: true,
			ranges: {
				'<?php echo Today; ?>': [moment(), moment()],
				'<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
				'<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
				'<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
				'<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
				'<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
			},

			opens: 'right',
			drops: 'down',
			buttonClasses: ['btn', 'btn-sm'],
			applyClass: 'btn-primary',
			cancelClass: 'btn-default',
			separator: ' to ',
			locale: {
				applyLabel: 'Submit',
				cancelLabel: 'Cancel',
				fromLabel: 'From',
				toLabel: 'To',
				customRangeLabel: 'Custom',
				daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI','SAT'],
				monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
				firstDay: 1
			}
		}, 

		function(start, end, label) 
		{
			if (label == "Custom") 
			{
				var startDate = new Date(start);
				var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

				var endDate = new Date(end);
				var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

				var label = label+"("+startDate+" - "+endDate+")";
			}
			$("#showAddeddDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Appstarttime; ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
			$('#advance-daterange').css("color","#FF8000");
		});

		$('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
		{ 
		  	var dateValS = picker.startDate.format('YYYY-MM-DD'); 
			var dateValE = picker.endDate.format('YYYY-MM-DD');  
			$('#dateValS').val(dateValS);
			$('#dateValE').val(dateValE);
			$("#empTable").DataTable().ajax.reload();
		});
		$("#empTable").DataTable().ajax.reload();
 	}

  </script>


