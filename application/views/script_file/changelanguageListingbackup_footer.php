 <script>

 	function changeLanguage(languageId)
 	{
		if (confirm('<?php echo Areyousureyouwanttoupdatethisswedishtranslation; ?>')) 
		{

        	var swedishText = $("#languageId"+languageId).val();
		    $.ajax({
                url: '<?php echo base_url("ExternalListing/updateSaveLanguage_backup") ?>',
                type: 'POST',
                data: {languageId: languageId,swedishText: swedishText},
                success: function (result) 
                {
                    //location.reload();
                    $("#languageId"+languageId).css("border-bottom-color","white");
                    $.gritter.add({
						title: '<?php echo Success; ?>',
						text: '<?php echo Swedishtranslationupdated; ?>'
					});
                }
            });
		} else
		{
			$("#languageId"+languageId).css("border-bottom-color","white");
		}
 	}


 	function lanBorderColor(languageId,updatedValue,newValue)
 	{
 		if (updatedValue != newValue) 
 		{
 			$("#languageId"+languageId).css("border-bottom-color","red");
 		}else
 		{
 			$("#languageId"+languageId).css("border-bottom-color","white");
 		}
 	}
 	 

 	function laguageType()
	{
		var type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (type.length > 0) 
		{
			$("#typeSelectedValue").css('color','#FF8000');
			var html = "";
			if (in_array("'dashboard'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='dashboardButton' class='btn btn-sm btn-primary'><?php echo type; ?> - Dashboard  &nbsp;&nbsp;<i onclick='uncheckType(1)' class='fa fa-times'></i></button>";

			}

			if (in_array("'opapp'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='opappButton' class='btn btn-sm btn-primary'><?php echo type; ?> - OpApp  &nbsp;&nbsp;<i onclick='uncheckType(2)' class='fa fa-times'></i></button>";

			}
			
			if (in_array("'setappapi'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='setappapiButton' class='btn btn-sm btn-primary'><?php echo type; ?> - SetAppApi  &nbsp;&nbsp;<i onclick='uncheckType(3)' class='fa fa-times'></i></button>";

			}

			if (in_array("'opappapi'",type) != -1) 
			{
				html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='opappapiButton' class='btn btn-sm btn-primary'><?php echo type; ?> - OpAppApi  &nbsp;&nbsp;<i onclick='uncheckType(4)' class='fa fa-times'></i></button>";

			}



			
			$("#showType").html(html);
		}
		else
		{
			$("#TypeSelectedValue").css('color','#b8b0b0');
			$("#showtype").html("");
		}


		$("#empTable").DataTable().ajax.reload();
	}

	function in_array(needle, haystack)
	{
	    var found = 0;
	    for (var i=0, len=haystack.length;i<len;i++) 
	    {
	        if (haystack[i] == needle) return i;
	            found++;
	    }
	    return -1;
	}

	function uncheckType(id)
	{
		if (id == "1") 
		{
			$("#laguageTypedashboard").prop('checked',false);
			$("#dashboardButton").remove();
		}
		
		else if(id == "2") 
		{
			$("#laguageTypeopapp").prop('checked',false);
			$("#opappButton").remove();
		}

		else if(id == "3") 
		{
			$("#laguageTypesetappapi").prop('checked',false);
			$("#setappapiButton").remove();
		}

		else if(id == "4") 
		{
			$("#laguageTypeopappapi").prop('checked',false);
			$("#opappapiButton").remove();
		}

		var type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
							    }).get();
		if (type.length > 0) 
		{
		
		}
		else
		{
			$("#typeSelectedValue").css('color','#b8b0b0');
		}

		$("#empTable").DataTable().ajax.reload();
	}

 	function editLanguage(languageId,englishText,swedishText)
	{
		$("#languageId").val(languageId);
		$("#englishText").text(englishText);
		$("#swedishText").val(swedishText);
		$("#editLanguage").modal();
	}

	function updateSaveLanguage()
   	{
   		var languageId = $("#languageId").val();
		var swedishText = $("#swedishText").val();

		 $.ajax({
                url: '<?php echo base_url("ExternalListing/updateSaveLanguage") ?>',
                type: 'POST',
                data: {languageId: languageId,swedishText: swedishText},
                success: function (result) {
                    location.reload();
                }
            });
   	}

   	
	$(document).ready(function() 
	{

		  $('.lanText').keyup(function(e){
 	   		console.log("lanText");
	          $(this).css('background-color','red');
	      
	    });


		$('#empTable').removeAttr('width').DataTable({
		  "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
		  'processing': true,
		  "pagingType": "input", 
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 100,
		  "info" : false,
		  "searching" : true,
		  "lengthChange": false,
		  "stateSave": true,
		  "rowId" : '',
		  'ajax': {
			  'url':'changelanguage_pagination_backup',
			  "type": "POST",
				"data":function(data) {
					data.type = $('.type:checkbox:checked').map(function(){
								      return $(this).val();
								    }).get();
				},  
		  },
		  'columns': [
						/* { data: 'languageId' },*/
						 { data: 'type' },
						 { data: 'englishText' },
						 { data: 'swedishText' },/*
						 { data: 'textIdentify' },*/
						
					],
		  "columnDefs": [
			  	{
			      targets: 0,
			      orderable: false,
			    },
			  	{
			      targets: 1,
			      orderable: false,
			    },
			    {
			      targets: 2,
			      orderable: false,
			    }/*,
			    {
			      targets: 3,
			      orderable: false,
			    },
			    {
			      targets: 4,
			      orderable: false,
			    }*/
			], 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
				 $(".paginate_page").html('<?php echo Page; ?>');
                var paginate_of = $(".paginate_of").text();
                var paginate_of = paginate_of.split(" ");
                $(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
			},
	   });

	});



	
 	

  </script>


