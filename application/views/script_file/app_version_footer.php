<script>

    function updateAppVersion(app_version_id)
    {
    	$("#app_version_id").val(app_version_id);
    	$("#modal-update-appVersion").modal();
    }
    $('form#add_appVersion_form').submit(function(e) 
	 { 
        var form = $(this);
        e.preventDefault();
        $("#add_appVersion_submit").html('<?php echo Saving; ?>...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('admin/add_appVersion'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                	       $.gritter.add({
                			title: '<?php echo Success; ?>',
                			text: "<?php echo Newversionforappaddedsuccessfully; ?>."
                		})
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#modal-add-appVersion").modal('hide').fadeOut(1500); 
                        $("#add_appVersion_submit").html('Save')
                        window.setTimeout(function(){location.reload()},3000)
                },
                error: function() { 
                    alert("<?php echo Errorwhileaddingstacklightversion; ?>.");
                    location.reload();
                }
           });
    });
    
    $('form#update_appVersion_form').submit(function(e) 
	{ 
        var form = $(this);
        e.preventDefault();
        $("#update_appVersion_submit").html('<?php echo Saving; ?>...')
        var formData = new FormData(this);
        $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('admin/update_appVersion'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    $.gritter.add({
            			title: '<?php echo Success; ?>',
            			text: "<?php echoNewversionfileupdatedsuccessfully; ?>."
            		})
                    form.each(function(){
                        this.reset();
                    }); 
                    $("#update_appVersion_submit").html('Save')
                    window.setTimeout(function(){location.reload()},3000)
                },
                error: function() { 
                    alert("<?php echo ErrorwhileaddingVersionfile; ?>");
                    location.reload();
                }
           });
    });
	$(document).ready(function() 
    {

        <?php if ($accessPointEdit == true) 
        {
            $columns = " [
                 { data: 'app_version_file' },
                 { data: 'app_version_name' },
                 { data: 'app_version_code' },
                 { data: 'created_date' },
                 { data: 'update_file' }
              ]";

              $columnDefs = "[
                             {
                              targets: 0,
                              orderable: false,
                            },
                            {
                              targets: 4,
                              orderable: false,
                            }
                        ]";
        }else 
        {
             $columns = " [
                 { data: 'app_version_file' },
                 { data: 'app_version_name' },
                 { data: 'app_version_code' },
                 { data: 'created_date' }
              ]";

              $columnDefs = "[
                            {
                              targets: 0,
                              orderable: false,
                            }
                        ]";
        } ?>
		$('#empTable').removeAttr('width').DataTable({ 
       "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
          "first": "<?php echo First; ?>",
          "last": "<?php echo Last; ?>",
          "next": "<?php echo Next; ?>",
          "previous": "<?php echo Previous; ?>",
          "page": "<?php echo Page; ?>",
          "of": "<?php echo of; ?>"
        }
          },
		  "pagingType": "input", 
		  'processing': true,
		  'serverSide': true,
		  'serverMethod': 'post',
		  "pageLength" : 50,
		  "order": [
			  [0, "desc" ] 
			],
		  'ajax': {
			  'url':'app_versions_pagination',
			  "type": "POST",
				"data":function(data) {
					data.appType = $('#appType').val();
				},  
		  },
		  'columns': <?php echo $columns; ?>,
		  "columnDefs": <?php echo $columnDefs; ?>, 
			fixedColumns: true, 
			drawCallback: function( settings ) { 
                $(".paginate_page").html('<?php echo Page; ?>');
                var paginate_of = $(".paginate_of").text();
                var paginate_of = paginate_of.split(" ");
                $(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
			},
	   }); 
	$("#filter a").click(function() {
			var clickedId = $(this).attr('id');
			var appType = clickedId.replace('appType',''); 
			$('#appType').val(appType);
			$("#filter a").removeClass('active');
			$(this).addClass('active');
			$("#empTable").DataTable().ajax.reload();
		});
	}); 
    </script>