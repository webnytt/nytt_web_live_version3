	<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  
	<script src="<?php echo base_url('assets/scroller_timepicker/picker.js'); ?>"></script>  
	<script>
		
		function selectView(viewId,viewName)
		{
			$("#viewId").val(viewId);
			$("#selectedView").text(viewName);
			$(".dropdown-item").removeClass('selectedView');
			$("#viewId"+viewId).addClass("selectedView");
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}


		function removeTransform(transformId)
		{
			if (confirm('<?php echo Areyousurewanttodelete.'?'; ?>')) 
			{

				var machineId = $("#machineId").val();

				var choose1 = $("#choose1").val();
				if (choose1 == "day") 
				{
					var choose2 = $("#choose2").val(); 
				} 
				else if (choose1 == "weekly") 
				{
					var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
				}
				else if (choose1 == "monthly") 
				{
					var choose2 = $("#choose4").val(); 
				}
				else if (choose1 == "yearly") 
				{
					var choose2 = $("#choose5").val();
				}
				
			    $.ajax({
					type: "POST",
					url: "<?php echo base_url('Analytics/deleteStateTransformer'); ?>", 
					data: {'transformId' : transformId,'machineId' : machineId,'choose1' : choose1,'choose2' : choose2}, 
					dataType: "html",
					success: function(data)
					{
						var obj = $.parseJSON(data);
						$("#transformId"+transformId).remove();
						if (obj.dataCount == 0) 
						{
							$("#transformDataRow").hide();
						}
						
					},
					error: function() 
					{ 
						
						location.reload();
					}
			   });
			} 

		}
		

		function stateTransformer()
		{
			$('.graph-loader').addClass("show").removeClass("hide");
			var machineId = $("#machineId").val();

			var choose1 = $("#choose1").val();
			if (choose1 == "day") 
			{
				var choose2 = $("#choose2").val(); 
			} 
			else if (choose1 == "weekly") 
			{
				var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
			}
			else if (choose1 == "monthly") 
			{
				var choose2 = $("#choose4").val(); 
			}
			else if (choose1 == "yearly") 
			{
				var choose2 = $("#choose5").val();
			}

			$("#transformData").html("");
				$.ajax({
					type: "POST",
					url: "<?php echo base_url('Analytics/getStateTransformer'); ?>", 
					data: {'machineId' : machineId,'choose1' : choose1,'choose2' : choose2}, 
					dataType: "html",
					success: function(data){
						var obj = $.parseJSON(data);

						if(obj.status == "1") 
						{	
							$('.graph-loader').addClass("hide").removeClass("show");
							$("#transformDataRow").show();
							$("#transformData").html(obj.transformData);
							
						}
						else 
						{ 	
							$('.graph-loader').addClass("hide").removeClass("show");
							$("#transformDataRow").hide();
						}

						$("#add-state-transformer").modal();
					},
					error: function() 
					{ 
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while updating machine.'
						});
						location.reload();
					}
			   });
			
		}

		function redirectPartsAnalytics()
		{
			var machineId = $("#machineId").val();
			location.href = '<?php echo base_url("Analytics/parts_analytics") ?>?machineId='+machineId;
		}
		
		analytics_type('wait_analytics');
		function analytics_type(type)
		{
			if (type == "wait_analytics") 
			{
				$(".stop_analytics").hide();
				$(".wait_analytics").show();
				$("#choose1").attr("disabled",false);
			}else
			{
				$(".wait_analytics").hide();
				$(".stop_analytics").show();
				$('#choose1 option[value=day]').attr('selected',false);
				$('#choose1 option[value=day]').attr('selected','selected');
				$("#choose1").attr("disabled",true);
				changeFilterType('day');
			}
		}
		
		function changeFilterType(value)
		{
			if (value == "day") 
			{
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").hide();
				$(".day").show();
				$("#headerChange").text("<?php echo Hourlymachineanalysis; ?>");
			}
			else if (value == "weekly") 
			{
				$(".day").hide();	
				$(".monthly").hide();
				$(".yearly").hide();
				$(".weekly").show();
				$("#headerChange").text("<?php echo Dailymachineanalysis; ?>");
			}
			else if (value == "monthly") 
			{
				$(".day").hide();	
				$(".yearly").hide();
				$(".weekly").hide();
				$(".monthly").show();
				$("#headerChange").text("<?php echo Dailymachineanalysis; ?>");
			}
			else if (value == "yearly") 
			{
				$(".day").hide();	
				$(".weekly").hide();
				$(".monthly").hide();
				$(".yearly").show();
				$("#headerChange").text("<?php echo Monthlymachineanalysis; ?>");
			}
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		function changeFilterValue()
		{
			machineId = $('#machineId').val();
			reloadGraph(machineId);
		}

		<?php if(!empty($_POST['mainChoose1'])) { ?>
			changeFilterType("<?php echo $_POST['mainChoose1'] ?>");
		<?php }else{ ?>
			changeFilterValue();
		<?php } ?>

		$(".slick-track").click(function() 
		{
			console.log('hello')
		});



		function reloadGraph(machineId) 
		{
			$("#machineIdParts").val(machineId);
			$("#machineIdStop").val(machineId);
			$("#machineIdWait").val(machineId);
			$('.graph-loader').addClass("show").removeClass("hide");
			var choose1 = $("#choose1").val(); 
			if (choose1 == "day") 
			{
				var choose2 = $("#choose2").val(); 
				var displayTooltip = "<?php echo seconds; ?>";
			} 
			else if (choose1 == "weekly") 
			{
				var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
				var displayTooltip = "<?php echo hours; ?>";
			}
			else if (choose1 == "monthly") 
			{
				var choose2 = $("#choose4").val(); 
				var displayTooltip = "<?php echo hours; ?>";
			}
			else if (choose1 == "yearly") 
			{
				var choose2 = $("#choose5").val(); 
				var displayTooltip = "<?php echo day; ?>";
			}
			$("#partsChoose1").val(choose1);
			$("#partsChoose2").val(choose2);

			$("#partsChooseStop1").val(choose1);
			$("#partsChooseStop2").val(choose2);

			$("#partsChooseWait1").val(choose1);
			$("#partsChooseWait2").val(choose2);
			var viewId = $("#viewId").val(); 

			$("#choose1").prop("disabled",true);
			$("#choose2").prop("disabled",true);
			$("#choose3").prop("disabled",true);
			$("#choose4").prop("disabled",true);
			$("#choose5").prop("disabled",true);
			$(".machineNameText").prop("disabled",true);
			$("#viewFilter").css('pointer-events','none');
			$("#viewFilter").css('opacity','0.4');
			$.post('<?php echo base_url();?>Analytics/dashboard5_pagination',{machineId:machineId,choose1:choose1,choose2:choose2,viewId:viewId}, function getattribute(returnData)
			{
				$("#choose1").prop("disabled",false);
				$("#choose2").prop("disabled",false);
				$("#choose3").prop("disabled",false);
				$("#choose4").prop("disabled",false);
				$("#choose5").prop("disabled",false);
				$(".machineNameText").prop("disabled",false);
				$("#viewFilter").css('pointer-events','fill');
				$("#viewFilter").css('opacity','1');
				$('.graph-loader').addClass("hide").removeClass("show");
				returnData = jQuery.parseJSON(returnData);

				$("#ordersData").html(returnData.ordersData);
				$("#currentOrderPartName").html(returnData.currentOrderPartName);

				if (returnData.currentOrderPartName == "") 
				{
					$(".partData").css("opacity","0.2");
					$("#ordersData").css("background-color","white");
					$(".noCycleTimeMessage").hide();
					$(".noPartMessage").show();
				}else
				{	
					if (returnData.CycleTimeFlag == true) 
					{
						$(".partData").css("opacity","0.2");
						$("#ordersData").css("background-color","white");
						$(".noPartMessage").hide();
						$(".noCycleTimeMessage").show();
					}else
					{	
						$(".partData").css("opacity","1");
						$("#ordersData").css("background-color","#f2f2f2");
						$(".noPartMessage").hide();
						$(".noCycleTimeMessage").hide();
					}
				}

				if (returnData.SetupText != "0") 
				{
					$("#SetupText").html('<span style="font-size: 16px;">'+returnData.SetupText.toFixed(1)+'</span> <?php echo hrs; ?>');
				}else
				{
					$("#SetupText").html('<span style="font-size: 16px;">'+returnData.SetupText+'</span> <?php echo hrs; ?>');
				}

				if (returnData.NoProductionText != "0") 
				{
					$("#NoProductionText").html('<span style="font-size: 16px;">'+returnData.NoProductionText.toFixed(1)+'</span> <?php echo hrs; ?>');
				}else
				{
					$("#NoProductionText").html('<span style="font-size: 16px;">'+returnData.NoProductionText+'</span> <?php echo hrs; ?>');
				}

				if (returnData.ActualProductionTextNew != "0") 
				{
					$("#ActualProductionText").html('<span style="font-size: 16px;">'+returnData.ActualProductionTextNew+'</span> <?php echo hrs; ?>');
				}else
				{
					$("#ActualProductionText").html('<span style="font-size: 16px;">'+returnData.ActualProductionTextNew+'</span> <?php echo hrs; ?>');
				}

//				var total = Number(returnData.ActualProductionText) + Number(returnData.SetupText) + Number(returnData.NoProductionText);

				var actualProductionRunning =  (returnData.ActualProductionRunningText != "0") ? (returnData.ActualProductionRunningText * 100) / returnData.ActualProductionText : 0;
				var actualProductionWaiting =  (returnData.ActualProductionWaitingText != "0") ? (returnData.ActualProductionWaitingText * 100) / returnData.ActualProductionText : 0;
				var actualProductionStopped =  (returnData.ActualProductionStoppedText != "0") ? (returnData.ActualProductionStoppedText * 100) / returnData.ActualProductionText : 0;
				var actualProductionOff =  (returnData.ActualProductionOffText != "0") ? (returnData.ActualProductionOffText * 100) / returnData.ActualProductionText : 0;
				var actualProductionNodet =  (returnData.ActualProductionNodetText != "0") ? (returnData.ActualProductionNodetText * 100) / returnData.ActualProductionText : 0;
				var actualProductionNoStacklightText =  (returnData.ActualProductionNoStacklightText != "0") ? (returnData.ActualProductionNoStacklightText * 100) / returnData.ActualProductionText : 0;

				

				$(".actualProductionRunning").css("width",actualProductionRunning+"%");
				$(".actualProductionWaiting").css("width",actualProductionWaiting+"%");
				$(".actualProductionStopped").css("width",actualProductionStopped+"%");
				$(".actualProductionOff").css("width",actualProductionOff+"%");
				$(".actualProductionNodet").css("width",actualProductionNodet+"%");
				$(".actualProductionNoStacklightText").css("width",actualProductionNoStacklightText+"%");

				var actualProductionRunningDis = (actualProductionRunning > 0) ? ' ('+ actualProductionRunning.toFixed(1)+'%) ' : ' (0%) ';
				var actualProductionWaitingDis = (actualProductionWaiting > 0) ? ' ('+ actualProductionWaiting.toFixed(1)+'%) ' : ' (0%) ';
				var actualProductionStoppedDis = (actualProductionStopped > 0) ? ' ('+ actualProductionStopped.toFixed(1)+'%) ' : ' (0%) ';
				var actualProductionOffDis = (actualProductionOff > 0) ? ' ('+ actualProductionOff.toFixed(1)+'%) ' : ' (0%) ';
				var actualProductionNodetDis = (actualProductionNodet > 0) ? ' ('+ actualProductionNodet.toFixed(1)+'%) ' : ' (0%) ';
				var actualProductionNoStacklightTextDis = (actualProductionNoStacklightText > 0) ? ' ('+ actualProductionNoStacklightText.toFixed(1)+'%) ' : ' (0%) ';

				if(actualProductionNoStacklightText > 0)
				{
					var actualProductionRunningDisGraph = (returnData.ActualProductionNoStacklightText * 100) / returnData.maxHour;
					var actualProductionRunning = (returnData.ActualProductionNoStacklightText * 100) / returnData.maxHour;
					var actualProductionRunningDisGraph = actualProductionRunningDisGraph.toFixed(1);
					var actualProductionNoStacklightTextDis =  ' ('+ actualProductionRunningDisGraph+'%) ';
				}else
				{
					var actualProductionRunningDisGraph = (actualProductionRunning > 0) ?  actualProductionRunning.toFixed(1) : ' 0 ';
				}


				$(".actualProductionRunning").attr('data-tooltip',returnData.ActualProductionRunningText+actualProductionRunningDis+" <?php echo hrsofrunningtime; ?>");
				$(".actualProductionWaiting").attr('data-tooltip',returnData.ActualProductionWaitingText+actualProductionWaitingDis+" <?php echo hrsofwaitingtime; ?>");
				$(".actualProductionStopped").attr('data-tooltip',returnData.ActualProductionStoppedText+actualProductionStoppedDis+" <?php echo hrsofstoppedtime; ?>");
				$(".actualProductionOff").attr('data-tooltip',returnData.ActualProductionOffText+actualProductionOffDis+" <?php echo hrsofofftime; ?>");
				$(".actualProductionNodet").attr('data-tooltip',returnData.ActualProductionNodetText+actualProductionNodetDis+" <?php echo hrsofnodatatime; ?>");
				$(".actualProductionNoStacklightText").attr('data-tooltip',returnData.ActualProductionNoStacklightText+actualProductionNoStacklightTextDis+" <?php echo  hrsofnostacklighttime; ?>");


				$("#goodPartsCount").text( (returnData.goodParts > 0) ? returnData.goodParts : 0);
				$("#scrapPartsCount").text( (returnData.scrapParts > 0) ? returnData.scrapParts : 0);

			/*	if(returnData.scrapParts == 0)
				{
					$("#scrapPartsData").show();
				}else{
					$("#scrapPartsData").show();
				}*/


				$("#actualProductionRunningText").html((actualProductionRunning > 10) ? actualProductionRunning.toFixed(1)+"%" : actualProductionRunning.toFixed(1)+"%");
				$("#actualProductionWaitingText").html((actualProductionWaiting > 10) ? "&nbsp;" : "&nbsp;");
				$("#actualProductionStoppedText").html((actualProductionStopped > 10) ? "&nbsp;" : "&nbsp;");
				$("#actualProductionOffText").html((actualProductionOff > 10) ? "&nbsp;" : "&nbsp;");
				$("#actualProductionNodetText").html((actualProductionNodet > 10) ? "&nbsp;" : "&nbsp;");
				$("#actualProductionNoStacklightTextText").html((actualProductionNoStacklightText > 10) ? "&nbsp;" : "&nbsp;");

				

				if (choose1 == "day") 
				{

					var screenWithCount = window.innerWidth;
					var columnCount = returnData.xAxisData.length;
					if (screenWithCount > 1200 && screenWithCount < 1850) 
					{
						var DailyGraph1Width = $('#DailyGraph1').width() - 150;
					}
					else
					{
						var DailyGraph1Width = $('#DailyGraph1').width() + 350;
					}
					var paddingCount =  DailyGraph1Width/ (columnCount *2); 
					var paddingText = [0, paddingCount , 0, 0]; 
				}
				else
				{
					var paddingText = [0, 0, 0, 0];
				}
				
				if (Math.round(returnData.machinePartsData.dayThrouputActual) > 0 || Math.round(actualProductionRunningDisGraph) > 0 || Math.round(returnData.ActualProductionTextNew) > 0 || Math.round(returnData.machinePartsData.dayCycleActual) > 0) 
				{
					var partsValue = [Math.round(returnData.machinePartsData.dayThrouputActual), Math.round(actualProductionRunningDisGraph), returnData.ActualProductionTextNew, Math.round(returnData.machinePartsData.dayCycleActual)];
					if (returnData.CycleTimeFlag == false) 
					{
						$(".partData").css("opacity","1");
						$(".noPartMessage").hide();
					}
				}else
				{
					var partsValue = [null, null, null, null];
				}

				var dayThrouputMaxTextValue = (returnData.machinePartsData.dayThrouputMax == 0) ? 1 : returnData.machinePartsData.dayThrouputMax;
				var dayCycleMaxTextValue = (returnData.machinePartsData.dayCycleMax == 0) ? 1 : returnData.machinePartsData.dayCycleMax; 

				var DailyGraph5 = echarts.init(document.getElementById('DailyGraph5'));  
				DailyGraph5.setOption(

				option = {
					tooltip: {
				        trigger: 'axis'
				    },
				    formatter: 
					function (params) 
					{
						var outStrng = "<?php echo Max; ?> " + Math.round(returnData.machinePartsData.dayThrouputMaxText) + " <?php echo THROUGHPUTRATE; ?> <br><?php echo Max; ?> 100% <?php echo PRODUCTIVITY; ?> <br><?php echo Max; ?> "+ returnData.maxHour +" <?php echo strtolower(hrs); ?> <br><?php echo Max; ?>  "+ Math.floor(returnData.machinePartsData.dayCycleMax) + " <?php echo CYCLES; ?>";
						return outStrng; 
					}, 
				    radar: {
				        indicator: [
				            { text: Math.floor(returnData.machinePartsData.dayThrouputActualText)+' <?php echo partshr; ?>\n <?php echo THROUGHPUTRATE; ?>', max: Math.round(dayThrouputMaxTextValue)},
				            { text: actualProductionRunningDisGraph +'% \n <?php echo PRODUCTIVITY; ?>', max: 100},
				            { text: returnData.ActualProductionTextNew +' <?php echo hrs; ?> \n <?php echo Productiontime; ?>', max: returnData.maxHour},
				            { text: Math.floor(returnData.machinePartsData.dayCycleActual)+'\n<?php echo (estimated ); ?>\n<?php echo parts; ?>', max: Math.floor(dayCycleMaxTextValue)}
				        ],
				        radius: 63,
				        name: {
			                textStyle: {
			                    color: '#002060'
			                }
			            },
				    },
				    series: [
				    {
				        type: 'radar',
				        tooltip: {
			                trigger: 'item'
			            },
				        name: '',
				        color: '#002060',
			            areaStyle: {color: '#ced7eb'},
				        data: [
				            {
				                value: partsValue,
				                name: ''
				            }
				        ]
				    }]
				}

				, true);



				var maxValue = returnData.maxValue;
				if (maxValue % 2 == 0) 
				{
					
				}
				else
				{
					var maxValue = maxValue + 1;
				}
				if (returnData.maxValue == 0) 
				{
					maxValue = 3600;
				}

				var intervalValue = maxValue / 8;

				if (returnData.machineId == 0) 
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
						},
						formatter: 
							function (params) 
							{
								var outStrng = params[0].name+"<br /><?php echo Running; ?> : "+(parseFloat(params[1].data) + parseFloat(params[9].data))+" "+displayTooltip+"<br /><?php echo Waiting; ?>: "+(parseFloat(params[2].data) + parseFloat(params[10].data))+" "+displayTooltip+"<br /><?php echo Stopped; ?>: "+(parseFloat(params[3].data) + parseFloat(params[11].data)) +" "+displayTooltip+"<br /><?php echo Off; ?>: "+(parseFloat(params[4].data) + parseFloat(params[12].data))+" "+displayTooltip+"<br /><?php echo Nodet; ?>: "+(parseFloat(params[5].data) + parseFloat(params[13].data))+" "+displayTooltip+"<br /><?php echo Setuptime; ?>: "+(parseFloat(params[6].data) + parseFloat(params[14].data))+" "+displayTooltip+"<br /><?php echo Noproduction; ?>: "+(parseFloat(params[7].data) + parseFloat(params[14].data))+" "+displayTooltip+"<br /><?php echo Productiontime; ?>: "+(parseFloat(params[0].data) + parseFloat(params[8].data))+" "+displayTooltip;
								return outStrng; 
							}, 
						grid: {
							left: '1%',
							right: '8%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
            				axisLabel: { 
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
						},
						series: [
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducation,
								color: '#002060',
							},
							{
								name: '<?php echo Running; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Running,
								color: '#76BA1B',
							}, 
							{
								name: '<?php echo Waiting; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Waiting,
								color: '#FFCF00',
							},
							{
								name: '<?php echo Stopped; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Stopped,
								color: '#F60100',
							},
							{
								name: '<?php echo Off; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Off,
								color: '#CCD2DF',
							},
							{
								name: '<?php echo Nodet; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Nodet,
								color: '#000000',
							},
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							},
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducationBreak,
								color: '#ced1d8',
							},
							{
								name: '<?php echo Running; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.RunningBreak,
								color: '#bfd4a3',
							}, 
							{
								name: '<?php echo Waiting; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.WaitingBreak,
								color: '#dece89',
							},
							{
								name: '<?php echo Stopped; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.StoppedBreak,
								color: '#d28989',
							},
							{
								name: '<?php echo Off; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.OffBreak,
								color: '#dce1ec',
							},
							{
								name: '<?php echo Nodet; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.NodetBreak,
								color: '#aba3a3',
							},
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetupBreak,
								color: '#9ca6b1',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducationBreak,
								color: '#d2c5b8',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
				}
				else if (returnData.noStacklight == "1") 
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
							formatter: 
							function (params) {
								var outStrng = params[0].name+"<br /><?php echo Setuptime; ?> : "+(parseFloat(params[0].data) + parseFloat(params[3].data))+"<br /><?php echo Noproduction; ?>: "+(parseFloat(params[1].data) + parseFloat(params[4].data))+"<br /><?php echo Productiontime; ?>: "+(parseFloat(params[2].data) + parseFloat(params[5].data));
								return outStrng; 
							},
						},

						grid: {
							left: '1%',
							right: '8%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
							axisLabel: {
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
							
						},
						series: [
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							},
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducation,
								color: '#002060',
							},
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetupBreak,
								color: '#9ca6b1',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducationBreak,
								color: '#d2c5b8',
							},
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducationBreak,
								color: '#ced1d8',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
				}
				else
				{
					var Yearly1 = document.getElementById('DailyGraph1');
					var YearlyGraph1 = echarts.init(Yearly1);  
					var YearlyGraph1Opt = {
						tooltip : {
							trigger: 'axis',
							axisPointer : {            
								type : 'shadow'
							},
							formatter: 
							function (params) {
								var outStrng = params[0].name+"<br /><?php echo Running; ?> : "+(parseFloat(params[0].data) + parseFloat(params[8].data))+" "+displayTooltip+"<br /><?php echo Waiting; ?>: "+(parseFloat(params[1].data) + parseFloat(params[9].data))+" "+displayTooltip+"<br /><?php echo Stopped; ?>: "+(parseFloat(params[2].data) + parseFloat(params[10].data)) +" "+displayTooltip+"<br /><?php echo Off; ?>: "+(parseFloat(params[3].data) + parseFloat(params[11].data))+" "+displayTooltip+"<br /><?php echo Nodet; ?>: "+(parseFloat(params[4].data) + parseFloat(params[12].data))+" "+displayTooltip+"<br /><?php echo Setuptime; ?>: "+(parseFloat(params[5].data) + parseFloat(params[13].data))+" "+displayTooltip+"<br /><?php echo Noproduction; ?>: "+(parseFloat(params[6].data) + parseFloat(params[13].data))+" "+displayTooltip+"<br /><?php echo Productiontime; ?>: "+(parseFloat(params[7].data) + parseFloat(params[14].data))+" "+displayTooltip;
								return outStrng; 
							}, 
						},
						grid: {
							left: '1%',
							right: '8%',
							containLabel: true
						},
						yAxis:  {
							type: 'value',
							axisLabel: {
								color:"#000000",
								fontWeight: 500
							},
							name: returnData.yAxisName,
							nameTextStyle: {
								color:"#000000",
								fontWeight: 500
							},
							splitLine: {
								show:true, 
							},
							interval:intervalValue,
							max:maxValue
						}, 
						xAxis: {
							type: 'category',
							data: returnData.xAxisData,
							name: returnData.xAxisName ,
							axisLabel: {
								padding : paddingText
							},
							nameTextStyle: {
								color:"#000000",
							},
							
						},
						series: [
							{
								name: '<?php echo Running; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Running,
								color: '#76BA1B',
							}, 
							{
								name: '<?php echo Waiting; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Waiting,
								color: '#FFCF00',
							},
							{
								name: '<?php echo Stopped; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Stopped,
								color: '#F60100',
							},
							{
								name: '<?php echo Off; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Off,
								color: '#CCD2DF',
							},
							{
								name: '<?php echo Nodet; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.Nodet,
								color: '#000000',
							},
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetup,
								color: '#124D8D',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducation,
								color: '#FF8000',
							},
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducation,
								color: '#002060',
							},
							{
								name: '<?php echo Running; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.RunningBreak,
								color: '#bfd4a3',
							}, 
							{
								name: '<?php echo Waiting; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.WaitingBreak,
								color: '#dece89',
							},
							{
								name: '<?php echo Stopped; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.StoppedBreak,
								color: '#d28989',
							},
							{
								name: '<?php echo Off; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.OffBreak,
								color: '#dce1ec',
							},
							{
								name: '<?php echo Nodet; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.NodetBreak,
								color: '#aba3a3',
							},
							{
								name: '<?php echo Setuptime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineSetupBreak,
								color: '#9ca6b1',
							},
							{
								name: '<?php echo Noproduction; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineNoProducationBreak,
								color: '#d2c5b8',
							},
							{
								name: '<?php echo Productiontime; ?>',
								type: 'bar',
								stack: 1,
								data: returnData.MachineActualProducationBreak,
								color: '#ced1d8',
							}
						]
					};
					
					YearlyGraph1.setOption(YearlyGraph1Opt, true);
					}
				});
			}





		$(document).ready(function() 
		{ 

			$('.datepicker').datepicker({
					format: "mm/dd/yyyy",
					endDate: "today",
					language: "<?php echo datepickerLanguage; ?>",
					autoclose: true
				});
			$('.datepicker1').datepicker({
					format: "M yyyy",
				    viewMode: "months", 
				    minViewMode: "months",
				    language: "<?php echo datepickerLanguage; ?>",
				    autoclose: true
				});

			$(".picker-minutes").attr('data-type','');

			$(".regular").slick({
		        dots: false,
		        infinite: false,
		        slidesToShow: 4,
		        slidesToScroll: 4,
		          responsive: [
			        {
			          breakpoint: 1024,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 3,
			            slidesToScroll: 3
			          }
			        },
			        {
			          breakpoint: 600,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 2,
			            slidesToScroll: 2
			          }
			        },
			        {
			          breakpoint: 480,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 1,
			            slidesToScroll: 1
			        }
			        
			        }
			     ],
			});

			$(".betweenStartTimeSlider").slick({
		        dots: false,
		        infinite: false,
		        slidesToShow: 3,
		        slidesToScroll: 3,
		          responsive: [
			        {
			          breakpoint: 1024,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 3,
			            slidesToScroll: 3
			          }
			        },
			        {
			          breakpoint: 600,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 2,
			            slidesToScroll: 2
			          }
			        },
			        {
			          breakpoint: 480,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 1,
			            slidesToScroll: 1
			          }
			        }
			      ],

		      });

			$(".betweenEndTimeSlider").slick({
		        dots: false,
		        infinite: false,
		        slidesToShow: 3,
		        slidesToScroll: 3,
		          responsive: [
			        {
			          breakpoint: 1024,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 3,
			            slidesToScroll: 3
			          }
			        },
			        {
			          breakpoint: 600,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 2,
			            slidesToScroll: 2
			          }
			        },
			        {
			          breakpoint: 480,
			          settings: {
			          	dots: false,
		        		infinite: false,
			            slidesToShow: 1,
			            slidesToScroll: 1
			          }
			        }
			      ],

		      });

			$(".filter").click(function(e) 
			{
				e.preventDefault();
				var clickedId = $(this).attr('id');
				var machineId = clickedId.replace('machine',''); 
				$('#machineId').val(machineId);
				$(".filter").removeClass('active');
				$(this).addClass('active');
				reloadGraph(machineId);
			});

			$(window).on('resize orientationChange', function(event) { //check window.width()... 
				$('.regular').slick('init');
				$('.regular').slick('reinit');
				$(".filter").click(function(e) {
					e.preventDefault();
					var clickedId = $(this).attr('id');
					var machineId = clickedId.replace('machine',''); 
					$('#machineId').val(machineId);
					$(".filter").removeClass('active');
					$(this).addClass('active');
					reloadGraph(machineId);
				});
			});
		});


		

		function in_array(needle, haystack)
		{
		    var found = 0;
		    for (var i=0, len=haystack.length;i<len;i++) 
		    {
		        if (haystack[i] == needle) return i;
		            found++;
		    }
		    return -1;
		}

		function selectStopTime(selectedBox)
		{
			var stopTimes = $("#stopTimes").val().split(",");
			if (in_array(selectedBox,stopTimes)!= -1) 
			{
				$("#selectStopTime"+selectedBox).css("opacity","0.5");
				for(var i = stopTimes.length - 1; i >= 0; i--) {
					if(stopTimes[i] == selectedBox) {
				        stopTimes.splice(i, 1);
				    }
				}
				
				var selectedselectedBox = stopTimes.join(",");
				$("#stopTimes").val(selectedselectedBox);

			}
			else
			{   
				var checkstopTimes = $("#stopTimes").val();
				if (checkstopTimes != "") 
				{
					var selectedselectedBox = $("#stopTimes").val()+','+selectedBox;
					$("#stopTimes").val(selectedselectedBox);
				}
				else
				{
					$("#stopTimes").val(selectedBox);
				}
				$("#selectStopTime"+selectedBox).css("opacity","1");
			}
		
		}
	</script>
  	<script type="text/javascript">
  	new Picker(document.querySelector('.js-inline-picker-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
      mindate: new Date(0, 0, 0, 1, 0),
      maxdate: new Date(0, 0, 0, 10, 0),
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});

  	new Picker(document.querySelector('.js-inline-picker-break-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-break-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});

  	new Picker(document.querySelector('.js-inline-picker-transform-from'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-transform-to'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});

 /* 	new Picker(document.querySelector('.js-inline-picker-edit-break-start'), 
  	{
      format: 'HH:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
	});

    new Picker(document.querySelector('.js-inline-picker-edit-break-stop'), 
    {
      format: 'AA:mm',
      date: new Date(0, 0, 0, 1, 0),
	  controls: true,
	  inline: true,
  	});*/


  	$('form#add_view_form').submit(function(e) 
	{ 
		var startTimeHour =  $(".js-inline-picker-start .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-start .picker-minutes .picker-picked").attr('data-value');

		var stopTimeHour =  $(".js-inline-picker-stop .picker-hours .picker-picked").attr('data-value');
		var stopTimeMinutes =  $(".js-inline-picker-stop .picker-minutes .picker-picked").attr('data-value');

		var form = $(this);
	    e.preventDefault();
		if (Number(startTimeHour) >= Number(stopTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else
		{	
	   		$("#add_view_submit").attr('disabled',true); 
			$("#add_view_submit").html('Saving...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/add_view'); ?>",
				data: {'formData': form.MytoJson(),'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': stopTimeHour+':'+stopTimeMinutes},  
				dataType: "html",
				success: function(data)
				{
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#add_view_submit").attr('disabled',false); 
						$("#add_view_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add')
					}
				},
				error: function() { 
					$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding task'
						});
					$("#add_view_submit").attr('disabled',false);
					$("#add_view_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> Add')
				}
		   });
		}
    }); 

    $('form#add_transform_form').submit(function(e) 
	{ 

		var machineId = $("#machineId").val(); 
		var choose1 = $("#choose1").val(); 
		if (choose1 == "day") 
		{
			var choose2 = $("#choose2").val(); 
		} 
		else if (choose1 == "weekly") 
		{
			var choose2 = $("#choose3").val()+"/"+$("#choose6").val(); 
		}
		else if (choose1 == "monthly") 
		{
			var choose2 = $("#choose4").val(); 
		}
		else if (choose1 == "yearly") 
		{
			var choose2 = $("#choose5").val();
		}
		var betweenStartTimeHour =  $(".js-inline-picker-transform-from .picker-hours .picker-picked").attr('data-value');

		var betweenEndTimeHour =  $(".js-inline-picker-transform-to .picker-hours .picker-picked").attr('data-value');

		var form = $(this);
	    e.preventDefault();
		if (Number(betweenStartTimeHour) >= Number(betweenEndTimeHour)) 
		{
			alert("<?php echo Pleasechoosevalidduration; ?>");
		}
		else
		{	
	   		$("#add_transformer_submit").attr('disabled',true); 
			$("#add_transformer_submit").html('<?php echo Saving.'...'; ?>'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/add_save_transform'); ?>",
				data: {'formData': form.MytoJson(),'betweenStartTime': betweenStartTimeHour,'betweenEndTime': betweenEndTimeHour,'choose1': choose1,'choose2': choose2,'machineId': machineId},  
				dataType: "html",
				success: function(data)
				{
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						$("#add_transformer_submit").attr('disabled',false); 
						$("#add_transformer_submit").html('Add');
						$("#transformDataRow").show();
						$("#transformName").val("");
						$("#transformData").append(obj.transformData);
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#add_transformer_submit").attr('disabled',false); 
						$("#add_transformer_submit").html('Add')
					}
				},
				error: function() { 
					$.gritter.add({
							title: '<?php echo Error; ?>',
							text: 'Error while adding task'
						});
					$("#add_transformer_submit").attr('disabled',false);
					$("#add_transformer_submit").html('Add')
				}
		   });
		}
    });

    function addBreak()
    {
    	$(".submitButton").hide();
    	$(".addBreakButton").hide();
    	$(".removeBreak").fadeIn();
    }
    
    function addTransformer()
    {
    	$(".submitButton").hide();
    	$(".addBreakButton").hide();
    	$(".removeBreak").fadeIn();
    }

	function removeBreak()
    {
    	$(".removeBreak").hide();
    	$(".submitButton").fadeIn();
    	$(".addBreakButton").fadeIn();
    }

    var i = 1;
    function saveBreaks()
    {
    	var startTimeHourAdd =  $(".js-inline-picker-start .picker-hours .picker-picked").attr('data-value');
		var stopTimeHourAdd =  $(".js-inline-picker-stop .picker-hours .picker-picked").attr('data-value');

    	var startTimeHour =  $(".js-inline-picker-break-start .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-break-start .picker-minutes .picker-picked").attr('data-value');

		var endTimeHour =  $(".js-inline-picker-break-stop .picker-hours .picker-picked").attr('data-value');
		var endTimeMinutes =  $(".js-inline-picker-break-stop .picker-minutes .picker-picked").attr('data-value');

		if (Number(startTimeHour) >= Number(endTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else if(Number(startTimeHourAdd) <= Number(startTimeHour) && Number(stopTimeHourAdd) >= Number(endTimeHour)) 
		{

			var html = '<div class="row" id="breakRow'+i+'"><div class="col-md-10"><span style="font-size: 16px;"><?php echo Breakstartsfrom; ?> </span><span style="color: #FF8000;font-size: 16px;">'+startTimeHour+':'+startTimeMinutes+'</span><span style="font-size: 16px;">&nbsp;&nbsp;to&nbsp;&nbsp;</span><span style="color: #FF8000;font-size: 16px;">'+endTimeHour+':'+endTimeMinutes+'</span></div><div class="col-md-2"><img onclick="removeBreakDataAdd('+i+')" style="width: 20px;" src="<?php echo base_url('assets/img/cross.svg'); ?>"></div><input type="hidden" name="breakStartTimes[]" value="'+startTimeHour+':'+startTimeMinutes+'"><input type="hidden" name="breakEndTimes[]" value="'+endTimeHour+':'+endTimeMinutes+'"></div>';

			$(".addBreakData").append(html);
			$(".removeBreak").hide();
	    	$(".addBreakData").fadeIn();
	    	$(".submitButton").fadeIn();
	    	$(".addBreakButton").fadeIn();
	    }
	    else
	    {
			alert("<?php echo Pleasechoosevalidduration; ?>");
	    }

    }


    function removeBreakDataAdd(breakViewId)
    {
    	$("#breakRow"+breakViewId).remove();
    }
    function removeBreakData(breakViewId)
    {
    	$('.graph-loader').addClass("show").removeClass("hide");
    	$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/removeBreakTime'); ?>", 
			data: {breakViewId:breakViewId}, 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$('.graph-loader').addClass("hide").removeClass("show");
					$("#breakRowEdit"+breakViewId).remove();
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
				}
			},
			error: function() { 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: '<?php echo Errorwhileupdatingmachine; ?>.'
				});
				location.reload();
			}
	   });
	}


    function editView(viewId)
    {
    	$('.graph-loader').addClass("show").removeClass("hide");
    	$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/getViewDataById'); ?>", 
			data: {viewId:viewId}, 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$('.graph-loader').addClass("hide").removeClass("show");
					new Picker(document.querySelector('.js-inline-picker-edit-start-dynamic'+viewId), 
					{
				      format: 'HH:mm',
				      date: new Date(0, 0, 0, obj.result.startTimeHour, 0),
					  controls: true,
					  inline: true,
					});

				    new Picker(document.querySelector('.js-inline-picker-edit-stop-dynamic'+viewId), 
				    {
				      format: 'AA:mm',
				      date: new Date(0, 0, 0, obj.result.stopTimeHour, 0),
					  controls: true,
					  inline: true,
				  	}); 

				  	new Picker(document.querySelector('.js-inline-picker-edit-break-start-dynamic'+viewId), 
				  	{
				      format: 'HH:mm',
				      date: new Date(0, 0, 0, 1, 0),
					  controls: true,
					  inline: true,
					});

				    new Picker(document.querySelector('.js-inline-picker-edit-break-stop-dynamic'+viewId), 
				    {
				      format: 'AA:mm',
				      date: new Date(0, 0, 0, 1, 0),
					  controls: true,
					  inline: true,
				  	});

				  	
				  	$(".picker-minutes").attr('data-type','');
					$("#deleteView").attr("onclick","deleteView("+viewId+")");
					$("#viewIdEdit-dynamic"+viewId).val(obj.result.viewId);
					$("#viewNameEdit-dynamic"+viewId).val(obj.result.viewName);
					$(".addEditBreakData-dynamic"+viewId).html(obj.result.breakData);
    				$("#edit-view-modal-dynamic"+viewId).modal();
    				$(".addEditBreakData-dynamic"+viewId).fadeIn();
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
				}
			},
			error: function() { 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: 'Error while updating machine.'
				});
				location.reload();
			}
	   });
    }


    function deleteView(viewId)
    {
    	$("#deleteViewId").val(viewId);
    	$("#delete-view-modal").modal();
    }


    $("form[id^='delete_view_form']").submit(function(e) 
    {  
		var form = $(this);
		e.preventDefault();
		
		$("#delete_view_submit").attr('disabled',true); 
		$("#delete_view_submit").html('Deleting...'); 
		
		$.ajax({
			type: "POST",
			url: "<?php echo site_url('Analytics/delete_view'); ?>", 
			data: form.serialize(), 
			dataType: "html",
			success: function(data){
				var obj = $.parseJSON($.trim(data));
				if(obj.status == "1") 
				{	
					$.gritter.add({
						title: '<?php echo Success; ?>',
						text: obj.message
					});
					window.setTimeout(function(){location.reload()},3000) 
				}
				else 
				{ 	
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: obj.message
					});
					$("#delete_view_submit").attr('disabled',false); 
					$("#delete_view_submit").html('Delete')
				}
			},
			error: function() 
			{ 
				$.gritter.add({
					title: '<?php echo Error; ?>',
					text: 'Error while updating machine.'
				});
				location.reload();
			}
	   });
	}); 

	$("form[id^='edit_view_form']").submit(function(e) 
	{  

		var form = $(this);
		e.preventDefault();
		var formId = form.attr('id');
		var viewId = formId.replace('edit_view_form',''); 

		var startTimeHour =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');
		var stopTimeHour =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var stopTimeMinutes =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');

		if (Number(startTimeHour) >= Number(stopTimeHour)) 
		{
			alert("Please choose valid duration");
		}
		else
		{
			var viewId = $("#viewIdEdit-dynamic"+viewId).val(); 
			var viewName = $("#viewNameEdit-dynamic"+viewId).val(); 
			
			$("#edit_view_submit").attr('disabled',true); 
			$("#edit_view_submit").html('Saving...'); 
			
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/edit_view'); ?>", 
				data: {'viewId' : viewId,'viewName' : viewName,'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': stopTimeHour+':'+stopTimeMinutes}, 
				dataType: "html",
				success: function(data){
					
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$.gritter.add({
							title: '<?php echo Success; ?>',
							text: obj.message
						});
						window.setTimeout(function(){location.reload()},3000) 
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
						$("#edit_view_submit").attr('disabled',false); 
						$("#edit_view_submit").html('Save')
					}
				},
				error: function() 
				{ 
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: 'Error while updating machine.'
					});
					location.reload();
				}
		   });
		}
	}); 

	function addEditBreak(viewId)
    {
    	$(".submitEditButton-dynamic"+viewId).hide();
    	$(".addEditBreakButton-dynamic"+viewId).hide();
    	$(".removeEditBreak-dynamic"+viewId).fadeIn();
    }

    function removeEditBreak(viewId)
    {
    	$(".removeEditBreak-dynamic"+viewId).hide();
    	$(".submitEditButton-dynamic"+viewId).fadeIn();
    	$(".addEditBreakButton-dynamic"+viewId).fadeIn();
    }

	var f = 1;
    function saveEditBreaks(viewId)
    {
		var startTimeHourEdit =  $(".js-inline-picker-edit-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var stopTimeHourEdit =  $(".js-inline-picker-edit-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeHour =  $(".js-inline-picker-edit-break-start-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var startTimeMinutes =  $(".js-inline-picker-edit-break-start-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');
		var endTimeHour =  $(".js-inline-picker-edit-break-stop-dynamic"+viewId+" .picker-hours .picker-picked").attr('data-value');
		var endTimeMinutes =  $(".js-inline-picker-edit-break-stop-dynamic"+viewId+" .picker-minutes .picker-picked").attr('data-value');

		//alert(startTimeHourEdit)

		if (Number(startTimeHour) >= Number(endTimeHour)) 
		{
			alert("<?php echo Pleasechoosevalidduration; ?>");
		}
		else if(Number(startTimeHourEdit) <= Number(startTimeHour) && Number(stopTimeHourEdit) >= Number(endTimeHour)) 
		{
			$('.graph-loader').addClass("show").removeClass("hide");
	    	$.ajax({
				type: "POST",
				url: "<?php echo site_url('Analytics/addBreakTime'); ?>", 
				data: {'viewId' : viewId,'startTime': startTimeHour+':'+startTimeMinutes,'stopTime': endTimeHour+':'+endTimeMinutes}, 
				dataType: "html",
				success: function(data){
					var obj = $.parseJSON($.trim(data));
					if(obj.status == "1") 
					{	
						$('.graph-loader').addClass("hide").removeClass("show");
						var html = '<div class="row" id="breakRowEdit'+obj.breakViewId+'"><div class="col-md-10"><span style="font-size: 16px;"><?php echo Breakstartsfrom; ?> </span><span style="color: #FF8000;font-size: 16px;">'+startTimeHour+':'+startTimeMinutes+'</span><span style="font-size: 16px;">&nbsp;&nbsp;to&nbsp;&nbsp;</span><span style="color: #FF8000;font-size: 16px;">'+endTimeHour+':'+endTimeMinutes+'</span></div><div class="col-md-2"><img onclick="removeBreakData('+obj.breakViewId+')" style="width: 20px;" src="<?php echo base_url('assets/img/cross.svg'); ?>"></div></div>';

						$(".addEditBreakData-dynamic"+viewId).append(html);
						$(".removeEditBreak-dynamic"+viewId).hide();
				    	$(".addEditBreakData-dynamic"+viewId).fadeIn();
				    	$(".submitEditButton-dynamic"+viewId).fadeIn();
				    	$(".addEditBreakButton-dynamic"+viewId).fadeIn();
					}
					else 
					{ 	
						$.gritter.add({
							title: '<?php echo Error; ?>',
							text: obj.message
						});
					}
				},
				error: function() 
				{ 
					$.gritter.add({
						title: '<?php echo Error; ?>',
						text: 'Error while updating machine.'
					});
					location.reload();
				}
		   });
		}
		else
		{
			alert("Please choose valid duration");
	    }
		

    }

    $('.carousel').carousel({
	    interval: false
	}); 
   

</script>