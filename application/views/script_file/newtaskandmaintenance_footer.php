    <script>

    function completeUncompleteTask(taskId)
    {
        if($("#taskIdCheck"+taskId).prop('checked') == true)
        {
            var status = 'completed';
            $("#taskIdCheck"+taskId).attr("disabled",true);
        }

        $.ajax({
            url: "<?php echo base_url("Tasks/update_task_status") ?>",
            type: "post",
            data: {status:status,taskId:taskId} ,
            success: function (response) 
            {
                var obj = $.parseJSON(response);
                if(obj.status == "1") 
                {
                    var taskData = JSON.stringify(obj.taskData);
                    socket.emit('Change task status', taskData,(data) => { });
                }else
                {
                    $.gritter.add({
                        title: '<?php echo Success; ?>',
                        text: obj.message
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
               console.log(textStatus, errorThrown);
            }
        });
    }
    $('#carousel-example').on('slide.bs.carousel', function (e) 
    {
         var $e = $(e.relatedTarget);
            var idx = $e.index();
            var itemsPerSlide = 5;
            var totalItems = $('.carousel-item').length;
            if (idx >= totalItems-(itemsPerSlide-1)) {
                var it = itemsPerSlide - (totalItems - idx);
                for (var i=0; i<it; i++) {
                    // append slides to end
                    if (e.direction=="left") {
                        $('.carousel-item').eq(i).appendTo('.carousel-inner');
                    }
                    else {
                        $('.carousel-item').eq(0).appendTo('.carousel-inner');
                    }
                }
            }
    });
    
    $('#modal-add-task').on('hidden.bs.modal', function () 
    {
      $("#addTaskButton").show();
    })
    $('#modal-add-task').on('shown.bs.modal', function () 
    {
      $("#addTaskButton").hide();
    })
    $('#modal-delete-task').on('hidden.bs.modal', function () 
    {
      $("#isDelete").val("0");
    })
    
    function closeDetails()
    {
        taskId = $("#taskId").val();
        $("#"+taskId).css("background", "white");
        $("#"+taskId).css("border-left", "none");
        $(".table_data").removeClass("col-md-9");
        $(".table_data").addClass("col-md-12");
        $(".detail").hide();
    }

    function delete_task(taskId)
    {
        $("#isDelete").val("1");
        $("#deleteTaskId").val(taskId);
        $("#modal-delete-task").modal();
    } 

    function Edit_task(taskId)
    {
        $("#isDelete").val();
        $("#deleteTaskId").val(taskId);
        $("#modal-Edit-task").modal();
    }

    function selectEndTask(value)
    {
        $("#removeEndTaskText").show();
        $("#EndAfteroccurances").attr("disabled",true);
    }

    function removeEndTask()
    {
        $("#endTaskDate").val("");
        $("#removeEndTaskText").hide();
        $("#EndAfteroccurances").attr("disabled",false);
    }

    function selectEndAfteroccurances(value)
    {
        if (value != "") 
        {
            $("#endTaskDate").attr("disabled",true);
        }else
        {
            $("#endTaskDate").attr("disabled",false);
        }
    }

    function selectEndTaskEdit(value)
    {
        $("#removeEndTaskTextEdit").show();
        $("#EndAfteroccurancesEdit").attr("disabled",true);
    }

    function removeEndTaskEdit()
    {
        $("#endTaskDateEdit").val("");
        $("#removeEndTaskTextEdit").hide();
        $("#EndAfteroccurancesEdit").attr("disabled",false);
    }

    function selectEndAfteroccurancesEdit(value)
    {
        if (value != "") 
        {
            $("#endTaskDateEdit").attr("disabled",true);
        }else
        {
            $("#endTaskDateEdit").attr("disabled",false);
        }
    }

    function selectRepeat(value)
    {
        if (value == "never") 
        {
            $(".yearSelect").hide();
            $(".monthSelect").hide();
            $(".weekSelect").hide();
            $("#selectedRepeat").html("<?php echo Never; ?>");
            $("#endTaskDate").attr("disabled",true);
            $("#dueDate").attr("disabled",false);
            $("#EndAfteroccurances").attr("disabled",true);
            $(".dueSelect").show();
            $("#repeat").val(value);
        }
        else if (value == "everyDay") 
        {
            $(".yearSelect").hide();
            $(".weekSelect").hide();
            $(".monthSelect").hide();
            $(".dueSelect").show();
            $("#dueDate").attr("disabled",true);
            $("#endTaskDate").attr("disabled",false);
            $("#EndAfteroccurances").attr("disabled",false);
            $("#selectedRepeat").html("<?php echo Everyday; ?>");
            $("#repeat").val(value);
            
        }
        else if (value == "everyWeek") 
        {
            $(".yearSelect").hide();
            $("#endTaskDate").attr("disabled",false);
            $("#EndAfteroccurances").attr("disabled",false);
            $(".dueSelect").hide();
            $(".monthSelect").hide();
            $(".weekSelect").show();
            $("#selectedRepeat").html("<?php echo Everyweek; ?>");
            $("#repeat").val(value);
        }
        else if (value == "everyMonth") 
        {
            $(".yearSelect").hide();
            $(".weekSelect").hide();
            $("#endTaskDate").attr("disabled",false);
            $("#EndAfteroccurances").attr("disabled",false);
            $(".dueSelect").hide();
            $(".monthSelect").show();
            $("#selectedRepeat").html("<?php echo Everymonth; ?>");
            $("#repeat").val(value);
        }
        else if (value == "everyYear") 
        {
            $(".yearSelect").show();
            $(".weekSelect").hide();
            $("#endTaskDate").attr("disabled",false);
            $("#EndAfteroccurances").attr("disabled",false);
            $(".dueSelect").hide();
            $(".monthSelect").hide();
            $("#selectedRepeat").html("<?php echo Everyyear; ?>");
            $("#repeat").val(value);
        }
    }


    function selectRepeatEdit(value)
    {
        if (value == "never") 
        {
            $(".yearSelectEdit").hide();
            $(".monthSelectEdit").hide();
            $(".weekSelectEdit").hide();
            $("#selectedRepeatEdit").html("<?php echo Never; ?>");
            $("#endTaskDateEdit").attr("disabled",true);
            $("#dueDateEdit").attr("disabled",false);
            $("#EndAfteroccurancesEdit").attr("disabled",true);
            $(".dueSelectEdit").show();
            $("#repeatEdit").val(value);
        }
        else if (value == "everyDay") 
        {
            $(".yearSelectEdit").hide();
            $(".weekSelectEdit").hide();
            $(".monthSelectEdit").hide();
            $(".dueSelectEdit").show();
            $("#dueDateEdit").attr("disabled",true);
            $("#endTaskDateEdit").attr("disabled",false);
            $("#EndAfteroccurancesEdit").attr("disabled",false);
            $("#selectedRepeatEdit").html("<?php echo Everyday; ?>");
            $("#repeatEdit").val(value);
        }
        else if (value == "everyWeek") 
        {
            $(".yearSelectEdit").hide();
            $("#endTaskDateEdit").attr("disabled",false);
            $("#EndAfteroccurancesEdit").attr("disabled",false);
            $(".dueSelectEdit").hide();
            $(".monthSelectEdit").hide();
            $(".weekSelectEdit").show();
            $("#selectedRepeatEdit").html("<?php echo Everyweek; ?>");
            $("#repeatEdit").val(value);
        }
        else if (value == "everyMonth") 
        {
            $(".yearSelectEdit").hide();
            $(".weekSelectEdit").hide();
            $("#endTaskDateEdit").attr("disabled",false);
            $("#EndAfteroccurancesEdit").attr("disabled",false);
            $(".dueSelectEdit").hide();
            $(".monthSelectEdit").show();
            $("#selectedRepeatEdit").html("<?php echo Everymonth; ?>");
            $("#repeatEdit").val(value);
        }
        else if (value == "everyYear") 
        {
            $(".yearSelectEdit").show();
            $(".weekSelectEdit").hide();
            $("#endTaskDateEdit").attr("disabled",false);
            $("#EndAfteroccurancesEdit").attr("disabled",false);
            $(".dueSelectEdit").hide();
            $(".monthSelectEdit").hide();
            $("#selectedRepeatEdit").html("<?php echo Everyyear; ?>");
            $("#repeatEdit").val(value);
        }
    }

    function checkmonthDueOn(value)
    {
        if (value == "1") 
        {
            $("#dueMonthMonth").attr("disabled",false);
            $("#dueMonthWeek").attr("disabled",true);
            $("#dueMonthDay").attr("disabled",true);
        }else
        {
            $("#dueMonthWeek").attr("disabled",false);
            $("#dueMonthDay").attr("disabled",false);
            $("#dueMonthMonth").attr("disabled",true);
        }
    }

    function checkmonthDueOnEdit(value)
    {
        if (value == "1") 
        {
            $("#dueMonthMonthEdit").attr("disabled",false);
            $("#dueMonthWeekEdit").attr("disabled",true);
            $("#dueMonthDayEdit").attr("disabled",true);
        }else
        {
            $("#dueMonthWeekEdit").attr("disabled",false);
            $("#dueMonthDayEdit").attr("disabled",false);
            $("#dueMonthMonthEdit").attr("disabled",true);
        }
    }

    function checkyearDueOn(value)
    {
        if (value == "1") 
        {
            $("#dueYearMonth").attr("disabled",false);
            $("#dueYearMonthDay").attr("disabled",false);
            $("#dueYearWeek").attr("disabled",true);
            $("#dueYearDay").attr("disabled",true);
            $("#dueYearMonthOnThe").attr("disabled",true);
        }else
        {
            $("#dueYearWeek").attr("disabled",false);
            $("#dueYearDay").attr("disabled",false);
            $("#dueYearMonthOnThe").attr("disabled",false);
            $("#dueYearMonth").attr("disabled",true);
            $("#dueYearMonthDay").attr("disabled",true);
        }
    }

    function checkyearDueOnEdit(value)
    {
        if (value == "1") 
        {
            $("#dueYearMonthEdit").attr("disabled",false);
            $("#dueYearMonthDayEdit").attr("disabled",false);
            $("#dueYearWeekEdit").attr("disabled",true);
            $("#dueYearDayEdit").attr("disabled",true);
            $("#dueYearMonthOnTheEdit").attr("disabled",true);
        }else
        {
            $("#dueYearWeekEdit").attr("disabled",false);
            $("#dueYearDayEdit").attr("disabled",false);
            $("#dueYearMonthOnTheEdit").attr("disabled",false);
            $("#dueYearMonthEdit").attr("disabled",true);
            $("#dueYearMonthDayEdit").attr("disabled",true);
        }
    }

    function plusMinusDate(type)
    {
        dueDate = $("#dueDate").val();
        $.ajax({
            url: "<?php echo base_url("Tasks/get_due_date") ?>",
            type: "post",
            data: {type:type,dueDate:dueDate} ,
            success: function (response) 
            {
                $("#dueDate").val($.trim(response));    
            },
            error: function(jqXHR, textStatus, errorThrown) 
            {
               console.log(textStatus, errorThrown);
            }
        });
    }
    function allOperators()
    {
        if($("#usersAll").prop('checked') == true)
        {
            $(".operatorsCheckAll").prop('checked',true);
            $(".operatorsLabelAll").css('color',"#FF8000");
        }
        else
        {
            $(".operatorsLabelAll").css('color',"#1f2225");
            $(".operatorsCheckAll").prop('checked',false);
        }
    }

    function Operators(userId)
    {
        if($("#users"+userId).prop('checked') == true)
        {
            $("#users"+userId).prop('checked',true);
            $("#operatorsLabel"+userId).css('color',"#FF8000");
        }
        else
        {
            $("#users"+userId).prop('checked',false);
            $("#operatorsLabel"+userId).css('color',"#1f2225");
        }
    }

    function checkRepeat(value)
    {
        $(".repeatAll").css("color","#1f2225");
        if($("#"+value).prop('checked') == true)
        {
            $("#"+value).prop('checked',true);
            $("#"+value+"Label").css('color',"#FF8000");
        }
        else
        {
            $("#"+value).prop('checked',false);
            $("#"+value+"Label").css('color',"#1f2225");
        }
    }

    function editTask()
    {
        taskId = $("#taskId").val();


        $.ajax({
            url: "<?php echo base_url('Tasks/getEditTask') ?>",
            type: "post",
            data: {taskId:taskId} ,
            success: function (response) 
            {   
                var obj = $.parseJSON(response);
                $("#taskEdit").val(obj.task);
                $("#taskIdEdit").val(obj.taskId);
                

                if (obj.repeat == "never") 
                {
                    $(".yearSelectEdit").hide();
                    $(".monthSelectEdit").hide();
                    $(".weekSelectEdit").hide();
                    $("#selectedRepeatEdit").html("<?php echo Never; ?>");
                    $("#endTaskDateEdit").attr("disabled",true);
                    $("#dueDateEdit").attr("disabled",false);
                    $("#EndAfteroccurancesEdit").attr("disabled",true);
                    $(".dueSelectEdit").show();
                    $("#repeatEdit").val(obj.repeat);
                    $("#dueDateEdit").val(obj.dueDate);
                }
                else if (obj.repeat == "everyDay") 
                {
                    $(".yearSelectEdit").hide();
                    $(".weekSelectEdit").hide();
                    $(".monthSelectEdit").hide();
                    $(".dueSelectEdit").show();
                    $("#dueDateEdit").attr("disabled",true);
                    $("#endTaskDateEdit").attr("disabled",false);
                    $("#EndAfteroccurancesEdit").attr("disabled",false);
                    $("#selectedRepeatEdit").html("<?php echo Everyday; ?>");
                    $("#repeatEdit").val(obj.repeat);
                }
                else if (obj.repeat == "everyWeek") 
                {
                    $(".yearSelectEdit").hide();
                    $("#endTaskDateEdit").attr("disabled",false);
                    $("#EndAfteroccurancesEdit").attr("disabled",false);
                    $(".dueSelectEdit").hide();
                    $(".monthSelectEdit").hide();
                    $(".weekSelectEdit").show();
                    $("#selectedRepeatEdit").html("<?php echo Everyweek; ?>");
                    $("#repeatEdit").val(obj.repeat);
                    $('#dueWeekDayEdit [value='+obj.dueWeekDay+']').attr('selected', true);
                    if (obj.dueWeekDay == "Monday") 
                    {
                        var dueWeekDayText = "<?php echo Monday; ?>";
                    }else if (obj.dueWeekDay == "Tuesday") 
                    {
                        var dueWeekDayText = "<?php echo Tuesday; ?>";
                    }else if (obj.dueWeekDay == "Wednesday") 
                    {
                        var dueWeekDayText = "<?php echo Wednesday; ?>";
                    }else if (obj.dueWeekDay == "Thursday") 
                    {
                        var dueWeekDayText = "<?php echo Thursday; ?>";
                    }else if (obj.dueWeekDay == "Friday") 
                    {
                        var dueWeekDayText = "<?php echo Friday; ?>";
                    }else if (obj.dueWeekDay == "Saturday") 
                    {
                        var dueWeekDayText = "<?php echo Saturday; ?>";
                    }else if (obj.dueWeekDay == "Sunday") 
                    {
                        var dueWeekDayText = "<?php echo Sunday; ?>";
                    }
                    $('#select2-dueWeekDayEdit-container').text(dueWeekDayText);
                }
                else if (obj.repeat == "everyMonth") 
                {
                    $(".yearSelectEdit").hide();
                    $(".weekSelectEdit").hide();
                    $("#endTaskDateEdit").attr("disabled",false);
                    $("#EndAfteroccurancesEdit").attr("disabled",false);
                    $(".dueSelectEdit").hide();
                    $(".monthSelectEdit").show();
                    $("#selectedRepeatEdit").html("<?php echo Everymonth; ?>");
                    $("#repeatEdit").val(obj.repeat);
                    $("#monthDueOnEdit1").attr("checked",false);
                    $("#monthDueOnEdit2").attr("checked",false);
                    $("#monthDueOnEdit"+obj.monthDueOn).attr("checked",true);
                    if (obj.dueMonthWeek == "1") 
                    {
                        var dueMonthWeekText = "<?php echo First; ?>";
                    }else if (obj.dueMonthWeek == "2") 
                    {
                        var dueMonthWeekText = "<?php echo Second; ?>";    
                    }else if (obj.dueMonthWeek == "3") 
                    {
                        var dueMonthWeekText = "<?php echo Third; ?>"; 
                    }else if (obj.dueMonthWeek == "4") 
                    {
                        var dueMonthWeekText = "<?php echo Fourth; ?>";    
                    }else if (obj.dueMonthWeek == "5") 
                    {
                        var dueMonthWeekText = "<?php echo Last; ?>";  
                    }

                    if (obj.monthDueOn == "1") 
                    {

                        if (obj.dueMonthMonth == "third_last_day") 
                        {
                            var dueMonthMonth = "<?php echo Thirdlastday; ?>";
                        }else if (obj.dueMonthMonth == "second_last_day") 
                        {
                            var dueMonthMonth = "<?php echo Secondlastday; ?>";
                        }else if (obj.dueMonthMonth == "last_day") 
                        {
                            var dueMonthMonth = "<?php echo Lastday; ?>";
                        }else
                        {
                            var dueMonthMonth = obj.dueMonthMonth;
                        }

                        $('#dueMonthMonthEdit [value='+obj.dueMonthMonth+']').attr('selected', true);
                        $('#select2-dueMonthMonthEdit-container').text(dueMonthMonth);

                    }else
                    {
                        $('#dueMonthWeekEdit [value='+obj.dueMonthWeek+']').attr('selected', true);
                        $('#select2-dueMonthWeekEdit-container').text(dueMonthWeekText);


                        /*if (obj.dueMonthDay == "third_last_day") 
                        {
                            var dueMonthDay = "<?php echo Thirdlastday; ?>";
                        }else if (obj.dueMonthDay == "second_last_day") 
                        {
                            var dueMonthDay ="<?php echo Secondlastday; ?>";
                        }else if (obj.dueMonthDay == "last_day") 
                        {
                            var dueMonthDay = "<?php echo Lastday; ?>";
                        }else
                        {
                            var dueMonthDay = obj.dueMonthDay;
                        }*/


                        if (obj.dueMonthDay == "Monday") 
                        {
                            var dueMonthDayText = "<?php echo Monday; ?>";
                        }else if (obj.dueMonthDay == "Tuesday") 
                        {
                            var dueMonthDayText = "<?php echo Tuesday; ?>";
                        }else if (obj.dueMonthDay == "Wednesday") 
                        {
                            var dueMonthDayText = "<?php echo Wednesday; ?>";
                        }else if (obj.dueMonthDay == "Thursday") 
                        {
                            var dueMonthDayText = "<?php echo Thursday; ?>";
                        }else if (obj.dueMonthDay == "Friday") 
                        {
                            var dueMonthDayText = "<?php echo Friday; ?>";
                        }else if (obj.dueMonthDay == "Saturday") 
                        {
                            var dueMonthDayText = "<?php echo Saturday; ?>";
                        }else if (obj.dueMonthDay == "Sunday") 
                        {
                            var dueMonthDayText = "<?php echo Sunday; ?>";
                        }

                        $('#dueMonthDayEdit [value='+obj.dueMonthDay+']').attr('selected', true);
                        $('#select2-dueMonthDayEdit-container').text(dueMonthDayText);
                    }

                    checkmonthDueOnEdit(obj.monthDueOn);
                }
                else if (obj.repeat == "everyYear") 
                {
                    $(".yearSelectEdit").show();
                    $(".weekSelectEdit").hide();
                    $("#endTaskDateEdit").attr("disabled",false);
                    $("#EndAfteroccurancesEdit").attr("disabled",false);
                    $(".dueSelectEdit").hide();
                    $(".monthSelectEdit").hide();
                    $("#yearDueOnEdit1").attr("checked",false);
                    $("#yearDueOnEdit2").attr("checked",false);
                    $("#yearDueOnEdit"+obj.yearDueOn).attr("checked",true);
                    $("#selectedRepeatEdit").html("<?php echo Everyyear; ?>");
                    $("#repeatEdit").val(obj.repeat);

                    if (obj.yearDueOn == "1") 
                    {

                        if (obj.dueYearMonth == "January") 
                        {
                            var dueYearMonthText = "<?php echo January; ?>";
                        }else if (obj.dueYearMonth == "February") 
                        {
                            var dueYearMonthText = "<?php echo February; ?>";
                        }else if (obj.dueYearMonth == "March") 
                        {
                            var dueYearMonthText = "<?php echo March; ?>";
                        }else if (obj.dueYearMonth == "April") 
                        {
                            var dueYearMonthText = "<?php echo April; ?>";
                        }else if (obj.dueYearMonth == "May") 
                        {
                            var dueYearMonthText = "<?php echo May; ?>";
                        }else if (obj.dueYearMonth == "June") 
                        {
                            var dueYearMonthText = "<?php echo June; ?>";
                        }else if (obj.dueYearMonth == "July") 
                        {
                            var dueYearMonthText = "<?php echo July; ?>";
                        }else if (obj.dueYearMonth == "August") 
                        {
                            var dueYearMonthText = "<?php echo August; ?>";
                        }else if (obj.dueYearMonth == "September") 
                        {
                            var dueYearMonthText = "<?php echo September; ?>";
                        }else if (obj.dueYearMonth == "October") 
                        {
                            var dueYearMonthText = "<?php echo October; ?>";
                        }else if (obj.dueYearMonth == "November") 
                        {
                            var dueYearMonthText = "<?php echo November; ?>";
                        }else if (obj.dueYearMonth == "December") 
                        {
                            var dueYearMonthText = "<?php echo December; ?>";
                        }

                        $('#dueYearMonthEdit [value='+obj.dueYearMonth+']').attr('selected', true);
                        $('#select2-dueYearMonthEdit-container').text(dueYearMonthText);

                        if (obj.dueYearMonthDay == "third_last_day") 
                        {
                            var dueYearMonthDay = "<?php echo Thirdlastday; ?>";
                        }else if (obj.dueYearMonthDay == "second_last_day") 
                        {
                            var dueYearMonthDay = "<?php echo Secondlastday; ?>";
                        }else if (obj.dueYearMonthDay == "last_day") 
                        {
                            var dueYearMonthDay = "<?php echo Lastday; ?>";
                        }else
                        {
                            var dueYearMonthDay = obj.dueYearMonthDay;
                        }


                        $('#dueYearMonthDayEdit [value='+obj.dueYearMonthDay+']').attr('selected', true);
                        $('#select2-dueYearMonthDayEdit-container').text(dueYearMonthDay);

                    }else
                    {
                        if (obj.dueYearWeek == "1") 
                        {
                            var dueYearWeekEditText = "<?php echo First; ?>";
                        }else if (obj.dueYearWeek == "2") 
                        {
                            var dueYearWeekEditText = "<?php echo Second; ?>"; 
                        }else if (obj.dueYearWeek == "3") 
                        {
                            var dueYearWeekEditText = "<?php echo Third; ?>";  
                        }else if (obj.dueYearWeek == "4") 
                        {
                            var dueYearWeekEditText = "<?php echo Fourth; ?>"; 
                        }else if (obj.dueYearWeek == "5") 
                        {
                            var dueYearWeekEditText = "<?php echo Last; ?>";   
                        }

                       /* if (obj.dueYearDay == "third_last_day") 
                        {
                            var dueYearDay = "<?php echo Thirdlastday; ?>";
                        }else if (obj.dueYearDay == "second_last_day") 
                        {
                            var dueYearDay = "<?php echo Secondlastday; ?>";
                        }else if (obj.dueYearDay == "last_day") 
                        {
                            var dueYearDay = "<?php echo Lastday; ?>";
                        }else
                        {
                            var dueYearDay = obj.dueYearDay;
                        }
*/

                        if (obj.dueYearDay == "Monday") 
                        {
                            var dueYearDayText = "<?php echo Monday; ?>";
                        }else if (obj.dueYearDay == "Tuesday") 
                        {
                            var dueYearDayText = "<?php echo Tuesday; ?>";
                        }else if (obj.dueYearDay == "Wednesday") 
                        {
                            var dueYearDayText = "<?php echo Wednesday; ?>";
                        }else if (obj.dueYearDay == "Thursday") 
                        {
                            var dueYearDayText = "<?php echo Thursday; ?>";
                        }else if (obj.dueYearDay == "Friday") 
                        {
                            var dueYearDayText = "<?php echo Friday; ?>";
                        }else if (obj.dueYearDay == "Saturday") 
                        {
                            var dueYearDayText = "<?php echo Saturday; ?>";
                        }else if (obj.dueYearDay == "Sunday") 
                        {
                            var dueYearDayText = "<?php echo Sunday; ?>";
                        }

                        $('#dueYearWeekEdit [value='+obj.dueYearWeek+']').attr('selected', true);
                        $('#select2-dueYearWeekEdit-container').text(dueYearWeekEditText);

                        $('#dueYearDayEdit [value='+obj.dueYearDay+']').attr('selected', true);
                        $('#select2-dueYearDayEdit-container').text(dueYearDayText);

                        if (obj.dueYearMonthOnThe == "January") 
                        {
                            var dueYearMonthOnTheText = "<?php echo January; ?>";
                        }else if (obj.dueYearMonthOnThe == "February") 
                        {
                            var dueYearMonthOnTheText = "<?php echo February; ?>";
                        }else if (obj.dueYearMonthOnThe == "March") 
                        {
                            var dueYearMonthOnTheText = "<?php echo March; ?>";
                        }else if (obj.dueYearMonthOnThe == "April") 
                        {
                            var dueYearMonthOnTheText = "<?php echo April; ?>";
                        }else if (obj.dueYearMonthOnThe == "May") 
                        {
                            var dueYearMonthOnTheText = "<?php echo May; ?>";
                        }else if (obj.dueYearMonthOnThe == "June") 
                        {
                            var dueYearMonthOnTheText = "<?php echo June; ?>";
                        }else if (obj.dueYearMonthOnThe == "July") 
                        {
                            var dueYearMonthOnTheText = "<?php echo July; ?>";
                        }else if (obj.dueYearMonthOnThe == "August") 
                        {
                            var dueYearMonthOnTheText = "<?php echo August; ?>";
                        }else if (obj.dueYearMonthOnThe == "September") 
                        {
                            var dueYearMonthOnTheText = "<?php echo September; ?>";
                        }else if (obj.dueYearMonthOnThe == "October") 
                        {
                            var dueYearMonthOnTheText = "<?php echo October; ?>";
                        }else if (obj.dueYearMonthOnThe == "November") 
                        {
                            var dueYearMonthOnTheText = "<?php echo November; ?>";
                        }else if (obj.dueYearMonthOnThe == "December") 
                        {
                            var dueYearMonthOnTheText = "<?php echo December; ?>";
                        }

                        $('#dueYearMonthOnTheEdit [value='+obj.dueYearMonthOnThe+']').attr('selected', true);
                        $('#select2-dueYearMonthOnTheEdit-container').text(dueYearMonthOnTheText);
                    }


                    checkyearDueOnEdit(obj.yearDueOn);
                }
                $("#userIdsEdit").val("");
                $(".colmdCarouselmarginstyle").css("opacity","0.5");

                $("#oldUserIdsEdit").val(obj.userIds);
                var userIds = obj.userIds.split(",");
                for (i = 0; i < userIds.length; i++) 
                {
                    selectUsersEdit(userIds[i]);
                }



                $('#machineIdEdit [value='+obj.machineId+']').attr('locked', 'locked');
                $('#machineIdEdit').val(obj.machineId).trigger('change');

                $("#repeatOldEdit").val(obj.repeat);
               // $("#selectedRepeatEdit").val(obj.task);
                $("#endTaskDateEdit").val(obj.endTaskDate);
                $("#EndAfteroccurancesEdit").val(obj.EndAfteroccurances);
                $("#modal-edit-task").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
        
    }

    $(function() {
       $('#machineIdEdit').select2({
         tags: true,
         placeholder: 'Select an option',
         templateSelection : function (tag, container){
                // here we are finding option element of tag and
            // if it has property 'locked' we will add class 'locked-tag' 
            // to be able to style element in select
            var $option = $('#machineIdEdit option[value="'+tag.id+'"]');
            if ($option.attr('locked')){
               $(container).addClass('locked-tag');
               tag.locked = true; 
               $(container).find( "span" ).text("");
            }

            return tag.text;
         },
       })
       .on('select2:unselecting', function(e){
            // before removing tag we check option element of tag and 
          // if it has property 'locked' we will create error to prevent all select2 functionality
           if ($(e.params.args.data.element).attr('locked')) {
               e.preventDefault();
              $(container).find( "span" ).text("");
            }
         });
    });

    $('form#update_task_form').submit(function(e) 
    { 
        var form = $(this);
        e.preventDefault();
        $("#update_task_submit").html('<?php echo Saving; ?>...')
        var formData = new FormData(this);
        $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Tasks/update_task'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var obj = $.parseJSON($.trim(data));
                    if(obj.status == "1") 
                    {
                        socket.emit('Update task data', obj.taskData,'update', (data) => {
                        });
                        $("#update_task_submit").html('<?php echo Saved; ?>...')
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: obj.message
                        });
                        window.setTimeout(function(){location.reload()},3000) 
                    }
                    else 
                    {   
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: obj.message
                        });
                        $("#update_task_submit").html('Save')
                    }
                },
                error: function() { 
                    alert("Error while update task.");
                }
           });
    });

    $('form#delete_task_form').submit(function(e) 
    { 
        var form = $(this);
        e.preventDefault();
        $("#delete_task_submit").html('<?php echo deleteing; ?>...')
        var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Tasks/delete_task'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var obj = $.parseJSON($.trim(data));
                    if(obj.status == "1") 
                    {
                    
                        socket.emit('Update task data', obj.taskData,'add', (data) => {
                        });
                    $("#delete_task_submit").html('<?php echo Deleted; ?>')
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: obj.message
                        });
                        window.setTimeout(function(){location.reload()},3000) 
                    }
                    else 
                    {   
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: obj.message
                        });
                        $("#delete_task_submit").html('<?php echo Delete; ?>')
                    }
                },
                error: function() { 
                    alert("Error while update task.");
                }
           });
    });
    
    $(document).ready(function() 
    {

       

        $(".regular").slick({
                dots: false,
                infinite: false,
                slidesToShow: 5,
                slidesToScroll: 5,
                  responsive: [
                    {
                      breakpoint: 1024,
                      settings: {
                        dots: false,
                        infinite: false,
                        slidesToShow: 3,
                        slidesToScroll: 3
                      }
                    },
                    {
                      breakpoint: 600,
                      settings: {
                        dots: false,
                        infinite: false,
                        slidesToShow: 2,
                        slidesToScroll: 2
                      }
                    },
                    {
                      breakpoint: 480,
                      settings: {
                        dots: false,
                        infinite: false,
                        slidesToShow: 1,
                        slidesToScroll: 1
                      }
                    }
                  ],

              });

        
        $('.datepicker58').datepicker({
            format: "mm/dd/yyyy",
            autoclose : true,
            language: "<?php echo datepickerLanguage; ?>"
        });




        $('.datepicker59').datepicker({
            format: "mm/dd/yyyy",
            autoclose : true,
            startDate: new Date(),
             language: "<?php echo datepickerLanguage; ?>"
        });

        <?php 
            $dateValE = date("F d, Y", strtotime('today'));
            $dateValS =  date("F d, Y", strtotime('today - 29 days'));
        ?>

        var DdateValE = "<?php echo $dateValE; ?>"
        var DdateValS = "<?php echo $dateValS; ?>"

        $('#advance-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'> <?php echo Datecreated ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
            $('#advance-daterange').css("color","#FF8000");
        });


        $('.datepicker58').datepicker({
            format: "mm/dd/yyyy",
            autoclose : true,
            language: "<?php echo datepickerLanguage; ?>"
        });

        <?php 
            $dueDateValS = date("F d, Y", strtotime('today'));
            $dueDateValE =  date("F d, Y", strtotime('today - 29 days'));
        ?>

        var DdateValE = "<?php echo $dueDateValS; ?>"
        var DdateValS = "<?php echo $dueDateValE; ?>"
        
        $('#due-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();
                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();
                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo Duedate ?> - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
            $('#due-daterange').css("color","#FF8000");
        });

        $('table').on( 'click', 'tr', function () 
        {
        	/*$.gritter.add({
                title: '<?php echo Success; ?>',
                text: "test gritter",
                close: "Test"
            });*/
            taskId = $(this).attr('id');
            if(taskId != "" && taskId != undefined)
            {
                $.ajax({
                    url: "<?php echo base_url('Tasks/getTaskById') ?>",
                    type: "post",
                    data: {taskId:taskId} ,
                    success: function (response) 
                    {
                        isDelete = $("#isDelete").val();
                        if (isDelete == "0") 
                        {
                            var obj = $.parseJSON(response);
                            taskId = obj.taskId;
                            userName = obj.userName;
                            machineName = obj.machineName;
                            createdDate = obj.createdDate;
                            task = obj.task;
                            status = obj.status;
                            repeat = obj.repeatData;
                            userIds = obj.userIds;
                            userImage = "<?php echo base_url('assets/img/user/') ?>"+obj.userImage;
                            $("#taskId").val(taskId);
                            $("#userName").text(userName);
                            $("#machineName").text(machineName);
                            $("#createdDate").text(createdDate);
                            $("#task").text(task);
                            $("#repeatName").text(obj.repeatName);
                            $("#statusHtml").html(status);
                            $("#repeatHtml").html(repeat);
                            $("#operatorsHtml").html(userIds);

                            $(".month").hide();
                            $(".year").hide();
                            $(".week").hide();
                            if (obj.repeat == "everyDay") 
                            {
                                $(".month").hide();
                                $(".year").hide();
                                $(".neverT").hide();
                                $(".week").hide();
                                $(".dayT").show();
                            } 
                            else if (obj.repeat == "never") 
                            {
                                $('#dueWeekDayText').text(obj.dueWeekDay);
                                $(".month").hide();
                                $(".year").hide();
                                $(".dayT").hide();
                                $(".week").hide();
                                $(".neverT").show();
                            }
                            else if (obj.repeat == "everyWeek") 
                            {
                                if (obj.newTask == "0") 
                                {
                                    $('#dueWeekDayText').text("<?php echo Friday; ?>");
                                }else
                                {

                                     if (obj.dueWeekDay == "Monday") 
                                    {
                                        var dueWeekDayText = "<?php echo Monday; ?>";
                                    }else if (obj.dueWeekDay == "Tuesday") 
                                    {
                                        var dueWeekDayText = "<?php echo Tuesday; ?>";
                                    }else if (obj.dueWeekDay == "Wednesday") 
                                    {
                                        var dueWeekDayText = "<?php echo Wednesday; ?>";
                                    }else if (obj.dueWeekDay == "Thursday") 
                                    {
                                        var dueWeekDayText = "<?php echo Thursday; ?>";
                                    }else if (obj.dueWeekDay == "Friday") 
                                    {
                                        var dueWeekDayText = "<?php echo Friday; ?>";
                                    }else if (obj.dueWeekDay == "Saturday") 
                                    {
                                        var dueWeekDayText = "<?php echo Saturday; ?>";
                                    }else if (obj.dueWeekDay == "Sunday") 
                                    {
                                        var dueWeekDayText = "<?php echo Sunday; ?>";
                                    }
                                     $('#dueWeekDayText').text(dueWeekDayText);
                                }
                                $(".month").hide();
                                $(".year").hide();
                                $(".dayT").hide();
                                $(".neverT").hide();
                                $(".week").show();
                            }
                            else if (obj.repeat == "everyMonth") 
                            {
                                
                                $(".week").hide();
                                $(".year").hide();
                                $(".dayT").hide();
                                $(".neverT").hide();
                                if (obj.dueMonthWeek == "1") 
                                {
                                    var dueMonthWeekText = "<?php echo First; ?>";
                                }else if (obj.dueMonthWeek == "2") 
                                {
                                    var dueMonthWeekText = "<?php echo Second; ?>";    
                                }else if (obj.dueMonthWeek == "3") 
                                {
                                    var dueMonthWeekText = "<?php echo Third; ?>"; 
                                }else if (obj.dueMonthWeek == "4") 
                                {
                                    var dueMonthWeekText = "<?php echo Fourth; ?>";    
                                }else if (obj.dueMonthWeek == "5") 
                                {
                                    var dueMonthWeekText = "<?php echo Last; ?>";  
                                }


                                if (obj.monthDueOn == "1") 
                                {

                                    if (obj.dueMonthMonth == "third_last_day") 
                                    {
                                        var dueMonthMonth = "<?php echo Thirdlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueMonthMonth == "second_last_day") 
                                    {
                                        var dueMonthMonth = "<?php echo Secondlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueMonthMonth == "last_day") 
                                    {
                                        var dueMonthMonth = "<?php echo Lastday; ?> <?php echo ' '.of; ?>";
                                    }else
                                    {
                                        var dueMonthMonth = obj.dueMonthMonth;
                                    }

                                    if (obj.newTask == "0") 
                                    {
                                        $('#dueMonthMonthText').text("Every month");
                                        $(".month3").show();
                                    }else
                                    {
                                        $('#dueMonthMonthText').text(dueMonthMonth);
                                        $(".month1").show();
                                    }
                                    

                                }else
                                {

                                   /* if (obj.dueMonthDay == "third_last_day") 
                                    {
                                        var dueMonthDay = "<?php echo Thirdlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueMonthDay == "second_last_day") 
                                    {
                                        var dueMonthDay = "<?php echo Secondlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueMonthDay == "last_day") 
                                    {
                                        var dueMonthDay = "<?php echo Lastday; ?> <?php echo ' '.of; ?>";
                                    }else
                                    {
                                        var dueMonthDay = obj.dueMonthDay;
                                    }*/

                                    if (obj.newTask == "0") 
                                    {
                                        $(".month3").show();
                                    }else
                                    {
                                        $('#dueMonthWeekText').text(dueMonthWeekText);
                                        if (obj.dueMonthDay == "Monday") 
                                        {
                                            var dueMonthDayText = "<?php echo Monday; ?>";
                                        }else if (obj.dueMonthDay == "Tuesday") 
                                        {
                                            var dueMonthDayText = "<?php echo Tuesday; ?>";
                                        }else if (obj.dueMonthDay == "Wednesday") 
                                        {
                                            var dueMonthDayText = "<?php echo Wednesday; ?>";
                                        }else if (obj.dueMonthDay == "Thursday") 
                                        {
                                            var dueMonthDayText = "<?php echo Thursday; ?>";
                                        }else if (obj.dueMonthDay == "Friday") 
                                        {
                                            var dueMonthDayText = "<?php echo Friday; ?>";
                                        }else if (obj.dueMonthDay == "Saturday") 
                                        {
                                            var dueMonthDayText = "<?php echo Saturday; ?>";
                                        }else if (obj.dueMonthDay == "Sunday") 
                                        {
                                            var dueMonthDayText = "<?php echo Sunday; ?>";
                                        }
                                        $('#dueMonthDayText').text(dueMonthDayText);
                                        $(".month2").show();
                                    }
                                    
                                }


                            }
                            else if (obj.repeat == "everyYear") 
                            {
                                $(".week").hide();
                                $(".month").hide();
                                $(".dayT").hide();
                                $(".neverT").hide();
                                if (obj.yearDueOn == "1") 
                                {

                                    if (obj.dueYearMonthDay == "third_last_day") 
                                    {
                                        var dueYearMonthDay = "<?php echo Thirdlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueYearMonthDay == "second_last_day") 
                                    {
                                        var dueYearMonthDay = "<?php echo Secondlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueYearMonthDay == "last_day") 
                                    {
                                        var dueYearMonthDay = "<?php echo Lastday; ?> <?php echo ' '.of; ?>";
                                    }else
                                    {
                                        var dueYearMonthDay = obj.dueYearMonthDay;
                                    }

                                    

                                    if (obj.newTask == "0") 
                                    {
                                        $(".year3").show();
                                    }else
                                    {

                                        if (obj.dueYearMonth == "January") 
                                        {
                                            var dueYearMonthText = "<?php echo January; ?>";
                                        }else if (obj.dueYearMonth == "February") 
                                        {
                                            var dueYearMonthText = "<?php echo February; ?>";
                                        }else if (obj.dueYearMonth == "March") 
                                        {
                                            var dueYearMonthText = "<?php echo March; ?>";
                                        }else if (obj.dueYearMonth == "April") 
                                        {
                                            var dueYearMonthText = "<?php echo April; ?>";
                                        }else if (obj.dueYearMonth == "May") 
                                        {
                                            var dueYearMonthText = "<?php echo May; ?>";
                                        }else if (obj.dueYearMonth == "June") 
                                        {
                                            var dueYearMonthText = "<?php echo June; ?>";
                                        }else if (obj.dueYearMonth == "July") 
                                        {
                                            var dueYearMonthText = "<?php echo July; ?>";
                                        }else if (obj.dueYearMonth == "August") 
                                        {
                                            var dueYearMonthText = "<?php echo August; ?>";
                                        }else if (obj.dueYearMonth == "September") 
                                        {
                                            var dueYearMonthText = "<?php echo September; ?>";
                                        }else if (obj.dueYearMonth == "October") 
                                        {
                                            var dueYearMonthText = "<?php echo October; ?>";
                                        }else if (obj.dueYearMonth == "November") 
                                        {
                                            var dueYearMonthText = "<?php echo November; ?>";
                                        }else if (obj.dueYearMonth == "December") 
                                        {
                                            var dueYearMonthText = "<?php echo December; ?>";
                                        }
                                        $('#dueYearMonthText').text(dueYearMonthText);
                                        $('#dueYearMonthDayText').text(dueYearMonthDay);
                                        $(".year1").show();
                                    }

                                }else
                                {
                                    if (obj.dueYearWeek == "1") 
                                    {
                                        var dueYearWeekEditText = "<?php echo First; ?>";
                                    }else if (obj.dueYearWeek == "2") 
                                    {
                                        var dueYearWeekEditText = "<?php echo Second; ?>"; 
                                    }else if (obj.dueYearWeek == "3") 
                                    {
                                        var dueYearWeekEditText = "<?php echo Third; ?>";  
                                    }else if (obj.dueYearWeek == "4") 
                                    {
                                        var dueYearWeekEditText = "<?php echo Fourth; ?>"; 
                                    }else if (obj.dueYearWeek == "5") 
                                    {
                                        var dueYearWeekEditText = "<?php echo Last; ?>";   
                                    }

                                    if (obj.dueYearDay == "third_last_day") 
                                    {
                                        var dueYearDay = "<?php echo Thirdlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueYearDay == "second_last_day") 
                                    {
                                        var dueYearDay = "<?php echo Secondlastday; ?> <?php echo ' '.of; ?>";
                                    }else if (obj.dueYearDay == "last_day") 
                                    {
                                        var dueYearDay = "<?php echo Lastday; ?> <?php echo ' '.of; ?>";
                                    }else
                                    {
                                        var dueYearDay = obj.dueYearDay;
                                    }

                                    if (obj.newTask == "0") 
                                    {
                                        $(".year3").show();
                                    }else
                                    {
                                        $('#dueYearWeekText').text(dueYearWeekEditText);

                                        if (obj.dueYearDay == "Monday") 
                                        {
                                            var dueYearDayText = "<?php echo Monday; ?>";
                                        }else if (obj.dueYearDay == "Tuesday") 
                                        {
                                            var dueYearDayText = "<?php echo Tuesday; ?>";
                                        }else if (obj.dueYearDay == "Wednesday") 
                                        {
                                            var dueYearDayText = "<?php echo Wednesday; ?>";
                                        }else if (obj.dueYearDay == "Thursday") 
                                        {
                                            var dueYearDayText = "<?php echo Thursday; ?>";
                                        }else if (obj.dueYearDay == "Friday") 
                                        {
                                            var dueYearDayText = "<?php echo Friday; ?>";
                                        }else if (obj.dueYearDay == "Saturday") 
                                        {
                                            var dueYearDayText = "<?php echo Saturday; ?>";
                                        }else if (obj.dueYearDay == "Sunday") 
                                        {
                                            var dueYearDayText = "<?php echo Sunday; ?>";
                                        }
                                        $('#dueYearDayText').text(dueYearDayText);

                                         if (obj.dueYearMonthOnThe == "January") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo January; ?>";
                                        }else if (obj.dueYearMonthOnThe == "February") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo February; ?>";
                                        }else if (obj.dueYearMonthOnThe == "March") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo March; ?>";
                                        }else if (obj.dueYearMonthOnThe == "April") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo April; ?>";
                                        }else if (obj.dueYearMonthOnThe == "May") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo May; ?>";
                                        }else if (obj.dueYearMonthOnThe == "June") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo June; ?>";
                                        }else if (obj.dueYearMonthOnThe == "July") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo July; ?>";
                                        }else if (obj.dueYearMonthOnThe == "August") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo August; ?>";
                                        }else if (obj.dueYearMonthOnThe == "September") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo September; ?>";
                                        }else if (obj.dueYearMonthOnThe == "October") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo October; ?>";
                                        }else if (obj.dueYearMonthOnThe == "November") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo November; ?>";
                                        }else if (obj.dueYearMonthOnThe == "December") 
                                        {
                                            var dueYearMonthOnTheText = "<?php echo December; ?>";
                                        }

                                        $('#dueYearMonthOnTheText').text(dueYearMonthOnTheText);
                                        $(".year2").show();
                                    }

                                    
                                }
                            }

                            $(".dueTaskDate").text(obj.dueDate);

                            if (obj.endTaskDate == null) 
                            {
                                $("#endTaskDate").text("N/A");
                            }else
                            {
                                $("#endTaskDate").text(obj.endTaskDate);
                            }

                            if (obj.EndAfteroccurances == null) 
                            {
                                $("#EndAfteroccurancesText").text("N/A");
                            }else
                            {
                                $("#EndAfteroccurancesText").text(obj.EndAfteroccurances);
                            }
                            

                            $("#userImage").attr("src",userImage);
                            $('tr').css("border-left", "none");
                            $('tr').css("background", "#FFFFFF");
                            $("#"+taskId).css("background", "#F2F2F2");
                            $("#"+taskId).css("border-left", "7px solid #FF8000");
                            $("#empTable").css("width", "100%");
                            $(".table_data").removeClass("col-md-12");
                            $(".table_data").addClass("col-md-9");
                            $(".detail").show();
                        }
                       
                    },
                    error: function(jqXHR, textStatus, errorThrown) {
                       console.log(textStatus, errorThrown);
                    }
                });
            }
        } )



        <?php 
        if ($accessPointEdit == true) 
        {
            $tableData = "[
             { data: 'status' },
             { data: 'createdDate' },
             { data: 'dueDate' },
             { data: 'machineName' },
             { data: 'repeat' },
             { data: 'operators' },
             { data: 'task' },
             { data: 'delete' },
          ]";

          $columnDefs = "[
                {
                  targets: 0,
                  orderable: false,
                },
                {
                  targets: 1,
                  orderable: false,
                },
                {
                  targets: 2,
                  orderable: false,
                },
                {
                  targets: 3,
                  orderable: false,
                },
                {
                  targets: 4,
                  orderable: false,
                },
                {
                  targets: 5,
                  orderable: false,
                },
                {
                  targets: 6,
                  orderable: false
                },
                {
                  targets: 7,
                  orderable: false
                }
            ]";
        }else
        {
            $tableData = "[
                 { data: 'status' },
                 { data: 'createdDate' },
                 { data: 'dueDate' },
                 { data: 'machineName' },
                 { data: 'repeat' },
                 { data: 'operators' },
                 { data: 'task' },
              ]";

              $columnDefs = "[
                {
                  targets: 0,
                  orderable: false,
                },
                {
                  targets: 1,
                  orderable: false,
                },
                {
                  targets: 2,
                  orderable: false,
                },
                {
                  targets: 3,
                  orderable: false,
                },
                {
                  targets: 4,
                  orderable: false,
                },
                {
                  targets: 5,
                  orderable: false,
                },
                {
                  targets: 6,
                  orderable: false
                }
            ]";
        }
         ?>
        $('#empTable').removeAttr('width').DataTable({
           "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
          'processing': true,
          "pagingType": "input", 
          'serverSide': true,
          'serverMethod': 'post',
          "pageLength" : 100,
          "info" : false,
          "searching" : false,
          "lengthChange": false,
          "stateSave": true,
          "rowId" : 'taskId',
          'ajax': {
              'url':'task_maintenace_pagination',
              "type": "POST",
                "data":function(data) {
                    data.userId = $('.filterUserName:checkbox:checked').map(function(){
                                      return $(this).val();
                                    }).get();
                    data.repeat = $('.filterRepeat:checkbox:checked').map(function(){
                                      return $(this).val();
                                    }).get();
                    data.dateValS = $('#dateValS').val();
                    data.dateValE = $('#dateValE').val();

                    data.dueDateValS = $('#dueDateValS').val();
                    data.dueDateValE = $('#dueDateValE').val();

                    data.status = $('.filterStatus:checkbox:checked').map(function(){
                                      return $(this).val();
                                    }).get();
                    data.machineIds = $('#machineIds').val();

                    data.machineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                    }).get();
                    data.dueDate = $('#filterDueDate').val();
                },  
          },
          'columns': <?php echo $tableData; ?>,
          "columnDefs": <?php echo $columnDefs; ?>, 
            fixedColumns: true, 
            drawCallback: function( settings ) { 
                $(".paginate_page").html('<?php echo Page; ?>');
                var paginate_of = $(".paginate_of").text();
                var paginate_of = paginate_of.split(" ");
                $(".paginate_of").text('<?php echo " ".of." " ?>'+paginate_of[2]);
                
            },
       });
        <?php if ($accessPointEdit == true) { ?>
            $(".col-sm-5").html('<a id="addTaskButton" style="border-radius: 50px;padding: 10px 19px 10px 18px;" href="#modal-add-task" data-toggle="modal" class="btn btn-primary"><i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;&nbsp;<?php echo Addtask; ?></a>');
        <?php } ?>

        $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#dateValS').val(dateValS);
            $('#dateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#dueDateValS').val(dateValS);
            $('#dueDateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });
    }); 

    function uncheckCreatedDate()
    {
        $("#showCreatedDate").html("");
        $('#advance-daterange').css("color","#b8b0b0");

        <?php 
            $dateValE = date("F d, Y", strtotime('today'));
            $dateValS =  date("F d, Y", strtotime('today - 3000 days'));
            $dateValESET = date("Y-m-d", strtotime('today'));
            $dateValSSET =  date("Y-m-d", strtotime('today - 3000 days'));
        ?>
        var DdateValE = "<?php echo $dateValE; ?>"
        var DdateValS = "<?php echo $dateValS; ?>"

        $('#dateValS').val("<?php echo $dateValSSET; ?>");
        $('#dateValE').val("<?php echo $dateValESET; ?>");
        $('#advance-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

                var label = label+"("+startDate+" - "+endDate+")";
            }
            $("#showCreatedDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Created Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
            $('#advance-daterange').css("color","#FF8000");
        });

        $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dateValE = picker.endDate.format('YYYY-MM-DD');  
            $('#dateValS').val(dateValS);
            $('#dateValE').val(dateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckDueDate()
    {
        $("#showDueDate").html("");
        $('#due-daterange').css("color","#b8b0b0");
        <?php 
            $dueDateValS = date("F d, Y", strtotime('today'));
            $dueDateValE = date("Y-m-d", strtotime("-29 day"));
            $dateValESET = date("Y-m-d", strtotime('today'));
            $dateValSSET =  date("Y-m-d", strtotime('today -29 days'));
        ?>

        var DdateValE = "<?php echo $dueDateValS; ?>"
        var DdateValS = "<?php echo $dueDateValE; ?>"

        $('#dueDateValS').val("");
        $('#dueDateValE').val("");

        $('#due-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE), 
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
             ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        }, 

        function(start, end, label) 
        {
            if (label == "<?php echo Custom; ?>") 
            {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth()+1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth()+1) + '-' + endDate.getDate();

                var label = label+"("+startDate+" - "+endDate+")";
            }

            $("#showDueDate").html("<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonResetDueDate' class='btn btn-sm btn-primary'>Due Date - "+label+" &nbsp;&nbsp;<i onclick='uncheckDueDate()' class='fa fa-times'></i></button>");
        
            $('#due-daterange').css("color","#FF8000");
        });

        $('#due-daterange').on('apply.daterangepicker', function(ev, picker) 
        { 
            var dueDateValS = picker.startDate.format('YYYY-MM-DD'); 
            var dueDateValE = picker.endDate.format('YYYY-MM-DD');     
            $('#dueDateValS').val(dueDateValS);
            $('#dueDateValE').val(dueDateValE);
            $("#empTable").DataTable().ajax.reload();
        });

        $("#empTable").DataTable().ajax.reload();
    }

    function filterStatus()
    {
        var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterStatus.length > 0) 
        {
            $("#filterStatusSelected").css('color','#FF8000');

            var html = "";
            if (in_array("'completed'",filterStatus) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Status - <?php echo Completed ?> &nbsp;&nbsp;<i onclick='uncheckStatus(0)' class='fa fa-times'></i></button>";
            }

            if (in_array("'uncompleted'",filterStatus) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusUncompleted' class='btn btn-sm btn-primary'>Status - <?php echo Uncompleted ?> &nbsp;&nbsp;<i onclick='uncheckStatus(1)' class='fa fa-times'></i></button>";
            }

            $("#showStatus").html(html);
        }
        else
        {
            $("#filterStatusSelected").css('color','#b8b0b0');
            $("#showStatus").html("");
        }

        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckStatus(id)
    {
        if (id == "0") 
        {
            $("#filterStatusCompleted").prop('checked',false);
            $("#buttonFilterstatusCompleted").remove();
        }
        else if (id == "1") 
        {
            $("#filterStatusUncompleted").prop('checked',false);
            $("#buttonFilterstatusUncompleted").remove();
        }

        var filterStatus = $('.filterStatus:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterStatus.length > 0) 
        {

        }
        else
        {
            $("#filterStatusSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }


    function filterUserName(userId,userName)
        {
        var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterUserName.length > 0) 
        {
            $("#filterUserNameSelected").css('color','#FF8000');
            if (in_array(userId,filterUserName) != -1) 
            {
                html = "<button id='removeUserButton"+userId+"' style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Operator; ?> - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
                $("#showUserFilter").append(html);
            }
            else
            {
                $("#removeUserButton"+userId).remove();
            }
        }
        else
        {
            $("#showUserFilter").html("");
            $("#filterUserNameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckUserName(userId)
    {
        $("#removeUserButton"+userId).remove();
        $("#filterUserName"+userId).prop('checked',false);
        var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterUserName.length > 0) 
        {

        }
        else
        {
            $("#filterUserNameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function filterRepeat()
    {
        var filterRepeat = $('.filterRepeat:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterRepeat.length > 0) 
        {
            $("#filterRepeatSelected").css('color','#FF8000');

            var html = "";
            if (in_array("'never'",filterRepeat) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRepeatNever' class='btn btn-sm btn-primary'><?php echo Repeat . " - ". Never; ?> &nbsp;&nbsp;<i onclick='uncheckRepeat(1)' class='fa fa-times'></i></button>";
            }
            
            if (in_array("'everyDay'",filterRepeat) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRepeatEveryday' class='btn btn-sm btn-primary'><?php echo Repeat . " - ". Everyday; ?>  &nbsp;&nbsp;<i onclick='uncheckRepeat(2)' class='fa fa-times'></i></button>";
            }

            if (in_array("'everyWeek'",filterRepeat) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRepeatEveryWeek' class='btn btn-sm btn-primary'><?php echo Repeat . " - ". Everyweek; ?>  &nbsp;&nbsp;<i onclick='uncheckRepeat(3)' class='fa fa-times'></i></button>";
            }

            if (in_array("'everyMonth'",filterRepeat) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRepeatEveryMonth' class='btn btn-sm btn-primary'><?php echo Repeat . " - ". Everymonth; ?>  &nbsp;&nbsp;<i onclick='uncheckRepeat(4)' class='fa fa-times'></i></button>";
            }

            if (in_array("'everyYear'",filterRepeat) != -1) 
            {
                html += "<button style='margin-left:10px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRepeatEveryYear' class='btn btn-sm btn-primary'><?php echo Repeat . " - ". Everyyear; ?>  &nbsp;&nbsp;<i onclick='uncheckRepeat(5)' class='fa fa-times'></i></button>";
            }

            $("#showRepeat").html(html);
        }
        else
        {
            $("#filterRepeatSelected").css('color','#b8b0b0');
            $("#showRepeat").html("");
        }

        $("#empTable").DataTable().ajax.reload();
    }



    function uncheckRepeat(id)
    {
        if (id == "1") 
        {
            $("#filterRepeatNever").prop('checked',false);
            $("#buttonFilterRepeatNever").remove();
        }
        else if (id == "2") 
        {
            $("#filterRepeatEveryDay").prop('checked',false);
            $("#buttonFilterRepeatEveryday").remove();
        }
        else if (id == "3") 
        {
            $("#filterRepeatEveryWeek").prop('checked',false);
            $("#buttonFilterRepeatEveryWeek").remove();
        }
        else if (id == "4") 
        {
            $("#filterRepeatEveryMonth").prop('checked',false);
            $("#buttonFilterRepeatEveryMonth").remove();
        }
        else if (id == "5") 
        {
            $("#filterRepeatEveryYear").prop('checked',false);
            $("#buttonFilterRepeatEveryYear").remove();
        }

        var filterRepeat = $('.filterRepeat:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterRepeat.length > 0) 
        {

        }
        else
        {
            $("#filterRepeatSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function filterMachineId(machineId, machineName)
    {
        var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();

        if (filterMachineId.length > 0) 
        {
            $("#filterMachineIdSelected").css('color','#FF8000');

            if (in_array(machineId,filterMachineId) != -1) 
            {
                html = "<button id='removeMachineButton"+machineId+"' style='margin-left:10px;font-weight: 400;font-size: 11px; margin-top:10px;' class='btn btn-sm btn-primary'><?php echo Machine; ?> - "+machineName+" &nbsp;&nbsp;<i onclick='uncheckMachineName("+machineId+")' class='fa fa-times'></i></button>";

                $("#showMachinefilter").append(html);
            }
            else
            {
                $("#removeMachineButton"+machineId).remove();
            }
        }
        else
        {
            $("#showMachinefilter").html("");
            $("#filterMachineIdSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckMachineName(machineId)
    {
        $("#removeMachineButton"+machineId).remove();
        $("#filterMachineId"+machineId).prop('checked',false);
        var filterMachineId = $('.filterMachineId:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterMachineId.length > 0) 
        {
        }
        else
        {
            $("#filterMachineIdSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function in_array(needle, haystack)
    {
        var found = 0;
        for (var i=0, len=haystack.length;i<len;i++) 
        {
            if (haystack[i] == needle) return i;
                found++;
        }
        return -1;
    }

    function selectMachine(machineId)
    {
        if (machineId == "all") 
        {
            $(".machineOpacity").css("opacity","0.2");
            $(".selectMachine").css("stroke","#002060");
            $("#machineOpacityall").css("opacity","1");
            $("#selectMachineall").css("stroke","#CCD2DF");
            $(".selectedMachineName").css("color","black"); 
            $("#selectedMachineNameall").css("color","#FF8000");
            $("#machineIds").val("");   
        }
        else
        {
            var machineIds = $("#machineIds").val().split(",");
            if (in_array(machineId,machineIds)!= -1) 
            {
                $("#selectedMachineNameall").css("color","black");  
                $("#machineOpacity"+machineId).css("opacity","0.2");
                $("#selectMachine"+machineId).css("stroke","#002060");  
                $("#selectedMachineName"+machineId).css("color","black");

                for(var i = machineIds.length - 1; i >= 0; i--) 
                {
                    if(machineIds[i] == machineId) 
                    {
                        machineIds.splice(i, 1);
                    }
                }
                
                var selectedMachineId = machineIds.join(",");
                $("#machineIds").val(selectedMachineId);
                if (selectedMachineId == "") 
                {
                    $(".machineOpacity").css("opacity","0.2");
                    $(".selectMachine").css("stroke","#002060");
                    $("#machineOpacityall").css("opacity","1");
                    $("#selectMachineall").css("stroke","#CCD2DF");
                    $(".selectedMachineName").css("color","black"); 
                    $("#selectedMachineNameall").css("color","#FF8000");
                }

            }
            else
            {   
                var checkMachineIds = $("#machineIds").val();
                if (checkMachineIds != "") 
                {
                    var selectedMachineId = $("#machineIds").val()+','+machineId;
                    $("#machineIds").val(selectedMachineId);
                }
                else
                {
                    $("#machineIds").val(machineId);
                }
                $("#selectedMachineNameall").css("color","black");  
                $("#machineOpacityall").css("opacity","0.2");
                $("#selectMachineall").css("stroke","#002060");
                $("#machineOpacity"+machineId).css("opacity","1");
                $("#selectMachine"+machineId).css("stroke","#CCD2DF");  
                $("#selectedMachineName"+machineId).css("color","#FF8000");
            }
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function selectUsers(userId)
    {
        var userIds = $("#userIds").val().split(",");
            if (in_array(userId,userIds)!= -1) 
            {
                $("#userIdBlock"+userId).css("opacity","0.5");
                for(var i = userIds.length - 1; i >= 0; i--) 
                {
                    if(userIds[i] == userId) 
                    {
                        userIds.splice(i, 1);
                    }
                }
                var selectedUserId = userIds.join(",");
                $("#userIds").val(selectedUserId);

            }
            else
            {   
                var checkUserIds = $("#userIds").val();
                if (checkUserIds != "") 
                {
                    var selectedUserId = $("#userIds").val()+','+userId;
                    $("#userIds").val(selectedUserId);
                }
                else
                {
                    $("#userIds").val(userId);
                }
                $("#userIdBlock"+userId).css("opacity","1");
            }
    }

    function selectUsersEdit(userId)
    {
        var userIds = $("#userIdsEdit").val().split(",");
            if (in_array(userId,userIds)!= -1) 
            {
                $("#userIdBlockEdit"+userId).css("opacity","0.5");
                for(var i = userIds.length - 1; i >= 0; i--) 
                {
                    if(userIds[i] == userId) 
                    {
                        userIds.splice(i, 1);
                    }
                }
                var selectedUserId = userIds.join(",");
                $("#userIdsEdit").val(selectedUserId);

            }
            else
            {   
                var checkUserIds = $("#userIdsEdit").val();
                if (checkUserIds != "") 
                {
                    var selectedUserId = $("#userIdsEdit").val()+','+userId;
                    $("#userIdsEdit").val(selectedUserId);
                }
                else
                {
                    $("#userIdsEdit").val(userId);
                }
                $("#userIdBlockEdit"+userId).css("opacity","1");
            }
    }

    $('form#add_task_form').submit(function(e) 
    { 
        var form = $(this);
        e.preventDefault();
            $("#add_task_submit").attr('disabled',true); 
            $("#add_task_submit").html('<?php echo Saving; ?>...'); 
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Tasks/add_task'); ?>",
                data: form.MytoJson(),  
                dataType: "html",
                success: function(data){
                    var obj = $.parseJSON($.trim(data));
                    if(obj.status == "1") 
                    {  
                        socket.emit('Update task data', obj.taskData,'add', (data) => {
                        });
                        $("#add_task_submit").html('<?php echo Saved; ?>...')
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: obj.message
                        });
                        window.setTimeout(function(){location.reload()},3000) 
                    }
                    else 
                    {   
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: obj.message
                        });
                        $("#add_task_submit").attr('disabled',false); 
                        $("#add_task_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addtask; ?>')
                    }
                },
                error: function() { 
                    $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: 'Error while adding task'
                        });
                    $("#add_task_submit").attr('disabled',false);
                    $("#add_task_submit").html('<i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo Addtask; ?>')
                }
           });
    }); 

    $('form#edit_task_form').submit(function(e) 
    { 
        var form = $(this);
        e.preventDefault();
            $("#edit_task_submit").attr('disabled',true); 
            $("#edit_task_submit").html('<?php echo Saving; ?>...'); 
            $.ajax({
                type: "POST",
                url: "<?php echo site_url('Tasks/edit_task'); ?>",
                data: form.MytoJson(),  
                dataType: "html",
                success: function(data){
                    var obj = $.parseJSON($.trim(data));
                    if(obj.status == "1") 
                    {  
                        socket.emit('Update task data', obj.taskData,'update', (data) => {
                        });
                        $("#edit_task_submit").attr('disabled',true); 
                        $("#edit_task_submit").html('<?php echo Saved; ?>')
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: obj.message
                        });
                        window.setTimeout(function(){location.reload()},3000) 
                    }
                    else 
                    {   
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: obj.message
                        });
                        $("#edit_task_submit").attr('disabled',false); 
                        $("#edit_task_submit").html('<?php echo Save; ?>')
                    }
                },
                error: function() { 
                    $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: 'Error while adding task'
                        });
                    $("#edit_task_submit").attr('disabled',false);
                    $("#edit_task_submit").html('<?php echo Save; ?>')
                }
           });
    }); 
</script>




