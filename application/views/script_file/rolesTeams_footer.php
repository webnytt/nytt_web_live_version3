<script >

    function showPassword(id,type)
    {
        if (type == "text") 
        {
            type1 = "'password'";
            class1 = "fa fa-eye"; 
        }
        else
        {
            type1 = "'text'";
            class1 = "fa fa-eye-slash"; 
        }
        $("#"+id).attr("type",type);
        id1 = "'"+id+"'";
        $("#icon"+id).html('<i onclick="showPassword('+id1+','+type1+');" style="margin-top: -5px;" class="'+class1+'"></i>');
    } 

    function checkIsView(subResponsibilitiesId,responsibilitiesId)
    {
        if($("#isEditId"+subResponsibilitiesId).prop('checked') == true)
        {
            $("#isViewId"+responsibilitiesId).prop("disabled",false);
            $("#isViewClass"+responsibilitiesId).prop("checked",true);
            $("#isViewId"+subResponsibilitiesId).prop("checked",true);
        }   
    }

    function checkIsViewEdit(subResponsibilitiesId,responsibilitiesId)
    {
        if($("#isEditIdEdit"+subResponsibilitiesId).prop('checked') == true)
        {
            $("#isViewIdEdit"+responsibilitiesId).prop("disabled",false);
            $("#isViewClassEdit"+responsibilitiesId).prop("checked",true);
            $("#isViewIdEdit"+subResponsibilitiesId).prop("checked",true);
        }   
    }

    function checkIsExecute(subResponsibilitiesId,responsibilitiesId)
    {
        if (subResponsibilitiesId == 23) 
        {
            if($("#isExecuteId"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteId24").prop("checked",true);
                $("#isExecuteId25").prop("checked",true);
            }else
            {
                $("#isExecuteId24").prop("checked",false);
                $("#isExecuteId25").prop("checked",false);
            }
        }

        if (subResponsibilitiesId == 24) 
        {
            if($("#isExecuteId"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteId23").prop("checked",true);
            }else
            {
                $("#isExecuteId23").prop("checked",false);
                $("#isExecuteId25").prop("checked",false);   
            }
        }

         if (subResponsibilitiesId == 25) 
        {
            if($("#isExecuteId"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteId23").prop("checked",true);
                $("#isExecuteId24").prop("checked",true);
            }   
        }
    }

    function checkIsExecuteEdit(subResponsibilitiesId,responsibilitiesId)
    {
        if (subResponsibilitiesId == 23) 
        {
            if($("#isExecuteIdEdit"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteIdEdit24").prop("checked",true);
                $("#isExecuteIdEdit25").prop("checked",true);
            }else
            {
                $("#isExecuteIdEdit24").prop("checked",false);
                $("#isExecuteIdEdit25").prop("checked",false);
            }
        }

        if (subResponsibilitiesId == 24) 
        {
            if($("#isExecuteIdEdit"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteIdEdit23").prop("checked",true);
            }else
            {
                $("#isExecuteIdEdit23").prop("checked",false);
                $("#isExecuteIdEdit25").prop("checked",false);   
            }
        }

         if (subResponsibilitiesId == 25) 
        {
            if($("#isExecuteIdEdit"+subResponsibilitiesId).prop('checked') == true)
            {
                $("#isExecuteIdEdit23").prop("checked",true);
                $("#isExecuteIdEdit24").prop("checked",true);
            }   
        }
    }

    function checkIsEdit(subResponsibilitiesId,responsibilitiesId)
    {
        if($("#isViewId"+subResponsibilitiesId).prop('checked') == false)
        {
            $("#isEditId"+responsibilitiesId).prop("disabled",true);
            $("#isEditId"+subResponsibilitiesId).prop("checked",false);
        }   
    }

    function checkIsEditEdit(subResponsibilitiesId,responsibilitiesId)
    {
        if($("#isViewIdEdit"+subResponsibilitiesId).prop('checked') == false)
        {
            $("#isEditIdEdit"+responsibilitiesId).prop("disabled",true);
            $("#isEditIdEdit"+subResponsibilitiesId).prop("checked",false);
        }   
    }

    function checkSubResponsibilities(responsibilitiesId,type)
    {
        if (type == "view") 
        {
            if($("#isViewClass"+responsibilitiesId).prop('checked') == true)
            {
                $(".isViewClass"+responsibilitiesId).prop("disabled",false);
                $(".isViewClass"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isViewClass"+responsibilitiesId).prop("disabled",true);
                $(".isViewClass"+responsibilitiesId).prop("checked",false);

                $(".isEditClass"+responsibilitiesId).prop("disabled",true);
                $("#isEditClass"+responsibilitiesId).prop("checked",false);
                $(".isEditClass"+responsibilitiesId).prop("checked",false);
            }
        }

        if (type == "edit") 
        {

            $("#isViewClass"+responsibilitiesId).prop("checked",true);

            $(".isViewClass"+responsibilitiesId).prop("disabled",false);
            $(".isViewClass"+responsibilitiesId).prop("checked",true);


            if($("#isEditClass"+responsibilitiesId).prop('checked') == true)
            {
                $(".isEditClass"+responsibilitiesId).prop("disabled",false);
                $(".isEditClass"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isEditClass"+responsibilitiesId).prop("disabled",true);
                $(".isEditClass"+responsibilitiesId).prop("checked",false);
            }
        }

        if (type == "execute") 
        {
            if($("#isExecuteClass"+responsibilitiesId).prop('checked') == true)
            {
                $(".isExecuteClass"+responsibilitiesId).prop("disabled",false);
                $(".isExecuteClass"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isExecuteClass"+responsibilitiesId).prop("disabled",true);
                $(".isExecuteClass"+responsibilitiesId).prop("checked",false);
            }
        }
    }

    function checkSubResponsibilitiesEdit(responsibilitiesId,type)
    {
        if (type == "view") 
        {
            if($("#isViewClassEdit"+responsibilitiesId).prop('checked') == true)
            {
                $(".isViewClassEdit"+responsibilitiesId).prop("disabled",false);
                $(".isViewClassEdit"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isViewClassEdit"+responsibilitiesId).prop("disabled",true);
                $(".isViewClassEdit"+responsibilitiesId).prop("checked",false);

                $(".isEditClassEdit"+responsibilitiesId).prop("disabled",true);
                $("#isEditClassEdit"+responsibilitiesId).prop("checked",false);
                $(".isEditClassEdit"+responsibilitiesId).prop("checked",false);
            }
        }

        if (type == "edit") 
        {

            $("#isViewClassEdit"+responsibilitiesId).prop("checked",true);

            $(".isViewClassEdit"+responsibilitiesId).prop("disabled",false);
            $(".isViewClassEdit"+responsibilitiesId).prop("checked",true);


            if($("#isEditClassEdit"+responsibilitiesId).prop('checked') == true)
            {
                $(".isEditClassEdit"+responsibilitiesId).prop("disabled",false);
                $(".isEditClassEdit"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isEditClassEdit"+responsibilitiesId).prop("disabled",true);
                $(".isEditClassEdit"+responsibilitiesId).prop("checked",false);
            }
        }

        if (type == "execute") 
        {
            if($("#isExecuteClassEdit"+responsibilitiesId).prop('checked') == true)
            {
                $(".isExecuteClassEdit"+responsibilitiesId).prop("disabled",false);
                $(".isExecuteClassEdit"+responsibilitiesId).prop("checked",true);
            }else
            {
                $(".isExecuteClassEdit"+responsibilitiesId).prop("disabled",true);
                $(".isExecuteClassEdit"+responsibilitiesId).prop("checked",false);
            }
        }
    }

    function addUser(userId)
    {
        var assignOperator = $("#assignOperator").val().split(",");
        assignOperator.push(userId);
        var assignOperatorVal = assignOperator.join();
        $("#assignOperator").val(assignOperatorVal);
        $("#addUser"+userId).fadeOut(400);
        $("#userRemove"+userId).fadeIn(400);
    } 

    function editRoles(rolesId)
    {
        $('.graph-loader').addClass("show").removeClass("hide");
        $.ajax({
            url: "<?php echo base_url('Roles/getRolesById') ?>",
            type: "post",
            data: {rolesId:rolesId},
            success: function (response) 
            {   
                var obj = JSON.parse(response);
                $("#editRolesId").val(obj.rolesId);
                $("#editRolesName").val(obj.rolesName);
                $("#rolesResponsibilitiesHtml").html(obj.rolesResponsibilities);
                $('.graph-loader').addClass("hide").removeClass("show");
                $("#modalEditNewRole").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
        
    }

    function removeUser(userId)
    {
        var assignOperator = $("#assignOperator").val().split(",");

        for (var i = assignOperator.length - 1; i >= 0; i--) {
         if ( assignOperator[i] == userId) {
          assignOperator.splice(i, 1);
         }
        }
        var assignOperatorVal = assignOperator.join();
        $("#assignOperator").val(assignOperatorVal);

        $("#userRemove"+userId).fadeOut(400);
        $("#addUser"+userId).fadeIn(400);
    }

    function deleteRoles(rolesId,rolesName)
    {
        $("#role_title").text('<?php echo Delete; ?>:-'+rolesName);
        $("#deleteRolesId").val(rolesId);
        $("#modal-delete-roles").modal();
    }

    function assignOperatorRole(rolesType)
    {
        $('.graph-loader').addClass("show").removeClass("hide");
        $.ajax({
            url: "<?php echo base_url('Roles/getAssignOperatorRole') ?>",
            type: "post",
            data: {rolesType:rolesType},
            success: function (response) 
            {   
                var response = JSON.parse(response);
                $("#hide_menu").html(response.allUsers);
                $("#show_menu").html(response.assignUsersHtml);
                $("#assignOperator").val(response.assignOperator);
                $("#oldAssignOperator").val(response.assignOperator);
                $("#rolesType").val(rolesType);
                $('.graph-loader').addClass("hide").removeClass("show");
                $("#AddUsers").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });
        
    }

    function openCity(cityName, elmnt, color) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablink");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].style.backgroundColor = "";
        }
        document.getElementById(cityName).style.display = "block";
        elmnt.style.backgroundColor = color;

    }
// Get the element with id="defaultOpen" and click on it
document.getElementById("defaultOpen").click();


function in_array(needle, haystack) {
    var found = 0;
    for (var i = 0, len = haystack.length; i < len; i++) {
        if (haystack[i] == needle) return i;
        found++;
    }
    return -1;
}

   
    function delete_User(userId)
    {
        $("#deleteUserId").val(userId);
        $("#modal-delete-users").modal();
    } 

    function Edit_user(userId)
    {
        $('.graph-loader').addClass("show").removeClass("hide");
         $.ajax({
            url: "<?php echo base_url('Roles/getUserById') ?>",
            type: "post",
            data: {userId:userId},
            success: function (response) 
            {   
                var obj = JSON.parse(response);

                if(obj.userRole != "")
                {
                    $('#editUserRole option[value='+obj.userRole+']').attr('selected','selected');
                }
                $("#editUserImsge").attr("src","<?php echo base_url("assets/img/user/") ?>"+obj.userImage);
                $("#editUserName").val(obj.userName);
                $("#editUserId").val(obj.userId);
                $("#currentPassword").val(obj.userPassword);
                $("#workingMachineHtml").html(obj.workingMachineHtml);
                $("#assignMachineHtml").html(obj.assignMachineHtml);
                $(".userMachineSel").select2({ placeholder: "Select machines" }); 
                $('.graph-loader').addClass("hide").removeClass("show");
                $("#modalEditUserList").modal();
            },
            error: function(jqXHR, textStatus, errorThrown) {
               console.log(textStatus, errorThrown);
            }
        });

    } 

    function filterUserName(userId,userName)
    {
        var filterUserName = $('.filterUserName:checkbox:checked').map(function()
        {
                                      return $(this).val();
                                }).get();
        // console.log(filterUserName);
        if (filterUserName.length > 0) 
        {
            $("#filterUserNameSelected").css('color','#FF8000');
            if (in_array(userId,filterUserName) != -1) 
            {
                html = "<button id='removeUserButton"+userId+"' style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>Operator - "+userName+" &nbsp;&nbsp;<i onclick='uncheckUserName("+userId+")' class='fa fa-times'></i></button>";
                $("#showUserFilter").append(html);
            }
            else
            {
                $("#removeUserButton"+userId).remove();
            }
        }
        else
        {
            $("#showUserFilter").html("");
            $("#filterUserNameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckUserName(userId)
    {
        $("#removeUserButton"+userId).remove();
        $("#filterUserName"+userId).prop('checked',false);
        var filterUserName = $('.filterUserName:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterUserName.length > 0) 
        {

        }
        else
        {
            $("#filterUserNameSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function filterRoleAss(rolesId,rolesName)
    {
        var filterRoleAss = $('.filterRoleAss:checkbox:checked').map(function()
        {
                                      return $(this).val();
                                }).get();
        // console.log(filterRoleAss);
        if (filterRoleAss.length > 0) 
        {
            $("#filterRolesAssignedSelected").css('color','#FF8000');
            if (in_array(rolesId,filterRoleAss) != -1) 
            {
                html = "<button id='removeUserButton"+rolesId+"' style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>Operator - "+rolesName+" &nbsp;&nbsp;<i onclick='uncheckRolessId("+rolesId+")' class='fa fa-times'></i></button>";
                $("#showUserFilter").append(html);
            }
            else
            {
                $("#removeUserButton"+rolesId).remove();
            }
        }
        else
        {
            $("#showUserFilter").html("");
            $("#filterRolesAssignedSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckRolessId(rolesId)
    {
        $("#removeUserButton"+rolesId).remove();
        $("#filterRoleAss"+rolesId).prop('checked',false);
        var filterRoleAss = $('.filterRoleAss:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterRoleAss.length > 0) 
        {

        }
        else
        {
            $("#filterRolesAssignedSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }


    // function filterRolesAssigned()
    // {
    //     var filterRolesAssigned = $('.filterRolesAssigned:checkbox:checked').map(function(){
    //                                   return $(this).val();
    //                             }).get();

    //     if (filterRolesAssigned.length > 0) 
    //     {
    //         $("#filterRolesAssignedSelected").css('color','#FF8000');

    //         var html = "";
    //         if (in_array("'0'",filterRolesAssigned) != -1) 
    //         {
    //             html += "<button style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRoleOperator' class='btn btn-sm btn-primary'>Role Assigned - Operator &nbsp;&nbsp;<i onclick='uncheckRepeat(1)' class='fa fa-times'></i></button>";
    //         }
            
    //         if (in_array("'1'",filterRolesAssigned) != -1) 
    //         {
    //             html += "<button style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRoleManager' class='btn btn-sm btn-primary'>Role Assigned - Manager &nbsp;&nbsp;<i onclick='uncheckRepeat(2)' class='fa fa-times'></i></button>";
    //         }

    //         if (in_array("'2'",filterRolesAssigned) != -1) 
    //         {
    //             html += "<button style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterRoleAdministrator' class='btn btn-sm btn-primary'>Role Assigned - Administrator &nbsp;&nbsp;<i onclick='uncheckRepeat(3)' class='fa fa-times'></i></button>";
    //         }

    //         $("#showRole").html(html);
    //     }
    //     else
    //     {
    //         $("#filterRolesAssignedSelected").css('color','#b8b0b0');
    //         $("#showRole").html("");
    //     }

    //     $("#empTable").DataTable().ajax.reload();
    // }



    // function uncheckRepeat(id)
    // {
    //     if (id == "1") 
    //     {
    //         $("#filterRoleOperator").prop('checked',false);
    //         $("#buttonFilterRoleOperator").remove();
    //     }
    //     else if (id == "2") 
    //     {
    //         $("#filterRoleManager").prop('checked',false);
    //         $("#buttonFilterRoleManager").remove();
    //     }
    //     else if (id == "3") 
    //     {
    //         $("#filterRoleAdministrator").prop('checked',false);
    //         $("#buttonFilterRoleAdministrator").remove();
    //     }
       

    //     var filterRolesAssigned = $('.filterRolesAssigned:checkbox:checked').map(function(){
    //                                   return $(this).val();
    //                             }).get();

    //     if (filterRolesAssigned.length > 0) 
    //     {

    //     }
    //     else
    //     {
    //         $("#filterRolesAssignedSelected").css('color','#b8b0b0');
    //     }
    //     $("#empTable").DataTable().ajax.reload();
    // }

    function readURL(input) 
        {
            var reader = new FileReader();
            reader.onload = function (e) 
            {
                $(".file-upload-image").css("width","139px");
                $(".file-upload-image").css("height","139px");
                $(".file-upload-image").css("margin","0px");
                $(".file-upload-image").css("border-radius","50%");
                $('.file-upload-image').attr('src', e.target.result);
                $('.image-title').html(input.files[0].name);
            };
           reader.readAsDataURL(input.files[0]);
        }


        function filterRoleAss(rolesId,rolesName)
    {
        var filterRoleAss = $('.filterRoleAss:checkbox:checked').map(function()
        {
                                      return $(this).val();
                                }).get();
        // console.log(filterRoleAss);
        if (filterRoleAss.length > 0) 
        {
            $("#filterRolesAssignedSelected").css('color','#FF8000');
            if (in_array(rolesId,filterRoleAss) != -1) 
            {
                html = "<button id='removeUserButton"+rolesId+"' style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' class='btn btn-sm btn-primary'>Operator - "+rolesName+" &nbsp;&nbsp;<i onclick='uncheckRolessId("+rolesId+")' class='fa fa-times'></i></button>";
                $("#showUserFilter").append(html);
            }
            else
            {
                $("#removeUserButton"+rolesId).remove();
            }
        }
        else
        {
            $("#showUserFilter").html("");
            $("#filterRolesAssignedSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

    function uncheckRolessId(rolesId)
    {
        $("#removeUserButton"+rolesId).remove();
        $("#filterRoleAss"+rolesId).prop('checked',false);
        var filterRoleAss = $('.filterRoleAss:checkbox:checked').map(function(){
                                      return $(this).val();
                                }).get();
        if (filterRoleAss.length > 0) 
        {

        }
        else
        {
            $("#filterRolesAssignedSelected").css('color','#b8b0b0');
        }
        $("#empTable").DataTable().ajax.reload();
    }

 





$(document).ready(function() 
{



    $('.datepicker58').datepicker({
        format: "mm/dd/yyyy",
        autoclose: true,
        daysOfWeekDisabled: [0, 6],
    }); 
    <?php
      $dateValE = date("F d, Y", strtotime('today'));
      $dateValS = date("F d, Y", strtotime('today - 3000 days')); 
    ?>
    var DdateValE = "<?php echo $dateValE; ?>"
    var DdateValS = "<?php echo $dateValS; ?>"

    $('#advance-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE),
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                '<?php echo Today; ?>': [moment(), moment()],
                '<?php echo Yesterday; ?>': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                '<?php echo Last7Days; ?>': [moment().subtract(6, 'days'), moment()],
                '<?php echo Last30Days; ?>': [moment().subtract(29, 'days'), moment()],
                '<?php echo ThisMonth; ?>': [moment().startOf('month'), moment().endOf('month')],
                '<?php echo LastMonth; ?>': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: '<?php echo Submit; ?>',
                cancelLabel: '<?php echo Cancel; ?>',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: '<?php echo Custom; ?>',
                daysOfWeek: ['<?php echo Su ?>', '<?php echo Mo ?>', '<?php echo Tu ?>', '<?php echo We ?>', '<?php echo Th ?>', '<?php echo Fr ?>','<?php echo Sa ?>'],
                monthNames: ['<?php echo January ?>', '<?php echo February ?>', '<?php echo March ?>', '<?php echo April ?>', '<?php echo May ?>', '<?php echo June ?>', '<?php echo July ?>', '<?php echo August ?>', '<?php echo September ?>', '<?php echo October ?>', '<?php echo November ?>', '<?php echo December ?>'],
                firstDay: 1
            }
        },

        function(start, end, label) {
            if (label == "Custom") {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

                var label = label + "(" + startDate + " - " + endDate + ")";
            }
            $("#showAddeddDate").html("<button style='margin-left:20px;font-weight: 400;font-size: 11px;margin-top:10px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'><?php echo AddedDate; ?> - " + label + " &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");

            $('#advance-daterange').css("color", "#FF8000");
        });

    $('#empTable').removeAttr('width').DataTable({
        "language": {
            "lengthMenu": "<?php echo Show; ?> _MENU_ <?php echo entries; ?>",
            "search": "<?php echo Search; ?>:",
            "processing": "<?php echo processing; ?>...",
            "zeroRecords": "<?php echo Nomatchingrecordsfound; ?>",
            "info": "<?php echo Showing; ?>  _START_ <?php echo to; ?>  _END_ <?php echo of; ?> _TOTAL_ <?php echo entries; ?>",
            "infoFiltered": "(<?php echo filteredfrom; ?> _MAX_ <?php echo totalentries; ?>)",
             "paginate": {
              "first": "<?php echo First; ?>",
              "last": "<?php echo Last; ?>",
              "next": "<?php echo Next; ?>",
              "previous": "<?php echo Previous; ?>",
              "page": "<?php echo Page; ?>",
              "of": "<?php echo of; ?>"
            }
          },
        'processing': true,
        "pagingType": "input",
        'serverSide': true,
        'serverMethod': 'post',
        "pageLength": 100,
        "info": false,
        "searching": false,
        "lengthChange": false,
        "stateSave": true,
        "rowId": 'userId',
        'ajax': {
            'url': 'RolesandTeams_pagination',
            "type": "POST",
            "data": function(data) {
                data.dateValS   = $('#dateValS').val();
                data.dateValE   = $('#dateValE').val();
                data.userId     = $('.filterUserName:checkbox:checked').map(function(){
                                    return $(this).val();
                                }).get();
                data.userName     = $('.filterUserName:checkbox:checked').map(function(){
                                    return $(this).val();
                                }).get();

                // data.userRole   = $('.filterRolesAssigned:checkbox:checked').map(function() {
                //                     return $(this).val();
                //                 }).get();
                data.userRole   = $('.filterRoleAss:checkbox:checked').map(function() {
                                    return $(this).val();
                                }).get();
            },
        },
        'columns': [
                {data: 'userId'},
                {data: 'userName'},
                {data: 'createdDate'},
                {data: 'userRole'},
                {data: 'Edit'},
                {data: 'delete'},
        ],
        "columnDefs": [{
                targets: 0,
                orderable: false,
            },
            {
                targets: 1,
                orderable: false,
            },
            {
                targets: 2,
                orderable: false,
            },
            {
                targets: 3,
                orderable: false,
            },
            {
                targets: 4,
                orderable: false,
            },
            {
                targets: 5,
                orderable: false,
            }
        ],
        fixedColumns: true,
        drawCallback: function(settings) {

        },
    });


   $('#submit').submit(function(e)
        {
            e.preventDefault(); 
            $('#messages').html('');
                $.ajax({
                        url:'<?php echo base_url('Roles/AddNewUser');?>',
                        type:"post",
                        data:new FormData(this),
                        processData:false,
                        contentType:false,
                        cache:false,
                        async:false,
                        success: function(data)
                        {
                            var response = JSON.parse(data);
                            if (response.status == true) 
                            {
                                // alert(response.message);
                                $('#messages').html(response.message);
                                $('#messages').css('color', 'green');
                            }
                            else
                            {
                                // $('#messages').html(response.message);
                                // $('#messages').css('color', 'red');
                                alert(response.message);
                            }
                        },
                        
                    });
                });
            


    $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) {
        var dateValS = picker.startDate.format('YYYY-MM-DD');
        var dateValE = picker.endDate.format('YYYY-MM-DD');
        $('#dateValS').val(dateValS);
        $('#dateValE').val(dateValE);
        $("#empTable").DataTable().ajax.reload();
    });

});

function uncheckCreatedDate() {
    $("#showAddeddDate").html("");
    $('#advance-daterange').css("color", "#b8b0b0");

    <?php
    $dateValE = date("Y-m-d");
    $dateValS = date("Y-m-d", strtotime("-3000 day"));

    $dateValESET = date("Y-m-d", strtotime('today'));
    $dateValSSET = date("Y-m-d", strtotime('today - 3000 days'));

    ?>
    var DdateValE = "<?php echo $dateValE; ?>"
    var DdateValS = "<?php echo $dateValS; ?>"

    $('#dateValS').val("<?php echo $dateValSSET; ?>");
    $('#dateValE').val("<?php echo $dateValESET; ?>");
    $('#advance-daterange').daterangepicker({
            format: 'MM/DD/YYYY',
            startDate: moment(DdateValS),
            endDate: moment(DdateValE),
            minDate: '01/01/2018',
            maxDate: '12/31/2050',
            showDropdowns: true,
            showWeekNumbers: true,
            timePicker: false,
            timePickerIncrement: 1,
            timePicker12Hour: true,
            ranges: {
                'Today': [moment(), moment()],
                'Yesterday': [moment().subtract(1, 'days'), moment().subtract(1, 'days')],
                'Last 7 Days': [moment().subtract(6, 'days'), moment()],
                'Last 30 Days': [moment().subtract(29, 'days'), moment()],
                'This Month': [moment().startOf('month'), moment().endOf('month')],
                'Last Month': [moment().subtract(1, 'month').startOf('month'), moment().subtract(1, 'month').endOf('month')]
            },
            opens: 'right',
            drops: 'down',
            buttonClasses: ['btn', 'btn-sm'],
            applyClass: 'btn-primary',
            cancelClass: 'btn-default',
            separator: ' to ',
            locale: {
                applyLabel: 'Submit',
                cancelLabel: 'Cancel',
                fromLabel: 'From',
                toLabel: 'To',
                customRangeLabel: 'Custom',
                daysOfWeek: ['SUN', 'MON', 'TUE', 'WED', 'THU', 'FRI', 'SAT'],
                monthNames: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December'],
                firstDay: 1
            }
        },

        function(start, end, label) {
            if (label == "Custom") {
                var startDate = new Date(start);
                var startDate = startDate.getFullYear() + '-' + (startDate.getMonth() + 1) + '-' + startDate.getDate();

                var endDate = new Date(end);
                var endDate = endDate.getFullYear() + '-' + (endDate.getMonth() + 1) + '-' + endDate.getDate();

                var label = label + "(" + startDate + " - " + endDate + ")";
            }
            $("#showAddeddDate").html("<button style='margin-left:20px;margin-top:10px;font-weight: 400;font-size: 11px;' id='buttonFilterstatusCompleted' class='btn btn-sm btn-primary'>Addedd Date - " + label + " &nbsp;&nbsp;<i onclick='uncheckCreatedDate()' class='fa fa-times'></i></button>");
            $('#advance-daterange').css("color", "#FF8000");
        });

    $('#advance-daterange').on('apply.daterangepicker', function(ev, picker) {
        var dateValS = picker.startDate.format('YYYY-MM-DD');
        var dateValE = picker.endDate.format('YYYY-MM-DD');
        $('#dateValS').val(dateValS);
        $('#dateValE').val(dateValE);
        $("#empTable").DataTable().ajax.reload();
    });
    $("#empTable").DataTable().ajax.reload();
}




    


     $(document).ready(function() 
    {
        $('form#add_operator_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#submit_new_Operator").html('<?php echo Saving; ?>...')
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Roles/add_new_operator'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){
                    if($.trim(data) == "1") { 
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: '<?php echo Operatoraddedsuccessfully; ?>.'
                        })
                        form.each(function(){
                            this.reset();
                        }); 
                        $("#submit_new_Operator").html('<?php echo Saved; ?>...')
                        $("#modal_addnewOperator").modal('hide').fadeOut(1500); 
                        $("#submit_new_Operator").html('Save')
                        window.setTimeout(function(){location.reload()})
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: data,
                        })
                        $("#submit_new_Operator").html('<?php echo Save; ?>')
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: '<?php echo Error; ?>',
                        text: '<?php echo Errorwhileaddinguser; ?>.',
                        fade_out_speed: 20000
                    })
                    location.reload();
                }
            });
        });

        $('form#delete_operator_form').submit(function(e) 
        { 
            var answer = $("#Deleted").val();
            if (answer != 'DELETE') 
            {
                $.gritter.add({
                    title: '<?php echo Error; ?>',
                    text: "<?php echo Operatorisnotdeleted; ?>"
                })
                return false;
            } 
            else 
            {
                var form = $(this);
                e.preventDefault();
                $("#delete_operator_submit").html('<?php echo Deleting; ?>...')
                var formData = new FormData(this);
                $.ajax({ 
                    type: 'POST',
                    url: "<?php echo site_url('Roles/delete_user'); ?>",
                    data:formData,
                    cache:false,
                    contentType: false,
                    processData: false,
                    dataType: "html",
                    success: function(data){
                        if($.trim(data) == "1") { 
                            $.gritter.add({
                                title: '<?php echo Success; ?>',
                                text: '<?php echo Userdeletedsuccessfully; ?>.'
                            })
                            
                            $("#delete_operator_submit").html('<?php echo Deleted; ?>');

                            location.reload();
                            
                        } 
                        else 
                        { 
                            $.gritter.add({
                                title: '<?php echo Error; ?>',
                                text: "Error"
                            })
                            $("#delete_operator_submit").html('<?php echo Delete; ?>');
                        }
                    },
                    error: function() 
                    { 
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: '<?php echo Errorwhileadding ; ?>user.'
                        })
                       // location.reload();
                    }
                });
            }
        });

        $('form#edit_user_form').submit(function(e) 
        { 
            var form = $(this);
            e.preventDefault();
            $("#edit_user_submit").html('<?php echo Updating; ?>...')
            var formData = new FormData(this);
            $.ajax({ 
                type: 'POST',
                url: "<?php echo site_url('Roles/edit_save_user'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data){

                    var obj = JSON.parse(data);
                    if(obj.status == true) { 
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: '<?php echo Usereditsuccessfully; ?>.'
                        })
                        
                        $("#edit_user_submit").html('<?php echo Updated; ?>');

                        socket.emit('Assign machine', obj.userId); 

                        location.reload();
                        
                    } 
                    else 
                    { 
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: obj.message
                        })
                        $("#edit_user_submit").html('<?php echo Update; ?>');
                    }
                },
                error: function() 
                { 
                    $.gritter.add({
                        title: '<?php echo Error; ?>',
                        text: 'Error while adding user.'
                    })
                   // location.reload();
                }
            });
        });

        $('#add_rolesResponsibilities_form').submit(function(e)
        {
            var form = $(this);
            e.preventDefault();
            $('#add_rolesResponsibilities_submit').text('<?php echo Saving; ?>...');
            $('#add_rolesResponsibilities_submit').attr('disabled',true);
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Roles/add_rolesResponsibilities'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var response = JSON.parse(data);
                    if (response.status == true) 
                    {
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: response.message
                        });
                        location.reload();
                    }
                    else
                    {
                        $('#add_rolesResponsibilities_submit').attr('disabled',false);
                        $('#add_rolesResponsibilities_submit').text('<?php echo Save; ?>');
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: response.message
                        });
                    }
                },
                
            });
        });

        $('form#edit_rolesResponsibilities_form').submit(function(e)
        {
            var form = $(this);
            e.preventDefault();
            $('#edit_rolesResponsibilities_submit').attr('disabled',true);
            $('#edit_rolesResponsibilities_submit').text('<?php echo Updating; ?>...');
            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Roles/edit_rolesResponsibilities'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var response = JSON.parse(data);
                    if (response.status == true) 
                    {
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: response.message
                        });
                        location.reload();
                    }
                    else
                    {
                        $('#edit_rolesResponsibilities_submit').attr('disabled',false);
                        $('#edit_rolesResponsibilities_submit').text('Save');
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: response.message
                        });
                    }
                },
                
            });
        });

        $('#assignOperatorToRole_form').submit(function(e)
        {
            
            
            var form = $(this);
            e.preventDefault();
            $('#assignOperatorToRole_submit').text('<?php echo Saving; ?>...');
            $('#assignOperatorToRole_submit').attr('disabled',true);
            var formData = new FormData(this);

            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Roles/assignOperatorToRole'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var response = JSON.parse(data);
                    if (response.status == true) 
                    {
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: response.message
                        });
                        location.reload();
                    }
                    else
                    {
                        $('#assignOperatorToRole_submit').attr('disabled',false);
                        $('#assignOperatorToRole_submit').text('Save');
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: response.message
                        });
                    }
                },
                
            });
        });

        $('#deleteRoles_form').submit(function(e)
        {

            var form = $(this);
            e.preventDefault();
            $('#deleteRoles_submit').text('<?php echo deleteing; ?>...');
            $('#deleteRoles_submit').attr('disabled',true);
            var formData = new FormData(this);
            $.ajax({
                type: 'POST',
                url: "<?php echo site_url('Roles/deleteRoles'); ?>",
                data:formData,
                cache:false,
                contentType: false,
                processData: false,
                dataType: "html",
                success: function(data)
                {
                    var response = JSON.parse(data);
                    if (response.status == true) 
                    {
                        $.gritter.add({
                            title: '<?php echo Success; ?>',
                            text: response.message
                        });
                        location.reload();
                    }
                    else
                    {
                        $('#deleteRoles_submit').attr('disabled',false);
                        $('#deleteRoles_submit').text('<?php echo Save; ?>');
                        $.gritter.add({
                            title: '<?php echo Error; ?>',
                            text: response.message
                        });
                    }
                },
                
            });
        });

    });




</script>