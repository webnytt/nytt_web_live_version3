<script type="text/javascript">

		$('#i_file').change( function(event) 
		{
		    var name = event.target.files[0].name;
		    $("#disp_tmp_path").html(name);
		});
		
		function checkwordcount()
		{
			var description = $("#description").val();
			var descriptionLength = description.length;
			$("#descriptionLength").text(descriptionLength);
		}

		$('form#add_helpReport_form').submit(function(e) 
		{ 
	        var form = $(this);
	    	e.preventDefault();
	        $("#add_helpReport_submit").prop("type", "button");
	        $("#add_helpReport_submit").html('<?php echo Saving; ?>...')
	        
	        var formData = new FormData(this);
				$.ajax({ 
	                type: 'POST',
	                url: "<?php echo site_url('admin/add_helpReport'); ?>",
	                data:formData,
	                cache:false,
	                contentType: false,
	                processData: false,
	                dataType: "html",
	                success: function(response)
	                {
	                	if (response.trim() == "success") 
	                	{
		                    $.gritter.add({
		            			title: 'Success',
		            			text: "<?php echo Helpreported; ?>."
		            		})
		                    
		                    //$("#add_helpReport_submit").html('Submitted')
		                    window.setTimeout(function(){location.reload()},3000)
	                	}
	                	else
	                	{
	                		$.gritter.add({
		            			title: 'Error',
		            			text: response
		            		})
	                	}
					},
	                error: function() 
	                { 
	                    alert("Error while update help report.");
	                }
	           });
	    });

	</script>