<link href="<?php echo base_url('assets/css/configure_version_new.css');?>" rel="stylesheet" />
<div id="content" class="content">
  
  <h1 style="font-size: 22px;color: #002060" class="page-header">Configure Version</h1>

  <div class="row">
  <input type="hidden" name="versionId"  value="0">
    <div class="col-md-12 table_data" style="padding-left:14px;">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;" >
       <div class="row" style="float:left;margin: 10px;">
          <span class="displayfilterrowstyle" id="showStacklightFilterNamefilter"></span>
          <span class="displayfilterrowstyle" id="showAddeddDate"></span>
        </div>
        <div class="panel-body">
          <table id="empTable" class="display table m-b-0"  width="100%" cellspacing="0">
            <thead>
              <tr>  
                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;min-width: 100px!important;">Version Id<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th> 
                
                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterstackLightTypeNameSelected" class="dropdown" style="padding: 4px;border-radius: 5px;min-width: 110px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterstackLightTypeNameSelectedValue"> Stacklight Type Name&nbsp;</span>
                    <div class="dropdown-menu">
                      <?php foreach ($stackLightType as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterstackLightTypeName(<?php echo $value['stackLightTypeId'] ?>,'<?php echo $value['stackLightTypeName'] ?>')" class="form-check-input filterstackLightTypeName" type="checkbox" id="filterstackLightTypeName<?php echo $value['stackLightTypeId'] ?>" name="filterstackLightTypeName" 
                              value="<?php echo $value['stackLightTypeId']; ?>">
                            <label class="form-check-label" for="filterstackLightTypeName<?php echo $value['stackLightTypeId'] ?>">
                              <?php echo $value['stackLightTypeName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Version Name<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th> 

                <th style="color: #b8b0b0;cursor: pointer;padding: 6px 15px !important;">Files<br>
                  <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
                
                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important;" >
                  <input type="hidden" name="dateValS" id="dateValS" value="<?php echo date("Y-m-d", strtotime("-3000 day")); ?>" />  
                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo date("Y-m-d"); ?>" />   
                    <div id="advance-daterange" name="advance-daterange" style="width: 87px;padding-bottom: 5px!important">
                      <span>
                          Added Date&nbsp;&nbsp;
                      </span> 
                     <i class="fa fa-caret-down m-t-2"></i>
                    </div>
                    <small  style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div> 

    <div class="col-md-3 detail" style="margin-top: -74px;margin-bottom: -63px;background: #FFFFFF !important;border-bottom-left-radius: 5px;   border-bottom-right-radius: 5px;box-shadow: gray -10px 10px 10px -10px;display: none;height: 100%;position: fixed;right: 0%;width: 23%;overflow: auto;">
       <div class="panel panel-inverse panel-primary" >
        <div class="panel-body" style="padding: 0;">
            
            <div class="row" style="padding: 8px;padding-top: 0 !important;background: #002060;height: 164px; border-top-left-radius: 5px;    border-top-right-radius: 5px;margin-bottom: 30px;">
              <div class="col-md-12" style="height: 47px !important;">
                <div style="padding: 14px 0 0 0;">
                  <a href="javascript:void(0);" onclick="closeDetails();" style="opacity: 1.0!important;">
                    <img width="16" src="<?php echo base_url("assets/img/cross.svg"); ?>"></a>
                </div>
              </div>
              <div class="col-md-12" >
               <center>
                  <span style="color: #9c9aab;">
                     <img id="stackLightTypeImage" style="border-radius: 50%;width: 40%;height: 90px;" 
                        src="<?php echo base_url('assets/img/stackLightType/2.jpg'); ?>">
                  </span> 
                <span style="color: white;" id="userName"></span> </center>
              </div>
            </div>

            <form class="p-b-20" action="update_task_form" method="POST" id="update_task_form" >
              <input type="hidden" id="taskId" name="taskId">
              <div class="row" style="padding-left: 15px;padding-right: 15px;">
                <div class="col-md-12">
                  <small>Version Id</small>
                  <textarea style="    margin-top: 5px;border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="versionId" id="versionId"></textarea>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>

              <div class="row" style="padding-left: 15px;padding-right: 15px;">
                <div class="col-md-12" style="margin-bottom: 5px;">
                  <small>Stacklight Type Name</small>
                   <textarea style="    margin-top: 5px;border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="stackLightTypeName" id="stackLightTypeName"></textarea>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>

               <div class="row" style="padding-left: 15px;padding-right: 15px;">
                <div class="col-md-12" style="margin-bottom: 5px;">
                  <small>Version Name</small>
                   <textarea style="    margin-top: 5px;border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="versionName" id="versionName"></textarea>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>

               <div class="row" style="padding-left: 15px;padding-right: 15px;">
                <div class="col-md-12" style="margin-bottom: 5px;">
                  <small>Added Date</small>
                   <textarea style="    margin-top: 5px;border: hidden;width: 100%;height: 53px;color: #124D8D;font-weight: 600;" name="versionDate" id="versionDate"></textarea>
                  <hr style="height: 2px;margin-top: 0rem;">
                </div>
              </div>
              
            </form>
          </div>
        </div>
        <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
      </div>
    


  <div class="modal fade" id="modal-add-stackLightType" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header" style="background-color: #002060;">
              <h4 style="color: #FF8000;padding: 4px;" class="modal-title">Add stacklight type</h4>
              <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
        </div>
        <div class="modal-body">
          <h4 class="modal-title m-b-10"></h4>
          <div id="add_stackLightType_error" class="m-b-10 m-r-10 alert alert-danger fade hide"></div>
          <div id="add_stackLightType_success" class="m-b-10 m-r-10 alert alert-success fade hide" ></div>
          <form class="p-b-20" action="" method="POST" id="add_stackLightType_form" enctype="multipart/form-data" > 
            <div class="row">
              <div class="form-group col-md-6" data-toggle="tooltip" data-title="Stack light type name" >
                <label>Name</label>
                <input type="text" class="form-control" id="stackLightTypeName" name="stackLightTypeName" placeholder="Enter stacklight type name" required > 
              </div>
              <div class="form-group col-md-6">
                <label>Image</label>
                <input type="file" class="form-control" id="stackLightTypeImage" name="stackLightTypeImage" required>
              </div>
            
              <div class="form-group col-md-6">
                <label>Upload file classification.cfg</label>
                <input type="file" class="form-control col-md-12" id="classification_cfg" name="classification_cfg" required >
              </div>
              <div class="form-group col-md-6">
                <label>Upload file classification.weights</label>
                <input type="file" class="form-control col-md-12" id="classification_weights" name="classification_weights" required >
              </div>
              <div class="form-group col-md-6">
                <label>Upload file detection.cfg</label>
                <input type="file" class="form-control col-md-12" id="detection_cfg" name="detection_cfg" required >
              </div>
              <div class="form-group col-md-6">
                <label>Upload file detection.weights</label>
                <input type="file" class="form-control col-md-12" id="detection_weights" name="detection_weights" required >
              </div>
              <div class="form-group col-md-6">
                <label>Upload file label.txt</label>
                <input type="file" class="form-control col-md-12" id="label_txt" name="label_txt" required >
              </div>
              <div class="form-group col-md-6">
                <label>Upload file labels.txt</label>
                <input type="file" class="form-control col-md-12" id="labels_txt" name="labels_txt" required >
              </div>
            </div>
            <center>
              <button type="submit" class="btn btn-sm btn-primary m-r-5" id="add_stackLightType_submit">Save</button>
            </center>
          </form>
        </div>
      </div>
    </div>
  </div>
</div>

