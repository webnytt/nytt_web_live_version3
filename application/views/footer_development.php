	<script> COLOR_BLACK = 'rgba(255,255,255,0.75)'; </script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/js/bootstrap-timepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/chart-js/Chart.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/jquery.dataTables.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/media/js/dataTables.bootstrap.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/DataTables/extensions/Responsive/js/dataTables.responsive.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/gritter/js/jquery.gritter.js"></script> 
	<script src="<?php echo base_url(); ?>assets/plugins/isotope/jquery.isotope.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/lightbox/js/lightbox.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/moment.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/input.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/select2/dist/js/select2.min.js"></script>
	<script src="<?php echo base_url(); ?>assets/plugins/image-picker/image-picker.min.js"></script> 


	<script src="<?php echo base_url('assets/slick/slick.js'); ?>" type="text/javascript" charset="utf-8"></script>
</body>
</html>
<script>

		function noStacklightFunc()
		{
			if($("#noStacklight").is(':checked'))
			{
			    $(".noStacklight").fadeOut( "slow" ); 
			    $(".noStacklightShow").fadeIn( "slow" ); 
			}
			else
			{
			    $(".noStacklightShow").fadeOut( "slow" );
			    $(".noStacklight").fadeIn( "slow" );
			}
		}


		var getUrl = window.location;
		function ckeck_function(machineId)
		{
			stacklight_disabled_color_value('running',machineId);
			stacklight_disabled_color_value('waiting',machineId);
			stacklight_disabled_color_value('stopped',machineId);
			stacklight_disabled_color_value('off',machineId);
		}

		function get_machine_name(name)
		{
			if (name != "") 
			{
				$(".machine_name").text(name);
			}else
			{
				$(".machine_name").text("Your machine name");
			}
		}

		function get_machine_name_edit(name,machineId)
		{
			if (name != "") 
			{
				$(".machine_name_"+machineId).text(name);
			}
			else
			{
				$(".machine_name_"+machineId).text("Your machine name");
			}
		}

		function machineLoadingChange(value)
		{
			if (value == "Manual") 
			{
				$("#loading_type").hide();
			}else
			{
				$("#loading_type").show();
			}
		}

		function changeHourStatus(status)
		{
			if (status == "0") 
			{
				message = "Are you sure reduce an hour from clock!";
			}else
			{
				message = "Are you sure increase an hour from clock!";
			}
			var r = confirm(message);
			if (r == true) 
			{
				  $.ajax({
			        url: "<?php echo base_url("admin/updateHourStatus") ?>",
			        type: "post",
			        data: {status:status} ,
			        success: function (data) 
			        {
			        	location.reload();
			        	
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
			} 
		}

		function machineLoadingChangeMachine(value,machineId)
		{
			if (value == "Manual") 
			{
				$("#loading_type_"+machineId).hide();
			}else
			{
				$("#loading_type_"+machineId).show();
			}
		}

		function add_color_sequence(title)
		{

			title_value1 = title.split(" - ");

			title_value = title_value1.join(",");


			$('#running  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#waiting  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#stopped  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);
			$('#off  option[value="' + title_value + '"]').not(':selected').attr("disabled",false);

			window.setTimeout(function(){
				disabled_color_value("running");
				disabled_color_value("waiting");
				disabled_color_value("stopped");
				disabled_color_value("off");
			},100);



		}


		function add_stacklight_color_sequence(title,type,machineId)
		{
		
			title_value1 = title.split(" - ");
			title_value = title_value1.join(",");
			html = "<option value='"+title_value+"'>"+title+"</option>";
			$('#running'+machineId+' option[value="' + title_value + '"]').remove();
			$('#waiting'+machineId+' option[value="' + title_value + '"]').remove();
	    	$('#stopped'+machineId+' option[value="' + title_value + '"]').remove();
		    $('#off'+machineId+' option[value="' + title_value + '"]').remove();
			$("#running"+machineId).append(html);
			$("#waiting"+machineId).append(html);
			$("#stopped"+machineId).append(html);
			$("#off"+machineId).append(html);
		}

		function disabled_color_value(type)
		{	
			disabled_value = [];
			$("#"+type+" :selected").map(function(i, el) {
	       		 disabled_value.push($(el).val());
			}).get();

			$.each(disabled_value, function(k, v) 
	        {
	        	if (type == "running") 
	        	{
	        		
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
	        	}
	        	if (type == "waiting") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
			    }
	        	if (type == "stopped") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
				    $('#off option[value="' + v + '"]').attr("disabled",true);
			    }
	        	if (type == "off") 
	        	{
	        		
			    	$('#running option[value="' + v + '"]').attr("disabled",true);
			    	$('#waiting option[value="' + v + '"]').attr("disabled",true);
			    	$('#stopped option[value="' + v + '"]').attr("disabled",true);
				}


				$("#running").select2("destroy");
				$("#waiting").select2("destroy");
				$("#stopped").select2("destroy");
				$("#off").select2("destroy");

				$("#running").select2({
					placeholder: "Select an option"
				});
				$("#waiting").select2({
					placeholder: "Select an option"
				});
				$("#stopped").select2({
					placeholder: "Select an option"
				});
				$("#off").select2({
					placeholder: "Select an option"
				});
				$(".select2-selection__choice").attr("onclick","add_color_sequence(this.title)");
			});	
				
		}
		function stacklight_disabled_color_value(type,machineId)
		{	
			disabled_value = [];

	        $("#"+type+machineId+" :selected").map(function(i, el) {
	       		 disabled_value.push($(el).val());
			}).get();
			
			$.each(disabled_value, function(k, v) 
	        {
	        	if (type == "running" && v != "off") 
	        	{
	        		
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
	        	}
	        	if (type == "waiting" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
			    }
	        	if (type == "stopped" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
				    $('#off'+machineId+' option[value="' + v + '"]').remove();
			    }
	        	if (type == "off" && v != "off") 
	        	{
	        		
			    	$('#running'+machineId+' option[value="' + v + '"]').remove();
			    	$('#waiting'+machineId+' option[value="' + v + '"]').remove();
			    	$('#stopped'+machineId+' option[value="' + v + '"]').remove();
				}


				$(".select2-selection__choice").attr("onclick","add_stacklight_color_sequence(this.title,'"+ type +"',"+ machineId +")");
			});	
				
		}



		$(document).ready(function() 
		{

			


			<?php if($this->uri->segment(2) == 'overview2') { ?>
			setInterval(function()
			{
			  	location.reload();
			}, 60 * 10000);

			<?php } ?>


			setInterval(function()
			{
			  	location.href("<?php echo base_url("admin/logout"); ?>");
			}, 7000 * 1000);
		
		$(".js-example-basic-multiple")
		.select2({
			placeholder: "Select an option"
		});

		var selected = new Array();
		var colors_ids = new Array();
		$(".img-check").click(function(){
				ids = $(this).attr('id');
				image = $(this).attr('data-img-src');
				color_name = $(this).attr('data-color-name');
				i = $(this).attr('data-color_id');
				spilit_idds = ids.split("_");
				check_uncheck = 0;
				if ($("#machineLight_"+i).prop('checked')==true)
			    { 
			    	$("#data_image_remove_"+i).remove();
			    	selected = jQuery.grep(selected, function(value) {
					  return value != color_name;
					});

					$(this).removeClass("check");
					$("#machineLight_"+i).attr("checked",false);
					check_uncheck = 1;
			    }else if($("#machineLight_"+i).prop('checked')==false)
			    {
			    	 data = '<div style="text-align: center;" id="data_image_remove_'+i+'"><img  style="width: 35px;height: 35px;border-radius: 5px;display: inline;margin-bottom: 10px;" src="'+image+'" alt="..." class="img-check" ></div>';

			        $("#colorLightSelect").append(data);

			        $("#machineLight_"+i).attr("checked",true);

			        selected.push(color_name);
					$(this).addClass("check");
					check_uncheck = 1;
			    }

				if(check_uncheck = 1){
					html = "";
					if (selected.length == 1) 
				    {
						var nodet_array = ["off"];
				    	nodet_array.push(selected[0]);
				    	html += "<option value='off'>Off</option>";
				    	html += "<option value='"+ selected[0] +"'>"+ selected[0] +"</option>";
				    }
				    else if (selected.length == 2) 
				    {
						var nodet_array = ["off"];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[0]+','+selected[1]);
				    	html += "<option value='off'>Off</option>";
				    	html += "<option value='"+selected[0]+"'>"+ selected[0] +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ selected[1] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+"'>"+ selected[0] +' - '+ selected[1] +"</option>";
				    } else if (selected.length == 3) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	html += "<option value='off'>Off</option>";
				    	html += "<option value='"+selected[0]+"'>"+ selected[0] +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ selected[1] +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[1] +"'>"+ selected[0] +' - '+ selected[1] +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[2] +"'>"+ selected[0] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[2] +"'>"+ selected[1] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1] +','+selected[2] +"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +"</option>";
				    }else if (selected.length == 4) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]);



				    	html += "<option value='off'>Off</option>";
				    	html += "<option value='"+selected[0]+"'>"+ selected[0] +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ selected[1] +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ selected[2] +"</option>";
				    	html += "<option value='"+selected[3]+"'>"+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[1]+"'>"+ selected[0] +' - '+ selected[1] +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[2]+"'>"+ selected[0] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+ selected[3]+"'>"+ selected[0] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[2]+"'>"+ selected[1] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[1]+','+ selected[3]+"'>"+ selected[1] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[2]+','+ selected[3]+"'>"+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+"'>"+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+ selected[0] +','+ selected[1] +','+ selected[2] +','+ selected[3] +"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
				    }else if (selected.length == 5) 
				    {
				    	var nodet_array = ['Off'];
				    	nodet_array.push(selected[0]);
				    	nodet_array.push(selected[1]);
				    	nodet_array.push(selected[2]);
				    	nodet_array.push(selected[3]);
				    	nodet_array.push(selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]);
				    	nodet_array.push(selected[0]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[4]);
				    	nodet_array.push(selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[1]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);
				    	nodet_array.push(selected[0]+'-'+selected[1]+'-'+selected[2]+'-'+selected[3]+'-'+selected[4]);


				    	html += "<option value='off'>Off</option>";
				    	html += "<option value='"+selected[0]+"'>"+ selected[0] +"</option>";
				    	html += "<option value='"+selected[1]+"'>"+ selected[1] +"</option>";
				    	html += "<option value='"+selected[2]+"'>"+ selected[2] +"</option>";
				    	html += "<option value='"+selected[3]+"'>"+ selected[3] +"</option>";
				    	html += "<option value='"+selected[4]+"'>"+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+"'>"+ selected[0] +' - '+ selected[1] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+"'>"+ selected[0] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[2]+"'>"+ selected[1] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[3]+"'>"+ selected[1] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[1]+','+selected[4]+"'>"+ selected[1] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[2]+','+selected[3]+"'>"+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[2]+','+selected[4]+"'>"+ selected[2] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[3]+','+selected[4]+"'>"+ selected[3] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[2] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[3]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
						
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+"'>"+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[4]+"'>"+ selected[1] +' - '+ selected[2] +' - '+ selected[4] +"</option>";
						html += "<option value='"+selected[1]+','+selected[3]+','+selected[4]+"'>"+ selected[1] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
						html += "<option value='"+selected[2]+','+selected[3]+','+selected[4]+"'>"+ selected[2] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
						
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[3]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[3]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
				    	html += "<option value='"+selected[0]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
						
						html += "<option value='"+selected[1]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
						
				    	html += "<option value='"+selected[0]+','+selected[1]+','+selected[2]+','+selected[3]+','+selected[4]+"'>"+ selected[0] +' - '+ selected[1] +' - '+ selected[2] +' - '+ selected[3] +' - '+ selected[4] +"</option>";
				    }
				}  
				$("#running").html(html);
				$("#waiting").html(html);
				$("#stopped").html(html);
				$("#off").html(html);
				$("#nodet").val(nodet_array);
			});
			
			App.init();
			
			$("select#factoryId").select2({  
			  tags: false,
			  minimumResultsForSearch: 4 
			});
			
			$("select#factoryId").on("select2:select", function (evt) { 
				var factoryId = $("select#factoryId").val();
				$.post('<?php echo base_url();?>admin/changeFactory',{factoryId:factoryId},function getattribute(data) {  
					var obj = jQuery.parseJSON( data );
					//console.log(obj); 
					factoryType = obj.factoryType;
					previousFactoryType =  obj.previousFactoryType;

					if(factoryType == previousFactoryType)
					{
						location.reload();
					}else
					{
						location.href = "<?php echo base_url("admin/overview2"); ?>";
					}
				});
			});
		});
	(function(jQuery){
            jQuery.fn.MytoJson = function(options) 
            {
        
            options = jQuery.extend({}, options);
        	var self = this,
                json = {},
                push_counters = {},
                patterns = {
                    "validate": /^[a-zA-Z][a-zA-Z0-9_]*(?:\[(?:\d*|[a-zA-Z0-9_]+)\])*$/,
                    "key":      /[a-zA-Z0-9_]+|(?=\[\])/g,
                    "push":     /^$/,
                    "fixed":    /^\d+$/,
                    "named":    /^[a-zA-Z0-9_]+$/
                };
        		this.build = function(base, key, value){
                base[key] = value;
                return base;
            };
        		this.push_counter = function(key){
                if(push_counters[key] === undefined){
                    push_counters[key] = 0;
                }
                return push_counters[key]++;
            };
        
            jQuery.each(jQuery(this).serializeArray(), function()
            {
        		if(!patterns.validate.test(this.name))
        		{
                    return;
                }
        		var k,
                    keys = this.name.match(patterns.key),
                    merge = this.value,
                    reverse_key = this.name;
        			while((k = keys.pop()) !== undefined){
        			reverse_key = reverse_key.replace(new RegExp("\\[" + k + "\\]$"), '');
        			if(k.match(patterns.push))
        			{
                        merge = self.build([], self.push_counter(reverse_key), merge);
                    }
                    else if(k.match(patterns.fixed)){
                        merge = self.build([], k, merge);
                    }
                    else if(k.match(patterns.named)){
                        merge = self.build({}, k, merge);
                    }
                }
        		json = jQuery.extend(true, json, merge);
            });
        	return json;
        }
        })(jQuery);

	</script>
		<script src='<?php echo base_url('assets/socket/socket.io.js') ?>'></script> 
	<script>

	


		var factoryId = "<?php echo $this->session->userdata('factoryId'); ?>"; 
		var socket = io.connect('https://nyttdev.com:3009?deviceId=bb734da4fa6c5b23&application=SetApp'); 
		//var socket = io.connect('https://nyttdev.com:3022'); 

		var randomNumber;
		function checkLiveStatus(machineId,machineName)
		{
			var randomNumber = $("#randomNumber").val();
			dataInput = {"userId": randomNumber,"machineId" : machineId};
			//socket.emit('Screen Share Request', dataInput);

			socket.emit('Screen Share Request', dataInput, (data) => {
			 // console.log(data); // data will be 'woot'
			});
			$("#machineIdCheckIn").val(machineId);
			$("#liveMachineName").text(machineName.replace("+","'"));
			$("#compareRandomNumber").val(randomNumber);
		}


		function AddBreakdownReason()
		{
			dataInput = {"userId": "56","factoryId" : "5","version" : "test","appVersionCode" : "15"};
			var dataInput = JSON.stringify(dataInput);
			socket.emit('update phone detail opaap', dataInput, (data) => 
			{
			 
			});
		}


		socket.on('phone_detail_response',function(obj) 
		{	 
			console.log(obj);
		});


		socket.on('setapp_not_start_response',function(obj) 
		{	 
			console.log(obj);
			var factoryIdObj = obj.factoryId;
			var userId = obj.userId;
			var machineId = obj.machineId;
			var message = obj.message;
			var machineSetAppNotStart = $("#machineSetAppNotStart").val();
			var sessionUserId = "<?php echo $this->session->userdata('userId'); ?>";

			if (factoryId == factoryIdObj && userId == sessionUserId && machineSetAppNotStart == 1) 
			{
				//console.log(obj);
				$(".statusMessage"+machineId).css("color", "#FF8000") 
				$(".statusMessage"+machineId).addClass("m-t-10");
				$("#card-light"+machineId).show();
				$("#machineStatusDisconnect"+machineId).val("0");
				$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+',0)" style="width: 100px;margin-left: 35px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
				$(".statusMessage"+machineId).html('SetApp turned off. Check your phone or contact : info@nytt-tech.com');

				$.gritter.add({
					title: 'Error',
					text: message
				});
				$("#machineSetAppNotStart").val(0);
			}
		});

		socket.on('virtual_machine_log_response',function(obj) 
		{	 
			var IOName = obj['IOName'];
			var signal = obj['signal'];
			if (signal == "0") 
			{
				$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/no_production.png") ?>")
			}else
			{
				$("#detectionImageVM"+IOName).attr("src","<?php echo base_url("assets/overview_gifs/production.png") ?>")
			}
		});

		

		socket.on('change_machine_status_response',function(response) 
		{	 
			var machineStatus = response.machineStatus;
			var machineId = response.machineId;
			
			var isMachineNoStacklight = $("#isMachineNoStacklight"+machineId).val();
			console.log(isMachineNoStacklight);
			if(factoryId == response.factoryId) 
			{
				$(".statusMessage"+machineId).css("color", "#FF8000") 
				$(".statusMessage"+machineId).addClass("m-t-10");
				if (machineStatus == "0") 
				{ 
					$("#machineStatusDisconnect"+machineId).val("0");
					if (isMachineNoStacklight == "0") 
					{
						$("#card-light"+machineId).show();
						$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+','+machineStatus+')" style="width: 100px;margin-left: 35px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
						$(".statusMessage"+machineId).html(' SetApp turned off. Check your phone or contact : info@nytt-tech.com');
					}else
					{
						$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/production.png'); ?>">');
						$(".statusMessage"+machineId).html(' No setapp installation required as no stacklight is available.');
					}
				}else if (machineStatus == "1") 
				{
					$("#card-light"+machineId).hide();
					$(".statusMessage"+machineId).css("min-height", "35px") 
					$("#machineStatusDisconnect"+machineId).val("1");
					$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/setup.png'); ?>">');
					if (isMachineNoStacklight == "0") 
					{
						$(".statusMessage"+machineId).html("Machine is in setup state. You can't start setapp right now");
					}else
					{
						$(".statusMessage"+machineId).html(' No setapp installation required as no stacklight is available.');
					}
				}else
				{
					$("#card-light"+machineId).hide();
					$("#machineStatusDisconnect"+machineId).val("2");
					$(".detectionImage"+machineId).html('<img style="height: 82px;" src="<?php echo base_url('assets/overview_gifs/no_production.png'); ?>">');
					if (isMachineNoStacklight == "0") 
					{
						$(".statusMessage"+machineId).html("Machine is in no production state. You can't start setapp right now");
					}else
					{
						$(".statusMessage"+machineId).html(' No setapp installation required as no stacklight is available.');
					}
				}
			}
		});	

		socket.on('change_machine_stop_and_wait_notification_time_response',function(obj) 
		{	 
			machineId = $("#machineIdStop").val();
			analytics = $("#analytics").val();
			if(factoryId == obj.factoryId && machineId == obj.machineId) 
			{
				if (analytics == "stopAnalytics") 
				{
					$("#errorNotification").text((obj.receiveErrorNotification == "0") ? "None" : "After "+ obj.receiveErrorNotification +" min");
					$("#errorReasonNotification").text((obj.provideReasonForErrors == "0") ? "None" : "After "+ obj.provideReasonForErrors +" min");
				}else
				{
					$("#errorNotification").text((obj.receiveWaitingNotification == "0") ? "None" : "After "+ obj.receiveWaitingNotification +" min");
					$("#errorReasonNotification").text((obj.provideReasonForWaiting == "0") ? "None" : "After "+ obj.provideReasonForWaiting +" min");
				}
			}
		});	

		
		var iJK = 1;
		socket.on('change_setting_data_response',function(userId) 
		{	 
			$.ajax({
		        url: "<?php echo base_url("Operator/getOperatorDetailById") ?>",
		        type: "post",
		        data: {userId:userId} ,
		        success: function (response) 
		        { 
			        	var obj = $.parseJSON(response);
			        	$("#userNameText"+userId).text(obj.userName);
			        	$("#userImageText"+userId).attr("src","<?php echo base_url('assets/img/user/') ?>"+obj.userImage+'?'+iJK);

			        	$("#userName"+userId).val(obj.userName);
			        	$(".file-upload-image-edit"+userId).attr("src","<?php echo base_url('assets/img/user/') ?>"+obj.userImage+'?'+iJK);
			        	iJK++;
			       
		        },
		        error: function(jqXHR, textStatus, errorThrown) {
		           console.log(textStatus, errorThrown);
		        }
		    });
		});

		$('#liveDetectonImage').on('hidden.bs.modal', function () {
		   $("#machinePopupOpen").val(1)
		})

		socket.on('screen_share_update_response',function(response) 
		{	 
			var obj = $.parseJSON(response);
			var machineIdCheckIn = $("#machineIdCheckIn").val();
			var factoryIdObj = obj.factoryId;
			var machinePopupOpen = $("#machinePopupOpen").val();
			if (machineIdCheckIn == obj.machineId && machinePopupOpen == 1 && factoryId == factoryIdObj) 
			{
				var detectionName = obj.detectionName;
				detectionState = obj.stateVal;
				var currentTime = moment(parseInt(obj.currentTime)).format("DD/MM/YYYY hh:mm:ss");
				var compareFacotryId = obj.factoryId;
				var userId = "<?php echo $this->session->userdata('userId'); ?>";
				var sessionUserId = obj.userId;
				var binary = '';
			    var bytes = new Uint8Array( obj.imageBytes );
			    var len = bytes.byteLength;
			    for (var i = 0; i < len; i++) {
			        binary += String.fromCharCode( bytes[ i ] );
			    }
			    imageBytes = window.btoa( binary );
				document.getElementById("ItemPreview").src = "data:image/png;base64," + imageBytes;
				var randomNumber = $("#randomNumber").val();
				var compareRandomNumber = $("#compareRandomNumber").val();

				$("#detectionMachineId").val(obj.machineId);
				$("#imageName").val(imageBytes);
				$("#currentTime").text(currentTime);
				$("#detectionName").text(detectionName);
				$("#detectionState").text(detectionState);

				$("#timestamp").val(currentTime);
				$("#color").val(detectionName);
				$("#state").val(detectionState);
				
				if (sessionUserId == randomNumber) 
				{	
					$("#liveDetectonImage").modal();
					$("#machinePopupOpen").val(0)
				}
			}
		});	

			

		socket.on('Battery Update',function(factoryIdSocket, machineId, battery, chargeFlag) 
		{	
			 if(factoryId == factoryIdSocket) {
				 $("#battery_percentage"+machineId).attr('data-original-title', 'Battery percentage '+battery+'%'); 

				 $("#batteryLevel"+machineId).css("width",battery+"%");
				 $("#batteryLevelO"+machineId).css("width",battery+"%");
				 $("#textBatteryLevel"+machineId).text(battery+"%");
				if(battery >= 50) { 
					
					$("#textBatteryLevel"+machineId).css("color","#76BA1B"); 
					$("#batteryOuter"+machineId).css("border-color","#76BA1B"); 
					$("#batteryLevelO"+machineId).css("border-color","#76BA1B"); 
					$("#batteryLevelO"+machineId).css("background-color","#76BA1B"); 
					$("#batteryBump"+machineId).css("background-color","#76BA1B"); 
					$("#batteryLevel"+machineId).css("background-color","#ff8000"); 
				} else {
					$("#textBatteryLevel"+machineId).css("color","#F60100"); 
					$("#batteryOuter"+machineId).css("border-color","#F60100"); 
					$("#batteryLevelO"+machineId).css("border-color","#F60100"); 
					$("#batteryLevelO"+machineId).css("background-color","#F60100"); 
					$("#batteryBump"+machineId).css("background-color","#F60100"); 
					$("#batteryLevel"+machineId).css("background-color","#F60100");
				}
				 if(chargeFlag == '1') {
					 $("#chargeFlag"+machineId).removeClass('hide').addClass('show')
				 } else if(chargeFlag == '0') {
					 $("#chargeFlag"+machineId).removeClass('show').addClass('hide')
				 } 
			 }
		  });
		socket.on('Connect SetApp', function(id, factoryIdSocket, machineId) 
		{ 
			if(factoryId == factoryIdSocket) {
				
				if($("#cardContent"+machineId).hasClass('card-inactive') == true) {
					$("#cardContent"+machineId).removeClass('card-inactive');
				}
				$("#inactiveText"+machineId).html(''); 
			}
		});

		
		socket.on('Disconnect SetApp', function(reason, factoryIdSocket, machineId)
		{ 
			//console.log("factoryIdSocket"+factoryIdSocket+" machineId - "+machineId+",reason - "+reason);	
			if(factoryId == factoryIdSocket) {
				if($("#cardContent"+machineId).hasClass('card-inactive') == false) {
					$("#cardContent"+machineId).addClass('card-inactive');
				}
				var reasonText;
				if(reason == 'client namespace disconnect' || reason == 'transport close' || reason == 'transport error') { 
					reasonText = 'Setapp turned off.';
				} else if(reason == 'detection disconnect') {
					reasonText = 'Setapp is in home screen.';
				} else if(reason == 'ping timeout') {
					reasonText = 'No internet or phone turned off.';
				} 
				$("#inactiveText"+machineId).html(reasonText+' Check your phone or contact : info@nytt-tech.com'); 

				$("#show_colorRO"+machineId).removeClass("rounded");
				$("#show_colorRO"+machineId).css("border-radius", "13px"); 
				$("#show_colorRO"+machineId).css("border", "3px solid #F60100"); 
				$("#show_colorRO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorRO"+machineId).css("opacity", "!important"); 
				$("#show_colorRO"+machineId).css("box-shadow", "none"); 

				$("#show_colorYO"+machineId).removeClass("rounded");
				$("#show_colorYO"+machineId).css("border-radius", "13px"); 
				$("#show_colorYO"+machineId).css("border", "3px solid #FFCF00"); 
				$("#show_colorYO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorYO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorYO"+machineId).css("box-shadow", "none");

				$("#show_colorGO"+machineId).removeClass("rounded");
				$("#show_colorGO"+machineId).css("border-radius", "13px"); 
				$("#show_colorGO"+machineId).css("border", "3px solid #76BA1B"); 
				$("#show_colorGO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorGO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorGO"+machineId).css("box-shadow", "none"); 

				$("#show_colorWO"+machineId).removeClass("rounded");
				$("#show_colorWO"+machineId).css("border-radius", "13px"); 
				$("#show_colorWO"+machineId).css("border", "3px solid black"); 
				$("#show_colorWO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorWO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorWO"+machineId).css("box-shadow", "none"); 

				$("#show_colorBO"+machineId).removeClass("rounded");
				$("#show_colorBO"+machineId).css("border-radius", "13px"); 
				$("#show_colorBO"+machineId).css("border", "3px solid #124D8D"); 
				$("#show_colorBO"+machineId).css("background-color", "#FFFFFF");
				$("#show_colorBO"+machineId).css("opacity", "1 !important"); 
				$("#show_colorBO"+machineId).css("box-shadow", "none");

				$(".statusMessage"+machineId).css("color", "#FF8000") 
				$(".statusMessage"+machineId).addClass("m-t-10");
				var machineStatusDisconnect = $("#machineStatusDisconnect"+machineId).val();
				if (machineStatusDisconnect == "0") 
				{
					$(".statusMessage"+machineId).html(reasonText+' Check your phone or contact : info@nytt-tech.com');
					if(reason == 'client namespace disconnect' || reason == 'transport close' || reason == 'transport error' || reason == 'detection disconnect') 
					{ 
						$(".detectionImage"+machineId).html('<img id="playImage'+machineId+'" onclick="startSetApp('+machineId+',0)" style="width: 100px;margin-left: 35px;cursor: pointer;" src="<?php echo base_url('assets/overview_gifs/play.png'); ?>">');
					} else if(reason == 'ping timeout') 
					{
						$(".detectionImage"+machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	
					} 
				}
			}
		});
		
		
		var method = 'refresh_color'; 
		<?php if($this->uri->segment(2) == 'overview' || $this->uri->segment(2) == 'overview2' || $this->uri->segment(2) == 'overview3' || $this->uri->segment(2) == 'overview_development') { ?>
			method = 'beta_color_upadate';  
		<?php } ?>
		
		var refreshLog = 0;	
		var text = '<p class="card-text" ></p>';
		

		socket.on(method,function(data){  

			console.log(data);

			if(factoryId == data.factoryId ) { 
				refreshLog = 1; 

				var detectionMachineName = "'"+$("#detectionMachineName"+data.machineId).val().replace("'","+")+"'";
				$("#card-light"+data.machineId).show();
				if (data.stateVal == "Running") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/running.gif'); ?>">');
				}else if (data.stateVal == "Waiting") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/waiting.gif'); ?>">');
				}else if (data.stateVal == "Stopped") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/stop.png'); ?>">');
				}else if (data.stateVal == "Off") 
				{
					$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/off.png'); ?>">');
				}else if (data.stateVal == "nodet") 
				{

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 

					$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');	

					$(".statusMessage"+data.machineId).removeClass("m-t-10");
					$(".statusMessage"+data.machineId).html('No detection. Check your phone or contact : info@nytt-tech.com <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
				}
				
				if(data.color == 'NoData') {
					if($("#cardContent"+data.machineId).hasClass('card-inactive') == false) {
						$("#cardContent"+data.machineId).addClass('card-inactive');
					}
					var reasonText = 'No detection.';
					$("#inactiveText"+data.machineId).html(reasonText+' Check your phone or contact : info@nytt-tech.com'); 

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 

					$(".statusMessage"+data.machineId).css("color", "#FF8000");
					$(".statusMessage"+data.machineId).removeClass("m-t-10");
					$(".statusMessage"+data.machineId).html('No detection. Check your phone or contact : info@nytt-tech.com <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
					$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
				} else {
					if($("#cardContent"+data.machineId).hasClass('card-inactive') == true) {
						$("#cardContent"+data.machineId).removeClass('card-inactive');
					}
					$("#inactiveText"+data.machineId).html(''); 
				}
				if(data.redStatus == '1') {
					$("#show_colorRO"+data.machineId).addClass("rounded");
					$("#show_colorRO"+data.machineId).css("background-color", "#F60100");
					$("#show_colorRO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorRO"+data.machineId).css("box-shadow", "none");
					$("#show_colorR"+data.machineId).css("opacity", "1");
				} else 
				{
					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorR"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.yellowStatus == '1') {
					refreshLog = 0;
					$("#show_colorYO"+data.machineId).addClass("rounded");
					$("#show_colorYO"+data.machineId).css("background-color", "#FFCF00");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");
					$("#show_colorY"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorY"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.greenStatus == '1') 
				{
					$("#show_colorGO"+data.machineId).addClass("rounded");
					$("#show_colorGO"+data.machineId).css("background-color", "#76BA1B");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorGO"+data.machineId).css("box-shadow", "none");
					$("#show_colorG"+data.machineId).css("opacity", "1"); 
					
				} else {
					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorG"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.whiteStatus == '1') 
				{
					$("#show_colorWO"+data.machineId).addClass("rounded");
					$("#show_colorWO"+data.machineId).css("background-color", "#fff");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorWO"+data.machineId).css("box-shadow", "none");
					$("#show_colorW"+data.machineId).css("opacity", "1"); 
					
				} else 
				{
					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorW"+data.machineId).css("opacity", "0.4"); 
				}
				
				if(data.blueStatus == '1') 
				{
					$("#show_colorBO"+data.machineId).addClass("rounded");
					$("#show_colorBO"+data.machineId).css("background-color", "#124D8D");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important");
					$("#show_colorBO"+data.machineId).css("box-shadow", "none");
					$("#show_colorB"+data.machineId).css("opacity", "1"); 
					
				} else 
				{
					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 
					$("#show_colorB"+data.machineId).css("opacity", "0.4"); 
				}

				if(data.redStatus == '0' && data.yellowStatus == '0' && data.greenStatus == '0' && data.whiteStatus == '0' && data.blueStatus == '0' ) 
				{
					$("#show_colorR"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorY"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorG"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorW"+data.machineId).css("opacity", "0.4"); 
					$("#show_colorB"+data.machineId).css("opacity", "0.4"); 

					$("#show_colorRO"+data.machineId).removeClass("rounded");
					$("#show_colorRO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorRO"+data.machineId).css("border", "3px solid #F60100"); 
					$("#show_colorRO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorRO"+data.machineId).css("opacity", "!important"); 
					$("#show_colorRO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorYO"+data.machineId).removeClass("rounded");
					$("#show_colorYO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorYO"+data.machineId).css("border", "3px solid #FFCF00"); 
					$("#show_colorYO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorYO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorYO"+data.machineId).css("box-shadow", "none");

					$("#show_colorGO"+data.machineId).removeClass("rounded");
					$("#show_colorGO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorGO"+data.machineId).css("border", "3px solid #76BA1B"); 
					$("#show_colorGO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorGO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorGO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorWO"+data.machineId).removeClass("rounded");
					$("#show_colorWO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorWO"+data.machineId).css("border", "3px solid black"); 
					$("#show_colorWO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorWO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorWO"+data.machineId).css("box-shadow", "none"); 

					$("#show_colorBO"+data.machineId).removeClass("rounded");
					$("#show_colorBO"+data.machineId).css("border-radius", "13px"); 
					$("#show_colorBO"+data.machineId).css("border", "3px solid #124D8D"); 
					$("#show_colorBO"+data.machineId).css("background-color", "#FFFFFF");
					$("#show_colorBO"+data.machineId).css("opacity", "1 !important"); 
					$("#show_colorBO"+data.machineId).css("box-shadow", "none"); 
					$(".statusMessage"+data.machineId).css("color", "#FF8000");

					if (data.stateVal == "Off" && data.color != 'NoData') 
					{
						$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/off.png'); ?>">');

						$(".statusMessage"+data.machineId).css("color", "#FF8000") 
						$(".statusMessage"+data.machineId).addClass("m-t-10");
						$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
					}else
					{
						if (data.stateVal == "Running" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/running.gif'); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
						}else if (data.stateVal == "Waiting" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/waiting.gif'); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');

						}else if (data.stateVal == "Stopped" && data.color == "off") 
						{
							$(".detectionImage"+data.machineId).html('<img style="width: 150px;" src="<?php echo base_url('assets/overview_gifs/stop.png'); ?>">');
							$(".statusMessage"+data.machineId).css("color", "#FF8000") 
							$(".statusMessage"+data.machineId).addClass("m-t-10");
							$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');

						}else
						{
							$(".statusMessage"+data.machineId).removeClass("m-t-10");
							$(".statusMessage"+data.machineId).html('No detection. Check your phone or contact : info@nytt-tech.com <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
							$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
						}
					}
						
				} 
				else
				{	
					if (data.stateVal == "nodet") 
					{
						$(".statusMessage"+data.machineId).removeClass("m-t-10");
						$(".statusMessage"+data.machineId).html('No detection. Check your phone or contact : info@nytt-tech.com <br> <a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
						$(".detectionImage"+data.machineId).html('<img style="width: 77px;margin-left: 40px;" src="<?php echo base_url('assets/overview_gifs/no_detection.png'); ?>">');
					}else
					{
						$(".statusMessage"+data.machineId).css("color", "#FF8000") 
						$(".statusMessage"+data.machineId).addClass("m-t-10");
						$(".statusMessage"+data.machineId).html('<a onclick="checkLiveStatus('+data.machineId+','+detectionMachineName+')" style="width: auto;margin-left: 20px;" href="javascript:void(0);" class="btn btn-primary" data-toggle="tooltip" data-title="" data-original-title="" title="">Check live status</a>');
					}
				}
				
				text = '<p class="card-text" >'+data.stateVal+'</p>'; 

				$("#liveDetectionState"+data.machineId).val(data.stateVal);
				
				$("#notifyText"+data.machineId).html(text);
				
				$(".graphBlockOuter"+data.machineId).first().remove();  
				$("#card-text-graph"+data.machineId).append("<div class='graphBlockOuter graphBlockOuter"+data.machineId+"' ><span class='graphBlock' style='background-color:#"+data.color1+"' ></span><span class='graphBlock' style='background-color:#"+data.color2+"' ></span></div>")
			} 
        });

	

	

		socket.on('Disconnect SetAppAgv', function(reason, factoryIdSocket, machineId){ 
			
			if(factoryId == factoryIdSocket) {
				
				$("#AgvColor"+machineId).removeClass("NoAgvFind");
				$("#AgvColor"+machineId).removeClass("AgvFind");
				$("#AgvColor"+machineId).addClass("NoAgvDetection");

				var reasonText;
				if(reason == 'client namespace disconnect' || reason == 'transport close') { 
					reasonText = 'SetApp turned off.';
				} else if(reason == 'ping timeout') {
					reasonText = 'No internet or phone turned off.';
				} 

				/*if (factoryId == 6) 
				{

					$.ajax({
				        url: "<?php echo base_url("admin/update_overview_data") ?>",
				        type: "post",
				        data: {machineId:machineId} ,
				        success: function (response) {
				        	

				        },
				        error: function(jqXHR, textStatus, errorThrown) {
				           console.log(textStatus, errorThrown);
				        }
				    });
				}*/

				$("#agv"+machineId).html("<p style='color: black;font-size: 18px;'>"+reasonText+" Check your phone or contact : info@nytt-tech.com</p>"); 

			}
		});

        socket.on("agv_color_upadate",function(data){  
				
			if(factoryId == data.factoryId ) { 
				refreshLog = 1; 
				var color = data.color;
				var phoneId = data.machineId;
				var lable = data.lable;

				if (color == "NoData") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvFind");
					$("#AgvColor"+phoneId).removeClass("AgvFind");
					$("#AgvColor"+phoneId).addClass("NoAgvDetection");
					$("#agv"+phoneId).html("<p style='color: black;font-size: 18px;'>No detection. Please check your phone or contact: info@nytt-tech.com</p>");
				}

				if (color == "NoAGV") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvDetection");
					$("#AgvColor"+phoneId).removeClass("AgvFind");
					$("#AgvColor"+phoneId).addClass("NoAgvFind");
					$("#agv"+phoneId).html("<h2 style='color: white'>No AGVs</h2>");
				}

				if (color != "NoData" && color != "NoAGV") 
				{
					$("#AgvColor"+phoneId).removeClass("NoAgvDetection");
					$("#AgvColor"+phoneId).removeClass("NoAgvFind");
					$("#AgvColor"+phoneId).addClass("AgvFind");
					$("#agv"+phoneId).html("<h2 style='color: white'>"+lable+"</h2>");
				}
			
			} 
        });
		
		
		
		$(document).ready(function() {
			
			
			
			
			var minimum = 2; 
			
			$('form#add_machine_form').submit(function(e) {
				
				e.preventDefault();
				var submit = 0;
				var running = $.trim($('#running').val());
				var waiting = $.trim($('#waiting').val());
				var stopped = $.trim($('#stopped').val());
				var off = $.trim($('#off').val());

				if($("#noStacklight").prop('checked') == true)
				{
				    var noStacklight = true;
				}else
				{
					var noStacklight = false;
				}


				if ((running  === '' || waiting  === '' || stopped  === '' || off  === '') && noStacklight == false) {
				 	var emptyArr = []; 
				 	if(running  === '') { 
				 		emptyArr.push('running') 
				 	}
				 	if(waiting  === '') { 
				 		emptyArr.push('waiting')
				 	}
				 	if(stopped  === '') { 
				 		emptyArr.push('stopped')
				 	}
				 	if(off  === '') { 
				 		emptyArr.push('off') 
				 	}
					emptyArr[0] = emptyArr[0].charAt(0).toUpperCase() + emptyArr[0].slice(1); 
					
				 	var emptyText = emptyArr.join();
				 	
				 	if(emptyText.length > 0 ) { 
					 	if(emptyArr.length == 1) {
					 		emptyText += ' is ';
					 	} else {
							emptyText += ' are ';
					 	}
					 	emptyText += 'not assigned, are you sure you want to continue?';
					} 
					if(confirm(emptyText)){
						submit = 1;
					}
					
				} else {
					submit = 1;
				}
				
				if(submit == 1) {
 					var form = $(this);
						$("#add_machine_submit").html('Saving...')
						var machineLightNewArr = [];
						$('#colorLightSelect [id^="data_image_remove_"]').each(function(){
							var getId = $(this).attr('id').replace('data_image_remove_', '')
							machineLightNewArr.push(getId); 
						});
						var machineLightNew = machineLightNewArr.join();
						
						var inputData = form.MytoJson();
						inputData['machineLightNew'] = machineLightNew; 
						
						$.ajax({
							type: "POST",
							url: "<?php echo site_url('admin/add_machine'); ?>",
							data: inputData,  
							dataType: "html",
							success: function(data){
								  
									if($.trim(data) == "1") {  
										$.gritter.add({
											title: 'Success',
											text: 'Machine added successfully'
										});
										
										form.each(function(){
											this.reset();
										}); 
										$("#add_machine_submit").html('Saved...')
										
										$("#modal-add-machine").modal('hide').fadeOut(1500); 
										$("#add_machine_submit").html('Save')
										window.setTimeout(function(){location.reload()},3000) 
										
									} else { 
										
										$.gritter.add({
											title: 'Error',
											text: data
										});
										$("#add_machine_submit").html('Save')
									}
								
								
							},
							error: function() { 
								alert("Error while adding machine.");
								$.gritter.add({
									title: 'Error',
									text: 'Error while adding machine.'
								});
								 
							}
					   });
				}
			});

			$("form[id^='update_stacklight_configuration']").submit(function(e) {
			
				var form = $(this);
				e.preventDefault();
				var submit = 0;
				var machineId = form.find('input[name="machineId"]').val();
				var running = $.trim($('#running'+machineId).val());
				var waiting = $.trim($('#waiting'+machineId).val());
				var stopped = $.trim($('#stopped'+machineId).val());
				var off = $.trim($('#off'+machineId).val());
				if (running  === '' || waiting  === '' || stopped  === '' || off  === '') {
				 	var emptyArr = []; 
				 	if(running  === '') { 
				 		emptyArr.push('running') 
				 	}
				 	if(waiting  === '') { 
				 		emptyArr.push('waiting')
				 	}
				 	if(stopped  === '') { 
				 		emptyArr.push('stopped')
				 	}
				 	if(off  === '') { 
				 		emptyArr.push('off') 
				 	}
					emptyArr[0] = emptyArr[0].charAt(0).toUpperCase() + emptyArr[0].slice(1); 
					
				 	var emptyText = emptyArr.join();
				 	
				 	if(emptyText.length > 0 ) { 
					 	if(emptyArr.length == 1) {
					 		emptyText += ' is ';
					 	} else {
							emptyText += ' are ';
					 	}
					 	emptyText += 'not assigned, are you sure you want to continue?';
					} 
					if(confirm(emptyText)){
						submit = 1;
					}
				} else {
					submit = 1;
				}
				if(submit == 1) { 
					$(".add_stacklight_configuration_submit").html('Saving...')
					
					$.ajax({
						type: "POST",
						url: "<?php echo site_url('admin/update_stacklight_configuration'); ?>",
						data: form.MytoJson(),  
						dataType: "html",
						success: function(data){
							
								if($.trim(data) == "1") {  
									$.gritter.add({
										title: 'Success',
										text: 'Stacklight configuration updated successfully'
									});
									
									$(".add_stacklight_configuration_submit").html('Saved...')
								
									location.reload(1000);
									
								} else { 
									
									$.gritter.add({
										title: 'Error',
										text: data
									});
									$(".add_stacklight_configuration_submit").html('Save')
								}
							
							
						},
						error: function() { 
							$.gritter.add({
								title: 'Error',
								text: 'Error while updating stacklight configuration.'
							});
						}
				   });
				}
				
			});
			
			$("form[id^='edit_machine_form']").submit(function(e) {  
				
				var form = $(this);
				e.preventDefault();
				var formId = form.attr('id');
				var machineId = formId.replace('edit_machine_form',''); 
				
				$("[id^='edit_machine_submit"+machineId+"']").html('Saving...')
				
				var err_div = "[id^='edit_machine_error"+machineId+"']";
				var suc_div = "[id^='edit_machine_success"+machineId+"']";
				var modal_div = "[id^='modal-edit-machine"+machineId+"']";
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/edit_machine'); ?>", 
					data: form.serialize(), 
					dataType: "html",
					success: function(data){
						
							if($.trim(data) == "Machine updated successfully") { 
								$.gritter.add({
									title: 'Success',
									text: 'Machine updated successfully'
								});
								$("[id^='edit_machine_submit"+machineId+"']").html('Saved...')
								
								$(modal_div).modal('hide').fadeOut(1500);
								$("[id^='edit_machine_submit"+machineId+"']").html('Save')
								window.setTimeout(function(){location.reload()},3000)
							} else { 
								$.gritter.add({
									title: 'Error',
									text: data
								});
								$("[id^='edit_machine_submit"+machineId+"']").html('Save')
							}
						
						
					},
					error: function() { 
						$.gritter.add({
							title: 'Error',
							text: 'Error while updating machine.'
						});
						location.reload();
					}
			   });
			
			});
			
			$("form[id^='delete_machine_form']").submit(function(e) {  
				
				var form = $(this);
				e.preventDefault();
				var formId = form.attr('id');
				var machineId = formId.replace('delete_machine_form',''); 
				
				$("[id^='delete_machine_submit"+machineId+"']").html('Deleting...')
				
				var suc_div = "[id^='delete_machine_success"+machineId+"']";
				var modal_div = "[id^='modal-delete-machine"+machineId+"']";
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/delete_machine'); ?>", 
					data: form.serialize(), 
					dataType: "html",
					success: function(data){
						
							if($.trim(data) == "Machine deleted successfully") { 
							    $.gritter.add({
									title: 'Success',
									text: 'Machine deleted successfully'
								});
								$("[id^='delete_machine_submit"+machineId+"']").html('Deleted...')
								 
								$(modal_div).modal('hide').fadeOut(1500);
								$("[id^='delete_machine_submit"+machineId+"']").html('Delete')
								window.setTimeout(function(){location.reload()},3000)
							} 
						
					},
					error: function() { 
						$.gritter.add({
							title: 'Error',
							text: 'Error while deleting machine.'
						});
						location.reload();
					}
			   });
			});
			
			$("form[id^='unlock_machine_form']").submit(function(e) {  
				
				var form = $(this);
				e.preventDefault();
				var formId = form.attr('id');
				var machineId = formId.replace('unlock_machine_form',''); 
				
				var modal_div = "[id^='modal-unlock-machine"+machineId+"']";
				
				$("[id^='unlock_machine_submit"+machineId+"']").html('Unlocking...')
				
				$.ajax({
					type: "POST",
					url: "<?php echo site_url('admin/unlock_machine'); ?>", 
					data: form.serialize(), 
					dataType: "html",
					success: function(data){
						
							$.gritter.add({
								title: 'Success',
								text: 'Machine unlocked successfully.'
							});
							$("[id^='unlock_machine_submit"+machineId+"']").html('Unlocked...')
							
							$(modal_div).modal('hide').fadeOut(1500);
							$("[id^='unlock_machine_submit"+machineId+"']").html('Unlock')
							window.setTimeout(function(){location.reload()},3000)
						
					},
					error: function() { 
						alert("Error while unlocking machine.");
						location.reload();
					}
			   });
			});
        
			
			$("#machineAutomation").change(function(){
				var selVal = $( "#machineAutomation option:selected" ).text();
				if ( selVal == "Automatic" || selVal == "Both" ) {
					$("#machineLoading").prop('required',true);
				} else {
					$("#machineLoading").prop('required',false);
				}
			}) 
			
			
			$("[id^='machineAutomationEdit']").change(function(){
				var machine_id = $(this).attr('id').replace('machineAutomationEdit',''); 
				var selVal = $( '#machineAutomationEdit'+machine_id+' option:selected' ).text(); 
				if ( selVal == "Automatic" || selVal == "Both" ) { 
					$("#machineLoading"+machine_id).prop('required',true);
					} else {
					$("#machineLoading"+machine_id).prop('required',false);
					}
			}) 
			
			$("select.machineStateValue, select[name^='machineMTId'], select[name^='machineType'], select[name^='machineAutomation'], select[name^='machineLoading'], select[name^='repeat'], select[name^='machineId'], select[name^='weekStartDate'], select[name^='monthStartDate'], select[name^='yearStartDate'], select[name^='weekEndDate'], select[name^='yearEndDate']").select2({ 
			  tags: false
			});

			$(".select2Select").select2({ 
			  tags: false
			});


			$("select.machineStateValue, select[name^='machineMTId']").on("select2:select", function (evt) {
			  var element = evt.params.data.element;
			  var $element = $(element);
			  
			  $element.detach();
			  $(this).append($element);
			  $(this).trigger("change");
			}); 
		
		$( "select[name^='machineLight']").imagepicker({
		  hide_select : true,
		  show_label  : false 
		}); 
		
		$( "select[name^='stackLightTypeId']").imagepicker({
		  hide_select : false,
		  show_label  : false 
		}); 
		
		}); 
		
</script>


	<script type="text/javascript">
		
        function startSetApp(machineId,machineStatus)
        {
        	if (machineStatus == "0") 
        	{
        		$("#machineSetAppNotStart").val(1);
        		$("#playImage"+machineId).attr("onclick","");
        		$("#playImage"+machineId).attr("src","<?php echo base_url('assets/overview_gifs/loader_gif.gif'); ?>");
	        	$.ajax({
			        url: "<?php echo base_url("admin/setAppStart") ?>",
			        type: "post",
			        data: {machineId:machineId} ,
			        success: function (response) 
			        {
			        	//	console.log(response);
				        	var obj = $.parseJSON(response);
				        	if (obj.status == 1) 
				        	{
				        		$("#playImage"+machineId).css("width","110px");
				        		$("#playImage"+machineId).css("margin-left","20px");
				        		$(".statusMessage"+machineId).html(obj.message);
				        		
								$.gritter.add({
									title: 'Success',
									text: obj.message
								});	
				        	}
				        	else
				        	{
				        		$("#playImage"+machineId).attr("onclick","startSetApp("+machineId+",0)");
				        		$("#playImage"+machineId).attr("src","<?php echo base_url('assets/overview_gifs/play.png'); ?>");
				        		$.gritter.add({
									title: 'Error',
									text: obj.message
								});
				        	}
				       
			        },
			        error: function(jqXHR, textStatus, errorThrown) {
			           console.log(textStatus, errorThrown);
			        }
			    });
        	}else
        	{
        		if (machineStatus == "2") 
        		{
        			$.gritter.add({
						title: 'Error',
						text: "Machine is in no producation state. You can't start setapp right now"
					});	
        		}else
        		{
        			$.gritter.add({
						title: 'Error',
						text: "Machine is in Setup  state. You can't start setapp right now"
					});
        		}
        		
        	}
        	
        }

        $('form#dashboardCaptureCamera_form').submit(function(e) {  
            var form = $(this);
			e.preventDefault();
			$("#dashboardCaptureCamera_submit").html('Report wrong detection...'); 
			$.ajax({
				type: "POST",
				url: "<?php echo site_url('admin/dashboardCaptureCamera'); ?>",
				data: form.MytoJson(),  
				dataType: "html",
				success: function(data){
				
						if($.trim(data) == "1") {  
							$("#dashboardCaptureCamera_submit").html('Report wrong detection')
							$.gritter.add({
								title: 'Success',
								text: "Detection issue mail send successfully."
							});
							$("#liveDetectonImage").modal('toggle');;
						} 
						else 
						{ 
							$.gritter.add({
								title: 'Error',
								text: data
							});
							$("#dashboardCaptureCamera_submit").html('Report wrong detection')
						}
					
				},
				error: function() { 
					$.gritter.add({
							title: 'Error',
							text: 'Error while saving configuration..'
						});
					$("#dashboardCaptureCamera_submit").html('Report wrong detection')
				}
		   });
        }); 


	</script>
