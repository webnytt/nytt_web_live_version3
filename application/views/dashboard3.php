<link href="<?php echo base_url('assets/css/dashboard3.css');?>" rel="stylesheet" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js"></script>
<script src="<?php echo base_url(); ?>assets/plugins/echarts/echarts.min.js"></script>  

<style>
    .administratorImageStyle
    {
        width: 30px!important;margin: -5px 10px -5px 0!important;border-radius: 30px!important;height: 30px!important;
    }
    .AnalyticsHeadingStyle
    {
        font-size: 22px!important;color: #002060!important;
    }
    .StopAnalyticsButtonStyle
    {
        background: #F60100!important;padding-left: 18px !important;padding-right: 18px !important;margin-left: 10px!important;
    }
    .Mainspanstyle
    {
        float: right;font-weight: 500;
    }
    .colmd1Showbystyle
    {
        width:100%!important;flex: 0 0 5% !important;padding-left:0px!important;padding-right: 0px !important;padding-top: 5px!important;
    }
    .changeFilterstyle
    {
        padding-left: 5px!important;padding-right: 0px!important;
    }
    .ForStyle
    {
        padding-left:8px!important;padding-right: 5px!important;
    }
    .colmd2datStyle
    {
        width:100%!important;padding-left: 5px!important;padding-right: 12px!important;
    }

    </style>

<div id="content" class="content">
    
    <h1 class="page-header AnalyticsHeadingStyle">Analytics</h1>
    <br />
    <div class="row" style="margin-top: -23px;">
        <div class="col-md-3" style="padding-left: 0px;">
            <a href="<?php echo base_url('admin/breakdown3'); ?>"class="btn btn-primary StopAnalyticsButtonStyle">Stop analytics</a>
        </div>
        <div class="col-md-1 show_by colmd1Showbystyle">
            <label>Show by :</label>
        </div>
        <div class="col-md-2 changeFilterstyle">
            <select  class="form-control" id="choose1" onchange="changeFilterType(this.value);">
                <option class="new_test" value="day">Day</option>
                <option class="new_test" value="weekly">Week</option>
                <option class="new_test" value="monthly"><span> Month </span></option>
                <option class="new_test" value="yearly"><span> Year </span></option>
            </select>
        </div>
        <div class="col-md-1 day show_by">
            <label class="ForStyle"> For:&nbsp;</label>
        </div> 
        <div class="col-md-2 day colmd2datStyle">
            <input onchange="changeFilterValue()" type="text" class="form-control datepicker" id="choose2" style="width:100%;" value="<?php echo date('m/d/Y'); ?>">
        </div>
        <div class="col-md-2 weekly" style="display: none;">
            <select class="form-control" id="choose3" onchange="changeFilterValue()">
                <option <?php if(date('W') == "1") { echo "selected"; } ?> value="1">Week 1</option>
                <option <?php if(date('W') == "2") { echo "selected"; } ?> value="2">Week 2</option>
                <option <?php if(date('W') == "3") { echo "selected"; } ?> value="3">Week 3</option>
                <option <?php if(date('W') == "4") { echo "selected"; } ?> value="4">Week 4</option>
                <option <?php if(date('W') == "5") { echo "selected"; } ?> value="5">Week 5</option>
                <option <?php if(date('W') == "6") { echo "selected"; } ?> value="6">Week 6</option>
                <option <?php if(date('W') == "7") { echo "selected"; } ?> value="7">Week 7</option>
                <option <?php if(date('W') == "8") { echo "selected"; } ?> value="8">Week 8</option>
                <option <?php if(date('W') == "9") { echo "selected"; } ?> value="9">Week 9</option>
                <option <?php if(date('W') == "10") { echo "selected"; } ?> value="10">Week 10</option>
                <option <?php if(date('W') == "11") { echo "selected"; } ?> value="11">Week 11</option>
                <option <?php if(date('W') == "12") { echo "selected"; } ?> value="12">Week 12</option>
                <option <?php if(date('W') == "13") { echo "selected"; } ?> value="13">Week 13</option>
                <option <?php if(date('W') == "14") { echo "selected"; } ?> value="14">Week 14</option>
                <option <?php if(date('W') == "15") { echo "selected"; } ?> value="15">Week 15</option>
                <option <?php if(date('W') == "16") { echo "selected"; } ?> value="16">Week 16</option>
                <option <?php if(date('W') == "17") { echo "selected"; } ?> value="17">Week 17</option>
                <option <?php if(date('W') == "18") { echo "selected"; } ?> value="18">Week 18</option>
                <option <?php if(date('W') == "19") { echo "selected"; } ?> value="19">Week 19</option>
                <option <?php if(date('W') == "20") { echo "selected"; } ?> value="20">Week 20</option>
                <option <?php if(date('W') == "21") { echo "selected"; } ?> value="21">Week 21</option>
                <option <?php if(date('W') == "22") { echo "selected"; } ?> value="22">Week 22</option>
                <option <?php if(date('W') == "23") { echo "selected"; } ?> value="23">Week 23</option>
                <option <?php if(date('W') == "24") { echo "selected"; } ?> value="24">Week 24</option>
                <option <?php if(date('W') == "25") { echo "selected"; } ?> value="25">Week 25</option>
                <option <?php if(date('W') == "26") { echo "selected"; } ?> value="26">Week 26</option>
                <option <?php if(date('W') == "27") { echo "selected"; } ?> value="27">Week 27</option>
                <option <?php if(date('W') == "28") { echo "selected"; } ?> value="28">Week 28</option>
                <option <?php if(date('W') == "29") { echo "selected"; } ?> value="29">Week 29</option>
                <option <?php if(date('W') == "30") { echo "selected"; } ?> value="30">Week 30</option>
                <option <?php if(date('W') == "31") { echo "selected"; } ?> value="31">Week 31</option>
                <option <?php if(date('W') == "32") { echo "selected"; } ?> value="32">Week 32</option>
                <option <?php if(date('W') == "33") { echo "selected"; } ?> value="33">Week 33</option>
                <option <?php if(date('W') == "34") { echo "selected"; } ?> value="34">Week 34</option>
                <option <?php if(date('W') == "35") { echo "selected"; } ?> value="35">Week 35</option>
                <option <?php if(date('W') == "36") { echo "selected"; } ?> value="36">Week 36</option>
                <option <?php if(date('W') == "37") { echo "selected"; } ?> value="37">Week 37</option>
                <option <?php if(date('W') == "38") { echo "selected"; } ?> value="38">Week 38</option>
                <option <?php if(date('W') == "39") { echo "selected"; } ?> value="39">Week 39</option>
                <option <?php if(date('W') == "40") { echo "selected"; } ?> value="40">Week 40</option>
                <option <?php if(date('W') == "41") { echo "selected"; } ?> value="41">Week 41</option>
                <option <?php if(date('W') == "42") { echo "selected"; } ?> value="42">Week 42</option>
                <option <?php if(date('W') == "43") { echo "selected"; } ?> value="43">Week 43</option>
                <option <?php if(date('W') == "44") { echo "selected"; } ?> value="44">Week 44</option>
                <option <?php if(date('W') == "45") { echo "selected"; } ?> value="45">Week 45</option>
                <option <?php if(date('W') == "46") { echo "selected"; } ?> value="46">Week 46</option>
                <option <?php if(date('W') == "47") { echo "selected"; } ?> value="47">Week 47</option>
                <option <?php if(date('W') == "48") { echo "selected"; } ?> value="48">Week 48</option>
                <option <?php if(date('W') == "49") { echo "selected"; } ?> value="49">Week 49</option>
                <option <?php if(date('W') == "50") { echo "selected"; } ?> value="50">Week 50</option>
                <option <?php if(date('W') == "51") { echo "selected"; } ?> value="51">Week 51</option>
                <option <?php if(date('W') == "52") { echo "selected"; } ?> value="52">Week 52</option>
            </select>
        </div>


<style>
    .monthlyShowByStyle
    {
        max-width: 2% !important;flex: 0 0 5% !important;padding-left: 0px !important;padding-right: 0px !important;display: none;
    }
    .colmd2monthlyStyle
    {
        max-width: 23% !important;flex: 0 0 23% !important;padding-left: 3px!important;padding-right: 10px!important;display: none;
    }
</style>
        <div class="col-md-1 weekly" style="display: none;">
        </div>
        <input type="hidden" id="choose6" name="" value="<?php echo date('Y'); ?>">
        <div class="col-md-1 monthly show_by monthlyShowByStyle">
            <label> for </label>
        </div>
        <div class="col-md-2 monthly colmd2monthlyStyle">
            <input onchange="changeFilterValue()" type="text" class="form-control datepicker1" id="choose4">
        </div>
        <?php $years = date('Y',strtotime("+1 year")) - 2018;  ?>
        <div class="col-md-2 yearly" style="display: none;">
            <select class="form-control"  id="choose5" onchange="changeFilterValue()">
                <?php for($i=0 ; $i < $years; $i++){ 
                    $yearValue = 2018 + $i;
                    ?>
                    <option <?php if ($yearValue == date('Y')) {
                        echo "selected";
                    } ?> value="<?php echo $yearValue; ?>"><?php echo $yearValue; ?></option>
                <?php } ?>
            </select>
        </div>
        <div class="col-md-1 yearly" style="display: none;"></div>
    </div>
   <!--  <div class="row" style="margin-top: 13px;">
        <div class="col-md-12" style="padding-left: 6px !important;padding-right: 2px!important;">
                
                <div id="carousel-example" class="carousel slide carousel-multi-item" data-ride="carousel" data-interval="false">
                <div class="container carousel-inner  no-padding" role="listbox">

                    <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3 active showAll" style="padding-left: 0px !important;">
                            <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> Show all  </a>
                        </div>
                      <?php 
                    foreach($listMachines->result() as $machine) { ?>
                        <?php if($machine->machineId == $_POST['machineId']){  ?>
                        <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3" style="padding-left: 0px !important;">
                            <a href="javascript:;" class="btn btn-default btn-lg filter active machineNameText" id="machine<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                        </div>
                    <?php } ?>
                    <?php } ?>
                    <?php 
                    $i = 1;
                    foreach($listMachines->result() as $machine) { ?>
                        <?php if($machine->machineId != $_POST['machineId']){ ?>
                        <div class="carousel-item col-12 col-sm-6 col-md-4 col-lg-3" style="padding-left: 0px !important;">
                            <a href="javascript:;" class="btn btn-default btn-lg filter machineNameText" id="machine<?php echo $machine->machineId; ?>"> <?php echo $machine->machineName; ?> </a>
                        </div>
                    <?php }  ?>
                    <?php $i++; } ?>
                </div>
                <?php $count = count($listMachines->result());
                if ($count > 3) { ?>
                <a class="carousel-control-prev" href="#carousel-example" role="button" data-slide="prev">
                    <span style="background-color: #FFFFFF !important;color: #FF8000 !important;" class="carousel-control-prev-icon"><i style="font-size: 20px;" class="fa fa-angle-left" aria-hidden="true"></i></span>
                </a>
                <a class="carousel-control-next" href="#carousel-example" role="button" data-slide="next">
                    <span style="background-color: #FFFFFF !important;color: #FF8000 !important;" class="carousel-control-next-icon"><i style="font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i></span>
                </a>
                <?php } ?>
            </div>
        </div> 
    </div> -->

    <div class="row" style="margin-top: 13px;">
        <input type="hidden" name="" id="machineId" value="<?php if(!empty($_POST['machineId'])){ echo $_POST['machineId']; }else{ echo "0"; } ?>">
        <div id="demo" class="carousel slide" data-ride="carousel" style="width: 100%;" data-interval="false">

          <div class="carousel-inner no-padding">
            <?php 
                $countQuery = 1;
                $f = 1;
                $machineNameList = $listMachines->result_array();
                $searchKey = array_search($_POST['machineId'], array_column($machineNameList, 'machineId'));
                $searchKeyCompar = ceil(($searchKey - 2) / 4);

            ?>
            <div class="carousel-item <?php if(!$_POST || $searchKeyCompar == 0) { echo "active";} ?>">
                <div class="col-xs-3 col-sm-3 col-md-3 machineDegine slicksliderstyle">
                    <a href="javascript:;" class="btn btn-default btn-lg filter boxShadow <?php if(!$_POST) { echo "active";} ?>" id="machine0"> Show all  </a>
                </div>
        <?php 
        
            foreach($machineNameList as $key => $machine) { ?>
              <?php if($countQuery % 4 == 0){ 
                                       ?>
                </div>
                <div class="carousel-item <?php if($f == $searchKeyCompar){ echo "active"; } ?>">
              <?php $f++;  } ?>
              <div class="col-xs-3 col-sm-3 col-md-3 machineDegine slicksliderstyle">
                <a href="javascript:;" class="btn btn-default btn-lg filter machineNameText <?php if($machine['machineId'] == $_POST['machineId']){  echo "active"; }?>" id="machine<?php echo $machine['machineId']; ?>"> <?php echo $machine['machineName']; ?> </a>
              </div>    
        <?php $countQuery++; } ?>
            </div>
          </div>
          
          <?php if ($count > 3) { ?>
              <a class="carousel-control-prev" href="#demo" data-slide="prev" style="background-color: #FFFFFF !important;color: #FF8000 !important;">
                <i style="font-size: 20px;" class="fa fa-angle-left" aria-hidden="true"></i>
              </a>
              <a class="carousel-control-next" href="#demo" data-slide="next" style="background-color: #FFFFFF !important;color: #FF8000 !important;">
                <i style="font-size: 20px;" class="fa fa-angle-right" aria-hidden="true"></i>
              </a>
          <?php } ?>
        </div>
    </div>
    <style>
        
    </style>

    <div class="row" id="chartwrap" style="margin-top: 13px;display: flex;flex-wrap: wrap;justify-content:space-between;margin-left: -20px;">
        <div class="col-md-5" style="max-height: 390px !important;">
            <div class="tab-content boxShadow" style="min-height: 335px!important;" >
                <div class="tab-pane fade active show">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-md-12" >
                            <h4 style="color: #002060;">Machine mode</h4>
                            <center style="margin-bottom: -30px;margin-top: -24px;">
                                <div id="DailyGraph3" class="DailyGraphs3" style="padding-right:20px;height:330px;"></div>
                            </center>
                        </div>
                        <div id="DailyGraph3 " class="col-md-12 DailyGraphs3" style="height:273px;display:none;" >
                            <h4 style="color: #002060;">Actual production time</h4>
                            <center>
                                <div class="laptopsizegraph" style="height: 180px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 180px;" >
                                    <div class="laptopsizegraph2" style="height: 150px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 150px;" ></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-4" style="max-height: 390px !important;">
           <div class="tab-content boxShadow" style="min-height: 335px!important;">
                <div class="tab-pane fade active show" >
                    <div class="row m-l-0 m-r-0">
                        <div id="dataDailyGraph4" class="col-md-12">
                            <h4 style="color: #002060;">Production time</h4>
                            <center>
                                <div id="DailyGraph4" style="height:270px;" ></div>
                            </center>
                        </div>
                        <div id="blankDailyGraph4" class="col-md-12 graphmargin" style="height:273px;display:none;" >
                            <h4 style="color: #002060;">Production time</h4>
                            <center>
                                <div class="laptopsizegraph" style="height: 160px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14%;width: 160px;" >
                                    <div class="laptopsizegraph2" style="height: 130px;border: 1px solid #d8cfcf;border-radius: 50%;margin-top: 14px;width: 130px;" ></div>
                                </div>
                            </center>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-3">
            <div class="tab-content laptopsize boxShadow" style="height: 335px;">
                <div class="tab-pane fade active show">
                    
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #76BA1B;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> RUNNING</div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #FFCF00;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> WAITING</div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #F60100;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> STOPPED</div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #CCD2DF;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> OFF</div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #000000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> NODET</div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #002060;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> PRODUCTION TIME </div>
                    </div>
                
                    <div style="padding-bottom: 20px;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #124D8D;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;">  SETUP TIME </div>
                    </div>
                
                    <div style="padding-bottom: 6%;">
                        <div class="bagerClass bagerClasss laptopsize" style="background: #FF8000;"> &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</div><div style="padding-left: 10px;"> NO PRODUCTION </div>
                    </div>
                    
                </div>
            </div>
        </div>
    </div>
    <div class="graph-loader hide" ></div>
        <div class="row"  style="margin-left: -20px;">
        <div class="col-md-12" >
            <div class="tab-content boxShadow" style="">
                <div class="tab-pane fade active show" style="overflow-x: scroll!important;">
                    <div class="row m-l-0 m-r-0">
                        <div class="col-md-12" id="graphdynamicsize">
                            <h3 id="headerChange" style="color: #002060;">Hourly machine analysis</h3>
                            <div id="DailyGraph1" style="height:350px;width:100%;" >                        
                            </div> 
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <hr style="background: gray;">
        <p>@2021 nytt | All Rights Reserved</p>
</div><a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>


    