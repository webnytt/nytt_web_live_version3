		<div id="content" class="content">
			
			<h1 class="page-header" style="color: #002060!important;">Maintenance & check-in</h1>
			<?php ?> 
			<?php ?>
				<input type="hidden" name="userId" id="userId" value="<?php if($_POST) { echo $_POST['userId']; } else { echo "0"; } ?>" /> 
				<div id="options" class="m-b-10">
					<span class="gallery-option-set text-center" id="filter" data-option-key="filter">
					<a href="javascript:;" class="btn btn-primary btn-sm <?php if(!$_POST) { echo "active";} ?>" id="user0" >Show All</a>
					<?php foreach($listOperators->result() as $user) { ?>
						<a href="javascript:;" id="user<?php echo $user->userId; ?>" class="btn btn-primary btn-sm  <?php if($_POST) { if($user->userId == $_POST['userId']) echo "active"; } ?>" ><?php echo $user->userName; ?></a>
					<?php } ?>
					</span> 
				</div>
				
					<?php 
					if($_POST && isset($_POST['dateValS'])) { 
						$dateValE = date("F d, Y", strtotime($_POST['dateValE']));
						$dateValS = date("F d, Y", strtotime($_POST['dateValS']));  
					}elseif ($_GET && isset($_GET['dateValS'])) {
						$dateValE = date("F d, Y", strtotime($_GET['dateValE']));
						$dateValS = date("F d, Y", strtotime($_GET['dateValS'])); 
					} else { 
						$dateValE =  date("F d, Y", strtotime("-29"));
						$dateValS = date("F d, Y"); 
					} 
					?>
					<div class="form-group row">
						<div class="col-md-8 offset-md-2 col-sm-12">
							<input type="hidden" name="dateValS" id="dateValS" <?php if($_POST) { ?>value="<?php echo $_POST['dateValS'];?>"<?php }  ?> <?php if($_GET) { ?>value="<?php echo $_GET['dateValS'];?>"<?php }  ?> />  
							<input type="hidden" name="dateValE" id="dateValE" <?php if($_POST) { ?>value="<?php echo $_POST['dateValE'];?>"<?php }  ?> <?php if($_GET) { ?>value="<?php echo $_GET['dateValE'];?>"<?php }  ?> />   
							<div id="advance-daterange" name="advance-daterange" class="btn btn-primary btn-block text-left f-s-12">
								<i class="fa fa-caret-down pull-right m-t-2"></i>
								<span>
								<?php echo $dateValS.' - '.$dateValE; ?>
								</span> 
							</div>
						</div>
					</div>
			<div class="row">
				<div class="col-md-12">
				  <div class="panel panel-inverse panel-primary" style="overflow-x: scroll;" >
						<div class="panel-body">
							<table id="empTable" class="display table table-bordered m-b-0" width="100%" cellspacing="0">
								<thead>
									<tr>
										<th>Id</th>
										<th>User</th>
										<th>Machine</th>
										<th>Status</th>
										<th>Checkin time</th>
										<th>Checkout time</th>
										<th>Maintenance</th>
										<th>Type</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div> 
			</div>
		</div>
		<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
	</div>
	
	

