<link href="<?php echo base_url('assets/css/taskMaintenance.css'); ?>" rel="stylesheet" />
<link href="<?php echo base_url('assets/css/instruction.css'); ?>" rel="stylesheet" />

<style>
    .cke_dialog_ui_vbox_child {
        padding: 10px 0px !important;
    }

    input:disabled {
        background-color: #eee !important;
        border-color: #eee !important;
    }

    .cke_dialog_ui_input_file {
        height: 100px !important;
    }

    .nav-tabs>li>a {
        margin-right: 0px !important;
    }

    .navtabscustStyle {
        min-width: 100px;
        text-align: center;
    }
    .addInstructionButStyle
    {
        float:right!important;border-radius: 50px!important;
    }
    .MaintitlefontStyle
    {
        font-size: 18px!important;font-weight: 600!important;
    }
    .MainTitleDeleteIconStyle
    {
        float:right!important;width: 26px!important;margin-top: -8px!important;cursor:pointer!important;
    }
    .SubinstructionFontSize
    {
        font-size:14px;min-height: 60px;
    }
    .SubinstructionIconSize
    {
        width: 20px;cursor: pointer;float: right;
    }
    .SubinstructionImagestyle
    {
        max-width:90%!important;box-shadow: grey 0px 0px 8px -2px !important;display: block!important;margin:0 auto!important;border-radius:20px!important;
    }
    .SubinstructionImagesstyle
    {
        width:90%;box-shadow: grey 0px 0px 8px -2px !important;border-radius:20px;
    }
    .SetMainTitleGifStyle
    {
        max-width:230px!important;display: block!important;margin: 0 auto!important;box-shadow: grey 0px 0px 8px -2px !important;border-radius:20px!important;
    }
    .SetSubTitleStyle
    {
        font-size:14px;overflow: auto;height: 80px;
    }
    .SetSubEditIcon
    {
        width: 20px;cursor: pointer;
    }
    .SetSubEditIcons
    {
        width:250px!important;box-shadow: grey 0px 0px 8px -2px !important;display:block!important;margin: 0 auto!important;border-radius: 20px!important;
    }
    .OpAppTitleStyle
    {
        font-size: 18px!important;font-weight: 600!important;
    }
    .OpAppImageStyle
    {
        max-width:230px!important;border-radius:20px!important;display: block!important;margin: 0 auto!important;
    }
    .OpAppSubtitleStyle
    {
        font-size:14px!important;min-height: 60px!important;
    }
    .OpAppSubtitleGifStyle
    {
        max-width: 230px!important;display: block!important;margin: 0 auto;box-shadow: grey 0px 0px 8px -2px !important;border-radius:20px!important;
    }
    .modalAddTaskStyle
    {
        padding: 0 5px!important;
    }
    .EditModalpad
    {
        padding: 0 5px!important;
    }

</style>
<div id="content" class="content">


    <h1 class="page-header TaskAndMainTenanceHeaderStyle"><?php echo Managehelpinstructions; ?></h1>

    <?php if ($userRole == "2") { ?>

        <a href="#modal-add-task" data-toggle="modal" class="btn btn-primary addInstructionButStyle">
            <i class="fa fa-plus"></i>&nbsp;&nbsp;&nbsp;<?php echo ADDINSTRUCTION; ?>
        </a>
    <?php } ?>

    <ul class="nav nav-tabs" role="tablist">
        <li class="active nav-item navtabscustStyle"><a href="#dashbaord" role="tab" data-toggle="tab" class="nav-link active show"><?php echo Dashboard; ?></a></li>
        <li class="nav-item navtabscustStyle"><a href="#opaap" role="tab" data-toggle="tab" class="nav-link"><?php echo OpApp; ?></a></li>
        <li class="nav-item navtabscustStyle"><a href="#setapp" role="tab" data-toggle="tab" class="nav-link"><?php echo SetApp; ?></a></li>
    </ul>



    <div class="tab-content boxShadow">

        <div class="tab-pane active" id="dashbaord">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control col-md-12 border-left-right-top-hide h-35 d-inline-block" onkeyup="searchInstruction(this.value,'1')" placeholder="<?php echo Searchkeyword; ?>" required>
                </div>
            </div>
            <div class="row" id="dashboardSerachData">
                <?php foreach ($instructionDashbaord as $key => $value) { ?>
                    <div class="col-md-12 m-t-30">
                        <?php $subInstruction = $this->AM->getWhereDB(array("instructionId" => $value['instructionId']), "subInstruction"); ?>
                        <div class="col-md-12">
                            <div class="MaintitlefontStyle"><?php echo $value['main_title']; ?> </div>
                            <?php if ($userRole == "2") { ?>
                                <img onclick="editInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                <img onclick="deleteInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/delete.svg'); ?>">
                            <?php } ?>
                        </div>

                        <?php if (!empty($value['main_title_gif'])) { ?> <br>
                            <img class="SubinstructionImagestyle" src="<?php echo base_url('assets/instruction/') . $value['main_title_gif']; ?>">
                        <?php } ?>

                        <?php if (is_array($subInstruction) && count($subInstruction) > 0) { ?>
                            <div class="row m-t-15">
                                <?php foreach ($subInstruction as $keySub => $valueSub) { ?>
                                    <div class="col-md-6 m-t-30">
                                        <div class="SubinstructionFontSize">
                                            <?php echo $valueSub['subTitle']; ?> &nbsp;
                                            <?php if ($userRole == "2") { ?>
                                                <img class="main-img SubinstructionIconSize" onclick="editSubInstruction(<?php echo $valueSub['subInstructionId']; ?>)" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                            <?php } ?>
                                        </div>
                                        <br>
                                        <img class="SubinstructionImagesstyle" src="<?php echo base_url('assets/instruction/') . $valueSub['subTitleGif']; ?>">
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="tab-pane" id="opaap">
            <div class="row">
                <div class="col-md-8"></div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control col-md-12 border-left-right-top-hide h-35 d-inline-block" onkeyup="searchInstruction(this.value,'2')" placeholder="<?php echo Searchkeyword; ?>" required>
                </div>
            </div>
            <div class="row" id="opAppSerachData">
                <?php foreach ($instructionOpaap as $key => $value) { ?>
                    <div class="col-md-12 m-t-15">
                        <?php $subInstruction = $this->AM->getWhereDB(array("instructionId" => $value['instructionId']), "subInstruction"); ?>
                        <div class="col-md-10">
                            <div class="MaintitlefontStyle"><?php echo $value['main_title']; ?> </div>
                        </div>

                        <div class="col-md-2" style="float:right;">
                            <?php if ($userRole == "2") { ?>
                                <img onclick="editInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                <img onclick="deleteInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/delete.svg'); ?>">
                            <?php } ?>
                        </div>

                        <?php if (!empty($value['main_title_gif'])) { ?> <br>
                            <img class="SetMainTitleGifStyle" src="<?php echo base_url('assets/instruction/') . $value['main_title_gif']; ?>">
                        <?php } ?>

                        <?php if (is_array($subInstruction) && count($subInstruction) > 0) { ?>
                            <div class="row">
                                <?php foreach ($subInstruction as $keySub => $valueSub) { ?>
                                    <div class="col-md-6 m-t-30">
                                        <div class="row">
                                            <div class="col-md-10 SetSubTitleStyle">
                                                <?php echo $valueSub['subTitle']; ?> &nbsp;
                                            </div>
                                            <div class="col-md-2">
                                                <?php if ($userRole == "2") { ?>
                                                    <img class="SetSubEditIcon" onclick="editSubInstruction(<?php echo $valueSub['subInstructionId']; ?>)" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                                <?php } ?>
                                            </div>
                                            <br>
                                            <img class="SetSubEditIcons" src="<?php echo base_url('assets/instruction/') . $valueSub['subTitleGif']; ?>">
                                        </div>
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>

        <div class="tab-pane" id="setapp">

            <div class="row">
                <div class="col-md-8"></div>
                <div class="form-group col-md-4">
                    <input type="text" class="form-control col-md-12 border-left-right-top-hide h-35 d-inline-block" onkeyup="searchInstruction(this.value,'3')" placeholder="<?php echo Searchkeyword; ?>" required>
                </div>
            </div>
            <div class="row" id="setAppSerachData">
                <?php foreach ($instructionSetApp as $key => $value) { ?>
                    <div class="col-md-12 m-t-15">
                        <?php $subInstruction = $this->AM->getWhereDB(array("instructionId" => $value['instructionId']), "subInstruction"); ?>
                        <div class="col-md-12">
                            <div class="OpAppTitleStyle"><?php echo $value['main_title']; ?> </div>
                            <?php if ($userRole == "2") { ?>
                                <img onclick="editInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                <img onclick="deleteInstruction(<?php echo $value['instructionId']; ?>)" 
                                class="MainTitleDeleteIconStyle" src="<?php echo base_url('assets/nav_bar/delete.svg'); ?>">
                            <?php } ?>
                        </div>
                        <?php if (!empty($value['main_title_gif'])) { ?> <br>
                            <img class="OpAppImageStyle" style="" src="<?php echo base_url('assets/instruction/') . $value['main_title_gif']; ?>">
                        <?php } ?>

                        <?php if (is_array($subInstruction) && count($subInstruction) > 0) { ?>
                            <div class="row m-t-15">
                                <?php foreach ($subInstruction as $keySub => $valueSub) { ?>
                                    <div class="col-md-6 m-t-15">
                                        <div class="row">
                                            <div class="col-md-9 OpAppSubtitleStyle">
                                                <?php echo $valueSub['subTitle']; ?> &nbsp;
                                            </div>

                                            <div class="col-md-2">
                                                <?php if ($userRole == "2") { ?>
                                                    <img loading="lazy" onclick="editSubInstruction(<?php echo $valueSub['subInstructionId']; ?>)" style="width: 20px;cursor: pointer;" src="<?php echo base_url('assets/nav_bar/create.svg'); ?>">
                                                <?php } ?>
                                            </div>
                                        </div>

                                        <br>
                                        <img class="OpAppSubtitleGifStyle" src="<?php echo base_url('assets/instruction/') . $valueSub['subTitleGif']; ?>">
                                    </div>
                                <?php } ?>
                            </div>
                        <?php } ?>
                    </div>
                <?php } ?>
            </div>
        </div>
    </div>
    <hr style="background: gray;">
    <p>@2021 nytt | All Rights Reserved</p>
</div>

<a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top"><i class="fa fa-angle-up"></i></a>
</div>

<div class="modal fade" id="modal-add-task" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title" id="liveMachineName" style="color: #ff8000;"><?php echo ADDINSTRUCTION; ?></h4>
                <button type="button" class="close" style="opacity: 1.0!important;" data-dismiss="modal">
                    <img width="16" height="16"  src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="add_instruction" method="POST" id="add_instruction_form" enctype="multipart/form-data" style="">

                    <div class="row m-r-0 m-l-0">
                        <div class="col-md-12" style="padding: 0 5px;">
                            <label style="font-weight: 600;"><?php echo Type; ?></label>
                        </div>
                        <div class="col-md-12" style="padding: 0 5px;">
                            <select style="min-width:100%;width:100%;" class="form-control select2 select2Select" name="instruction_type" id="instruction_type">
                                <option selected="" disabled=""><?php echo Selecttype; ?></option>
                                <option value="1"><?php echo Dashboard; ?></option>
                                <option value="2"><?php echo OpApp; ?></option>
                                <option value="3"><?php echo SetApp; ?></option>
                            </select>
                        </div>
                    </div>


                    <div class="row m-l-0 m-r-0 m-t-10">

                        <div style="" class="col-md-12">
                            <label style="font-weight: 600;"><?php echo Maintitledetail; ?></label>
                        </div>

                        <div  class="form-group col-md-8 modalAddTaskStyle">
                            <input type="text" style="" class="form-control col-md-12 border-left-right-top-hide h-30" id="main_title" name="main_title" placeholder="<?php echo Entermaintitle; ?>" required>
                        </div>

                        <div class="form-group col-md-4 modalAddTaskStyle">
                            <input type="text" style="" class="form-control col-md-12 border-left-right-top-hide h-30" id="search_keyword" name="search_keyword" placeholder="<?php echo Entersearchkeyword; ?>" required>
                        </div>

                        <div class="form-group col-md-8 modalAddTaskStyle">
                            <input type="file" style="" class="form-control col-md-12 border-left-right-top-hide h-30" name="main_title_gif">
                        </div>

                        <div class="form-group col-md-4 modalAddTaskStyle">
                            <input type="text" style="display: none;" class="form-control col-md-12 border-left-right-top-hide h-30" id="search_position_main" name="search_position_main" placeholder="<?php echo Enterposition; ?>" value="1">
                        </div>

                        <div class="col-md-12">
                            <hr style="background: gray;">
                        </div>

                    </div>

                    <div id="sub_title_detail"></div>

                    <div class="row m-t-15 m-b-15">
                        <div class="col-md-12">
                            <center> <a onclick="add_sub_detail()" style="cursor: pointer;"><i class="fa fa-plus"></i>
                                <?php echo  Addsubtitledetail; ?></a> </center>
                        </div>
                    </div>
                    <div class="row">
                        <div style="padding: 0 5px;" class="col-md-12">
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5" id="add_instruction_submit">
                                    <i class="fa fa-plus" style="font-size: 15px;"></i> <?php echo ADDINSTRUCTION; ?></button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-task" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title" id="liveMachineName" style="color: #ff8000;"><?php echo Editinstruction; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="edit_instruction" method="POST" id="edit_instruction_form" enctype="multipart/form-data">


                    <input type="hidden" name="instructionId" id="instructionId">
                    <input type="hidden" name="old_edit_main_title_gif" id="old_edit_main_title_gif">


                    <div class="row m-r-0 m-l-0 m-t-10">

                        <div class="col-md-12 EditModalpad">
                            <label style="font-weight: 600;"><?php echo Maintitledetail; ?></label>
                        </div>

                        <div class="form-group col-md-8 EditModalpad">
                            <input type="text" style="" class="form-control col-md-12 h-30 d-inline-block border-left-right-top-hide" id="edit_main_title" name="edit_main_title" placeholder="<?php echo Entermaintitle; ?>" required>
                        </div>

                        <div class="form-group col-md-4 EditModalpad">
                            <input type="text" style="" class="form-control col-md-12 h-30 d-inline-block border-left-right-top-hide" id="edit_search_keyword" name="edit_search_keyword" placeholder="<?php echo Entersearchkeyword; ?>" required>
                        </div>

                        <div class="form-group col-md-8 EditModalpad">
                            <input type="file" style="" class="form-control col-md-12 h-30 d-inline-block border-left-right-top-hide" name="edit_main_title_gif">
                        </div>

                        <div class="form-group col-md-4 EditModalpad">
                            <input type="text" style="display: none;" class="form-control h-30 d-inline-block col-md-12 border-left-right-top-hide" id="edit_search_position_main" name="edit_search_position_main" placeholder="<?php echo Enterposition; ?>" value="1">

                        </div>

                        <div class="col-md-12">
                            <hr style="background: gray;">
                        </div>
                    </div>
                    <div id="edit_sub_title_detail"></div>

                    <div class="row m-t-15 m-b-15">
                        <div class="col-md-12">
                            <center> 
                                <a onclick="edit_sub_detail()" style="cursor: pointer;"><i class="fa fa-plus"></i> <?php echo Addsubtitledetail; ?></a> 
                            </center>
                        </div>
                    </div>

                    <div class="row">
                        <div style="padding: 0 5px;" class="col-md-12">
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5" id="edit_instruction_submit">
                                    <?php echo save; ?></button>
                            </center>
                        </div>
                    </div>

                </form>
            </div>
        </div>
    </div>
</div>

<div class="modal fade" id="modal-edit-sub-task" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header" style="background-color: #002060;">
                <h4 class="modal-title" id="liveMachineName" style="color: #ff8000;"><?php echo Editsubinstruction; ?></h4>
                <button type="button" class="close" data-dismiss="modal" style="opacity: 1.0!important;">
                    <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
                </button>
            </div>
            <div class="modal-body">
                <form class="p-b-20 m-l-10 m-r-10" action="edit_sub_instruction" method="POST" id="edit_sub_instruction_form" enctype="multipart/form-data">

                    <input type="hidden" name="subInstructionId" id="subInstructionId">
                    <input type="hidden" name="old_edit_sub_title_gif" id="old_edit_sub_title_gif">

                    <div class="row m-l-0 m-r-0 m-t-10">

                        <div class="col-md-12 EditModalpad">
                            <label style="font-weight: 600;"><?php echo Subtitledetail; ?></label>
                        </div>

                        <div class="form-group col-md-8 EditModalpad">
                            <input type="text" style="" class="form-control col-md-12 border-left-right-top-hide h-30" id="edit_sub_title" name="edit_sub_title" placeholder="<?php echo Entersubtitle; ?>" required>
                        </div>

                        <div class="form-group col-md-4 EditModalpad">
                            <input type="text" style="display: none;" class="form-control col-md-12 border-left-right-top-hide h-30" id="edit_sub_search_position" name="edit_sub_search_position" placeholder="<?php echo Enterposition; ?>" value="1">

                        </div>

                        <div class="form-group col-md-8 EditModalpad">
                            <input type="file" style="" class="form-control col-md-12 border-left-right-top-hide h-30" name="edit_sub_title_gif">
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-12 EditModalpad">
                            <center>
                                <button type="submit" class="btn btn-primary m-r-5" id="edit_sub_instruction_submit">
                                    <?php echo save; ?></button>
                            </center>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>