<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8" />
    <meta http-equiv="Access-Control-Allow-Origin" content="*">
    <!-- <link rel="icon" href="<?php echo base_url(); ?>assets/img/app_logo_orange.png"> -->
    <title><?php echo dashboard; ?></title>
    <meta content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no" name="viewport" />
    <meta content="" name="description" />
    <meta content="" name="author" />
    <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/css/bootstrap.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/font-awesome/5.3/css/all.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/animate/animate.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/gritter/css/jquery.gritter.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/isotope/isotope.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/lightbox/css/lightbox.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-daterangepicker/daterangepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/select2/dist/css/select2.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/image-picker/image-picker.css" rel="stylesheet" />
    <link rel="icon" href="<?php echo base_url('assets/img/favicon.png'); ?>" type="image/png" sizes="16x16">


    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" />


    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick.css'); ?>">
    <link rel="stylesheet" type="text/css" href="<?php echo base_url('assets/slick/slick-theme.css'); ?>">

    <style type="text/css">
        html,
        body {
            margin: 0;
            padding: 0;
        }

        * {
            box-sizing: border-box;
        }

        .slider {
            width: 96%;
            margin: 16px auto;
        }

        .slick-slide {
            margin: 0px 20px;
        }

        .slick-slide img {
            width: 100%;
        }

        .slick-prev:before,
        .slick-next:before {
            color: gray;
        }


        .slick-slide {
            transition: all ease-in-out .3s;
            opacity: .2;
        }

        .slick-active {
            opacity: 1;
        }

        .slick-current {
            opacity: 1;
        }
    </style>

    <link href="<?php echo base_url(); ?>assets/css/facebook/style.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/facebook/style-responsive.min.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/added-style.css" rel="stylesheet" />
    <link href="<?php echo base_url(); ?>assets/css/facebook/theme/default.css" rel="stylesheet" id="theme" />
    <style>
        .card-light {
            box-shadow: 0 5px 5px rgba(0, 0, 0, 0.5), 0 1px 2px rgba(0, 0, 0, 0.5);
        }

        .card-text {
            border: 1px solid #aaa;
        }

        .card {}

        .filter-panel-content {
            background-color: rgba(255, 255, 255, 0.85);
        }

        body,
        label {
            color: #002060;
        }

        .sidebar-wrapper,
        .sidebar-wrapper .sidebar-dropdown>a:after,
        .sidebar-wrapper ul li a i,
        .page-wrapper .page-content {
            -webkit-transition: all 0.3s;
            -moz-transition: all 0.3s;
            -o-transition: all 0.3s;
            -ms-transition: all 0.3s;
            transition: all 0.3s;
        }

        @keyframes swing {
            0% {
                transform: rotate(0deg);
            }

            10% {
                transform: rotate(10deg);
            }

            30% {
                transform: rotate(0deg);
            }

            40% {
                transform: rotate(-10deg);
            }

            50% {
                transform: rotate(0deg);
            }

            60% {
                transform: rotate(5deg);
            }

            70% {
                transform: rotate(0deg);
            }

            80% {
                transform: rotate(-5deg);
            }

            100% {
                transform: rotate(0deg);
            }
        }

        /*----------------page-wrapper----------------*/
        .page-wrapper {
            height: 100vh;
        }

        /*----------------toggeled sidebar----------------*/
        .page-wrapper.toggled .sidebar-wrapper {
            left: 0px;
        }

        @media screen and (min-width: 768px) {
            .page-wrapper.toggled .page-content {
                padding-left: 300px;
            }
        }

        .page-wrapper.toggled #toggle-sidebar {
            position: absolute;
            color: #cacaca;
        }

        /*----------------sidebar-wrapper----------------*/
        .sidebar-wrapper {
            width: 260px;
            background: #1d1d1d;
            height: 100%;
            max-height: 100%;
            position: fixed;
            top: 0;
            left: -300px;
        }

        .sidebar-wrapper ul li:hover a i,
        .sidebar-wrapper .sidebar-dropdown .sidebar-submenu li a:hover:before,
        .sidebar-wrapper .sidebar-search input.search-menu:focus+span,
        .sidebar-wrapper .sidebar-menu .sidebar-dropdown.active a i {
            color: #fb5edf;
        }

        .sidebar-wrapper ul {
            list-style-type: none;
            padding: 0;
            margin: 0;
        }

        .sidebar-wrapper a {
            text-decoration: none;
        }

        /*----------------sidebar-content----------------*/
        .sidebar-content {
            max-height: calc(100% - 30px);
            height: calc(100% - 30px);
            overflow-y: auto;
            position: relative;
        }

        .sidebar-content.desktop {
            overflow-y: hidden;
        }

        /*--------------------sidebar-brand----------------------*/
        .sidebar-wrapper .sidebar-brand {
            padding: 15px 20px;
            text-align: center;
        }

        .sidebar-wrapper .sidebar-brand>a { 
            color: #fff;
            text-transform: uppercase;
            font-weight: bold;
        }

        /*--------------------sidebar-header----------------------*/
        .sidebar-wrapper .sidebar-header {
            padding: 20px;
            overflow: hidden;
            border-top: 1px solid #2b2b2b;
        }

        .sidebar-wrapper .sidebar-header .user-pic {
            float: left;
            width: 60px;
            padding: 2px;
            border: 1px solid #585858;
            border-radius: 8px;
            margin-right: 15px;
        }

        .sidebar-wrapper .sidebar-header .user-info {
            float: left;
            color: #b3b8c1;
        }

        .sidebar-wrapper .sidebar-header .user-info span {
            display: block;
        }

        .sidebar-wrapper .sidebar-header .user-info .user-role {
            font-size: 12px;
            color: #7c818a;
        }

        .sidebar-wrapper .sidebar-header .user-info .user-status * {
            display: inline-block;
            color: #7c818a;
            font-size: 12px;
        }

        .sidebar-wrapper .sidebar-header .user-info .user-status i {
            font-size: 8px;
            color: #21e80b;
        }

        /*-----------------------sidebar-search------------------------*/

        .sidebar-wrapper .sidebar-search {
            border-top: 1px solid #2b2b2b;
        }

        .sidebar-wrapper .sidebar-search>div {
            padding: 10px 20px;
        }

        .sidebar-wrapper .sidebar-search input.search-menu,
        .sidebar-wrapper .sidebar-search .input-group-addon {
            background: #2b2b2b;
            box-shadow: none;
            color: #7c818a;
            border-color: #2b2b2b;
            transition: color 0.5s;
        }

        /*----------------------sidebar-menu-------------------------*/
        .sidebar-wrapper .sidebar-menu {
            border-top: 1px solid #2b2b2b;
            padding-bottom: 10px;
        }

        .sidebar-wrapper .sidebar-menu .header-menu span {
            font-weight: bold;
            font-size: 14px;
            color: #4e5767;
            padding: 15px 20px 5px 20px;
            display: inline-block;
        }

        .sidebar-wrapper .sidebar-menu ul li a {
            display: inline-block;
            width: 100%;
            color: #7c818a;
            text-decoration: none;
            transition: color 0.3s;
            position: relative;
            padding: 8px 30px 8px 20px;
        }

        .sidebar-wrapper .sidebar-menu ul li:hover>a,
        .sidebar-wrapper .sidebar-menu .sidebar-dropdown.active>a {
            color: #b3b8c1;
        }

        .sidebar-wrapper .sidebar-menu ul li a i {
            margin-right: 10px;
            font-size: 14px;
            background: #2b2b2b;
            width: 30px;
            height: 30px;
            line-height: 30px;
            text-align: center;
            border-radius: 4px;
        }

        .sidebar-wrapper .sidebar-menu ul li a:hover>i::before {
            display: inline-block;
            animation: swing ease-in-out 0.5s 1 alternate;
        }

        .sidebar-wrapper .sidebar-menu .sidebar-dropdown div {
            background: #2b2b2b;
        }

        .sidebar-wrapper .sidebar-menu .sidebar-dropdown>a:after {
            content: "\f105";
            font-family: FontAwesome;
            font-weight: 400;
            font-style: normal;
            display: inline-block;
            text-align: center;
            text-decoration: none;
            background: 0 0;
            speak: none;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            position: absolute;
            right: 15px;
            top: 14px;
        }

        .sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu ul {
            padding: 5px 0;
        }

        .sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li {
            padding-left: 25px;
            font-size: 13px;
        }

        .sidebar-wrapper .sidebar-menu .sidebar-dropdown .sidebar-submenu li a:before {
            content: "\f10c";
            font-family: FontAwesome;
            font-weight: 400;
            font-style: normal;
            display: inline-block;
            text-align: center;
            text-decoration: none;
            speak: none;
            -webkit-font-smoothing: antialiased;
            -moz-osx-font-smoothing: grayscale;
            margin-right: 10px;
            font-size: 10px;
        }


        /*--------------------------side-footer------------------------------*/

        .sidebar-footer {
            position: absolute;
            width: 100%;
            bottom: 0;
            display: flex;
            background: #2b2b2b;
        }

        .sidebar-footer>a {
            flex-grow: 1;
            text-align: center;

            height: 30px;
            line-height: 30px;
            color: #7c818a;
            position: relative;
        }

        .sidebar-footer>a:hover {
            color: #b3b8c1;
        }

        .sidebar-footer>a .notification {
            position: absolute;
            top: 0;
        }

        /*--------------------------page-content-----------------------------*/
        .page-wrapper .page-content {
            display: inline-block;
            width: 100%;
            padding-left: 0px;
        }

        .page-wrapper .page-content>div {
            padding: 20px 40px;
        }

        .page-wrapper .page-content {
            overflow-x: hidden;
        }

        /*---------------------toggle-sidebar button-------------------------*/
        #toggle-sidebar {
            position: fixed;
            top: 13px;
            left: 25px;
            color: #0f151f;
            font-size: 18px;
        }

        ul ul {
            list-style-type: none !important;
        }

        a {
            color: #fff;
        }

        ul li a:hover {
            color: white !important;
        }
    </style>
    <style type="text/css">
        @media only screen and (min-device-width: 320px) and (max-device-width: 960px) {

            .desktop-logo {
                margin-top: 32px;
            }
        }
    </style>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery/jquery-3.3.1.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/jquery-ui/jquery-ui.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/bootstrap/4.1.3/js/bootstrap.bundle.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/slimscroll/jquery.slimscroll.min.js"></script>
    <script src="<?php echo base_url(); ?>assets/plugins/js-cookie/js.cookie.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/theme/facebook.min.js"></script>

    <script src="<?php echo base_url(); ?>assets/js/apps.min.js"></script>
</head>

<body>
    <div class="page-cover"></div>
    <div id="page-loader" class="fade show"><span class="spinner"></span></div>
    <div id="page-container" class="fade page-sidebar-fixed page-header-fixed">
        <div id="header" class="header navbar-inverse">
            <div class="navbar-header">
                <a href="<?php echo base_url(); ?>admin/overview" class="navbar-brand">
                    <div class="widget-img widget-img-md m-r-5" style="background-image: url(<?php echo base_url(); ?>assets/img/app_logo_orange.png);float:left;width:94px;"></div><span style="font-weight:bold;"></span>
                </a>
                <button type="button" class="navbar-toggle" data-click="sidebar-toggled">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
            </div>
        </div>

        <div id="sidebar" class="sidebar">
            <div style="position: relative;overflow: auto;width: auto;height: 90%;" data-height="100%">
                <ul class="nav desktop-logo" style="background-color: #002060;">
                    <li class="nav-profile">
                        <div class="info">
                            <img width="165" src="<?php echo base_url('assets/img/app_logo_orange.png') ?>">
                            <?php if ($this->AM->checkUserRole() == 2) { ?>
                                <div class="form-group m-b-5">
                                    <select class="form-control form-control-lg" id="factoryId" name="factoryId" required>
                                        <?php foreach ($listFactories->result() as $factory) { ?>
                                            <option <?php if ($factory->factoryId == $factoryData->factoryId) {
                                                        echo "selected";
                                                    } ?> value="<?php echo $factory->factoryId; ?>"><?php echo base64_decode($factory->factoryName); ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            <?php } else { ?>
                                <?php echo base64_decode($factoryData->factoryName); ?>
                            <?php } ?>
                        </div>
                    </li>
                </ul>

                <ul class="nav">
                        <li <?php if ($this->uri->segment(2) == 'overview') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url(); ?>admin/overview">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/home.svg'); ?>">
                                <span style="padding-left: 17px;"><?php echo Overview; ?></span>
                            </a>
                        </li>
                    <?php 
                    $factoryId = $this->session->userdata('factoryId');
                    $factoryType = $this->session->userdata('factoryType');
                    /*if ($factoryType == "machine") {*/ ?>
                    
                    <li <?php if($this->uri->segment(2) == 'operator_details' || $this->uri->segment(2) == 'operator_checkin_checkout' ) { echo 'class="active"'; } ?> >
                        <a href="<?php echo base_url('Operator/operator_details'); ?>" >
                            <img style="margin-top: -4px;" width="20" height="20" src="<?php echo base_url('assets/nav_bar/operators.svg'); ?>">  
                            <span style="padding-left: 17px;"><?php echo Operators; ?></span> 
                        </a>
                    </li>
                   

                      <li class="sidebar-dropdown">
                            <a href="javascript:void(0)"><i class="fa fa-dashboard"></i>
                                <img style="margin-top: -4px;" width="20" height="20" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
                                <span style="padding-left: 17px;"><?php echo Analytics; ?><i style="float:right;margin-right:30%;" class="fas fa-caret-down"></i></span>
                            </a>
                            <div class="sidebar-submenu" style="display: none;">
                                <ul>
                                    <li style="padding: 5%;padding-left:20%;">
                                        <a href="<?php echo base_url('Analytics/dashboard'); ?>"></i><?php echo Graphanalytics; ?></a>
                                    </li>
                                    <li style="padding: 5%;padding-left:20%;">
                                        <a href="<?php echo base_url('Analytics/breakdown'); ?>"> <?php echo Stopanalytics; ?></a>
                                    </li>
                                    <li style="padding: 5%;padding-left:20%;">
                                        <a href="<?php echo base_url('Analytics/waitBreakdown') ?>"><?php echo Waitanalytics; ?></a>
                                    </li>
                                </ul>
                            </div>
                        </li>

                    
                    
                    <?php if ($this->AM->checkUserRole() == 2) { ?>

                    <li <?php if ($this->uri->segment(2) == 'configure_stacklighttype' || $this->uri->segment(2) == 'configure_versions') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url(); ?>admin/configure_stacklighttype">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
                                <span style="padding-left: 17px;"><?php echo Training; ?></span>
                            </a>
                        </li>

                <?php } /*}else{*/ ?>

                <!--    <li <?php if($this->uri->segment(2) == 'avg_color_beta_details_list' ) { echo 'class="active"'; } ?> >
                        <a href="<?php echo base_url(); ?>admin/avg_color_beta_details_list" >
                            <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>"> 
                            <span style="padding-left: 17px;">DATA</span> 
                        </a>
                    </li> -->
                <?php /*}*/ if ($this->AM->checkUserRole() == 2) { ?>
                    
                <li <?php if ($this->uri->segment(2) == 'app_versions') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url(); ?>admin/app_versions">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/analytics.svg'); ?>">
                                <span style="padding-left: 17px;"><?php echo Appversions; ?></span>
                            </a>
                        </li>
                <?php   } ?>

                    <?php 

                        $reportedNotesCount = $this->AM->getWhereCount(array("isSeen" => "0"),"reportProblem");
                        if ($reportedNotesCount > 0) 
                        {
                            $reportedNotesdisplay = "inline-block";
                        }else
                        {
                            $reportedNotesdisplay = "none";
                        }
                        ?>

                        <li <?php if ($this->uri->segment(2) == 'reportProblem') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url(); ?>admin/reportProblem">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/report.svg'); ?>">
                                <span style="padding-left: 15px;"><?php echo Reportednotes; ?> <i id="reportedNotesDots" style="float: none;width: 0px;text-align: center;font-size: 8px;display: <?php echo $reportedNotesdisplay; ?>;" class="fa fa-circle"></i> </span>
                            </a>
                        </li>

                     <li <?php if ($this->uri->segment(2) == 'meeting_notes' || $this->uri->segment(2) == 'meeting_note_details') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url('MeetingNotes/meeting_notes'); ?>">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/meeting_notes_white.png'); ?>">
                                <span style="padding-left: 17px;"><?php echo Meetingnotes; ?></span>
                            </a>
                        </li>

                    <li <?php if ($this->uri->segment(2) == 'taskMaintenance') {
                                echo 'class="active"';
                            } ?>>
                            <a href="<?php echo base_url('Tasks/taskMaintenance'); ?>">
                                <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/maintenace.svg'); ?>">
                                <!-- <span style="padding-left: 17px;">Tasks & maintenace</span> -->
                                <span style="padding-left: 17px;"><?php echo Tasksandmaintenance; ?></span>
                            </a>
                        </li>

                    
                      <li>
                        <a href="<?php echo base_url(); ?>admin/logout">
                            <img style="margin-top: -4px;" width="18" height="18" src="<?php echo base_url('assets/nav_bar/log_out.svg'); ?>">
                            <span style="padding-left: 17px;"><?php echo Logout; ?></span>
                        </a>
                    </li>

                </ul>
            </div>
            <div style="margin-left: 32px;font-size: 11px;opacity: 1;position: absolute;bottom: 20px;">
                <a href="<?php echo base_url(); ?>admin/help_report">
                    <img style="margin-top: -4px;" width="10" height="10" src="<?php echo base_url('assets/nav_bar/help.svg'); ?>">
                    <span style="padding-left: 6px;color: white;"><?php echo ContactNytt; ?></span>
                </a>
            </div>
        </div>
        <div class="sidebar-bg"> </div>

        <style type="text/css">
           .pointHide {
    list-style-type: none !important;
}

.pointHide .dropdown-item:focus, .pointHide .dropdown-item:hover,.pointHide .dropdown-menu>li>a:focus, .pointHide .dropdown-menu>li>a:hover {
    background: white;
    color: #ff8000 !important;
}
        </style>



        <span style="float: right;font-weight: 500;margin-top:-25px;margin-right: 15px;" id="admin-info">
            <?php 
            $site_lang = $this->session->userdata('site_lang');
            if (!$this->session->userdata('userImage')) {
                $imgUrl = base_url() . 'assets/img/user/default.jpg';
            } else {
                $imgUrl = base_url() . 'assets/img/user/' . $this->session->userdata('userImage');
            } ?>

            <a class="nav-link arrowPoint dropdown-toggle pointHide" href="javascript:void(0)" id="navbarDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" style="color:black;"><?php echo Hi;?>, <?php echo $this->session->userdata('userName'); ?>&nbsp;&nbsp;&nbsp;<img class="administratorImage" src="<?php echo $imgUrl; ?>" alt="administrator"></a>
            </a>
            <div class="dropdown-menu" aria-labelledby="navbarDropdown" style="border-radius: 10px;top:65px!important;">
                <a class="dropdown-item" style="font-weight:600;" href="<?php echo base_url('admin/edit_profile'); ?>"><?php echo Profile; ?></a>
                <a class="dropdown-item" style="font-weight:600;" href="<?php echo base_url('Instruction'); ?>"><?php echo Help; ?></a>
                <a class="dropdown-item" style="font-weight:600;" href="<?php echo base_url('admin/help_report'); ?>"><?php echo ContactNytt; ?></a>
                <a class="dropdown-item" style="font-weight:600;" href="javascript:void(0)"><?php echo Changelanguage; ?></a>
                <ul class="pointHide">
                    <li>
                        <a class="dropdown-item" style="padding-left:15%;color : <?php echo  ($site_lang == "1") ?  "#ff8000" : "black"; ?>" href="<?php echo base_url("Admin/switchLang/1"); ?>">English</a>
                    </li>
                    <li>
                        <a class="dropdown-item" style="padding-left:15%;color : <?php echo  ($site_lang == "2") ?  "#ff8000" : "black"; ?>" href="<?php echo base_url("Admin/switchLang/2"); ?>">Svenska</a>
                    </li>
                </ul>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" style="font-weight:600;" href="<?php echo base_url(); ?>admin/logout"><?php echo Logout; ?></a>
            </div>
        </span>



        <script type="text/javascript">
            $(".sidebar-dropdown > a").click(function() {
                $(".sidebar-submenu").slideUp(250);
                if (
                    $(this)
                    .parent()
                    .hasClass("active")
                ) {
                    $(".sidebar-dropdown").removeClass("active");
                    $(this)
                        .parent()
                        .removeClass("active");
                } else {
                    $(".sidebar-dropdown").removeClass("active");
                    $(this)
                        .next(".sidebar-submenu")
                        .slideDown(250);
                    $(this)
                        .parent()
                        .addClass("active");
                }
            });

            $("#toggle-sidebar").click(function() {
                $(".page-wrapper").toggleClass("toggled");
            });


            $(document).ready(function() 
            {

             // Get current page URL
             var url = window.location.href;

             // remove # from URL
             url = url.substring(0, (url.indexOf("#") == -1) ? url.length : url.indexOf("#"));

             // remove parameters from URL
             url = url.substring(0, (url.indexOf("?") == -1) ? url.length : url.indexOf("?"));

             // select file name
             url = url.substr(url.lastIndexOf("/") + 1);
             
             // If file name not avilable
             if(url == ''){
             url = 'index.html';
             }
             
             // Loop all menu items
             $('.menu li').each(function(){

              // select href
              var href = $(this).find('a').attr('href');
             
              // Check filename
              if(url == href){

               // Add active class
               $(this).addClass('active');
              }
             });
            });
        </script>