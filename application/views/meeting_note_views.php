
<link href="<?php echo base_url('assets/css/meeting_note_views.css');?>" rel="stylesheet" />
<div id="content" class="content">
  

  <h1 style="font-size: 22px;color: #002060" class="page-header"><?php echo Meetingnote; ?> - 
    <?php    if ($meetingNoteDetails->filterType == "1") 
      {
        echo Daily;
      }
      elseif ($meetingNoteDetails->filterType == "2") 
      {
        echo Weekly;
      }
      elseif ($meetingNoteDetails->filterType == "3") 
      {
        echo Monthly;
      }
      elseif ($meetingNoteDetails->filterType == "4") 
      {
        echo Yearly;
      } 
    ?>

  </h1>

  <small class="smallstyle">
    <a style="color: #002060 !important;" href="<?php echo base_url('MeetingNotes/meeting_notes') ?>"> <?php echo Meetingnotes; ?> </a> &gt; <?php echo $meetingNoteDetails->noteName; ?>
  </small>
  <a class="btn btn-primary downloadPreviewmobilestyle noofstoppagesHeadingstyle" style="margin-top:-10px!important;" href="#modal-download-meeting-notes " data-toggle="modal" class="btn btn-primary">
        <img class="downloadimagebuttonstyle" src="<?php echo base_url('assets/nav_bar/download.png'); ?>"><?php echo Download; ?></a>

  <div id="pdfContent">
    <input type="hidden" name="filterType" id="filterType" value="<?php echo $meetingNoteDetails->filterType; ?>">
      <div class="col-md-12" style="margin-top: 10px;">
        <div class="row">
            <div class="col-md-12">
                <?php foreach ($machines as $key => $value) { ?>
                  <button type="button" class="btn btn-primary" style="background: #002060 !important;border-color: #002060!important;"><?php echo $value['machineName']; ?></button>
                <?php } ?>
            </div>
        </div>
        <div class="row" style="margin-top: 20px;">
          <div class="col-md-6">
            <div class="noofstoppagesHeadingtextstyle">
              <h4 style="color:white;"><center> <?php echo Noofstoppages; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row"> 
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle startdateenddatefontmobile h3startenddate">
                      <?php
                        if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->startDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->startDate));
                        } 
                      ?>
                    </h3>
                  </center>
                </div>

                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle startdateenddatefontmobile h3startenddate">
                      <?php
                        if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->endDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->endDate));
                        } 
                      ?>
                    </h3>
                  </center>
                </div>
             <span class="divdotlinestyle" style="margin-left:22%;">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
              </span>
              </div>

              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle">
                      <h3 style="color:#002060;">
                        <?php echo $NoOfStoppagesStart; ?></h3>
                    </div>
                  </center>
                </div>

                <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                  <?php if ($NoOfStoppagesStart > $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow noofstoppagespanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">-<?php echo $NoOfStoppagesStart - $NoOfStoppagesEnd; ?></center>
                        </h3>
                   </div>
                   <div class="panel panel-inverse panel-primary boxShadow noofstoppagespanelstylee">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100"><img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                  <?php } else if ($NoOfStoppagesStart < $NoOfStoppagesEnd) { ?>
                  <div class="panel panel-inverse panel-primary boxShadow noofstoppagespanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $NoOfStoppagesEnd - $NoOfStoppagesStart; ?>
                          </center>
                        </h3>
                   </div>
                   <div class="panel panel-inverse panel-primary boxShadow noofstoppagespanelstylee">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increaseImageStopped.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                  <?php } else { ?>
                   <div class="panel panel-inverse panel-primary boxShadow noofstoppagespanelstablestylee">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php  } ?>
                </div>

                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle"><h3 style="color:#002060;"><?php echo  $NoOfStoppagesEnd; ?></h3>
                    </div>
                  </center>
                </div>

              </div>
            </div>
         </div>


          <div class="col-md-6">
            <div class="noofreportsheaderstyle">
              <h4 style="color:white;"><center> <?php echo Noofreports; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle startdateenddatefontmobile h3startenddate h3startenddate h3startenddate">
                    	<?php
                        if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->startDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->startDate));
                        } 
                      ?>
                   	</h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle startdateenddatefontmobile h3startenddate h3startenddate">
                    	<?php if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->endDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->endDate));
                        } 
                      ?>
                    </h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="color:#002060;"><?php echo $NoOfReportsStart; ?></h3>
                    </div>
                  </center>
                </div>
                <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  <?php if ($NoOfReportsEnd > $NoOfReportsStart) { ?>
                     <div class="panel panel-inverse panel-primary boxShadow nooofreportspanelStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $NoOfReportsEnd - $NoOfReportsStart; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow noofrepotsIncreasedPanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconreports.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>

                   <?php }else if ($NoOfReportsEnd < $NoOfReportsStart) { ?>
                     <div class="panel panel-inverse panel-primary boxShadow nooofreportspanelStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="color:white">-<?php echo $NoOfReportsStart - $NoOfReportsEnd; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow noofreportsdecreasedPanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreasediconreports.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>

                   <?php }else{ ?>

                    <div class="panel panel-inverse panel-primary boxShadow noofreportsstablePanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;">&nbsp;<?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php  } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle"><h3 style="color:#002060;"><?php echo $NoOfReportsEnd; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
         </div>
        </div>
        <div class="row">
          <div class="col-md-6">
            <div class="runningstateheaderstyle">
              <h4 style="color:white;"><center> <?php echo Runningtime; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle startdateenddatefontmobile h3startenddate"><?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?>
                    </h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle startdateenddatefontmobile h3startenddate h3startenddate">
                      <?php    if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->endDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->endDate));
                        } 
                      ?>
                        
                      </h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>

              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="font-size:12px;color:#002060;"><?php echo $runningStartDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>

                <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  <?php if ($runningEndDataHours > $runningStartDataHours) { ?>
                   <div class="panel panel-inverse panel-primary boxShadow runningPanelstableStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                    </div>
                   <div class="panel panel-inverse panel-primary boxShadow runningPanelincreasedDecreasedStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasedicon.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                   <?php }else if ($runningEndDataHours < $runningStartDataHours) { ?>
                    <div class="panel panel-inverse panel-primary boxShadow runningPanelstableStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white"><?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                    </div>
                    <div class="panel panel-inverse panel-primary boxShadow runningPanelincreasedDecreasedStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/runningDecreased.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                   <?php }else{ ?>
                    <div class="panel panel-inverse panel-primary boxShadow runningPanelstableStyleee">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#76ba1b;"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php  } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle">
                      <h3 style="font-size:12px;color:#002060;"><?php echo $runningEndDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="noproductionheaderstyle">
              <h4 style="color:white;"><center> <?php echo Noproductiontime; ?> </center></h4>
            </div>
            <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
              <div class="row">
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="startdateStyle startdateenddatefontmobile h3startenddate"><?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?></h3>
                  </center>
                </div>
                <div class="col-md-3" style="min-width: 50%;">
                  <center>
                    <h3 class="enddateStyle startdateenddatefontmobile h3startenddate"><?php    if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?></h3>
                  </center>
                </div>
                <span class="divdotlinestyle" style="margin-left:22%;">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                  <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                </span>
              </div>
              <div class="row">
                <div class="col-md-2" style="min-width: 45%;">
                  <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="font-size:12px;color:#002060;"><?php echo $noProductionStartDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>
                <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  <?php if ($noProductionStartDataHours > $noProductionEndDataHours) { ?>
                    
                  <div class="panel panel-inverse panel-primary boxShadow noproductionPanelStyle">
                        <h3 style="font-size: 12px;color:#002060;padding:4px;">
                          <center style="padding-top: 3px;color:white">-<?php echo $noProductionStartDataHours - $noProductionEndDataHours; ?></center>
                        </h3>
                  </div>
                   <div class="panel panel-inverse panel-primary boxShadow noproductionPanelDecresedStyle">
                        <h3 style="font-size: 12px;color:#002060;padding:4px;">
                          <center style="color:#ff8000">
                            <img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconNoproduction.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                    </div>
                  <?php }else if ($noProductionStartDataHours < $noProductionEndDataHours) {  ?>
                    <div class="panel panel-inverse panel-primary boxShadow noproductionPanelStyle">
                        <h3 style="font-size: 12px;color:#002060;padding:4px;">
                          <center style="color:white">+<?php echo $noProductionEndDataHours - $noProductionStartDataHours; ?></center>
                        </h3>
                  </div>
                   <div class="panel panel-inverse panel-primary boxShadow noproductionPanelIncreasedStyle">
                        <h3 style="font-size: 12px;color:#002060;padding:4px;">
                          <center style="padding-top: 3px;color:#ff8000"><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreaseiconNoporduction.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                    </div>
                  <?php }else{ ?>
                   <div class="panel panel-inverse panel-primary boxShadow noproductionpanelstablestyle">
                        <h3 style="font-size: 12px;color:#002060;padding:4px;">
                          <center style="color:#ff8000"><?php echo Stable; ?></center>
                        </h3>
                    </div>
                  <?php } ?>
                </div>
                <div class="col-md-2" style="min-width: 45%;">
                   <center>
                    <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle"><h3 style="font-size:12px;color:#002060;"><?php echo $noProductionEndDataHours; ?><br><?php echo Hours; ?></h3>
                    </div>
                  </center>
                </div>
              </div>
            </div>
         </div>
        </div>
      </div>
    </div>

    <div class="col-md-12">
      <div class="panel panel-inverse panel-primary boxShadow" style="overflow: auto;min-height: 278px;">
        <h3 style="color:#002060;margin-top: 25px;margin-left: 20px;"><?php echo Reports; ?></h3>
        <div class="panel-body">
          <table id="" class="display table m-b-0 reportAProblemTable"  width="100%" cellspacing="0">
            <thead>
              <tr role="row" style="border-left: none;background:rgb(255,255,255); ">
                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterUserNameSelected" class="dropdown" style="padding: 4px;border-radius: 5px;min-width: 90px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterUserNameSelectedValue"> <?php echo Operator; ?>&nbsp;</span>
                    <div class="dropdown-menu .hdhdhhd">
                      <?php foreach ($users as $key => $value) { ?>
                        <div class="dropdown-item">
                          <div class="form-check" style="width: max-content;">
                            <input onclick="filterUserName()" class="filterUserName form-check-input" type="checkbox" value="<?php echo $value['userId']; ?>" id="filterUserName<?php echo $key; ?>" >
                            <label class="form-check-label" for="filterUserName<?php echo $key; ?>">
                              <?php echo $value['userName']; ?>
                            </label>
                          </div>
                        </div>
                      <?php } ?>
                    </div>
                  </div>
                <small style="color: #FF8000;text-align : center;" >&nbsp;</small>
                </th>

                <th id="tableheadreportresolved" >
                  <div style="margin-bottom: 17px;"><?php echo Date; ?><div>
                  <input type="hidden" name="dateValS" id="dateValS" value="<?php echo $meetingNoteDetails->startDate; ?>" />  
                  <input type="hidden" name="dateValE" id="dateValE" value="<?php echo $meetingNoteDetails->endDate; ?>" />   
                </th>

                <th style="color: #b8b0b0;cursor: pointer;padding-bottom: 0px !important; ">
                  <div id="filterTypeSelected" class="dropdown" style="padding: 4px;border-radius: 5px;text-transform: capitalize;min-width: 55px;" >
                    <span class="dropdown-toggle" data-toggle="dropdown" id="filterTypeSelectedValue"> <?php echo type; ?>&nbsp;</span>
                    <div class="dropdown-menu" >
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'1'" class="filterType form-check-input" type="checkbox" id="accident1" 
                           >
                          <label class="form-check-label" for="accident1">
                            <?php echo Accident; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'0'" class="filterType form-check-input" type="checkbox" id="incident1" 
                           >
                          <label class="form-check-label" for="incident1">
                            <?php echo Incident; ?>
                          </label>
                        </div>
                      </div>
                      <div class="dropdown-item">
                        <div class="form-check" style="width: max-content;">
                          <input onclick="filterType()" value="'2'" class="filterType form-check-input" type="checkbox" id="suggestion1" 
                           >
                          <label class="form-check-label" for="suggestion1">
                            <?php echo Suggestion; ?>
                          </label>
                        </div>
                      </div>
                    </div>
                  </div>
                <small style="color: #FF8000;"  >&nbsp;</small>
                </th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Description; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Cause; ?><div></th>
                <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Improvement; ?><div></th>

                <th id="tableheadreportresolved"> 
                <div class="dropdown" id="filterStatusSelected tableheadreportresolvedselected">
                  <span class="dropdown-toggle" data-toggle="dropdown" id="filterStatusSelectedValue" style="width: 50px;"> <?php echo confidential; ?>&nbsp;
                  </span>
                  <div class="dropdown-menu">
                    <div class="dropdown-item">
                      <div class="form-check" style="width: max-content;">
                        <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="confidential1" value="'1'">
                        <label class="form-check-label" for="confidential1">
                          <?php echo confidential; ?>
                        </label>
                      </div>
                    </div>
                    <div class="dropdown-item">
                      <div class="form-check" style="width: max-content;">
                        <input onclick="filterStatus()" class="filterStatus form-check-input" type="checkbox" id="not_confidential1" value="'0'">
                        <label class="form-check-label" for="not_confidential1">
                          <?php echo Notconfidential; ?>
                        </label>
                      </div>
                    </div>
                  </div>
                </div>
                <small style="color: #FF8000;text-align : center;"  >&nbsp;</small>
              </th>
              </tr>
            </thead>
          </table>
        </div>
      </div>
    </div>
    <div class="panel panel-inverse panel-primary boxShadow mainpagenotesHeader">
      <h3 class="mainpagenotesHeadernotes"><?php echo Note; ?></h3>
      <input type="hidden" id="noteId" value="<?php echo $meetingNoteDetails->noteId; ?>">
        <div class="row" style="margin-left: 10px;">
          <div class="col-md-12" style="flex: 0 0 47% !important;max-width: 47% !important;">
            <textarea class="mainpagenotespanelstyle" readonly="" placeholder="<?php echo EnteranynoteshereOptional; ?>" required maxlength="1000" onkeyup="checkwordcount();"  name="problemText" id="description"><?php echo $meetingNoteDetails->notes; ?></textarea>
          </div>
        </div>
      <hr style="background: gray;">
    <p>&copy; <?php echo date('Y'); ?> nytt | <?php echo AllRightsReserved; ?></p>
    </div>
    <a href="javascript:;" class="btn btn-icon btn-circle btn-success btn-scroll-to-top fade" data-click="scroll-top">
      <i class="fa fa-angle-up"></i>
    </a>
</div>

<div class="modal fade" id="modal-update-report-meeting-notes" style="display: none;" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header" style="background-color: #002060;">
        <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title machine_name" id="liveMachineName"><?php echo Report; ?></h4>
        <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal"><img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>"></button>
      </div>
      <div class="modal-body">
        <form class="p-b-20" action="update_reportProblem_form" method="POST" id="update_reportProblem_form" style="margin-left: 10px;margin-right: 10px;">
            <div class="row">
                <div class="col-md-4 colmd4updatereportproblemform">
                  <img id="userImage" style="border-radius: 50%;border: 3px solid #FFFFFF" height="85" width="80" src="">  
                </div>
                <div class="col-md-8 modalpanelnamestyle">
                  <span id="userName" style="font-size: 17px;color: #FF8000;font-weight: 700;">testnytt</span> <br>
                  <small style="font-size: 80%;" id="userId"><?php echo UserID; ?> : 56</small>
                </div>
              </div>

              <div class="row" style="">
                <div class="col-md-6 colmd6style">
                  <h3 id="typeText" style="color: #002060;margin-left: 10px;"></h3>
                  <input type="hidden" name="type" id="type">
                </div>

                <div class="col-md-6 colmd6style">
                  <small style="float: right;margin-top: 10px;padding-right: 10px;" id="date">2020-12-14</small>
                </div>
              </div>
              <div class="row" style="padding-left: 15px;margin-bottom: 15px;">
            </div>

            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Description; ?></b></small><br>
                <textarea class="textareaheader" name="description" id="descriptionText" readonly maxlength="1000" onkeyup="checkReportWordCount(this.id)"></textarea>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>
            
            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Cause; ?></b></small><br>
                <textarea class="textareaheader" name="cause" id="cause" maxlength="1000" readonly onkeyup="checkReportWordCount(this.id)"></textarea>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>

            <div class="row">
              <div class="col-md-12" style="padding-left: 26px;">
                <small><b><?php echo Improvement; ?></b></small><br>
                <textarea class="textareaheader" name="improvement" id="improvement" readonly maxlength="1000" onkeyup="checkReportWordCount(this.id)"></textarea>
                <hr style="height: 2px;margin-top: 0rem;">
              </div>
            </div>

            <div class="row" style="margin-top: 10px;">
              <div class="col-md-12" style="padding-left: 26px;">
                <div id="not_confidentials" style="display: none;">
                  <img  width="16" style="" src="<?php echo base_url('assets/img/cross.svg'); ?>"> 
                  <span style="color: red"> &nbsp;&nbsp;<b><?php echo Notconfidential; ?></b></span>
                </div>
                <div id="confidentials" style="display: none;">
                  <img width="16" style="" src="<?php echo base_url('assets/img/tick.svg'); ?>"> 
                  <span style="color: green"> &nbsp;&nbsp;<b><?php echo confidential; ?></b></span>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>
  </div>

  <div class="modal fade" id="modal-download-meeting-notes" style="display: none;" aria-hidden="true">
    <div class="modal-dialog">
      <div class="modal-content downloadpreviewmodalstyle modaldialogbody" >
        <div class="modal-header" style="background-color: #002060;">
          <h4 style="color: #FF8000;padding-left: 15px;" class="modal-title"><?php echo Downloadpreview; ?></h4>
          <button type="button" class="close" style="padding: 12px 32px !important;opacity: 1.0;" data-dismiss="modal">
            <img width="16" height="16" src="<?php echo base_url('assets/img/cross.svg'); ?>">
          </button>
        </div>
        <div class="modal-body" style="background: #f2f2f2;">

          <div class="row">
            <div class="col-md-12">
                <?php foreach ($machines as $key => $value) { ?>
                  <button type="button" class="btn btn-primary" style="background: #002060 !important;border-color: #002060!important;"><?php echo $value['machineName']; ?></button>
                <?php } ?>
            </div>
        </div>
        <div class="modaldialogbodystyle" style="background-color: white;margin-left: 68px;margin-right: 68px;padding: 20px;">
        <h3 style="color:#002060;margin-left:18px;">
          <?php echo ucfirst($meetingNoteDetails->noteName); ?> - <?php    if ($meetingNoteDetails->filterType == "1") 
          {
            echo Daily;
          }
          elseif ($meetingNoteDetails->filterType == "2") 
          {
            echo Weekly;
          }
          elseif ($meetingNoteDetails->filterType == "3") 
          {
            echo Monthly;
          }
          elseif ($meetingNoteDetails->filterType == "4") 
          {
            echo Yearly;
          } 
          ?>
        </h3>
        <div class="col-md-12">
          <div class="row" style="margin-top: 20px;">
            <div class="col-md-6">
              <div class="modalnoofstoppagesstyle">
                <h4 style="color:white;"><center> <?php echo Noofstoppages; ?> </center></h4>
              </div>
              <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
                <div class="row">
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="startdateStyle startdateenddatefontmobile modalrunningstatedatestyle">
                        <?php
                          if ($meetingNoteDetails->filterType == "1") 
                          {
                            echo $meetingNoteDetails->startDate; 
                          }
                          elseif ($meetingNoteDetails->filterType == "2") 
                          {
                            echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "3") 
                          {
                            echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "4") 
                          {
                            echo  date('Y',strtotime($meetingNoteDetails->startDate));
                          } 
                        ?>
                      </h3>
                    </center>
                  </div>
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="enddateStyle startdateenddatefontmobile modalrunningstatedatestyle">
                        <?php    if ($meetingNoteDetails->filterType == "1") 
                          {
                            echo $meetingNoteDetails->endDate; 
                          }
                          elseif ($meetingNoteDetails->filterType == "2") 
                          {
                            echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "3") 
                          {
                            echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "4") 
                          {
                            echo  date('Y',strtotime($meetingNoteDetails->endDate));
                          } 
                      ?>
                    </h3>
                    </center>
                  </div>
                  <span class="divdotlinestyle" style="margin-left:20%;">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                    <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  </span>
                </div>

                <div class="row">
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfStoppagesStart; ?></h3>
                      </div>
                    </center>
                  </div>
                  <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                    <?php if ($NoOfStoppagesStart > $NoOfStoppagesEnd) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofstoppagespanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">-<?php echo $NoOfStoppagesStart - $NoOfStoppagesEnd; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofstoppagespanelDecreasedStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100">
                            <img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                      </div>
                    <?php }else if ($NoOfStoppagesStart < $NoOfStoppagesEnd) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofstoppagespanelplusStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $NoOfStoppagesEnd - $NoOfStoppagesStart; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofstoppagespanelIncreasedStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increaseImageStopped.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                      </div>
                    <?php }else{ ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofstoppagespanelstableStyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#f60100">
                            <!-- <img style="height:9px;" src="<?php echo base_url();?>assets/img/DecreasedImage.png"> -->
                          &nbsp;<?php echo Stable; ?></center>
                        </h3>
                      </div>
                    <?php  } ?>
                  </div>
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle" style="font-size: 18px;"><h3 style="color:#002060;font-size: 18px;"><?php echo  $NoOfStoppagesEnd; ?></h3>
                      </div>
                    </center>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="modalnoofreportsheader">
                <h4 style="color:white;"><center> <?php echo Noofreports;?> </center></h4>
              </div>
              <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
                <div class="row">
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="startdateStyle startdateenddatefontmobile modalrunningstatedatestyle" style="">
                        <?php
                        if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->startDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->startDate));
                        } 
                        ?>
                      </h3>
                    </center>
                  </div>
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="enddateStyle startdateenddatefontmobile modalrunningstatedatestyle" style="">
                        <?php    if ($meetingNoteDetails->filterType == "1") 
                        {
                          echo $meetingNoteDetails->endDate; 
                        }
                        elseif ($meetingNoteDetails->filterType == "2") 
                        {
                          echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "3") 
                        {
                          echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                        }
                        elseif ($meetingNoteDetails->filterType == "4") 
                        {
                          echo  date('Y',strtotime($meetingNoteDetails->endDate));
                        } 
                        ?>
                      </h3>
                    </center>
                  </div>
                  <span class="divdotlinestyle" style="margin-left:20%;">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                    <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  </span>
                </div>
                <div class="row">
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfReportsStart; ?></h3>
                      </div>
                    </center>
                  </div>
                  <div class="col-md-1 circlecenterStyle" style="width: 10%;">

                    <?php if ($NoOfReportsEnd > $NoOfReportsStart) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofreportspanelheaderstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $NoOfReportsEnd - $NoOfReportsStart; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofreportspaneldecreasedstyle" style="min-width:45px;border-radius: 50px;width: 60px;height: 25px;font-size: 10px;width:max-content;">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:black;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconreports.png">&nbsp;<?php echo Increased; ?></center>
                         
                        </h3>
                      </div>

                    <?php }else if ($NoOfReportsEnd < $NoOfReportsStart) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofreportspanelheaderstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">-<?php echo $NoOfReportsStart - $NoOfReportsEnd; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoofreportspaneldecreasedstyle">
                        <h3 class="panelh3tagstyle">
                         <center style="padding-top: 3px;color:black;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreasediconreports.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                      </div>

                    <?php }else{ ?>

                      <div class="panel panel-inverse panel-primary boxShadow modalnoofreportsstablestyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:black;"><?php echo Stable; ?></center>
                        </h3>
                      </div>

                    <?php  } ?>
                  </div>
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle"><h3 style="color:#002060;font-size: 18px;"><?php echo $NoOfReportsEnd; ?></h3>
                      </div>
                    </center>
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div class="row">
            <div class="col-md-6">
              <div class="divtopmainstyle">
                <h4 style="color:white;"><center> <?php echo Runningtime; ?> </center>
                </h4>
              </div>
              <div class="panel panel-inverse panel-primary boxShadow" style="min-height: 160px;margin-top:8px;">
                <div class="row">
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="startdateStyle startdateenddatefontmobile modalrunningstatedatestyle">
                      <?php
                      if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->startDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->startDate));
                      } 
                      ?></h3>
                    </center>
                  </div>
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="enddateStyle startdateenddatefontmobile modalrunningstatedatestyle">
                        <?php    if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?></h3>
                    </center>
                  </div>
                  <span class="divdotlinestyle" style="margin-left:20%;">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                    <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  </span>
                </div>

                <div class="row">
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle"><h3 style="font-size:12px;color:#002060;"><?php echo $runningStartDataHours; ?><br><?php echo Hours; ?></h3>
                      </div>
                    </center>
                  </div>

                  <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                  <?php if ($runningEndDataHours > $runningStartDataHours) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalrunningpanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                      </div>
                      
                      <div class="panel panel-inverse panel-primary boxShadow modalrunningpanelincreasedstyle">
                        <h3 class="panelh3tagstyle">
                        	<center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasedicon.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                      </div>
                    <?php } else if ($runningEndDataHours < $runningStartDataHours) { ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalrunningpanelstylee">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white"><?php echo $runningEndDataHours - $runningStartDataHours; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalrunningpanelDecreasedstylee">
                        <h3 class="panelh3tagstyle">
                           <center style="padding-top: 3px;color:#76ba1b;"><img style="height:9px;" src="<?php echo base_url();?>assets/img/runningDecreased.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                      </div>
                    <?php }else{ ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalrunningpanelstablestyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#76ba1b;">
                          &nbsp;<?php echo Stable; ?></center>
                        </h3>
                      </div>
                    <?php  } ?>
                  </div>
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle">
                        <h3 style="font-size:12px;color:#002060;"><?php echo $runningEndDataHours; ?><br><?php echo Hours; ?></h3>
                      </div>
                    </center>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-md-6">
              <div class="modalnoproductionmainpanelstyle">
                <h4 style="color:white;"><center> <?php echo Noproductiontime; ?> </center></h4>
              </div>
              <div class="panel panel-inverse panel-primary boxShadow modalnoproductionpanelstyle">
                <div class="row">
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="startdateStyle startdateenddatefontmobile modalrunningstatedatestyle" style=""> 
                        <?php
                          if ($meetingNoteDetails->filterType == "1") 
                          {
                            echo $meetingNoteDetails->startDate; 
                          }
                          elseif ($meetingNoteDetails->filterType == "2") 
                          {
                            echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->startDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "3") 
                          {
                            echo  date('F, Y',strtotime($meetingNoteDetails->startDate));
                          }
                          elseif ($meetingNoteDetails->filterType == "4") 
                          {
                            echo  date('Y',strtotime($meetingNoteDetails->startDate));
                          } 
                          ?>
                        </h3>
                    </center>
                  </div>
                  <div class="col-md-3" style="min-width: 50%;">
                    <center>
                      <h3 class="enddateStyle startdateenddatefontmobile modalrunningstatedatestyle">
                        <?php if ($meetingNoteDetails->filterType == "1") 
                      {
                        echo $meetingNoteDetails->endDate; 
                      }
                      elseif ($meetingNoteDetails->filterType == "2") 
                      {
                        echo  'Week '. date('W, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "3") 
                      {
                        echo  date('F, Y',strtotime($meetingNoteDetails->endDate));
                      }
                      elseif ($meetingNoteDetails->filterType == "4") 
                      {
                        echo  date('Y',strtotime($meetingNoteDetails->endDate));
                      } 
                      ?></h3>
                    </center>
                  </div>
                  <span class="divdotlinestyle" style="margin-left:20%;">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                    <img class="dotlinestyle" style="max-width: 63%;" src="<?php echo base_url();?>assets/img/Path 378.png">
                    <img src="<?php echo base_url();?>assets/img/Ellipse 290.png">
                  </span>
                </div>
                <div class="row">
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepanelone mainpanelstyle">
                        <h3 style="font-size:12px;color:#002060;"><?php echo $noProductionStartDataHours; ?><br><?php echo Hours; ?></h3>
                      </div>
                    </center>
                  </div>
                  <div class="col-md-1 circlecenterStyle" style="width: 10%;">
                    <?php if ($noProductionStartDataHours > $noProductionEndDataHours) { ?>

                      <div class="panel panel-inverse panel-primary boxShadow modalnoproductionminuspanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">-<?php echo $noProductionStartDataHours - $noProductionEndDataHours; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoproductionDecreasedpanelstyle" style="">
                        <h3 class="panelh3tagstyle">
                           <center style="padding-top: 3px;color:#ff8000"><img style="height:9px;" src="<?php echo base_url();?>assets/img/increasediconNoproduction.png">&nbsp;<?php echo Decreased; ?></center>
                        </h3>
                      </div>
                    <?php }else if ($noProductionStartDataHours < $noProductionEndDataHours) {  ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoproductionpluspanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:white">+<?php echo $noProductionEndDataHours - $noProductionStartDataHours; ?></center>
                        </h3>
                      </div>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoproductionIncreasedpanelstyle">
                        <h3 class="panelh3tagstyle">
                           <center style="padding-top: 3px;color:#ff8000"><img style="height:9px;" src="<?php echo base_url();?>assets/img/decreaseiconNoporduction.png">&nbsp;<?php echo Increased; ?></center>
                        </h3>
                      </div>
                    <?php }else{ ?>
                      <div class="panel panel-inverse panel-primary boxShadow modalnoproductionstablepanelstyle">
                        <h3 class="panelh3tagstyle">
                          <center style="padding-top: 3px;color:#ff8000"><?php echo Stable; ?></center>
                        </h3>
                      </div>
                    <?php } ?>
                  </div>
                  <div class="col-md-2" style="min-width: 45%;">
                    <center>
                      <div class="panel panel-inverse panel-primary boxShadow circlepaneltwo mainpanelstyle">
                        <h3 style="font-size:12px;color:#002060;"><?php echo $noProductionEndDataHours; ?><br><?php echo Hours; ?></h3>
                      </div>
                    </center>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
        <div class="col-md-12 " style="padding-right: 0px;padding-left: 0px;">
          <div class="panel panel-inverse panel-primary boxShadow modalreportstyle">
            <h3 style="color:#002060;margin-top: 25px;margin-left: 20px;"><?php echo Report; ?></h3>
            <div class="panel-body">
              <table id="" class="display table m-b-0 reportAProblemTableModel"  width="50%" cellspacing="0">
                <thead>
                  <tr class="modalreporttrrowstyle" role="row">
                    <input type="hidden" name="dateValS" id="dateValS" value="<?php echo $meetingNoteDetails->startDate; ?>" />  
                    <input type="hidden" name="dateValE" id="dateValE" value="<?php echo $meetingNoteDetails->endDate; ?>" />  
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Operator; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Dates; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo type; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Description; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Cause; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo Improvement; ?><div></th>
                    <th class="tableheadreport"><div class="tableheadreportdescription"><?php echo confidential; ?><div></th>
                  </tr>
                </thead>
              </table>
            </div>
          </div>
        </div>

        <div class="panel panel-inverse panel-primary boxShadow modalnotesdetailstyle">
          <h3 class="modalnotesstyle"><?php echo Note; ?></h3>
            <div class="row" style="margin-left: 10px;">
              <div class="col-md-12 modalnotesmodalcolmd12style">
                <textarea class="modalmeetingnotescontentstyle" id="notesText" required maxlength="1000" readonly>
                  <?php echo $meetingNoteDetails->notes; ?>
                </textarea>
              </div>
            </div>
          </div>
        <hr style="height: 2px;background: #e5e7e8;">
      </div>
    </div>
    <div class="modal-footer">
      <center style="margin-bottom: 25px;">
        <a class="btn btn-primary modaldownloadfinalbuttonstyle" href="<?php echo base_url('MeetingNotes/downloadPdf/').$meetingNoteDetails->noteId; ?>" class="btn btn-primary">
          <img class="modaldownloadbuttonstyle" src="<?php echo base_url('assets/nav_bar/download.png'); ?>"> <?php echo Download; ?>
        </a>
      </center>
    </div>
  </div>
</div>


